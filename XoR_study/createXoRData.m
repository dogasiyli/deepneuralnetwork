function dS = createXoRData( dataTypeStr, numberOfSamples, percentages )
    dS = [];
    if ~exist('percentages','var')
       percentages = fillPercentages([]);
    else
        percentages = fillPercentages(percentages);
    end
    
    switch (dataTypeStr)
        otherwise%'2D'
            trainCnt = floor(numberOfSamples*percentages.Train);
            validCnt = floor(numberOfSamples*percentages.Validation);
            testCnt = floor(numberOfSamples*percentages.Test);
            [dS.dataTr, dS.labelsTr] = get2DXoR(trainCnt,1,'train',[1 3 1]);
            [dS.dataVa, dS.labelsVa] = get2DXoR(validCnt,1,'valid',[1 3 2]);
            [dS.dataTe, dS.labelsTe] = get2DXoR(testCnt,1,'test',[1 3 3]);
    end
end

function [X, labels] = get2DXoR(numberOfSamples, plotFigID, titleStr, plotFigSubID)
    if exist('plotFigID','var')
        figure(plotFigID);
    end
    if exist('plotFigSubID','var')
        h = subplot(plotFigSubID(1),plotFigSubID(2),plotFigSubID(3));hold on;
        cla(h);
    else
        clf;
    end
    
    X = rand(2,numberOfSamples);%all between 0 and 1
    sS = floor(linspace(0,numberOfSamples,5));%samplingStops
    labels = ones(1,numberOfSamples);
    k=0;
    for i1=[-1 +1]
        for j2=[-1 +1]
            k = k+1;%areaID
            fr = sS(k)+1;
            to = sS(k+1);
            X(1,fr:to) = i1*X(1,fr:to);
            X(2,fr:to) = j2*X(2,fr:to);            
            labels(fr:to) = -i1*j2;
            if     i1==-1 && j2 ==-1
                scatterModeStr = 'ro';%same=C1
            elseif i1==-1 && j2 ==+1
                scatterModeStr = 'r^';%dif=C2
            elseif i1==+1 && j2 ==-1
                scatterModeStr = 'g^';%dif=C2
            elseif i1==+1 && j2 ==+1
                scatterModeStr = 'go';%same=C1
            end
            scatter(X(1,fr:to),X(2,fr:to),scatterModeStr);
        end
    end 
    if exist('titleStr','var')
        title([titleStr '-n(' num2str(numberOfSamples) ')']);
    end
    labels(labels==+1) = 2;
    labels(labels==-1) = 1;
end

function percentages = fillPercentages(percentages)
    if isempty(percentages) || ~isstruct(percentages)
       percentages = struct; 
    end
    if isstruct(percentages)
        if (isfield(percentages,'Train') && ~isfield(percentages,'Validation'))
            percentages.Validation = 1-percentages.Train;  
        end
        if (~isfield(percentages,'Train') || ~isfield(percentages,'Validation'))
            percentages.Train = 0.85;
            percentages.Validation = 1-percentages.Train;  
        end
        if (~isfield(percentages,'Test'))
            percentages.Test = 0.2;
        end
    end
end
