function [X, labelsNew, labelCrossMat, accRetVec, confMat_ELM, W_2Layer, progressMade, zeroElements] = reOrganizeLabels(X, labelsGiven, labelCrossMatIn, maxClusterCount, testLDA, figIDVec)
    %We need to set maximum number of clusters to pick
    if nargin>2 && exist('figIDVec','var') && ~isempty(figIDVec)
        initiatePlot(figIDVec);
    end
%0. Helm = labelsGiven;
    [Helm, newClassLabels] = set_Helm(labelsGiven, labelCrossMatIn);
    
    [Welm, labelsPred, ~, ~, rankDeficient] = getELM_Weights_SoftMax(X', Helm);
    ldaMaxAcc = NaN(1,3);
    
    if testLDA
        labelsLDA = testLDAFunction(X,labelsGiven);
    else
        labelsLDA = [];
    end
    
    %outELM = X'*Welm;
%1. Apply weights on data
    [countFeat, countSamples] = size(X);
    %outELM = X*Welm;
    %[~,labelsPred] = max(outELM,[],2);
%2. Create the shuffleMat
    labelsGiven = reshape(labelsGiven,[],1);
    %sortELMMat = [labelsGiven labelsPred outELM];
    %[exampleShuffleMat, idx] = sortrows(sortELMMat,[1 2]);

%3. check th     
    categoryCountOld = max(labelsGiven);
    [accuracy_ELM, confMat_ELM] = calculateConfusion(labelsPred,labelsGiven,true,categoryCountOld);
    
%4. not all the elements of confusion matrix is meaningful
%   for example 6 or 8 samples is not meaningful for
    elementCountTreshold = mean(confMat_ELM(confMat_ELM>0))/10;
    elementCountsConfMat = sort(confMat_ELM(:),'descend');
    if categoryCountOld^2>maxClusterCount && elementCountTreshold<elementCountsConfMat(maxClusterCount)
        elementCountTreshold = elementCountsConfMat(maxClusterCount);
        disp(['treshold element count for this confusion matrix set to ' num2str(elementCountTreshold) ' due to maxClusterCount(' num2str(maxClusterCount) ')' ]);
    else
        disp(['treshold element count for this confusion matrix set to ' num2str(elementCountTreshold) ]);
    end
    
    [labelsNew, idxNew, labelCrossMat] = setNewXORcategoryLabel( labelsGiven, labelsPred, labelCrossMatIn, elementCountTreshold);

    if nargin>2 && exist('figIDVec','var') && ~isempty(figIDVec)
        plot_2Class_NCluster(X, labelsGiven, labelsPred, labelCrossMat);
    end
    
    %set unlabelled elements randomly
    slct = idxNew==0;
    zeroElements = find(slct);
    if ~isempty((zeroElements))
        labelsNew(zeroElements) = [];
        idxNew(zeroElements) = [];
        
        zeroElementsGiven = (1:length(labelsGiven))';
        zeroElementsGiven(idxNew) = [];
        
        try
            zeroElementRootLabels = labelsGiven(zeroElementsGiven);
            zeroElRtLblMap = labelCrossMatIn(:,end);
            zeroElementRootLabels = zeroElRtLblMap(zeroElementRootLabels);
            removedSamplesLabelHist = hist(zeroElementRootLabels, unique(zeroElRtLblMap));
            disp([mat2str(removedSamplesLabelHist) ' elements will be removed from learning.']);
        catch
            disp([num2str(length(zeroElementsGiven)) ' elements will be removed from learning.']);
        end
        labelsGiven(zeroElementsGiven) = [];
        labelsPred(zeroElementsGiven) = [];        
        if testLDA && ~isempty(labelsLDA)
            labelsLDA(zeroElementsGiven) = [];
        end
        
        idxNewMap = zeros(max(idxNew),1);
        idxNewMap(zeroElementsGiven) = 1;
        idxNewMap = cumsum(idxNewMap);
        idxVals = reshape(1:length(idxNewMap),[],1);
        idxVals = idxVals-idxNewMap;
        idxNew = idxVals(idxNew);
        clear idxNewMap idxVals
    end
    
    if ~isempty(zeroElements)
        X(:,zeroElementsGiven) = [];
    end
    X = X(:,idxNew);
    %plot_2Class_NCluster(idxNew);
    %can we give also 2 layer network params and accuracy??
    %calculate 2 layer accuracy and return 2 layer weights
    
    if exist('labelCrossMatIn','var') && ~isempty(labelCrossMatIn)
        [labelsGiven_Org, accuracy_Org, confMat_Org, labelCrossMat, accuracy_Org_LDA] = getRootClassLabels(labelsGiven, labelsPred, labelsLDA, labelCrossMat, labelCrossMatIn);
        ldaMaxAcc(2) = accuracy_Org_LDA;
        titleStr = {['Accuracy_{OriginMapped} = ' num2str(accuracy_Org,'%4.2f')];['confMat_{OriginMapped} = ' mat2str(confMat_Org)]};
        accRetVec = [accuracy_ELM accuracy_Org];
    else
        titleStr = {['confELM_{Original} = ' mat2str(confMat_ELM)]};
        accRetVec = [accuracy_ELM accuracy_ELM];
    end
    
    
    if exist('labelsGiven_Org','var')
        labelsOriginalReSorted = labelsGiven_Org(idxNew);
    else
        labelsOriginalReSorted = labelsGiven(idxNew);
    end
    X_layer2 = X'*Welm;
    X_layer2(X_layer2<0) = 0;
    [Welm_L2, labelsPred_2] = getELM_Weights_SoftMax(X_layer2, labelsOriginalReSorted);
    W_2Layer = cell(2,1);
    W_2Layer{1} = Welm;
    W_2Layer{2} = Welm_L2;
    disp('2 Layer network ConfMat according to source labels : ')
    [accuracy_2Layer, confMat_2Layer2] = calculateConfusion(labelsPred_2,labelsOriginalReSorted,true);
    titleStr = [['Accuracy_{MultiCluster=' num2str(categoryCountOld) '} = SL_{(' num2str(accuracy_ELM,'%4.2f') ')},2L_{(' num2str(accuracy_2Layer,'%4.2f') ')}'];titleStr(:,1)];
    titleStr = [titleStr(:,1);['LDA_{ACC(' mat2str(ldaMaxAcc,4) ')}']];
        
    %plot the old-vs-current situation
    if nargin>2 && exist('figIDVec','var')
        title(titleStr);
    end
    try
        categoryCountNew = size(labelCrossMat,1);
        progressMade = categoryCountNew>categoryCountOld;
    catch
        progressMade = true;
    end
    accRetVec = [accRetVec accuracy_2Layer ldaMaxAcc];
end

function isSubplot = initiatePlot(figIDVec)
    isSubplot = false;
    if length(figIDVec)==1
        figure(figIDVec);clf;hold on;            
    else
        %figIDVec can be [figID subPlot{R,C,rcid}] - 4 length
        figure(figIDVec(1));
        isSubplot = true;
        if figIDVec(4)==1
            clf;
        end
        h_sub = subplot(figIDVec(2),figIDVec(3),figIDVec(4));
        cla(h_sub);
        hold on;
    end
end

function [Helm, newClassLabels] = set_Helm(labelsGiven, labelCrossMatIn)
    Helm = full(sparse(labelsGiven,1:length(labelsGiven),ones(1,length(labelsGiven))))';
    Helm(Helm<1) = -1;
    newClassLabels = unique(labelsGiven);
    if ~isempty(labelCrossMatIn)
        newClassLabels = labelCrossMatIn(:,1)';
        for nc = newClassLabels
            ocMain = labelCrossMatIn(nc,2);
            for nc2 = newClassLabels
                ncRows = find(labelsGiven==nc2);
                ocSub = labelCrossMatIn(nc2,2);
                if ocMain==ocSub
                    Helm(ncRows,nc) = Helm(ncRows,nc) + 1;
%                 else
%                     Helm(ncRows,nc) = Helm(ncRows,nc) - 1;
                end
            end
        end
    end
end

function [labelsGiven_Org, accuracy_Org, confMat_Org, labelCrossMat, accuracy_Org_LDA] = getRootClassLabels(labelsGiven, labelsPred, labelsLDA, labelCrossMat, labelCrossMatIn)
    categoryCountNew = max(labelCrossMat(:,2));
    if exist('labelCrossMatIn','var') && ~isempty(labelCrossMatIn)
        labelsGiven_Org = labelsGiven;
        labelsPred_Org = labelsPred;
        if ~isempty(labelsLDA)
            labelsLDA_Org = labelsLDA;
        end
        categoryLabel_Org_Base = categoryCountNew+1;
        for c=1:categoryCountNew
            originalCategory = labelCrossMat(c,end);
            currentCategory = labelCrossMat(c,3);
            labelsGiven_Org(labelsGiven_Org==currentCategory) = originalCategory+categoryLabel_Org_Base;
            labelsPred_Org(labelsPred_Org==currentCategory) = originalCategory+categoryLabel_Org_Base;
            if ~isempty(labelsLDA)
                labelsLDA_Org(labelsLDA_Org==currentCategory) = originalCategory+categoryLabel_Org_Base;
            end
        end
        labelsGiven_Org = labelsGiven_Org - categoryLabel_Org_Base;
        labelsPred_Org = labelsPred_Org - categoryLabel_Org_Base;
        [accuracy_Org, confMat_Org] = calculateConfusion(labelsPred_Org,labelsGiven_Org,false);
        if ~isempty(labelsLDA)
            labelsLDA_Org = labelsLDA_Org - categoryLabel_Org_Base;
            [accuracy_Org_LDA, confMat_Org_LDA] = calculateConfusion(labelsLDA_Org,labelsGiven_Org,false);
        else
            accuracy_Org_LDA = NaN;
        end
    end
end

function labelsLDA = testLDAFunction(X,labelsGiven)
    labelsLDA = [];
    try
        [W_3D, W, infoGain, remainingFeatIDs, accPerMethod, bestMethodID_not1, methodIDPref, confMatAll] = ldaDG(X', labelsGiven);
        W_LDA = squeeze(W_3D(1:end-1,:,bestMethodID_not1));
        H_LDA = X(remainingFeatIDs,:)'*W_LDA;
        [~,labelsLDA] = max(H_LDA,[],2);
        [accuracy_LDA, confMat_LDA] = calculateConfusion(labelsLDA,labelsGiven,false);
        ldaMaxAcc([1 3]) = [accuracy_LDA 100*max(accPerMethod)];
    catch
    end
end
