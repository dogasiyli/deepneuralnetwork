orig_state = warning;
warning('off','all');
clc;
runMode=1;
testLDA = false;
ts = 2;

rng('default');
switch runMode
    case 1
        trials = [9 2;7 4;13 4;1 4;37 4;28 4];
        maxClusterCount = 12;
    case 2
        trials = [9 10;7 10;13 10;1 10;37 10;28 10];
        maxClusterCount = 16;
end
rng(trials(ts,1));
%%1. create XOR data
%dS.dataTr,dS.dataVa,dS.dataTe are [d by Nx]
%dS.labelsTr,dS.labelsVa,dS.labelsTe are [1 by Nx]
switch runMode
    case 1
        dS = createXoRData( '', 1000);
    case 2
        dS = load('C:\FastDiskTemp\cC(after_cde4e7e_2_8)-ds(CIFAR10)-exID(5)\dsSample.mat','dS_2_8');
        dS = dS.dS_2_8;
end
%initial
T = trials(ts,2);

switch runMode
    case 1
        Rc = ceil(sqrt(T));
        Cc = ceil(T/Rc);
        figIDVec = [2 Rc Cc 1];
    case 2
        figIDVec = [];
end

[resultTable, W_2Layer, labelCrossMat, confMatCell] = apply_XOR_Test(dS, T, maxClusterCount, testLDA, figIDVec);
resultTableColCnt = size(resultTable,2);
switch resultTableColCnt
    case 9
        disptable(resultTable,'hidNodeCnt|TrELM|TrOrigMapped|Tr2Layer|LDASimp|LDAOrigMapped|LDABest|Va2L|Te2L')
    case 8
        disptable(resultTable,'hidNodeCnt|TrELM|TrOrigMapped|Tr2Layer|LDASimp|LDAOrigMapped|LDABest|VaOrTe')
    case 7
        disptable(resultTable,'hidNodeCnt|TrELM|TrOrigMapped|Tr2Layer|LDASimp|LDAOrigMapped|LDA_VaTe')
    case 6
        disptable(resultTable,'hidNodeCnt|TrELM|TrOrigMapped|Tr2Layer|Va2L|Te2L')
end

XTr = dS.dataTr'* W_2Layer{end}{1};
XTr(XTr<0) = 0;%ReLu
disp('Train result without removal of data');
predTr = analyzeActivationsElm(XTr, dS.labelsTr,  W_2Layer{end}{2}, false);
[accTr, confTr] = calculateConfusion(predTr,dS.labelsTr,true);

if isfield(dS,'dataVa') && isfield(dS,'labelsVa')
    XVa = dS.dataVa'* W_2Layer{end}{1};
    XVa(XVa<0) = 0;%ReLu
    disp('Validation result without removal of data');
    predVa = analyzeActivationsElm(XVa, dS.labelsVa,  W_2Layer{end}{2}, false);
    [accVa, confVa] = calculateConfusion(predVa,dS.labelsVa,true);
end

if isfield(dS,'dataTe') && isfield(dS,'labelsTe')
    XTe = dS.dataTe'* W_2Layer{end}{1};
    XTe(XTe<0) = 0;%ReLu
    disp('Test result without removal of data');
    predTe = analyzeActivationsElm(XTe, dS.labelsTe,  W_2Layer{end}{2}, false);
    [accTe, confTe] = calculateConfusion(predTe,dS.labelsTe,true);
end

% %lCM = [1 1 1;2 2 1;1 3 2;2 4 2];
% lCM = [1 1 1;2 2 2];
% categoryCountNew = max(lCM(:,2));
% figure(3);clf;
% subplot(3,1,1);
% labTrPlot = setNewXORcategoryLabel( dS.labelsTr, predTr, [], 0);
% plot_2Class_NCluster(XTr, dS.labelsTr, predTr, lCM, categoryCountNew);
% subplot(3,1,2);
% plot_2Class_NCluster(XVa, dS.labelsVa, predVa, lCM, categoryCountNew);
% subplot(3,1,3);
% plot_2Class_NCluster(XTe, dS.labelsTe, predTe, lCM, categoryCountNew);

warning(orig_state);

% %%2. classify XOR data using ELM
% [Welm, predictionsELM, confMat_ELM, accuracy_ELM, rankDeficient] = getELM_Weights_SoftMax(dS.dataTr', dS.labelsTr);
% categoryCount = 2;
% %2.1. now get the output values
%     outELM = dS.dataTr'*Welm;
%     sortELMMat = [dS.labelsTr' predictionsELM outELM];
%     [exampleShuffleMat, idx] = sortrows(sortELMMat,[1 2]);
%     labels2 = categoryCount * (exampleShuffleMat(:,1)-1) + exampleShuffleMat(:,2);
%     dataTr = dS.dataTr(:,idx);
% 
% [Welm2, predictionsELM2, confMat_ELM2, accuracy_ELM2, rankDeficient2] = getELM_Weights_SoftMax(dataTr', labels2);
% categoryCount = 4;
%     outELM2 = dataTr'*Welm2;
%     sortELMMat = [labels2 predictionsELM2 outELM2];
%     [exampleShuffleMat, idx] = sortrows(sortELMMat,[1 2]);