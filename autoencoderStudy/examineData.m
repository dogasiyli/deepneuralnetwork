function calcedInfo = examineData(X, knownInfo, displayResults)
    calcedInfo = struct;
    calcedInfo.meanX = mean(X);
    calcedInfo.varX = var(X);
    calcedInfo.varSumX = sum(calcedInfo.varX);
    calcedInfo.covarX = cov(X);
    
    if displayResults
        if ~isempty(knownInfo) && isstruct(knownInfo) && isfield(knownInfo,'meanX')
            disp('The given mean of data(top), the calculated mean(bottom) : ');
            disp([knownInfo.meanX;calcedInfo.meanX]);        
        else
            disp('The calculated mean : ');
            disp(calcedInfo.meanX);        
        end
        if ~isempty(knownInfo) && isstruct(knownInfo) && isfield(knownInfo,'varX')
            disp('The given var of data(top), the calculated var(bottom) : ');
            disp([knownInfo.varX;calcedInfo.varX]);        
        else
            disp('The calculated mean : ');
            disp(calcedInfo.varX);        
        end
    end
end