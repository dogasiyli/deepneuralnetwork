function [ X, Y, Y_sig] = generateAutoencoderExample(numOfSamplesToGenerate, dimension_X, dimension_Y, displayResults)
    %% 1.Generate the X as the source data
    %dimension_X = 4;
    knownInfo.meanX = 3*ones(1,dimension_X);
    knownInfo.varX = ones(1,dimension_X);
    knownInfo.covarX = eye(dimension_X);
    %numOfSamplesToGenerate = 10000;    
    X = mvnrnd(knownInfo.meanX,knownInfo.covarX,numOfSamplesToGenerate);
    
    %% 2.Generate an orthonormal weight matrix
    %dimension_Y = 10;
    W_X_Y = rand(dimension_X,dimension_Y);
    W_X_Y_ort = orthonormalizeWeightMat(W_X_Y')';
    
    %% 3.Calculate Y by projecting X with this orthogonal matrix
    Y = X*W_X_Y_ort;    

    %% 5.calculate Y_sigmoid as Y_sig
    [act, actDerivative, actCatalizor] = setActivationFunction('sigm');
    Y_sig = act(Y);
    
    %% 4.Examine X, Y and Y_sig
    calcedInfo_X = examineData(X, knownInfo, displayResults);
    calcedInfo_Y = examineData(Y, [], displayResults);
    calcedInfo_Y_sig = examineData(Y_sig, [], displayResults);    
end