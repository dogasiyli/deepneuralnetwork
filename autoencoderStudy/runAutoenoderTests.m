function runAutoenoderTests
    dimension_X = 4; 
    dimension_Y = 10;
    [X, Y, Y_sig] = generateAutoencoderExample(dimension_X, dimension_Y, true);
    
    displayOpts = setDisplayOpts('all');
    saveOpts = setSaveOpts('none');
    paramsAutoEncode = setParams_autoEncoder();
    applyZCA = false;
    applySparsity = true;
    useActSecondLayer = true;
    
    [opttheta_Y_1_1, W, b, meanData, ZCA, sizeVis] = autoencodeData(Y', dimension_X ...
                                                              , 'displayOpts', displayOpts ...
                                                              , 'saveOpts', saveOpts...
                                                              , 'paramsAutoEncode', paramsAutoEncode...
                                                              , 'applySparsity', applySparsity...
                                                              , 'useActSecondLayer', useActSecondLayer...
                                                              , 'applyZCA', applyZCA );
    [Welm, biasELM, W_randoms] = ELM_autoencoder(Y_sig, 'sizeHid', dimension_X, 'useBias', true, 'displayOpts', displayOpts);
end

