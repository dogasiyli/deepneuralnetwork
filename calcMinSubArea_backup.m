function [minSubTotalCur, rCur, cCur, subArea] = calcMinSubArea(M,subRowCnt,subColCnt)
    if isempty(M)
        M = reshape(1:(subRowCnt*subColCnt*4),subRowCnt*2,subColCnt*2);
        M(subRowCnt - 1,subColCnt + 1) =-100;
    end
    imSizeCur = size(M);
    strideFixed = 1;
    M1 = reshape(cumsum(M(:)),imSizeCur);
    M2 = zeros(imSizeCur+1);
    M2(2:end,2:end) = M1;
    
    
    row_y_len = imSizeCur(1);
    col_x_len = imSizeCur(2);
    imCropVec = zeros(1,4);%rowY_beg, rowY_end, colX_beg, colX_end,
    
    row_y_cur = subRowCnt;
    col_x_cur = subColCnt;

    %<col_x_cur> will move with <strideFixed> steps on <col_x_len>
    row_y_begs = 1:strideFixed:(row_y_len-row_y_cur+1);%row_y_begs - beginnings
    col_x_begs = 1:strideFixed:(col_x_len-col_x_cur+1);%col_x_begs - beginnings

    row_y_ends = row_y_begs + row_y_cur -1;
    col_x_ends = col_x_begs + col_x_cur -1;

    row_rep_cnt = size(row_y_ends,2);
    col_rep_cnt = size(col_x_ends,2);


    [mX,mY] = meshgrid(1:col_rep_cnt,1:row_rep_cnt);
    row_y_begs = row_y_begs(mY);
    col_x_begs = col_x_begs(mX);
    row_y_ends = row_y_ends(mY);
    col_x_ends = col_x_ends(mX);

    imCntSizPerScaleVec = [col_rep_cnt*row_rep_cnt row_y_cur col_x_cur];
    imCropVec = [row_y_begs(:) row_y_ends(:) col_x_begs(:) col_x_ends(:)];
    
    A = M2(sub2ind(1+imSizeCur,row_y_begs(:),col_x_begs(:)));
    B = M2(sub2ind(1+imSizeCur,row_y_begs(:),1+col_x_ends(:)));
    C = M2(sub2ind(1+imSizeCur,1+row_y_ends(:),col_x_begs(:)));
    D = M2(sub2ind(1+imSizeCur,1+row_y_ends(:),1+col_x_ends(:)));
    
    subTotalsAll = A+D-B-C;
    
    [minSubTotalCur, minInds] = min(subTotalsAll);
    rCur = row_y_begs(minInds);
    cCur = col_x_begs(minInds);
    subArea = [row_y_begs(minInds) row_y_ends(minInds) col_x_begs(minInds) col_x_ends(minInds)];%rowY_beg, rowY_end, colX_beg, colX_end,
end