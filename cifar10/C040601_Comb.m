%% 1. clear all and then create the dataset from scracth to have 4 classes
% clearvars;runFirst;clc; %#ok<CLSCR>
% dS = checkRecreateCifarBatches([1 2 6 8], 5, [20 12 12], true);
global fastDiskTempPath;
displayOpts_None = setDisplayOpts('none');
displayOpts_All = setDisplayOpts('all');
saveMainFolder = [fastDiskTempPath 'C040601_Comb' filesep];
if exist(saveMainFolder,'dir')
    rmdir(saveMainFolder,'s');
end
saveOpts_None = setSaveOpts('none', saveMainFolder);


%% 2. assign the network names and places
global dropboxPath;
net1fn_L12_19_1_2 = [dropboxPath 'slctNetStruct' filesep 'L12_cnnStr_eID19_acc85.75.mat'];
net2fn_L17_09_1_6 = [dropboxPath 'slctNetStruct' filesep 'L17_cnnStr_eID09_acc93.46.mat'];
net3fn_S21_10_1_8 = [dropboxPath 'slctNetStruct' filesep 'S21_cnnStr_eID10_acc92.97.mat'];
net4fn_L18_08_2_6 = [dropboxPath 'slctNetStruct' filesep 'L18_cnnStr_eID08_acc94.81.mat'];
net5fn_S20_10_2_8 = [dropboxPath 'slctNetStruct' filesep 'S20_cnnStr_eID10_acc94.76.mat'];
net6fn_L15_09_6_8 = [dropboxPath 'slctNetStruct' filesep 'L15_cnnStr_eID09_acc81.96.mat'];


%% 3. manually create the last layer with additional information about the final layers
allC = [1 2 6 8];

disp('***********L12_1_2***************')
[dataAllLL_L12_tr, labelAllLL_L12_tr, dataAllLL_L12_va, labelAllLL_L12_va, dataAllLL_L12_te, labelAllLL_L12_te, W_L12_C1_C2, res_L12_C1_C2] = runNetOnMoreData(dS, net1fn_L12_19_1_2, allC, [1 2], 'L12-C_{1-2-6-8}', 12681201, saveMainFolder);
% (1124/7972)85.90 percent accuracy
% (287/2028)85.85 percent accuracy
% (253/2000)87.35 percent accuracy

disp('***********L17_1_6***************')
[dataAllLL_L17_tr, labelAllLL_L17_tr, dataAllLL_L17_va, labelAllLL_L17_va, dataAllLL_L17_te, labelAllLL_L17_te, W_L17_C1_C6, res_L17_C1_C6] = runNetOnMoreData(dS, net2fn_L17_09_1_6, allC, [1 6], 'L17-C_{1-2-6-8}', 12681701, saveMainFolder);
% (565/7961)92.90 percent accuracy
% (157/2039)92.30 percent accuracy
% (172/2000)91.40 percent accuracy

disp('***********S21_1_8***************')
[dataAllLL_S21_tr, labelAllLL_S21_tr, dataAllLL_S21_va, labelAllLL_S21_va, dataAllLL_S21_te, labelAllLL_S21_te, W_S21_C1_C8, res_S21_C1_C8] = runNetOnMoreData(dS, net3fn_S21_10_1_8, allC, [1 8], 'S21-C_{1-2-6-8}', 12682101, saveMainFolder);
% (603/8009)92.47 percent accuracy
% (147/1991)92.62 percent accuracy
% (162/2000)91.90 percent accuracy

disp('***********L18_2_6***************')
[dataAllLL_L18_tr, labelAllLL_L18_tr, dataAllLL_L18_va, labelAllLL_L18_va, dataAllLL_L18_te, labelAllLL_L18_te, W_L18_C2_C6, res_L18_C2_C6] = runNetOnMoreData(dS, net4fn_L18_08_2_6, allC, [2 6], 'L18-C_{1-2-6-8}', 12681801, saveMainFolder);
% (423/7961)94.69 percent accuracy
% (111/2039)94.56 percent accuracy
% (116/2000)94.20 percent accuracy

disp('***********S20_2_8***************')
[dataAllLL_S20_tr, labelAllLL_S20_tr, dataAllLL_S20_va, labelAllLL_S20_va, dataAllLL_S20_te, labelAllLL_S20_te, W_S20_C2_C8, res_S20_C2_C8] = runNetOnMoreData(dS, net5fn_S20_10_2_8, allC, [2 8], 'S20-C_{1-2-6-8}', 12682001, saveMainFolder);
% (409/8009)94.89 percent accuracy
% (131/1991)93.42 percent accuracy
% (124/2000)93.80 percent accuracy

disp('***********L15_6_8***************')
[dataAllLL_L15_tr, labelAllLL_L15_tr, dataAllLL_L15_va, labelAllLL_L15_va, dataAllLL_L15_te, labelAllLL_L15_te, W_L15_C6_C8, res_L15_C6_C8] = runNetOnMoreData(dS, net6fn_L15_09_6_8, allC, [6 8], 'L15-C_{1-2-6-8}', 12681501, saveMainFolder);
% (1406/7998)82.42 percent accuracy
% (394/2002)80.32 percent accuracy
% (411/2000)79.45 percent accuracy

dataAll_tr = [dataAllLL_L12_tr;dataAllLL_L17_tr;dataAllLL_S21_tr;dataAllLL_L18_tr;dataAllLL_S20_tr;dataAllLL_L15_tr];
dataAll_va = [dataAllLL_L12_va;dataAllLL_L17_va;dataAllLL_S21_va;dataAllLL_L18_va;dataAllLL_S20_va;dataAllLL_L15_va];
dataAll_te = [dataAllLL_L12_te;dataAllLL_L17_te;dataAllLL_S21_te;dataAllLL_L18_te;dataAllLL_S20_te;dataAllLL_L15_te];

labelAll_tr = labelAllLL_L12_tr;
labelAll_va = labelAllLL_L12_va;
labelAll_te = labelAllLL_L12_te;

[W_sl_All, ~, ~, ~, rd_All] = getELM_Weights_SoftMax(dataAll_tr', labelAll_tr, [], displayOpts_All);
[~, ~, ~, acc_sl_ALL_tr, cM_sl_All_tr] = analyzeActivationsElm(dataAll_tr', labelAll_tr, W_sl_All, true);
[~, ~, ~, acc_sl_ALL_va, cM_sl_All_va] = analyzeActivationsElm(dataAll_va', labelAll_va, W_sl_All, true);
[~, ~, ~, acc_sl_ALL_te, cM_sl_All_te] = analyzeActivationsElm(dataAll_te', labelAll_te, W_sl_All, true);
% (3332/15970)79.14 percent accuracy
% (868/4030)78.46 percent accuracy
% (863/4000)78.42 percent accuracy

W_hid_75_12 = zeros(75,12);
W_hid_75_12( 1:10,[ 1  2]) = W_L12_C1_C2;
W_hid_75_12(11:20,[ 3  4]) = W_L17_C1_C6;
W_hid_75_12(21:30,[ 5  6]) = W_S21_C1_C8;
W_hid_75_12(31:40,[ 7  8]) = W_L18_C2_C6;
W_hid_75_12(41:50,[ 9 10]) = W_S20_C2_C8;
W_hid_75_12(51:75,[11 12]) = W_L15_C6_C8;

dataAll_tr_75_12 = W_hid_75_12'*dataAll_tr;
dataAll_va_75_12 = W_hid_75_12'*dataAll_va;
dataAll_te_75_12 = W_hid_75_12'*dataAll_te;
[W_75_12_All, ~, ~, ~, rd_All] = getELM_Weights_SoftMax(dataAll_tr_75_12', labelAll_tr, [], displayOpts_All);
[~, ~, ~, acc_75_12_ALL_tr, cM_75_12_All_tr] = analyzeActivationsElm(dataAll_tr_75_12', labelAll_tr, W_75_12_All, true);
[~, ~, ~, acc_75_12_ALL_va, cM_75_12_All_va] = analyzeActivationsElm(dataAll_va_75_12', labelAll_va, W_75_12_All, true);
[~, ~, ~, acc_75_12_ALL_te, cM_75_12_All_te] = analyzeActivationsElm(dataAll_te_75_12', labelAll_te, W_75_12_All, true);
% (3539/15970)77.84 percent accuracy
% (931/4030)76.90 percent accuracy
% (925/4000)76.88 percent accuracy

loadFolderForDummyData = [saveMainFolder 'data' filesep];
dS_LastLayer = create_dS_frData(dataAll_tr, labelAll_tr, 'data_valid', dataAll_va, 'labels_valid', labelAll_va,  'data_test', dataAll_te, 'labels_test', labelAll_te, 'loadFolder', loadFolderForDummyData);
[opt_cnnnet, numOfELMLearnt, elmStruct_dropTrue, categoryCount] = createFullyConnectedWeights(dS_LastLayer);


% % %% 3. combine the first two networks
% % [cnnnet_01_L12_L17, combCNNFile_01_L12_L17] = combineTwoNet( net1fn_L12_19_1_2, net2fn_L17_09_1_6, 'L12_{19}-L17_{09}', false);
% % [cnnnet_02_S21_L18, combCNNFile_02_S21_L18] = combineTwoNet( net3fn_S21_10_1_8, net4fn_L18_08_2_6, 'S21_{10}-L18_{08}', false);
% % [cnnnet_03_S20_L15, combCNNFile_03_S20_L15] = combineTwoNet( net5fn_S20_10_2_8, net6fn_L15_09_6_8, 'S20_{10}-L15_{09}', false);
% % 
% % [cnnnet_04_L12_L17_S21_L18, combCNNFile_04_L12_L17_S21_L18] = combineTwoNet( combCNNFile_01_L12_L17, combCNNFile_02_S21_L18, 'L12-L17-S21-L18', false);
% % [cnnnet_05_L12_L17_S21_L18_S20_L15, combCNNFile_05_L12_L17_S21_L18_S20_L15] = combineTwoNet( combCNNFile_04_L12_L17_S21_L18, combCNNFile_03_S20_L15, 'L12-L17-S21-L18-S20-L15', false);
% % 
% % %% 4. run the combined cnnet with train validation test
% % [ cnnnet_05_L12_L17_S21_L18_S20_L15, ~, ~, ~, dataLL_C040601_tr, labels_LL_C040601_tr] = deepNet_Test( dS, cnnnet_05_L12_L17_S21_L18_S20_L15, displayOpts_None, saveOpts_None, 'train', 'C040601', 0, true);
% %         cnnnet_05_L12_L17_S21_L18_S20_L15{end, 1}.Weight = cnnnet_05_L12_L17_S21_L18_S20_L15{1, 1}.Welm';
% %         cnnnet_05_L12_L17_S21_L18_S20_L15{end, 1}.BiasParams = zeros(size(cnnnet_05_L12_L17_S21_L18_S20_L15{end, 1}.BiasParams));
% % cnnnet_05_L12_L17_S21_L18_S20_L15 = deepNet_Test( dS, cnnnet_05_L12_L17_S21_L18_S20_L15, displayOpts_None, saveOpts_None, 'train', 'C040601', 1, true);
% % 
% % [ ~, ~, ~, ~, dataLL_C040601_va, labels_LL_C040601_va] = deepNet_Test( dS, cnnnet_05_L12_L17_S21_L18_S20_L15, displayOpts_None, saveOpts_None, 'valid', 'C040601', 1, true);
% % [ ~, ~, ~, ~, dataLL_C040601_te, labels_LL_C040601_te] = deepNet_Test( dS, cnnnet_05_L12_L17_S21_L18_S20_L15, displayOpts_None, saveOpts_None, 'test', 'C040601', 1, true);
% % 
% % %% 4. first check the implementation details by picking the so called classes and their features
% % netCols = [1 10;11 20;21 30;31 40;41 50;51 75];
% % %net1fn_L12_19_1_2
% % slct_1_2_tr = find(labels_LL_C040601_tr==1 | labels_LL_C040601_tr==2);
% % data_C1_C2_f01_f10_tr = dataLL_C040601_tr(netCols(1,1):netCols(1,2),slct_1_2_tr);
% % labels_C1_C2_tr = labels_LL_C040601_tr(slct_1_2_tr);
% % 
% % slct_1_2_va = find(labels_LL_C040601_va==1 | labels_LL_C040601_va==2);
% % data_C1_C2_f01_f10_va = dataLL_C040601_va(netCols(1,1):netCols(1,2),slct_1_2_va);
% % labels_C1_C2_va = labels_LL_C040601_va(slct_1_2_va);
% % 
% % slct_1_2_te = find(labels_LL_C040601_te==1 | labels_LL_C040601_te==2);
% % data_C1_C2_f01_f10_te = dataLL_C040601_te(netCols(1,1):netCols(1,2),slct_1_2_te);
% % labels_C1_C2_te = labels_LL_C040601_te(slct_1_2_te);
% % 
% % [W_C1_C2_f01_f10, ~, ~, ~, rd_C1_C2_f01_f10] = getELM_Weights_SoftMax(data_C1_C2_f01_f10_tr', labels_C1_C2_tr, [], setDisplayOpts('all'));
% % [~, ~, ~, accuracy_C1_C2_f01_f10_tr, confMat_C1_C2_f01_f10_tr] = analyzeActivationsElm(data_C1_C2_f01_f10_tr', labels_C1_C2_tr, W_C1_C2_f01_f10, true);
% % [~, ~, ~, accuracy_C1_C2_f01_f10_va, confMat_C1_C2_f01_f10_va] = analyzeActivationsElm(data_C1_C2_f01_f10_va', labels_C1_C2_va, W_C1_C2_f01_f10, true);
% % [~, ~, ~, accuracy_C1_C2_f01_f10_te, confMat_C1_C2_f01_f10_te] = analyzeActivationsElm(data_C1_C2_f01_f10_te', labels_C1_C2_te, W_C1_C2_f01_f10, true);
% % 
% % 
% % 
% % dS = checkRecreateCifarBatches([1 2 6 8], 5, [20 12 12], true);
% % [cnnnet_07_L12_L15, combCNNFile_07_L12_L15] = combineTwoNet( net1fn_L12_19_1_2, net6fn_L15_09_6_8, 'L12_{19}-L15_{09}', false);
% % 
% % [ cnnnet_07_L12_L15, ~, ~, dataLL_L12_L15_tr, labeLL_L12_L15_tr] = deepNet_Test( dS, cnnnet_07_L12_L15, displayOpts_None, saveOpts_None, 'train', 'L12-L15-C_{1-2-6-8}', 12681215, true);
% %         cnnnet_07_L12_L15{end, 1}.Weight = cnnnet_07_L12_L15{1, 1}.Welm';
% %         cnnnet_07_L12_L15{end, 1}.BiasParams = zeros(size(cnnnet_07_L12_L15{end, 1}.BiasParams));
% % cnnnet_07_L12_L15 = deepNet_Test( dS, cnnnet_07_L12_L15, displayOpts_None, saveOpts_None, 'train', 'L12-L15-C_{1-2-6-8}', 12681215, true);
% % [ ~, ~, ~, ~, dataLL_L12_L15_va, labels_LL_L12_L15_va] = deepNet_Test( dS, cnnnet_07_L12_L15, displayOpts_None, saveOpts_None, 'valid', 'L12-L15-C_{1-2-6-8}', 12681215, true);
% % [ ~, ~, ~, ~, dataLL_L12_L15_te, labels_LL_L12_L15_te] = deepNet_Test( dS, cnnnet_07_L12_L15, displayOpts_None, saveOpts_None, 'test', 'L12-L15-C_{1-2-6-8}', 12681215, true);
% % 
% % cnnnet_L15_C_6_8{15, 1}.countCategory = 4;
% % [ ~, ~, ~, ~, dataAllLL_L15_tr, labelAllLL_L15_tr] = deepNet_Test( dS, cnnnet_L15_C_6_8, displayOpts_None, saveOpts_None, 'train', 'L15-C_{1-2-6-8}', 126815, true);
% % [ ~, ~, ~, ~, dataAllLL_L15_va, labelAllLL_L15_va] = deepNet_Test( dS, cnnnet_L15_C_6_8, displayOpts_None, saveOpts_None, 'valid', 'L15-C_{1-2-6-8}', 126815, true);
% % [ ~, ~, ~, ~, dataAllLL_L15_te, labelAllLL_L15_te] = deepNet_Test( dS, cnnnet_L15_C_6_8, displayOpts_None, saveOpts_None, 'test', 'L15-C_{1-2-6-8}', 126815, true);
% % 
% % dataAllLL_L12_L15_tr = [dataAllLL_L12_tr;dataAllLL_L15_tr];
% % dataAllLL_L12_L15_va = [dataAllLL_L12_va;dataAllLL_L15_va];
% % dataAllLL_L12_L15_te = [dataAllLL_L12_te;dataAllLL_L15_te];
% % assert(sum(labelAllLL_L12_tr-labelAllLL_L15_tr)==0,'these must be same')
% % assert(sum(labelAllLL_L12_va-labelAllLL_L15_va)==0,'these must be same')
% % assert(sum(labelAllLL_L12_te-labelAllLL_L15_te)==0,'these must be same')
% % labelAllLL_L12_L15_tr = labelAllLL_L12_tr;
% % labelAllLL_L12_L15_va = labelAllLL_L12_va;
% % labelAllLL_L12_L15_te = labelAllLL_L12_te;
% % [W_CALL_L12_L15, ~, ~, ~, rd_CALL_L12_L15] = getELM_Weights_SoftMax(dataAllLL_L12_L15_tr', labelAllLL_L12_L15_tr, [], setDisplayOpts('all'));
% % [~, ~, ~, accuracy_CALL_L12_L15_tr, confMat_CALL_L12_L15_tr] = analyzeActivationsElm(dataAllLL_L12_L15_tr', labelAllLL_L12_L15_tr, W_CALL_L12_L15, true);
% % [~, ~, ~, accuracy_CALL_L12_L15_va, confMat_CALL_L12_L15_va] = analyzeActivationsElm(dataAllLL_L12_L15_va', labelAllLL_L12_L15_va, W_CALL_L12_L15, true);
% % [~, ~, ~, accuracy_CALL_L12_L15_te, confMat_CALL_L12_L15_te] = analyzeActivationsElm(dataAllLL_L12_L15_te', labelAllLL_L12_L15_te, W_CALL_L12_L15, true);

%can I do better than %70 on the combined final layers of L12 and L15?
%is the networks combination procedure correct - if it was the accuracies
%would be the same, instead of 68.45-68.78-68.55 / 70.56-70.82-70.22
%if we have a fully connected layer that learns from the combined data -
%how would the accuracies change?
%if we have added a clever selectively connected hidden layer how would it
%change the results?








