function [ cnnnet_initial ] = backwardLearning( cnnnetFileName, categoriesToLoad )
    %cnnnetFileName = 'D:\cnnnetStruct_5a9e18a_1_2_epochID(07)_accuracy(87.66).mat';
    %categoriesToLoad = [1 2];
    
%% 1.set the necessary parameters
    global bigDataPath;
    global cifar10Folder;
    displayOpts = setDisplayOpts('none');
    saveOpts = setSaveOpts('none');
    dataLoadFunction = @createInitialDataStruct_CIFAR;
    dataLoadParams = {cifar10Folder,categoriesToLoad};    
    dS = feval(dataLoadFunction, dataLoadParams{:});
    load(cnnnetFileName,'cnnnet');%loads cnnnet
    cnnnet_initial = cnnnet;
    layerCnt = size(cnnnet_initial,1);
    
    for l=1:layerCnt
        if isfield(cnnnet_initial{l,1},'fwdPropAllBatches')
            cnnnet_initial{l,1}.fwdPropAllBatches = true;
        end
    end
    
    saveOpts.MainFolder = [bigDataPath 'tempBackwardLearning' filesep];
    saveOpts_MainFolder_Backup = saveOpts.MainFolder;
    if ~exist(saveOpts_MainFolder_Backup,'dir')
        mkdir(saveOpts_MainFolder_Backup);
    else
        delete(saveOpts_MainFolder_Backup);
    end
    saveOpts.MainFolder = [saveOpts.MainFolder 'Train' filesep];
    initialSaveTheBatchesToActivatedDataFolder(dS, displayOpts, saveOpts);
    saveOpts.MainFolder = saveOpts_MainFolder_Backup;
    deletePrevLayerBatch = false;
    
    [cnnnet_initial, cnnnetResults_Init, ~, pred_labels_train_Init, data_LL_Init, labels_LL_Init] = deepNet_Test( dS, cnnnet_initial, displayOpts, saveOpts, 'train', '',1, deletePrevLayerBatch);
    
    layerID = 15;
    W_cell = cell(layerID,1);
    while layerID>1
        [caseID, layerIDIn, layerIDOut] = getCaseID(cnnnet, layerID);
        switch caseID
            case 42 %Soft<-Pool
                %[X_out, labels_out] = gatherLayerData(layerIDOut, dS.countBatch.train, [saveOpts.MainFolder 'Train\ActivatedData\']);
                [X_in, labels_in] = gatherLayerData(layerIDIn, dS.countBatch.train, [saveOpts.MainFolder 'Train' filesep 'ActivatedData' filesep]);
                [Welm, predictions, confMat_ELM, accuracy_ELM, rankDeficient] = getELM_Weights_SoftMax(X_in', labels_in, [], displayOpts);
                falseSamples = find(predictions~=labels_in);
                trueSamples = find(predictions==labels_in);
                falseSampleCnt = length(falseSamples);
                Helm = Welm'*X_in;
                Helm(:,trueSamples) = 0;
                for c = 1:2
                    %false samples of class c
                    slct = find(predictions~=labels_in & labels_in==c);
                    other_c = 1:2;
                    other_c(other_c==c) = [];
                    Helm(other_c,slct) = 0;%-2*abs(Helm(other_c,slct));
                    Helm(c,slct) = 3*max(abs(Helm(:,slct)));
                end
                X_out_expected = [X_in;Helm];
                W_cell{layerID,1} = [Welm;eye(2)];
                outputExpected = W_12'*X_out_expected;
                [predictionsExpected, ~, ~, accExpected, confMatExpected] = analyzeActivationsElm(X_out_expected', labels_LL_Init, W_12, true);
        end
    end
    
    %Conv(12)->Relu(13)->Pool(14) (Case 2)
    %I have added 2 new dimensions to the Pooling layer, which means 2 new
    %filters to previous convolution layer
    %upsample Helm to convert them into real 6 by 6 patches of 2 filters
    [Helm_12, learntParams ] = upsample( Helm, [6 6], [1 1 2 7972]);
    
    %Conv(10)->Relu(11)->Conv(12) (Case )    
    X_10 = gatherLayerData(10, 4, [saveOpts.MainFolder 'Train' filesep 'ActivatedData' filesep ]);
    pixelCnt = numel(Helm_12)/2;
    patchPerSample = pixelCnt/size(X_10,2);
    disp(['There are ' num2str(pixelCnt) ' pixels in Helm_12. This means there are ' num2str(pixelCnt) ' patches in X_10']);
    [map_singleInput,~,convMap2D] = mapDataForConvolution( cnnnet_initial{12,1}.filterSize(1)         ... patchSize, 
                                            ,cnnnet_initial{12,1}.stride(1)             ... stride, 
                                            ,cnnnet_initial{12,1}.inputSize_original(1) ... sizeImg, 
                                            ,size(X_10,2)                               ... countSamples,
                                            ,cnnnet_initial{12,1}.inputChannelSize      ... inputDataChannelSize, 
                                            ,true);  %tutorialMode
    X_10_mapped = X_10(ind2sub(size(X_10),convMap2D));
    convPixSize = prod(cnnnet_initial{12,1}.outputSize_original(1:2));
    W_10 = zeros(1000,2);
    for f = 1:2
        fr = (f-1)*convPixSize+1;
        to = f*convPixSize;
        expFiltRes = reshape(Helm_12(fr:to,:),1,[]);
        slct = find(predictions~=labels_LL_Init & labels_LL_Init==c);
        other_c = 1:2;
        other_c(other_c==c) = [];
        Helm(other_c,slct) = 0;%-2*abs(Helm(other_c,slct));
        Helm(c,slct) = 3*max(abs(Helm(:,slct)));
    end
end

function [caseID, layerIDIn, layerIDOut] = getCaseID(cnnnet, layerIDOut)
    layerCount = size(cnnnet,1);
    lns = repmat('0',1,layerCount);%layerNameStr
    for l = 1:layerCount
        switch cnnnet{l,1}.type
            case 'convolution'
                lns(l) = '1';
            case 'pooling'
                lns(l) = '2';
            case 'fullyconnected'
                lns(l) = '3';
            case 'softmax'
                lns(l) = '4';
            case 'activation'
                lns(l) = '5';
            otherwise
                error('You are suggesting a layer that is not present.Layers are - convolution, activation, pooling, softmax');
        end
    end
    
    i = layerIDOut;
    caseID = [];
    switch lns(i)
        case '2'%Pool
            if strcmp(lns(i-1),'5') && strcmp(lns(i-2),'1')
                %Pool<-Act<-Conv
                caseID = 251;
            end
        case '4'%Soft
            if strcmp(lns(i-1),'2')
                %Soft<-Pool
                caseID = 42;
                layerIDIn = layerIDOut-1;
                %layerIDOut = layerIDOut;
            elseif strcmp(lns(i-1),'5')
                if strcmp(lns(i-2),'1')
                    %Soft<-Act<-Conv
                    caseID = 451;
                elseif strcmp(lns(i-2),'3')
                    %Soft<-Act<-Full
                    caseID = 453;
                end
            end
        case '5'%Act
            if strcmp(lns(i-1),'1')
                if strcmp(lns(i-2),'2')
                    %Act<-Conv<-Pool
                    caseID = 512;
                elseif strcmp(lns(i-2),'5') && strcmp(lns(i-3),'1')
                    %Act<-Conv<-Act<-Conv                    
                    caseID = 5151;
                end
            elseif strcmp(lns(i-1),'3')
                if strcmp(lns(i-2),'5') && strcmp(lns(i-3),'3')
                    %Act<-Full<-Act<-Full                    
                    caseID = 5353;
                elseif strcmp(lns(i-2),'5') && strcmp(lns(i-3),'1')
                    %Act<-Full<-Act<-Conv                    
                    caseID = 5351;
                end
            end
    end
    if isempty(caseID)
        error('unknown case')
    end
end

