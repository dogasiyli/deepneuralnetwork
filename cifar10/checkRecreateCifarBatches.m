function [dS, recreated] = checkRecreateCifarBatches( categoriesToLoad, validBatchID, batch_Count_Vec, enforceRecreateBatches, labelResetMat)
    global cifar10Folder;
    recreated = false;
    
    %first regardless of what is in "enforceRecreateBatches" check batch_Count_Vec
    %with the existing files - if there is any
    if exist('enforceRecreateBatches','var') && enforceRecreateBatches==false
        try
            %this line goes through if there is a proper batch set in "cifar10Folder" directory
            dS = createBatches_CIFAR(categoriesToLoad,  cifar10Folder, cifar10Folder, 32, [], validBatchID, batch_Count_Vec);
            %if we are here it loaded the dataStruct as dS
            %now check if the batch_Count_Vec is same
            batch_Count_Vec_cur = [dS.countBatch.train dS.countBatch.valid dS.countBatch.test];
            enforceRecreateBatches = sum(abs(batch_Count_Vec_cur-batch_Count_Vec))~=0;
            if enforceRecreateBatches
                %this says that batch_Count_Vec is not the same as what is in "cifar10Folder" directory
                disp(['Will recreate batches because ' mat2str(batch_Count_Vec_cur) ' ~= ' mat2str(batch_Count_Vec)]);
            end
            if ~enforceRecreateBatches
                %the batchIDs fit, but what about categoriesToLoad
                enforceRecreateBatches = sum(sort(dS.categoryIDsToLoad)-sort(categoriesToLoad))~=0;
                if enforceRecreateBatches
                    %this says that dS.categoryIDsToLoad is not the same as what is in "categoriesToLoad"
                    disp(['Will recreate batches because ' mat2str(dS.categoryIDsToLoad) ' ~= ' mat2str(categoriesToLoad)]);
                end
            end
            if ~enforceRecreateBatches
                %check if all the files exist for train, valid and test
                for dataTypeStr = {'train' 'valid' 'test'}
                    batchCount = eval(['dS.countBatch.' dataTypeStr{1} ';']);
                    dS = dS.updateDataStruct(dS, {'type', dataTypeStr{1}});
                    for b=1:batchCount
                        dS = dS.updateDataStruct(dS, {'batchID_current', b});
                        if ~exist(dS.fileName_current,'file')
                            enforceRecreateBatches = true;
                        end
                    end
                end
                if enforceRecreateBatches
                    %this says that some files missing
                    disp('Will recreate batches because some files missing..');
                end
            end
            disp('No file missing, hence no recreation of bathces needed..');
        catch
            %there was a problem loading dS -> hence recreate the dS for sure
            enforceRecreateBatches = true;
            if enforceRecreateBatches
                disp('Will recreate batches because problem loadin dataStruct from file');
            end
        end
    else
        %recreation wanted no matter what
        enforceRecreateBatches = true;
    end

    if enforceRecreateBatches
        delete([cifar10Folder 'data_batch_train_*.mat']);
        delete([cifar10Folder 'data_batch_valid_*.mat']);
        delete([cifar10Folder 'data_batch_test_*.mat']);
        delete([cifar10Folder 'batchInfo_CIFAR10.mat']);
        [dS, batchInfo] = createBatches_CIFAR(categoriesToLoad,  cifar10Folder, cifar10Folder, 32, [], validBatchID, batch_Count_Vec);
        recreated = true;
    end

    if exist('labelResetMat','var') && ~isempty(labelResetMat) && size(labelResetMat,1)==2
        %labelResetMat(1,:) = originalLabels
        %labelResetMat(2,:) = newLabels
        disp(['Labels will be changed from ' mat2str(labelResetMat(1,:)) ' to ' mat2str(labelResetMat(2,:))]);
        for dataTypeStr = {'train' 'valid' 'test'}
            batchCount = eval(['dS.countBatch.' dataTypeStr{1} ';']);
            dS = dS.updateDataStruct(dS, {'type', dataTypeStr{1}});
            for b=1:batchCount
                dS = dS.updateDataStruct(dS, {'batchID_current', b});
                if exist(dS.fileName_current,'file')
                    load(dS.fileName_current,'data','labels');
                    assert(sum(labelResetMat(1,:)-unique(labels))==0,'these must be same');
                    labels= reSetLabels( labels, labelResetMat(1,:), labelResetMat(2,:) );
                    assert(sum(unique(labelResetMat(2,:))-unique(labels))==0,'these must be same');
                    save(dS.fileName_current,'data','labels');
                end
                disp([dataTypeStr{1} '-b(' num2str(b,'%02d') ') - done!']);
            end
        end
    end
end

