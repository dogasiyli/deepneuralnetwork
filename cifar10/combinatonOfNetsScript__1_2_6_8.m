runFirst;
global cifar10Folder;
displayOpts = setDisplayOpts('none');
saveOpts = setSaveOpts('none');

[ds_1_6, bi_1_6] = createBatches_CIFAR([1 6], cifar10Folder, cifar10Folder, 32, [], 5, [10 3 4]);

%% W08 / 1_6
load('D:\Experiments Backup\W08_cC(7ce118a_[1 6])-ds(CIFAR10)-exID(4)\NetworkStructure\cnnnetStruct_epochID(16)_accuracy(93.78).mat')

%W08, Data = 1-6
[cnnnet, cnnnetResults_Train, ~, pred_labels_train, data_1_6_W08_Tr, label_1_6_W08_Tr] = deepNet_Test( ds_1_6, cnnnet, displayOpts, saveOpts, 'train', '',16);
Welm_remainingFeatIDs_1_6_W08 = cnnnet{1, 1}.Welm_remainingFeatIDs;
Welm_removableFeats_1_6_W08 = cnnnet{1, 1}.Welm_removableFeats;
[~, crVa_W08_1_6, ~, ~, data_1_6_W08_Va, label_1_6_W08_Va] = deepNet_Test( ds_1_6, cnnnet, displayOpts, saveOpts, 'valid', '',16);
[~, crTe_W08_1_6, ~, ~, data_1_6_W08_Te, label_1_6_W08_Te] = deepNet_Test( ds_1_6, cnnnet, displayOpts, saveOpts, 'test', '',16);

%W08, Data = 2-8
[ds_2_8, bi_2_8] = createBatches_CIFAR([2 8], cifar10Folder, cifar10Folder, 32, [], 5, [10 3 4]);
[cnnnet, crTr_W08_2_8, ~, ~, data_2_8_W08_Tr, label_2_8_W08_Tr] = deepNet_Test( ds_2_8, cnnnet, displayOpts, saveOpts, 'train', '',17);
[~, crVa_W08_2_8, ~, ~, data_2_8_W08_Va, label_2_8_W08_Va] = deepNet_Test( ds_2_8, cnnnet, displayOpts, saveOpts, 'valid', '',17);
[~, crTe_W08_2_8, ~, ~, data_2_8_W08_Te, label_2_8_W08_Te] = deepNet_Test( ds_2_8, cnnnet, displayOpts, saveOpts, 'test', '',17);

%% W06 / 2_8
load('D:\Experiments Backup\W05_cC(after_2c5dddf_2_8)-ds(CIFAR10)-exID(3)\NetworkStructure\cnnnetStruct_epochID(09)_accuracy(93.90).mat')

%W08, Data = 2-8
[cnnnet, crTr_W05_2_8, ~, ~, data_2_8_W05_Tr, label_2_8_W05_Tr] = deepNet_Test( ds_2_8, cnnnet, displayOpts, saveOpts, 'train', '',21);
[~, crVa_W05_2_8, ~, ~, data_2_8_W05_Va, label_2_8_W05_Va] = deepNet_Test( ds_2_8, cnnnet, displayOpts, saveOpts, 'valid', '',21);
[~, crTe_W05_2_8, ~, ~, data_2_8_W05_Te, label_2_8_W05_Te] = deepNet_Test( ds_2_8, cnnnet, displayOpts, saveOpts, 'test', '',21);

%Data = 1-6
createBatches_CIFAR([1 6], cifar10Folder, cifar10Folder, 32, [], 5, [10 3 4]);

Welm_remainingFeatIDs_2_8_W05 = cnnnet{1, 1}.Welm_remainingFeatIDs;
Welm_removableFeats_2_8_W05 = cnnnet{1, 1}.Welm_removableFeats;
[cnnnet, crTr_W05_1_6, ~, ~, data_1_6_W05_Tr, label_1_6_W05_Tr] = deepNet_Test( ds_1_6, cnnnet, displayOpts, saveOpts, 'train', '',22);
[~, crVa_W05_1_6, ~, ~, data_1_6_W05_Va, label_1_6_W05_Va] = deepNet_Test( ds_1_6, cnnnet, displayOpts, saveOpts, 'valid', '',22);
[~, crTe_W05_1_6, ~, ~, data_1_6_W05_Te, label_1_6_W05_Te] = deepNet_Test( ds_1_6, cnnnet, displayOpts, saveOpts, 'test', '',22);


%% Label switch (1_6;(1-1)->1,(6-2)->3),(2_8;(2-1)->2,(8-2)->4)
%1_6;(6-2)->3
label_1_6_W05_Te(label_1_6_W05_Te==2) = 3;
label_1_6_W05_Tr(label_1_6_W05_Tr==2) = 3;
label_1_6_W05_Va(label_1_6_W05_Va==2) = 3;
label_1_6_W08_Te(label_1_6_W08_Te==2) = 3;
label_1_6_W08_Tr(label_1_6_W08_Tr==2) = 3;
label_1_6_W08_Va(label_1_6_W08_Va==2) = 3;
%2_8;(8-2)->4
label_2_8_W05_Te(label_2_8_W05_Te==2) = 4;
label_2_8_W05_Tr(label_2_8_W05_Tr==2) = 4;
label_2_8_W05_Va(label_2_8_W05_Va==2) = 4;
label_2_8_W08_Te(label_2_8_W08_Te==2) = 4;
label_2_8_W08_Tr(label_2_8_W08_Tr==2) = 4;
label_2_8_W08_Va(label_2_8_W08_Va==2) = 4;
%2_8;(1-2)->2
label_2_8_W05_Te(label_2_8_W05_Te==1) = 2;
label_2_8_W05_Tr(label_2_8_W05_Tr==1) = 2;
label_2_8_W05_Va(label_2_8_W05_Va==1) = 2;
label_2_8_W08_Te(label_2_8_W08_Te==1) = 2;
label_2_8_W08_Tr(label_2_8_W08_Tr==1) = 2;
label_2_8_W08_Va(label_2_8_W08_Va==1) = 2;

%% combine transformed data
data_tr_W5_W8 = [data_1_6_W08_Tr data_2_8_W08_Tr;data_1_6_W05_Tr data_2_8_W05_Tr];
data_va_W5_W8 = [data_1_6_W08_Va data_2_8_W08_Va;data_1_6_W05_Va data_2_8_W05_Va];
data_te_W5_W8 = [data_1_6_W08_Te data_2_8_W08_Te;data_1_6_W05_Te data_2_8_W05_Te];
labels_tr = [label_1_6_W05_Tr;label_2_8_W08_Tr];
labels_va = [label_1_6_W05_Va;label_2_8_W08_Va];
labels_te = [label_1_6_W05_Te;label_2_8_W08_Te];

[Welm, ~, ~, ~, rankDeficient_Comb] = getELM_Weights_SoftMax(data_tr_W5_W8', labels_tr, [], setDisplayOpts('all'));
[~, ~, ~, accuracy_Comb_tr, confMat_Comb_tr] = analyzeActivationsElm(data_tr_W5_W8', labels_tr, Welm, true);
[~, ~, ~, accuracy_Comb_va, confMat_Comb_va] = analyzeActivationsElm(data_va_W5_W8', labels_va, Welm, true);
[~, ~, ~, accuracy_Comb_te, confMat_Comb_te] = analyzeActivationsElm(data_te_W5_W8', labels_te, Welm, true);

%Pick only transformed of W_08 (dog vs airplane)
data_tr_W_08 = data_tr_W5_W8(1:50,:);
data_va_W_08 = data_va_W5_W8(1:50,:);
data_te_W_08 = data_te_W5_W8(1:50,:);
[Welm, ~, ~, ~, rankDeficient_W08] = getELM_Weights_SoftMax(data_tr_W_08', labels_tr, [], setDisplayOpts('all'));
[~, ~, ~, accuracy_W08_tr, confMat_W08_tr] = analyzeActivationsElm(data_tr_W_08', labels_tr, Welm, true);
[~, ~, ~, accuracy_W08_va, confMat_W08_va] = analyzeActivationsElm(data_va_W_08', labels_va, Welm, true);
[~, ~, ~, accuracy_W08_te, confMat_W08_te] = analyzeActivationsElm(data_te_W_08', labels_te, Welm, true);

%Pick only transformed of W_05 (horse vs automobile)
data_tr_W_05 = data_tr_W5_W8(51:100,:);
data_te_W_05 = data_te_W5_W8(51:100,:);
data_va_W_05 = data_va_W5_W8(51:100,:);
[Welm, ~, ~, ~, rankDeficient_W05] = getELM_Weights_SoftMax(data_tr_W_05', labels_tr, [], setDisplayOpts('all'));
[~, ~, ~, accuracy_W05_tr, confMat_W05_tr] = analyzeActivationsElm(data_tr_W_05', labels_tr, Welm, true);
[~, ~, ~, accuracy_W05_va, confMat_W05_va] = analyzeActivationsElm(data_va_W_05', labels_va, Welm, true);
[~, ~, ~, accuracy_W05_te, confMat_W05_te] = analyzeActivationsElm(data_te_W_05', labels_te, Welm, true);

%% Phase 2 - try adding the third claasifier network 1_2 into the combine data_tr

[ds_1_6, bi_1_6] = createBatches_CIFAR([1 6], cifar10Folder, cifar10Folder, 32, [], 5, [10 3 4]);

%% W08 / 1_6
load('D:\Experiments Backup\S12_cC(after_a2134f8_1_2)-ds(CIFAR10)-exID(2)\NetworkStructure\cnnnetStruct_epochID(20)_accuracy(90.07).mat')
Welm_remainingFeatIDs_S12 = cnnnet{1, 1}.Welm_remainingFeatIDs;
Welm_removableFeats_S12 = cnnnet{1, 1}.Welm_removableFeats;

%S12, Data = 1-2
[cnnnet_S12, cnnnetResults_S12, ~, ~, data_1_6_S12_Tr, label_1_6_S12_Tr] = deepNet_Test( ds_1_6, cnnnet, displayOpts, saveOpts, 'train', '',1216);
[~, crVa_S12_1_6, ~, ~, data_1_6_S12_Va, label_1_6_S12_Va] = deepNet_Test( ds_1_6, cnnnet_S12, displayOpts, saveOpts, 'valid', '',1216);
[~, crTe_S12_1_6, ~, ~, data_1_6_S12_Te, label_1_6_S12_Te] = deepNet_Test( ds_1_6, cnnnet_S12, displayOpts, saveOpts, 'test', '',1216);

%W08, Data = 2-8
[ds_2_8, bi_2_8] = createBatches_CIFAR([2 8], cifar10Folder, cifar10Folder, 32, [], 5, [10 3 4]);
[cnnnet_S12, crTr_S12_2_8, ~, ~, data_2_8_S12_Tr, label_2_8_S12_Tr] = deepNet_Test( ds_2_8, cnnnet, displayOpts, saveOpts, 'train', '',1228);
[~, crVa_S12_2_8, ~, ~, data_2_8_S12_Va, label_2_8_S12_Va] = deepNet_Test( ds_2_8, cnnnet_S12, displayOpts, saveOpts, 'valid', '',1228);
[~, crTe_S12_2_8, ~, ~, data_2_8_S12_Te, label_2_8_S12_Te] = deepNet_Test( ds_2_8, cnnnet_S12, displayOpts, saveOpts, 'test', '',1228);

data_tr_W5_W8_S12 = [data_1_6_S12_Tr data_2_8_S12_Tr;data_tr_W5_W8];
data_va_W5_W8_S12 = [data_1_6_S12_Va data_2_8_S12_Va;data_va_W5_W8];
data_te_W5_W8_S12 = [data_1_6_S12_Te data_2_8_S12_Te;data_te_W5_W8];

[Welm_W5_W8_S12, ~, ~, ~, rankDeficient_W5_W8_S12] = getELM_Weights_SoftMax(data_tr_W5_W8_S12', labels_tr, [], setDisplayOpts('all'));
[~, ~, ~, accuracy_W5_W8_S12_tr, confMat_W5_W8_S12_tr] = analyzeActivationsElm(data_tr_W5_W8_S12', labels_tr, Welm_W5_W8_S12, true);
[~, ~, ~, accuracy_W5_W8_S12_va, confMat_W5_W8_S12_va] = analyzeActivationsElm(data_va_W5_W8_S12', labels_va, Welm_W5_W8_S12, true);
[~, ~, ~, accuracy_W5_W8_S12_te, confMat_W5_W8_S12_te] = analyzeActivationsElm(data_te_W5_W8_S12', labels_te, Welm_W5_W8_S12, true);

%apply mean and variance normalization to all dimensions of data_tr_W5_W8_S12
var_W5_W8_S12 = var(data_tr_W5_W8_S12');
mean_W5_W8_S12 = mean(data_tr_W5_W8_S12');

data_tr_W5_W8_S12_mv = bsxfun(@minus, data_tr_W5_W8_S12, mean_W5_W8_S12');
mean_W5_W8_S12_mv = mean(data_tr_W5_W8_S12_mv');
data_tr_W5_W8_S12_v = bsxfun(@rdivide, data_tr_W5_W8_S12_mv, sqrt(var_W5_W8_S12)');
var_W5_W8_S12_v = var(data_tr_W5_W8_S12_v');

data_va_W5_W8_S12_mv = bsxfun(@minus, data_va_W5_W8_S12, mean_W5_W8_S12');
data_va_W5_W8_S12_mv = bsxfun(@rdivide, data_va_W5_W8_S12_mv, sqrt(var_W5_W8_S12)');
mean_W5_W8_S12_mv_va = mean(data_va_W5_W8_S12_mv');
var_W5_W8_S12_mv_va = var(data_va_W5_W8_S12_mv');

data_te_W5_W8_S12_mv = bsxfun(@minus, data_te_W5_W8_S12, mean_W5_W8_S12');
data_te_W5_W8_S12_mv = bsxfun(@rdivide, data_te_W5_W8_S12_mv, sqrt(var_W5_W8_S12)');
mean_W5_W8_S12_mv_te = mean(data_te_W5_W8_S12_mv');
var_W5_W8_S12_mv_te = var(data_te_W5_W8_S12_mv');

remainingFeatIDs = 1:150;
irrelevFeats_cur = isnan(var_W5_W8_S12_v);
remainingFeatIDs(irrelevFeats_cur) = [];

[Welm_W5_W8_S12_mv, ~, ~, ~, rankDeficient_W5_W8_S12_mv] = getELM_Weights_SoftMax(data_tr_W5_W8_S12_v(remainingFeatIDs,:)', labels_tr, [], setDisplayOpts('all'));
[~, ~, ~, accuracy_W5_W8_S12_tr_mv, confMat_W5_W8_S12_tr_mv] = analyzeActivationsElm(data_tr_W5_W8_S12_v(remainingFeatIDs,:)', labels_tr, Welm_W5_W8_S12_mv, true);
[~, ~, ~, accuracy_W5_W8_S12_va_mv, confMat_W5_W8_S12_va_mv] = analyzeActivationsElm(data_va_W5_W8_S12_mv(remainingFeatIDs,:)', labels_va, Welm_W5_W8_S12_mv, true);
[~, ~, ~, accuracy_W5_W8_S12_te_mv, confMat_W5_W8_S12_te_mv] = analyzeActivationsElm(data_te_W5_W8_S12_mv(remainingFeatIDs,:)', labels_te, Welm_W5_W8_S12_mv, true);


