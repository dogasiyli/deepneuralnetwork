%% 1. clear all and then create the dataset from scracth to have 4 classes
clearvars;runFirst;
clc; %#ok<CLSCR>
global fastDiskTempPath;
displayOpts_None = setDisplayOpts('none');
displayOpts_All = setDisplayOpts('all');
smf_str = 'CombineNets_L16_S12';
saveMainFolder = [fastDiskTempPath smf_str filesep];
if exist(saveMainFolder,'dir')
    rmdir(saveMainFolder,'s');
end
mkdir(saveMainFolder);
saveOpts_None = setSaveOpts('none', saveMainFolder);


%% 2. assign the network names and places
global dropboxPath;
netFolder = [dropboxPath 'A_slctNetStruct' filesep];
netfn_List = { 'L16_cnnStr_eID20_acc73.92.mat';'S12_cnnStr_eID20_acc90.07.mat';...'L12_cnnStr_eID19_acc85.75.mat';'L13_cnnStr_eID03_acc80.10.mat';...'D02_cnnStr_eID35_acc96.81.mat';...
               ...'L17_cnnStr_eID09_acc93.46.mat';...
               ...'S21_cnnStr_eID10_acc92.97.mat';...
               ...'L18_cnnStr_eID08_acc94.81.mat';...
               ...'S20_cnnStr_eID10_acc94.76.mat';...
               ...'S24_cnnStr_eID14_acc87.98.mat'
              };
classes_List = [   1 2;1 2;...94.1
                   ...1 6;...91.4
                   ...1 8;...91.9
                   ...2 6;...94.2
                   ...2 8;...93.8
                   ...6 8 ...84.25
                   ];
netCnt = length(netfn_List);

%% 3. manually create the last layer with additional information about the final layers
allC = sort(unique(classes_List(:)));
classCnt = length(allC);
allC_map = 1:classCnt;
dS = checkRecreateCifarBatches(allC, 5, [5*classCnt 3*classCnt 2*classCnt], false);


diary([saveMainFolder smf_str '_mos.txt']);

cell_d = cell(netCnt,3);
cell_l = cell(netCnt,3);
cell_W = cell(netCnt,1);
cell_r = cell(netCnt,1);

nodeCnts = zeros(1,netCnt);
for n=1:netCnt
    netID = netfn_List{n,1}(1:3);
    disp(['***********net(' netID ')_' mat2str(classes_List(n,:)) '***************']);
    netFileName = [netFolder netfn_List{n,1}];
    netC = classes_List(n,:);
    testInitFinalStr = ['net(' num2str(n) ')-' netID '-C_{o' mat2str(netC) '}_{g' mat2str(allC) '}'];
    epochID = n*10+1;
    [d_tr, l_tr, d_va, l_va, d_te, l_te, Wcur, resCur] = runNetOnMoreData(dS, netFileName, allC, netC, testInitFinalStr, epochID, saveMainFolder);
    cell_d{n,1} = d_tr;
    cell_l{n,1} = l_tr;
    cell_d{n,2} = d_va;
    cell_l{n,2} = l_va;
    cell_d{n,3} = d_te;
    cell_l{n,3} = l_te;
    cell_W{n,1} = Wcur; 
    cell_r{n,1} = resCur;
    nodeCnts(n) = length(cell_W{n,1});
    
    dispVariableSize(cell_d, 'cell_d', true);
    dispVariableSize(cell_l, 'cell_l', true);
    disp(['+++finished net(' netID ')-classes(' mat2str(netC) ')-nodeCnt(' num2str(nodeCnts(n)) ')']);
end

toNode = 0;
dataAll = cell(3,1);
labelsAll = cell(3,1);
netNodes = zeros(sum(nodeCnts),classCnt);
for i=1:3
    dataAll{i,1} = [];
    labelsAll{i,1} = [];
    for n=1:netCnt
        netC = classes_List(n,:);
        dataAll{i,1} = [dataAll{i,1};cell_d{n,i}];
        if n~=1
            assert(sum(labelsAll{i,1}-cell_l{n,i})==0,'must equal to zero');
        else
            labelsAll{i,1} = cell_l{n,i};
        end
        if i==1
            netID = netfn_List{n,1}(1:3);
            %node arrangements
            nodeCnt = nodeCnts(n);
            frNode = toNode + 1;
            toNode = frNode + nodeCnt - 1;
            disp(['+++combining net(' netID ')-classes(' mat2str(netC) '-nodes(' mat2str([frNode toNode]) ')']);
            nodeCols = find(ismember(allC,netC));
            netNodes(frNode:toNode,nodeCols) = 1;
        end
    end
end

[W_sl_All, ~, ~, ~, rd_All] = getELM_Weights_SoftMax(dataAll{1,1}', labelsAll{1,1}, [], displayOpts_All);
[~, ~, ~, acc_sl_ALL_tr, cM_sl_All_tr] = analyzeActivationsElm(dataAll{1,1}', labelsAll{1,1}, W_sl_All, true);
[~, ~, ~, acc_sl_ALL_va, cM_sl_All_va] = analyzeActivationsElm(dataAll{2,1}', labelsAll{2,1}, W_sl_All, true);
[~, ~, ~, acc_sl_ALL_te, cM_sl_All_te] = analyzeActivationsElm(dataAll{3,1}', labelsAll{3,1}, W_sl_All, true);
disp('Test conf : ')
displayConfusionMatrix(cM_sl_All_te, allC);

% labels_N1_vs_N2_tr = labelsAll{1,1};
% labels_N1_vs_N2_tr(ismember(labels_N1_vs_N2_tr,[1 2])) = 1;
% labels_N1_vs_N2_tr(ismember(labels_N1_vs_N2_tr,[3 4])) = 2;
% [W_N1_vs_N2, ~, cm_N1_vs_N2, ~, rd_N1_vs_N2] = getELM_Weights_SoftMax(dataAll{1,1}', labels_N1_vs_N2_tr, [], displayOpts_All);


diary('off');



