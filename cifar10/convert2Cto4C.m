function cnnnet = convert2Cto4C(cnnnet, allC, netC)
    allC_cnt = length(unique(allC));
    netC_cnt = length(unique(netC));
    finalLayerCnt = cnnnet{end, 1}.countFeat;

    assert(netC_cnt==cnnnet{end, 1}.countCategory,'class count must be same');
    cnnnet{end, 1}.countCategory = allC_cnt;

    W = cnnnet{end, 1}.Weight;
    b = cnnnet{end, 1}.BiasParams;

    W_new = zeros(allC_cnt,finalLayerCnt);
    b_new = zeros(allC_cnt,1);

    slctClasses = false(1,allC_cnt);
    for i=1:allC_cnt
        cID = allC(i);
        slctClasses(i) = ~isempty(find(cID==netC,1));
    end
    W_new(slctClasses,:) = W;
    b_new(slctClasses,:) = b;

    cnnnet{end, 1}.Weight = W_new;
    cnnnet{end, 1}.BiasParams = b_new;
    cnnnet{end, 1}.countCategory = allC_cnt;
end

