function [initialDataStruct, batchInfo] = createBatches_CIFAR(categoriesToLoad, readFolder, writeFolder, imageRowColSize, bathSizeMaxMB, validBatchID, batch_Count_Vec)

    %categoriesToLoad should have numbers [1 to 10]
    categoryCountOriginal = 10;
    categoriesToLoad = sort(categoriesToLoad);
    categoryCount = length(categoriesToLoad);

    if (~exist('imageRowColSize','var') || isempty(imageRowColSize))
        imageRowColSize = 32;
    end
    if (~exist('batch_Count_Vec','var') || isempty(batch_Count_Vec))
        batch_Count_Vec = [10 5 4];
    end
    
    if (~strcmp(readFolder(end),filesep))
        readFolder = [readFolder filesep];
    end
    assert(exist(readFolder,'dir')>0,['The readFolder(' readFolder ') doesnt exist']);

    if (~strcmp(writeFolder(end),filesep))
        writeFolder = [writeFolder filesep];
    end
    if (~exist(writeFolder,'dir'))
        mkdir(writeFolder)
    end
    
    global bigDataPath;
    batchInfoSaveFileName = [bigDataPath 'CIFAR10' filesep 'batchInfo_CIFAR10.mat'];
    if exist(batchInfoSaveFileName,'file')
        load(batchInfoSaveFileName,'batchInfo');
    else
        batchInfo = seperateAndCreateBatchInfoCifar10(categoriesToLoad, readFolder, imageRowColSize, bathSizeMaxMB, batch_Count_Vec, validBatchID);
        save(batchInfoSaveFileName,'batchInfo');
    end
    
    samplesPicked = cell(6,1);
    anySkipped = false;
    for bt = 1:3 %Batch Type - Train(1), Validation(2), Test(3)
        [btStr, batch_Count] = getVals(bt, batchInfo);
        for b=1:batch_Count
            fileNameBatch = [writeFolder 'data_batch_' btStr '_' num2str(b,'%02d') '-' num2str(batch_Count,'%02d') '.mat'];
            if exist(fileNameBatch,'file')
                load(fileNameBatch,'data','labels');
                Nf = size(data,4);
                if bt==1
                    Nb = sum(batchInfo.image_Count_Per_Batch_Train(b,:));                    
                elseif bt==2
                    Nb = sum(batchInfo.image_Count_Per_Batch_Valid(b,:));
                elseif bt==3
                    Nb = sum(batchInfo.image_Count_Per_Batch_Test(b,:));
                end
                assert(Nf==Nb,'These must be same');
                disp(['***Data for (' fileNameBatch ') is being skipped']);
                anySkipped = true;
                continue;
            end
            batch_image_count = getBIC(bt, batchInfo, b);
            disp(['**Initializing data for (' fileNameBatch ')']);
            data = rand(imageRowColSize,imageRowColSize,3,batch_image_count);
            labels = zeros(1,batch_image_count);
            
            [data, labels, samplesPicked] = fillBatchWithData(categoriesToLoad, samplesPicked, data, labels, b, bt, batchInfo, readFolder);
            data = data./255;

            disp(['Data for (' fileNameBatch ') is being saved']);
            a = whos('data');
            uncompressedSize = a.bytes/(1024*1024);
            save(fileNameBatch,'data','labels','uncompressedSize','-v7.3');
            a = dir(fileNameBatch);
            compressedSize = a.bytes/(1024*1024);
            disp(['Data of size ' num2str(uncompressedSize, '%6.3f') 'MB is compressed into ' num2str(compressedSize, '%6.3f') 'MB']);
            disp(['**Data for (' fileNameBatch ') is saved']);
            clear a compressedSize uncompressedSize
        end %b=1:batch_Count
    end
    
    %check if everything went right
    if ~anySkipped
        for matID = 1:6
            batchFileName = ['data_batch_' num2str(matID)];
            load([readFolder batchFileName '.mat'],'labels')
            labels = labels+1;
            for i = 1:categoryCountOriginal
                if sum(categoriesToLoad==i)==0
                    labels(labels==i) = 0;
                else
                    c = find(categoriesToLoad==i);
                    labels(labels==i) = c;
                end
            end
            assert(sum(samplesPicked{matID,1}-double(labels))==0,'');
        end
    end
    initialDataStruct = createInitialDataStruct_CIFAR(readFolder, categoriesToLoad);
end

function [dataFilled, labelsFilled, samplesPicked] = fillBatchWithData(categoriesToLoad, samplesPicked, dataFilled, labelsFilled, batchIDToFill, bt, batchInfo, mainFolder)
    % trainData can be in 3 different forms
    % 2 dimensional - [d numberOfSamples] = size(trainData)
    % 3 dimensional - [imageDim imageDim numberOfSamples] = size(trainData);
    % 4 dimensional - [imageDim imageDim channelSize numberOfSamples] = size(trainData)

    %we need to create the batch file which consists of
    %trainData - 
    %trainLabels - 
    %additional information can also be added like
    %each samples information from dataInfo which is N by 10

    categoryCount = length(categoriesToLoad);
    
    to = 0;
    for i = 1:categoryCount
        c = categoriesToLoad(i);
        if (bt==1)%train
            imageCountCategoryBatchAll = batchInfo.image_Count_Per_Batch_Train(batchIDToFill,i);             
        elseif (bt==2)%valid
            imageCountCategoryBatchAll = batchInfo.image_Count_Per_Batch_Valid(batchIDToFill,i);               
        else%test
            imageCountCategoryBatchAll = batchInfo.image_Count_Per_Batch_Test(batchIDToFill,i); 
        end
        disp(['BatchToFill-' num2str(batchIDToFill) ',Category-' num2str(i) ', image count = ' num2str(imageCountCategoryBatchAll)]);
        
        for matID = 1:6
            fr = to+1;
            %the samples that fall into the Batch Type and Batch ID
            slct = getSelections(batchInfo.batchDistributions, bt, matID, i, batchIDToFill);

            %images_RGBD is [imageRowColSize imageRowColSize 4 imageCountCur]
            slctImageCount = sum(slct==1);
            
            batchFileName = ['data_batch_' num2str(matID)];
            load([mainFolder batchFileName '.mat'],'data','labels')
            
            labels = labels+1;
            idx = find(labels==c);
            
            if isempty(samplesPicked{matID,1})
                samplesPicked{matID,1} = zeros(size(labels));
            end
            
            idxSlct = idx(slct);
            %check if it was used before
            samplesPickedCurMat = samplesPicked{matID,1};
            assert(0==sum(samplesPickedCurMat(idxSlct)),'The samples were picked before')
            samplesPickedCurMat(idxSlct) = i;
            samplesPicked{matID,1} = samplesPickedCurMat;
            
            
            labelsAdd = labels(idxSlct);
            %check if all matches c
            assert(sum(labelsAdd==c)==slctImageCount,'these 2 must be same');
            dataAdd = data(idxSlct,:); %#ok<NODEF>
            
            to = fr+slctImageCount-1;
            
            dataFilled(:,:,:,fr:to) = reshape(dataAdd',32,32,3,slctImageCount);
            labelsFilled(fr:to) = i*ones(1,slctImageCount);%reshape(labelsAdd,1,[]);
        end
    end
end

function [btStr, batch_Count] = getVals(bt,batchInfo)
    switch bt
        case 1
            btStr = 'train';
            batch_Count = batchInfo.batch_Count_Train;
        case 2
            btStr = 'valid';
            batch_Count = batchInfo.batch_Count_Valid;
        case 3
            btStr = 'test';
            batch_Count = batchInfo.batch_Count_Test;
    end
end

function [batch_image_count] = getBIC(bt,batchInfo,b)
    switch bt
        case 1
            batch_image_count = sum(batchInfo.image_Count_Per_Batch_Train(b,:));
        case 2
            batch_image_count = sum(batchInfo.image_Count_Per_Batch_Valid(b,:));
        case 3
            batch_image_count = sum(batchInfo.image_Count_Per_Batch_Test(b,:));
    end
end

function [slct] = getSelections(batchDistributions, bt, matID, i, batchIDToFill)
    bid = batchDistributions(matID,i).bid;
    switch bt
        case 1
            slct = batchDistributions(matID,i).TR==1 & bid==batchIDToFill;
        case 2
            slct = batchDistributions(matID,i).VA==1 & bid==batchIDToFill;
        case 3
            slct = batchDistributions(matID,i).TE==1 & bid==batchIDToFill;
    end  
end
