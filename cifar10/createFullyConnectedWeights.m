function [opt_cnnnet] = createFullyConnectedWeights(dS_LastLayer)
%     batchCount = dS_LastLayer.countBatch.train;
%     elmStruct_dropTrue = struct;
%     to = 0;
%     b=0;
%     falseSamplesRemain = 1;
%     dS_LastLayer = dS_LastLayer.updateDataStruct( dS_LastLayer, {'type', 'train', 'data_act', 'data', 'batchID_current', 1});
%     load(dS_LastLayer.fileName_current,'data','labels');
%     data_LL_train = data;clear data;
%     labels_LL_train = labels;clear labels;
%     
%     dataAll = data_LL_train;
%     labsAll = labels_LL_train;
%     categoryCount = length(unique(labels_LL_train));
%     numOfSamples = length(labels_LL_train);
%     confMat_accumulated = zeros(categoryCount);
%     
%     predAll = zeros(batchCount,numOfSamples);
%     diagSlct = eye(categoryCount)==1;
%     while b<=batchCount && falseSamplesRemain>0
%         b = b+1;
%         fr = to+1;
%         to = fr+categoryCount-1;
%         
%         [Welm_b, pred_b, confMat_b, accuracy_b, rankDeficient_b] = getELM_Weights_SoftMax(dataAll', labsAll);
% %         if b>1
% %             Welm_b = -Welm_b;%[elmStruct_dropTrue(1).Welm_b(:,2) elmStruct_dropTrue(1).Welm_b(:,1)];
% %             [pred_b, ~, ~, accuracy_b, confMat_b] = analyzeActivationsElm(dataAll', labsAll, Welm_b);
% %         end
%         
%         falseSamplesSlct = find(pred_b~=labsAll);
%         confMat_b_true = confMat_b;
%         confMat_b_true(diagSlct==0) = 0;
%         confMat_accumulated = confMat_accumulated + confMat_b_true;
%         %display - x samples removed because they are correctly classified
%         %assuming that all remainin classified wrong, upto now confMat is =
%         disp([num2str(sum(confMat_b_true(:))) ' samples truly classified.Current ELM : ']);
%         disp(confMat_b_true);
%         disp(['Upto Now(b=' num2str(b) ') : '])        
%         disp(confMat_accumulated);
%         
%         elmStruct_dropTrue(b).Welm_b = Welm_b;
%         elmStruct_dropTrue(b).confMat_b = confMat_b;
%         elmStruct_dropTrue(b).accuracy_b = accuracy_b;
%         elmStruct_dropTrue(b).rankDeficient_b = rankDeficient_b;
%         falseSamplesRemain = length(falseSamplesSlct);
%         
%         
%         [pred_b, ~, ~, accuracy_all, confMat_all] = analyzeActivationsElm(data_LL_train', labels_LL_train, Welm_b);
%         predAll(b,:) = pred_b;
% 
%         disp(['ConfMat(b=' num2str(b) ') : '])        
%         disp([num2str(sum(confMat_all(diagSlct))) ' samples truly classified, Acc' num2str(accuracy_all,'%4.2f')]);
%         disp(confMat_all);
%         
%         elmStruct_dropTrue(b).accuracy_all = accuracy_all;
%         
%         
%         dataAll = dataAll(:,falseSamplesSlct);
%         labsAll = labsAll(falseSamplesSlct);
%     end 
%     numOfELMLearnt = b;
%     predAll = predAll(1:b,:);  
    
    displayOpts = setDisplayOpts('none');
    saveOpts = setSaveOpts('none');
    sizeHid = 80;
    cnnnet_new = appendNetworkLayer( [], 'fullyconnected', displayOpts, 'activationType', 'sigm', 'sizeHid', sizeHid, 'initializeWeightMethod', 'ELM');
    cnnnet_new = appendNetworkLayer( cnnnet_new, 'activation', displayOpts);
    cnnnet_new = appendNetworkLayer( cnnnet_new, 'fullyconnected', displayOpts, 'activationType', 'sigm', 'sizeHid', sizeHid, 'initializeWeightMethod', 'ELM');
    cnnnet_new = appendNetworkLayer( cnnnet_new, 'activation', displayOpts);
    for i=1:4
        cnnnet_new{i,1}.fwdPropAllBatches = false;
    end
    cnnnet_new = appendNetworkLayer( cnnnet_new, 'softmax', displayOpts, 'initializeWeightMethod', 'Random', 'minFuncMaxIter', 0 ,'useAccuracyAsCost', false);
    disp('*****************************************INITIAL*TRAINING***************************************')
        softMaxOptimizeMaxIteration = 0;
        vararginParams = {'displayOpts', displayOpts,'saveOpts', saveOpts,'softMaxOptimizeMaxIteration',softMaxOptimizeMaxIteration};
        [cnnnetInitial, cnnnetResults_InitialTrain, ~, cnnnetResults_InitialValid, cnnnetResults_InitialTest, pred_labels_train_init] = deepNet_InitialTrain( dS_LastLayer, cnnnet_new, vararginParams{:});
        clear vararginParams;
    disp('*************************************BACKPROPAGATION*TRAINING***********************************')
        paramsMinFunc = setParams_minFunc();
        dS_LastLayer = dS_LastLayer.updateDataStruct( dS_LastLayer, {'type', 'train', 'data_act', 'data', 'batchID_current', 1});
        batchParams = struct;
        batchParams.maxIter = 50;
        batchParams.epochIter = 40;
        dataShuffleMode = 'none';
        vararginParams = {'dataStruct' , dS_LastLayer,'displayOpts', displayOpts,'saveOpts', saveOpts,'paramsMinFunc', paramsMinFunc,'useAccuracyAsCost',false,'batchParams', batchParams, 'dataShuffleMode', dataShuffleMode};
        [ opt_cnnnet, deepNetTrainBPOutput] = deepNetTrainBP( cnnnetInitial,vararginParams{:}); 
end
