function displayCIFAR10( loadFolderName, batchID, dispMode )
    %loadFolderName = 'G:\Data\cifar-10-batches-mat\';
    
    label_names = [];
    load([loadFolderName 'batches.meta.mat']);
    data = [];
    labels = [];
    batchFileName = ['data_batch_' num2str(batchID)];
    load([loadFolderName batchFileName '.mat'])
    labels = labels + 1;

    figure(1);hold on;clf;
    if strcmp(dispMode,'single')
        displayImgByImg(data, labels, label_names);
    elseif strcmp(dispMode,'singleCategorySorted')
        [labels, idx] = sort(labels);
        data = data(idx,:);
        displayImgByImg(data, labels, label_names);
    elseif strcmp(dispMode,'categoryPerImage')
        
    end
end

function displayImgByImg(data, labels, label_names)
    imgCnt = size(data,1); 
    uniqLabelIDs = unique(labels);
    curLabelCounter = zeros(size(uniqLabelIDs));
    for i=1:imgCnt
        curLabelID = labels(i);
        curLabelStr = label_names{curLabelID,1};
        curLabelCounter(curLabelID) = curLabelCounter(curLabelID) + 1;
        curImg = reshape(data(i,:),[32 32 3]);
        imshow(curImg);
        title([curLabelStr '-' num2str(curLabelCounter(curLabelID)) '-' num2str(i)]);
        drawnow;
    end
end