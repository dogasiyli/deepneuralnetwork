function [X_in, labels_in] = gatherLayerData(layerID, batchCount, loadFolderName)
    X_in = [];
    labels_in = [];
    for b = 1:batchCount
        fileName = [loadFolderName 'layer(' num2str(layerID,'%02d') ')_batch(' num2str(b,'%02d') ' of ' num2str(batchCount,'%02d') ').mat'];
        load(fileName,'X','labels');
        X_in = [X_in,X];
        if nargout>1
            labels_in = [labels_in,labels];
        end
    end
end

