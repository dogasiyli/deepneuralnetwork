function [dS_New, batch_Count_Vec_new] = grabFalseSamplesReOrganize(initialCNNNETFile, categoriesToLoad, batch_Count_Vec, trainFinalTidyType, enforceRecreateBatches)
    %cnnnetFileName = 'D:\cnnnetStruct_5a9e18a_1_2_epochID(07)_accuracy(87.66).mat';
    %categoriesToLoad = [1 2];
    %batch_Count_Vec = [12 4 5];
    
%% 1.set the necessary parameters
    global bigDataPath;
    displayOpts = setDisplayOpts('none');
    saveOpts = setSaveOpts('none');
    
    validBatchID = 5;
    [dS, recreated] = checkRecreateCifarBatches( categoriesToLoad, validBatchID, batch_Count_Vec, enforceRecreateBatches);
    %dataLoadFunction = @createInitialDataStruct_CIFAR;
    %dataLoadParams = {cifar10Folder,categoriesToLoad};
    %dS = feval(dataLoadFunction, dataLoadParams{:});
    
    [cnnnetInitial, initFileMode] = loadCnnetFromFile(initialCNNNETFile);
    layerCnt = size(cnnnetInitial,1);
    
    for l=1:layerCnt
        if isfield(cnnnetInitial{l,1},'fwdPropAllBatches')
            cnnnetInitial{l,1}.fwdPropAllBatches = false;
        end
    end
    
    saveOpts.MainFolder = [bigDataPath 'tempBackwardLearning' filesep];
    saveOpts_MainFolder_Backup = saveOpts.MainFolder;
    if ~exist(saveOpts_MainFolder_Backup,'dir')
        mkdir(saveOpts_MainFolder_Backup);
    else
        delete(saveOpts_MainFolder_Backup);
    end
    
    finalGroupType = 'moreTrueSamples';
    
    [dS_New, cnnnet_new]= reorganizeByType(dS, cnnnetInitial, displayOpts, saveOpts, 'train', finalGroupType, trainFinalTidyType);
    dS_New = reorganizeByType(dS_New, cnnnet_new, displayOpts, saveOpts, 'valid', 'all', 'none');
    dS_New = reorganizeByType(dS_New, cnnnet_new, displayOpts, saveOpts, 'test', 'all', 'none');
    
    batch_Count_Vec_new = [dS_New.countBatch.train dS_New.countBatch.valid dS_New.countBatch.test];
    
    try %#ok<TRYNC>
        rmdir(saveOpts_MainFolder_Backup);
    end
end