function resultStruct = loopOverUbuntu_BXX(methodResult, bID)
% Models - M
% mi_mj - model learnt for discriminating mi_mj
% di_dj - data used from the output of mi_mj model
    resultStruct = struct;
    
    if methodResult == 1
        resultStruct.accM_tr = NaN(45,45);%accuracyMat
        resultStruct.accM_te = NaN(45,45);%accuracyMat
        resultStruct.accM_tr_cross = NaN(45,45);%accuracyMat
        resultStruct.accM_te_cross = NaN(45,45);%accuracyMat
        resultStruct.accM_tr_base = NaN(10,10);%accuracyMat
        resultStruct.accM_te_base = NaN(10,10);%accuracyMat
    elseif methodResult == 2
        resultStruct.accMat = zeros(45,4);
        resultStruct.representData_tr = [];
        resultStruct.representData_te = [];
                
        resultStruct.labels_tr = [];
        resultStruct.labels_te = [];
                
        resultStruct.predictionProbs_orig_tr = [];
        resultStruct.predictionProbs_orig_te = [];
                
        resultStruct.prediction_orig_tr = [];
        resultStruct.prediction_orig_te = [];
                
        resultStruct.prediction_ELM_tr = [];
        resultStruct.prediction_ELM_te = [];
    end
    baseFolderName = 'D:\FromUbuntu\';
    for mi = 1:9
        for mj = mi+1 : 10
            tic;
            [projFolderName, MXX] = getFolderName(mi,mj, bID);
            disp(['Running for ' num2str(mi) '_' num2str(mj) ', ' MXX])
            
            disp('Loading test_all_data')
            test_all_data = importdata([baseFolderName projFolderName '\test_data_B' num2str(bID,'%02d') '_' MXX '.txt']);

            toc;
            disp('Loading train_all_data')
            train_all_data = importdata([baseFolderName projFolderName '\train_data_B' num2str(bID,'%02d') '_' MXX '.txt']);
            
            toc;
            disp('Loading test_all_labels')
            test_all_labels = importdata([baseFolderName projFolderName '\test_label_B' num2str(bID,'%02d') '_' MXX '.txt']);
            
            toc;
            disp('Loading train_all_labels')
            train_all_labels = importdata([baseFolderName projFolderName '\train_label_B' num2str(bID,'%02d') '_' MXX '.txt']);

            toc;
            disp('Loading train_model_pred2D')
            test_model_pred2D = importdata([baseFolderName projFolderName '\test_predData_B' num2str(bID,'%02d') '_' MXX '.txt']);

            toc;
            disp('Loading train_model_pred2D')
            train_model_pred2D = importdata([baseFolderName projFolderName '\train_predData_B' num2str(bID,'%02d') '_' MXX '.txt']);

            test_all_labels = test_all_labels + 1;
            train_all_labels = train_all_labels + 1;
            
            [slct_tr, slct_te] = getSelectedDataLabels(mi,mj, train_all_data, train_all_labels, test_all_data, test_all_labels);

            [~,predTr_mij] = max(train_model_pred2D(slct_tr,:),[],2);
            [~,predTe_mij] = max(test_model_pred2D(slct_te,:),[],2);
            labelTr_mij = train_all_labels(slct_tr);
            labelTe_mij = test_all_labels(slct_te);
            [labelTr_mij, predTr_mij] = resetLabelsTo2Class(labelTr_mij, predTr_mij, mi, mj);
            [labelTe_mij, predTe_mij] = resetLabelsTo2Class(labelTe_mij, predTe_mij, mi, mj);
            
            accBase_mij_tr = calculateConfusion(predTr_mij, labelTr_mij, false, 2);
            accBase_mij_te = calculateConfusion(predTe_mij, labelTe_mij, false, 2);
            
            disp(['Base accuracy for ' num2str(mi) '_' num2str(mj) '; tr(' num2str(accBase_mij_tr,'%4.2f') '), te(' num2str(accBase_mij_te,'%4.2f') ')']);
            
            if methodResult == 1
                resultStruct.accM_tr_base(mi,mj) = accBase_mij_tr;
                resultStruct.accM_te_base(mi,mj) = accBase_mij_te;

                [accM_tr, accM_te, accM_tr_cross, accM_te_cross] = loopOverModelOutput(mi,mj,train_all_data, train_all_labels, test_all_data, test_all_labels);
                resultStruct = addResultsNewData(resultStruct, accM_tr, accM_te, accM_tr_cross, accM_te_cross);
            elseif methodResult == 2
                [~, ~, data_ij_tr, labels_ij_tr, data_ij_te, labels_ij_te] = getSelectedDataLabels(mi,mj, train_all_data, train_all_labels, test_all_data, test_all_labels);
                [Welm_mi_mj, acc_ELM_tr, acc_ELM_te] = getAndOrApplyELM(mi,mj,mi,mj,data_ij_tr, labels_ij_tr, data_ij_te, labels_ij_te);
                
                mId = getModelID(mi,mj);
                resultStruct.accMat(mId,:) = [accBase_mij_tr acc_ELM_tr accBase_mij_te acc_ELM_te];
                resultStruct.representData_tr = [resultStruct.representData_tr,train_all_data];
                resultStruct.representData_te = [resultStruct.representData_te,test_all_data];
                
                resultStruct.labels_tr = [resultStruct.labels_tr,train_all_labels];    
                resultStruct.labels_te = [resultStruct.labels_te,test_all_labels];    
                
                train_model_pred2D = bsxfun(@minus,train_model_pred2D,max(train_model_pred2D,[],2));
                test_model_pred2D = bsxfun(@minus,test_model_pred2D, max(test_model_pred2D,[],2));
                
                [train_model_pred2D_max,predTr_mij] = max(train_model_pred2D,[],2);
                [test_model_pred2D_max,predTe_mij] = max(test_model_pred2D,[],2);
                train_model_pred2D = exp(train_model_pred2D);
                test_model_pred2D = exp(test_model_pred2D);
            
                resultStruct.predictionProbs_orig_tr = [resultStruct.predictionProbs_orig_tr,train_model_pred2D];
                resultStruct.predictionProbs_orig_te = [resultStruct.predictionProbs_orig_te,test_model_pred2D];
                
                resultStruct.prediction_orig_tr = [resultStruct.prediction_orig_tr,2*(predTr_mij-1.5)];
                resultStruct.prediction_orig_te = [resultStruct.prediction_orig_te,2*(predTe_mij-1.5)];
            end
            toc;
        end
    end
    
    if methodResult==2
        X_tr = resultStruct.representData_tr;
        L_tr = resultStruct.labels_tr(:,1);
        X_te = resultStruct.representData_te;
        L_te = resultStruct.labels_te(:,1);
        PP_tr = resultStruct.predictionProbs_orig_tr;
        PP_te = resultStruct.predictionProbs_orig_te;
        PC_tr = resultStruct.prediction_orig_tr;
        PC_te = resultStruct.prediction_orig_te;
        
        [Welm_rep, ~, conf_rep_tr, acc_rep_tr] = getELM_Weights_SoftMax(X_tr, L_tr, [], setDisplayOpts('none'));
        [~, ~, ~, acc_rep_te, conf_rep_te] = analyzeActivationsElm(X_te, L_te, Welm_rep, false);
        [Welm_PP, ~, conf_PP_tr, acc_PP_tr] = getELM_Weights_SoftMax(PP_tr, L_tr, [], setDisplayOpts('none'));
        [~, ~, ~, acc_PP_te, conf_PP_te] = analyzeActivationsElm(PP_te, L_te, Welm_PP, false);
        [Welm_PC, ~, conf_PC_tr, acc_PC_tr] = getELM_Weights_SoftMax(PC_tr, L_tr, [], setDisplayOpts('none'));
        [~, ~, ~, acc_PC_te, conf_PC_te] = analyzeActivationsElm(PC_te, L_te, Welm_PC, false);        
        
        h1 = drawConfusionDetailed(conf_rep_tr, struct('figureID', {1}, 'figureTitle' , 'conf_rep_tr'));
        h2 = drawConfusionDetailed(conf_rep_te, struct('figureID', {2}, 'figureTitle' , 'conf_rep_te'));
        h3 = drawConfusionDetailed(conf_PP_tr, struct('figureID', {3}, 'figureTitle' , 'conf_PP_tr'));
        h4 = drawConfusionDetailed(conf_PP_te, struct('figureID', {4}, 'figureTitle' , 'conf_PP_te'));
        h5 = drawConfusionDetailed(conf_PC_tr, struct('figureID', {5}, 'figureTitle' , 'conf_PC_tr'));
        h6 = drawConfusionDetailed(conf_PC_te, struct('figureID', {6}, 'figureTitle' , 'conf_PC_te'));
    end
end

function [givenLabels, predLabels] = resetLabelsTo2Class(givenLabels, predLabels, mi, mj)
    curPredLabels = unique(predLabels);
    if sum(curPredLabels-[1;2])~=0
        predLabels(predLabels==mi) = 1;
        predLabels(predLabels==mj) = 2;
    end

    givenLabels(givenLabels==mi) = 1;
    givenLabels(givenLabels==mj) = 2;
end

function resultStruct = addResultsNewData(resultStruct, accM_tr, accM_te, accM_tr_cross, accM_te_cross)
    resultStruct.accM_tr(~isnan(accM_tr)) = accM_tr(~isnan(accM_tr));
    resultStruct.accM_te(~isnan(accM_te)) = accM_te(~isnan(accM_te));
    resultStruct.accM_tr_cross(~isnan(accM_tr_cross)) = accM_tr_cross(~isnan(accM_tr_cross));
    resultStruct.accM_te_cross(~isnan(accM_te_cross)) = accM_te_cross(~isnan(accM_te_cross));
end

function [accM_tr, accM_te, accM_tr_cross, accM_te_cross] = loopOverModelOutput(mi, mj, train_all_data, train_all_labels, test_all_data, test_all_labels)
    accM_tr = NaN(45,45);%accuracyMat
    accM_te = NaN(45,45);%accuracyMat
    accM_tr_cross = NaN(45,45);%accuracyMat
    accM_te_cross = NaN(45,45);%accuracyMat
    mId = getModelID(mi,mj);
    [slct_tr, slct_te, data_ij_tr, labels_ij_tr, data_ij_te, labels_ij_te] = getSelectedDataLabels(mi,mj, train_all_data, train_all_labels, test_all_data, test_all_labels);
    [Welm_mi_mj, accM_tr(mId,mId), accM_te(mId,mId)] = getAndOrApplyELM(mi,mj,mi,mj,data_ij_tr, labels_ij_tr, data_ij_te, labels_ij_te);
    disp(['  ELM-Accuracy for ' num2str(mi) '_' num2str(mj) '; tr(' num2str(accM_tr(mId,mId),'%4.2f') '), te(' num2str(accM_te(mId,mId),'%4.2f') ')']);
    for di = 1:9
        for dj = di+1 : 10
            %now pick only samples of [di and dj] from the output of
            %model output given here
            
            %learn an extreme learning machine with this representation to
            %see how useful it is to compare this totally different class
            %subset [di and dj]
            dId = getModelID(di,dj);
            if mId ~= dId
                disp(['  Running data grabbed from exported model (' num2str(mi) '_' num2str(mj) '), tried on data couple (' num2str(di) '_' num2str(dj) ')'])
                [slct_tr, slct_te, data_ij_tr, labels_ij_tr, data_ij_te, labels_ij_te] = getSelectedDataLabels(di,dj, train_all_data, train_all_labels, test_all_data, test_all_labels);
                [Welm_md_mij, accM_tr(mId,dId), accM_te(mId,dId), accM_tr_cross(mId,dId), accM_te_cross(mId,dId)] = getAndOrApplyELM(mi,mj,mi,mj,data_ij_tr, labels_ij_tr, data_ij_te, labels_ij_te, Welm_mi_mj);
                disp(['    ELM-Accuracy for ' num2str(di) '_' num2str(dj) '; tr(' num2str(accM_tr(mId,dId),'%4.2f') '), te(' num2str(accM_te(mId,dId),'%4.2f') ')']);
                disp(['    Cross-Accuracy for ' num2str(di) '_' num2str(dj) '; tr(' num2str(accM_tr_cross(mId,dId),'%4.2f') '), te(' num2str(accM_te_cross(mId,dId),'%4.2f') ')']);
            end
        end
    end
end

function [slct_tr, slct_te, data_ij_tr, labels_ij_tr, data_ij_te, labels_ij_te] = getSelectedDataLabels(di,dj, train_all_data, train_all_labels, test_all_data, test_all_labels)
    slct_tr = find(train_all_labels==di | train_all_labels==dj);
    slct_te = find(test_all_labels==di | test_all_labels==dj);

    if nargout >2
        data_ij_tr = train_all_data(slct_tr,:);
        data_ij_te = test_all_data(slct_te,:);

        labels_ij_tr = train_all_labels(slct_tr);
        labels_ij_te = test_all_labels(slct_te);
        
        [labels_ij_tr, labels_ij_te] = resetLabelsTo2Class(labels_ij_tr, labels_ij_te, di, dj);
    end
end

function [Welm_di_dj, acc_di_dj_tr, acc_di_dj_te, acc_md_ij_tr, acc_md_ij_te] = getAndOrApplyELM(mi,mj,di,dj, Xtr, Ltr, Xte, Lte, W_mi_mj)
    %1. use the data of classes di and dj
    %   the representation of learner output(mi and mj)
    %   learn final layer with elm
    [Welm_di_dj, ~, conf_di_dj_tr, acc_di_dj_tr] = getELM_Weights_SoftMax(Xtr, Ltr, [], setDisplayOpts('none'));
    
    %2. test the learnt elm weight on test data
    [~, ~, ~, acc_di_dj_te, conf_di_dj_te] = analyzeActivationsElm(Xte, Lte, Welm_di_dj, false);
    
    if exist('W_mi_mj', 'var')
        %3. if the ELM weights of original mi and mj exist
        %   test those weights on classes di and dj to see resemblance
        [~, ~, ~, acc_md_ij_tr, conf_md_ij_tr] = analyzeActivationsElm(Xtr, Ltr, W_mi_mj, false);
        acc_md_ij_tr = signAccuracyResult(conf_md_ij_tr, acc_md_ij_tr);
        [~, ~, ~, acc_md_ij_te, conf_md_ij_te] = analyzeActivationsElm(Xte, Lte, W_mi_mj, false);
        acc_md_ij_te = signAccuracyResult(conf_md_ij_te, acc_md_ij_te);
    else
        acc_md_ij_tr = NaN;
        conf_md_ij_tr = NaN;
        acc_md_ij_te = NaN;
        conf_md_ij_te = NaN;
    end
end

function accRes = signAccuracyResult(confMat, accRes)
   if (confMat(1,1) + confMat(2,2)) < (confMat(1,2) + confMat(2,1))
       accRes = -(100-accRes);
   end
end

function [projFolderName, MXX] = getFolderName(mi,mj,b)
    mId = getModelID(mi,mj);
    MXX = ['M' num2str(mId,'%02d')];
    projFolderName = ['cifar_B' num2str(b,'%02d') '_' MXX '_' num2str(mi) '-' num2str(mj) '_0-1'];
end

function mId = getModelID(mi,mj)
    mId = 0;
    for i = 1:9
        for j = i+1 : 10
            mId = mId + 1;
            if mi==i && mj==j
                return
            end
        end
    end
end