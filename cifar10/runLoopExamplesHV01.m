    clearvars;
    clc;%

    global bigDataPath;
    global fastDiskTempPath;
    
    %set for running this script 
    %e.g. : exampleID = 1;runExample_Script;
    
    %you can reset these variables in the exampleID switch
    exampleID = 1;
    %dataShuffleMode = 'uniform';%'none';%
    architectureIdentifier = 'CIFAR10_HV01_PCA_All';
    batchParamsMode = struct;
    batchParamsMode.maxIter = 20;
    batchParamsMode.epochIter = 20;
    
    batch_Count_Vec = [10 5 4];%batch_Count_Vec = [2 3 3];%
    validBatchID = 5;
    commitCode = 'b815734';
    
    categoriesToLoad_Mat = [...
                            3  7;%  1-H1 -'bird'-'frog'
                            9 10 %  2-H2 -'ship'-'truck'
                            3 10;%  3-H3 -'bird'-'truck'
                            1  9;%  4-H4 -'airplane'-'ship'
                            2 10;%  5-H5 -'automobile'-'truck'
                            1  3;%  6-H6 -'airplane'-'bird'
                            1 10;%  7-H7 -'airplane'-'truck'
                            1  7;%  8-H8 -'airplane'-'frog'
                            2  3;%  9-H9 -'automobile'-'bird'
                            2  7;% 10-H10-'automobile'-'frog'
                            3  9;% 11-H11-'bird'-'ship'
                            2  9;% 12-H12-'automobile'-'ship'
                            7  9;% 13-H13-'frog'-'ship'
                            7 10% 14-H14-'frog'-'truck'
                            ];
    numOfExperiments = length(categoriesToLoad_Mat);
    for i=2:numOfExperiments
        try
            curProjCode = ['H' num2str(i,'%02d')];
            diary([fastDiskTempPath curProjCode '_mos.txt']);
            categoriesToLoad = categoriesToLoad_Mat(i,:);
            dataLoadFunction = @createInitialDataStruct_CIFAR;
            dataSetName = 'CIFAR10';%
            [dS, recreated] = checkRecreateCifarBatches(categoriesToLoad, validBatchID, batch_Count_Vec, true);
            dataLoadParams = {[bigDataPath 'CIFAR10\'],categoriesToLoad};

        %%  STEP 1  : Learn
            [opt_cnnnet, outParams, batchInfo, saveMainFolder] = ...
                                                 runExample_Func(dataSetName, dataLoadFunction, dataLoadParams...
                                                 ,curProjCode , commitCode, architectureIdentifier...
                                                 ,'categoriesToLoad',categoriesToLoad...
                                                 ,'exampleID',exampleID...
                                                 ,'batchParamsMode',batchParamsMode);
            dataShuffleMode = 'uniform';
            save([saveMainFolder '\outresults.mat'],'architectureIdentifier', 'batchInfo', 'opt_cnnnet', 'outParams', 'categoriesToLoad', 'dataShuffleMode', '-v7.3');
        catch
            disp(['Skipped (' curProjCode ') for some reason']);
        end
        diary('off');
        clc;
    end
                                     
                                     
                                     