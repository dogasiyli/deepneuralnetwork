    clearvars;
    clc;%

    global bigDataPath;
    global fastDiskTempPath;
    
    whereWas_i_init = 1;
    whereWasI_fileName = [fastDiskTempPath 'whereWasI.mat'];
    if exist(whereWasI_fileName,'file')
        load(whereWasI_fileName,'whereWas_i_init');
    end
    whereWas_i = whereWas_i_init;

    %set for running this script 
    %e.g. : exampleID = 1;runExample_Script;
    
    %you can reset these variables in the exampleID switch
    exampleID = 1;
    priorInfoUse = 'max';
    %dataShuffleMode = 'uniform';%'none';%
    architectureIdentifier = 'CIFAR10_DEEP01_Rand';
    batchParamsMode = struct;
    batchParamsMode.maxIter = 20;
    batchParamsMode.epochIter = 20;
    batchParamsMode.batchLoopType = 'all';
    
    batch_Count_Vec = [10 5 4];%batch_Count_Vec = [2 3 3];%
    validBatchID = 5;
    commitCode = 'af_4c52adf';
    
    categoriesToLoad_Mat = nchoosek(1:10,2);
    
    numOfExperiments = size(categoriesToLoad_Mat,1);
    for i=whereWas_i:numOfExperiments
        try
            curProjCode = ['H' num2str(i+11,'%02d')];
            diary([fastDiskTempPath curProjCode '_mos.txt']);
            categoriesToLoad = categoriesToLoad_Mat(i,:);
            dataLoadFunction = @createInitialDataStruct_CIFAR;
            dataSetName = 'CIFAR10';%
            disp(['categoriesToLoad = ' mat2str(categoriesToLoad)]);
            [dS, recreated] = checkRecreateCifarBatches(categoriesToLoad, validBatchID, batch_Count_Vec, true);
            dataLoadParams = {[bigDataPath 'CIFAR10\'],categoriesToLoad};

        %%  STEP 1  : Learn
            [opt_cnnnet, outParams, batchInfo, saveMainFolder] = ...
                                                 runExample_Func(dataSetName, dataLoadFunction, dataLoadParams...
                                                 ,curProjCode , commitCode, architectureIdentifier...
                                                 ,'categoriesToLoad',categoriesToLoad...
                                                 ,'exampleID',exampleID...
                                                 ,'batchParamsMode',batchParamsMode...
                                                 ,'priorInfoUse',priorInfoUse);
            dataShuffleMode = 'uniform';
            save([saveMainFolder '\outresults.mat'],'architectureIdentifier', 'batchInfo', 'opt_cnnnet', 'outParams', 'categoriesToLoad', 'dataShuffleMode', '-v7.3');
        catch
            disp(['Skipped (' curProjCode ') for some reason']);
        end
        diary('off');
        clc;
        
        whereWas_i_init = i+1;
        save(whereWasI_fileName,'whereWas_i_init');        
    end
                                     
                                     
                                     