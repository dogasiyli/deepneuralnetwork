    clearvars;
    clc;%

    global bigDataPath;
    global fastDiskTempPath;
    
    whereWas_j_i_init = [1 1];
    whereWasI_fileName = [fastDiskTempPath 'whereWasI.mat'];
    if exist(whereWasI_fileName,'file')
        load(whereWasI_fileName,'whereWas_j_i_init');
    end
    whereWas_j_i = whereWas_j_i_init;
    
    %set for running this script 
    %e.g. : exampleID = 1;runExample_Script;
    
    %you can reset these variables in the exampleID switch
    exampleID = 1;
    %dataShuffleMode = 'uniform';%'none';%
    architectureIdentifier = 'CIFAR10_DEEP04_PCA_All';
    batchParamsMode = struct;
    batchParamsMode.maxIter = 20;
    batchParamsMode.epochIter = 20;
    
    batch_Count_Vec = [10 5 4];%batch_Count_Vec = [2 3 3];%
    validBatchID = 5;
    commitCode = 'af_905dde2';
    
    categoriesToLoad = [1 2 3 5 7 9 10];
    labelResetMat_All =        [1 2 3 4 5 6 7;1 2 2 2 2 2 2];% 1 - 2 3 5 7 9 10
    labelResetMat_All(:,:,2) = [1 2 3 4 5 6 7;2 1 2 2 2 2 2];% 2 - 1 3 5 7 9 10
    labelResetMat_All(:,:,3) = [1 2 3 4 5 6 7;2 2 1 2 2 2 2];% 3 - 1 2 5 7 9 10
    labelResetMat_All(:,:,4) = [1 2 3 4 5 6 7;2 2 2 1 2 2 2];% 5 - 1 2 3 7 9 10
    labelResetMat_All(:,:,5) = [1 2 3 4 5 6 7;2 2 2 2 1 2 2];% 7 - 1 2 3 5 9 10
    labelResetMat_All(:,:,6) = [1 2 3 4 5 6 7;2 2 2 2 2 1 2];% 9 - 1 2 3 5 7 10
    labelResetMat_All(:,:,7) = [1 2 3 4 5 6 7;2 2 2 2 2 2 1];%10 - 1 2 3 5 7  9
    
    numOfExperiments = size(labelResetMat_All,3);
    priorInfoUse_All={'max','none'};
    for j=whereWas_j_i(1):2
        for i=whereWas_j_i(2):numOfExperiments
            priorInfoUse = priorInfoUse_All{1,j};
            try
                S_XX = i + (j-1)*numOfExperiments + 41;
                curProjCode = ['S' num2str(S_XX,'%02d')];
                diary([fastDiskTempPath curProjCode '_mos.txt']);
                labelResetMat = squeeze(labelResetMat_All(:,:,i));
                disp(['categoriesToLoad = ' mat2str(categoriesToLoad)]);
                disp(['labelResetMat = ' mat2str(labelResetMat)]);
                
                dataLoadFunction = @createInitialDataStruct_CIFAR;
                dataSetName = 'CIFAR10';%
                [dS, recreated] = checkRecreateCifarBatches(categoriesToLoad, validBatchID, batch_Count_Vec, true, labelResetMat);
                dataLoadParams = {[bigDataPath 'CIFAR10\'],categoriesToLoad};

            %%  STEP 1  : Learn
                [opt_cnnnet, outParams, batchInfo, saveMainFolder] = ...
                                                     runExample_Func(dataSetName, dataLoadFunction, dataLoadParams...
                                                     ,curProjCode , commitCode, architectureIdentifier...
                                                     ,'categoriesToLoad',categoriesToLoad...
                                                     ,'exampleID',exampleID...
                                                     ,'batchParamsMode',batchParamsMode...
                                                     ,'priorInfoUse',priorInfoUse);
                dataShuffleMode = 'uniform';
                save([saveMainFolder '\outresults.mat'],'architectureIdentifier', 'batchInfo', 'opt_cnnnet', 'outParams', 'categoriesToLoad', 'dataShuffleMode','priorInfoUse', '-v7.3');
            catch
                disp(['Skipped (' curProjCode ') for some reason']);
            end
                diary('off');
                clc;
            whereWas_j_i_init = [j i+1];
            save(whereWasI_fileName,'whereWas_j_i_init');
        end
        whereWas_j_i_init = [j+1 1];
        save(whereWasI_fileName,'whereWas_j_i_init');
    end                           