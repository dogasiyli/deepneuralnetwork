function [ dataAllLL_tr, labelAllLL_tr, dataAllLL_va, labelAllLL_va, dataAllLL_te, labelAllLL_te, W_CurClasses, res_CurClasses ] = ...
         runNetOnMoreData(dS, netFileName, allC, netC, testInitFinalStr, epochID, saveMainFolder)

    [displayOpts_None, displayOpts_All, saveOpts_None] = cerateHelperVars(saveMainFolder);
    
    cnnnet_Cur = loadCnnetFromFile(netFileName);
    cnnnet_ALL = convert2Cto4C(cnnnet_Cur, allC, netC);
    [cnnnet_ALL, ~, ~, ~, dataAllLL_tr, labelAllLL_tr] = deepNet_Test( dS, cnnnet_ALL, displayOpts_None, saveOpts_None, 'train', testInitFinalStr, epochID-1, true);
            cnnnet_ALL{end, 1}.Weight = cnnnet_ALL{1, 1}.Welm';
            cnnnet_ALL{end, 1}.BiasParams = zeros(size(cnnnet_ALL{end, 1}.BiasParams));
    cnnnet_ALL = deepNet_Test( dS, cnnnet_ALL, displayOpts_None, saveOpts_None, 'train', testInitFinalStr, epochID, true);
    [ ~, ~, ~, ~, dataAllLL_va, labelAllLL_va] = deepNet_Test( dS, cnnnet_ALL, displayOpts_None, saveOpts_None, 'valid', testInitFinalStr, epochID, true);
    [ ~, ~, ~, ~, dataAllLL_te, labelAllLL_te] = deepNet_Test( dS, cnnnet_ALL, displayOpts_None, saveOpts_None, 'test', testInitFinalStr, epochID, true);

    [slct_tr, labels_tr] = getSlct(labelAllLL_tr, allC, netC);
    data_tr = dataAllLL_tr(:,slct_tr);

    [slct_va, labels_va] = getSlct(labelAllLL_va, allC, netC);
    data_va = dataAllLL_va(:,slct_va);

    [slct_te, labels_te] = getSlct(labelAllLL_te, allC, netC);
    data_te = dataAllLL_te(:,slct_te);

    [W_CurClasses, ~, ~, ~, rd_Cur] = getELM_Weights_SoftMax(data_tr', labels_tr, [], displayOpts_All);
    [~, ~, ~, acc_tr, cM_tr] = analyzeActivationsElm(data_tr', labels_tr, W_CurClasses, true);
    displayConfusionMatrix(cM_tr, netC);
    [~, ~, ~, acc_va, cM_va] = analyzeActivationsElm(data_va', labels_va, W_CurClasses, true);
    displayConfusionMatrix(cM_va, netC);
    [~, ~, ~, acc_te, cM_te] = analyzeActivationsElm(data_te', labels_te, W_CurClasses, true);
    displayConfusionMatrix(cM_te, netC);
    
    res_CurClasses = struct;
    res_CurClasses.rd_Cur = rd_Cur;
    res_CurClasses.acc_tr = acc_tr;
    res_CurClasses.acc_va = acc_va;
    res_CurClasses.acc_te = acc_te;
    res_CurClasses.cM_tr = cM_tr;
    res_CurClasses.cM_va = cM_va;
    res_CurClasses.cM_te = cM_te;
end

function [slct_xx, labels_xx] = getSlct(labelAllLL, allC, netC)
    labelAllLL = allC(labelAllLL);
    labels_xx = NaN(size(labelAllLL));
    slct_xx = false(size(labelAllLL));
    cni = 1;
    for ci = 1:length(netC)
        c = netC(ci);
        slct_cur = labelAllLL==c;
        N = sum(slct_cur==1);
        if N>0
            disp([num2str(N) ' samples are taken from class(' num2str(c) ')']);
            slct_xx(slct_cur) = true;
            labels_xx(slct_cur) = cni;
            cni = cni + 1;
        end
    end
    labels_xx(isnan(labels_xx)) = [];
end
function [displayOpts_None, displayOpts_All, saveOpts_None] = cerateHelperVars(saveMainFolder)
    displayOpts_None = setDisplayOpts('none');
    displayOpts_All = setDisplayOpts('all');
    saveOpts_None = setSaveOpts('none', saveMainFolder);
end