function [ batchInfo ] = seperateAndCreateBatchInfoCifar10( categoriesToLoad, loadFolderName, imageRowColSize, bathSizeMaxMB, batch_Count_Vec, validBatchID)
%seperateCifar10 This function will seperate the data into batches
%   There are 5 train batches given in the dataset - so training and valid
%   will be taken from them
%   

    %categoriesToLoad should have numbers [1 to 10]
    categoryCountOriginal = 10;
    categoriesToLoad = sort(categoriesToLoad);
    categoryCount = length(categoriesToLoad);

    %% 1. take the number of samples in the input matrix
    imgCntTrain_In = zeros(5,10);
    for batchID_in=1:5
        batchFileName = ['data_batch_' num2str(batchID_in)];
        load([loadFolderName batchFileName '.mat'],'labels')
        labels = labels + 1;
        imgCntTrain_In(batchID_in,:) = hist(labels,1:10);
    end
    imgCntTest_In = 1000*ones(1,10);
    
    %Matrix is 5 by 10 where each row represents a batch in and each column
    %represents a class
    batchDistributions = struct([]);
    
    %I need trainData, validationData and testData
    %testData obviously will only have the testPerson's data
    %train data would have the samples
    
    %The max batch size is given in mb or count of images
    %I will know how many image can there be in a batch
    
    singleImageSize = imageRowColSize^2*3;%this many double values
    singleImageSize = singleImageSize*8;%bytes
    singleImageSize = singleImageSize/(1024*1024);%megabytes
    disp(['Single image size is ' num2str(singleImageSize) ' MB'])
    
    if isempty(bathSizeMaxMB)
        imageCountMaxPerBatch = [];
    else
        imageCountMaxPerBatch = floor(bathSizeMaxMB/singleImageSize);
        disp(['Maximum allowed image count per batch is ' num2str(imageCountMaxPerBatch)])
    end

    imgCntMxTrIn = imgCntTrain_In((1:5)~=validBatchID,categoriesToLoad);
    imgCntMxVaIn = imgCntTrain_In(validBatchID,categoriesToLoad);
    imgCntMxTeIn = imgCntTest_In(:,categoriesToLoad);
    
    image_Count_Total_Test = sum(imgCntMxTeIn(:));
    if ~isempty(imageCountMaxPerBatch)
        batch_Count_Test = ceil(image_Count_Total_Test/imageCountMaxPerBatch);
    elseif exist('batch_Count_Vec','var') && ~isempty(batch_Count_Vec)
        batch_Count_Test = batch_Count_Vec(3);
    else
        batch_Count_Test = 3;
    end
    repCount = ceil(image_Count_Total_Test/batch_Count_Test);
    bid_Test = repmat(1:batch_Count_Test,1,repCount);
    bid_Test = bid_Test(1:image_Count_Total_Test); 
    
    image_Count_Total_Train = sum(imgCntMxTrIn(:));
    image_Count_Total_Valid = sum(imgCntMxVaIn(:));    
    image_Count_Total_TrainValid = image_Count_Total_Train + image_Count_Total_Valid;

    if ~isempty(imageCountMaxPerBatch)
        batch_Count_Train = ceil((image_Count_Total_Train)/imageCountMaxPerBatch);
        batch_Count_Valid = ceil((image_Count_Total_Valid)/imageCountMaxPerBatch);
    elseif exist('batch_Count_Vec','var') && ~isempty(batch_Count_Vec)
        batch_Count_Train = batch_Count_Vec(1);
        batch_Count_Valid = batch_Count_Vec(2);
    else
        batch_Count_Train = 4;
        batch_Count_Valid = 1;
    end
    
    repCount_Tr = ceil(image_Count_Total_Train/batch_Count_Train);
    bid_Train = repmat(1:batch_Count_Train,1,repCount_Tr);
    bid_Train = bid_Train(1:image_Count_Total_Train); 
    
    repCount_Va = ceil(image_Count_Total_Valid/batch_Count_Valid);
    bid_Valid = repmat(1:batch_Count_Valid,1,repCount_Va);
    bid_Valid = bid_Valid(1:image_Count_Total_Valid)+batch_Count_Train;
    clear repCount_Tr repCount_Va
    
    bid_TrainValid = NaN(1, image_Count_Total_TrainValid);
    
    to = 0;
    for batchID_in=1:5
        imgCntTrainInBatch = imgCntTrain_In(batchID_in,categoriesToLoad);
        sampleCntToPush = sum(imgCntTrainInBatch);
        fr = to + 1;
        to = fr + sampleCntToPush-1;
        if batchID_in==validBatchID
            bid_TrainValid(fr:to) = bid_Valid(1:sampleCntToPush);
            bid_Valid(1:sampleCntToPush) = [];
        else
            bid_TrainValid(fr:to) = bid_Train(1:sampleCntToPush);
            bid_Train(1:sampleCntToPush) = [];            
        end
    end    
    assert(isempty(bid_Train),'all samples must have been pushed in train')
    assert(isempty(bid_Valid),'all samples must have been pushed in valid')
    clear imgCntTrainInBatch sampleCntToPush fr to bid_Train bid_Valid

    assert(image_Count_Total_Train == sum(bid_TrainValid<=batch_Count_Train),'Must be equal');
    assert(image_Count_Total_Valid == sum(bid_TrainValid>batch_Count_Train),'Must be equal');
    
    disp(['Train - imageCount(' num2str(image_Count_Total_Train) '), batchCount(' num2str(batch_Count_Train) ')'])
    disp(['Valid - imageCount(' num2str(image_Count_Total_Valid) '), batchCount(' num2str(batch_Count_Valid) ')'])
    disp(['Test  - imageCount(' num2str(image_Count_Total_Test) '), batchCount(' num2str(batch_Count_Test) ')'])
    

    % mainFolder = 'G:\Data\dataset5';
    
    image_Count_Matrix_Train = zeros(1,categoryCount);
    image_Count_Matrix_Valid = zeros(1,categoryCount);
    image_Count_Matrix_Test  = zeros(1,categoryCount);
    image_Count_Per_Batch_Train = zeros(batch_Count_Train,categoryCount);
    image_Count_Per_Batch_Valid = zeros(batch_Count_Valid,categoryCount);
    image_Count_Per_Batch_Test = zeros(batch_Count_Test,categoryCount);

    %first I need to fill in batchDistributions cell
    %so that I can know which sample to put into which batch data
    
    %train and validation
    for batchID_in = 1:5
        for i = 1:categoryCount
            c = categoriesToLoad(i);
            imageCountCur = imgCntTrain_In(batchID_in,c);
            %these images will be distributed as
            %train or validation
            %Batch ID information will be set as 1-2-3-...-batch_Count_TrainValid
            %if "1<=bid<=batch_Count_Train" then it is train sample
            %else if "batch_Count_Train+1<=bid<=batch_Count_TrainValid" then it is validation sample
            bid = bid_TrainValid(1:imageCountCur);
            bid_TrainValid = bid_TrainValid(1+imageCountCur : end);

            %train-validation-test information will be 
            %1=train if 
            tvt = 1*ones(1,imageCountCur);%will be either train(1) or validation(2)
            tvt(bid>batch_Count_Train) = 2;
            batchDistributions(batchID_in,i).tvt = tvt;
            batchDistributions(batchID_in,i).TR = bid <= batch_Count_Train;
            batchDistributions(batchID_in,i).VA = bid >  batch_Count_Train;
            batchDistributions(batchID_in,i).TE = false(1,imageCountCur);

            bid(bid>batch_Count_Train) = bid(bid>batch_Count_Train)-batch_Count_Train;
            batchDistributions(batchID_in,i).bid = bid;

            image_Count_Matrix_Train(1,i) = image_Count_Matrix_Train(1,i) + sum(batchDistributions(batchID_in,i).TR==1);
            image_Count_Matrix_Valid(1,i) = image_Count_Matrix_Valid(1,i) + sum(batchDistributions(batchID_in,i).VA==1);
            
            for b=1:batch_Count_Train
                image_Count_Per_Batch_Train(b,i) = image_Count_Per_Batch_Train(b,i) + sum(batchDistributions(batchID_in,i).TR==1 & bid==b);
            end
            for b=1:batch_Count_Valid
                image_Count_Per_Batch_Valid(b,i) = image_Count_Per_Batch_Valid(b,i) + sum(batchDistributions(batchID_in,i).VA==1 & bid==b);
            end
        end
    end
    assert(sum(image_Count_Matrix_Train+image_Count_Matrix_Valid-(sum(imgCntMxTrIn)+imgCntMxVaIn))==0,'Train-Valid allocation problematic');
    
    
    for i = 1:categoryCount
        c = categoriesToLoad(i);
        imageCountCur = imgCntTest_In(1,c);
        %test
        %train-validation-test information will all be 3=test
        tvt = 3*ones(1,imageCountCur);%all will be testData
        %Batch ID information will be set as 1-2-3-...-batch_Count_Test
        bid = bid_Test(1:imageCountCur);
        bid_Test = bid_Test(1+imageCountCur : end);

        batchDistributions(6,i).tvt = tvt;
        batchDistributions(6,i).bid = bid;
        batchDistributions(6,i).TR = false(1,imageCountCur);
        batchDistributions(6,i).VA = false(1,imageCountCur);
        batchDistributions(6,i).TE = true(1,imageCountCur);

        image_Count_Matrix_Test(1,i) = image_Count_Matrix_Test(1,i) + sum(batchDistributions(6,i).TE==1);
        for b=1:batch_Count_Test
            image_Count_Per_Batch_Test(b,i) = image_Count_Per_Batch_Test(b,i) + sum(batchDistributions(6,i).TE==1 & bid==b);
        end
    end      
   
    batchInfo.batchSizesMB_Train = singleImageSize*sum(image_Count_Per_Batch_Train,2);
    batchInfo.batchSizesMB_Valid = singleImageSize*sum(image_Count_Per_Batch_Valid,2);
    batchInfo.batchSizesMB_Test = singleImageSize*sum(image_Count_Per_Batch_Test,2);
    
    batchInfo.image_Count_Per_Batch_Train = image_Count_Per_Batch_Train;
    batchInfo.image_Count_Per_Batch_Valid = image_Count_Per_Batch_Valid;
    batchInfo.image_Count_Per_Batch_Test = image_Count_Per_Batch_Test;
    
    batchInfo.image_Count_Matrix_Train = image_Count_Matrix_Train;
    batchInfo.image_Count_Matrix_Valid = image_Count_Matrix_Valid;
    batchInfo.image_Count_Matrix_Test = image_Count_Matrix_Test;
    batchInfo.batch_Count_Train = batch_Count_Train;
    batchInfo.batch_Count_Valid = batch_Count_Valid;
    batchInfo.batch_Count_Test = batch_Count_Test;
    batchInfo.batchDistributions = batchDistributions;
end