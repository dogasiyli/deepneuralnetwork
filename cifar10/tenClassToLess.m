function [ accResults, testCaseResults, testCaseResults2] = tenClassToLess(mainFolder)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    accResults = struct;
    mainFolder = 'D:\FromUbuntu\acc82_all';

    disp('Loading test_all_labels')
    test_all_labels = importdata([mainFolder '\test_label_all82.txt']);

    disp('Loading train_all_labels')
    train_all_labels = importdata([mainFolder '\train_label_all82.txt']);

    disp('Loading train_model_pred2D')
    test_model_pred2D = importdata([mainFolder '\test_predData_all82.txt']);

    disp('Loading train_model_pred2D')
    train_model_pred2D = importdata([mainFolder '\train_predData_all82.txt']);

    test_all_labels = test_all_labels + 1;
    train_all_labels = train_all_labels + 1;

    [predTr, predTe] = getPredictions_01(train_model_pred2D, test_model_pred2D);
    
    
    runCaseArr = [3 4];

    
    if ~isempty(find(runCaseArr==0,1))
        %1. base model results
        figIDsToUse = [1,2];
        [accBase_tr, accBase_te, confBase_tr, confBase_te, h1, h2] = analyzePerExperiment(train_all_labels, test_all_labels, predTr, predTe, figIDsToUse, 'base82');
        accResults.accBase_tr = accBase_tr;
        accResults.accBase_te = accBase_te;
        disp(['Base accuracy for all82; tr(' num2str(accBase_tr,'%4.2f') '), te(' num2str(accBase_te,'%4.2f') ')']);
    else
        
    end
    
    
    if ~isempty(find(runCaseArr==1,1))
        %2.%case1 - C1 = 1 2 4 9 10 and C2 = 3 5 6 7 8
        [labTr, labTe, predTr, predTe] = getPredictions_02(train_model_pred2D, test_model_pred2D, [1 2 4 9 10], [3 5 6 7 8], train_all_labels, test_all_labels);
        figIDsToUse = [3,4];
        [accCase01_tr, accCase01_te, confCase01_tr, confCase01_te, h3, h4] = analyzePerExperiment(labTr, labTe, predTr, predTe, figIDsToUse, 'case01');
        accResults.accCase01_tr = accCase01_tr;
        accResults.accCase01_te = accCase01_te;
        disp(['Base accuracy for case1; tr(' num2str(accCase01_tr,'%4.2f') '), te(' num2str(accCase01_te,'%4.2f') ')']);
    end
    
    if ~isempty(find(runCaseArr==2,1))
        %3.%case2 - C1 = 1 2 7 9 10 and C2 = 3 4 5 6 8
        [labTr, labTe, predTr, predTe] = getPredictions_02(train_model_pred2D, test_model_pred2D, [1 2 7 9 10], [3 4 5 6 8], train_all_labels, test_all_labels);
        figIDsToUse = [5,6];
        [accCase02_tr, accCase02_te, confCase02_tr, confCase02_te, h5, h6] = analyzePerExperiment(labTr, labTe, predTr, predTe, figIDsToUse, 'case02');
        accResults.accCase02_tr = accCase02_tr;
        accResults.accCase02_te = accCase02_te;
        disp(['Base accuracy for case2; tr(' num2str(accCase02_tr,'%4.2f') '), te(' num2str(accCase02_te,'%4.2f') ')']);
    end
    
    
    if ~isempty(find(runCaseArr==3,1))
        %4.run a race test, for all possible 2 sets, what is the best two 5
        %couples, given that 4 and 6 are in the same list
        combos = nchoosek(1:8,5);
        combos(combos==4) = 9;
        combos(combos==6) = 10;
        numOfCombos = size(combos,1);
        testCaseResults = NaN(numOfCombos,20);
        for i=1:numOfCombos
            c1Arr = combos(i,:);
            c2Arr = 1:10;
            c2Arr(c1Arr) = [];
            [labTr, labTe, predTr, predTe] = getPredictions_02(train_model_pred2D, test_model_pred2D, c1Arr, c2Arr, train_all_labels, test_all_labels);
            [acc_tr_cur, acc_te_cur, conf_tr_cur, conf_te_cur] = analyzePerExperiment(labTr, labTe, predTr, predTe, []);
            testCaseResults(i,:) = [sort(c1Arr) c2Arr acc_tr_cur acc_te_cur reshape(conf_tr_cur,1,[]) reshape(conf_te_cur,1,[])];
        end
        testCaseResults = sortrows(testCaseResults,12);
    else
        testCaseResults = [];
    end
    
    if ~isempty(find(runCaseArr==4,1))
        %4.run a race test, for all possible 2 sets, what is the best two 5
        %couples, given that 4 and 6 are in the same list
        combos = nchoosek(1:10,5);
        numOfCombos = size(combos,1);
        testCaseResults2 = NaN(numOfCombos,20);
        for i=1:numOfCombos
            c1Arr = combos(i,:);
            c2Arr = 1:10;
            c2Arr(c1Arr) = [];
            [labTr, labTe, predTr, predTe] = getPredictions_02(train_model_pred2D, test_model_pred2D, c1Arr, c2Arr, train_all_labels, test_all_labels);
            [acc_tr_cur, acc_te_cur, conf_tr_cur, conf_te_cur] = analyzePerExperiment(labTr, labTe, predTr, predTe, []);
            testCaseResults2(i,:) = [sort(c1Arr) c2Arr acc_tr_cur acc_te_cur reshape(conf_tr_cur,1,[]) reshape(conf_te_cur,1,[])];
        end
        testCaseResults2 = sortrows(testCaseResults2,-12);
    else
        testCaseResults2 = [];
    end
    

end

function [predTr, predTe] = getPredictions_01(train_model_pred2D, test_model_pred2D)
%get the predictions as they were
    [~,predTr] = max(train_model_pred2D,[],2);
    [~,predTe] = max(test_model_pred2D,[],2);
end


function [labTr, labTe, predTr, predTe] = getPredictions_02(train_model_pred2D, test_model_pred2D, c1Arr, c2Arr, labTr, labTe)
%get the predictions as if
%case1 - C1 = 1 2 4 9 10 and C2 = 3 5 6 7 8
%case2 - C1 = 1 2 7 9 10 and C2 = 3 4 5 6 8
    [~,predTr] = max(train_model_pred2D,[],2);
    [~,predTe] = max(test_model_pred2D,[],2);  
    mapToUse = [c1Arr;c2Arr];
    labTr = mapPredictionsOrLabels(labTr, mapToUse);
    labTe = mapPredictionsOrLabels(labTe, mapToUse);
    predTr = mapPredictionsOrLabels(predTr, mapToUse);
    predTe = mapPredictionsOrLabels(predTe, mapToUse);
end

function pred_lab = mapPredictionsOrLabels(pred_lab, mapToUse)
    newClassCount = size(mapToUse,1);%rowCount = new class count
    for i=1:size(mapToUse,2)
        for j=1:newClassCount
            pred_lab(pred_lab==mapToUse(j,i)) = 100 + j;
        end
    end
    pred_lab = pred_lab - 100;
end

function [accTr, accTe, confTr, confTe, h1, h2] = analyzePerExperiment(labTr, labTe, predTr, predTe, figIDsToUse, figTitleBase)
    classCount = length(unique(labTr));
    [accTr, confTr] = calculateConfusion(predTr, labTr, false, classCount);
    [accTe, confTe] = calculateConfusion(predTe, labTe, false, classCount);
    if ~isempty(figIDsToUse)
        h1 = drawConfusionDetailed(confTr, struct('figureID', {figIDsToUse(1)}, 'figureTitle' , [figTitleBase '_{tr}']));
        h2 = drawConfusionDetailed(confTe, struct('figureID', {figIDsToUse(2)}, 'figureTitle' , [figTitleBase '_{te}']));
    else
        h1 = [];
        h2 = [];
    end
end

