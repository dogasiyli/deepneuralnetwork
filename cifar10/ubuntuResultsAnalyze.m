function [ analysis_struct ] = ubuntuResultsAnalyze( resultStruct )
% resultMatFileName will lod a resultStruct with fields:
%1. accM_tr = 45 by 45 matrix, rows(the model used;r1=m<1,2>), cols(the dataset used;c2=d<1,3>)
%2. accM_te = the test results
%3. accM_tr_cross = model(r1=m<1,2>) result on dataset(c2=d<1,3>)
%4. accM_te_cross = the test results
%5. accM_tr_base = 10 by 10 upper diagonal matrix showing non-elm classification accuracy of model<r,c>
%6. accM_te_base = the test result
    %load(resultMatFileName,'resultStruct');
    accM_te = resultStruct.accM_te;%45 by 45
    accM_te_cross = resultStruct.accM_te_cross;%45 by 45
    accM_te_base = resultStruct.accM_te_base;%10 by 10
% output files to generate
% 1. baseVsOthers - Base vs other results of 45 rows and 6 columns
%   <1> base
%   <2> ELM
%   <3> bestModelOtherMatch - ELM is applied on top
%   <4> bestModelOtherNoMatch - ELM is applied on top
%   <5> bestModelCrossMatch - models ELM is used
%   <6> bestModelCrossNoMatch - models ELM is used
% 2. baseVsOthersDetailed - Base vs other results of 45 rows and 14 columns
%   <1> base
%   <2> ELM
%   < 3, 4, 5> bestModelOtherMatch - ELM is applied on top
%   < 6, 7, 8> bestModelOtherNoMatch - ELM is applied on top
%   < 9,10,11> bestModelCrossMatch - models ELM is used
%   <12,13,14> bestModelCrossNoMatch - models ELM is used
% 3. bestModelOtherMatch - 45rx5c - rows are datasets, c(1,2) accuracies, c(3,4,5) model info, the best model can be used for classifying di-dj; where {di,dj} element of {mi,mj}
% 4. bestModelOtherNoMatch - 45rx5c - rows are datasets, c(1,2) accuracies, c(3,4,5) model info, the best model can be used for classifying di-dj; where {di,dj} not element of {mi,mj}
% 5. sortedModelMatch_Acc - 45rx16c - rows are datasets, columns are the accuracies sorted per dataset results where {di,dj} is element of {mi,mj}
% 6. sortedModelMatch_Ind - 45rx16c - rows are datasets, the model ids of sortedModelMatch_Acc
% 7. sortedModelNoMatch_Acc - 45rx28c - rows are datasets, columns are the accuracies sorted per dataset results where {di,dj} is NOT element of {mi,mj}
% 8. sortedModelNoMatch_Ind - 45rx28c - rows are datasets, the model ids of sortedModelNoMatch_Acc
% 9 crossModelMatch_Acc - 45rx16c - rows are datasets, columns are the accuracies sorted per dataset results where {di,dj} is element of {mi,mj}
%10. crossModelMatch_Ind - 45rx16c - rows are datasets, the model ids of sortedModelMatch_Acc
%11. crossModelNoMatch_Acc - 45rx28c - rows are datasets, columns are the accuracies sorted per dataset results where {di,dj} is NOT element of {mi,mj}
%12. crossModelNoMatch_Ind - 45rx28c - rows are datasets, the model ids of sortedModelNoMatch_Acc

    accComp_01_base_ELM = NaN(10,10);%<mir,mjc - base>_lower_left *** <dir,djc - ELM>_upper_right
    accComp_02_best_Match = NaN(10,10);%<mir,mjc - best>_lower_left *** <dir,djc - otherELM>_upper_right
    accComp_03_base_Cross = NaN(10,10);%<mir,mjc - best>_lower_left *** <dir,djc - cross>_upper_right

    baseVsOthers = NaN(45,6);
    baseVsOthersDetailed = NaN(45,14);

    sortedModelMatch_Acc = NaN(45,16);
    sortedModelMatch_Ind = NaN(45,16);
    sortedModelNoMatch_Acc = NaN(45,28);
    sortedModelNoMatch_Ind = NaN(45,28);

    crossModelMatch_Acc = NaN(45,16);
    crossModelMatch_Ind = NaN(45,16);
    crossModelNoMatch_Acc = NaN(45,28);
    crossModelNoMatch_Ind = NaN(45,28);
    
    modelIDMap = NaN(45,3);
    for di=1:9
        for dj=di+1:10
            dID = getModelID(di,dj);
            modelIDMap(dID,:) = [dID,di,dj];
        end
    end
    for di=1:9
        for dj=di+1:10
            %the dataset is taken as <di vs dj>
            dID = getModelID(di,dj);
            for mi=1:9
                for mj=mi+1:10
                    %the model is taken as <mi vs mj>
                    mID = getModelID(mi,mj);
                    
                    %here we will be dealing with the result of data
                    %classification accuracy of dID<di vs dj>
                    %using the model mID<mi vs mj>
                    acc_data_model_ELM = accM_te(mID,dID);
                    acc_data_model_cross = accM_te_cross(mID,dID);
                    
                    %does the data classes match any model classes
                    matchingClasses = length(unique([di,dj,mi,mj]));
                    if matchingClasses==2
                        assert(dID==mID,'these must be same')
                        acc_base = accM_te_base(di,dj);
                        acc_ELM = accM_te(dID,dID);
                        %<mir,mjc - base>_lower_left *** <dir,djc - ELM>_upper_right
                        accComp_01_base_ELM(dj,di) = acc_base;
                        accComp_01_base_ELM(di,dj) = acc_ELM;
                        accComp_02_best_Match(dj,di) = max(acc_base, acc_ELM);
                        accComp_03_base_Cross(dj,di) = max(acc_base, acc_ELM);                        
                        %we will fill the following matrix:
                    elseif matchingClasses==3
                        %at least 1 class match
                        %we will fill the following matrix:
                        % sortedModelMatch_Acc = NaN(dID,16);
                        % sortedModelMatch_Ind = NaN(dID,16);
                        % crossModelMatch_Acc = NaN(dID,16);
                        % crossModelMatch_Ind = NaN(dID,16);
                        colToFill_01 = find(isnan(sortedModelMatch_Acc(dID,:)),1,'first');
                        colToFill_02 = find(isnan(sortedModelMatch_Ind(dID,:)),1,'first');
                        colToFill_03 = find(isnan(crossModelMatch_Acc(dID,:)),1,'first');
                        colToFill_04 = find(isnan(crossModelMatch_Ind(dID,:)),1,'first');
                        assert(colToFill_01==colToFill_02,'these must be same')
                        assert(colToFill_03==colToFill_04,'these must be same')
                        
                        sortedModelMatch_Acc(dID,colToFill_01) = acc_data_model_ELM;
                        sortedModelMatch_Ind(dID,colToFill_02) = mID;
                        crossModelMatch_Acc(dID,colToFill_03) = acc_data_model_cross;
                        crossModelMatch_Ind(dID,colToFill_04) = mID;
                    elseif matchingClasses==4
                        %no classes match between dataset and model
                        %we will fill the following matrix:
                        % sortedModelNoMatch_Acc = NaN(dID,28);
                        % sortedModelNoMatch_Ind = NaN(dID,28);
                        % crossModelNoMatch_Acc = NaN(dID,28);
                        % crossModelNoMatch_Ind = NaN(dID,28);
                        colToFill_01 = find(isnan(sortedModelNoMatch_Acc(dID,:)),1,'first');
                        colToFill_02 = find(isnan(sortedModelNoMatch_Ind(dID,:)),1,'first');
                        colToFill_03 = find(isnan(crossModelNoMatch_Acc(dID,:)),1,'first');
                        colToFill_04 = find(isnan(crossModelNoMatch_Ind(dID,:)),1,'first');
                        assert(colToFill_01==colToFill_02,'these must be same')
                        assert(colToFill_03==colToFill_04,'these must be same')
                        
                        sortedModelNoMatch_Acc(dID,colToFill_01) = acc_data_model_ELM;
                        sortedModelNoMatch_Ind(dID,colToFill_02) = mID;
                        crossModelNoMatch_Acc(dID,colToFill_03) = acc_data_model_cross;
                        crossModelNoMatch_Ind(dID,colToFill_04) = mID;
                    else
                        error('we should not be here');
                    end
                end%mj=mi+1:10
            end%mi=1:9
            
            %so it is time to sort according to accuracy results
            [sortedModelMatch_Acc(dID,:), sortedModelMatch_Ind(dID,:)] = sortDataSetAccuracies(sortedModelMatch_Acc(dID,:), sortedModelMatch_Ind(dID,:));
            [sortedModelNoMatch_Acc(dID,:), sortedModelNoMatch_Ind(dID,:)] = sortDataSetAccuracies(sortedModelNoMatch_Acc(dID,:), sortedModelNoMatch_Ind(dID,:));
            [crossModelMatch_Acc(dID,:), crossModelMatch_Ind(dID,:)] = sortDataSetAccuracies(crossModelMatch_Acc(dID,:), crossModelMatch_Ind(dID,:));
            [crossModelNoMatch_Acc(dID,:), crossModelNoMatch_Ind(dID,:)] = sortDataSetAccuracies(crossModelNoMatch_Acc(dID,:), crossModelNoMatch_Ind(dID,:));
            
            %now that the results are sorted we can fill best results for dID by looking at the first cols of above matrixes dID rows
            baseVsOthers(dID,1:6) = [acc_base, acc_ELM, sortedModelMatch_Acc(dID,1), sortedModelNoMatch_Acc(dID,1), crossModelMatch_Acc(dID,1), crossModelNoMatch_Acc(dID,1)];        
            %   <1> base
            %   <2> ELM
            %   <3> bestModelOtherMatch - ELM is applied on top
            %   <4> bestModelOtherNoMatch - ELM is applied on top
            %   <5> bestModelCrossMatch - models ELM is used
            %   <6> bestModelCrossNoMatch - models ELM is used
            baseVsOthersDetailed(dID,1:14) = [  acc_base, acc_ELM...1,2
                                              , sortedModelMatch_Acc(dID,1)   , modelIDMap(sortedModelMatch_Ind(dID,1),2:3)...   3, 4, 5
                                              , sortedModelNoMatch_Acc(dID,1) , modelIDMap(sortedModelNoMatch_Ind(dID,1),2:3)... 6, 7, 8
                                              , crossModelMatch_Acc(dID,1)    , modelIDMap(crossModelMatch_Ind(dID,1),2:3)...    9,10,11
                                              , crossModelNoMatch_Acc(dID,1)  , modelIDMap(crossModelNoMatch_Ind(dID,1),2:3)];% 12,13,14  
            %   <1> base
            %   <2> ELM
            %   < 3, 4, 5> bestModelOtherMatch - ELM is applied on top
            %   < 6, 7, 8> bestModelOtherNoMatch - ELM is applied on top
            %   < 9,10,11> bestModelCrossMatch - models ELM is used
            %   <12,13,14> bestModelCrossNoMatch - models ELM is used             
            accComp_02_best_Match(di,dj) = max(sortedModelMatch_Acc(dID,1), sortedModelNoMatch_Acc(dID,1));
            accComp_03_base_Cross(di,dj) = max(crossModelMatch_Acc(dID,1), crossModelNoMatch_Acc(dID,1));
        end
    end
        
    analysis_struct = struct;
    analysis_struct.accComp_01_base_ELM = accComp_01_base_ELM;
    analysis_struct.accComp_02_best_Match = accComp_02_best_Match;
    analysis_struct.accComp_03_base_Cross = accComp_03_base_Cross;
    
    analysis_struct.baseVsOthers = baseVsOthers;
    analysis_struct.baseVsOthersDetailed = baseVsOthersDetailed;
    
    analysis_struct.sortedModelMatch_Acc = sortedModelMatch_Acc;
    analysis_struct.sortedModelMatch_Ind = sortedModelMatch_Ind;
    analysis_struct.sortedModelNoMatch_Acc = sortedModelNoMatch_Acc;
    analysis_struct.sortedModelNoMatch_Ind = sortedModelNoMatch_Ind;
    
    analysis_struct.crossModelMatch_Acc = crossModelMatch_Acc;
    analysis_struct.crossModelMatch_Ind = crossModelMatch_Ind;
    analysis_struct.crossModelNoMatch_Acc = crossModelNoMatch_Acc;
    analysis_struct.crossModelNoMatch_Ind = crossModelNoMatch_Ind;
    
    analysis_struct.accM_te = accM_te;
    analysis_struct.accM_te_cross = accM_te_cross;
    analysis_struct.accM_te_base = accM_te_base;
    analysis_struct.modelIDMap = modelIDMap;
end

function [accRow, indsRow] = sortDataSetAccuracies(accRow, indsRow)
    [accRow, inds] = sort(abs(accRow),'descend');
    indsRow = indsRow(inds);
end

function mId = getModelID(mi,mj)
    mId = 0;
    for i = 1:9
        for j = i+1 : 10
            mId = mId + 1;
            if mi==i && mj==j
                return
            end
        end
    end
end
