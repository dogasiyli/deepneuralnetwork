function distMat = changeDistMatForSpectralClustering(distMat, inputMinMax)
    %we need to change the range of the values from 0 to 1
    [r, c] = size(distMat);
    if isempty(distMat)
        return
    elseif (r~=c)
        return
    end
    N = r;
    clear r c;
    
    D_rightUpperTriangle = triu(ones(N),+1);
    distanceValues = distMat(D_rightUpperTriangle==1);
    if exist('inputMinMax','var')
        minVal = min(distanceValues);
        maxVal = max(distanceValues);
        if inputMinMax(1)>minVal
            valuesLessThanDefinedMinVal = distanceValues<inputMinMax(1);
            disp([ num2str(sum(valuesLessThanDefinedMinVal==1)) ' of values are less than defined min value - and it is corrected']);
            distanceValues(valuesLessThanDefinedMinVal) = inputMinMax(1);
        end
        if inputMinMax(2)<maxVal
            valuesBiggerThanDefinedMaxVal = distanceValues>inputMinMax(2);
            disp([ num2str(sum(valuesBiggerThanDefinedMaxVal==1)) ' of values are biiger than defined max value - and it is corrected']);
            distanceValues(valuesBiggerThanDefinedMaxVal) = inputMinMax(2);
        end
        distanceValues = [distanceValues;inputMinMax(:)];
    end
    distanceValuesNew = map2_a_b(distanceValues,0,1);
    if exist('inputMinMax','var')
        distanceValuesNew = distanceValuesNew(1:end-2);
    end
    
    distMat(D_rightUpperTriangle==1) = distanceValuesNew; 
end