function [fileNameList, predictedLabels, labelsMat] = clusterFolderOfKHS(sourceFolder, optParamStruct)
    %OPS_clusterFolderOfKHS = struct('hogVersion', 2);
    %srcFold_clusterFolderOfKHS = '/home/dg/Desktop/clusterTestFolder';
    if ~exist('optParamStruct', 'var') || ~isstruct(optParamStruct)
        optParamStruct = [];
    end
    kVec       	        = getStructField(optParamStruct, 'kVec', [6 8 10 12]);
    clusteringParams    = getStructField(optParamStruct, 'clusteringParams', struct('tryMax',400,'kVec',kVec,'gtLabelsGrouping',[],'modeStr','kMedoids','groupClusterLabels',false));
    hogVersion       	= getStructField(optParamStruct, 'hogVersion', 2);
        
    folderNames = getFolderList(sourceFolder, false, false);
    labelCount = length(folderNames);
    hogImArr_cell = [];
    fileNameList = [];
    labelNameCells = [];
    labelsMat = [];
    for i = 1:labelCount
        khsFolderName = folderNames{i};
        [hogImArr_cell_khsFold, fileNameListCur, imcropVec_struct, strideFixed]= step01(sourceFolder, khsFolderName, hogVersion);
       
        labelsMat = [labelsMat;i*ones(length(fileNameListCur),1)]; %#ok<AGROW>
        labelNameCells = [labelNameCells;{khsFolderName}]; %#ok<AGROW>
        hogImArr_cell = [hogImArr_cell;hogImArr_cell_khsFold]; %#ok<AGROW>
        fileNameList = [fileNameList;fileNameListCur]; %#ok<AGROW>
        clear fileNameListCur khsFolderName
    end
    
    if hogVersion==2
        hogImArr_cell = step01_extrapolateHogV2(sourceFolder, hogImArr_cell, imcropVec_struct, strideFixed, fileNameList);
    end
    
    
    %for each key hand shape 
    %it will create 10 centroid key hand shapes
    %then it will save them as keyHandShapeCentroids
    %for creating such centroids
    %I need to ceate the dist mat per handshape
    for i = 1:labelCount
        labelName = labelNameCells{i};
        step02_fileName_label = [sourceFolder filesep labelName filesep 'step02_' labelName '_distmat_hv' num2str(hogVersion,'%d') '.mat'];
        reCalculateDist = true;
        relatedRows = [];
        if exist(step02_fileName_label,'file')
            disp(['loading distMat hog from ' step02_fileName_label]);
            load(step02_fileName_label,'distMat','relatedRows');
            reCalculateDist = false;
        end

        relatedRowsCur = find(labelsMat==i);
        reCalculateDist = reCalculateDist | length(relatedRowsCur)~=length(relatedRows);
        if (reCalculateDist==false), reCalculateDist = sum(relatedRowsCur(:)-relatedRows(:))~=0; end
        
        if reCalculateDist
            fileCount = length(relatedRowsCur); 
            difCntTotal = fileCount*(fileCount-1);
            distMat = NaN(fileCount,fileCount);
            disp([labelName '-calculating ' num2str(difCntTotal,'%d') ' hog distances with version ' num2str(hogVersion,'%d')]);
            disp(datestr(now))
            dispstat('','init');
            completedDifCnt = 0;
            tic;
            for li=1:fileCount-1
                row_i = relatedRowsCur(li);
                for rj=li+1:fileCount
                    row_j = relatedRowsCur(rj);
                    switch hogVersion
                        case 1
                            minDistVal_01 = calcDistofHogs(hogImArr_cell{row_i,1}, hogImArr_cell{row_j,1}, 1);
                            distMat(li,rj) = minDistVal_01;
                        case 2
                            minDistVal = pdist_hogV2(hogImArr_cell{row_i,1}, hogImArr_cell{row_j,1});
                            distMat(li,rj) = minDistVal;
                    end
                    completedDifCnt = completedDifCnt + 1;
                    if mod(completedDifCnt,1000)==0
                        dispstat(sprintf('%d of %d completed',completedDifCnt,difCntTotal),'timestamp');
                    end
                end
            end
            toc;
            disp(datestr(now))
            disp([labelName '-calculated ' num2str(difCntTotal,'%d') ' hog distances with version ' num2str(hogVersion,'%d')]);
            relatedRows = relatedRowsCur;
            save(step02_fileName_label,'distMat','relatedRows');
        end 
        
        clear relatedRowsCur labelName reCalculateDist relatedRows;
    end    

    %for each key hand shape 
    %it will create 10 centroid key hand shapes
    %then it will save them as keyHandShapeCentroids
    %for creating such centroids
    %I need to ceate the dist mat per handshape
    
    khsCentroids = [];
    for i = 1:labelCount
        labelName = labelNameCells{i};
        
        step02_fileName_label = [sourceFolder filesep labelName filesep 'step02_' labelName '_distmat_hv' num2str(hogVersion,'%d') '.mat'];
        load(step02_fileName_label,'distMat','relatedRows');
        
        step03_fileName_label = [sourceFolder filesep labelName filesep 'step03_' labelName '_medoidHist_hv' num2str(hogVersion,'%d') '.mat'];
        reCalculateMedoids = true;
        
        if exist(step03_fileName_label,'file')
            disp(['loading medoidHist_Best and clusterLabelsCell of hogVersion(' num2str(hogVersion,'%d') ') from ' step03_fileName_label]);
            load(step03_fileName_label,'medoidHist_Best','clusterLabelsCell');
            reCalculateMedoids = false;
        end

        if reCalculateMedoids
            disp(['starting clustering ' labelName 'hogV(' num2str(hogVersion,'%d') ')']);
            disp(datestr(now))
            tic;
            [medoidHist_Best, clusterLabelsCell] = run_kmedoidsDG_Loop(distMat, clusteringParams);
            save(step03_fileName_label,'medoidHist_Best','clusterLabelsCell');
            toc; 
            disp(datestr(now))
            disp(['finished clustering ' labelName 'hogV(' num2str(hogVersion,'%d') ')']);
        end
        
        medoidHist_Best = medoidHist_Best(clusteringParams.kVec,:);
        medoidHistResults = [medoidHist_Best(:,end) medoidHist_Best(:,end)./clusteringParams.kVec(:)];
        
        [~, selectedMedoidRow] = min(medoidHistResults(:,2));
        selectedMedoidClusterCnt = clusteringParams.kVec(selectedMedoidRow);
        selectedMedoidIDs = medoidHist_Best(selectedMedoidRow,(selectedMedoidClusterCnt-max(clusteringParams.kVec))+1 : end-1); 
        assignedLabels = clusterLabelsCell{selectedMedoidClusterCnt};
        distMat = makeDistMatFull(distMat);
        %selectedDistMatPart = distMat(selectedMedoidIDs,selectedMedoidIDs);
        
        figure(22);clf;
        for f=1:length(kVec)
            subplot(1,length(kVec),f);
            hist(clusterLabelsCell{1,kVec(f)},1:kVec(f));
            xlim([0.5 kVec(f)+0.5]);
        end        

        khsCentroids_cur = cell(selectedMedoidClusterCnt,5);
        for k=1:selectedMedoidClusterCnt
            sampleID = selectedMedoidIDs(k);
            sampleID_rowAll = relatedRows(sampleID);
            khsCentroids_cur{k,1} = hogImArr_cell{sampleID_rowAll};
            
            fNameOfSample = fileNameList{sampleID_rowAll};
            [signID, userID, repID, handID, frameID] = getDetailedLabelsFromFileName(fNameOfSample);
            
            khsCentroids_cur{k,2} = [signID, userID, repID, handID, frameID, i];
            
            maxDistAssigned = distMat(sampleID,assignedLabels==k);
            khsCentroids_cur{k,3} = max(maxDistAssigned);
            khsCentroids_cur{k,4} = max(length(maxDistAssigned));
            khsCentroids_cur{k,5} = labelNameCells{i};
        end
        khsCentroids = [khsCentroids;khsCentroids_cur]; %#ok<AGROW>
        clear relatedRows medoidHist_Best clusterLabelsCell distMat f assignedLabels fNameOfSample 
        clear signID userID repID handID frameID 
        clear i k  sampleID sampleID_rowAll
        clear khsCentroids_cur labelName maxDistAssigned medoidHistResults reCalculateMedoids selectedMedoidIDs;
    end    

    fileCount = length(hogImArr_cell);
    predictedLabels = zeros(fileCount,4);
    khsCnt = length(khsCentroids);
    dispstat('','init');
    for f=1:fileCount
        hogImCur = hogImArr_cell{f};
        fName = fileNameList{f};
        distToKHS = zeros(khsCnt,4);
        for khsID = 1:khsCnt
            switch hogVersion
                case 1
                    minDistVal_01 = calcDistofHogs(hogImCur, khsCentroids{khsID,1}, 1);
                    distToKHS(khsID,:) = [minDistVal_01 khsCentroids{khsID,3} minDistVal_01/khsCentroids{khsID,3} labelsMat(f)];
                case 2
                    minDistVal = pdist_hogV2(hogImCur, khsCentroids{khsID,1});
                    distToKHS(khsID,:) = [minDistVal khsCentroids{khsID,3} minDistVal/khsCentroids{khsID,3} labelsMat(f)];
            end   
            accCur = 100*sum(predictedLabels(1:f,4)==labelsMat(1:f))/f;
            dispstat(sprintf('%d of %d - %4.2f acc\n', f, fileCount, accCur));
        end
        [d,idx] = sort(distToKHS(:,3));
        distToKHS = distToKHS(idx,:);
        predictedLabels(f,:)= distToKHS(1,:);
    end
    %find(not(cellfun('isempty', strfind(labelNameCells, khsCentroids{khsID,5}))))
%     
%     clusteringParams = struct('gtLabelsGrouping',[],'groupClusterLabels',true, 'kVec', kVec);
%     OPS_displayClusterOfPngs = struct('figID',1,'gtLabelsGrouping', [], 'clusteringParams', clusteringParams);
%     [hSummary, confAccBest] = displayClusterOfPngs(sourceFolder, distMat, imageLabels, medoidHist_Best, clusterLabelsCell, OPS_displayClusterOfPngs);
end


function [signID, userID, repID, handID, frameID] = getDetailedLabelsFromFileName(fNameOfSample)
    fNameSplitted = split(fNameOfSample,{'_','.png'});
    signID = str2double(fNameSplitted{1}(2:end));
    userID = str2double(fNameSplitted{2}(2:end));
    repID = str2double(fNameSplitted{3}(2:end));
    handStr = fNameSplitted{4};
    frameID = str2double(fNameSplitted{5});
    switch(handStr)
        case 'LH'
            handID = 1;
        case 'RH'
            handID = 2;
        case 'BH'
            handID = 3;
    end
end

function [hogImArr_cell, fileNameList, imcropVec_struct, strideFixed, imcropVec_cell] = step01(sourceFolder, khsFolderName, hogVersion)
    srcFold_khs = [sourceFolder filesep khsFolderName];
    fileNameList = getFileList(srcFold_khs,'.png','',false);
    fileNameList = sort(fileNameList);
    fileCount = length(fileNameList); 
    
    hogStruct = struct('blockCounts',[10 10], 'histogramBinCount', 9);
    handInfo = struct('handDominant', 'LH', 'handCur', 'LH');

    hogImArr_cell = cell(fileCount,1);
    switch hogVersion
        case 1
            scales2Use = 1:-0.10:0.70;
            strideFixed = 5;
            imcropVec_cell = cell(fileCount,1);
            imcropVec_struct = [];
        case 2
            strideFixed = 1;
            blockCountsVec = [10 11 12];
            rotDegrees = [-30 -20 -10 0 10 20 30];
            imcropVec_cell = [];

            imcropVec_struct = struct;
            imcropVec_struct.rotDegrees = rotDegrees;
            imcropVec_struct.blockCountsVec = blockCountsVec;
            imcropVec_struct.hogStruct = hogStruct;
    end  
    
    step01_fileName = [srcFold_khs filesep 'step01_' khsFolderName '_hv' num2str(hogVersion,'%d') '.mat'];
    reExtractHOG = true;
    if exist(step01_fileName,'file')
        disp(['loading extracted hog from ' step01_fileName]);
        load(step01_fileName,'hogImArr_cell','imcropVec_cell','imcropVec_struct','fileNameList');
        reExtractHOG = false;
    end
    
    reExtractHOG = reExtractHOG | fileCount~=length(fileNameList);
    
    if reExtractHOG
        disp(['extracting hog of ' num2str(fileCount,'%d') ' files from folder(' khsFolderName ') with version ' num2str(hogVersion,'%d')]);
        disp(datestr(now))
        tic;
        for f=1:fileCount
            fileName = fileNameList{f};
            a = imread([srcFold_khs filesep fileName]);
            handInfo = getHandInfoFromFileName(fileName, handInfo);
            switch hogVersion
                case 1
                    [hogImArr_cell{f,1}, imcropVec_cell{f,1}] = extrapolateImForHOGComp(a, scales2Use, strideFixed, hogStruct, handInfo);
                case 2
                    hogImArr_cell{f,1} = extrapolateImForHOGComp_v2(a, rotDegrees, blockCountsVec, hogStruct, handInfo);
                    %655 --> 49.068028 seconds
                    %662 --> 47.604788 seconds.
            end
        end    
        toc;
        disp(datestr(now))
        disp(['extracted hog of ' num2str(fileCount,'%d') ' files from folder(' khsFolderName ') with version ' num2str(hogVersion,'%d')]);
        save(step01_fileName,'hogImArr_cell','imcropVec_cell','imcropVec_struct','fileNameList', 'strideFixed');
    end
end

function hogImArr_cell = step01_extrapolateHogV2(srcFold, hogImArr_cell, imcropVec_struct, strideFixed, fileNameList) %#ok<INUSD>
    fileCount = length(hogImArr_cell);
    step01_extrapolateHogV2_fileName = [srcFold filesep 'step01_hv2_ext.mat'];
    reCreateMat = true;
    if exist(step01_extrapolateHogV2_fileName,'file')
        load(step01_extrapolateHogV2_fileName,'hogImArr_cell', 'imcropVec_struct','fileNameList', 'strideFixed');
        reCreateMat = false;
    end  
    
    reCreateMat = reCreateMat | fileCount~=length(hogImArr_cell);
    if reCreateMat
        disp(datestr(now))
        tic;
        dispstat('','init');
        dispstat(sprintf('extrapolateHogV2 hog of %d files with version 2',fileCount),'timestamp','keepthis');
        for f=1:fileCount
            hogImArr_cell{f,1} = extrapolateHogV2(hogImArr_cell{f,1}, imcropVec_struct, strideFixed);
            if mod(f,10)==0
                dispstat(sprintf('%d of %d completed',f,fileCount),'timestamp');
            end
        end  
        toc;
        disp(datestr(now))
        dispstat(sprintf('extrapolated hog of %d files with version 2',fileCount),'timestamp','keepthis');
        save(step01_extrapolateHogV2_fileName,'hogImArr_cell', 'imcropVec_struct', 'strideFixed','fileNameList','-v7.3');
    end
end

function handInfo = getHandInfoFromFileName(fileName, handInfo)
    if contains(fileName, '_LH_')
        handInfo.handCur = 'LH';
    elseif contains(fileName, '_RH_')
        handInfo.handCur = 'RH';
    elseif contains(fileName, '_BH_')
        handInfo.handCur = 'BH';
    end
end