function [fileNameList, distMat, medoidHist_Best, clusterLabelsCell, hSummary, confAccBest] = clusterFolderOfPngs(folderWithImages, optParamStruct)
    if ~exist('optParamStruct', 'var') || ~isstruct(optParamStruct)
        optParamStruct = [];
    end
    kVec       	        = getStructField(optParamStruct, 'kVec', [10 12 15 20]);
    clusteringParams    = getStructField(optParamStruct, 'clusteringParams', struct('tryMax',3000,'kVec',kVec,'gtLabelsGrouping',[],'modeStr','kMedoids','groupClusterLabels',false));
    hogVersion       	= getStructField(optParamStruct, 'hogVersion', 2);
    imageLabels       	= getStructField(optParamStruct, 'imageLabels', []);

    if isempty(imageLabels)
        load([folderWithImages filesep 'labelsOfFolder.mat']);
        imageLabels = labelsOfFolder;
        clear labelsOfFolder
    end

    hogStruct = struct('blockCounts',[10 10], 'histogramBinCount', 9);
    handInfo = struct('handDominant', 'LH', 'handCur', 'LH');

    fileNameList = getFileList(folderWithImages,'.png','',false);
    fileCount = length(fileNameList);
    
    hogImArr_cell = cell(fileCount,1);
    switch hogVersion
        case 1
            scales2Use = 1:-0.10:0.70;
            strideFixed = 5;
            imcropVec_cell = cell(fileCount,1);
            imcropVec_struct = [];
        case 2
            strideFixed = 1;
            blockCountsVec = [10 11 12];
            rotDegrees = [-30 -20 -10 0 10 20 30];
            imcropVec_cell = [];

            imcropVec_struct = struct;
            imcropVec_struct.rotDegrees = rotDegrees;
            imcropVec_struct.blockCountsVec = blockCountsVec;
            imcropVec_struct.hogStruct = hogStruct;
    end
    
    step01_fileName = [folderWithImages filesep 'step01_hv' num2str(hogVersion,'%d') '.mat'];
    if exist(step01_fileName,'file')
        disp(['loading extracted hog from ' step01_fileName]);
        load(step01_fileName);
    else
        disp(['extracting hog of ' num2str(fileCount,'%d') ' files with version ' num2str(hogVersion,'%d')]);
        disp(datestr(now))
        tic;
        for f=1:fileCount
            fileName = fileNameList{f};
            a = imread([folderWithImages filesep fileName]);
            handInfo = getHandInfoFromFileName(fileName, handInfo);
            switch hogVersion
                case 1
                    [hogImArr_cell{f,1}, imcropVec_cell{f,1}] = extrapolateImForHOGComp(a, scales2Use, strideFixed, hogStruct, handInfo);
                case 2
                    hogImArr_cell{f,1} = extrapolateImForHOGComp_v2(a, rotDegrees, blockCountsVec, hogStruct, handInfo);
                    hogImArr_cell{f,1} = extrapolateHogV2(hogImArr_cell{f,1}, imcropVec_struct, strideFixed);
            end
        end    
        toc;
        disp(datestr(now))
        disp(['extracted hog of ' num2str(fileCount,'%d') ' files with version ' num2str(hogVersion,'%d')]);
        save(step01_fileName,'hogImArr_cell','imcropVec_cell','imcropVec_struct');
    end
    
    distMat = NaN(fileCount,fileCount);
    
    step02_fileName = [folderWithImages filesep 'step02_hv' num2str(hogVersion,'%d') '.mat'];
    if exist(step02_fileName,'file')
        disp(['loading distMat hog from ' step02_fileName]);
        load(step02_fileName,'distMat');
    else
        disp(['calculating ' num2str(fileCount*(fileCount-1),'%d') ' hog distances with version ' num2str(hogVersion,'%d')]);
        disp(datestr(now))
        tic;
        for li=1:fileCount-1
            for rj=li+1:fileCount
                switch hogVersion
                    case 1
                        minDistVal_01 = calcDistofHogs(hogImArr_cell{li,1}, hogImArr_cell{rj,1}, 1);
                        distMat(li,rj) = minDistVal_01;
                    case 2
                        minDistVal = pdist_hogV2(hogImArr_cell{li,1}, hogImArr_cell{rj,1});
                        distMat(li,rj) = minDistVal;
                end
            end
        end
        toc;
        disp(datestr(now))
        disp(['calculated ' num2str(fileCount*(fileCount-1),'%d') ' hog distances with version ' num2str(hogVersion,'%d')]);
        save(step02_fileName,'distMat');
    end
     
    step03_fileName = [folderWithImages filesep 'step03_hv' num2str(hogVersion,'%d') '.mat'];
    if exist(step03_fileName,'file')
        disp(['loading medoidHist_Best and clusterLabelsCell from ' step03_fileName]);
        load(step03_fileName,'medoidHist_Best','clusterLabelsCell');
    else
        [medoidHist_Best, clusterLabelsCell] = run_kmedoidsDG_Loop(distMat, clusteringParams);
        save(step03_fileName,'medoidHist_Best','clusterLabelsCell');
    end    
    
    figure(22);clf;
    for i=1:length(kVec)
        subplot(1,length(kVec),i);
        hist(clusterLabelsCell{1,kVec(i)},1:kVec(i));
        xlim([0.5 kVec(i)+0.5]);
    end
    
    clusteringParams = struct('gtLabelsGrouping',[],'groupClusterLabels',true, 'kVec', kVec);
    OPS_displayClusterOfPngs = struct('figID',1,'gtLabelsGrouping', [], 'clusteringParams', clusteringParams);
    [hSummary, confAccBest] = displayClusterOfPngs(folderWithImages, distMat, imageLabels, medoidHist_Best, clusterLabelsCell, OPS_displayClusterOfPngs);
end

function handInfo = getHandInfoFromFileName(fileName, handInfo)
    if contains(fileName, '_LH_')
        handInfo.handCur = 'LH';
    elseif contains(fileName, '_RH_')
        handInfo.handCur = 'RH';
    elseif contains(fileName, '_BH_')
        handInfo.handCur = 'BH';
    end
end