function [h2, imComb_imPalette_curCluster] = createCollageImForAKluster(k, j, confAccBestVec, srcFold, handsStruct, sortedClusterLabels, sampleIndsVec_best, uniqueMappingLabels, newKlusterCell, medoidIDsOdCluster, randomColors, imCollage_imSizeMax, figID)
    surroundWithColorStruct = struct('thicknessParam',0.2,'colorParam',[0 0 0]);
    k_im = newKlusterCell{j};
    clusterGroupCnt = length(k_im);
    %all medoid IDs are in medoidIDsOdCluster
    %grab the medoids that belong to "class j" into im_k
    im_k = medoidIDsOdCluster(k_im);
    %labels of "class j" medoids will be put in labelID_medoidCenter
    labelID_medoidCenter = handsStruct.detailedLabels(im_k,6);
    if clusterGroupCnt==1
        im01_medoidIm = retrieveImageFromDetailedLabels(srcFold, im_k, handsStruct.detailedLabels);
        surroundWithColorStruct.colorParam = randomColors(labelID_medoidCenter,:);
        im01_medoidIm = putImg_IntoBox(im01_medoidIm, int32((1+surroundWithColorStruct.thicknessParam)*[size(im01_medoidIm,1) size(im01_medoidIm,2)]), true, surroundWithColorStruct);
    else
        [im01_medoidIm, paletteRCSize_medoidIm] = imCollageFuncs( 'createPalette', struct('imCnt',clusterGroupCnt,'channelCnt',3,'imSizeMax',3*imCollage_imSizeMax));
        for cToAdd = 1:clusterGroupCnt
            imageToAdd = retrieveImageFromDetailedLabels(srcFold, im_k(cToAdd), handsStruct.detailedLabels);
            labelToAdd = handsStruct.detailedLabels(im_k(cToAdd),6);
            surroundWithColorStruct.colorParam = randomColors(labelToAdd,:);
            if isempty(imageToAdd)
                continue
            end                
            im01_medoidIm = imCollageFuncs('insertIntoPalette', struct('imageID',cToAdd,'paletteRCSize',paletteRCSize_medoidIm,'imPalette',im01_medoidIm,'imageToAdd',imageToAdd,'boxSize',3*imCollage_imSizeMax, 'surroundWithColorStruct', surroundWithColorStruct));
        end                
    end

    allImIDs_curCluster = find(ismember(sortedClusterLabels,j));
    positionOfClusterMedoids = find(ismember(sampleIndsVec_best(allImIDs_curCluster),im_k));
    if (length(positionOfClusterMedoids)~=clusterGroupCnt)
        warning(['length(positionOfClusterMedoids) [' mat2str(positionOfClusterMedoids) ' != ' num2str(clusterGroupCnt)  '] clusterGroupCnt']);
    end
    allImIDs_curCluster(positionOfClusterMedoids) = [];
    allImIDs_curCluster = sampleIndsVec_best(allImIDs_curCluster);
    imCnt_collage = length(allImIDs_curCluster);

    labelsOfSamples = handsStruct.detailedLabels(allImIDs_curCluster,6);
    uniqLabelsAssigned = unique(labelsOfSamples);
    uniqLabelCnt = length(uniqLabelsAssigned);
    [im02_imPalette_distinctLabels, paletteRCSize_distincLabels] = imCollageFuncs( 'createPalette', struct('imCnt',uniqLabelCnt,'channelCnt',3,'imSizeMax',3*imCollage_imSizeMax));
    for labelToAdd = 1:uniqLabelCnt
        %find a sample of the label - if possible the one closest
        %to the 
        labelIDCur = uniqLabelsAssigned(labelToAdd);
        bestLabelHolderSampleID = find(labelsOfSamples==labelIDCur,1);
        bestLabelHolderImageID = allImIDs_curCluster(bestLabelHolderSampleID);

        surroundWithColorStruct.colorParam = randomColors(labelIDCur,:);

        imageToAdd = retrieveImageFromDetailedLabels(srcFold, bestLabelHolderImageID, handsStruct.detailedLabels);
        if isempty(imageToAdd)
            continue
        end                
        im02_imPalette_distinctLabels = imCollageFuncs('insertIntoPalette', struct('imageID',labelToAdd,'paletteRCSize',paletteRCSize_distincLabels,'imPalette',im02_imPalette_distinctLabels,'imageToAdd',imageToAdd,'boxSize',3*imCollage_imSizeMax, 'surroundWithColorStruct', surroundWithColorStruct));
    end
    [im04_imPalette_labelProbs, paletteRCSize_distincLabels] = imCollageFuncs( 'createPalette', struct('imCnt',uniqLabelCnt,'channelCnt',3,'imSizeMax',3*imCollage_imSizeMax));
    for labelToAdd = 1:uniqLabelCnt
        %find a sample of the label - if possible the one closest
        %to the 
        labelIDCur = uniqLabelsAssigned(labelToAdd);
        thisClusterHas = sum(labelsOfSamples==labelIDCur) + double(sum(ismember(labelID_medoidCenter,labelIDCur)));                
        totalOfLabelCur = sum(handsStruct.detailedLabels(:,6)==labelIDCur);
        remainLabelsCount = totalOfLabelCur - thisClusterHas;

        surroundWithColorStruct.colorParam = randomColors(labelIDCur,:);
        surroundWithColorStruct.thicknessParam = remainLabelsCount/totalOfLabelCur;

        probVal = 0.5 + 0.5*(1-surroundWithColorStruct.thicknessParam);
        rowSize = floor(imCollage_imSizeMax(1)*probVal);
        colSize = floor(imCollage_imSizeMax(2)*probVal);

        imageToAdd1 = getNumberImage(thisClusterHas, rowSize, colSize, imCollage_imSizeMax);
        imageToAdd2 = getNumberImage(totalOfLabelCur, rowSize, colSize, imCollage_imSizeMax);
        imageToAdd = [imageToAdd1 zeros(size(imageToAdd1)) imageToAdd2];
        imageToAdd = cat(3, imageToAdd, imageToAdd, imageToAdd);
        im04_imPalette_labelProbs = imCollageFuncs('insertIntoPalette', struct('imageID',labelToAdd,'paletteRCSize',paletteRCSize_distincLabels,'imPalette',im04_imPalette_labelProbs,'imageToAdd',imageToAdd,'boxSize',3*imCollage_imSizeMax, 'surroundWithColorStruct', surroundWithColorStruct));
    end

    surroundWithColorStruct.thicknessParam = 0.1;
    [im03_imPalette_curCluster, paletteRCSize_curCluster] = imCollageFuncs( 'createPalette', struct('imCnt',imCnt_collage,'channelCnt',3,'imSizeMax',imCollage_imSizeMax));
    for imageIDToAdd = 1:length(allImIDs_curCluster)
        eachClusterIm_i = allImIDs_curCluster(imageIDToAdd);
        imageToAdd = retrieveImageFromDetailedLabels(srcFold, eachClusterIm_i, handsStruct.detailedLabels);
        if isempty(imageToAdd)
            continue
        end
        labelIDCur = handsStruct.detailedLabels(eachClusterIm_i,6);
        surroundWithColorStruct.colorParam = randomColors(labelIDCur,:);
        im03_imPalette_curCluster = imCollageFuncs('insertIntoPalette', struct('imageID',imageIDToAdd,'paletteRCSize',paletteRCSize_curCluster,'imPalette',im03_imPalette_curCluster,'imageToAdd',imageToAdd,'boxSize',imCollage_imSizeMax, 'surroundWithColorStruct', surroundWithColorStruct));
    end

    %I need 1 sample from all assigned clusters            
    [imComb_imPalette_curCluster, imComb_paletteRCSize_curCluster] = imCollageFuncs( 'createPalette', struct('imCnt',3,'channelCnt',3,'imSizeMax',8*imCollage_imSizeMax));
    if length(labelID_medoidCenter)==1
        surroundWithColorStruct.colorParam = 1-randomColors(labelID_medoidCenter,:);
    else
        surroundWithColorStruct.colorParam = 1-mean(randomColors(labelID_medoidCenter,:));
    end
    imComb_imPalette_curCluster = imCollageFuncs('insertIntoPalette', struct('imageID',1,'paletteRCSize',imComb_paletteRCSize_curCluster,'imPalette',imComb_imPalette_curCluster,'imageToAdd',im01_medoidIm,'boxSize',8*imCollage_imSizeMax,'surroundWithColorStruct',surroundWithColorStruct));
    imComb_imPalette_curCluster = imCollageFuncs('insertIntoPalette', struct('imageID',2,'paletteRCSize',imComb_paletteRCSize_curCluster,'imPalette',imComb_imPalette_curCluster,'imageToAdd',im02_imPalette_distinctLabels,'boxSize',8*imCollage_imSizeMax));
    imComb_imPalette_curCluster = imCollageFuncs('insertIntoPalette', struct('imageID',3,'paletteRCSize',imComb_paletteRCSize_curCluster,'imPalette',imComb_imPalette_curCluster,'imageToAdd',im03_imPalette_curCluster,'boxSize',8*imCollage_imSizeMax));
    imComb_imPalette_curCluster = imCollageFuncs('insertIntoPalette', struct('imageID',4,'paletteRCSize',imComb_paletteRCSize_curCluster,'imPalette',imComb_imPalette_curCluster,'imageToAdd',im04_imPalette_labelProbs,'boxSize',8*imCollage_imSizeMax));

    h2 = figure(figID+2);
    clf;
    %hold on;
    %subplot(rC, cC, sub2ind([cC,rC],k_im,k-1));%row:k-1, col-im
    %cl_str = ['<' num2str(clusterLabels_med1(im)) ',' num2str(clusterLabels_med2(im)) '>'];
    %usr_str = ['s(' num2str(dLab_k(1)) ')-u(' num2str(dLab_k(2)) '-' num2str(dLab_k(3))];
    accStr = [',acc(' mat2str(confAccBestVec(k), 5) ')'];
    %subplot(1,2,1);
    image(imComb_imPalette_curCluster);

    if length(newKlusterCell)==k
        kStr = ['k(' num2str(j) ' of ' num2str(k) '), kCombined[' strrep(strrep(mat2str(k_im),'[',''),']','') ']'];
    else
        kStr = ['k(' num2str(j) ' of ' num2str(length(newKlusterCell)) '<' num2str(k) '), kCombined[' strrep(strrep(mat2str(k_im),'[',''),']','') ']'];
    end
    %gv = find(ismember(clusterLabelsGroupingVec,j));
    %gv2 = find(ismember(gtLabelsGrouping,gv));
    bestAssignedLabels = find(ismember(uniqueMappingLabels(:,3),j));
    %assert(sum(gv2(:)-bestAssignedLabels(:))==0,'this must hold')
    %if handsStruct_detailedLabels_colCnt==6
    %    gv_str = [',g(' mat2str(gv2) ')'];
    %else
    %    gv_str = '';
    %end
    title([kStr accStr ',labels(' mat2str(uniqLabelsAssigned) '),bestAssignedLabels(' mat2str(bestAssignedLabels) ')']);
    %title([usr_str '_{' strrep(fName_k,'H_','-') '}),k(' num2str(k_im) '-' num2str(k) ')' gv_str accStr]);
    %subplot(1,2,2);
    %hist(labelsOfSamples, reshape(uniqLabelsAssigned,1,[]));
    %title(['histogram of labels for current cluster - labels(' mat2str(uniqLabelsAssigned) ')'])
end
