function [randomColors, im00_imPalette_distinctLabels] = createInitialLabelsCollage(srcFold, handsStruct, imCollage_imSizeMax, figID)
    surroundWithColorStruct = struct('thicknessParam',0.2,'colorParam',[0 0 0]);
    labelsOfSamples = handsStruct.detailedLabels(:,end);
    uniqLabelsAssigned = unique(labelsOfSamples);
    uniqLabelCnt = length(uniqLabelsAssigned);
    maxLabelID = max(uniqLabelsAssigned);
    randomColors = getRandomColorsMat(maxLabelID);
    
    [im00_imPalette_distinctLabels, paletteRCSize_distincLabels] = imCollageFuncs( 'createPalette', struct('imCnt',uniqLabelCnt,'channelCnt',3,'imSizeMax',3*imCollage_imSizeMax));
    for labelToAdd = 1:uniqLabelCnt
        %find a sample of the label - if possible the one closest
        %to the 
        labelIDCur = uniqLabelsAssigned(labelToAdd);
        bestLabelHolderImageID = find(labelsOfSamples==labelIDCur,1);
        surroundWithColorStruct.colorParam = randomColors(labelIDCur,:);
        imageToAdd = retrieveImageFromDetailedLabels(srcFold, bestLabelHolderImageID, handsStruct.detailedLabels);
        if isempty(imageToAdd)
            continue
        end                
        im00_imPalette_distinctLabels = imCollageFuncs('insertIntoPalette', struct('imageID',labelToAdd,'paletteRCSize',paletteRCSize_distincLabels,'imPalette',im00_imPalette_distinctLabels,'imageToAdd',imageToAdd,'boxSize',3*imCollage_imSizeMax, 'surroundWithColorStruct', surroundWithColorStruct));
    end
    if exist('figID','var') && ~isempty(figID) && figID>0
        figure(figID);
        clf;
        image(im00_imPalette_distinctLabels);
        title(['distinctLabels - labelCount(' num2str(uniqLabelCnt) ')']);
    end
end