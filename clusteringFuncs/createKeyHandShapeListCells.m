function [table_01, khsListCells_single, khsListCells_both, singleSignList, bothSignList, frameCountPerVideo, frameCntTable] = createKeyHandShapeListCells(optParamStruct)
%% 0. Define the variables
    if ~exist('optParamStruct', 'var') || ~isstruct(optParamStruct)
        optParamStruct = [];
    end
    srcFold             = getStructField(optParamStruct, 'srcFold', getVariableByComputerName('srcFold'));
    signIDList          = getStructField(optParamStruct, 'signIDList', [7,13,49,69,74,85,86,87,88,89,96,112,125,221,222,247,269,273,277,278,290,296,310,335,339,353,396,403,404,424,428,450,521,533,535,536,537,583,586,593,636]);
    figureSaveFolder    = getStructField(optParamStruct, 'figureSaveFolder', getVariableByComputerName('srcFold'));
     
    
    OPS_summarizeMultipleGestures = struct('srcFold', srcFold);
    [singleSignSummary, bothSignSummary] = summarizeMultipleGestures(signIDList, OPS_summarizeMultipleGestures);
    
    khsListCells_single = f(singleSignSummary);
    khsListCells_both = f(bothSignSummary);
    
    [surList, listElemntCnt] = createSURList(srcFold, 2, struct('signIDList',signIDList,'userID',2:7,'maxRptCnt',20));
    
    table_01 = cell(3,1);
    
    table_summary = cell(4,2);
    table_summary{1,1} = '#signs sentences';    table_summary{1,2} = num2str(length(unique(surList(:,1))));
    table_summary{2,1} = '#signers';            table_summary{2,2} = num2str(length(unique(surList(:,2))));
    table_summary{3,1} = '#repetitions';        table_summary{3,2} = mat2str([mean(surList(:,3)) max(surList(:,3))]);
    table_summary{4,1} = '#videos';             table_summary{4,2} = num2str(length(surList));
    table_01{1} = table_summary;
    
    %videoID_01/handID_02/frameID_03/khsID_04/dateOfLabel_05
    %vidID	handID	frameID	khsID	labelDate
    singleSignList = cell(0,5);
    bothSignList = cell(0,5);
    frameCountPerVideo = zeros(length(signIDList),2);
    for i=1:listElemntCnt
        s = surList(i,1);
        u = surList(i,2);
        r = surList(i,3);
        [singleLabels, bothLabels] = mapSignLabelIDs(srcFold, s, khsListCells_single, khsListCells_both);
        vidID = s*1000+u*100+r;
        [labels_LH, fileDateStr_LH] = getLabelVec(srcFold,s,u,r,1); 
        [labels_RH, fileDateStr_RH] = getLabelVec(srcFold,s,u,r,1); 
        [labels_BH, fileDateStr_BH] = getLabelVec(srcFold,s,u,r,1); 
        if ~isempty(labels_LH)
            labels_LH_reArranged = reArrangeLabels(labels_LH,length(singleLabels),singleLabels);
            singleSignList = appendLabels(singleSignList, vidID, 1, labels_LH_reArranged, fileDateStr_LH);
        end
        if ~isempty(labels_RH)
            labels_RH_reArranged = reArrangeLabels(labels_RH,length(singleLabels),singleLabels);
            singleSignList = appendLabels(singleSignList, vidID, 2, labels_RH_reArranged, fileDateStr_RH);
        end
        if ~isempty(labels_BH)
            labels_BH_reArranged = reArrangeLabels(labels_BH,length(bothLabels),bothLabels);
            bothSignList = appendLabels(bothSignList, vidID, 3, labels_BH_reArranged, fileDateStr_BH);
        end
        lengthOfVecs = length(labels_LH);
        assert(lengthOfVecs==length(labels_RH))
        assert(lengthOfVecs==length(labels_BH))
        
        signRow = find(signIDList==s);
        frameCountPerVideo(signRow,1) = s;%signID
        frameCountPerVideo(signRow,2) = frameCountPerVideo(signRow,2) + lengthOfVecs;
    end
    sTot = 2*sum(frameCountPerVideo(:,2));%singleHandFrameTotalCnt
    bTot = sum(frameCountPerVideo(:,2));%bothHandFrameTotalCnt
    disp(['singleHandTotalFrameCnt(' num2str(sTot) '),bothHansTotalFramecnt(' num2str(bTot) ')']);
    
    labelsAll_single = cell2mat(singleSignList(:,4));
    labelsAll_both = cell2mat(bothSignList(:,4));
    
    table_singleHand = cell(5,2);
    table_singleHand{1,1}  = '#uniqueKHS';          table_singleHand{1,2} = num2str(length(unique(labelsAll_single)-1));
    table_singleHand{2,1}  = '#frames-All';         table_singleHand{2,2} = num2str(size(singleSignList,1));
    table_singleHand{3,1}  = '#frames-transition';  table_singleHand{3,2} = num2str(sum(labelsAll_single==0));
    table_singleHand{4,1}  = '#frames-initial';     table_singleHand{4,2} = num2str(sum(labelsAll_single==1));
    table_singleHand{5,1}  = '#frames-handShape';   table_singleHand{5,2} = num2str(sum(labelsAll_single>1));
    table_01{2} = table_singleHand;
    
    table_bothHand = cell(5,2);
    table_bothHand{1,1}  = '#uniqueKHS';          table_bothHand{1,2} = num2str(length(unique(labelsAll_both)-1));
    table_bothHand{2,1}  = '#frames-All';         table_bothHand{2,2} = num2str(size(bothSignList,1));
    table_bothHand{3,1}  = '#frames-transition';  table_bothHand{3,2} = num2str(sum(labelsAll_both==0));
    table_bothHand{4,1}  = '#frames-initial';     table_bothHand{4,2} = num2str(sum(labelsAll_both==1));
    table_bothHand{5,1}  = '#frames-handShape';   table_bothHand{5,2} = num2str(sum(labelsAll_both>1));
    table_01{3} = table_bothHand;
    
    if ~isempty(figureSaveFolder)
       khsImages_single_folder = [figureSaveFolder filesep 'khsImages' filesep 'singleHand'];
       khsImages_both_folder = [figureSaveFolder filesep 'khsImages' filesep 'bothHands'];
       createFolderIfNotExist(khsImages_single_folder);
       createFolderIfNotExist(khsImages_both_folder);
    else
        khsImages_single_folder = [];
        khsImages_both_folder = [];
    end
    
    frameCntTable.s = zeros(length(signIDList),2+size(khsListCells_single,1));%signID,noLabel,khsCount
    frameCntTable.b = zeros(length(signIDList),2+size(khsListCells_both,1));%signID,noLabel,khsCount
    signRows_s = cell2mat(singleSignList(:,1));
    for si = 1:length(signIDList)
        s = signIDList(si);
        signRows = find(((signRows_s-s*1000)<1000)&((signRows_s-s*1000)>0));
        subList = labelsAll_single(signRows);
        frameCntTable.s(si,1) = s;
        for i=0:size(khsListCells_single,1)
            frameCntTable.s(si,i+2) = sum(subList==i);
        end
    end
    for i=1:size(khsListCells_single,1)
        numOfAssignedFrames = sum(labelsAll_single==khsListCells_single{i});
        khsListCells_single{i,6} = numOfAssignedFrames;
        if ~isempty(khsImages_single_folder)
            dL = khsListCells_single{i,[3 4 5]};
            s = floor(dL/1000);
            u = floor((dL-s*1000)/100);
            r = mod(dL,100);
            detailedLabels = [s u r khsListCells_single{i,[4 5]}];
            a = retrieveImageFromDetailedLabels(srcFold,1,detailedLabels);
            imwrite(a,[khsImages_single_folder filesep khsListCells_single{i,2} '.png']);
        end
    end
    signRows_b = cell2mat(bothSignList(:,1));
    for si = 1:length(signIDList)
        s = signIDList(si);
        signRows = find(((signRows_b-s*1000)<1000)&((signRows_b-s*1000)>0));
        subList = labelsAll_both(signRows);
        frameCntTable.b(si,1) = s;
        for i=0:size(khsListCells_both,1)
            frameCntTable.b(si,i+2) = sum(subList==i);
        end
    end
    for i=1:size(khsListCells_both,1)
        numOfAssignedFrames = sum(labelsAll_both==khsListCells_both{i});
        khsListCells_both{i,6} = numOfAssignedFrames;
        if ~isempty(khsImages_both_folder)
            dL = khsListCells_both{i,[3 4 5]};
            s = floor(dL/1000);
            u = floor((dL-s*1000)/100);
            r = mod(dL,100);
            detailedLabels = [s u r khsListCells_both{i,[4 5]}];
            a = retrieveImageFromDetailedLabels(srcFold,1,detailedLabels);
            imwrite(a,[khsImages_both_folder filesep khsListCells_both{i,2} '.png']);
        end
    end
end

function signList = appendLabels(signList, vidID, handID, labels_reArranged, fileDateStr)
%     %vidID handID	frameID	khsID labelDate
%     singleSignList = cell(0,5);
    fr = size(signList,1)+1;
    frameCount = length(labels_reArranged);
    to = fr + frameCount - 1;
    signList(fr:to,1) = {vidID};
    signList(fr:to,2) = {handID};
    signList(fr:to,3) = num2cell(1:frameCount);
    signList(fr:to,4) = num2cell(labels_reArranged);
    signList(fr:to,5) = {fileDateStr};    
end

function [labelsVec, fileDateStr] = getLabelVec(srcFold,s,u,r,handID)
    labelsVec = [];
    fileDateStr = '';
    handChar = {'L','R','B'};
    foldName_labels = [srcFold filesep num2str(s) filesep 'User_' num2str(u) '_' num2str(r) filesep 'labelFiles' filesep];
    labelFileName = [foldName_labels 'labelsCS_' handChar{handID} 'H.txt'];
    if exist(labelFileName,'file')
        labelsVec = getLabelVecFromFile(labelFileName, false);
        fileInfo = dir(labelFileName);
        [YYYY, MM, DD, HH, MN, SS] = datevec(fileInfo.datenum); 
        fileDateStr = [ num2str(YYYY) '-' num2str(MM,'%02d') '-' num2str(DD,'%02d') ' ' num2str(HH,'%02d') ':' num2str(MN,'%02d') ':' num2str(SS,'%02d')];
    end
end

function [singleLabels, bothLabels] = mapSignLabelIDs(srcFold, signID, khsListCells_single, khsListCells_both)
    clusterNames_single = getClusterNamesAssignedFromTxt(srcFold, struct('signIDList',signID,'single0_double1',0));
    clusterNames_both = getClusterNamesAssignedFromTxt(srcFold, struct('signIDList',signID,'single0_double1',1));
    
    singleLabelCount = size(clusterNames_single,2);
    bothLabelCount = size(clusterNames_both,2);
    
    singleLabels = zeros(1,singleLabelCount);
    bothLabels = zeros(1,bothLabelCount);
    %find(not(cellfun('isempty', strfind(labelNameCells, khsCentroids{khsID,5}))))
    for i=1:singleLabelCount
        rowID = find(not(cellfun('isempty',strfind(khsListCells_single(:,2),clusterNames_single{i})))); 
        if length(rowID)==1
            singleLabels(i) = rowID;
        else
            for ri = 1:length(rowID)
                r = rowID(ri);
                if strcmp(khsListCells_single(r,2),clusterNames_single{i})==1
                    singleLabels(i) = r;
                end
            end
        end
    end
    for i=1:bothLabelCount
        rowID = find(not(cellfun('isempty',strfind(khsListCells_both(:,2),clusterNames_both{i})))); 
        if length(rowID)==1
            bothLabels(i) = rowID;
        else
            for ri = 1:length(rowID)
                r = rowID(ri);
                if strcmp(khsListCells_both(r,2),clusterNames_both{i})==1
                    bothLabels(i) = r;
                end
            end
        end
    end
%IndexC = strfind(khsListCells_single(:,2),clusterNames_single{i});
%Index = find(not(cellfun('isempty',IndexC)));    
end

function khsListCells = f(signSummary)
    khsCount = length(signSummary.labelNamesSummary);
    khsListCells = cell(khsCount,5);
    %khsID khsName repFrameVidID repFrameHandID repFrameFrameID
    for i=1:khsCount
        khsID = signSummary.labelNamesSummary{i,1};
        khsName = signSummary.labelNamesSummary{i,2};
        
        repFrameID_rowIDs = signSummary.labelNamesSummary{i,3};
        repFrameID_vec = signSummary.representativeFrameIDs(repFrameID_rowIDs(1),:);
        
        repFrameVidID = repFrameID_vec(1)*1000 + repFrameID_vec(2)*100 + repFrameID_vec(3);
        
        repFrameHandID = repFrameID_vec(4);
        
        repFrameFrameID = repFrameID_vec(5);
        
        khsListCells{i,1} = khsID;    
        khsListCells{i,2} = khsName;    
        khsListCells{i,3} = repFrameVidID;    
        khsListCells{i,4} = repFrameHandID;    
        khsListCells{i,5} = repFrameFrameID;    
    end
end
