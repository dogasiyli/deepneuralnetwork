function [ resultSummary ] = createLabelTxtsFromDnnResults( labelsTextFileName, labelFolderCreateRoot, labelNamesCell, labelIDConversionVec )
%createLabelTxtsFromDnnResults Dnn will predict frame classes
%   This function will create labelsCS_LH.txt files into 
%   'labelFolderCreateRoot' by preserving the root structure
    resultSummary = zeros(0,5);%[signID, userID, repID, handID, frameCnt];
    
    
    %labelsTextFileName = '/home/doga/Desktop/predicted_labels_586.txt';
    %labelFolderCreateRoot = '/home/doga/Desktop/createdLabelsFromDnn';
    %labelNamesCell = {'noLabel','singleStill','priceShow','phoneTwist','writeShow','oneShow','sideFive','singlePalmUp'};
    %labelIDConversionVec = [0 1 2 2 3 4 5 6];

%% 0.Check input variables
    if ~exist('labelIDConversionVec','var')
        labelIDConversionVec = 0:length(labelNamesCell)-1;
    end

    
%% 1.read the label file into cell     
    fid = fopen(labelsTextFileName);
    %S586U4R6-LH_106.jpg*nolabel*41.33 
    lineCells = textscan(fid,'%s%s%s','Delimiter','*','TreatAsEmpty','~');
    fclose(fid);
    numOfLines = length(lineCells{1});

%% 2.now convert each line into s u r handID frameID labelID
    labelArr = zeros(numOfLines,6);
    for i = 1:numOfLines
        surFrTxt = lineCells{1}{i};%S586U4R6-LH_106.jpg
        labelName = lineCells{2}{i};%noLabel/singleStill
        confidence = lineCells{3}{i};%41.33
        surSplitted = strsplit(surFrTxt,{'-','_','.','S','U','R'},'CollapseDelimiters',true);%['-','_','.','S','U','R']
        
        signID = str2double(surSplitted{2});
        userID = str2double(surSplitted{3});
        repID = str2double(surSplitted{4});
        [handID, handName] = findClusterNameAndIDs(surSplitted{5}, {'LH','H','BH'});
        frameID = str2double(surSplitted{6});
        [labelID, labelName] = findClusterNameAndIDs(labelName, lower(labelNamesCell));
        labelID = labelIDConversionVec(labelID);
        
        labelArr(i,:) = [signID, userID, repID, handID, frameID, labelID];
    end
    labelArr = sortrows(labelArr, [1 2 3 4 5]);
    
%% 3. from labelArr - go for each video and each hand
    signIDs = unique(labelArr(:,1))';
    userIDs = unique(labelArr(:,2))';
    maxRepID = max(labelArr(:,3));
    for s = signIDs(:)
        signFolder = [labelFolderCreateRoot filesep num2str(s)];
        createFolderIfNotExist(signFolder,true);
        for u = userIDs
            for r = 1:maxRepID
                signUserRepRows = labelArr(labelArr(:,1)==s & labelArr(:,2)==u & labelArr(:,3)==r,:);
                if isempty(signUserRepRows)
                    continue
                end
                userRepFolder = [signFolder filesep 'User_' num2str(u) '_' num2str(r)];
                createFolderIfNotExist(userRepFolder,true);
 
                leftHandVec = labelArr(signUserRepRows(:,4)==1,6);
                rightHandVec = labelArr(signUserRepRows(:,4)==2,6);
                writeLabelVecToFile(userRepFolder, leftHandVec, 1, true);
                writeLabelVecToFile(userRepFolder, rightHandVec, 2, true);
                resultSummary = [resultSummary;s u r 1 length(leftHandVec)];
                resultSummary = [resultSummary;s u r 2 length(rightHandVec)];
            end
        end
    end
end

