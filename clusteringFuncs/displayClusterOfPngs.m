function [hSummary, confAccBest] = displayClusterOfPngs(folderWithImages, distMat, imageLabels, medoidHist_Best, clusterLabelsCell, optionalParamsStruct)
    %% Step-00 : get informative parameters and make initializations with them
    kMax = size(medoidHist_Best,1);
    kReduced = zeros(1,kMax);
    confAccBestVec = zeros(1,kMax);
    fileNameList = getFileList(folderWithImages,'.png','',false);
    fileNameList = sort(fileNameList);
    
    %% Step-00 : set and get optional parameters
    %OPS_displayClusterOfPngs = struct('figID',1,'gtLabelsGrouping', []);
    figID = getOptionalParamsFromStruct(optionalParamsStruct, 'figID', 1, true);
    gtLabelsGrouping = getOptionalParamsFromStruct(optionalParamsStruct, 'gtLabelsGrouping', [], true);
    clusteringParams = getOptionalParamsFromStruct(optionalParamsStruct, 'clusteringParams', struct('gtLabelsGrouping',[],'groupClusterLabels',false), true);
    if isempty(gtLabelsGrouping) && ~isempty(clusteringParams.gtLabelsGrouping)
        gtLabelsGrouping = clusteringParams.gtLabelsGrouping;
    end
    distMatInfoStruct = getOptionalParamsFromStruct(optionalParamsStruct, 'distMatInfoStruct', [], true);
    
    %% Step-01 : figure related functions and initializations
    %a base-size value and a struct parameter for collageImages
    imCollage_imSizeMax = [50 50];
    %clear and initialize the summary image figure
    hSummary = figure(figID);clf;hold on;
    %set and get the folder to write the result images into
    clusterResultFolder = setClusterResultFolder(folderWithImages, distMatInfoStruct);
   
    [randomColors] = fwf_createInitialLabelsCollage(folderWithImages, fileNameList, imageLabels, imCollage_imSizeMax, figID);
    clusterDistinctLablesFileName = [clusterResultFolder filesep 'distinctLabelsInput.png'];
    saveas(hSummary, clusterDistinctLablesFileName); 
    
    if isfield(clusteringParams,'kMax')
        kMax = clusteringParams.kMax;
        kVec = 1:clusteringParams.kMax;
    elseif isfield(clusteringParams,'kVec')
        kVec = clusteringParams.kVec;
        kMax = max(kVec);
    else
        error('wtf');
    end
    
    for k=kVec %rows
        %clusterLabels_cur have the assigned cluster ID's of each sample
        clusterLabels_cur = clusterLabelsCell{1,k};
        uniqLabelsAssigned = unique(clusterLabels_cur);
        k_uniqCount = length(uniqLabelsAssigned);
        medoidIDsOdCluster = medoidHist_Best(k, kMax-k+1 : kMax);
        if k_uniqCount<k
            disp(['labels are changed from ' mat2str(uniqLabelsAssigned) ' to ' mat2str(1:k_uniqCount)]);
            clusterLabels_cur = reSetLabels(clusterLabels_cur, uniqLabelsAssigned, 1:k_uniqCount);
            medoidIDsOdCluster = medoidIDsOdCluster(uniqLabelsAssigned);
        end
        histOfLabelAssignments = hist(clusterLabels_cur,1:k_uniqCount);
        %each row has the medoid and the closest sample to it        
        medoidIDsOdCluster(medoidIDsOdCluster<0) = [];
        medoidIDsOdCluster(medoidIDsOdCluster==inf) = [];        
        medoidCount = length(medoidIDsOdCluster);
        
        disp(['cluster summary for k=' num2str(k) ' is :']);
        printClusterSummary(clusterLabels_cur);
        
        disp(['There are ' num2str(medoidCount) ' medoids found']);
        
        disp('Part of distance matrix showing cluster medoids distances:');
        disptable(distMat(medoidIDsOdCluster,medoidIDsOdCluster));
        
        %clusterLabels_med2 = clusterLabels_cur(sampleIndsVec_cur(medoidIDsOdCluster));
        imageCell = cell(1,k_uniqCount);
        
        
        if clusteringParams.groupClusterLabels
            [confMatBest, uniqueMappingLabels, confAccBestVec(k), outStruct, h1, h2] = groupClusterLabelsIntoGroundTruth(clusterLabelsCell{1,k}, imageLabels, struct('gtLabelsGrouping', gtLabelsGrouping,'printResults', true,'figID', 1));
            newKlusterCell      = outStruct.newKlusterCell;
            clusterLabels_best  = outStruct.clusLabels_Mapped;            
            kReduced(k)         = length(newKlusterCell);

            disp(['Best confusion matrix for cluster count(' num2str(k) ') is found with k=' num2str(kReduced(k)) ' and the acc(' num2str(confAccBestVec(k),'%4.2f') ')']);
            disptable(confMatBest);
            disptable(uniqueMappingLabels, 'originalLabels|userMappedLabels|bestConfusionLabels');

            clusterBestConfusionImageFileName = [clusterResultFolder filesep 'clusterResult_k' num2str(k,'%02d') '_conf_best.png'];
            saveas(h1, clusterBestConfusionImageFileName);
            clusterOriginalConfusionImageFileName = [clusterResultFolder filesep 'clusterResult_k' num2str(k,'%02d') '_conf_orig.png'];
            saveas(h2, clusterOriginalConfusionImageFileName);

            %I want to have - for each cluster
            % 1 : medoidCluster visual
            % 2 : 1 sample from labels that are assigned to this cluster - Ground truth label(1-1) is assigned to US-cluster(2)
            % 3 : all samples that are assigned into this group

            %As a summary of all clusters     
            % 1 : the confusion matrix
            sampleIndsVec_best = plotDistanceMatrixAsData(distMat, 2, clusterLabels_best, []);
            sortedClusterLabels = clusterLabels_best(sampleIndsVec_best);        
            for j = 1:length(newKlusterCell) %cols   
                [h2, imageCell{j}] = fwf_createCollageImForAKluster(k_uniqCount, j, confAccBestVec, folderWithImages, fileNameList, imageLabels, sortedClusterLabels, sampleIndsVec_best, uniqueMappingLabels, newKlusterCell, medoidIDsOdCluster, randomColors, imCollage_imSizeMax, figID);
                %save h2 as clusterResult_k01_02.png
                %under
                imFullName = [clusterResultFolder filesep 'clusterResult_k' num2str(k,'%02d') '_' num2str(j,'%02d') '.png'];
                saveas(h2, imFullName);
            end
        else
            %I have k clusters
            %clusterLabels_cur are my cluster labels
            sampleIndsVec = plotDistanceMatrixAsData(distMat, 2, clusterLabels_cur, []);
            sortedClusterLabels = clusterLabels_cur(sampleIndsVec);
            [G, uniqueMappingLabels, uniqueGTLabels, gtLabelsGrouping] = getUniqueMappingLabels(imageLabels(:), gtLabelsGrouping);
            medoidsClusterIDs = clusterLabels_cur(medoidIDsOdCluster);
            assert(length(unique(medoidsClusterIDs))==k,'this must hold');
            newKlusterCell = num2cell(medoidsClusterIDs);
            for j = 1:k_uniqCount %cols   
                [h2, imageCell{j}] = fwf_createCollageImRandomMatch(j, folderWithImages, fileNameList, imageLabels, sortedClusterLabels, sampleIndsVec, newKlusterCell, medoidIDsOdCluster, randomColors, imCollage_imSizeMax, figID);
                %save h2 as clusterResult_k01_02.png
                %under
                imFullName = [clusterResultFolder filesep 'clusterResult_k' num2str(k,'%02d') '_' num2str(j,'%02d') '.png'];
                saveas(h2, imFullName);
            end            
        end
    end
    k_confAccBestVec = [reshape(1:kMax,kMax,[]) kReduced(:) confAccBestVec(:)];
    k_confAccBestVec = k_confAccBestVec(kVec,:);
    uniqLabelsIn = unique(imageLabels(:));
    labelsCount = length(uniqLabelsIn);
    confAccBest_kSpecificity = (k_confAccBestVec(:,2)-1)/(labelsCount-1);
    confAccBest_similarity = k_confAccBestVec(:,3).*confAccBest_kSpecificity;
    confAccBest = [k_confAccBestVec confAccBest_kSpecificity confAccBest_similarity (1-k_confAccBestVec(:,3))*length(imageLabels)];
    if clusteringParams.groupClusterLabels
        confAccBest(:,3:5) = confAccBest(:,3:5)*100;
        try
            confAccBest = sortrows(confAccBest,5,'descend');
        catch
            confAccBest = sortrows(confAccBest,-5);
        end
        disptable(confAccBest, 'originalClusterCount|reducedKlusterCount|confusionAccuracy|kSpecificity|combinedPrecision|misClassified',[],'%4.2f',1);    
    end
end

function [randomColors, im00_imPalette_distinctLabels] = fwf_createInitialLabelsCollage(folderWithImages, fileNameList, labelsOfSamples, imCollage_imSizeMax, figID)
    surroundWithColorStruct = struct('thicknessParam',0.2,'colorParam',[0 0 0]);
    uniqLabelsAssigned = unique(labelsOfSamples);
    uniqLabelCnt = length(uniqLabelsAssigned);
    maxLabelID = max(uniqLabelsAssigned);
    randomColors = getRandomColorsMat(maxLabelID);
    
    [im00_imPalette_distinctLabels, paletteRCSize_distincLabels] = imCollageFuncs( 'createPalette', struct('imCnt',uniqLabelCnt,'channelCnt',3,'imSizeMax',3*imCollage_imSizeMax));
    for labelToAdd = 1:uniqLabelCnt
        %find a sample of the label - if possible the one closest
        %to the 
        labelIDCur = uniqLabelsAssigned(labelToAdd);
        bestLabelHolderImageID = find(labelsOfSamples==labelIDCur,1);
        surroundWithColorStruct.colorParam = randomColors(labelIDCur,:);
        imageToAdd = imread([folderWithImages filesep fileNameList{bestLabelHolderImageID}]);
        if isempty(imageToAdd)
            continue
        end                
        im00_imPalette_distinctLabels = imCollageFuncs('insertIntoPalette', struct('imageID',labelToAdd,'paletteRCSize',paletteRCSize_distincLabels,'imPalette',im00_imPalette_distinctLabels,'imageToAdd',imageToAdd,'boxSize',3*imCollage_imSizeMax, 'surroundWithColorStruct', surroundWithColorStruct));
    end
    figure(figID);
    clf;
    image(im00_imPalette_distinctLabels);
    title(['distinctLabels - labelCount(' num2str(uniqLabelCnt) ')']);
end

function [h2, imComb_imPalette_curCluster] = fwf_createCollageImForAKluster(k, j, confAccBestVec, folderWithImages, fileNameList, imageLabels, sortedClusterLabels, sampleIndsVec_best, uniqueMappingLabels, newKlusterCell, medoidIDsOdCluster, randomColors, imCollage_imSizeMax, figID)
    surroundWithColorStruct = struct('thicknessParam',0.2,'colorParam',[0 0 0]);
    k_im = newKlusterCell{j};
    clusterGroupCnt = length(k_im);
    im_k = medoidIDsOdCluster(k_im);
    labelID_medoidCenter = imageLabels(im_k);
    if clusterGroupCnt==1
        im01_medoidIm = imread([folderWithImages filesep fileNameList{im_k}]);
        surroundWithColorStruct.colorParam = randomColors(labelID_medoidCenter,:);
        im01_medoidIm = putImg_IntoBox(im01_medoidIm, int32((1+surroundWithColorStruct.thicknessParam)*[size(im01_medoidIm,1) size(im01_medoidIm,2)]), true, surroundWithColorStruct);
    else
        [im01_medoidIm, paletteRCSize_medoidIm] = imCollageFuncs( 'createPalette', struct('imCnt',clusterGroupCnt,'channelCnt',3,'imSizeMax',3*imCollage_imSizeMax));
        for cToAdd = 1:clusterGroupCnt
            imID = im_k(cToAdd);
            imageToAdd = imread([folderWithImages filesep fileNameList{imID}]);
            labelToAdd = imageLabels(imID);
            surroundWithColorStruct.colorParam = randomColors(labelToAdd,:);
            if isempty(imageToAdd)
                continue
            end                
            im01_medoidIm = imCollageFuncs('insertIntoPalette', struct('imageID',cToAdd,'paletteRCSize',paletteRCSize_medoidIm,'imPalette',im01_medoidIm,'imageToAdd',imageToAdd,'boxSize',3*imCollage_imSizeMax, 'surroundWithColorStruct', surroundWithColorStruct));
        end                
    end


    allImIDs_curCluster = find(ismember(sortedClusterLabels,j));
    positionOfClusterMedoids = find(ismember(sampleIndsVec_best(allImIDs_curCluster),im_k));
    assert(length(positionOfClusterMedoids)==clusterGroupCnt,'this must hold');
    allImIDs_curCluster(positionOfClusterMedoids) = [];
    allImIDs_curCluster = sampleIndsVec_best(allImIDs_curCluster);
    imCnt_collage = length(allImIDs_curCluster);

    labelsOfSamples = imageLabels(allImIDs_curCluster);
    uniqLabelsAssigned = unique(labelsOfSamples);
    uniqLabelCnt = length(uniqLabelsAssigned);
    [im02_imPalette_distinctLabels, paletteRCSize_distincLabels] = imCollageFuncs( 'createPalette', struct('imCnt',uniqLabelCnt,'channelCnt',3,'imSizeMax',3*imCollage_imSizeMax));
    for labelToAdd = 1:uniqLabelCnt
        %find a sample of the label - if possible the one closest
        %to the 
        labelIDCur = uniqLabelsAssigned(labelToAdd);
        bestLabelHolderSampleID = find(labelsOfSamples==labelIDCur,1);
        bestLabelHolderImageID = allImIDs_curCluster(bestLabelHolderSampleID);

        surroundWithColorStruct.colorParam = randomColors(labelIDCur,:);

        imageToAdd = imread([folderWithImages filesep fileNameList{bestLabelHolderImageID}]);
        if isempty(imageToAdd)
            continue
        end                
        im02_imPalette_distinctLabels = imCollageFuncs('insertIntoPalette', struct('imageID',labelToAdd,'paletteRCSize',paletteRCSize_distincLabels,'imPalette',im02_imPalette_distinctLabels,'imageToAdd',imageToAdd,'boxSize',3*imCollage_imSizeMax, 'surroundWithColorStruct', surroundWithColorStruct));
    end
    [im04_imPalette_labelProbs, paletteRCSize_distincLabels] = imCollageFuncs( 'createPalette', struct('imCnt',uniqLabelCnt,'channelCnt',3,'imSizeMax',3*imCollage_imSizeMax));
    for labelToAdd = 1:uniqLabelCnt
        %find a sample of the label - if possible the one closest
        %to the 
        labelIDCur = uniqLabelsAssigned(labelToAdd);
        thisClusterHas = sum(labelsOfSamples==labelIDCur) + double(sum(ismember(labelID_medoidCenter,labelIDCur)));                
        totalOfLabelCur = sum(imageLabels(:)==labelIDCur);
        remainLabelsCount = totalOfLabelCur - thisClusterHas;

        surroundWithColorStruct.colorParam = randomColors(labelIDCur,:);
        surroundWithColorStruct.thicknessParam = remainLabelsCount/totalOfLabelCur;

        probVal = 0.5 + 0.5*(1-surroundWithColorStruct.thicknessParam);
        rowSize = floor(imCollage_imSizeMax(1)*probVal);
        colSize = floor(imCollage_imSizeMax(2)*probVal);

        imageToAdd1 = getNumberImage(thisClusterHas, rowSize, colSize, imCollage_imSizeMax);
        imageToAdd2 = getNumberImage(totalOfLabelCur, rowSize, colSize, imCollage_imSizeMax);
        imageToAdd = [imageToAdd1 zeros(size(imageToAdd1)) imageToAdd2];
        imageToAdd = cat(3, imageToAdd, imageToAdd, imageToAdd);
        im04_imPalette_labelProbs = imCollageFuncs('insertIntoPalette', struct('imageID',labelToAdd,'paletteRCSize',paletteRCSize_distincLabels,'imPalette',im04_imPalette_labelProbs,'imageToAdd',imageToAdd,'boxSize',3*imCollage_imSizeMax, 'surroundWithColorStruct', surroundWithColorStruct));
    end

    surroundWithColorStruct.thicknessParam = 0.1;
    [im03_imPalette_curCluster, paletteRCSize_curCluster] = imCollageFuncs( 'createPalette', struct('imCnt',imCnt_collage,'channelCnt',3,'imSizeMax',imCollage_imSizeMax));
    for imageIDToAdd = 1:length(allImIDs_curCluster)
        eachClusterIm_i = allImIDs_curCluster(imageIDToAdd);
        imageToAdd = imread([folderWithImages filesep fileNameList{eachClusterIm_i}]);
        if isempty(imageToAdd)
            continue
        end
        labelIDCur = imageLabels(eachClusterIm_i);
        surroundWithColorStruct.colorParam = randomColors(labelIDCur,:);
        im03_imPalette_curCluster = imCollageFuncs('insertIntoPalette', struct('imageID',imageIDToAdd,'paletteRCSize',paletteRCSize_curCluster,'imPalette',im03_imPalette_curCluster,'imageToAdd',imageToAdd,'boxSize',imCollage_imSizeMax, 'surroundWithColorStruct', surroundWithColorStruct));
    end

    %I need 1 sample from all assigned clusters            
    [imComb_imPalette_curCluster, imComb_paletteRCSize_curCluster] = imCollageFuncs( 'createPalette', struct('imCnt',3,'channelCnt',3,'imSizeMax',8*imCollage_imSizeMax));
    if length(labelID_medoidCenter)==1
        surroundWithColorStruct.colorParam = 1-randomColors(labelID_medoidCenter,:);
    else
        surroundWithColorStruct.colorParam = 1-mean(randomColors(labelID_medoidCenter,:));
    end
    imComb_imPalette_curCluster = imCollageFuncs('insertIntoPalette', struct('imageID',1,'paletteRCSize',imComb_paletteRCSize_curCluster,'imPalette',imComb_imPalette_curCluster,'imageToAdd',im01_medoidIm,'boxSize',8*imCollage_imSizeMax,'surroundWithColorStruct',surroundWithColorStruct));
    imComb_imPalette_curCluster = imCollageFuncs('insertIntoPalette', struct('imageID',2,'paletteRCSize',imComb_paletteRCSize_curCluster,'imPalette',imComb_imPalette_curCluster,'imageToAdd',im02_imPalette_distinctLabels,'boxSize',8*imCollage_imSizeMax));
    imComb_imPalette_curCluster = imCollageFuncs('insertIntoPalette', struct('imageID',3,'paletteRCSize',imComb_paletteRCSize_curCluster,'imPalette',imComb_imPalette_curCluster,'imageToAdd',im03_imPalette_curCluster,'boxSize',8*imCollage_imSizeMax));
    imComb_imPalette_curCluster = imCollageFuncs('insertIntoPalette', struct('imageID',4,'paletteRCSize',imComb_paletteRCSize_curCluster,'imPalette',imComb_imPalette_curCluster,'imageToAdd',im04_imPalette_labelProbs,'boxSize',8*imCollage_imSizeMax));

    h2 = figure(figID+2);
    clf;
    %hold on;
    %subplot(rC, cC, sub2ind([cC,rC],k_im,k-1));%row:k-1, col-im
    %cl_str = ['<' num2str(clusterLabels_med1(im)) ',' num2str(clusterLabels_med2(im)) '>'];
    %usr_str = ['s(' num2str(dLab_k(1)) ')-u(' num2str(dLab_k(2)) '-' num2str(dLab_k(3))];
    accStr = [',acc(' mat2str(confAccBestVec(k), 5) ')'];
    %subplot(1,2,1);
    image(imComb_imPalette_curCluster);

    if length(newKlusterCell)==k
        kStr = ['k(' num2str(j) ' of ' num2str(k) '), kCombined[' strrep(strrep(mat2str(k_im),'[',''),']','') ']'];
    else
        kStr = ['k(' num2str(j) ' of ' num2str(length(newKlusterCell)) '<' num2str(k) '), kCombined[' strrep(strrep(mat2str(k_im),'[',''),']','') ']'];
    end
    %gv = find(ismember(clusterLabelsGroupingVec,j));
    %gv2 = find(ismember(gtLabelsGrouping,gv));
    bestAssignedLabels = find(ismember(uniqueMappingLabels(:,3),j));
    %assert(sum(gv2(:)-bestAssignedLabels(:))==0,'this must hold')
    %if handsStruct_detailedLabels_colCnt==6
    %    gv_str = [',g(' mat2str(gv2) ')'];
    %else
    %    gv_str = '';
    %end
    title([kStr accStr ',labels(' mat2str(uniqLabelsAssigned) '),bestAssignedLabels(' mat2str(bestAssignedLabels) ')']);
    %title([usr_str '_{' strrep(fName_k,'H_','-') '}),k(' num2str(k_im) '-' num2str(k) ')' gv_str accStr]);
    %subplot(1,2,2);
    %hist(labelsOfSamples, reshape(uniqLabelsAssigned,1,[]));
    %title(['histogram of labels for current cluster - labels(' mat2str(uniqLabelsAssigned) ')'])
end

function [h2, imComb_imPalette_curCluster] = fwf_createCollageImRandomMatch(k, folderWithImages, fileNameList, imageLabels, sortedClusterLabels, sampleIndsVec_best, newKlusterCell, medoidIDsOdCluster, randomColors, imCollage_imSizeMax, figID)
    surroundWithColorStruct = struct('thicknessParam',0.2,'colorParam',[0 0 0]);
    k_im = newKlusterCell{k};
    clusterGroupCnt = length(k_im);
    im_k = medoidIDsOdCluster(k);
    labelID_medoidCenter = imageLabels(im_k);
    if clusterGroupCnt==1
        im01_medoidIm = imread([folderWithImages filesep fileNameList{im_k}]);
        surroundWithColorStruct.colorParam = randomColors(labelID_medoidCenter,:);
        im01_medoidIm = putImg_IntoBox(im01_medoidIm, int32((1+surroundWithColorStruct.thicknessParam)*[size(im01_medoidIm,1) size(im01_medoidIm,2)]), true, surroundWithColorStruct);
    else
        [im01_medoidIm, paletteRCSize_medoidIm] = imCollageFuncs( 'createPalette', struct('imCnt',clusterGroupCnt,'channelCnt',3,'imSizeMax',3*imCollage_imSizeMax));
        for cToAdd = 1:clusterGroupCnt
            imageToAdd = imread([folderWithImages filesep fileNameList{im_k(cToAdd)}]);
            labelToAdd = imageLabels(im_k(cToAdd));
            surroundWithColorStruct.colorParam = randomColors(labelToAdd,:);
            if isempty(imageToAdd)
                continue
            end                
            im01_medoidIm = imCollageFuncs('insertIntoPalette', struct('imageID',cToAdd,'paletteRCSize',paletteRCSize_medoidIm,'imPalette',im01_medoidIm,'imageToAdd',imageToAdd,'boxSize',3*imCollage_imSizeMax, 'surroundWithColorStruct', surroundWithColorStruct));
        end                
    end

    allImIDs_curCluster = find(ismember(sortedClusterLabels,k_im));
    positionOfClusterMedoids = find(ismember(sampleIndsVec_best(allImIDs_curCluster),im_k));
    assert(length(positionOfClusterMedoids)==clusterGroupCnt,'this must hold');
    %allImIDs_curCluster(positionOfClusterMedoids) = [];
    allImIDs_curCluster = sampleIndsVec_best(allImIDs_curCluster);
    imCnt_collage = length(allImIDs_curCluster);

    [labelsOfSamples, uniqLabelsAssigned, uniqLabelCnt,uniqLabelsSortedPerListAppearence] = sortUniqLabelsFirstAppearListFirst(imageLabels(allImIDs_curCluster));
    [im02_imPalette_distinctLabels, paletteRCSize_distincLabels] = imCollageFuncs( 'createPalette', struct('imCnt',uniqLabelCnt,'channelCnt',3,'imSizeMax',3*imCollage_imSizeMax));
    for labelToAdd = 1:uniqLabelCnt
        %find a sample of the label - if possible the one closest
        %to the 
        labelIDCur = uniqLabelsSortedPerListAppearence(labelToAdd);
        bestLabelHolderSampleID = find(labelsOfSamples==labelIDCur,1);
        bestLabelHolderImageID = allImIDs_curCluster(bestLabelHolderSampleID);

        surroundWithColorStruct.colorParam = randomColors(labelIDCur,:);

        imageToAdd = imread([folderWithImages filesep fileNameList{bestLabelHolderImageID}]);
        if isempty(imageToAdd)
            continue
        end                
        im02_imPalette_distinctLabels = imCollageFuncs('insertIntoPalette', struct('imageID',labelToAdd,'paletteRCSize',paletteRCSize_distincLabels,'imPalette',im02_imPalette_distinctLabels,'imageToAdd',imageToAdd,'boxSize',3*imCollage_imSizeMax, 'surroundWithColorStruct', surroundWithColorStruct));
    end
    [im04_imPalette_labelProbs, paletteRCSize_distincLabels] = imCollageFuncs( 'createPalette', struct('imCnt',uniqLabelCnt,'channelCnt',3,'imSizeMax',3*imCollage_imSizeMax));
    for labelToAdd = 1:uniqLabelCnt
        %find a sample of the label - if possible the one closest
        %to the 
        labelIDCur = uniqLabelsSortedPerListAppearence(labelToAdd);
        thisClusterHas = sum(labelsOfSamples==labelIDCur);%+ double(sum(ismember(labelID_medoidCenter,labelIDCur)));                
        totalOfLabelCur = sum(imageLabels(:)==labelIDCur);
        remainLabelsCount = totalOfLabelCur - thisClusterHas;

        surroundWithColorStruct.colorParam = randomColors(labelIDCur,:);
        surroundWithColorStruct.thicknessParam = remainLabelsCount/totalOfLabelCur;

        probVal = 0.5 + 0.5*(1-surroundWithColorStruct.thicknessParam);
        rowSize = floor(imCollage_imSizeMax(1)*probVal);
        colSize = floor(imCollage_imSizeMax(2)*probVal);

        imageToAdd1 = getNumberImage(thisClusterHas, rowSize, colSize, imCollage_imSizeMax);
        imageToAdd2 = getNumberImage(totalOfLabelCur, rowSize, colSize, imCollage_imSizeMax);
        imageToAdd = [imageToAdd1 zeros(size(imageToAdd1)) imageToAdd2];
        imageToAdd = cat(3, imageToAdd, imageToAdd, imageToAdd);
        im04_imPalette_labelProbs = imCollageFuncs('insertIntoPalette', struct('imageID',labelToAdd,'paletteRCSize',paletteRCSize_distincLabels,'imPalette',im04_imPalette_labelProbs,'imageToAdd',imageToAdd,'boxSize',3*imCollage_imSizeMax, 'surroundWithColorStruct', surroundWithColorStruct));
    end

    surroundWithColorStruct.thicknessParam = 0.1;
    [im03_imPalette_curCluster, paletteRCSize_curCluster] = imCollageFuncs( 'createPalette', struct('imCnt',imCnt_collage,'channelCnt',3,'imSizeMax',imCollage_imSizeMax));
    for imageIDToAdd = 1:length(allImIDs_curCluster)
        eachClusterIm_i = allImIDs_curCluster(imageIDToAdd);
        imageToAdd = imread([folderWithImages filesep fileNameList{eachClusterIm_i}]);
        if isempty(imageToAdd)
            continue
        end
        labelIDCur = imageLabels(eachClusterIm_i);
        surroundWithColorStruct.colorParam = randomColors(labelIDCur,:);
        im03_imPalette_curCluster = imCollageFuncs('insertIntoPalette', struct('imageID',imageIDToAdd,'paletteRCSize',paletteRCSize_curCluster,'imPalette',im03_imPalette_curCluster,'imageToAdd',imageToAdd,'boxSize',imCollage_imSizeMax, 'surroundWithColorStruct', surroundWithColorStruct));
    end

    %I need 1 sample from all assigned clusters            
    [imComb_imPalette_curCluster, imComb_paletteRCSize_curCluster] = imCollageFuncs( 'createPalette', struct('imCnt',3,'channelCnt',3,'imSizeMax',8*imCollage_imSizeMax));
    if length(labelID_medoidCenter)==1
        surroundWithColorStruct.colorParam = 1-randomColors(labelID_medoidCenter,:);
    else
        surroundWithColorStruct.colorParam = 1-mean(randomColors(labelID_medoidCenter,:));
    end
    imComb_imPalette_curCluster = imCollageFuncs('insertIntoPalette', struct('imageID',1,'paletteRCSize',imComb_paletteRCSize_curCluster,'imPalette',imComb_imPalette_curCluster,'imageToAdd',im01_medoidIm,'boxSize',8*imCollage_imSizeMax,'surroundWithColorStruct',surroundWithColorStruct));
    imComb_imPalette_curCluster = imCollageFuncs('insertIntoPalette', struct('imageID',2,'paletteRCSize',imComb_paletteRCSize_curCluster,'imPalette',imComb_imPalette_curCluster,'imageToAdd',im02_imPalette_distinctLabels,'boxSize',8*imCollage_imSizeMax));
    imComb_imPalette_curCluster = imCollageFuncs('insertIntoPalette', struct('imageID',3,'paletteRCSize',imComb_paletteRCSize_curCluster,'imPalette',imComb_imPalette_curCluster,'imageToAdd',im03_imPalette_curCluster,'boxSize',8*imCollage_imSizeMax));
    imComb_imPalette_curCluster = imCollageFuncs('insertIntoPalette', struct('imageID',4,'paletteRCSize',imComb_paletteRCSize_curCluster,'imPalette',imComb_imPalette_curCluster,'imageToAdd',im04_imPalette_labelProbs,'boxSize',8*imCollage_imSizeMax));

    h2 = figure(figID+2);
    clf;
    image(imComb_imPalette_curCluster);
    kStr = ['k(' mat2str(k_im) ' of ' num2str(length(newKlusterCell)) ')'];
    title([kStr ',labels(' mat2str(uniqLabelsSortedPerListAppearence) ')']);
end
%[labelsOfSamples, uniqLabelsAssigned, uniqLabelCnt,uniqLabelsSortedPerListAppearence] = sortUniqLabelsFirstAppearListFirst(handsStruct.detailedLabels(allImIDs_curCluster,6));
function [labelsOfSamples, uniqLabelsAssigned, uniqLabelCnt, uniqLabelsSortedPerListAppearence] = sortUniqLabelsFirstAppearListFirst(labelsOfSamples)
    uniqLabelsAssigned = unique(labelsOfSamples);
    uniqLabelCnt = length(uniqLabelsAssigned);
    if nargout>3
        labelsOfSamples_del = labelsOfSamples;
        uniqLabelsSortedPerListAppearence = NaN(size(uniqLabelsAssigned));
        for i=1:uniqLabelCnt
            uniqLabelsSortedPerListAppearence(i) = labelsOfSamples_del(1);
            labelsOfSamples_del(labelsOfSamples_del==uniqLabelsSortedPerListAppearence(i)) = [];
        end
    end
end