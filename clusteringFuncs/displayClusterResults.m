function [hSummary, confAccBest, clusterResultCells] = displayClusterResults(srcFold, distMat, handsStruct, medoidHist_Best, clusterLabelsCell, OPS)
    %% Step-00 : get informative parameters and make initializations with them
    kMax = size(medoidHist_Best,1);
    assert(size(handsStruct.detailedLabels,2)==6,'The labels must be given as 6th column')

    kReduced = zeros(1,kMax);
    confAccBestVec = zeros(1,kMax);
    
    %% Step-00 : set and get optional parameters
    figID            = getOptionalParamsFromStruct(OPS, 'figID', 1, true);
    gtLabelsGrouping = getOptionalParamsFromStruct(OPS, 'gtLabelsGrouping', [], true);
    clusteringParams = getOptionalParamsFromStruct(OPS, 'clusteringParams', struct('gtLabelsGrouping',[],'groupClusterLabels',false), true);
    displayLevel     = getOptionalParamsFromStruct(OPS, 'displayLevel', 0, true);%0,1,2,3
    if isempty(gtLabelsGrouping) && ~isempty(clusteringParams.gtLabelsGrouping)
        gtLabelsGrouping = clusteringParams.gtLabelsGrouping;
    end
    distMatInfoStruct   = getOptionalParamsFromStruct(OPS, 'distMatInfoStruct', [], true);
    %set and get the folder to write the result images into
    clusterResultFolder = getStructField(OPS, 'clusterResultFolder', setClusterResultFolder(srcFold, distMatInfoStruct,[],struct('createDirIfNotExist',false)));
    saveAllClusterResultImages = getStructField(OPS, 'saveAllClusterResultImages', true);
    
    
    %% Step-01 : figure related functions and initializations
    %a base-size value and a struct parameter for collageImages
    imCollage_imSizeMax = [50 50];
    %clear and initialize the summary image figure
    hSummary = figure(figID);clf;hold on;
    maximizeFigureWindow(hSummary);
   
    createFolderIfNotExist(clusterResultFolder);
    removePrevClusterResults(clusterResultFolder);
    
    gtLabels = handsStruct.detailedLabels(:,end);
    [randomColors] = createInitialLabelsCollage(srcFold, handsStruct, imCollage_imSizeMax, figID);
    clusterDistinctLablesFileName = [clusterResultFolder filesep 'distinctLabelsInput.png'];
    saveas(hSummary, clusterDistinctLablesFileName); 
    
    [kMax, kVec] = fwf_get_k_params(clusteringParams);
    distMat = makeDistMatFull(distMat);
    
    %k, acc, numOfSamples, wrongSampleCount, wrongSamplesCell
    clusterResultCells = cell(length(kVec),5);
    
    for ki=1:length(kVec) %rows
        k = kVec(ki);
        %clusterLabels_cur have the assigned cluster ID's of each sample
        medoidIDsOdCluster = medoidHist_Best(k, kMax-k+1 : kMax);
        %[~, clusterLabels_cur] = calculateEnergyFromDistMatWithMedoids(distMat, medoidIDsOdCluster);
        clusterLabels_cur = clusterLabelsCell{1,k};
        uniqLabelsAssigned = unique(clusterLabels_cur);
        k_uniqCount = length(uniqLabelsAssigned);
        if k_uniqCount<k
            if displayLevel>=2
                disp(['labels are changed from ' mat2str(uniqLabelsAssigned) ' to ' mat2str(1:k_uniqCount)]);
            end
            clusterLabels_cur = reSetLabels(clusterLabels_cur, uniqLabelsAssigned, 1:k_uniqCount);
            medoidIDsOdCluster = medoidIDsOdCluster(uniqLabelsAssigned);
        end
        
        if clusteringParams.groupClusterLabels
            [medoidTable, acc, clusterLabels_cur] = getMedoidTable(distMat, medoidIDsOdCluster, gtLabels, struct('displayTable',true)); %#ok<ASGLU>
        end
        %
        %each row has the medoid and the closest sample to it        
        medoidIDsOdCluster(medoidIDsOdCluster<0) = [];
        medoidIDsOdCluster(medoidIDsOdCluster==inf) = [];        
        medoidCount = length(medoidIDsOdCluster);
        
        if displayLevel>=2
            disp(['cluster summary for k=' num2str(k) ' is :']);
            printClusterSummary(clusterLabels_cur);
        elseif displayLevel>=1
            histOfLabelAssignments = hist(clusterLabels_cur,1:k_uniqCount);
            disp(['histOfLabelAssignments = ' mat2str(sort(histOfLabelAssignments))]);
        end
        
        if displayLevel>=2
            disp(['There are ' num2str(medoidCount) ' medoids found']);
            disp('Part of distance matrix showing cluster medoids distances:');
            disptable(distMat(medoidIDsOdCluster,medoidIDsOdCluster));
        end
        
        %clusterLabels_med2 = clusterLabels_cur(sampleIndsVec_cur(medoidIDsOdCluster));
        imageCell = cell(1,k_uniqCount);
        
        if clusteringParams.groupClusterLabels
            [confMatBest, uniqueMappingLabels, confAccBestVec(k), outStruct, h1] = groupClusterLabelsIntoGroundTruth(clusterLabels_cur, handsStruct.detailedLabels(:,6), struct('gtLabelsGrouping', gtLabelsGrouping,'printResults', false,'figID', 1)); 
            newKlusterCell      = outStruct.newKlusterCell;
            clusterLabels_best  = outStruct.clusLabels_Mapped;            
            kReduced(k) = length(newKlusterCell);

            if displayLevel>=1
                disp(['Best confusion matrix for cluster count(' num2str(k) ') is found with k=' num2str(kReduced(k)) ' and the acc(' num2str(confAccBestVec(k),'%4.2f') '[' num2str(sum(diag(confMatBest))) '/' num2str(sum(confMatBest(:))) ']' ')']);
                disptable(confMatBest);
            end
            if displayLevel>=2
                disptable(uniqueMappingLabels, 'originalLabels|userMappedLabels|bestConfusionLabels');
            end

            clusterBestConfusionImageFileName = [clusterResultFolder filesep 'clusterResult_k' num2str(k,'%02d') '_conf_best.png'];
            saveas(h1, clusterBestConfusionImageFileName);
            %clusterOriginalConfusionImageFileName = [clusterResultFolder filesep 'clusterResult_k' num2str(k,'%02d') '_conf_orig.png'];
            %saveas(h2, clusterOriginalConfusionImageFileName);

            %I want to have - for each cluster
            % 1 : medoidCluster visual
            % 2 : 1 sample from labels that are assigned to this cluster - Ground truth label(1-1) is assigned to US-cluster(2)
            % 3 : all samples that are assigned into this group

            %As a summary of all clusters     
            % 1 : the confusion matrix
            sampleIndsVec_best = plotDistanceMatrixAsData(distMat, 2, clusterLabels_best, []);
            sortedClusterLabels = clusterLabels_best(sampleIndsVec_best);   
            if saveAllClusterResultImages
                for j = 1:length(newKlusterCell) %cols   
                    [h2, imageCell{j}] = createCollageImForAKluster(k_uniqCount, j, confAccBestVec, srcFold, handsStruct, sortedClusterLabels, sampleIndsVec_best, uniqueMappingLabels, newKlusterCell, medoidIDsOdCluster, randomColors, imCollage_imSizeMax, figID);
                    %save h2 as clusterResult_k01_02.png
                    %under
                    imFullName = [clusterResultFolder filesep 'clusterResult_k' num2str(k,'%02d') '_' num2str(j,'%02d') '.png'];
                    maximizeFigureWindow(h2);
                    saveas(h2, imFullName);
                end
            end
            
            clusterResultCells{ki,1} = k;
            clusterResultCells{ki,2} = 100*confAccBestVec(k);
            clusterResultCells{ki,3} = length(sampleIndsVec_best);
            trueSamplesCnt = sum(diag(confMatBest));
            falseSamplesCnt = clusterResultCells{ki,3}-trueSamplesCnt;
            clusterResultCells{ki,4} = falseSamplesCnt;
            falseSampleInds = find(outStruct.gtLabels_ReMapped(:)~=outStruct.clusLabels_Mapped(:));
            falseSampleDetails = [handsStruct.detailedLabels(falseSampleInds,:) reshape(outStruct.clusLabels_Mapped(falseSampleInds),[],1)];
            clusterResultCells{ki,5} = falseSampleDetails;
        else
            %I have k clusters
            %clusterLabels_cur are my cluster labels
            sampleIndsVec = plotDistanceMatrixAsData(distMat, 2, clusterLabels_cur, []);
            sortedClusterLabels = clusterLabels_cur(sampleIndsVec);
            [G, uniqueMappingLabels, uniqueGTLabels, gtLabelsGrouping] = getUniqueMappingLabels(handsStruct.detailedLabels(:,6), gtLabelsGrouping);
            medoidsClusterIDs = clusterLabels_cur(medoidIDsOdCluster);
            assert(length(unique(medoidsClusterIDs))==k,'this must hold');
            newKlusterCell = num2cell(medoidsClusterIDs);
            for j = 1:k_uniqCount %cols   
                [h2, imageCell{j}] = createCollageImRandomMatch(j, srcFold, handsStruct, sortedClusterLabels, sampleIndsVec, newKlusterCell, medoidIDsOdCluster, randomColors, imCollage_imSizeMax, figID);
                %save h2 as clusterResult_k01_02.png
                %under
                imFullName = [clusterResultFolder filesep 'clusterResult_k' num2str(k,'%02d') '_' num2str(j,'%02d') '.png'];
                saveas(h2, imFullName);
            end            
        end
    end
    k_confAccBestVec = [reshape(1:kMax,kMax,[]) kReduced(:) confAccBestVec(:)];
    k_confAccBestVec = k_confAccBestVec(kVec,:);
    uniqLabelsIn = unique(handsStruct.detailedLabels(:,end));
    labelsCount = length(uniqLabelsIn);
    confAccBest_kSpecificity = (k_confAccBestVec(:,2)-1)/(labelsCount-1);
    confAccBest_similarity = k_confAccBestVec(:,3).*confAccBest_kSpecificity;
    confAccBest = [k_confAccBestVec confAccBest_kSpecificity confAccBest_similarity];
    if clusteringParams.groupClusterLabels
        confAccBest(:,3:5) = confAccBest(:,3:5)*100;
        try
            confAccBest = sortrows(confAccBest,5,'descend');
        catch
            confAccBest = sortrows(confAccBest,-5);
        end
    end
%     disptable(confAccBest, 'originalClusterCount|reducedKlusterCount|confusionAccuracy|kSpecificity|combinedPrecision',[],'%4.2f',1);    
end

function removePrevClusterResults(clusterResultFolder)
    [fileNames, numOfFiles] = getFileList(clusterResultFolder, '.png');
    for i=1:numOfFiles
        fn = [clusterResultFolder filesep fileNames{i}];
        delete(fn);
    end
end

function [kMax, kVec] = fwf_get_k_params(clusteringParams)
    if isfield(clusteringParams,'kMax')
        kMax = clusteringParams.kMax;
        kVec = 1:clusteringParams.kMax;
    elseif isfield(clusteringParams,'kVec')
        kVec = clusteringParams.kVec;
        kMax = max(kVec);
    else
        error('wtf');
    end
end

function hSummary = displayClusterresults_01(srcFold, distMat, handsStruct, medoidHist_Best, clusterLabelsCell, sampleIndsVecCell, medoidCouplesCell, gtLabelsGrouping, figID)
    kMax = size(medoidHist_Best,1);
    handsStruct_detailedLabels_colCnt = size(handsStruct.detailedLabels,2);
    lh1_rh2_bh3_str = {'LH','RH','BH'};    
    
    %each row if showing a clusters samples
    %col-i is the ith cluster center
    rC = kMax-1;
    cC = kMax;
    confAccBestVec = zeros(1,kMax);
    hSummary = figure(figID);clf;hold on;
    for k=2:kMax %rows
        clusterLabels_cur = clusterLabelsCell{1,k};
        sampleIndsVec_cur = sampleIndsVecCell{1,k};
        medoidCouples_cur = medoidCouplesCell{1,k};              
        medoidIDsOdCluster = medoidHist_Best(k, kMax-k+1 : kMax);
        medoidIDsOdCluster(medoidIDsOdCluster<0) = [];
        clusterLabels_med1 = clusterLabels_cur(medoidIDsOdCluster);%-this is the correct way of checking which cluster the sample is assigned to
        disp(['cluster summary for k=' num2str(k) ' is :']);
        printClusterSummary(clusterLabels_cur);
        disp('Part of distance matrix showing cluster medoids distances:');
        disptable(distMat(medoidIDsOdCluster,medoidIDsOdCluster));
        %clusterLabels_med2 = clusterLabels_cur(sampleIndsVec_cur(medoidIDsOdCluster));
        imageCell = cell(1,k);
        if handsStruct_detailedLabels_colCnt==6
            [confMatBest, uniqueMappingLabels, confAccBestVec(k), outStruct] = groupClusterLabelsIntoGroundTruth(clusterLabelsCell{1,k}, handsStruct.detailedLabels(:,6), struct('gtLabelsGrouping', gtLabelsGrouping,'printResults', true,'figID', 1));
            clusterLabelsGroupingVec = outStruct.clusterLabelsGroupingVec;
            clear outStruct;
        else
            confAccBestVec(k)=100;
            clusterLabelsGroupingVec=1:k;
        end
        
        for im = 1:length(medoidIDsOdCluster) %cols
            %im_k = sampleIndsVec_cur(medoidIDsOdCluster(im));
            im_k = medoidIDsOdCluster(im);
            dLab_k = handsStruct.detailedLabels(im_k,:);%detailedLabels [signID userID repID lh1_rh2_bh3 imageIDInVideo]
            usrFold_k = ['User_' num2str(dLab_k(2)) '_' num2str(dLab_k(3))];
            if handsStruct_detailedLabels_colCnt==5
                fName_k = [lh1_rh2_bh3_str{dLab_k(4)} '_' num2str(dLab_k(end),'%03d') ];
            elseif handsStruct_detailedLabels_colCnt==6
                fName_k = [lh1_rh2_bh3_str{dLab_k(4)} '_' num2str(dLab_k(end-1),'%03d') ];
            end       
            imageCell{im} = imread([srcFold filesep num2str(dLab_k(1)) filesep usrFold_k filesep 'cropped' filesep 'crop_' fName_k '.png']);
            figure(3);hold on;
            subplot(rC, cC, sub2ind([cC,rC],im,k-1));%row:k-1, col-im
            %cl_str = ['<' num2str(clusterLabels_med1(im)) ',' num2str(clusterLabels_med2(im)) '>'];
            usr_str = ['s(' num2str(dLab_k(1)) ')-u(' num2str(dLab_k(2)) '-' num2str(dLab_k(3))];
            if handsStruct_detailedLabels_colCnt==6
                gv_str = [',g(' num2str(clusterLabelsGroupingVec(im)) ')'];
            else
                gv_str = '';
            end
            if im==1 && handsStruct_detailedLabels_colCnt==6
                accStr = [',acc(' mat2str(confAccBestVec(k), 5) ')'];
            else
                accStr = '';
            end
            image(imageCell{im});title([usr_str '_{' strrep(fName_k,'H_','-') '}),k(' num2str(im) '-' num2str(k) ')' gv_str accStr]);
        end 
    end
end