function [ ] = display_digit( index , digitsVector )    
    digit = reshape(digitsVector(index,:) , [28,28]);
    imshow(transpose(digit))    
end

