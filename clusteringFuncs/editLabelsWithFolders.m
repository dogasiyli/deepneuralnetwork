function [problematicVids, changedLabelsSummary_All, summaryCells] = editLabelsWithFolders(signID, copyFrames1_editLabels2, optParamStruct)

    %copyFrames1_editLabels2 == 1
    %- copy the frames from srcFold to saveToFolder; according to the labels from srcFold

    %copyFrames1_editLabels2 == 2
    %- update labels according to the frames in nolabel and labelledFrames folders
    %- update labels at -> srcFold or saveToFolder folder??

    %optParamStruct_editLabelsWithFolders = struct('saveToFolder', 'D:\editLabelsFrames');
    %pv = editLabelsWithFolders(7, 1, optParamStruct_editLabelsWithFolders);
    if ~exist('optParamStruct','var')
        optParamStruct=[];
    end
    srcFold = getStructField(optParamStruct, 'srcFold', getVariableByComputerName('srcFold'));
    userList = getStructField(optParamStruct, 'userList', 2:7);
    maxRptCnt = getStructField(optParamStruct, 'maxRptCnt', 20);
    saveToFolder = getSaveToFolder(optParamStruct, 'saveToFolder', srcFold);
    updateLabelsAt = getSaveToFolder(optParamStruct, 'updateLabelsAt', 'srcFold');
    absenceOfBH = getSaveToFolder(optParamStruct, 'absenceOfBH', 'copyDiverseHand');%['doNothing','copyLH','copyDiverseHand','singleToBothVec']
    sbVec = getSaveToFolder(optParamStruct, 'sbVec', []);
    if isempty(sbVec) && strcmpi(absenceOfBH,'singleToBothVec')
        absenceOfBH = 'copyDiverseHand';
    end
    
%     framesOpts = struct('bigVideoShow',false,'bigVideoSaveScale',0,'show',false,'save',true);
%     optParamStruct_cropVidFrames = struct('srcFold',srcFold,...
%                                           'saveToFoldRoot',saveToFolder,...
%                                           'makeZippedFolder',false,...
%                                           'removeUnzippedFolder',false,...
%                                           'framesOpts', framesOpts,...
%                                           'mirrorOnSaveStruct',struct('mirrorLH',true,'mirrorRH',false,'mirrorBH',false),...
%                                           'multipliers',struct('bigHandFaceRatio',[9/8 3/2],'smlHandFaceRatio',[1, 2/3]),...
%                                           'resizeImVec', [132 92],... 
%                                           'enforceReCreateNew', false);

    %not to get an error on 'blah blah not returned :)'
    summaryCells = [];
    problematicVids = [];
    changedLabelsSummary_All = [];
    
    [foldersToSaveCreated, struct_singleSign, struct_bothSign] = create_labelledFramesFolders(signID, srcFold, saveToFolder);
    if (foldersToSaveCreated==0)
        return
    end
    
    %1. go to the userFolder
    [surList, listElemntCnt] = createSURList(srcFold, 2, struct('signIDList',signID,'userID',userList,'maxRptCnt',maxRptCnt));
    frameOfSignCopiedVec = {[];[];[]};
    for l = 1:listElemntCnt
        s = surList(l,1);
        u = surList(l,2);
        r = surList(l,3);
        clear labelsVec_LH labelsVec_RH labelsVec_BH
        foldName_user = [srcFold filesep num2str(s) filesep 'User_' num2str(u) '_' num2str(r)];
        switch updateLabelsAt
            case 'srcFold'
                foldName_labels = [srcFold filesep num2str(s) filesep 'User_' num2str(u) '_' num2str(r) filesep 'labelFiles' filesep];
            otherwise %'saveToFolder'
                foldName_labels = [saveToFolder filesep num2str(s) filesep 'User_' num2str(u) '_' num2str(r) filesep 'labelFiles' filesep];
        end
        foldName_cropped = [foldName_user filesep 'cropped' filesep];
        foldName_noLabel = [saveToFolder filesep num2str(signID) filesep 'noLabel' filesep];
        if (fwf_createFolder(foldName_noLabel)==0), problematicVids = [problematicVids;s u r], continue, end
        newNameInitial = ['s' num2str(s,'%03d') '_u' num2str(u,'%02d') '_r' num2str(r,'%02d') '_'];
        
        if exist(foldName_labels,'dir')==0
            disp(['Labels folder(' foldName_labels ') not exist. skipping']);
            problematicVids = [problematicVids;s u r]; %#ok<AGROW>
            continue
        end
        
        %labelFiles folder must exist
        
        %cropped    folder must exist
        %   if not cropped.zip must be extracted
        %      if not cropped.zip must be created using "color.mp4 and skeleton.mat"
        %          if not skip
        if copyFrames1_editLabels2==1
            cropFolderExist = cropVidFrames(s, u, r, struct('removeUnzippedFolder', false));
            if ~cropFolderExist
                disp(['Cropped Images Folder under (' foldName_cropped ') not exist. skipping']);
                problematicVids = [problematicVids;s u r];
                continue
            end
        %elseif copyFrames1_editLabels2==2
        end
        
        try
            if exist([foldName_labels 'labelsCS_LH.txt'],'file')
                labelsVec_LH = getLabelVecFromFile([foldName_labels 'labelsCS_LH.txt'], false);
                if (copyFrames1_editLabels2==1)
                    foc = applyCopyingFromSrcToEditFolders(struct_singleSign, labelsVec_LH, 'LH', foldName_cropped, foldName_noLabel, newNameInitial);
                    frameOfSignCopiedVec{1,1} = addFramesOfSignCopied(frameOfSignCopiedVec{1,1}, foc);
                elseif (copyFrames1_editLabels2==2)
                    [labelsVecNew, changedLabelsSummary] = getNewLabelsVecFromEditFolders([s u r], struct_singleSign, labelsVec_LH, 'LH', foldName_labels, foldName_noLabel, newNameInitial);
                    changedLabelsSummary_All = [changedLabelsSummary_All;changedLabelsSummary]; %#ok<*AGROW>
                end
            else
                labelsVec_LH = [];
            end
            if exist([foldName_labels 'labelsCS_RH.txt'],'file')
                labelsVec_RH = getLabelVecFromFile([foldName_labels 'labelsCS_RH.txt'], false);
                if (copyFrames1_editLabels2==1)
                   foc = applyCopyingFromSrcToEditFolders(struct_singleSign, labelsVec_RH, 'RH', foldName_cropped, foldName_noLabel, newNameInitial);
                   frameOfSignCopiedVec{2,1} = addFramesOfSignCopied(frameOfSignCopiedVec{2,1}, foc);
                elseif (copyFrames1_editLabels2==2)
                    [labelsVecNew, changedLabelsSummary] = getNewLabelsVecFromEditFolders([s u r], struct_singleSign, labelsVec_RH, 'RH', foldName_labels, foldName_noLabel, newNameInitial);
                    changedLabelsSummary_All = [changedLabelsSummary_All;changedLabelsSummary];
                end
            else
                labelsVec_RH = [];
            end
            if exist([foldName_labels 'labelsCS_BH.txt'],'file')
                labelsVec_BH = getLabelVecFromFile([foldName_labels 'labelsCS_BH.txt'], false);
                if (copyFrames1_editLabels2==1)
                    foc = applyCopyingFromSrcToEditFolders(struct_bothSign, labelsVec_BH, 'BH', foldName_cropped, foldName_noLabel, newNameInitial);
                    frameOfSignCopiedVec{3,1} = addFramesOfSignCopied(frameOfSignCopiedVec{3,1}, foc);
                elseif (copyFrames1_editLabels2==2)
                    [labelsVecNew, changedLabelsSummary] = getNewLabelsVecFromEditFolders([s u r], struct_bothSign, labelsVec_BH, 'BH', foldName_labels, foldName_noLabel, newNameInitial);
                    changedLabelsSummary_All = [changedLabelsSummary_All;changedLabelsSummary];
                end
            else
                warning(['labelsCS_BH.txt is missing and absenceOfBH option = ' absenceOfBH]);
                switch absenceOfBH
                    case 'copyLH'
                        labelsVec_BH = labelsVec_LH;
                        foc = applyCopyingFromSrcToEditFolders(struct_bothSign, labelsVec_BH, 'BH', foldName_cropped, foldName_noLabel, newNameInitial);
                        frameOfSignCopiedVec{3,1} = addFramesOfSignCopied(frameOfSignCopiedVec{3,1}, foc);
                        writeLabelVecToFile(foldName_labels, labelsVec_BH, 3, false);
                    case 'copyDiverseHand'
                        lhCnt = length(unique(labelsVec_LH));
                        rhCnt = length(unique(labelsVec_RH));
                        if rhCnt>lhCnt
                            labelsVec_BH = labelsVec_RH;
                        else
                            labelsVec_BH = labelsVec_LH;
                        end
                        foc = applyCopyingFromSrcToEditFolders(struct_bothSign, labelsVec_BH, 'BH', foldName_cropped, foldName_noLabel, newNameInitial);
                        frameOfSignCopiedVec{3,1} = addFramesOfSignCopied(frameOfSignCopiedVec{3,1}, foc);
                        writeLabelVecToFile(foldName_labels, labelsVec_BH, 3, false);
                    case 'singleToBothVec'
                        if isempty(sbVec)
                            labelsVec_BH = [];
                        else
                            labelsVec_BH = zeros(size(labelsVec_LH));
                            for i = 1:length(sbVec)
                                labelsVec_BH(labelsVec_LH==i) = sbVec(i);
                                labelsVec_BH(labelsVec_RH==i) = sbVec(i);
                            end
                            foc = applyCopyingFromSrcToEditFolders(struct_bothSign, labelsVec_BH, 'BH', foldName_cropped, foldName_noLabel, newNameInitial);
                            frameOfSignCopiedVec{3,1} = addFramesOfSignCopied(frameOfSignCopiedVec{3,1}, foc);   
                            writeLabelVecToFile(foldName_labels, labelsVec_BH, 3, false);
                        end
                    otherwise % case 'doNothing'
                        labelsVec_BH = [];
                end
            end
        catch err %#ok<NASGU>
            problematicVids = [problematicVids;s u r];
        end
    end
    if ~isempty(changedLabelsSummary_All)
        disptable(changedLabelsSummary_All,'sign|user|rep|hand|frame|oldLabel|newLabel');
        disp(['Total ' num2str(size(changedLabelsSummary_All,1)) ' frames changed the label'])
    end
    if (copyFrames1_editLabels2==1)
        summaryCells = summarizeFrameOfSignCopiedVec(frameOfSignCopiedVec, struct_singleSign, struct_bothSign);
    end
end

function focMain = addFramesOfSignCopied(focMain, focNew)
    if isempty(focMain)
        focMain = focNew;
    else
        focMain = focMain + focNew;
    end
end

function summaryCells = summarizeFrameOfSignCopiedVec(frameOfSignCopiedVec, struct_singleSign, struct_bothSign)
    %cell 1 LH, 2 RH, 3 BH
    summaryCells = struct;
    summaryCells.singleHand = [];
    summaryCells.bothHand = [];
    if ~isempty(frameOfSignCopiedVec{1,1}) && sum(frameOfSignCopiedVec{1,1})>0
        frameOfSignCopiedVecLH = frameOfSignCopiedVec{1,1};
        frameOfSignCopiedVecRH = frameOfSignCopiedVec{2,1};
        numOfLabels = length(frameOfSignCopiedVecLH) - 1;
        summaryCellsSingleHand = cell(numOfLabels,3);
        summaryCellsSingleHand{1,1} = 'noLabel';
        summaryCellsSingleHand{1,2} = num2str(frameOfSignCopiedVecLH(1));
        summaryCellsSingleHand{1,3} = num2str(frameOfSignCopiedVecRH(1));
        for i=1:numOfLabels
            signName = strsplit(struct_singleSign.folderNames{i,1}, filesep);
            signName = signName{end};
            summaryCellsSingleHand{i+1,1} = signName;
            summaryCellsSingleHand{i+1,2} = num2str(frameOfSignCopiedVecLH(i+1));
            summaryCellsSingleHand{i+1,3} = num2str(frameOfSignCopiedVecRH(i+1));
        end
        summaryCells.singleHand = summaryCellsSingleHand;
    end
    if ~isempty(frameOfSignCopiedVec{3,1}) && sum(frameOfSignCopiedVec{3,1})>0
        frameOfSignCopiedVecBH = frameOfSignCopiedVec{3,1};
        numOfLabels = length(frameOfSignCopiedVecBH) - 1;
        summaryCellsBothHand = cell(numOfLabels,2);
        summaryCellsBothHand{1,1} = 'noLabel';
        summaryCellsBothHand{1,2} = num2str(frameOfSignCopiedVecBH(1));
        for i=1:numOfLabels
            signName = strsplit(struct_bothSign.folderNames{i,1}, filesep);
            signName = signName{end};
            summaryCellsBothHand{i+1,1} = signName;
            summaryCellsBothHand{i+1,2} = num2str(frameOfSignCopiedVecBH(i+1));
        end
        summaryCells.bothHand = summaryCellsBothHand;
    end
    if ~isempty(summaryCells.singleHand)
        summaryCells.singleHand
    end
    if ~isempty(summaryCells.bothHand)
        summaryCells.bothHand
    end
end

%applyCopyingFromSrcToEditFolders(struct_singleSign, labelsVec_LH, 'LH', foldName_cropped, newNameInitial)
function frameOfSignCopiedVec = applyCopyingFromSrcToEditFolders(struct_Sign, labelsVec, XH, foldName_cropped, foldName_noLabel, newNameInitial)
    cnt_Signs = struct_Sign.labelCount;
    frameOfSignCopiedVec = zeros(1,cnt_Signs+1);
    problematicFrames = ones(1,length(labelsVec));
    for i=0:cnt_Signs
        if i==0
            saveToFoldNameCur = foldName_noLabel;
        else
            saveToFoldNameCur = struct_Sign.folderNames{i,1};
        end
        %now for all frames that fall under this label shall be
        %copied under 'foldNameCur' by renaming accordingly
        %sXXX_uXX_rXX_?H_XXX.png
        assignedFrameIDs = find(labelsVec==i);
        for f=assignedFrameIDs
            fileNameToRead = ['crop_' XH '_' num2str(f,'%03d') '.png'];
            fullFileNameToRead = cleanPathString([foldName_cropped fileNameToRead]);
            fullSaveFileName = cleanPathString([saveToFoldNameCur filesep newNameInitial strrep(fileNameToRead,'crop_','')]);
            if exist(fullSaveFileName,'file')==0
                if exist(fullFileNameToRead,'file')==2
                    %will proceed after if :)
                elseif f==length(labelsVec)
                    fileNameToRead = ['crop_' XH '_' num2str(f-1,'%03d') '.png'];
                    fullFileNameToRead = cleanPathString([foldName_cropped fileNameToRead]);
                    assert(exist(fullFileNameToRead,'file')==2,['<' fullFileNameToRead '> must exist.']);
                    fullSaveFileName = cleanPathString([saveToFoldNameCur filesep newNameInitial strrep(['crop_' XH '_' num2str(f,'%03d') '.png'],'crop_','')]);
                    warning(['copying last label from last minus one (' fullFileNameToRead ') to (' fullSaveFileName ')']);
                else
                    error(['<' fullFileNameToRead '> not exists to copy to <' fullSaveFileName '>'])
                end
                [status,message] = copyfile(fullFileNameToRead,fullSaveFileName,'f');
                if status~=1 && ~isempty(message)
                    disp(message);
                    error(['problem with copying file (' fullFileNameToRead ') to (' fullSaveFileName ')']);
                else
                    frameOfSignCopiedVec(i+1) = frameOfSignCopiedVec(i+1) +1;
                    problematicFrames(f) = 0;
                end                
            else
                frameOfSignCopiedVec(i+1) = frameOfSignCopiedVec(i+1) +1;
                problematicFrames(f) = 0;
            end
        end
    end   
    disp(['Summary of ' num2str(length(labelsVec)) ' : ' mat2str(frameOfSignCopiedVec) ' --> sum(' num2str(sum(frameOfSignCopiedVec)) ')'])
    if sum(frameOfSignCopiedVec)~=length(labelsVec)
        disp(['some frames not copied for (' foldName_cropped ', ' XH ')= ' mat2str(find(problematicFrames==1))])
        error('Problem while copying frames to designated folders')
    end
end

function [labelsVecNew, changedLabelsSummary] = getNewLabelsVecFromEditFolders(surVec, struct_Sign, labelsVecOld, XH, foldName_labels, foldName_noLabel, edittedNameInitial)
    cnt_Signs = struct_Sign.labelCount;
    labelCnt = length(labelsVecOld);
    labelsVecNew = -ones(size(labelsVecOld));
    changedLabelsSummary = [];
    for f=1:labelCnt
        fileNameOriginal = ['crop_' XH '_' num2str(f,'%03d') '.png'];
        fullSaveFileNameToCheck_ending = [edittedNameInitial strrep(fileNameOriginal,'crop_','')];
        for i=0:cnt_Signs
            if i==0
                edittedFoldNameCur = foldName_noLabel;
            else
                edittedFoldNameCur = struct_Sign.folderNames{i,1};
            end
            %now for all frames that fall under this label shall be
            %copied under 'foldNameCur' by renaming accordingly
            %sXXX_uXX_rXX_?H_XXX.png
            fullSaveFileNameToCheck = [edittedFoldNameCur filesep fullSaveFileNameToCheck_ending];
            if exist(fullSaveFileNameToCheck,'file')==2
                %newLabel is this
                labelsVecNew(f) = i;
            end
        end
    end
    nonLabelledFrames = find(labelsVecNew<0);
    if ~isempty(nonLabelledFrames)
        disp(['There are non-labelled frames in vid' mat2str(surVec) '. ' XH '-Check frames ' mat2str(nonLabelledFrames)]);
        return
    end
    changedLabelsIDs = reshape(find(labelsVecOld~=labelsVecNew),[],1);
    changedLabelCnt = size(changedLabelsIDs,1);
    handID = 0;
    switch(XH)
        case 'LH'
            handID = 1;
        case 'RH'
            handID = 2;
        case 'BH'
            handID = 3;
    end
    if changedLabelCnt>0
        changedLabelsSummary = [surVec(1)*ones(changedLabelCnt,1) surVec(2)*ones(changedLabelCnt,1) surVec(3)*ones(changedLabelCnt,1) handID*ones(changedLabelCnt,1) changedLabelsIDs labelsVecOld(changedLabelsIDs)' labelsVecNew(changedLabelsIDs)'];
        %disp(changedLabelsSummary);
        disptable(changedLabelsSummary,'sign|user|rep|hand|frame|oldLabel|newLabel');
        updateLabelText(surVec, XH, foldName_labels, labelsVecNew);
    end
end

function updateLabelText(surVec, XH, foldName_labels, labelsVecNew)
    nonLabelledFrames = find(labelsVecNew<0);
    if ~isempty(nonLabelledFrames)
        disp(['There are non-labelled frames in vid' mat2str(surVec) '. Check frames ' mat2str(nonLabelledFrames)]);
        return
    end
    srcFileName = [foldName_labels 'labelsCS_' XH '.txt'];
    fileInfo_src = dir(srcFileName);
    [sY, sM, sD, sH, sMN, sS] = datevec(fileInfo_src.datenum);  
    srcInt = sS + sMN*100 + sH*10000 + sD*1000000 + sM*100000000 + sY*10000000000;
    srcBackup = strrep(srcFileName, '.txt', ['_' num2str(srcInt,'%d') '.txt']);
    disp(['old label file = ' fileInfo_src.date])
    copyfile(srcFileName,srcBackup,'f');
    
    fileID = fopen(srcFileName,'w','n','windows-1258');
    fprintf(fileID,'%d\r\n',labelsVecNew);
    fclose(fileID);
    
    disp(['old labels(' srcFileName ') updated with new labels'])
end
%create_labelledFramesFolders(signID, srcFold, saveToFolder)
function [foldersCreated, struct_singleSign, struct_bothSign] = create_labelledFramesFolders(signID, srcFold, saveToFolder)
    foldersCreated = false;
    struct_singleSign = [];
    struct_bothSign = [];
    
    foldName_labelledFrames = [saveToFolder filesep num2str(signID) filesep 'labelledFrames' filesep];
    if (fwf_createFolder(foldName_labelledFrames)==0), return, end
    
    clusterNames_single = getClusterNamesAssignedFromTxt(srcFold, struct('signIDList',signID,'single0_double1',0));
    clusterNames_both = getClusterNamesAssignedFromTxt(srcFold, struct('signIDList',signID,'single0_double1',1));
    
    cnt_singleSigns = length(clusterNames_single);
    cnt_bothSigns = length(clusterNames_both);
    
    %createFolders
    struct_singleSign = struct('labelCount', cnt_singleSigns);
    struct_bothSign = struct('labelCount', cnt_bothSigns);
    
    struct_singleSign.folderNames = cell(cnt_singleSigns,1);
    struct_bothSign.folderNames = cell(cnt_singleSigns,1);
    
    for i=1:cnt_singleSigns
        struct_singleSign.folderNames{i,1} = [foldName_labelledFrames clusterNames_single{i}];
        if (fwf_createFolder(struct_singleSign.folderNames{i,1})==0), return, end
    end
    for i=1:cnt_bothSigns
        struct_bothSign.folderNames{i,1} = [foldName_labelledFrames clusterNames_both{i}];
        if (fwf_createFolder(struct_bothSign.folderNames{i,1})==0), return, end
    end
    foldersCreated = true;
end

function foldersCreated = fwf_createFolder(foldName_to_create)
    foldersCreated = true;
    try
        [labelledFrames_Created, errMsg] = createFolderIfNotExist(foldName_to_create);
        if ~labelledFrames_Created
            disp(errMsg);
            foldersCreated = false;
            return
        end
    catch err
        disp(err.message);
        foldersCreated = false;
    end
end
