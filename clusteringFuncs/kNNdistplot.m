function [ optimalEps ] = kNNdistplot( pts, k )

    dist = sort(pdist2(pts,pts));
    kNeighbourhhood = dist(2:k,:);
    meanNeighbourhood = mean(kNeighbourhhood,1);
    
    optimalEps = sort(meanNeighbourhood);

end

