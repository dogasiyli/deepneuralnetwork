function [labels , digits ] = read_data( filepath )
    data = csvread(filepath , 1,0);
    digits = data(:,[2:785]);
    labels = data(:,1);
end

