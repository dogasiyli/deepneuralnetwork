function [newGroupCell, newKlusterCell, extraOutputStruct] = relationShip2NewClusterGroupLabels( relationShipTable, confMatGK )
%relationShip2NewClusterGroupLabels a relationShipTable has G rows and K
%cols. This function gives the best grouping of G and K's such that;
    %TYPE-1 : 0K-1G nomatch - a groundTruth group can not be represented by any of the clusters
    %TYPE-2 : 1K-0G nomatch - a cluster can not represent any groundTruth group
    %TYPE-3 : 1K-1G can be a match - a cluster fully represents a groundTruth group
    %TYPE-4 : 1K-NG can be a match - a cluster fully represents a group of groundTruth groups
    %TYPE-5 : NK-1G can be a match - a group of clusters represent a groundTruth group
    %TYPE-6 : NK-NG can be a match - a group of clusters represent a group of groundTruth groups
    [G,K] = size(relationShipTable);
    gTypes = NaN(1,G);%types can be - 1,3,5 / types 2,4 can be understood from kLoop / type 6 is the remaining after both loops
    kTypes = NaN(1,K);%types can be - 2,3,4 / types 1,5 can be understood from gLoop / type 6 is the remaining after both loops
    gkBestMapGroupingMat = NaN(G,K);
    newGroupCell = cell(G,1);
    newGroupCount = 0;
    newKlusterCell = cell(1,K);
    newKlusterCount = 0;
    for k=1:K
        curK = reshape([relationShipTable{:,k}],2,[])';
        typeIdentifier_01 = sum(curK>0);%[how many G are assigned to k, how many of G's are included in k]
        if sum(typeIdentifier_01)==0
            %if sum(typeIdentifier_01) is zero this makes cur K has no relation to any G
            kTypes(k) = 2;
            %there should be all NaNs in colK
            assert(any(gkBestMapGroupingMat(:,k))==0,['there are some values assigned in col(' num2str(k) ')-' mat2str(gkBestMapGroupingMat(:,k))])
        elseif sum(typeIdentifier_01)==1
            %check the matching row
            matchingG = find(curK(:,typeIdentifier_01>0));
            matchingGroupRow = reshape([relationShipTable{matchingG,:}],2,[])';
            matchingKVec = find(sum(matchingGroupRow,2)>0);
            
            %we are sure that matchingGroupRow(matchingG, k) is bigger than 0
            assert(sum(matchingGroupRow(k, :))>0,'The value has to be bigger than zero');
            
            typeIdentifier_02 = sum(matchingGroupRow>0);%[how many K are assigned to matchingG, how many of K's are included in matchingG]
            if typeIdentifier_02(1)>0 && typeIdentifier_02(2)>0
                %TYPE-4 : 1K-NG can be a match - a cluster fully represents a group of groundTruth groups
                %TYPE-5 : NK-1G can be a match - a group of clusters represent a groundTruth group
                %TYPE-6 : NK-NG can be a match - a group of clusters represent a group of groundTruth groups            

                %try removing the relation of k with thse G's, then check if
                %the have any other relationship to any other k
                chkTbl = sum(reshape(cell2mat(relationShipTable(:,matchingKVec)),G,2,length(matchingKVec)),3);
                matchingGVec = find(sum(chkTbl,2)>0);
                if length(matchingKVec) == 1 && length(matchingGVec)>1
                    %TYPE-4 : 1K-NG can be a match - a cluster fully represents a group of groundTruth groups
                    assert(matchingKVec(1)==k,['matchingKVec(1) must equal to ' num2str(k) ' not ' num2str(matchingKVec(1))])
                    kTypes(k) = 4;
                    gkBestMapGroupingMat(matchingGVec,k) = 1;                
                elseif length(matchingKVec)>1 && length(matchingGVec)==1
                    %TYPE-5 : NK-1G can be a match - a group of clusters represent a groundTruth group  
                    if isnan(gTypes(matchingGVec))
                        gTypes(matchingGVec) = 5;%NK-1G
                    else
                        assert(gTypes(matchingGVec)==5,['gTypes(' num2str(matchingGVec) ') must equal to 5 not ' num2str(gTypes(matchingGVec))]);
                        assert(gkBestMapGroupingMat(matchingGVec,k)==1,['gkBestMapGroupingMat(' num2str(matchingGVec) ',' num2str(k) ') must equal to 1 not ' num2str(gkBestMapGroupingMat(matchingGVec,k))]);
                    end
                    kTypes(matchingKVec) = 5;
                    gkBestMapGroupingMat(matchingGVec,matchingKVec)=1;
                elseif length(matchingKVec)>1 && length(matchingGVec)>1
                    %TYPE-6 : NK-NG can be a match - a group of clusters represent a group of groundTruth groups            
                    kTypes(k) = 6;
                    gTypes(matchingGVec) = 6;%NK-NG
                    gkBestMapGroupingMat(matchingGVec,matchingKVec) = 1;
                end                         
            elseif (typeIdentifier_02(1)==1 && typeIdentifier_02(2)==0) || (typeIdentifier_02(1)==0 && typeIdentifier_02(2)==1)
                %TYPE-3 : 1K-1G can be a match - a cluster fully represents a groundTruth group
                kTypes(k) = 3;%1K-1G
                gTypes(matchingG) = 3;%1K-1G
                gkBestMapGroupingMat(matchingG,k) = 1;
            elseif typeIdentifier_02(1)==0 && typeIdentifier_02(2)>1
                %TYPE-5 : NK-1G can be a match - a group of clusters represent a groundTruth group
                %TYPE-6 : NK-NG can be a match - a group of clusters represent a group of groundTruth groups
                typeOfK_group = unique(kTypes(matchingKVec));
                if sum(isnan(kTypes(matchingKVec))) == length(matchingKVec)
                    %all will be set new
                    typeOfK_group = 5;%this is being set new as NK-1G                    
                elseif sum(isnan(kTypes(matchingKVec))) == 0
                    %do nothing - all are set to same thing - will be
                    %checked below in the first assert
                else
                    %some k are nan some are not
                    typeOfK_group(isnan(typeOfK_group)) = [];
                end
                assert(length(typeOfK_group)==1,['unique kTypes(' mat2str(typeOfK_group) ') must be single not ' num2str(length(typeOfK_group))]);
                
                switch typeOfK_group
                    case 5
                        %this was expected
                        if isnan(gTypes(matchingG))
                            gTypes(matchingG) = typeOfK_group;%NK-1G or NK-NG
                        else
                            assert(gTypes(matchingG)==typeOfK_group,['gTypes(' num2str(matchingG) ') must equal to ' num2str(typeOfK_group) ' not ' num2str(gTypes(matchingG))]);
                            assert(gkBestMapGroupingMat(matchingG,k)==1,['gkBestMapGroupingMat(' num2str(matchingG) ',' num2str(k) ') must equal to 1 not ' num2str(gkBestMapGroupingMat(matchingG,k))]);
                        end
                    case 6
                        %there is a group of g that this group of k are
                        %assigned to.. I have to check that!!!
                        %for now leave it as just assigning this g to the
                        %group with the type 6
                        if isnan(gTypes(matchingG))
                            gTypes(matchingG) = typeOfK_group;%NK-1G or NK-NG
%                         else
%                             assert(gTypes(matchingG)==typeOfK_group,['gTypes(' num2str(matchingG) ') must equal to ' num2str(typeOfK_group) ' not ' num2str(gTypes(matchingG))]);
%                             assert(gkBestMapGroupingMat(matchingG,k)==1,['gkBestMapGroupingMat(' num2str(matchingG) ',' num2str(k) ') must equal to 1 not ' num2str(gkBestMapGroupingMat(matchingG,k))]);
                        end                        
                    otherwise
                end
                kTypes(matchingKVec) = typeOfK_group;
                gkBestMapGroupingMat(matchingG,matchingKVec) = 1;
            else
                disptable(curK, 'percOfRowInCol|percOfRowIncludesCol');
                disptable(matchingGroupRow, 'percOfColGInRowKs|percOfRowKsIncludesColG');
                error('check this situation and enhance the algorithm');                
            end
        elseif sum(typeIdentifier_01)>1
            %TYPE-4 : 1K-NG can be a match - a cluster fully represents a group of groundTruth groups
            %TYPE-5 : NK-1G can be a match - a group of clusters represent a groundTruth group
            %TYPE-6 : NK-NG can be a match - a group of clusters represent a group of groundTruth groups            
            matchingGVec = find(sum(curK,2)>0);
            
            %try removing the relation of k with thse G's, then check if
            %the have any other relationship to any other k
            chkTbl = relationShipTable;
            chkTbl = chkTbl(matchingGVec,:);
            chkTbl = sum(reshape(cell2mat(chkTbl)',2,K,length(matchingGVec)),3);
            matchingKVec = find(sum(chkTbl)>0);
            uniqTypeOfKVec = unique(kTypes(matchingKVec));
            if (length(uniqTypeOfKVec)==1 && uniqTypeOfKVec==6)
                %k's are all NK-NG
                %find which g's we are talking about
                matchingGVec = find(~isnan(sum(gkBestMapGroupingMat(:,matchingKVec),2)));
                kTypes(matchingKVec) = 6;
                gTypes(matchingGVec) = 6;%NK-NG
                gkBestMapGroupingMat(matchingGVec,matchingKVec) = 1;
                %disp(['TYPE-6 : NK-NG k(' mat2str(matchingKVec) '),g(' mat2str(matchingGVec) ')']);
            elseif length(matchingKVec) == 1 && length(matchingGVec)==1
                %TYPE-3 : 1K-1G can be a match - a cluster fully represents a groundTruth group
                assert(matchingKVec(1)==k,['matchingKVec(1) must equal to ' num2str(k) ' not ' num2str(matchingKVec(1))])
                kTypes(k) = 3;
                gTypes(matchingGVec) = 3;
                gkBestMapGroupingMat(matchingGVec,k) = 1;         
                %disp(['TYPE-3: 1K-1G k(' mat2str(k) '),g(' mat2str(matchingGVec) ')']);
            elseif length(matchingKVec) == 1 && length(matchingGVec)>1
                %TYPE-4 : 1K-NG can be a match - a cluster fully represents a group of groundTruth groups
                assert(matchingKVec(1)==k,['matchingKVec(1) must equal to ' num2str(k) ' not ' num2str(matchingKVec(1))])
                kTypes(k) = 4;
                gkBestMapGroupingMat(matchingGVec,k) = 1; 
                %disp(['TYPE-4 : 1K-NG k(' mat2str(k) '),g(' mat2str(matchingGVec) ')']);
            elseif length(matchingKVec)>1 && length(matchingGVec)==1
                %TYPE-5 : NK-1G can be a match - a group of clusters represent a groundTruth group  
                if isnan(gTypes(matchingGVec))
                    gTypes(matchingGVec) = 5;%NK-1G
                else
                    assert(gTypes(matchingGVec)==5,['gTypes(' num2str(matchingGVec) ') must equal to 5 not ' num2str(gTypes(matchingGVec))]);
                    assert(gkBestMapGroupingMat(matchingGVec,k)==1,['gkBestMapGroupingMat(' num2str(matchingGVec) ',' num2str(k) ') must equal to 1 not ' num2str(gkBestMapGroupingMat(matchingGVec,k))]);
                end
                kTypes(matchingKVec) = 5;
                gkBestMapGroupingMat(matchingGVec,matchingKVec)=1;
                %disp(['TYPE-5 : NK-1G k(' mat2str(matchingKVec) '),g(' mat2str(matchingGVec) ')']);
            elseif length(matchingKVec)>1 && length(matchingGVec)>1
                %TYPE-6 : NK-NG can be a match - a group of clusters represent a group of groundTruth groups            
                kTypes(matchingKVec) = 6;
                gTypes(matchingGVec) = 6;%NK-NG
                gkBestMapGroupingMat(matchingGVec,matchingKVec) = 1;
                %disp(['TYPE-6 : NK-NG k(' mat2str(matchingKVec) '),g(' mat2str(matchingGVec) ')']);
            end         
        else
            disptable(curK, 'percOfRowInCol|percOfKIncludesCol');
            error('check this situation and enhance the algorithm');
        end
    end
    newGroupClusterMatch = [];
    for g=1:G
        assignedClustersVec = gkBestMapGroupingMat(g,:);
        assignedClustersCount = sum(assignedClustersVec==1);
        if assignedClustersCount==1
            assignedClusterID = find(assignedClustersVec==1);
            assignedGroupsVec = find(gkBestMapGroupingMat(:,assignedClusterID)==1);
            assignedGroupsCount = sum(assignedGroupsVec>0);
            if assignedGroupsCount==1
                %TYPE-3 : 1K-1G can be a match - a cluster fully represents a groundTruth group
                gTypeCur = 3;
                newGroupCount = newGroupCount + 1;
                newGroupCell{newGroupCount} = g;
                newKlusterCount = newKlusterCount + 1;
                newKlusterCell{newKlusterCount} = assignedClusterID;
                newGroupClusterMatch(newGroupCount, newKlusterCount) = gTypeCur; %#ok<AGROW>
            elseif assignedGroupsCount>1
                %TYPE-4 : 1K-NG can be a match - a cluster fully represents a group of groundTruth groups
                gTypeCur = 4;
                if g==min(assignedGroupsVec)
                    newGroupCount = newGroupCount + 1;
                    newGroupCell{newGroupCount} = assignedGroupsVec;
                    newKlusterCount = newKlusterCount + 1;
                    newKlusterCell{newKlusterCount} = assignedClusterID;
                    newGroupClusterMatch(newGroupCount, newKlusterCount) = gTypeCur; %#ok<AGROW>
                end
            else
                error('we shouldnt be here');
            end
        elseif assignedClustersCount>1
            assignedClusterIDVec = find(assignedClustersVec==1);
            assignedGroupsMat = gkBestMapGroupingMat(:,assignedClusterIDVec);
            assignedGroupIDs = find(sum(~isnan(assignedGroupsMat),2)>0);
            if length(assignedGroupIDs)>1
                %TYPE-6 : NK-NG can be a match - a group of clusters represent a group of groundTruth groups
                gTypeCur = 6;
                if g==min(assignedGroupIDs)
                    newGroupCount = newGroupCount + 1;
                    newGroupCell{newGroupCount} = assignedGroupIDs(:);
                    newKlusterCount = newKlusterCount + 1;
                    newKlusterCell{newKlusterCount} = sort(assignedClusterIDVec(:));
                    newGroupClusterMatch(newGroupCount, newKlusterCount) = gTypeCur; %#ok<AGROW>
                end
            elseif length(assignedGroupIDs)==1
                %TYPE-5 : NK-1G can be a match - a group of clusters represent a groundTruth group                
                gTypeCur = 5;
                newGroupCount = newGroupCount + 1;
                newGroupCell{newGroupCount} = assignedGroupIDs;
                newKlusterCount = newKlusterCount + 1;
                newKlusterCell{newKlusterCount} = sort(assignedClusterIDVec(:));
                newGroupClusterMatch(newGroupCount, newKlusterCount) = gTypeCur; %#ok<AGROW>
                %disp(['TYPE-5 : NK-1G k(' mat2str(assignedClusterIDVec) '),g(' mat2str(newGroupCell{newGroupCount}) ')']);
            else
                error('Check your implementation')
            end
        elseif assignedClustersCount==0
            %TYPE-1 : 0K-1G nomatch - a groundTruth group can not be represented by any of the clusters
            gTypeCur = 1;
        end
        
        if gTypes(g)==2
            %TYPE-2 : 1K-0G nomatch - a cluster can not represent any groundTruth group
            error('This is invalid type(2;1K-0G nomatch) for groundTruth groups');
        elseif isnan(gTypes(g))
            gTypes(g) = gTypeCur;
        else
            assert(gTypeCur==gTypes(g),['The types from k-loop(' num2str(gTypes(g)) ') doesnt match the one found here(' num2str(gTypeCur) ')'])
        end   
    end
    
    unassignedGroupIDs = find(gTypes==1);%there are some TYPE-1 : 0K-1G nomatch - a groundTruth group can not be represented by any of the clusters
    unassignedClusterIDs = find(kTypes==2);%there are some TYPE-2 : 1K-0G nomatch - a cluster can not represent any groundTruth group
    if length(unassignedGroupIDs)>1
        elmntCnt = max(confMatGK(unassignedGroupIDs,:),[],2);
        [elmCntSrted, idx] = sort(elmntCnt,'descend');
        unassignedGroupIDs = unassignedGroupIDs(idx);
    end
    for g = unassignedGroupIDs
        %what is the max elemnt count that g falls into
        [elmntCnt, kId] = max(confMatGK(g,:));
        k = fing_kId_in_newClusterCell(newKlusterCell, kId);
        if ~isempty(k)
            %add g into newGroupCell(k)
            switch kTypes(k) 
                case 3
                    %TYPE-3 : 1K-1G can be a match - a cluster fully represents a groundTruth group
                    %kTypes(k) should change from T3-1K-1G to T4-1K-NG
                    %the g that is related to this k should change from T3-1K-1G to T4-1K-NG
                    gOf_k = find(~isnan(gkBestMapGroupingMat(:,k)));
                    kTypes(k) = 4;
                    gTypes(g) = 4;
                    gTypes(gOf_k) = 4;
                    newGroupCell{k} = sort([newGroupCell{k};g]);
                    unassignedGroupIDs(unassignedGroupIDs==g) = [];
                    gkBestMapGroupingMat(k,g) = 1;
                    %disp(['TYPE-3 : 1K-1G k(' mat2str(k) '),g(' mat2str(g) '),gOf_k(' mat2str(gOf_k) ')']);
                case 4
                    %TYPE-4 : 1K-NG
                    %just add a new into K
                    kTypes(k) = 4;
                    gTypes(g) = 4;
                    newGroupCell{k} = sort([newGroupCell{k};g]);
                    unassignedGroupIDs(unassignedGroupIDs==g) = [];
                    gkBestMapGroupingMat(k,newGroupCell{k}) = 1;
                    %disp(['TYPE-4 : 1K-NG k(' mat2str(k) '),g(' mat2str(newGroupCell{k}) ')']);
                case 5
                    %TYPE-5 : NK-1G
                    %this means this k was a group of clusters that
                    %represent a G, we should check which G it was
                    %representing
                    gOf_k = find(~isnan(gkBestMapGroupingMat(:,k)));
                    %if the G is the same g as this TYPE-5 goes on
                    if (gOf_k==g)
                        error('why couldnt we find this relation before - there should be a mistake');
                    end
                    %else if G is a new G than this will switch to NK-NG
                    %and also that G which used to be a Type-5 would become
                    %a TYPE-6 NK-NG
                    klustersOfFoundG = find(~isnan(gkBestMapGroupingMat(gOf_k,:)));
                    %all types must be equal to 5
                    assert(sum(kTypes(klustersOfFoundG)==5)==length(klustersOfFoundG),'this must hold')
                    %then all types can be switched to 6
                    kTypes(klustersOfFoundG) = 6;
                    gTypes(g) = 6;
                    unassignedGroupIDs(unassignedGroupIDs==g) = [];
                    newGroupCell{k} = sort([newGroupCell{k};g]);
                    %disp(['TYPE-6 : NK-NG k(' mat2str(klustersOfFoundG) '),g(' mat2str(newGroupCell{k}) ')']);
                case 6
                    %just add this g into k
                    gTypes(g) = 6;
                    newGroupCell{k} = sort([newGroupCell{k};g]);
                    unassignedGroupIDs(unassignedGroupIDs==g) = [];
                    gkBestMapGroupingMat(k,newGroupCell{k}) = 1;
                case 2
                    %kTypes(k) = 2;
                    %gOf_k = find(~isnan(gkBestMapGroupingMat(:,k)));
                    %do nothing
                otherwise
                    error('implement when a sample shows up');
            end
            gkBestMapGroupingMat(g,k) = 1;
            if isempty(unassignedClusterIDs)
                continue
            else
                %goOn
            end
        end
        if isempty(unassignedClusterIDs)
            %this means that:
            %there is no clusterID that remained unassigned
            newGroupCount = newGroupCount + 1;
            newGroupCell{newGroupCount} = g;
            newKlusterCount = newKlusterCount + 1;
            newKlusterCell{newKlusterCount} = newKlusterCount;
            newGroupClusterMatch(newGroupCount, newKlusterCount) = 1;
            kTypes(newKlusterCount) = 1;
        else
            %there are some unassigned clusters and we need to assign one
            %of them to this group and vice-versa
            sampleCountsFound = confMatGK(g,unassignedClusterIDs);
            %disp(['The cluster(s)[' mat2str(unassignedClusterIDs) '] are not assigned to any groupID. And sample counts are ' mat2str(sampleCountsFound)]);
            [elmntCnt, idx]= max(sampleCountsFound,[],2);
            k = unassignedClusterIDs(idx);
            %disp(['There are ' num2str(elmntCnt) ' samples that are originated from groupID(' num2str(g) '). Hence we form a 1K-1G relation between that and cluster(' num2str(k) ')']);
            unassignedClusterIDs(idx) = [];
            newGroupCount = newGroupCount + 1;
            %disp(['A new group of groupIDs is generated - newGroupCount(' num2str(newGroupCount) ') and g(' num2str(g) ') is inserted in']);
            newGroupCell{newGroupCount} = g;
            newKlusterCount = newKlusterCount + 1;
            %disp(['A new group of clusters is generated - newKlusterCount(' num2str(newKlusterCount) ') and k(' num2str(k) ') is inserted in']);
            newKlusterCell{newKlusterCount} = k;
            newGroupClusterMatch(newGroupCount, newKlusterCount) = 1;
            kTypes(k) = 3;%TYPE-3 : 1K-1G 
            unassignedGroupIDs(unassignedGroupIDs==g) = [];
            gkBestMapGroupingMat(g,k) = 1;
            clear sampleCountsFound elmntCnt
        end
        gTypes(g) = 3;%TYPE-3 : 1K-1G
    end
    assert(isempty(unassignedGroupIDs),'this has to be empty by now');
    if length(unassignedClusterIDs)>1
        elmntCnt = max(confMatGK(:,unassignedClusterIDs));
        if elmntCnt>1
            [elmCntSrted, idx] = sort(elmntCnt,'descend');
            unassignedClusterIDs = unassignedClusterIDs(idx);
        end
    end
    for k = unassignedClusterIDs
        %what is the max elemnt count that k falls into
        [elmntCnt, g] = max(confMatGK(:,k)); %#ok<ASGLU>
        %
        assignedClustersVec = find(~isnan(gkBestMapGroupingMat(g,:)));
        %assignedClustersCount = length(assignedClustersVec);
        newGroupIDOfk = fing_kId_in_newClusterCell(newKlusterCell, assignedClustersVec(1));
        
        groupIDOfG = fing_kId_in_newClusterCell(newGroupCell', g);
        matchingGVec = newGroupCell{groupIDOfG};
        
        assignedClustersVec = [assignedClustersVec(:);k];        
        switch gTypes(g) 
            case 3
                %TYPE-3 : 1K-1G can be a match - a cluster fully represents a groundTruth group
                %will become
                %TYPE-5 : NK-1G
                gTypes(matchingGVec) = 5;
                kTypes(assignedClustersVec) = 5;
                %disp(['TYPE-5 : NK-1G k(' mat2str(assignedClustersVec) '),g(' mat2str(matchingGVec) ')']);
            case 4
                %TYPE-4 : 1K-NG
                %will become
                %TYPE-6 : NK-NG
                gTypes(matchingGVec) = 6;
                kTypes(assignedClustersVec) = 6;
                %disp(['TYPE-4 : 1K-NG k(' mat2str(assignedClustersVec) '),g(' mat2str(matchingGVec) ')']);
            case 5
                %TYPE-5 : NK-1G
                %just add as type-5
                kTypes(assignedClustersVec) = 5;
                %disp(['TYPE-5 : NK-1G k(' mat2str(assignedClustersVec) '),g(' mat2str(matchingGVec) ')']);
            case 6
                %just add this k into group of gs
                kTypes(k) = 3;%TYPE-3 : 1K-1G
                kTypes(assignedClustersVec) = 6;
                %disp(['TYPE-6 : NK-NG k(' mat2str(assignedClustersVec) '),g(' mat2str(matchingGVec) ')']);
            otherwise
                error('implement when a sample shows up');
        end
        newKlusterCell{newGroupIDOfk} = sort([newKlusterCell{newGroupIDOfk};k]);
        gkBestMapGroupingMat(g,assignedClustersVec) = 1;
    end
    
    newGroupCell(newGroupCount+1:end) = [];
    newKlusterCell(newKlusterCount+1:end) = [];
    
    extraOutputStruct = struct;
    extraOutputStruct.gkBestMapGroupingMat = gkBestMapGroupingMat;
    extraOutputStruct.gTypes = gTypes;
    extraOutputStruct.kTypes = kTypes;
end

function cellID = fing_kId_in_newClusterCell(newKlusterCell, kID)
    for cellID = 1:size(newKlusterCell,2)
        if ismember(kID, newKlusterCell{cellID})
            return
        end
    end
    cellID = [];
end
