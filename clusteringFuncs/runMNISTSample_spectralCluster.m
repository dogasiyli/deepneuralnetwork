function runMNISTSample_spectralCluster(clusterK)
    tic
    %READ the data 
    [labels , digits]=read_data('train.csv');

    %Select a subset from digits
    digitsSubset = select_subset(labels , 50);

    %first create the distance matrix
    pts = digits(digitsSubset,:);
    distMatrix =pdist2(pts,pts);

    clusterIdx = spectralCluster_gz(struct('distMatrix',distMatrix),clusterK);

    %RAND INDEX
    [AR,RI,MI,HI]=RandIndex(clusterIdx,labels(digitsSubset,:)+1);
    %[AR,RI,MI,HI]=RandIndex(clusterIdx,labels+1);

    figure(figID);
    clf;hold on;
    subplotRowCnt = ceil(sqrt(clusterK));
    subplotColCnt = ceil(clusterK/subplotRowCnt);    
    for k=1:clusterK    
        ind =find(clusterIdx==k);
        clusterMean = digits(digitsSubset,:);
        clusterMean=mean(clusterMean(ind,:) ,1);
        I = reshape(clusterMean ,28,28);
        subplot(subplotRowCnt,subplotColCnt,k), imagesc(I');
    end
    toc
    % eigenVectors = U' * digits(digitsSubset,:);
    % 
    % for i=1:11
    %    I = reshape(eigenVectors(i,:) ,28,28);
    %     fisubplot(3,4,i),  imagesc(I'); hold on;
    % end
end

function

end