function [ digitsSubset ] = select_subset( labels , sizeSubset )
    
    digitsSubset=zeros(sizeSubset*10, 1);
    for i=0:9
        vectorOfDigits = find(labels==i);
        indices = randperm(length(vectorOfDigits), sizeSubset);        
        digitsSubset(i*sizeSubset+1:i*sizeSubset+sizeSubset)=vectorOfDigits(indices);        
    end
    digitsSubset=sort(digitsSubset);
end

