function [ M ] = simGaussian( M , sigma )
    %Gaussian kernel
    M = exp( -M.^2 ./ (2*sigma^2) );
end

