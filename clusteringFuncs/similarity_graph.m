function [ gaussianDistMatrix ,adjacencyMatrix, distMatrix  ] = similarity_graph(pts_distMatrix_struct , epsilon , sigma) 
    %you can either pass the distance matrix or calculate it from pts
    if isfield(pts_distMatrix_struct,'pts')
        pts = pts_distMatrix_struct.pts;
        distMatrix =pdist2(pts,pts);
    elseif isfield(pts_distMatrix_struct,'distMatrix')
        distMatrix = pts_distMatrix_struct.distMatrix;
    end
    gaussianDistMatrix = simGaussian(distMatrix , sigma);    
    %clear distMatrix;
    
    indices =gaussianDistMatrix<epsilon;
    gaussianDistMatrix(indices)=0;
    clear indices;
    
    indices2 =gaussianDistMatrix>=epsilon;    
    adjacencyMatrix = zeros(size(gaussianDistMatrix));
    adjacencyMatrix(indices2)=1;
    clear indices2;
end

