%function [ clusterIdx ] = spectralCluster_gz(digits(digitsSubset,:),clusterK)
function [ clusterIdx ] = spectralCluster_gz(pts_distMatrix_struct, clusterK)
%spectralCluster_gz you can either pass pts as points or the distance
%matrix. the function will understand which is passed from the field name

    if isfield(pts_distMatrix_struct,'pts')
        pts = pts_distMatrix_struct.pts;
        distMatrix =pdist2(pts,pts);
        pts_distMatrix_struct = struct('distMatrix',distMatrix);
    elseif ~isfield(pts_distMatrix_struct,'distMatrix')
        error('You must either pass <pts> or <distMatrix> to the function in <pts_distMatrix_struct>')
    end

    %Construct epsilon neigbourhood similarity matrix
    [degs , W, distMatrix] = similarity_graph(pts_distMatrix_struct , 0.47 , 2000);
    %[degs , W] = similarity_graph(digits , 0.47 , 2000 );

    D = diag(sum(degs,2));

    %Normalized Laplacian 
    L = D - W ;
    normL = D^(-0.5) * L * D^(-0.5);

    %Find k smallest eigenvectors of the normalized Laplacian to reduce
    %dimentionality
    [U ,~]=eigs(normL , clusterK+1 , eps);

    %Apply kmeans on the rows of the eigenVectors
    clusterIdx = kmeans(U(:,2:clusterK+1) , clusterK);


end

