function [ confAccBestVec, clusterLabelsGroupingVec, additionalInfoCell] = testClustering()
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
    fName = '/home/dg/Dropbox/PhD Files/PhD_Proceedings/19_HospiSignStudy/groupClusterLabelsIntoGroundTruthTestData.mat';
    load(fName,'clusterLabelsCell','gtLabels','gtLabelsGrouping');
    kMax = size(clusterLabelsCell,2); %#ok<USENS>
    confAccBestVec = NaN(1,kMax);
    clusterLabelsGroupingVec = cell(1,kMax);
    additionalInfoCell = cell(1,kMax);
    for k=6:kMax %rows
        [confMatBest, uniqueMappingLabels, confAccBestVec(k), outStruct] ...
            = groupClusterLabelsIntoGroundTruth(clusterLabelsCell{1,k}, gtLabels, struct('gtLabelsGrouping', gtLabelsGrouping,'printResults', true,'figID', 1));
        clusterLabelsGroupingVec{k} = outStruct.clusterLabelsGroupingVec;
        newKlusterCell = outStruct.newKlusterCell;
        clusterLabels_best = outStruct.clusLabels_Mapped;
        
        additionalInfoCell{k} = struct;
        additionalInfoCell{k}.confMatBest = confMatBest;
        additionalInfoCell{k}.uniqueMappingLabels = uniqueMappingLabels;
        additionalInfoCell{k}.newKlusterCell = newKlusterCell;
        additionalInfoCell{k}.clusterLabels_best = clusterLabels_best;
    end

end

