distMatInfoStruct = struct('featureName','sn','cropMethod','mc','distanceMethod','c2');
optParamStruct = struct;
optParamStruct.clusteringParams = struct('tryMax',2000,'kVec',[8 10 12],'gtLabelsGrouping',[],'modeStr','kMedoids','groupClusterLabels',false);
pv = [];
for s = 0%[335 404 450 533 537]%[7 13 49 69 74 85 86 87 88 89 96 112 125 221 222 247 269 273 277 278 290 296 310 335 339 353 396 403 404 424 428 450 521 533 535 536 537 583 586 593 636]
    try
        surSlctListStruct = struct('labelingStrategyStr','fromSkeletonStableAll', 'single0_double1',0, 'signIDList',s, 'userList',2:5, 'maxRptID',1);
        diary('off');
        analyzeDistMat(surSlctListStruct, distMatInfoStruct, optParamStruct)
    catch
        diary('off');
        pv = [pv;s];
    end
end
pv