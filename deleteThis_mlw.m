%distMatInfoStruct = struct('featureName','sn','cropMethod','mc','distanceMethod','c2');
distMatInfoStruct = struct('featureName','hf','cropMethod','mc','distanceMethod','e3');
optParamStruct = struct;
optParamStruct.clusteringParams = struct('tryMax',2000,'kVec',[10 12 15],'gtLabelsGrouping',[],'modeStr','kMedoids','groupClusterLabels',false);
pv = [];
signIDList = [87 112 125 221 269 273 277];%87 428;%[85 86 87 88 586 593];%[7 13 49 69 74 85 86 87 88 89 96 112 125 221 222 247 269 273 277 278 290 296 310 335 339 353 396 403 404 424 428 450 521 533 535 536 537 583 586 593 636]
userList = 2:5;%%2;
for s = signIDList
    try
        %if s<400
        %    moveFiles(s, userList, struct('HOGFeat','restore','SurfNormFeat','restore','deleteSource',false,'overWrite',false));
        %end
        surSlctListStruct = struct('labelingStrategyStr','fromSkeletonStableAll', 'single0_double1',1, 'signIDList',s, 'userList',userList, 'maxRptID',1);
        diary('off');
        analyzeDistMat(surSlctListStruct, distMatInfoStruct, optParamStruct);
        %if s<400
        %    moveFiles(s, userList, struct('HOGFeat','backup','SurfNormFeat','backup','deleteSource',true,'overWrite',false));
        %end
    catch
        diary('off');
        pv = [pv;s];
    end
end
pv