%distMatInfoStruct = struct('featureName','sn','cropMethod','mc','distanceMethod','c2');

%kVec = [8 10 12];%[278 404 424 450 533 537]%310 353 396 521
kVec = [15 20 25 30];%[290 403 404 535]%339
distMatInfoStruct = struct('featureName','hf','cropMethod','mc','distanceMethod','e3');
optParamStruct = struct;
optParamStruct.clusteringParams = struct('tryMax',2000,'kVec',kVec,'gtLabelsGrouping',[],'modeStr','kMedoids','groupClusterLabels',false);
pv = [];

clusterResults_folderSrc = [srcFold filesep 'clusterResults_hf_mc_e3'];


for s = [290 403 404 535]%[278 424 450 533  537]%[290 335][][353 537 450 521][7 13 49 69 74 85 86 87 88 89 96 112 125 221 222 247 269 273 277 278 290 296 310 335 339 353 396 403 404 424 428 450 521 533 535 536 537 583 586 593 636]
    for sd= [0 1]
        singleDoubleStr = 'single';
        if sd==1
            singleDoubleStr = 'double';
        end
        clusterResults_folderDst = [srcFold filesep 'clusterResults_hf_mc_e3_' num2str(s,'%03d') '_' singleDoubleStr '_fsa'];
        skipThis = exist(clusterResults_folderDst,'dir');
        %skipThis = false;
        if skipThis
            disp(['skipping ' mat2str([s sd]) ]);
            continue
        end
        try
            surSlctListStruct = struct('labelingStrategyStr','fromSkeletonStableAll', 'single0_double1',sd, 'signIDList',s, 'userList',2:5, 'maxRptID',1);
            diary('off');
            analyzeDistMat(surSlctListStruct, distMatInfoStruct, optParamStruct);
            %hospSign_runForASign(s, struct('featName', 'hf','kVec', kVec, 'single0_double1', sd));
            %setLabelsFromClusterAnalysisResults(srcFold, surSlctListStruct, distMatInfoStruct, clusteringParams);
        catch
            diary('off');
            pv = [pv;s sd];
        end
        movefile(clusterResults_folderSrc,clusterResults_folderDst);
   end
end
pv