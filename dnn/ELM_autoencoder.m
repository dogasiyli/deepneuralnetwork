function [Welm, biasELM, W_randoms] = ELM_autoencoder(X, varargin)

    %This code is taken from web and altered according to my own needs and perception
    %dogasiyli@gmail.com
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %This code implements methods described and referenced in the paper at http://arxiv.org/abs/1412.8307

    %Author: Assoc Prof Mark D. McDonnell, University of South Australia
    %Email: mark.mcdonnell@unisa.edu.au
    %Date: January 2015
    %Citation: If you find this code useful, please cite the paper described at http://arxiv.org/abs/1412.8307

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %train the ELM
    %Mandotary inputs :
    %1. X = train data dxN (784x60000)
    %Inputs that are derived from other inputs
    %1. Y = 60000x784 - transpose of X or some given output

% STEP-1 : Set and extract parameters defaults
    [Y, sizeHid, sizeImg, countPix, W_randoms, useBias, paramsELM, displayOpts] = getOptionalParams(X, varargin{:});
    
% STEP-2 : Create receptiveFieldMask
    receptiveFieldMask = createMask(sizeHid, sizeImg, countPix, paramsELM, displayOpts);

% STEP-3 : Initialize biasELM and W_randoms
    [W_randoms, bias_randoms] = initiailizeParams(X, W_randoms, sizeHid, countPix, useBias, paramsELM, receptiveFieldMask, displayOpts);

% STEP-4 : Train the ELM
    [Welm, biasELM] = applyTraining(X, Y, W_randoms, useBias, paramsELM, displayOpts);
end

% STEP-1 : Set optional param defaults
function [ Y, sizeHid, sizeImg, countPix, W_randoms, useBias, paramsELM, displayOpts] = getOptionalParams(data, varargin)
    %Variables that are derived from other inputs
    % 1. Y - transpose of X or some given output
    % 2. sizeImg - 
    % 3. countPix - 
    %Optional inputs with default values
    % 1. sizeHid - number of hidden layer neurons -  1600 for this case
    % 2. W_randoms - 
    % 3. useBias - use bias parameters for learning or not
    % 4. paramsELM - 
    % 5. displayInfo - if want to get extra information displayed on screen
    %P1D0 -> Passed assed paramaeter = 1, Default value used = 0
    [   sizeHid,    paramsELM,   useBias,  displayOpts,  W_randoms, P1D0] = parseArgs(...
    {  'sizeHid'   'paramsELM'  'useBias' 'displayOpts' 'W_randoms'},...
    { size(data,1)      -1        false        -1            []    },...
    varargin{:});

    if (~isstruct(displayOpts) && displayOpts==-1)
        displayOpts = setDisplayOpts();
    end
    if (~isstruct(paramsELM) && paramsELM==-1)
        paramsELM = setParams_ELM('Standard');
    end
    if (displayOpts.defaultParamSet==1)
        %display the default parameters selected
        if (P1D0.sizeHid==0)
            disp([displayOpts.ies{8} 'sizeHid of the hidden layer is set to default as the equal dimension of data(' num2str(sizeHid) ').']);
        end
        if (P1D0.paramsELM==0)
            disp([displayOpts.ies{8} 'ELM Parameters are not passed as function parameter. Hence default values will be used.']);
            disp([displayOpts.ies{8} 'randomWeightInitialization(' paramsELM.randomWeightInitialization ')/useReceptiveFields(false)']);
        end
        if (P1D0.displayOpts==0)
            disp([displayOpts.ies{8} 'displayOpts are not passed as function parameter. Hence default values will be used.']);
        end
        if (P1D0.useBias==0)
            disp([displayOpts.ies{8} 'useBias is not passed as parameter and set to default(false)']);
        end
        if (P1D0.W_randoms==0)
            disp([displayOpts.ies{8} 'W_randoms is not passed as parameter hence initialized as empty by default.']);
        end
    end
    
    % Derive variables from other inputs
    countPix = size(data,1);
    sizeImg = sqrt(countPix);% size of the image
    
    %sanitary checks
    if paramsELM.useReceptiveFields && mod(sizeImg,1)>0
        if (displayOpts.warningInfo==1)
            disp([displayOpts.ies{8} 'Can not use receptive fields because input is not square']);
        end
        paramsELM.useReceptiveFields = false;
    end
    if (~strcmp(paramsELM.randomWeightInitialization,'Standard') && ~strcmp(paramsELM.randomWeightInitialization,'ConstrainedWeights'))
        if (displayOpts.warningInfo==1)
            disp([displayOpts.ies{8} 'randomWeightInitialization(' paramsELM.randomWeightInitialization ') is unknown or not proper. StandardELM will be used']);
        end
        paramsELM.randomWeightInitialization = 'Standard';
    end
    
    Y = data';
    if (displayOpts.defaultParamSet==1)
        disp([displayOpts.ies{8} 'Output data is set as the transpose of input data.']);
    end
end

% STEP-2 : Create receptiveFieldMask
function receptiveFieldMask = createMask(sizeHid, sizeImg, countPix, paramsELM, displayOpts)
    %receptive fields are gonna be used as masks of each hidden unit.
    %1 hidden unit will only deal with the so called receptive field area in
    %stead of the whole image
    %I am sure there is a more efiicient way to get these random fields
    
    if paramsELM.useReceptiveFields
        if (displayOpts.calculation==1)
            if (displayOpts.dataSize==1)
                disp([displayOpts.ies{8} 'receptiveFieldMask of size (' num2str(sizeHid) ' by ' num2str(countPix) ') will be constructed']);
            else
                disp([displayOpts.ies{8} 'receptiveFieldMask will be constructed']);
            end
        end
        
        time_spent = cputime;
            %get receptive field masks
            receptiveFieldMask =  zeros(sizeHid, countPix);
            for ii = 1:sizeHid
                SquareMask = zeros(sizeImg,sizeImg);
                rowInds = zeros(2,1);
                colInds = zeros(2,1);
                while (rowInds(2)-rowInds(1))*(colInds(2)-colInds(1)) < paramsELM.rf.minMaskSize
                    rowInds = paramsELM.rf.border+sort(randperm(sizeImg-2*paramsELM.rf.border,2));
                    colInds = paramsELM.rf.border+sort(randperm(sizeImg-2*paramsELM.rf.border,2));
                end
                SquareMask(rowInds(1):rowInds(2),colInds(1):colInds(2))=1;
                receptiveFieldMask(ii,:) =  SquareMask(:);
            end
        time_spent = cputime - time_spent;
        
        if (displayOpts.calculation==1 && displayOpts.timeMode>=3)
            disp([displayOpts.ies{8} 'receptiveFieldMask is constructed in (' num2str(time_spent,'%4.2f') ') cputime.']);
        elseif (displayOpts.calculation==1 && displayOpts.timeMode<3)
            disp([displayOpts.ies{8} 'receptiveFieldMask is constructed']);
        end        
    else %dont UseReceptiveFields
        receptiveFieldMask = ones(sizeHid,countPix);
        if (displayOpts.calculation==1)
            if (displayOpts.dataSize==1)
                disp([displayOpts.ies{8} 'receptiveFieldMask of size (' num2str(sizeHid) ' by ' num2str(countPix) ') is initialized as 1-matrix']);
            else
                disp([displayOpts.ies{8} 'receptiveFieldMask is initialized as 1-matrix']);
            end
        end
    end
end

% STEP-3 : Initialize biasELM and W_randoms
% TODO : displayOpts must be used properly
function [W_randoms, biasELM] = initiailizeParams(X, W_randoms, sizeHid, countPix, useBias, paramsELM, receptiveFieldMask, displayOpts)
    biasELM = zeros(sizeHid,1);
    if (isempty(W_randoms))
        W_randoms = zeros(sizeHid,countPix);
        switch paramsELM.randomWeightInitialization
            case 'Standard'
                W_randoms = sign(randn(sizeHid,countPix)); %get bipolar random weights
                W_randoms =  receptiveFieldMask.*W_randoms; %mask random weights
                W_randoms = paramsELM.scaling*diag(1./sqrt(eps+sum(W_randoms.^2,2)))*W_randoms; %normalise rows and scale
            case 'ConstrainedWeights'%C_ELM - Get the Constrained weights
                countSamples = size(X,2);
                for i = 1:sizeHid
                    Norm = 0;
                    sampleImageIndices = ones(2,1);
                    %select 2 different images with norm of weights bigger than eps
                    %for this particular hidden weights
                    %then assign particular weights according to
                    %1. mask of the hidden unit
                    %2. difference of the randomly selected 2 images
                    while sampleImageIndices(1) == sampleImageIndices(2) ||  Norm < eps
                        sampleImageIndices = randperm(countSamples,2);
                        X_Diff = X(:,sampleImageIndices(1))-X(:,sampleImageIndices(2));
                        Wrow = X_Diff.*receptiveFieldMask(i,:)'; %get masked C random weights
                        Norm  = sqrt(sum(Wrow.^2));
                    end
                    W_randoms(i,:) = Wrow/Norm;
                    biasELM(i) = 0.5*(X(:,sampleImageIndices(1))+X(:,sampleImageIndices(2)))'*Wrow/Norm;
                end
                W_randoms = paramsELM.scaling*W_randoms; %scale the weights (already normalised) 
            otherwise
                disp('error')
                return
        end
    end
    
    %to implement biases, set an extra input dimension to 1, and put the biases in the input weights matrix
    %X = [X;ones(1,k_train)];
    %W_randoms is sizeHid rows-countPix cols
    %biases    is sizeHid rows-1 cols
    if (useBias)
        W_randoms = [W_randoms biasELM];
    end
    %W_randoms became sizeHid rows-countPix cols
end    
    
% STEP-4 : Apply training   
% TODO : displayOpts must be used properly
function [Welm, biasELM, cost] = applyTraining(X, Y, W_randoms, useBias, paramsELM, displayOpts)
    sigm = @(z_) 1.0 ./ (1.0 + exp(-z_));
    countSamples = size(X,2);
    sizeHid = size(W_randoms,1);
    
    if useBias
        A = sigm(W_randoms*([X;ones(1,countSamples)]));%get hidden layer activations
        %Y = [Y,ones(countSamples,1)];%->this is not working correctly
        %A = [A;ones(1,countSamples)];%->this is not working correctly
        %X = B/A solves the symbolic system of linear equations in matrix form, X*A = B for X. 
    else
        A = sigm(W_randoms*X);%get hidden layer activations
    end
    %X = B/A solves the symbolic system of linear equations in matrix form, X*A = B for X. 
    Welm = (A*Y)'/(A*A'+paramsELM.ridgeParam*eye(sizeHid)); %find output weights by solving the for regularised least mean square weights
    
    %Welm is (countPix+1) rows by sizeHid cols
    %biasELM is sizeHid,1
    Welm = Welm';%
    biasELM = zeros(sizeHid,1);
    
    m = size(X,2);
    X_hat = (A'*Welm)';
    %X_hat = sigm(X_hat);
    deriv3 = (X_hat-X);
    cost = sum(sum(deriv3.^2))/(2*m);
%     if useBias
% %         biasELM = Welm(:,end);
%         Welm = Welm(1:end-1,:);
%     else
%         
%     end
end