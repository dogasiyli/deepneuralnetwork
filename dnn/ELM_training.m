function [accuracyCalced, predictedLabels, W_randoms_with_bias, Welm, A, confMatResult, labelActivations] = ELM_training(X, labels, varargin)

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %This code implements methods described and referenced in the paper at http://arxiv.org/abs/1412.8307

    %Author: Assoc Prof Mark D. McDonnell, University of South Australia
    %Email: mark.mcdonnell@unisa.edu.au
    %Date: January 2015
    %Citation: If you find this code useful, please cite the paper described at http://arxiv.org/abs/1412.8307

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %train the ELM
    %Mandotary inputs :
    %1. X = train data dxN (784x60000)
    %2. labels - labels themselves as 0 to 9
    
% STEP-1 : Set optional param defaults
    [ Y, W_randoms_with_bias, sizeHid, countPix, sampleCountTotal, countCategory,  paramsELM,  displayOpts] = getOptionalParams(X, labels, varargin{:});
    
% STEP-2 : Create receptiveFieldMask
    receptiveFieldMask = createMask(paramsELM, sizeHid, countPix);

% STEP-3 : Initialize biasELM and W_randoms
    if isempty(W_randoms_with_bias)
        W_randoms_with_bias = initiailizeParams(X, Y, labels, sizeHid, countPix, sampleCountTotal, countCategory, paramsELM, receptiveFieldMask, displayOpts);
    end

% STEP-4 : Apply training and get additional results to return
    [Welm, predictedLabels, accuracyCalced, confMatResult, A, labelActivations] = applyTraining(X, Y, sampleCountTotal, sizeHid, W_randoms_with_bias, paramsELM, displayOpts);

end

% STEP-1 : Set optional param defaults
function [ Y, W_randoms_with_bias, sizeHid, countPix, sampleCountTotal, countCategory,  paramsELM,  displayOpts] = getOptionalParams(X, labels, varargin)
    %Optional inputs with default values
    %1. sizeHid - number of hidden layer neurons -  1600 for this case
    %2. paramsELM - ELM parameters
    %3. displayOpts - if want to get extra information displayed on screen
    %P1D0 -> Passed assed paramaeter = 1, Default value used = 0
    [   sizeHid,  W_randoms_with_bias,   paramsELM,  displayOpts, P1D0] = parseArgs(...
    {  'sizeHid' 'W_randoms_with_bias'   'paramsELM' 'displayOpts' },...
    {  size(X,1)        []                   -1           -1       },...
    varargin{:});

    if (~isstruct(displayOpts) && displayOpts==-1)
        displayOpts = setDisplayOpts();
    end
    if (~isstruct(paramsELM) && paramsELM==-1)
        paramsELM = setParams_ELM('Standard');
    end
    if (displayOpts.defaultParamSet==1)
        %display the default parameters selected
        if (P1D0.sizeHid==0)
            disp([displayOpts.ies{4} 'sizeHid of the hidden layer is set to default as the equal dimension of data(' num2str(sizeHid) ').']);
        end
        if (P1D0.displayOpts==0)
            disp([displayOpts.ies{4} 'displayOpts options are not passed as function parameter. Hence default values will be used.']);
        end
        if (P1D0.paramsELM==0)
            disp([displayOpts.ies{4} 'ELM Parameters are not passed as function parameter. Hence default values will be used.']);
            disp([displayOpts.ies{4} 'randomWeightInitialization(' paramsELM.randomWeightInitialization ')/useReceptiveFields(false)']);
        end
    end
    
%Extract information from the parameters
    %Old inputs that now are derived from other inputs
    %1. Y = 60000x10 - label matrix for softMax output
    Y = full(sparse((labels)',1:length(labels),ones(1,length(labels))))';
    %2.
    [countPix,sampleCountTotal] = size(X);
    %3. sizeImg = sqrt(countPix);% size of the image
    % sizeImg = sqrt(countPix);
    %4. countCategory - 10 %how many different labels there are
    countCategory = length(unique(labels));%10
end

% STEP-2 : Create receptiveFieldMask
function  receptiveFieldMask = createMask(paramsELM, sizeHid, countPix)
    %receptive fields are gonna be used as masks of each hidden unit. 
    %1 hidden unit will only deal with the so called receptive field area in
    %stead of the whole image
    %I am sure there is a more efiicient way to get these random fields
    if paramsELM.useReceptiveFields %UseReceptiveFields
        if isfield(paramsELM,'inputChannelSize') && paramsELM.inputChannelSize>1
            countPix = countPix/paramsELM.inputChannelSize;
            sizeImg = sqrt(countPix);
            disp(['receptiveFieldMask is being constructed for [' num2str(sizeImg) ',' num2str(sizeImg) '] input image with ' num2str(paramsELM.inputChannelSize) ' channels.']);
        else
            sizeImg = sqrt(countPix);
        end
        %get receptive field masks
        receptiveFieldMask =  zeros(sizeHid,countPix);
        for ii = 1:sizeHid
            SquareMask = getRandomMask(sizeImg, paramsELM.rf.minMaskSize, paramsELM.rf.border);
            receptiveFieldMask(ii,:) =  SquareMask(:);
        end
        if isfield(paramsELM,'inputChannelSize') && paramsELM.inputChannelSize>1
            receptiveFieldMask = repmat(receptiveFieldMask,1,paramsELM.inputChannelSize);
        end
    else %dont UseReceptiveFields
        receptiveFieldMask = ones(sizeHid,countPix);
    end
end

% STEP-3 : Initialize biasELM and W_randoms
function [W_randoms, biasELM] = initiailizeParams(X, Y, labels, sizeHid, countPix, sampleCountTotal, countCategory, paramsELM, receptiveFieldMask, displayOpts)
    W_randoms = zeros(sizeHid,countPix);
    biasELM = zeros(sizeHid,1);
    switch paramsELM.randomWeightInitialization
        case 'Standard' %StandardELM
            W_randoms = sign(randn(sizeHid,countPix)); %get bipolar random weights
            W_randoms =  receptiveFieldMask.*W_randoms; %mask random weights
            W_randoms = paramsELM.scaling*diag(1./sqrt(eps+sum(W_randoms.^2,2)))*W_randoms; %normalise rows and scale
        case 'ComputedInputWeights' %CIW_ELM = get CIW weights
            sampleCountEachClass = sum(Y);
            M_CIWs = round(sizeHid*sampleCountEachClass/sampleCountTotal);
            M0 = sum(M_CIWs);
            if M0 ~= sizeHid
               M_CIWs(1) = M_CIWs(1) + sizeHid - M0;
            end
            Count = 1;
            for i = 1:countCategory
                ClassIndices = find(labels==i-1);
                W_randoms(Count:Count+M_CIWs(i)-1,:) = sign(randn(M_CIWs(i),length(ClassIndices)))*X(:,ClassIndices)';
                Count = Count + M_CIWs(i);
            end
            W_randoms =  receptiveFieldMask.*W_randoms; %mask random weights
            W_randoms = paramsELM.scaling*diag(1./sqrt(eps+sum(W_randoms.^2,2)))*W_randoms; %normalise rows and scale
        case 'ConstrainedWeights' %C_ELM - Get the Constrained weights
            normTresh = eps*ones(1,sizeHid);
            j = zeros(1,sizeHid);
            maskSizes = zeros(1,sizeHid);
            for i = 1:sizeHid
                changeMask = true;            
                while (changeMask)
                    [Wrow, Norm, sampleImageIndices,normTresh(i),j(i),maskSizes(i),changeMask] = getRandomImages_2(X, labels, sizeHid, receptiveFieldMask, i, 1);
                    if changeMask
                        SquareMask = getRandomMask(sizeImg,paramsELM.minMaskSize,paramsELM.rf.border);
                        receptiveFieldMask(i,:) =  SquareMask(:);
                    end
                end
                W_randoms(i,:) = Wrow/Norm;
                biasELM(i) = 0.5*(X(:,sampleImageIndices(1))+X(:,sampleImageIndices(2)))'*Wrow/Norm;
            end
            displayAdditionalAnalysis(displayOpts, normTresh, j, maskSizes);
            W_randoms = paramsELM.scaling*W_randoms; %scale the weights (already normalised) 
        otherwise
            disp('error')
            return
    end
    %to implement biases, set an extra input dimension to 1, and put the biases in the input weights matrix
    %X = [X;ones(1,k_train)];
    W_randoms = [W_randoms biasELM];
end

% STEP-4 : Apply training   
function [Welm, predictedLabels, accuracyCalced, confMatResult, A, labelActivations] = applyTraining(X, Y, sampleCountTotal, sizeHid, W_randoms_with_bias, paramsELM, displayOpts)
    %train the ELM
    sigm = @(z_) 1.0 ./ (1.0 + exp(-z_));
    if sum(W_randoms_with_bias(:)>eps)/numel(W_randoms_with_bias) < 0.25
        %W_randoms(W_randoms(:)<=eps) = 0;
        W_randoms_with_bias = sparse(W_randoms_with_bias);
    end
    A = sigm(W_randoms_with_bias*([X;ones(1,sampleCountTotal)]));%get hidden layer activations
    %X = B/A solves the symbolic system of linear equations in matrix form, X*A = B for X. 
    Welm = (A*Y)'/((A*A')+(paramsELM.ridgeParam*eye(sizeHid))); %find output weights by solving the for regularised least mean square weights
    
    if (~strcmp(displayOpts.confusionMat,'None'))
        disp([displayOpts.ies{6} 'Train data analysis of ELM :']);    
    end
    
    [~,givenLabels] = max(Y,[],2);
    %[predictedLabels, accuracyCalced, confMatResult, sampleCountTotal, sampleCountChanged, hiddenLayerActivations, labelActivations]
    [predictedLabels, accuracyCalced, confMatResult, ~, ~, ~, labelActivations] = analyzeResults(X, givenLabels, W_randoms_with_bias, Welm, ~strcmp(displayOpts.confusionMat,'None') , A);
end

function [SquareMask] = getRandomMask(sizeImg,minMaskSize,RF_Border)
    SquareMask = zeros(sizeImg,sizeImg);
    rowInds = zeros(2,1);
    colInds = zeros(2,1);
    while (rowInds(2)-rowInds(1))*(colInds(2)-colInds(1)) < minMaskSize
        rowInds = RF_Border+sort(randperm(sizeImg-2*RF_Border,2));
        colInds = RF_Border+sort(randperm(sizeImg-2*RF_Border,2));
    end
    SquareMask(rowInds(1):rowInds(2),colInds(1):colInds(2))=1;
    %receptiveFieldMask(ii,:) =  SquareMask(:);
end

function displayAdditionalAnalysis(displayOpts, normTresh, j, maskSizes)
    sizeHid = length(j);
    moreStepsCount = sum(j>0);
    insufWeightCount = sum(normTresh<eps);
    assert(moreStepsCount==insufWeightCount,['insufWeightCount(' num2str(insufWeightCount) ')~=(' num2str(moreStepsCount) ')moreStepsCount']);
    if strcmp(displayOpts.displayLevel,'minimal') %minor stuff
        if moreStepsCount>0
            disp([num2str(moreStepsCount) ' steps completed with while loops min(' num2str(min(j(j>0))) '),mean(' num2str(mean(j(j>0))) '),max(' num2str(max(j)) ')']);
        else
            disp('No steps completed with multiple while loops :)');
        end
    elseif ~strcmp(displayOpts.displayLevel,'none') %major stuff
        if insufWeightCount>0
            disp(['norms of insufficient weights(' num2str(insufWeightCount) ' of ' num2str(sizeHid) ') are min(' num2str(min(normTresh(normTresh<eps))) ')<mean(' num2str(mean(normTresh(normTresh<eps))) ')<max(' num2str(max(normTresh(normTresh<eps))) ')<eps(' num2str(eps) ')']);
        else
            disp(['norms of insufficient weights are all <eps(' num2str(eps) ')']);
        end
    elseif strcmp(displayOpts.displayLevel,'all') %every stuff
        if insufWeightCount>0
            disp('Insufficient norm weights are :');
            iw = [(1:sum(normTresh<eps))' find(normTresh<eps)', normTresh(normTresh<eps)', j(normTresh<eps)', maskSizes(normTresh<eps)'];
            disptable(iw,'S.Nu|Weight ID|Norm|While Loop Count|receptiveFieldMask Size');
        end
    end
end

function [Wrow, Norm, sampleImageIndices, normTresh, j, maskSize, changeMask] = getRandomImages_2(X, labels, sizeHid, receptiveFieldMask, i, displayInfoLevel)
    %displayInfoLevel = 3;%0 nothing,1 major stuff,2 also minor stuff, 3 anything possible
    if (~exist('displayInfoLevel','var') || isempty(displayInfoLevel))
        displayInfoLevel = 0;
    end
    %Get such 2 samples that
    %1. sample labels are different
    %2. norm of weights bigger than eps
    countCategory = length(unique(labels));%10
    
    normTresh = eps;
    Norm = 0;
    keepSearching = true;
    sampleImageIndices = ones(2,1);
    %select 2 different images with norm of weights bigger than eps
    %for this particular hidden weights
    %then assign particular weights according to
    %1. mask of the hidden unit
    %2. difference of the randomly selected 2 images
    maxNormTillNow = [-inf -1 -1 -1 -1];
    j_sameLabel = 0;
    j_norm = 0;
    j=0;
    maskSize = sum(receptiveFieldMask(i,:)>0);
    changeMask = false;
    printOverNormTresh = false;
    while keepSearching
        %1. sample labels are different
        if (maxNormTillNow(1)>normTresh && maxNormTillNow(4) ~= maxNormTillNow(5))
            sampleImageIndices = maxNormTillNow(2:3);
            sampleLabels = maxNormTillNow(4:5);
            printOverNormTresh = true;
        else
            if (j<20)
                [sampleImageIndices,sampleLabels] = getSampleIDs(labels, countCategory);
            else
                [sampleImageIndices,sampleLabels, v, changeMask] = getSampleIDs_Masked_mindist(X, labels, find(receptiveFieldMask(i,:)), countCategory, i);
                if changeMask
                    %[Wrow, Norm, sampleImageIndices, normTresh, j, maskSize, changeMask];
                    Wrow = [];
                    Norm = [];
                    normTresh = -1;
                    j = -1;
                    maskSize = -1;
                    if displayInfoLevel>=2
                        disp('Change the mask and re-evaluate');
                    end
                    return;
                end
            end
        end
        if (sampleLabels(1) == sampleLabels(2))
            j_sameLabel = j_sameLabel+1;
            warning('No same labels should be selected.USELESS IF STATEMENT-DELETE THIS WHEN CHECKS ARE COMPLETE.');
        end        
        %2. norm of weights bigger than eps
        X_Diff = X(:,sampleImageIndices(1))-X(:,sampleImageIndices(2));
        Wrow = X_Diff.*receptiveFieldMask(i,:)'; %get masked C random weights
        Norm  = sqrt(sum(Wrow.^2));
        if (maxNormTillNow(1)<Norm)
            maxNormTillNow = [Norm,sampleImageIndices(:)',sampleLabels(:)'];
        end
        if Norm < normTresh
            j_norm = j_norm+1;
            normTresh = normTresh/1.1;
            if mod(j_norm,500)==0 && j_norm>0 && displayInfoLevel>=1
                disp(['MJ-' num2str(i) ' of ' num2str(sizeHid) '-j_norm(' num2str(j_norm) '-' num2str(j) '),Norm(' num2str(Norm) ')<normTresh(' num2str(normTresh) ')<eps(' num2str(eps) '), maxNormTillNow(' num2str(maxNormTillNow(1)) ')']);                    
            elseif mod(j_norm,100)==0 && j_norm>0 && displayInfoLevel>=2
                disp(['MN-' num2str(i) ' of ' num2str(sizeHid) '-j_norm(' num2str(j_norm) '-' num2str(j) '),Norm(' num2str(Norm) ')<normTresh(' num2str(normTresh) ')<eps(' num2str(eps) '), maxNormTillNow(' num2str(maxNormTillNow(1)) ')']);                    
            end
        end
        keepSearching = sampleLabels(1) == sampleLabels(2) ||  Norm < normTresh;
        j = j_sameLabel+j_norm;
    end
    if printOverNormTresh && displayInfoLevel>=2
        disp(['MN-' num2str(i) ' of ' num2str(sizeHid) '-j_norm(' num2str(j_norm) '-' num2str(j) '),Norm(' num2str(Norm) ')<normTresh(' num2str(normTresh) ')<eps(' num2str(eps) '), maxNormTillNow(' num2str(maxNormTillNow(1)) ')']);                    
    elseif (j>100) && displayInfoLevel>=1
        disp(['MJ-' num2str(i) ' of ' num2str(sizeHid) ' completed in j(' num2str(j) ')']);
    end
end

function [sampleImageIndices,sampleLabels] = getSampleIDs(labels, countCategory)
    sampleLabels = randperm(countCategory,2);
    sampleIndicesForLabel = find(labels==sampleLabels(1));
    sampleImageIndices(1) = sampleIndicesForLabel(randperm(length(sampleIndicesForLabel),1));
    sampleIndicesForLabel = find(labels==sampleLabels(2));
    sampleImageIndices(2) = sampleIndicesForLabel(randperm(length(sampleIndicesForLabel),1));   
end

function [sampleImageIndices, sampleLabels, v, changeMask] = getSampleIDs_Masked_mindist(X, labels, currentMaskPixels, countCategory, hiddenLayerID)
    sampleLabels = randperm(countCategory,2);

    %now that I have sample Labels,
    %in a masked area we can face 3 different situations
    %1-sample values are all zeros in pixels mathing the mask
    %2-sample values are all ones  in pixels mathing the mask
    %3-sample has some non-zero values in pixels mathing the mask
    %I can select only the samples such that
    %X_Diff is not zero so
    %-label1 
    %-label2 
    %or vice versa
    %currentMaskPixels = find(receptiveFieldMask(i,:));
    sampleIdsOfLabel_1 = find(labels==sampleLabels(1));
    sampleIdsOfLabel_2 = find(labels==sampleLabels(2));
    area_maskedSamplesOfLabel_1 = X(currentMaskPixels,sampleIdsOfLabel_1);
    area_maskedSamplesOfLabel_2 = X(currentMaskPixels,sampleIdsOfLabel_2);

    %first the simpler check to see if we need to calc distances with
    %pdist2 or not
    sumArea_maskedSamplesOfLabel_1 = sum(area_maskedSamplesOfLabel_1);
    sampleIdsOfLabel_1_zeros     = sampleIdsOfLabel_1(sumArea_maskedSamplesOfLabel_1==0);
    sampleIdsOfLabel_1_ones      = sampleIdsOfLabel_1(sumArea_maskedSamplesOfLabel_1==length(currentMaskPixels));
    sumArea_maskedSamplesOfLabel_2 = sum(area_maskedSamplesOfLabel_2);
    sampleIdsOfLabel_2_zeros     = sampleIdsOfLabel_2(sumArea_maskedSamplesOfLabel_2==0);
    sampleIdsOfLabel_2_ones      = sampleIdsOfLabel_2(sumArea_maskedSamplesOfLabel_2==length(currentMaskPixels));
    all_ones_1  = length(sampleIdsOfLabel_1_ones) ==length(sumArea_maskedSamplesOfLabel_1);
    all_zeros_1 = length(sampleIdsOfLabel_1_zeros)==length(sumArea_maskedSamplesOfLabel_1);
    all_ones_2  = length(sampleIdsOfLabel_2_ones) ==length(sumArea_maskedSamplesOfLabel_2);
    all_zeros_2 = length(sampleIdsOfLabel_2_zeros)==length(sumArea_maskedSamplesOfLabel_2);

    changeMask = (all_ones_1&&all_ones_2) || (all_zeros_1&&all_zeros_2);
    if changeMask
        disp(['labels(' mat2str(sampleLabels) ') are not discriminable for mask of hidden layer(' num2str(hiddenLayerID) ')']);
        sampleImageIndices = [];
        v = [];
        return;
    end
    
    %grab the most discriminitive 2 samples according to this specific mask
    x = pdist2(area_maskedSamplesOfLabel_1',area_maskedSamplesOfLabel_2','euclidean')';
    m = max(x(:));
    [s_2,s_1] = find(x==m,1,'first');
    v = x(s_2,s_1);
    changeMask = v<eps;
    if changeMask
        disp(['euclidian distance of samples is (' num2str(v) ')-labels(' mat2str(sampleLabels) ') are not discriminable for mask of hidden layer(' num2str(hiddenLayerID) ')']);
        sampleImageIndices = [];
        v = [];
        return;
    end
    sampleImageIndices = [sampleIdsOfLabel_1(s_1),sampleIdsOfLabel_1(s_2)];
end

function [sampleImageIndices,sampleLabels] = getSampleIDs_Masked(X, labels, currentMaskPixels, countCategory, hiddenLayerID)
    findDifferentSampleLabels = true;
    while findDifferentSampleLabels
        sampleLabels = randperm(countCategory,2);

        %now that I have sample Labels,
        %in a masked area we can face 3 different situations
        %1-sample values are all zeros in pixels mathing the mask
        %2-sample values are all ones  in pixels mathing the mask
        %3-sample has some non-zero values in pixels mathing the mask
        %I can select only the samples such that
        %X_Diff is not zero so
        %-label1 
        %-label2 
        %or vice versa
        %currentMaskPixels = find(receptiveFieldMask(i,:));

        sampleIdsOfLabel_1 = find(labels==sampleLabels(1));
        sumArea_maskedSamplesOfLabel_1 = sum(X(currentMaskPixels,sampleIdsOfLabel_1));
        sampleIdsOfLabel_1_zeros     = sampleIdsOfLabel_1(sumArea_maskedSamplesOfLabel_1==0);
        sampleIdsOfLabel_1_ones      = sampleIdsOfLabel_1(sumArea_maskedSamplesOfLabel_1==length(currentMaskPixels));
        sampleIdsOfLabel_1_scrambled = sampleIdsOfLabel_1((sumArea_maskedSamplesOfLabel_1>0 + sumArea_maskedSamplesOfLabel_1<length(currentMaskPixels))==2);
        all_ones_1  = length(sampleIdsOfLabel_1_ones) ==length(sumArea_maskedSamplesOfLabel_1);
        all_zeros_1 = length(sampleIdsOfLabel_1_zeros)==length(sumArea_maskedSamplesOfLabel_1);

        sampleIdsOfLabel_2 = find(labels==sampleLabels(2));
        sumArea_maskedSamplesOfLabel_2 = sum(X(currentMaskPixels,sampleIdsOfLabel_2));
        sampleIdsOfLabel_2_zeros     = sampleIdsOfLabel_2(sumArea_maskedSamplesOfLabel_2==0);
        sampleIdsOfLabel_2_ones      = sampleIdsOfLabel_2(sumArea_maskedSamplesOfLabel_2==length(currentMaskPixels));
        sampleIdsOfLabel_2_scrambled = sampleIdsOfLabel_2((sumArea_maskedSamplesOfLabel_2>0 + sumArea_maskedSamplesOfLabel_2<length(currentMaskPixels))==2);
        all_ones_2  = length(sampleIdsOfLabel_2_ones) ==length(sumArea_maskedSamplesOfLabel_2);
        all_zeros_2 = length(sampleIdsOfLabel_2_zeros)==length(sumArea_maskedSamplesOfLabel_2);
        
        findDifferentSampleLabels = (all_ones_1&&all_ones_2) || (all_zeros_1&&all_zeros_2);
        disp(['labels(' mat2str(sampleLabels) ') are not discriminable for mask of hidden layer(' num2str(hiddenLayerID) ')']);
    end
    
    %if we are here it means that we can match
    if  ~isempty(sampleIdsOfLabel_1_scrambled)
        %get a sample from a scrambled sample in 1
        sampleImageIndices(1) = sampleIdsOfLabel_1_scrambled(randperm(length(sampleIdsOfLabel_1_scrambled),1));
        %take the other from anywhere you like
        sampleImageIndices(2) = sampleIdsOfLabel_2(randperm(length(sampleIdsOfLabel_2),1));
    elseif ~isempty(sampleIdsOfLabel_2_scrambled)
        %get a sample from a scrambled sample in 2
        sampleImageIndices(2) = sampleIdsOfLabel_2_scrambled(randperm(length(sampleIdsOfLabel_2_scrambled),1));
        %take the other from anywhere you like   
        sampleImageIndices(1) = sampleIdsOfLabel_1(randperm(length(sampleIdsOfLabel_1),1));
    elseif (~all_ones_1)
        %1 sample from all ones of 2
        sampleImageIndices(2) = sampleIdsOfLabel_2_ones(randperm(length(sampleIdsOfLabel_2_ones),1));
        %1 sample from all zeros or scrambled of 1
        if rand>=0.5
            sampleImageIndices(1) = sampleIdsOfLabel_1_zeros(randperm(length(sampleIdsOfLabel_1_zeros),1));
        else
            sampleImageIndices(1) = sampleIdsOfLabel_1_scrambled(randperm(length(sampleIdsOfLabel_1_scrambled),1));
        end
    elseif (~all_ones_2)
        %1 sample from all ones of 1
        sampleImageIndices(1) = sampleIdsOfLabel_1_ones(randperm(length(sampleIdsOfLabel_1_ones),1));
        %1 sample from all zeros or scrambled of 2
        if rand>=0.5
            sampleImageIndices(2) = sampleIdsOfLabel_2_zeros(randperm(length(sampleIdsOfLabel_2_zeros),1));
        else
            sampleImageIndices(2) = sampleIdsOfLabel_2_scrambled(randperm(length(sampleIdsOfLabel_2_scrambled),1));
        end
    else
        error('Not possible to be here!!!');
    end
end

function [Wrow, Norm, sampleImageIndices, normTresh, j] = getRandomImages(X, labels, sizeHid, receptiveFieldMask, i)
    displayInfoLevel = 3;%0 nothing,1 major stuff,2 also minor stuff, 3 anything possible
    sampleCountTotal = length(labels);
    countCategory = length(unique(labels));%10
    normTresh = eps;
    Norm = 0;
    sampleImageIndices = ones(2,1);
    sampleLabels = labels(sampleImageIndices);
    %select 2 different images with norm of weights bigger than eps
    %for this particular hidden weights
    %then assign particular weights according to
    %1. mask of the hidden unit
    %2. difference of the randomly selected 2 images
    maxNormTillNow = [-inf -1 -1 -1 -1];
    j_sameLabel = 0;
    j_norm = 0;
    printOverNormTresh = false;
    while sampleLabels(1) == sampleLabels(2) ||  Norm < normTresh
        j = j_sameLabel+j_norm;
        if (maxNormTillNow(1)>normTresh && maxNormTillNow(4) ~= maxNormTillNow(5))
            %1x2
            sampleImageIndices = maxNormTillNow(2:3);
            %1x2
            sampleLabels = maxNormTillNow(4:5);
            printOverNormTresh = true;
        else
            %1x2
            sampleImageIndices = randperm(sampleCountTotal,2);
            %1x2
            sampleLabels = labels(sampleImageIndices);
        end

        if (sampleLabels(1) == sampleLabels(2))
            j_sameLabel = j_sameLabel+1;
            sampleLabels = randperm(countCategory,2);
            sampleIndicesForLabel = find(labels==sampleLabels(1));
            sampleImageIndices(1) = sampleIndicesForLabel(randperm(length(sampleIndicesForLabel),1));
            sampleIndicesForLabel = find(labels==sampleLabels(2));
            sampleImageIndices(2) = sampleIndicesForLabel(randperm(length(sampleIndicesForLabel),1));
            if mod(j_sameLabel,100)==0 && j_sameLabel>0 && displayInfoLevel>=1
                %directly take samples from definitely 2
                %different labels
                disp(['MJ-' num2str(i) ' of ' num2str(sizeHid) '-j_sameLabel(' num2str(j_sameLabel) '-' num2str(j) '), sampleLabels(' mat2str(sampleLabels) ')']);
            end
        end
        X_Diff = X(:,sampleImageIndices(1))-X(:,sampleImageIndices(2));
        Wrow = X_Diff.*receptiveFieldMask(i,:)'; %get masked C random weights
        Norm  = sqrt(sum(Wrow.^2));

        if (maxNormTillNow(1)<Norm)
            maxNormTillNow = [Norm,sampleImageIndices(:)',sampleLabels(:)'];
        end
        if Norm < normTresh
            j_norm = j_norm+1;
            normTresh = normTresh/1.1;
        end                    
    end
    
    if printOverNormTresh && displayInfoLevel>=2
        disp(['MN-' num2str(i) ' of ' num2str(sizeHid) '-j_norm(' num2str(j_norm) '-' num2str(j) '),Norm(' num2str(Norm) ')<normTresh(' num2str(normTresh) ')<eps(' num2str(eps) '), maxNormTillNow(' num2str(maxNormTillNow(1)) ')']);                    
    elseif (j>100) && displayInfoLevel>=1
        disp(['MJ-' num2str(i) ' of ' num2str(sizeHid) ' completed in j(' num2str(j) ')']);
    end
end