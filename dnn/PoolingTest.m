clear all;%
clc;%
%'sigmoid' is assumed as activation function
act = @(z_) 1.0 ./ (1.0 + exp(-z_));
actDeriva = @(a_) a_.*(1-a_);

data_01_i = bsxfun(@plus,repmat((1:32)',1,2),[0 32]);
data_00_i = reshape(data_01_i,[4,4,2,2]);
a1 = rand(size(data_01_i));

cnnnet = cell(1,1);
cnnnet{1,1} = struct;
cnnnet{1,1}.name = 'pool_1_3';
cnnnet{1,1}.type = 'pooling';
cnnnet{1,1}.type2 = 'mean';%it can be mean or max pooling
cnnnet{1,1}.stride = [0,0];
cnnnet{1,1}.poolSize = [2 2];
cnnnet{1,1}.padInputWithZeros = [0 0 0 0];%left right top bottom consecutively
cnnnet{1,1}.sizeChn = 2;%This would be initially given in later layers this can be calculated
cnnnet{1,1}.inputSize_original = size(data_00_i);

displayOpts = setDisplayOpts('all');

data = data_01_i;
[cnnnet{1,1}, pooledVectorizedData, pooledOriginalShapedData] = poolingStep( cnnnet{1,1}, data , displayOpts, true);
data_02_B = pooledVectorizedData;
data_02   = pooledOriginalShapedData;

%suppose we have 5 nodes in the output layer
data_out = [1 0 0 0 0;0 0 1 0 0]';
W_02_out_i = reshape(1:36*5,36,5);
W_02_out = rand(36,5);

a_03 = data_02_B * W_02_out ;