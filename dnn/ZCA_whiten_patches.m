function [ZCA_patches, ZCA_p] = ZCA_whiten_patches(patches, ZCA_p, applyDummyZCA, displayProcessInfo)
% ZCA-Whiten patches
% Multiplying patches([10800 45050]) with its inverse.
% Elapsed time is 45.206533 seconds.
% Dividing the result to number of samples (45050).
% Calculating SVD of ZCA_patches([10800 10800])
% Elapsed time is 227.742091 seconds.
% Multiplying U([10800 10800]) with diagonal matrix S([10800 10800])
% Elapsed time is 18.507997 seconds.
% Multiplying ZCA_p([10800 10800]) with matrix U([10800 10800])
% Elapsed time is 20.015000 seconds.
% Multiplying ZCA_p([10800 10800]) with matrix patches([10800 45050])
% Elapsed time is 85.287376 seconds.    

    if ~exist('displayProcessInfo','var') || isempty(displayProcessInfo)
        displayProcessInfo = false;
    end

    if isempty(ZCA_p)
        if (exist('applyDummyZCA','var') && applyDummyZCA)
            if displayProcessInfo
                disp('Creating identity dummy ZCA_p; because-> applyDummyZCA(true)');
            end
            ZCA_p = eye(size(patches,1));
            ZCA_patches = patches;
            return;
        end

        N = size(patches, 2);
        if displayProcessInfo
            disp(['Multiplying patches(' mat2str(size(patches)) ') with its inverse.']);
            tic();
        end

        ZCA_patches = patches * patches';

        if displayProcessInfo
            toc();
            disp(['Dividing the result to number of samples (' num2str(N) ').']);
        end

        ZCA_patches = ZCA_patches / N;

        if displayProcessInfo
            disp(['Calculating SVD of ZCA_patches(' mat2str(size(ZCA_patches)) ')']);
            tic();
        end

        [U,S,~] = svd(ZCA_patches);

        if displayProcessInfo
            toc();
        end    

        ZCA_epsilon = 1e-5;
        S = diag(1./sqrt(diag(S) + ZCA_epsilon));

        if displayProcessInfo
            disp(['Multiplying U(' mat2str(size(U)) ') with diagonal matrix S(' mat2str(size(S)) ')']);
            tic();
        end    

        ZCA_p = U * S;

        if displayProcessInfo
            toc();

            disp(['Multiplying ZCA_p(' mat2str(size(ZCA_p)) ') with matrix U(' mat2str(size(U')) ')']);
            tic();
        end

        ZCA_p = ZCA_p * U' ;

        if displayProcessInfo
            toc();
            disp(['Multiplying ZCA_p(' mat2str(size(ZCA_p)) ') with matrix patches(' mat2str(size(patches)) ')']);
            tic();
        end
    elseif displayProcessInfo
        toc();
        disp(['Multiplying ZCA_p(' mat2str(size(ZCA_p)) ') with matrix patches(' mat2str(size(patches)) ')']);
        tic();
    end
    
    ZCA_patches = ZCA_p * patches;
    
    if displayProcessInfo
        toc();
    end
end