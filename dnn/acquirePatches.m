function [patches, invalidPatchIDs] = acquirePatches( patchSize, data, sizeChn, imageIDs, leftTopMostPixIDs, teacherMode)

    if (~exist('teacherMode', 'var') || isempty(teacherMode))
        teacherMode = false;
    end
    
    if (numel(patchSize)==1)
        patchSize = [patchSize patchSize];
    end
    [sizeImg, countSamples, sizeChn, countFeat] = exploreDataSize(data, sizeChn);
    if length(size(data))>2
        data = reshape(data,numel(data)/countSamples,countSamples);
    end
     %data is dxN
     
    %[countFeat,countSamples] = size(data);
    A = reshape(1:sizeImg^2,sizeImg,sizeImg);
    A = A(1:patchSize(1),1:patchSize(2));
    A = A(:);
    if sizeChn==1
        if (teacherMode)
            %imageIDs = randi(countSamples,1,patchCount);
            %leftTopMostPixIDs = randi(countFeat-max(A),1,patchCount);
            patches = sub2ind(size(data),leftTopMostPixIDs,imageIDs);
            patches = repmat(patches,prod(patchSize),1);
            patches = bsxfun(@plus,patches,A-1);
            patches = data(ind2sub(size(data),patches));
            imSample = reshape(data(:,imageIDs(1)),[sizeImg sizeImg]);
            reshape(imSample(ind2sub([sizeImg sizeImg],A-1+leftTopMostPixIDs(1))),patchSize(1),patchSize(2));
        else
            patches = data(ind2sub(size(data),bsxfun(@plus,repmat( sub2ind(size(data),leftTopMostPixIDs,imageIDs),prod(patchSize),1),A-1)));
        end
    else
        if (teacherMode)
            %the left top most pixels of initial channels image
            leftTopMostPixIDs = sub2ind(size(data),leftTopMostPixIDs,imageIDs);
            leftTopMostPixIDs = reshape(leftTopMostPixIDs,[],numel(leftTopMostPixIDs));
            %the left top most pixels of all input channels image
            %patches = repmat(patches,sizeChn,1);
            %A was the left top most square of size "sizeImg by sizeImg"
            %for the initial channel - 
            %next line will make it for all input channels
            A = repmat(A,1,sizeChn);
            eachChannelTotalSize = sizeImg^2;
            %B will  have the iteration for a left topmost patch
            B = eachChannelTotalSize*repmat((1:sizeChn)'-1,1,prod(patchSize))';
            A = A+B;
            A = reshape(A,[],1);
            %A has the sample 1's patch that starts at the left top most pixel
            patches = bsxfun(@plus,leftTopMostPixIDs,A-1);
            
            %if available memory is not enough then patches will be
            %assigned part by part
            try
                patches = data(ind2sub(size(data),patches));
            catch
                patchesSizeMB = getVariableSize( patches, 'mb' );
                [ memAvail, memMatlab ] = displayMemory(false);
                patchCountTotal = size(patches,1);
                patchBatchCounts = 4*ceil(patchesSizeMB/memAvail); 
                if (patchBatchCounts>1)
                    patchBatchSizeArr = round(linspace(1,patchCountTotal,patchBatchCounts));
                    patchBatchSizeArr = [patchBatchSizeArr(1:end-1)' patchBatchSizeArr(2:end)'-1];
                    patchBatchSizeArr(end,end) = patchCountTotal;
                    disp(['Patches will be mapped in (' num2str(patchBatchCounts) ') parts - memAvail(' num2str(memAvail) ')<<patchSize(' num2str(patchesSizeMB) ') ']);
                    for i=1:size(patchBatchSizeArr)
                        p_fr = patchBatchSizeArr(i,1);
                        p_to = patchBatchSizeArr(i,2);
                        disp(['patches ' num2str(p_fr) '-' num2str(p_to) ' is being mapped']);
                        patches(p_fr:p_to,:) = data(ind2sub(size(data),patches(p_fr:p_to,:)));
                    end
                else
                    error(['Somehow patches could not be mapped :( - memAvail(' num2str(memAvail) ')<<patchSize(' num2str(patchesSizeMB) ') ']);
                end
            end
   
            %imSample = reshape(data(:,imageIDs(1)),sizeImg,sizeImg,sizeChn);
        else
            %the left top most pixels of initial channels image
            leftTopMostPixIDs = sub2ind(size(data),leftTopMostPixIDs,imageIDs);
            leftTopMostPixIDs = reshape(leftTopMostPixIDs,[],numel(leftTopMostPixIDs));
            %the left top most pixels of all input channels image
            %patches = repmat(patches,sizeChn,1);
            %A was the left top most square of size "sizeImg by sizeImg"
            %for the initial channel - 
            %next line will make it for all input channels
            A = repmat(A,1,sizeChn);
            eachChannelTotalSize = sizeImg^2;
            %B will  have the iteration for a left topmost patch
            B = eachChannelTotalSize*repmat((1:sizeChn)'-1,1,prod(patchSize))';
            A = A+B;
            A = reshape(A,[],1);
            %A has the sample 1's patch that starts at the left top most pixel
            patches = bsxfun(@plus,leftTopMostPixIDs,A-1);
            patches = data(ind2sub(size(data),patches));
            %imSample = reshape(data(:,imageIDs(1)),sizeImg,sizeImg,sizeChn);
        end
    end
    invalidPatchIDs = find(sum(patches)==0);
end