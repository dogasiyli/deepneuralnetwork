function [patches, imageIDs, labelsSelected]= acquireRandomPatches( patchCountPerSample, patchSize, data, sizeChn, teacherMode, labels)

    if (~exist('teacherMode', 'var') || isempty(teacherMode))
        teacherMode = false;
    end
    
    if (numel(patchSize)==1)
        patchSize = [patchSize patchSize];
    end
    [sizeImg, countSamples, sizeChn, countFeat] = exploreDataSize(data, sizeChn);
    if length(size(data))>2
        data = reshape(data,numel(data)/countSamples,countSamples);
    end
     %data is dxN
     
    [countFeat,countSamples] = size(data);
    A = reshape(1:sizeImg^2,sizeImg,sizeImg);
    A = A(1:patchSize(1),1:patchSize(2));
    A = A(:);
    
    minPixInds = max(1,sizeImg^2-max(A));
    
    imageIDs = reshape(repmat(1:countSamples,patchCountPerSample,1),1,[]);%randi(countSamples,1,countSamples*patchCountPerSample);
    labelsSelected = labels(imageIDs);
    leftTopMostPixIDs = randi(minPixInds,1,countSamples*patchCountPerSample);
    [patches, invalidPatchIDs] = acquirePatches( patchSize, data, sizeChn, imageIDs, leftTopMostPixIDs, teacherMode);
    
    patches(:,invalidPatchIDs) = [];
    imageIDs(invalidPatchIDs) = [];
    labelsSelected(invalidPatchIDs) = [];
end

