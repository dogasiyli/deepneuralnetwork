function [ C_AC, activatedData ] = activationStep( C_AC, data, displayOpts, saveOpts, dataStruct, deletePrevLayerBatch)
%C_AC = Cell_ACtivation is a struct

% STEP-0 : Check the input channel size and input size original
    if isfield(C_AC,'inputSize_original') && ~isfield(C_AC,'outputSize_original')
        C_AC.outputSize_original = C_AC.inputSize_original;%This came from the previous steps output dimension
    end

% STEP-1 : Define the activation functions if not yet defined
    C_AC = defineActivationFunctions(C_AC, displayOpts);

% STEP-2 : Apply activation
    %if number of output arguments is more than 1 construct/calculate activatedData
    if nargout>1
        activatedData = applyActivation(C_AC, data, displayOpts);
        clear data;%no need to have this anymore
    end
       
    if C_AC.fwdPropAllBatches
        outputFilterStatsAllBatches = saveAllBatchesToActivatedDataFolder(C_AC, dataStruct, displayOpts, saveOpts, @applyActivation, activatedData, deletePrevLayerBatch);
        C_AC.outputFilterStatsAllBatchesInitial = outputFilterStatsAllBatches;
        C_AC.fwdPropAllBatches = false;
    end
end

% STEP-1 : Define the activation functions if not yet defined
function [C_AC, activationFuncAlreadyDefined] = defineActivationFunctions(C_AC, displayOpts)
    activationFuncAlreadyDefined = isfield(C_AC,'act');
    if ~activationFuncAlreadyDefined
        switch (C_AC.activationType) 
            %z = W'x + b
            case 'tanh'
                %f(z) = tanh(z) = (e^z - e^-z)/(e^z + e^-z)
                %f'(z) = 1 ? (f(z))^2
                C_AC.actCatalizor = @(expZ_,expZMinus_) (expZ_-expZMinus_) ./ (expZ_+expZMinus_);
                C_AC.act = @(z_) C_AC.actCatalizor(exp(z_),exp(-z_));
                C_AC.actDerivative = @(a_) a_.*(1-a_);
            case 'relu'
                %f(z)  --> if z<0 make it 0
                %f'(z)  --> if z>0 1 else 0
                %http://mochajl.readthedocs.io/en/latest/user-guide/neuron.html
                C_AC.act = @(z_) max(eps,z_);
                C_AC.actDerivative = @(a_) double((eps+a_)>0);
                % if (displayOpts.warningInfo==1)
                %     disp([displayOpts.ies{6} 'WARNING----']);
                %     warning('derivative of Relu not known yet :(. hence RELU backpropogation is not implemented. Please check the code if backpropogation is intended.');
                % end
            case {'sigm','sigmoid'}
                %f(z) = 1 / (1 + exp(-z))
                %f'(z) = f(z).*(1 - f(z))
                C_AC.act = @(z_) 1.0 ./ (1.0 + exp(-z_));
                C_AC.actDerivative = @(a_) a_.*(1-a_);
            otherwise
                error(['No activation type such as (' C_AC.activationType ') is known']);
        end
    end
end

% STEP-2 : Apply activation
function activatedData = applyActivation(C_AC, X, displayOpts)
    activatedData = zeros(size(X),'double'); %#ok<NASGU> - This line is needed for the matlab to run faster
    if displayOpts.dataSize
        disp([displayOpts.ies{6} 'Activated, data in memory will be (' num2str(numel(X)/131072,'%4.2f') ' MB)']);
    end
    activatedData = C_AC.act(X);
end
