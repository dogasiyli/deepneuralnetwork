function [a, cnnnet, time_spent_all] = activation_ForwardPropogate(cnnnet, layer, data, varargin)
% STEP-0 : initializations
    time_spent_all = cputime;
    a = data;
    clear data;
    
% STEP-1 : Set optional param defaults
   [displayOpts, saveOpts, activationID, memoryMethod, dataStruct, deletePrevLayerBatch] = getOptionalParams(varargin{:});
% STEP-2 : Learn and set input and output sizes
    cnnnet = arrangeLayerSizes(cnnnet, layer);
% STEP-3 : Give the user information about what will be done by printing on the screen
    displayProcessInformation(a, cnnnet, layer, displayOpts);
% STEP-4 : Apply activation
    [cnnnet, a] = applyActivation(cnnnet, a, layer, displayOpts, saveOpts, dataStruct, deletePrevLayerBatch);
% STEP-5 : Save the activations to a file if a folder name is passed
    cnnnet = afp_saveAct(cnnnet, a, activationID, layer, saveOpts, displayOpts, memoryMethod);
% STEP-6 : Write the timing information on screen
    time_spent_all = timingInfo(time_spent_all, displayOpts);
end

% STEP-1 : Set optional param defaults
% Tested 2016_03_02
function [displayOpts, saveOpts, activationID, memoryMethod, dataStruct, deletePrevLayerBatch] = getOptionalParams(varargin)
    %P1D0 -> Passed assed paramaeter = 1, Default value used = 0
    [ saveOpts,  displayOpts,   activationID,    memoryMethod,  dataStruct,  deletePrevLayerBatch, P1D0] = parseArgs(...
    {'saveOpts' 'displayOpts'  'activationID'   'memoryMethod' 'dataStruct' 'deletePrevLayerBatch'},...
    {    []          []              0         'all_in_memory'      []             true           },...
    varargin{:}); 
    %optional param defaults explanations:
    %1. saveOpts     - needed to save the activation outputs of
    %                  pooling step to be able to backpropogate
    %2. displayOpts  - needed to be able to display what is
    %                  happening on the screen such as how much
    %                  time spent for the activation procedure
    %3. activationID  - for saving the activations on the
    %                   disk to be able to backpropogate 
    %                   we need to store the id of activations.
    %                   At layer_i we can be dealing with a_<i-2>
    
    %if displayOpts is not passed as a parameter then set is to 'all' by
    %default to show the user what will happen in every detail.
    %also return it as a parameter because even if it is not passed in it
    %would be returned to be used for upcoming evaluations
    if (isempty(displayOpts))
        displayOpts = setDisplayOpts('all');
        if (displayOpts.defaultParamSet && P1D0.displayOpts==0)
            disp([displayOpts.ies{4} 'Display options is set to -all- by default.']);
        end
    end
    if (isempty(saveOpts))
        saveOpts = setSaveOpts();
    end
    if ~exist(saveOpts.MainFolder,'dir')
        mkdir(saveOpts.MainFolder);
    end 
    if ~exist([saveOpts.MainFolder saveOpts.SubFolders.ActivatedData],'dir')
        mkdir([saveOpts.MainFolder saveOpts.SubFolders.ActivatedData]);
    end 
    %saveOpts
    if (P1D0.saveOpts ==0 && displayOpts.defaultParamSet)
        disp([displayOpts.ies{4} 'saveOpts is set to default(none).']);
    end    
    
    %memoryMethod
    if (P1D0.memoryMethod ==0 && displayOpts.defaultParamSet)
        disp([displayOpts.ies{4} 'memoryMethod is set to default(all_in_memory).']);
    end
end

% STEP-2 : Learn and set input and output sizes
% Tested 2016_03_02
function cnnnet = arrangeLayerSizes(cnnnet, layer)
    %the previous layer can be 
    %pooling-convolution-fullyconnected-softMax
    if (layer>1)
        %for pooling and convolution
        if ~isfield(cnnnet{layer,1},'inputSize_original') && isfield(cnnnet{layer-1,1},'outputSize_original')
            cnnnet{layer,1}.inputSize_original = cnnnet{layer-1,1}.outputSize_original;
        %for fullyconnected and softMax
        elseif ~isfield(cnnnet{layer,1},'inputSize') && isfield(cnnnet{layer-1,1},'outputSize')
            cnnnet{layer,1}.inputSize = cnnnet{layer-1,1}.outputSize;
            cnnnet{layer,1}.outputSize = cnnnet{layer-1,1}.outputSize;
        end
    end
end

% STEP-3 : Give the user information about what will be done by printing on the screen
% Tested 2016_03_02
function countSamples = displayProcessInformation(a, cnnnet, layer, displayOpts)
    % if time spent needs to be passed here is how it goes
    countSamples = size(a, 2);
    if (isfield(cnnnet{layer,1},'inputSize_original') && displayOpts.calculation)
        disp([displayOpts.ies{4} 'Data of size ' mat2str([cnnnet{layer,1}.inputSize_original(1:end-1) countSamples]) ' will be activated using <' cnnnet{layer,1}.activationType '> function.']);       
    end
end

% STEP-4 : Apply activation
% Tested 2016_03_02
function [cnnnet, a] = applyActivation(cnnnet, a, layer, displayOpts, saveOpts, dataStruct, deletePrevLayerBatch)
    time_spent_activate = cputime;
        [cnnnet{layer,1},a] = activationStep( cnnnet{layer,1}, a, displayOpts, saveOpts, dataStruct, deletePrevLayerBatch);
    time_spent_activate = cputime - time_spent_activate;    
    if     (displayOpts.timeMode>=2 && displayOpts.calculation==1)
            disp([displayOpts.ies{4} 'Data has been activated using <' cnnnet{layer,1}.activationType '> function in ' num2str(time_spent_activate,'%4.2f') ' cputime.']);
    elseif (displayOpts.timeMode< 2 && displayOpts.calculation==1)
            disp([displayOpts.ies{4} 'Data has been activated using <' cnnnet{layer,1}.activationType '> function.']);
    elseif (displayOpts.timeMode< 2 && displayOpts.calculation==1)
            disp([displayOpts.ies{4} 'Data has been activated.']);
    end    
end

% STEP-5 : Save the activations to a file if a folder name is passed
% Tested 2016_03_02
function cnnnet = afp_saveAct(cnnnet, a, activationID, layerID, saveOpts, displayOpts, memoryMethod)
    save_onlyLastLayerNeeded = (strcmp(saveOpts.Params.Activations,'Last') && cnnnet{layerID,1}.LastActivationLayer);%if last will be saved
    save_everythingShallBeSaved = strcmp(saveOpts.Params.Activations,'Every');%each of them will be saved  
    save_memoryMethod_writeToDisk = activationID>0 && strcmp(memoryMethod,'write_to_disk');                    
    if (save_memoryMethod_writeToDisk && (save_onlyLastLayerNeeded || save_everythingShallBeSaved))
        saveActivation(a, saveOpts, activationID, layerID, displayOpts );
        if isfield(cnnnet{activationID,1},'activatedData')
            cnnnet{activationID,1} = rmfield(cnnnet{activationID,1},'activatedData');
        end
    end
    if strcmp(memoryMethod,'all_in_memory')
        cnnnet{activationID,1}.activatedData = a;
    end
end

% STEP-6 : Write the timing information on screen
% Tested 2016_03_02
function time_spent_all = timingInfo(time_spent_all, displayOpts)
    time_spent_all = cputime - time_spent_all;
    if (displayOpts.timeMode>=2)
        disp([displayOpts.ies{4} 'Activation step completed in ' num2str(time_spent_all) ' cputime.' ]);
    end
end