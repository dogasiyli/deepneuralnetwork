function addline_resultepochfile( resultTableFile_01_accPerBatch, dataTypeStr, dataStruct, epochID, values) %#ok<INUSL>
    try
        if nargin<4
            if exist(resultTableFile_01_accPerBatch,'file')
                delete(resultTableFile_01_accPerBatch);
            end
            fid = fopen(resultTableFile_01_accPerBatch, 'a');
            if (fid > 0)
                fprintf(fid,'% 10s ','EpochID');
                switch dataTypeStr
                    case 'train'
                        for i=1:eval(['dataStruct.countBatch.' dataTypeStr])
                            addNumStr = ['B' num2str(i,'%02d')];
                            fprintf(fid,'% 10s% 10s',addNumStr, addNumStr);
                        end
                    otherwise
                        for i=1:eval(['dataStruct.countBatch.' dataTypeStr])
                            addNumStr = ['B' num2str(i,'%02d')];
                            fprintf(fid,'% 10s',addNumStr);
                        end
                end
                fprintf(fid,'\n');
                fclose(fid);
            end
        else
            fid = fopen(resultTableFile_01_accPerBatch, 'a');
            if (fid > 0)
                fprintf(fid,'%6s %03d  ',' ',epochID);
                for i=1:eval(['dataStruct.countBatch.' dataTypeStr])
                    switch dataTypeStr
                        case 'train'
                            fprintf(fid,'%8.6f %8.6f ',values(i*2-1),values(i*2));
                        otherwise
                            fprintf(fid,'%8.6f ',values(i));
                    end
                end
                fprintf(fid,'\n');
                fclose(fid);
            end        
        end
    catch
    end
end

