function addline_resultsummaryfile( trainResultSummaryFileName, epochID, batchID, valuesMat)
    if nargin==1
        if exist(trainResultSummaryFileName,'file')
            delete(trainResultSummaryFileName);
        end
        fid = fopen(trainResultSummaryFileName, 'a');
        if (fid > 0)
            fprintf(fid,'%7s;% 8s;% 10s;% 8s;% 12s;% 11s;% 10s;% 9s;% 5s\n','epochID','batchID','iteration','funeval','stepLen','gtd','cost','accuracy','type');
            fclose(fid);
        end
    else
        fid = fopen(trainResultSummaryFileName, 'a');
        if (fid > 0)
            for i=1:size(valuesMat,1)
                iterationVal =valuesMat(i,1);
                funeval =valuesMat(i,2);
                stepLen =valuesMat(i,3);
                gtd_cur =valuesMat(i,4);
                cost_cur =valuesMat(i,5);
                accuracyCalced =valuesMat(i,6);
                typeOfIteration =valuesMat(i,7);
                if isnan(gtd_cur)==1
                    gtd_cur = -0;
                end
                fprintf(fid,'%4s%03d;%5s%03d;%6s%04d;%4s%04d; %7.5e; %4.3e; %5.3e;%2s%7.4f;   %02d\n',' ',epochID,' ',batchID,' ',iterationVal,' ',funeval,stepLen,gtd_cur,cost_cur,' ',accuracyCalced,typeOfIteration);
            end
            fclose(fid);
        end
    end
end

