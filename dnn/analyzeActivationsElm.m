function [gestLabels_Helm, sampleCountTotal, sampleCountChanged, accuracyCalced, confMatResult] = analyzeActivationsElm(data, labels, Welm, displayInfo)
    if ~exist('displayInfo','var') || isempty(displayInfo)
        displayInfo = false;
    end
    %sigm = @(z_) 1.0 ./ (1.0 + exp(-z_));
    Helm = data*Welm;%[nh = nd*dh]
    %Helm = sigm(Helm);
    [~,gestLabels_Helm] = max(Helm,[],2);
    sampleCountTotal = length(labels);
    sampleCountChanged = sum(gestLabels_Helm(:)~=labels(:));
    accuracyCalced = 100 - 100*sampleCountChanged/sampleCountTotal;
    if displayInfo
        disp([ '(' num2str((sampleCountChanged),'%d') '/' num2str((sampleCountTotal),'%d') ')' num2str((accuracyCalced),'%4.2f') ' percent accuracy'])
    end
    confMatResult = confusionmat(labels,gestLabels_Helm);
end