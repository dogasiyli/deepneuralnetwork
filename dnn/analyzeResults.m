function [predictedLabels, accuracyCalced, confMatResult, sampleCountTotal, sampleCountChanged, hiddenLayerActivations, labelActivations] = analyzeResults(X, givenLabels, W_randoms, Welm, dispConfusionMat, hiddenLayerActivations, OPS)
    %error('analyseResults function should also take bias params for the last layer into account');
    if ~exist('OPS', 'var') || ~isstruct(OPS)
        OPS = [];
    end
    categoryCount	= getStructField(OPS, 'categoryCount', []);
    categories	= getStructField(OPS, 'categories', []);
    givenLabels = reshape(givenLabels,1,[]);
    
    if (~exist('dispConfusionMat','var') || isempty(dispConfusionMat))
        dispConfusionMat = false;
    end
    if (~exist('hiddenLayerActivations','var'))
        hiddenLayerActivations = [];
        sampleCountTotal = size(X,2);
    else
        sampleCountTotal = size(hiddenLayerActivations,2);
    end
    if isempty(hiddenLayerActivations)
        sigm = @(z_) 1.0 ./ (1.0 + exp(-z_));
        [countFeat,sampleCountTotal] = size(X);
        hiddenLayerActivations = zeros(size(W_randoms,1),countFeat);
        hiddenLayerActivations = sigm(W_randoms*([X;ones(1,sampleCountTotal)]));
    end
    Helm = Welm*hiddenLayerActivations;
    [~,predictedLabels] = max(Helm);
    predictedLabels = reshape(predictedLabels,1,[]);
    
    if nargout>3 && ~exist('sampleCountTotal','var')
        sampleCountTotal = size(X,2);
    end
    if nargout>6
        Helm_maxNormalized = bsxfun(@rdivide,Helm,max(Helm));
        labelActivations = struct;
        matrixSize = size(Helm);
        labelActivations.predicted = Helm_maxNormalized(sub2ind(matrixSize,predictedLabels,1:length(predictedLabels)));
        try 
            labelActivations.given = Helm_maxNormalized(sub2ind(matrixSize,givenLabels,1:length(givenLabels)));
        catch
            labelActivations.given = NaN(size(labelActivations.predicted));
        end
        labelActivations.predicted_un = Helm(sub2ind(matrixSize,predictedLabels,1:length(predictedLabels)));
        try 
            labelActivations.given_un = Helm(sub2ind(matrixSize,givenLabels,1:length(givenLabels)));
        catch
            labelActivations.given_un = NaN(size(labelActivations.predicted_un));
        end
    end
    given_categories = unique(givenLabels);
    predictied_categories = unique(predictedLabels);
    all_categories = unique([given_categories predictied_categories]);
    if isempty(categoryCount)
        categoryCount = length(all_categories);        
    end
    if isempty(categories)
        categories = cellstr(num2str(all_categories));
    end
    
    [accuracyCalced, confMatResult, sampleCountChanged, label_map] = calculateConfusion(predictedLabels,givenLabels,dispConfusionMat);
end