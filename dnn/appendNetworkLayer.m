function cnnnet_out = appendNetworkLayer( cnnnet_in, layerType, displayOpts, varargin )    
    if (~exist('cnnnet_in','var') || isempty(cnnnet_in))
         cnnnet_out = cell(1,1);
    else
        layerCountIn = size(cnnnet_in,1);
        cnnnet_out = cell(layerCountIn+1,1);
        cnnnet_out(1:layerCountIn,1) = cnnnet_in(1:layerCountIn,1);
        clear cnnnet_in;
    end
    
    curLayerCount = size(cnnnet_out,1);
    curLayerUniqueID = layerTypeCount(cnnnet_out, layerType);
    curLayerName = [layerType '_' num2str(curLayerUniqueID+1) '_'  num2str(curLayerCount) ];

     if (displayOpts.calculation==1)
         if curLayerCount==1
            disp([displayOpts.ies{2} 'Your initial layer is being added to your cnn network.'])
         else
            disp([displayOpts.ies{2} 'Layer(' num2str(curLayerCount) ') is being added to your cnn network.'])
         end
     end
    
    cnnnet_out{curLayerCount,1}.name = curLayerName;
    cnnnet_out{curLayerCount,1}.type = layerType;
    cnnnet_out{curLayerCount,1}.layerIndice = curLayerCount;
    cnnnet_out{curLayerCount,1}.fwdPropAllBatches = true;
    if (displayOpts.layerNames==1)
        disp([displayOpts.ies{4} 'Type of layer : ' layerType])
        disp([displayOpts.ies{4} 'Name of layer : ' curLayerName])
    end

%This will append layers to the cnn
    switch layerType
        case 'convolution'
            cnnnet_out = appendConvolutionLayer(cnnnet_out, curLayerCount, displayOpts, varargin{:});
        case 'activation'
            cnnnet_out = appendActivationLayer(cnnnet_out, curLayerCount, displayOpts, varargin{:});
        case 'pooling'
            cnnnet_out = appendPoolingLayer(cnnnet_out, curLayerCount, displayOpts, varargin{:});            
        case 'softmax'
            cnnnet_out = appendSoftmaxLayer(cnnnet_out, curLayerCount, displayOpts, varargin{:});
        case 'fullyconnected'
            cnnnet_out = appendFullyConnected(cnnnet_out, curLayerCount, displayOpts, varargin{:});
        otherwise
            error('no such layer has been implemented yet');
    end
    cnnnet_out = addActivationCount(cnnnet_out, curLayerCount, layerType);
end

function cnnnet_out = addActivationCount(cnnnet_out, curLayerCount, layerType)
    cnnnet_out{curLayerCount,1}.LastActivationLayer = false;
    if curLayerCount==1
        cnnnet_out{curLayerCount,1}.ActivationLayerID = 1;
    else
        switch layerType
            case {'activation','pooling'}
                lastActivationLayerID = 0;
                for i=1:curLayerCount
                    if isfield(cnnnet_out{i,1},'ActivationLayerID')
                        lastActivationLayerID = cnnnet_out{i,1}.ActivationLayerID;
                    end
                    cnnnet_out{i,1}.LastActivationLayer = false;
                end
                cnnnet_out{curLayerCount,1}.LastActivationLayer = true;
                cnnnet_out{curLayerCount,1}.ActivationLayerID = lastActivationLayerID+1;
                cnnnet_out{1,1}.lastActivationLayer_ID = curLayerCount;
                cnnnet_out{1,1}.lastActivationLayer_actID = lastActivationLayerID+1;
        end
    end
end

function cnnnet_out = appendSoftmaxLayer(cnnnet_out, curLayerCount, displayOpts, varargin)
    %P1D0 -> Passed assed paramaeter = 1, Default value used = 0
    [ initializeWeightMethod,  softmaxLambda,  maxIter,  minFuncMaxIter , minFuncMethod,  useAccuracyAsCost,  countCategory,  priorInfoUse, P1D0] = parseArgs(...
    {'initializeWeightMethod' 'softmaxLambda' 'maxIter' 'minFuncMaxIter' 'minFuncMethod' 'useAccuracyAsCost' 'countCategory' 'priorInfoUse'},...
    {        'Random'             1e-4          30           200            'lbfgs'            false              []            'none'     },...
    varargin{:});

    cnnnet_out{curLayerCount,1}.initializeWeightMethod = initializeWeightMethod; %<this can be either random or ELM>
    cnnnet_out{curLayerCount,1}.useAccuracyAsCost = useAccuracyAsCost;
    
    cnnnet_out{curLayerCount,1}.softMaxParams = struct;
    cnnnet_out{curLayerCount,1}.softMaxParams.softmaxLambda = softmaxLambda;
    cnnnet_out{curLayerCount,1}.softMaxParams.maxIter = maxIter;
    
    cnnnet_out{curLayerCount,1}.softMaxParams.paramsMinFunc = struct;
    cnnnet_out{curLayerCount,1}.softMaxParams.paramsMinFunc.maxIter = minFuncMaxIter;
    cnnnet_out{curLayerCount,1}.softMaxParams.paramsMinFunc.Method = minFuncMethod;
    if (P1D0.countCategory == 1)
        cnnnet_out{curLayerCount,1}.countCategory = countCategory;
    end
    cnnnet_out{curLayerCount,1}.priorInfoUse = priorInfoUse;%can be "sum","max","none"
    
    if (displayOpts.defaultParamSet==1)
        if (P1D0.initializeWeightMethod == 0)
            disp([displayOpts.ies{4} 'weight initialization method set to default(' initializeWeightMethod]);
        end
        if (P1D0.softmaxLambda == 0)
            disp([displayOpts.ies{4} 'softmaxLambda set to default(' num2str(softmaxLambda) ')']);
        end
        if (P1D0.maxIter == 0)
            disp([displayOpts.ies{4} 'maximum iteration count of softMax layer is set to default(' num2str(maxIter) ')']);
        end
        if (P1D0.minFuncMaxIter == 0)
            disp([displayOpts.ies{4} 'maximum iteration count of softMax-minFunc parameters is set to default(' num2str(minFuncMaxIter) ')']);
        end
        if (P1D0.minFuncMethod == 0)
            disp([displayOpts.ies{4} 'minFuncMethod is set to default(' minFuncMethod ')']);
        end
        if (P1D0.useAccuracyAsCost == 0)
            disp([displayOpts.ies{4} 'useAccuracyAsCost is set to default(false)']);
        end
        if (P1D0.priorInfoUse == 0)
            disp([displayOpts.ies{4} 'priorInfoUse is set to default(none)']);
        end
    end
    if (strcmp(displayOpts.displayLevel,'all'))
        cnnnet_out{curLayerCount,1}.softMaxParams.paramsMinFunc.display = 'on';
        if (P1D0.initializeWeightMethod == 1)
            disp([displayOpts.ies{4} 'weight initialization method set to (' initializeWeightMethod ')']);
        end
        if (P1D0.countCategory == 1)
            disp([displayOpts.ies{4} 'countCategory is set to (' num2str(countCategory) ')']);
        end
        if (P1D0.priorInfoUse == 1)
            disp([displayOpts.ies{4} 'priorInfoUse method set to (' priorInfoUse ')']);
        end
    else
        cnnnet_out{curLayerCount,1}.softMaxParams.paramsMinFunc.display = 'off';
    end
end

function cnnnet_out = appendConvolutionLayer(cnnnet_out, curLayerCount, displayOpts, varargin)
    %mandotary values to be given :
    %countFilt and inputChannelSize(when it is initial layer)
    %e.g.
    %cnnnet = appendNetworkLayer( cnnnet, 'convolution', 'countFilt', 32, 'inputChannelSize', 1 )
    %P1D0 -> Passed assed paramaeter = 1, Default value used = 0
    [ stride,  filterSize,  countFilt,  inputChannelSize,  initializeWeightMethod,  activationType,  randInitMode,  deltaWeightMethod,   bnMode,   wvnMode,   wvnVal,   applyOrthonormalization,  filterStatsTrackMode, P1D0] = parseArgs(...
    {'stride' 'filterSize' 'countFilt' 'inputChannelSize' 'initializeWeightMethod' 'activationType' 'randInitMode' 'deltaWeightMethod'  'bnMode'  'wvnMode' 'wvnValue' 'applyOrthonormalization' 'filterStatsTrackMode'},...
    {  [1,1]     [2 2]          []               []              'Autoencoder'         'sigmoid'    'Experimental'  'slack_mul_activ'  'initial'  'initial'     6                 []               'perBatchInitial'   },...
    varargin{:});

    %Mandatory_01-countFilt
    if (isempty(countFilt))
        error('You have to define countFilt as an input parameter')
    else
        cnnnet_out{curLayerCount,1}.countFilt = countFilt;%NO DEFAULT VALUE
    end

    %Mandatory_02-inputChannelSize
    if isempty(inputChannelSize)%if no inputChannelSize is passed as a parameter
        if (curLayerCount==1)%for initial layer this is an error
            error('You have to define inputChannelSize for the initial layer')
        end
    else
        %This would be given for initial later for later layers this can be calculated/grabbed from previous layer
        cnnnet_out{curLayerCount,1}.inputChannelSize = inputChannelSize;
    end            

    %optional parameters which have default values
    cnnnet_out{curLayerCount,1}.stride = stride;%default = [1,1];
    cnnnet_out{curLayerCount,1}.filterSize = filterSize;%default [2 2];
    cnnnet_out{curLayerCount,1}.initializeWeightMethod = initializeWeightMethod;%can be <(default)'Autoencoder','SparseClassifier','Random','ELM'>
    cnnnet_out{curLayerCount,1}.deltaWeightMethod = deltaWeightMethod;
    cnnnet_out{curLayerCount,1}.activationType = activationType;%can be <(default)'sigmoid','ReLu','tanh'>
    [cnnnet_out{curLayerCount,1}.act, cnnnet_out{curLayerCount,1}.actDerivative, cnnnet_out{curLayerCount,1}.actCatalizor] = setActivationFunction(activationType);

    switch (initializeWeightMethod)%
        case {'Autoencoder','1'}
            cnnnet_out{curLayerCount,1}.initializeWeightMethod = 'Autoencoder';
            cnnnet_out{curLayerCount,1}.paramsAutoEncode = struct; 
            cnnnet_out{curLayerCount,1}.paramsAutoEncode.activationType = 'sigm';%activationType            
            if isempty (applyOrthonormalization)
                applyOrthonormalization = false;
            end
        case {'ELM','2'}
            cnnnet_out{curLayerCount,1}.initializeWeightMethod = 'ELM';
            %cnnnet_out{curLayerCount,1}.paramsELM = struct;
            if isempty (applyOrthonormalization)
                applyOrthonormalization = false;
            end
        case {'Random','3'}
            cnnnet_out{curLayerCount,1}.initializeWeightMethod = 'Random';
            cnnnet_out{curLayerCount,1}.randInitMode = randInitMode;%'Experimental, Xavier, Glorot_Bengio, He_Rang_Zhen                    
            if (displayOpts.warningInfo==1)
                disp([displayOpts.ies{4} 'WARNING : Random initialization has some drawbacks. Use autoencoder with small number of samples instead.']);
            end
            if isempty (applyOrthonormalization)
                applyOrthonormalization = true;
            end
        case {'SparseClassifier','4'}
            cnnnet_out{curLayerCount,1}.initializeWeightMethod = 'SparseClassifier';
            cnnnet_out{curLayerCount,1}.paramsSparseClassifier = struct; 
            cnnnet_out{curLayerCount,1}.paramsSparseClassifier.activationType = 'sigm';%activationType
            if isempty (applyOrthonormalization)
                applyOrthonormalization = false;
            end
    end
    cnnnet_out{curLayerCount,1}.applyOrthonormalization = applyOrthonormalization;
    
    %weightVarianceNormalizationMode
    switch wvnMode
        case 'none'
            cnnnet_out{curLayerCount,1}.applyVarNorm=false;
        case {'initial','epoch','batch','iteration'}
            %set to false right after applying
            cnnnet_out{curLayerCount,1}.applyVarNorm=true;
        otherwise
            wvnMode = 'none';
            cnnnet_out{curLayerCount,1}.applyVarNorm=false;
    end
    cnnnet_out{curLayerCount,1}.weightVarianceNormalizationMode = wvnMode;
    cnnnet_out{curLayerCount,1}.weightVarianceNormalizationValue = wvnVal;
    
    %biasNormalizationMode
    switch bnMode
        case 'none'
            cnnnet_out{curLayerCount,1}.applyBiasNorm=false;
        case {'initial','epoch','batch','iteration'}
            %set to false right after applying
            cnnnet_out{curLayerCount,1}.applyBiasNorm=true;
        otherwise
            bnMode = 'none';
            cnnnet_out{curLayerCount,1}.applyBiasNorm=false;
    end
    cnnnet_out{curLayerCount,1}.biasNormalizationMode = bnMode;
    
    
    switch filterStatsTrackMode
        case 'last'
            appendMeanVarOfFilter = false;
        case 'perBatchInitial'
            appendMeanVarOfFilter = true;
            %make sure to set to true again before calling minfunclog
        case 'perBatchIteration'
            appendMeanVarOfFilter = true;
            %make sure to not to set to false in convolutionStep.m
        otherwise
            filterStatsTrackMode = 'last';
    end
    cnnnet_out{curLayerCount,1}.filterStatsTrackMode = filterStatsTrackMode;
    cnnnet_out{curLayerCount,1}.appendMeanVarOfFilter = appendMeanVarOfFilter;
    

    if (displayOpts.defaultParamSet==1)
        if (P1D0.stride == 0)
            disp([displayOpts.ies{4} 'stride parameter set to default value(' mat2str(stride) ')']);
        end
        if (P1D0.filterSize == 0)
            disp([displayOpts.ies{4} 'filterSize set to default value(' mat2str(filterSize) ')']);
        end
        if (P1D0.initializeWeightMethod == 0)
            disp([displayOpts.ies{4} 'weight initialization method set to default(' initializeWeightMethod]);
        end
        if (P1D0.activationType == 0)
            disp([displayOpts.ies{4} 'activationType type set to default(' activationType ')']);
        end
        if (P1D0.randInitMode == 0 && strcmp(cnnnet_out{curLayerCount,1}.initializeWeightMethod,'Random'))
            disp([displayOpts.ies{4} 'random initialization mode set to default(' randInitMode ')']);
        end
        if (P1D0.deltaWeightMethod == 0)
            disp([displayOpts.ies{4} 'deltaWeightMethod mode set to default(' deltaWeightMethod ')']);
        end
    end
    if (strcmp(displayOpts.displayLevel,'all'))
        if (P1D0.stride == 1)
            disp([displayOpts.ies{4} 'stride parameter set to (' mat2str(stride) ')']);
        end
        if (P1D0.filterSize == 1)
            disp([displayOpts.ies{4} 'filterSize set to (' mat2str(filterSize) ')']);
        end
        if (P1D0.initializeWeightMethod == 1)
            disp([displayOpts.ies{4} 'weight initialization method set to (' initializeWeightMethod ')'] );
        end
        if (P1D0.activationType == 1)
            disp([displayOpts.ies{4} 'activationType type set to (' activationType ')']);
        end
        if (P1D0.randInitMode == 1 && strcmp(cnnnet_out{curLayerCount,1}.initializeWeightMethod,'Random'))
            disp([displayOpts.ies{4} 'random initialization mode set to (' randInitMode ')']);
        end
    end
end

function cnnnet_out = appendPoolingLayer(cnnnet_out, curLayerCount, displayOpts, varargin)
    %e.g. cnnnet = appendNetworkLayer( cnnnet, 'pooling', 'poolSize', [2 2], 'poolType', 'mean' )
    %P1D0 -> Passed assed paramaeter = 1, Default value used = 0
    [ stride,  poolSize,  padInputWithZeros,  poolType, P1D0] = parseArgs(...
    {'stride' 'poolSize' 'padInputWithZeros' 'poolType' },...
    {[0,0]    [2 2]      [0 0 0 0]           'mean'     },...
    varargin{:});

    cnnnet_out{curLayerCount,1}.poolType = poolType;%it can be mean or max pooling
    cnnnet_out{curLayerCount,1}.stride = stride;
    cnnnet_out{curLayerCount,1}.poolSize = poolSize;
    cnnnet_out{curLayerCount,1}.padInputWithZeros = padInputWithZeros;%left right top bottom consecutively

    if (displayOpts.defaultParamSet==1)
        if (P1D0.stride == 0)
            disp([displayOpts.ies{4} 'stride parameter set to default value(' mat2str(stride) ')']);
        end
        if (P1D0.poolSize == 0)
            disp([displayOpts.ies{4} 'poolSize set to default value(' mat2str(poolSize) ')']);
        end
        if (P1D0.padInputWithZeros == 0)
            disp([displayOpts.ies{4} 'padInputWithZeros set to default value(' mat2str(padInputWithZeros) ')']);
        end
        if (P1D0.poolType == 0)
            disp([displayOpts.ies{4} 'poolType type set to default(' poolType ')']);
        end
    end
    if (strcmp(displayOpts.displayLevel,'all'))
        if (P1D0.stride == 1)
            disp([displayOpts.ies{4} 'stride parameter set to (' mat2str(stride) ')']);
        end
        if (P1D0.poolSize == 1)
            disp([displayOpts.ies{4} 'poolSize set to (' mat2str(poolSize) ')']);
        end
        if (P1D0.padInputWithZeros == 1)
            disp([displayOpts.ies{4} 'padInputWithZeros set to (' mat2str(padInputWithZeros) ')']);
        end
        if (P1D0.poolType == 1)
            disp([displayOpts.ies{4} 'poolType type set to (' poolType ')']);
        end
    end
end

function cnnnet_out = appendActivationLayer(cnnnet_out, curLayerCount, displayOpts, varargin)
    %mandotary values to be given - none
    %optional parameters which have default values
    %activationType - can be taken from the previous layers activationtype
    %
    %e.g.
    %cnnnet = appendNetworkLayer( cnnnet, 'activation', 'activationType', 'sigmoid' )
    
    %try to get the previous layers activation function
    if isfield(cnnnet_out{curLayerCount-1,1},'activationType') 
        prevActivationType = cnnnet_out{curLayerCount-1,1}.activationType;
    else
        prevActivationType = [];
    end   
    if (isempty(prevActivationType))
        %prevActivationType is empty so this is an error.
        %previous layer has to have an activation function defined.
        error('An activation layer can be appended after convolution, softmax or fullyConnected layer and all three of them has an activation function.');
    end

    %now attempt to get activation function given explicitly by the user
    %into this function
    %P1D0 -> Passed assed paramaeter = 1, Default value used = 0
    [   activationType, P1D0] = parseArgs(...
    {  'activationType'  },...
    { prevActivationType },...
    varargin{:});

    if (strcmp(activationType,prevActivationType))
        %everything is ok.
        %the user explicitly mentioned the same activation function
        %although he/she doesnt need to
    elseif (~isempty(prevActivationType))
        %there is a mismatch between the new activation function to be used
        %and the previous one
        %so stick with the old one that has been taken from the previous
        %layer's aActivationType(prevActivationType)
        activationType = prevActivationType;
    end

    if (displayOpts.defaultParamSet==1 && P1D0.activationType==0)
        disp([displayOpts.ies{4} 'activationType parameter set to default value(' activationType ')']);
    elseif (strcmp(displayOpts.displayLevel,'all'))
        disp([displayOpts.ies{4} 'activationType parameter set to (' activationType ')']);        
    end

    cnnnet_out{curLayerCount,1}.activationType = activationType;%can be sigmoid, tanh or relu
    [cnnnet_out{curLayerCount,1}.act, cnnnet_out{curLayerCount,1}.actDerivative, cnnnet_out{curLayerCount,1}.actCatalizor] = setActivationFunction(activationType);
end

function cnnnet_out = appendFullyConnected(cnnnet_out, curLayerCount, displayOpts, varargin)
    %Tested - 20160222 - 
    %Random needs to be tested
    %e.g. cnnnet = appendNetworkLayer( cnnnet, 'fullyconnected', 'sizeHid', 100, 'activationType', 'sigmoid' )
    %P1D0 -> Passed assed paramaeter = 1, Default value used = 0
    [ sizeHid,  initializeWeightMethod,  activationType,  paramsAutoEncode,  randInitMode, P1D0] = parseArgs(...
    {'sizeHid' 'initializeWeightMethod' 'activationType' 'paramsAutoEncode' 'randInitMode'},...
    {   []          'Autoencoder'          'sigmoid'            -1          'Experimental'},...
    varargin{:});

    %Mandatory_01-countFilt
    if (isempty(sizeHid))
        error('You have to define sizeHid as an input parameter')
    else
        cnnnet_out{curLayerCount,1}.sizeHid = sizeHid;%NO DEFAULT VALUE
    end

    cnnnet_out{curLayerCount,1}.initializeWeightMethod = initializeWeightMethod;%can be <(default)'Autoencoder','Random','ELM'>
    cnnnet_out{curLayerCount,1}.activationType = activationType;
    switch (initializeWeightMethod)
        case 'Autoencoder'
            if (paramsAutoEncode==-1 || ~isstruct(paramsAutoEncode))
                paramsAutoEncode =  setParams_autoEncoder();
            end
            paramsAutoEncode = changeAutoencoderParams(paramsAutoEncode, 'activationType', activationType);
            cnnnet_out{curLayerCount,1}.paramsAutoEncode = paramsAutoEncode;            
        case 'ELM'
            cnnnet_out{curLayerCount,1}.paramsELM = setParams_ELM;
        case 'Random'
            cnnnet_out{curLayerCount,1}.randInitMode = randInitMode;%Experimental, Xavier, Glorot_Bengio, He_Rang_Zhen
    end
    [cnnnet_out{curLayerCount,1}.act, cnnnet_out{curLayerCount,1}.actDerivative, cnnnet_out{curLayerCount,1}.actCatalizor] = setActivationFunction(activationType);

    if (displayOpts.defaultParamSet==1)
        if (P1D0.initializeWeightMethod == 0)
            disp([displayOpts.ies{4} 'Weight initialization method set to default(' initializeWeightMethod ')']);
        end
        if (P1D0.activationType == 0)
            disp([displayOpts.ies{4} 'ActivationType type set to default(' activationType ')']);
        end
        if (P1D0.paramsAutoEncode == 0)
            disp([displayOpts.ies{4} 'AutoEncoderParams parameters set to default values']);
        end
        if (P1D0.randInitMode == 0 && strcmp(cnnnet_out{curLayerCount,1}.initializeWeightMethod,'Random'))
            disp([displayOpts.ies{4} 'Random initialization mode set to default(' randInitMode ')']);
        end
    end
    if (strcmp(displayOpts.displayLevel,'all'))
        if (P1D0.initializeWeightMethod == 1)
            disp([displayOpts.ies{4} 'Weight initialization method set to (' initializeWeightMethod ')']);
        end
        if (P1D0.activationType == 1)
            disp([displayOpts.ies{4} 'ActivationType type set to (' activationType ')']);
        end   
        if (P1D0.randInitMode == 1 && strcmp(cnnnet_out{curLayerCount,1}.initializeWeightMethod,'Random'))
            disp([displayOpts.ies{4} 'Random initialization mode set to (' randInitMode ')']);
        end                
        if isfield(cnnnet_out{curLayerCount,1},'paramsAutoEncode')
            disp([displayOpts.ies{4} 'AutoEncoder parameters set to values below']);
            disp([displayOpts.ies{6} 'sparsityParam = ' num2str(cnnnet_out{curLayerCount,1}.paramsAutoEncode.sparsityParam)]);
            disp([displayOpts.ies{6} 'weightDecayParam = ' num2str(cnnnet_out{curLayerCount,1}.paramsAutoEncode.weightDecayParam)]);
            disp([displayOpts.ies{6} 'sparsePenaltyWeight = ' num2str(cnnnet_out{curLayerCount,1}.paramsAutoEncode.sparsePenaltyWeight)]);
            disp([displayOpts.ies{6} 'activationType = ' cnnnet_out{curLayerCount,1}.paramsAutoEncode.activationType]);
        end
    end
end