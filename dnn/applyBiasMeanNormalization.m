function b = applyBiasMeanNormalization(b, outputFilterStats, inputChannelSize)
%     disp('try setting mean to zero');
    b = b - (outputFilterStats(:,2)./inputChannelSize);
end

