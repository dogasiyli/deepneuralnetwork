function [X_out, C_layerCurrent, weightVarianceVals] = applyFilterNormalization(paramsIn, C_layerCurrent)  
    funObj = paramsIn.funObj;
    X_out = paramsIn.X_out;%the batch that is convolved
    X_in = paramsIn.X_in;%for being able to apply the convolution again after changing the parameters
    wvnValue = paramsIn.wvnValue;
    applyVarNorm = paramsIn.applyVarNorm;
    applyBiasNorm = paramsIn.applyBiasNorm;
    applyOrthonormalization = paramsIn.applyOrthonormalization;

    displayOpts = paramsIn.displayOpts;
%     saveOpts = paramsIn.saveOpts;
%     figureID = paramsIn.figureID;
    
    countFilt = C_layerCurrent.countFilt;
    inputChannelSize = C_layerCurrent.inputChannelSize;
    if isfield(C_layerCurrent,'outputSize_original')
        outputSize_original = C_layerCurrent.outputSize_original;
    elseif isfield(C_layerCurrent,'outputSize')
        outputSize_original = C_layerCurrent.outputSize;
    end    
    outputFilterStats = C_layerCurrent.outputFilterStats;
    curLayerID = C_layerCurrent.layerIndice;
    
    zeroVarianceFeatsExist = true;
    tryCnt=0;
    weightVarianceVals = zeros(1,countFilt);
    while zeroVarianceFeatsExist && tryCnt<3
        tryCnt = tryCnt + 1;
        if tryCnt>1
            outputFilterStats = calculateMeanVar(X_out, outputSize_original);
            globalOutputStruct('updateFilterStatsTrack',outputFilterStats,'layerID',curLayerID,'insertMode',(tryCnt-1)*10+1);
        end
        % if countFilt<30
        %     disp('The statistics values of outputFilter initially :');
        %     disp(outputFilterStats);
        % end
%         if ~isempty(figureID)
%             h = figure(figureID);clf;hold on;
%             drawDataV2(X_in, X_out, C_layerCurrent.filtersWhitened, C_layerCurrent.biasesWhitened, outputFilterStats, 1, figureID);
%         end
        
        %% update weights for 'convolution' or 'fullyConnected' layers
        if applyOrthonormalization
            [W_ort, WtW] = orthonormalizeWeightMat( C_layerCurrent.filtersWhitened );
            C_layerCurrent.filtersWhitened = W_ort;
            if isfield(C_layerCurrent,'W_Conv_Mapped')
                C_layerCurrent = rmfield(C_layerCurrent,'W_Conv_Mapped');
            end

            [X_out, C_layerCurrent ]= feval(funObj, C_layerCurrent, X_in, displayOpts);
            outputFilterStats = calculateMeanVar(X_out, outputSize_original);
            globalOutputStruct('updateFilterStatsTrack',outputFilterStats,'layerID',curLayerID,'insertMode',(tryCnt-1)*10+2);
            % if countFilt<30
            %     disp('WtW and The statistics values of outputFilter applyOrthonormalization :');
            %     disp(WtW)
            %     disp(outputFilterStats);
            % end
            %drawDataV2([], X_out, W_ort, [], outputFilterStats, 2, figureID);            
        end
        
        %% update weights and re-run the convStep/fullyconnectedStep
        if applyVarNorm
            [W, weightVarianceVals, zeroVarianceFeatsExist] = applyWeightVarianceNormalization(C_layerCurrent.filtersWhitened, wvnValue, outputFilterStats, weightVarianceVals);
            C_layerCurrent.filtersWhitened = W;
            if isfield(C_layerCurrent,'W_Conv_Mapped')
                C_layerCurrent = rmfield(C_layerCurrent,'W_Conv_Mapped');
            end

            [X_out, C_layerCurrent ]= feval(funObj, C_layerCurrent, X_in, displayOpts);
            outputFilterStats = calculateMeanVar(X_out, outputSize_original);
            globalOutputStruct('updateFilterStatsTrack',outputFilterStats,'layerID',curLayerID,'insertMode',(tryCnt-1)*10+3);
            % if countFilt<30
            %     disp('The statistics values of outputFilter applyVarNorm :');
            %     disp(outputFilterStats);
            % end
            %drawDataV2([], X_out, W, [], outputFilterStats, 3, figureID);
        else
            zeroVarianceFeatsExist = ~isempty(find(outputFilterStats(:,4)==0, 1));
        end

        %% update bias and re-run the convStep/fullyconnectedStep
        if applyBiasNorm
            biasNormalized = applyBiasMeanNormalization(C_layerCurrent.biasesWhitened, outputFilterStats, inputChannelSize);
            C_layerCurrent.biasesWhitened = biasNormalized;
            X_out = feval(funObj, C_layerCurrent, X_in, displayOpts);

            outputFilterStats = calculateMeanVar(X_out, outputSize_original);
            globalOutputStruct('updateFilterStatsTrack',outputFilterStats,'layerID',curLayerID,'insertMode',(tryCnt-1)*10+4);
            % if countFilt<30
            %     disp('The statistics values of outputFilter applyBiasNorm :');
            %     disp(outputFilterStats);
            % end
            %drawDataV2([], X_out, [], C_layerCurrent.biasesWhitened, outputFilterStats, 4, figureID);
        end

%         if ~isempty(figureID)
%             drawnow;
%             saveFolder = [saveOpts.MainFolder saveOpts.SubFolders.WeightParams];
%             if ~exist(saveFolder,'dir')
%                 mkdir(saveFolder);
%             end
%             global outputMatrixStructCNN;
%             try
%                 saveFileName = ['batchWeightReOrg_epoc' num2str(outputMatrixStructCNN.iterationInfo.epochID_main,'%03d') '_b' num2str(outputMatrixStructCNN.iterationInfo.batchID,'%02d') '_l' num2str(curLayerID,'%02d') '_t' num2str(tryCnt,'%02d') '.png'];
%             catch
%                 saveFileName = ['batchWeightReOrg_l' num2str(curLayerID,'%02d') '_t' num2str(tryCnt,'%02d') '.png'];
%             end
%             saveas(h,[saveFolder saveFileName]);
%         end               
    end
    C_layerCurrent.outputFilterStats = outputFilterStats;
end

% function drawDataV2(X_in, X_out, W, b, outputFilterStats, i, figureID)
%     if isempty(figureID)
%         return
%     end
%     rf = 4;
%     cf = 4;
%     figure(figureID);hold on;
%     if i==1
%         subplot(rf,cf,[1 5 9]);imagesc(X_in);title('X_{in}');colorbar;set(gca,'Xtick',[]);
%         subplot(rf,cf,2);imagesc(W');title('W_{init}');colorbar;set(gca,'Xtick',[]);
%         subplot(rf,cf,6);imagesc(X_out');title('Xout_{init}');colorbar;set(gca,'Xtick',[]);ylabel('samples');xlabel('featMaps');
%         subplot(rf,cf,10);imagesc(b');title('biases_{init}');colorbar;set(gca,'Ytick',[]);set(gca,'XTick',round(linspace(1,length(b),3)));
%     elseif i>1
%         if ~isempty(W)
%             if i==2
%                 subplot(rf,cf,3);imagesc(W');title('W_{orth}');colorbar;set(gca,'Xtick',[]);
%             elseif i==3
%                 subplot(rf,cf,4);imagesc(W');title('W_{varN}');colorbar;set(gca,'Xtick',[]);
%             end
%         end
%         if ~isempty(X_out)
%             if i==2
%                 subplot(rf,cf,7);imagesc(X_out');title('X_{orth}');colorbar;set(gca,'Xtick',[]);set(gca,'Ytick',[]);
%             elseif i==3
%                 subplot(rf,cf,8);imagesc(X_out');title('X_{varN}');colorbar;set(gca,'Xtick',[]);set(gca,'Ytick',[]);
%             elseif i==4
%                 subplot(rf,cf,12);imagesc(X_out');title('X_{biasN}');colorbar;set(gca,'Xtick',[]);set(gca,'Ytick',[]);
%             end 
%         end
%         if ~isempty(b) %i==4       
%             subplot(rf,cf,11);imagesc(b');title('bias_{final}');colorbar;set(gca,'XTick',round(linspace(1,length(b),3)));
%         end
%     end
%     
%     try
%         subplot(rf,cf,12+i);
%         switch i
%             case 1
%                 titleStr = 'filtStats_{in}';
%             case 2
%                 titleStr = 'filtStats_{orth}';
%             case 3
%                 titleStr = 'filtStats_{var}';
%             case 4
%                 titleStr = 'filtStats_{bias}';
%         end
%         boxPlotManuel(outputFilterStats, struct('xlabelStr','filterIDs','titleStr',titleStr));
%     catch
%     end
%     
% end

% function drawData(X_in, X_out, W, biasesWhitened,i, figureID)
%     if isempty(figureID)
%         return
%     end
%     figure(figureID);hold on;
%     if i==1
%         subplot(3,4,[1 5 9]);imagesc(X_in);title('X-in');colorbar;
%         subplot(3,4,2);imagesc(W');title('W-init');colorbar;
%         subplot(3,4,6);imagesc(X_out');title('X-out-init');colorbar;
%         subplot(3,4,[10 11]);imagesc(biasesWhitened');title('biasesWhitened-init');colorbar;
%     elseif i>1
%         if ~isempty(W)
%             subplot(3,4,[3 4]);imagesc(W');title('W-2');colorbar;
%         end
%         if ~isempty(X_out)
%             subplot(3,4,6+i-1);imagesc(X_out');title('X-out');colorbar;
%         end
%         if ~isempty(biasesWhitened)        
%             subplot(3,4,12);imagesc(biasesWhitened');title('biasesWhitened');colorbar;        
%         end
%     end
% end
