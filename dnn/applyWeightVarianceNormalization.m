function [W, weightVarianceVals, zeroVarianceFeatsExist] = applyWeightVarianceNormalization(W, wvnValue, outputFilterStats, weightVarianceVals)
%     disp('try to set standart deviation');
    countFilt = size(outputFilterStats,1);
    zeroVarianceFeatsExist = false;
    for f = 1:countFilt
       outputFilterStats_curFilt = outputFilterStats(f,:);
       if outputFilterStats_curFilt(4)==0
            zeroVarianceFeatsExist = true;
            if weightVarianceVals(f)==0
                weightVarianceVals(f) = max(0.001,10*max(W(f,:))); 
            else
                weightVarianceVals(f) = weightVarianceVals(f)*10;
            end
            W(:,f) = weightVarianceVals(f)*rand(1,size(W,2));
       else
            maxFilt = outputFilterStats_curFilt(3);
            minFilt = outputFilterStats_curFilt(1);
            divNum_fil = wvnValue/abs(maxFilt-minFilt);
            if abs(maxFilt)<abs(minFilt)
                divNum_fil = divNum_fil*-1;
            end
            W(:,f) = W(:,f)*divNum_fil;
       end
    end
end

