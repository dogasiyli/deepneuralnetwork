function [resultTable, W_2Layer, labelCrossMat, confMatCell] = apply_XOR_Test(dataSt, numOfMaxIteration, maxClusterCount, testLDA, figIDVec)
    accMat = NaN(numOfMaxIteration,8);
    hidNodeCnt = zeros(numOfMaxIteration,1);
    W_2Layer = cell(numOfMaxIteration,1);
    confMatCell = cell(numOfMaxIteration,3);
    t=0;
    X = dataSt.dataTr;
    labelsNew = dataSt.labelsTr;
    labelCrossMat = [];
   
    goOn = true;
    while goOn
        if goOn
            t = t+1;
            disp(['*****apply_XOR_Test-Step(' num2str(t) ')']);
            if ~isempty(figIDVec)
                figIDVec(4) = t;
            end
            [X, labelsNew, labelCrossMat, accMat(t,1:6), confMatCell{t,1}, W_2Layer{t,1}, progressMade] = reOrganizeLabels(X, labelsNew, labelCrossMat(:,2:end), maxClusterCount, testLDA, figIDVec);
        end
        W1 = W_2Layer{t}{1};
        W2 = W_2Layer{t}{2};
        hidNodeCnt(t) = size(W1,2);
        if isfield(dataSt,'dataVa') && isfield(dataSt,'labelsVa')
            X_Va = dataSt.dataVa'*W1;
            X_Va(X_Va<0) = 0;
            [~, ~, ~, accMat(t,7), confMatCell{t,2}] = analyzeActivationsElm(X_Va, dataSt.labelsVa, W2, false);
        end

        if isfield(dataSt,'dataTe') && isfield(dataSt,'labelsTe')
            X_Te = dataSt.dataTe'*W1;
            X_Te(X_Te<0) = 0;
            [~, ~, ~, accMat(t,8), confMatCell{t,3}] = analyzeActivationsElm(X_Te, dataSt.labelsTe, W2, false);
        end
        goOn = t<numOfMaxIteration && progressMade;
    end
    W_2Layer = W_2Layer(1:t);
    confMatCell = confMatCell(1:t,:);
      
    accMat_Cols = 1:8;
    if ~isfield(dataSt,'dataTe') || ~isfield(dataSt,'labelsTe')
        accMat_Cols(:,8) =[];
    end
    if ~isfield(dataSt,'dataVa') || ~isfield(dataSt,'labelsVa')
        accMat_Cols(:,7) =[];
    end
    if ~testLDA
        accMat_Cols(:,4:6) =[];
    end
    resultTable = [hidNodeCnt(1:t) accMat(1:t,accMat_Cols)];    
end

