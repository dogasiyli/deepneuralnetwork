function optionalOutputs = assignOptionalObjectiveFunctionOutputs(objFuncParamsStruct, calculatedParams)
    try
        if isfield(objFuncParamsStruct,'optionalOutputs')
            optionalOutputs = objFuncParamsStruct.optionalOutputs;
            optionalOutputs = assignSingleParam(optionalOutputs, calculatedParams, 'accuracy');
            optionalOutputs = assignSingleParam(optionalOutputs, calculatedParams, 'hidLayeractivations');
            optionalOutputs = assignSingleParam(optionalOutputs, calculatedParams, 'hessianParam');
        else
            optionalOutputs = calculatedParams;
        end
    catch
        optionalOutputs = [];
    end
end

function optionalOutputs = assignSingleParam(optionalOutputs, calculatedParams, paramStr)
    if isfield(optionalOutputs,paramStr)
        if isfield(calculatedParams,paramStr)
            eval(['optionalOutputs.' paramStr ' = calculatedParams.' paramStr ';']);
        else
            warning(['Expected output (' paramStr ') not in calculatedParams']);
        end
    end
end