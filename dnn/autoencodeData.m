function [opttheta, W, b, meanData, ZCA, sizeVis] = autoencodeData(data, sizeHid, varargin)
    
% STEP-1 : Set optional param defaults
    [ paramsAutoEncode,  displayOpts, saveOpts,  paramsMinFunc,  applyZCA, applySparsity, useActSecondLayer] = getOptionalParams(varargin{:});

% STEP-2 : Sanity Check
    [data, sizeVis] = checkSanity(data, displayOpts);

% STEP-3 : ZCA-Normalization   
    [meanData, ZCA, data] = apply_ZCA_Normalization(data, sizeVis, applyZCA);

% STEP-4 : Learn autoencoder parameters
    [W, b, opttheta] = learnAutoencoderParams(data, sizeHid, applySparsity, useActSecondLayer, paramsAutoEncode, paramsMinFunc, displayOpts, saveOpts);
end

% STEP-1 : Set optional param defaults
function [ paramsAutoEncode,  displayOpts, saveOpts,  paramsMinFunc,  applyZCA, applySparsity, useActSecondLayer] = getOptionalParams(varargin)
    % Mandatory values - none
    %P1D0 -> Passed assed paramaeter = 1, Default value used = 0
    [ paramsAutoEncode,  displayOpts,  saveOpts,  paramsCategory_MinFunc,     paramsMinFunc,  applyZCA,  applySparsity,  useActSecondLayer, P1D0] = parseArgs(...
    {'paramsAutoEncode' 'displayOpts' 'saveOpts' 'paramsCategory_MinFunc'    'paramsMinFunc' 'applyZCA' 'applySparsity' 'useActSecondLayer' },...
    {        -1              -1           []        'showInfo_saveLog'            []           false          true            false         },...
    varargin{:});
    if (~isstruct(paramsAutoEncode) && paramsAutoEncode==-1)
        paramsAutoEncode = setParams_autoEncoder();
    end
    if (~isstruct(displayOpts) && displayOpts==-1)
        displayOpts = setDisplayOpts();
    end
    if isempty(paramsMinFunc)
        paramsMinFunc = setParams_minFunc(paramsCategory_MinFunc);
    end
    if (P1D0.paramsAutoEncode==1 && isfield(paramsAutoEncode,'maxIter'))
        paramsMinFunc.maxIter = paramsAutoEncode.maxIter;
    end
    if (displayOpts.defaultParamSet==1)
        %display the default parameters selected
        if (P1D0.paramsAutoEncode==0)
            disp([displayOpts.ies{4} 'Autoencoder options are not passed as function parameter. Hence default values will be used.']);
        end
        if (P1D0.displayOpts==0)
            disp([displayOpts.ies{4} 'DisplayOpts options are not passed as function parameter. Hence default values will be used.']);
        end
        if (P1D0.paramsMinFunc==0)
            disp([displayOpts.ies{4} 'MinFuncOptions options are not passed as function parameter. Hence default values will be used.']);
        end
        if (P1D0.applyZCA==0)
            disp([displayOpts.ies{4} 'Applying ZCA to data before passing it to autoencoder will be skipped by default.']);
        end
    end
end

% STEP-2 : Sanity Check
function [data, countFeat] = checkSanity(data, displayOpts)
    [countFeat, countSamples] = size(data);
    if (countSamples < countFeat)
        data = data';
        [countFeat, countSamples] = size(data);
        if (displayOpts.warningInfo==1)
            disp([ displayOpts.ies{2} 'WARNING : Cols must be observations and rows must be features. Hence data is transposed. countSamples(' num2str(countSamples) ') by (' num2str(countFeat) ')F']);
        end
    end    
end

% STEP-3 : ZCA-Normalization
function [meanData, ZCA, data] = apply_ZCA_Normalization(data, countFeat, applyZCA)
    if (applyZCA)
        [ meanData, ZCA, data ] = whitenData(data);
    else
        meanData = zeros(countFeat,1);
        ZCA = eye(countFeat);
    end
end

% STEP-4 : Learn autoencoder parameters
function [W, b, opttheta] = learnAutoencoderParams(data, sizeHid, applySparsity, useActSecondLayer, paramsAutoEncode, paramsMinFunc, displayOpts, saveOpts)
    %   data must be 2 dimensional
    %   [countFeat countSamples] = size(data)
    %   where countFeat can be [sizeImg*sizeImg*sizeChn] when we think of and image dataset
    OPS_in.sizeHid = sizeHid;
    switch paramsAutoEncode.thetaInitMode
        case 'Random'
            OPS_in.varianceMode = 'KnownVariance';
            OPS_in.varianceVal = 1;
        case 'UseData'
            OPS_in.initModeData = paramsAutoEncode.initModeData;%'PCA' is default
            switch OPS_in.initModeData
                case 'PCA'
                    OPS_in.sizeHidMax = sizeHid;
                case 'LDA'
                case 'SparseClassifier'
                case 'Classifier'
            end
    end     
    OPS_in.applySparsity = applySparsity;  
    OPS_in.useActSecondLayer = useActSecondLayer;
    if displayOpts.defaultParamSet
        disp(['applySparsity = ' num2str(double(applySparsity))])
        disp(['useActSecondLayer = ' num2str(double(useActSecondLayer))])
    end
    [W, opttheta] = trainAutoEncoder(  data ...
                                     ,'OPS_in', OPS_in ...
                                     ,'paramsMinFunc', paramsMinFunc ...
                                     ,'paramsAutoEncode', paramsAutoEncode ...
                                     ,'saveOpts', saveOpts ...
                                     ,'displayOpts', displayOpts);
    %W = reshape(opttheta(1:sizeHid * sizeVis), sizeHid,sizeVis);
    b = zeros(sizeHid,1);%opttheta(2*sizeHid*sizeVis+1:2*sizeHid*sizeVis+sizeHid);
end