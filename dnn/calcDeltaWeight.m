function  [W_grad, b_grad, weightDecayCost]  = calcDeltaWeight( s_prev, a_next, W, weightDecayParam, layerIdentifier, displayOpts ,varargin)
    switch (layerIdentifier)
        case 'convolution'
            convolutionCell= parseArgs({'convolutionCell'},{[]},varargin{:});
            [W_grad, b_grad, weightDecayCost]  = cDW_convolution(convolutionCell, s_prev, a_next, weightDecayParam, displayOpts);
        case {'fullyconnected'}
            [W_grad, b_grad, weightDecayCost]  = cDW_fullyconnected(s_prev, a_next, W, weightDecayParam, displayOpts);
        case {'softmax'}
            [groundTruth, probsAll] = parseArgs({'groundTruth', 'probsAll'},{[] []},varargin{:});
            [W_grad, b_grad, weightDecayCost]  = cDW_softmax( W, groundTruth, probsAll, a_next, weightDecayParam, displayOpts);
        case 'pool-mean'
            error('pooling shouldnt have delta weight')
        otherwise
            error(['This kind of layer (' layerIdentifier ') is not recognized.']);
    end
end

function [W_grad, b_grad, weightDecayCost]  = cDW_convolution(convolutionCell, s_prev, a_next, weightDecayParam, displayOpts) 
    [W_grad, b_grad]  = cDW_Conv_sample_by_sample(convolutionCell, s_prev, a_next, displayOpts);
    
    %Apply weight decay here
    W_grad = W_grad + weightDecayParam*convolutionCell.filtersWhitened;
    weightDecayCost = (weightDecayParam/2)*sum(convolutionCell.filtersWhitened(:).^2);    
end
function [W_grad, b_grad]  = cDW_Conv_sample_by_sample(C_CV, s_prev, a_next, displayOpts)
   countSamples = C_CV.inputSize_original(4);
   if (displayOpts.calculation==1)
        disp([displayOpts.ies{6} 'sample_by_sample - method is being used']);
        dispstat('','init');%for the following dispstat to seem better on screen
    end
    if (displayOpts.dataSize==1)
        dispstat('','init');
        dispstat(sprintf([displayOpts.ies{6} '<' mat2str(C_CV.inputSize_original(1:2)) '> by <' mat2str(C_CV.outputSize_original(1:2)) '> convolution will be done for ' num2str(countSamples) ' images']),'keepthis');
    end
    if (displayOpts.dataSize==1 || displayOpts.calculation==1)
        dispstat('','init');
        dispstat(sprintf(['\n' displayOpts.ies{6} 'Start : ']),'timestamp','keepthis');
        dispstat('','init');
    end

    s_prev = reshape(s_prev, C_CV.outputSize_original);
    s_prev = permute(s_prev,[1 2 4 3]);
    s_prev = reshape(s_prev,[],C_CV.countFilt);
    
    a_next = reshape(a_next, C_CV.inputSize_original);
    a_next = reshape(a_next,[],countSamples);
    
    [~, ~, convMap2D] = mapDataForConvolution( C_CV.filterSize, C_CV.stride, C_CV.inputSize_original(1:2), countSamples, C_CV.inputChannelSize, false);
    W_grad = zeros([C_CV.filterSize C_CV.inputChannelSize C_CV.countFilt]);
    
    imToConv = a_next(ind2sub(size(a_next),convMap2D));
    for c = 1:C_CV.inputChannelSize
        impix = ((c-1)*prod(C_CV.filterSize)) + (1:prod(C_CV.filterSize));
        for f = 1:C_CV.countFilt
            W_grad(:,:,c,f) =  W_grad(:,:,c,f) + reshape(imToConv(impix,:)*reshape(s_prev(:,f),[],1),C_CV.filterSize);
            if (displayOpts.calculation==1)
                if (f == C_CV.countFilt && c == C_CV.inputChannelSize)
                    dispstat(sprintf([displayOpts.ies{7} 'Processing c(%d/%d) of f(%d/%d)'],c, C_CV.inputChannelSize,f, C_CV.countFilt),'timestamp','keepthis'); 
                else
                    dispstat(sprintf([displayOpts.ies{7} 'Processing c(%d/%d) of f(%d/%d)'],c, C_CV.inputChannelSize,f, C_CV.countFilt),'timestamp'); 
                end
            end
        end
    end 
    W_grad  = reshape(W_grad,size(C_CV.filtersWhitened));
    b_grad = C_CV.inputChannelSize*sum(s_prev)';
    
    if (displayOpts.dataSize==1 || displayOpts.calculation==1)
        dispstat('','init');
        dispstat([displayOpts.ies{6} 'End : '],'timestamp','keepthis');
    end
    %b_grad  =reshape(b_grad,size(C_CV.biasesWhitened));
end

function [W_grad, b_grad, weightDecayCost]  = cDW_fullyconnected(s_prev, a_next, W, weightDecayParam, displayOpts)
    W_grad = s_prev*a_next';
    b_grad = sum(s_prev,2);
    %Apply weight decay here
    W_grad = W_grad + weightDecayParam*W;
    weightDecayCost = (weightDecayParam/2)*sum(W(:).^2);
end

function [W_grad, b_grad, weightDecayCost]  = cDW_softmax( W, groundTruth, probsAll, a_next, weightDecayParam, displayOpts)
    sampleCount = size(a_next,2);
    Y = groundTruth-probsAll;
    W_grad = -(Y*a_next')/sampleCount;
    b_grad = -sum(Y,2)./sampleCount;

    %Apply weight decay here
    W_grad = W_grad + weightDecayParam*W;
    weightDecayCost = (weightDecayParam/2)*sum(W(:).^2);
end
