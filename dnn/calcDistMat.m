clear all;
clc;
%'euclidean',  - this should be our guy (3-2-2-3-3) -
%'seuclidean', - in group but too slow (6-6-7-7-7)  -
%'cityblock',  - in group but slow(7-4-5-6-6)       -
%'minkowski',****in group and fast (1-7-3-2)
%'chebychev',  - fast but out of group (5-1-1-1)
%'cosine',     - slow but in group (4-5-6-5)
%'hamming'     - seems problematic
for i=1:5
    countFeat = 300*i;
    sampleCount_1 = 800*i;
    sampleCount_2 = 1000*i;
    sizeA = [countFeat,sampleCount_1];
    sizeB = [countFeat,sampleCount_2];
    % A = reshape(1:prod(sizeA),sizeA(1),sizeA(2));
    % B = reshape(prod(sizeA)+1:prod(sizeA)+prod(sizeB),sizeB(1),sizeB(2));
    A = rand(sizeA);
    B = rand(sizeB);
    distances = {'euclidean','seuclidean','cityblock','minkowski','chebychev','cosine','hamming'};
    D = length(distances);
    curToc = zeros(1,D);
    results = zeros(D,5);
    for di = randperm(D)
        disp(['Started-' distances{1,di}]);
        tic();
        if (strcmp(distances{1,di},'minkowski'))
            x = pdist2(A',B','minkowski',1)';
        else
            x = pdist2(A',B',distances{1,di})';
        end
        curToc(di) = toc();
        m = max(x(:));
        [r,c] = find(x==m,1,'first');
        v = x(r,c);
        results(di,:) = [curToc(di) r c v m];
        %disp(['time=' num2str(curToc(di)) '-vals_r(' num2str(r) ')_c(' num2str(c) ')_v(' num2str(v) ')_m(' num2str(m) ')']);    
    end
    [B, IX] = sort(curToc);
    %disptable(curToc(IX),joinStrings(distances(1,IX),'|'))
    disp(['countFeat(' num2str(countFeat) ')_sampleCount_1(' num2str(sampleCount_1) ')_sampleCount_2(' num2str(sampleCount_2) ')']);
    disptable(results(IX,:)',joinStrings(distances(1,IX),'|'),'time|r|c|v|m')
end