function [statsInput, statsOutput, meanVecs, channelFilterVals] = calcInOutStats_Conv( X_in, X_out, inputSize_original, outputSize_original)
    %calculate statistical values - min, mean, max
    %for each channel of input
    if ~isempty(X_in)
        [statsInput, meanInput, varInput, inputChannelCount, inputChannelDimensionCount, inputChannelTotalDimensionCount] = calculateMeanVar(X_in, inputSize_original);
    else
        statsInput = [];
        meanInput = [];
    end
    
    [statsOutput, meanOutput, varOutput, outputFilterCount, outputFilterDimensionCount, outputFilterTotalDimensionCount] = calculateMeanVar(X_out, outputSize_original);
    
    if nargout>=3
        meanVecs = struct;
        meanVecs.inputMean = meanInput;
        meanVecs.outputMean = meanOutput;
        
        if ~isempty(X_in)
            inputMean_rep = statsInput(:,2);
            inputMean_rep = repmat(inputMean_rep',inputChannelDimensionCount,1);
            inputMean_rep = reshape(inputMean_rep,inputChannelTotalDimensionCount,1);        
            meanVecs.inputMean_rep = inputMean_rep;
        end
        
        outputMean_rep = statsOutput(:,2);
        outputMean_rep = repmat(outputMean_rep',outputFilterDimensionCount,1);
        outputMean_rep = reshape(outputMean_rep,outputFilterTotalDimensionCount,1);  
        meanVecs.outputMean_rep = outputMean_rep;
    else
        meanVecs = [];
    end
    
    if nargout==4
        channelFilterVals = struct;
        if ~isempty(X_in)
            channelFilterVals.inputChannelCount = inputChannelCount;
            channelFilterVals.inputChannelDimensionCount = inputChannelDimensionCount;
            channelFilterVals.inputChannelTotalDimensionCount = inputChannelTotalDimensionCount;
        end
        channelFilterVals.outputFilterCount = outputFilterCount;
        channelFilterVals.outputFilterDimensionCount = outputFilterDimensionCount;
        channelFilterVals.outputFilterTotalDimensionCount = outputFilterTotalDimensionCount;
    else
        channelFilterVals = [];
    end
end
