function [ s_out, cost ] = calcSlack( s_in, layerIdentifier, varargin)

%This function is meant to calculate the slack variable at any circumstance

cost = 0;
%% STEP 0 : Define activation function
%layerIdentifier = 'softMax';
%1.output : tested MNIST 20150810
%2.fullyconnected-softMax  : tested softMax MNIST 20150810
%3.pool-mean : only tested mapping with test data
%4.convolution
    switch (layerIdentifier)
        case 'pool-mean' %tested by reading 2015-11-14
            poolCell = parseArgs({'poolCell'},{[]},varargin{:});
            ps = prod(poolCell.poolSize);
            [ s_out, learntParams ] = upsample( s_in, poolCell.poolSize, poolCell.outputSize_original);
            s_out = s_out./ps;%for a mean pooling I think division of every slack variable to ps is appropriate.we have to think about max pooling!!!
            
            %if needed check out some samples via :
            %sampleID = 1;
            %s_out_r = reshape(s_out,learntParams.upsampled_size_vectorized);
            %s_out_r_X = squeeze(s_out_r(:,:,:,sampleID));
            %s_in_r = reshape(s_in,learntParams.pooled_size_original);
            %s_in_r_X = squeeze(s_in_r(:,:,:,sampleID));
            
            %the slack variable also has the derivative of its layers' activation in an elementwise manner
            %2015-11-12 : 
            % I think there is a problem with using the
            % actDerivative here. Pooling doesnt have an activation following. Hence no multiplication with activation derivative 
            % seems more appropriate 
            % s_out = s_out.*actDerivative(a_in);
        case 'convolution' %tested by reading 2015-11-14
            %warning('not tested yet')
            convolutionCell = parseArgs({'convolutionCell'},{[]},varargin{:});
            
            %1. create the fully connected way of W by augmenting the
            %filter values in convolutionCell.filtersWhitened
            W_Conv = convolutionCell.W_ConvMap;
            W_Conv(W_Conv>0) = convolutionCell.filtersWhitened(W_Conv(W_Conv>0));
            
            %2. Now do the normal slack variable calculation as it should be done
                %the following 2 lines can be replaced by the third line alone
                %W_Conv = W_Conv';
                %s_out = (W_Conv'*s_in).*actDerivative(a_in);
            s_out = (W_Conv*s_in);%.*actDerivative(reshape(a_in,size(W_Conv,1),size(s_in,2)));
        case {'softmax'}
            %tested as output 20150810
            %warning('not tested yet')
            %a_in = softMaxedData
            [groundTruth, W, probsAll] = parseArgs({'groundTruth', 'W', 'probsAll'},{[] [] []},varargin{:});
            %for instabilities when there is a zero probability class
            probsAll(probsAll==0) = eps;
            Y = log(probsAll);%logProbsAll;
            
%             [~,labelsGiven] = max(groundTruth);
%             labelHist = hist(labelsGiven,unique(labelsGiven));
%             priorProbs = labelHist./sum(labelHist);

            % Compute the cost and gradient for softmax regression.
            sampleCount = size(groundTruth,2);
            cost = (-1/sampleCount)*sum(Y(:).*groundTruth(:));
            s_out = (-1/sampleCount)*W'*(groundTruth-probsAll);
        case {'fullyconnected'} %not tested yet
            %are we sure about the next line(s)?
            %slack{i} = (W{i+1}'*slack{i+1}).*sigmDeriva(a{i+1});
            W = parseArgs({'W'},{[]},varargin{:});
            s_out = (W'*s_in);%.*actDerivative(a_in);
        otherwise
            error(['This kind of layer (' layerIdentifier ') is not recognized.']);
    end
end
