function [ possibleOutputs ] = calculateConvolutionOutputParams( inputDataDim, convolutionSizeOptions, strideSizeOptions, maxMBAllowed, batchCount, excelFileExport)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    ri = inputDataDim(1);%row size input
    ci = inputDataDim(2);%col size input
    fi = inputDataDim(3);%feature size input
     n = inputDataDim(4);%sample count input

    
    %for each convolutionSizeOption there can be several stride parameter
    %option
    
    %when we set convolution size and stride param
    %we have to find out how many features to learn parameter
    %if the number of samples do not change we can calculate the product of
    %featureCount x patchCount that can be extracted from 1 single input 
    %but we need to know if the memory requirement is for a single batch of
    %data or batchCount of batches
    %because if e.g. there are 17 train batches then minimum patchCount can
    %be set to 1 for each sample and featureSize can be automatically found
    %by that information
    
    %also there can be a maxMBAllowed parameter and we can check for that
    %instead of looking at the input data size - and I think this can be an
    %option like : if there is no params passed calculate according to
    %input data
    if (~exist('maxMBAllowed','var') || isempty(maxMBAllowed))
        pixelCount = ri*ci*fi*n;%pixels
        maxMBAllowed = pixelCount2SizeMB(pixelCount);
    end 
    if (~exist('batchCount','var') || isempty(batchCount))
        batchCount = 1;%only a single batch exist
    end
    if ~exist('strideSizeOptions','var')
        strideSizeOptions = [];
    end
    if (~exist('convolutionSizeOptions','var') || isempty(convolutionSizeOptions))
        convolutionSizeOptions = [2 2; 3 3; 4 4; 5 5; 10 10];
    end
    convolutionSizeOptions = tidyConvSizeOpts(convolutionSizeOptions);
    
    possibleOutputs = getPossibleConvSizeStrideSizeOpts(ri, ci, convolutionSizeOptions, strideSizeOptions);
    %Col-01 : convolutionSize_Row
    %Col-02 : convolutionSize_Col
    %Col-03 : stride_Row
    %Col-04 : stride_Col
    %Col-05 : outputSize_Row
    %Col-06 : outputSize_Col
    %Col-07 : singleBatch_singleInput featureCountxpatchCount
    %   Col-08 : singleBatch_singleInput numberOfSamples planned
    %   Col-09 : singleBatch_singleInput size of mb it will require in ram
    %Col-10 : multiBatch_singleInput featureCountxpatchCount
    %   Col-11 : multiBatch_singleInput numberOfSamples planned
    %   Col-12 : multiBatch_singleInput size of mb it will require in ram
    
    for i=1:size(possibleOutputs,1)
         rc = possibleOutputs(i,1);%row size convolution
         cc = possibleOutputs(i,2);%col size convolution
        rst = possibleOutputs(i,3);%row stride size
        cst = possibleOutputs(i,4);%col stride size
        
        ro = 1+ (ri-rc)/rst;%row size output-Col 05
        co = 1+ (ci-cc)/cst;%col size output-Col 06
        if(mod(ro,1)~=0 || mod(co,1)~=0)
            warning('output row and col size must be an integer');
            possibleOutputs(i,:) = -1;
            continue;
        end
        possibleOutputs(i,5) = ro;
        possibleOutputs(i,6) = co;
        
        maxPixAllowed = sizeMB2PixelCount(maxMBAllowed);
        %now calculate the featureCount x patchCount for
        %singleBatch_singleInput
        %think as if we have only 1 single batch of this data
        %we know that we can fill maxMBAllowed of RAM
        countSamples = n;%Col-08
        %(ro*co*countSamples)*fo;%pixels
        featCountMax_fwdProp_SingleBatch = max(1,floor(maxPixAllowed/(ro*co*countSamples)));%Col-07
        %(rc*cc*countSamples*fi)*patchCount;%pixels
        patchCountMax_featLearn_SingleBatch = max(1,floor(maxPixAllowed/(rc*cc*countSamples*fi)));%Col-08
        singleBatch_singleInput_MB = pixelCount2SizeMB(featCountMax_fwdProp_SingleBatch*ro*co*countSamples);%Col-09
        possibleOutputs(i,7) = featCountMax_fwdProp_SingleBatch;
        possibleOutputs(i,8) = patchCountMax_featLearn_SingleBatch;
        possibleOutputs(i,9) = singleBatch_singleInput_MB;
        
        %now calculate the featureCount x patchCount for
        %multiBatch_singleInput
        %think as if we have multiple batches of this data
        %we know that we can fill maxMBAllowed of RAM
        countSamples = batchCount*n;%Col-12
        %(ro*co*countSamples)*fo;%pixels
        featCountMax_fwdProp_MultiBatch = max(1,floor(maxPixAllowed/(ro*co*countSamples)));%Col-10
        %(rc*cc*countSamples*fi)*patchCount;%pixels
        patchCountMax_featLearn_MultiBatch = max(1,floor(maxPixAllowed/(rc*cc*countSamples*fi)));%Col-11

        multiBatch_singleInput_MB = pixelCount2SizeMB(featCountMax_fwdProp_MultiBatch*ro*co*countSamples);%Col-13
        possibleOutputs(i,10) = featCountMax_fwdProp_MultiBatch;
        possibleOutputs(i,11) = patchCountMax_featLearn_MultiBatch;
        possibleOutputs(i,12) = sizeMB2PixelCount(maxMBAllowed)/(featCountMax_fwdProp_SingleBatch*countSamples);
        possibleOutputs(i,13) = countSamples;
        possibleOutputs(i,14) = multiBatch_singleInput_MB;
    end
    rowsToDelete = sum(possibleOutputs,2)<0;
    if (sum(rowsToDelete)>0)
        possibleOutputs(rowsToDelete,:) = [];
    end
    possibleOutputCount = size(possibleOutputs,1);
    
    colStrings = 'convR|convC|strdR|strdC|ro|co|featCnt|patchCnt|MB|featCnt|patchCnt|focPtchCnt|countSamples|MB';
    rowStrings = repmat({'sampleXX'},possibleOutputCount,1);
    for i=1:possibleOutputCount
        rowStrings{i,1} = strrep(rowStrings{i,1},'XX',num2str(i,'%02d'));
    end
    disptable(possibleOutputs, colStrings, rowStrings);
    global dnnCodePath;
    
    if exist('excelFileExport','var') && excelFileExport
        [ fileWrittenStatus, errMsgStr ] = writeToExcelFile( 'ConvolutionPlanningParams.xlsx', possibleOutputs, 'draftFileName', [dnnCodePath 'dnn' filesep 'ConvolutionPlanningDraft.xlsx'], 'xlRange', 'B7','sheetID', 1);
        if (fileWrittenStatus)
            disp('ConvolutionPlanningParams.xlsx is recorded successfully');
        else
            disp(['ConvolutionPlanningParams.xlsx couldnt be recorded errMsg : ' errMsgStr.message]);        
        end
        [ fileWrittenStatus, errMsgStr ] = writeToExcelFile( 'ConvolutionPlanningParams.xlsx', [ri ci fi n]', 'xlRange', 'B1:B4','sheetID', 1, 'columnNamesCells', []);
        if (fileWrittenStatus)
            disp('input params added into ConvolutionPlanningParams.xlsx successfully');
        else
            disp(['ConvolutionPlanningParams.xlsx couldnt be recorded errMsg : ' errMsgStr.message]);        
        end
    end
end

function sizeMB = pixelCount2SizeMB(pixelCount)
    %pixelCount = ri*ci*fi*n;%pixels
    sizeByte = pixelCount*8;%bytes
    sizeMB = sizeByte/(1024*1024);%megabytes
end

function pixelCount = sizeMB2PixelCount(sizeMB)
    %pixelCount = ri*ci*fi*n;%pixels
    %maxMBAllowed = pixelCount*8;%bytes
    %maxMBAllowed = maxMBAllowed/(1024*1024);%megabytes
    pixelCount = (sizeMB*(1024*1024))/8;
end

function possibleOutputs = getPossibleConvSizeStrideSizeOpts(ri,ci,convolutionSizeOptions, strideSizeOptions)
    possibleConvSizeCount = size(convolutionSizeOptions,1);
    if ~isempty(strideSizeOptions)
        possibleStrideSizeCount = size(strideSizeOptions,1);
    else
        possibleStrideSizeCount = possibleConvSizeCount;
    end
    
    maxPossibleOutputCount = possibleConvSizeCount*possibleStrideSizeCount;
    possibleOutputs = zeros(maxPossibleOutputCount,14);
    %Col-01 : convolutionSize_Row
    %Col-02 : convolutionSize_Col
    %Col-03 : stride_Row
    %Col-04 : stride_Col
    %Col-05->14 : other params
    i = 1;
    for c = 1:possibleConvSizeCount
        convolutionSize_Row = convolutionSizeOptions(c,1);
        convolutionSize_Col = convolutionSizeOptions(c,2);
        if ~isempty(strideSizeOptions)
            if size(strideSizeOptions,1)<size(strideSizeOptions,2)
                strideSizeOptions = strideSizeOptions';%be sure that this matrix has samples in its rows
            end
            if size(strideSizeOptions,2)==1
                strideSizeOptions = repmat(strideSizeOptions,1,2);%duplicate the numbers in column 1 to column 2
            end 
            possibleStrideRowCols = strideSizeOptions;
        elseif (convolutionSize_Row==convolutionSize_Col)
            %tested
            possibleStrideRowCols = getAllPossibleDivisors(ri - convolutionSize_Row);
            possibleStrideRowCols(possibleStrideRowCols>convolutionSize_Row) = [];
            possibleStrideRowCols = reshape(possibleStrideRowCols,[],1);%be sure that this matrix has samples in its rows
            possibleStrideRowCols = repmat(possibleStrideRowCols,1,2);
        else
            possibleStrideRows = getAllPossibleDivisors(ri - convolutionSize_Row)';
            possibleStrideRows(possibleStrideRows>convolutionSize_Row) = [];
            possibleStrideCols = getAllPossibleDivisors(ci - convolutionSize_Col)';
            possibleStrideCols(possibleStrideCols>convolutionSize_Col) = [];
            possibleRowSizesCount = size(possibleStrideRows,1);
            possibleColSizesCount = size(possibleStrideCols,1);
            if possibleRowSizesCount > possibleColSizesCount
                paramsToDelete =  randperm(possibleRowSizesCount);
                paramsToDelete = paramsToDelete(1:(possibleRowSizesCount - possibleColSizesCount));
                possibleStrideRows(paramsToDelete) = [];
            elseif possibleColSizesCount > possibleRowSizesCount               
                paramsToDelete =  randperm(possibleColSizesCount);
                paramsToDelete = paramsToDelete(1:(possibleColSizesCount - possibleRowSizesCount));
                possibleStrideCols(paramsToDelete) = [];
            end
            possibleStrideRowCols = [possibleStrideRows possibleStrideCols];
        end
        
        possibleCombinationsCount = size(possibleStrideRowCols,1);
        
        fr = i;
        to = i+possibleCombinationsCount-1;
        possibleOutputs(fr:to,[1 2]) = repmat([convolutionSize_Row convolutionSize_Col],possibleCombinationsCount,1);
        possibleOutputs(fr:to,[3 4]) = possibleStrideRowCols;
        
        i = to + 1;
    end    
    if i<maxPossibleOutputCount
        possibleOutputs(i:maxPossibleOutputCount,:) = [];
    end
end

function [convolutionSizeOptions, possibleConvSizeCount] = tidyConvSizeOpts(convolutionSizeOptions)
    if size(convolutionSizeOptions,1)<size(convolutionSizeOptions,2)
        convolutionSizeOptions = convolutionSizeOptions';%be sure that this matrix has samples in its rows
    end
    if size(convolutionSizeOptions,2)==1
        convolutionSizeOptions = repmat(convolutionSizeOptions,1,2);%duplicate the numbers in column 1 to column 2
    end
    possibleConvSizeCount = size(convolutionSizeOptions,1);
end
