function [filterStats, meanX, varX, filterCount, filterDimensionCount, filterTotalDimensionCount] = calculateMeanVar(X, size_original)
    %for each filter in output    
    [filterIndicesMap, filterCount, filterDimensionCount, filterTotalDimensionCount] = getFilterIndsMap(size_original);  
    filterStats = zeros(filterCount,4);%min-mean-max-std for columns, filters for rows
    meanX = mean(X,2);
    for i = 1:filterCount
       fr = filterIndicesMap(i,1);
       to = filterIndicesMap(i,2);
       vals = X(fr:to,:);%outputMean(fr:to)
       statsRow = [min(vals(:)) mean(vals(:)) max(vals(:)) std(vals(:))];
       filterStats(i,:) = statsRow;
    end
    varX = statsRow(:,4);
end