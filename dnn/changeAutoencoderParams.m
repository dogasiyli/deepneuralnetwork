function paramsAutoEncode = changeAutoencoderParams(paramsAutoEncode, varargin)
%changeAutoencoderParams Sets some default parameters to autoencoder
%   The values of an autoencoder can be set according to some setup. This
%   function is used to change some values in the paramsAutoEncode structure
            
% the default values are set to -1. User will pass true(1) or false(0). So
% anything with a '-1' will be set according to displayLevel
    %P1D0 -> Passed assed paramaeter = 1, Default value used = 0
    [ sparsityParam,  weightDecayParam,  sparsePenaltyWeight,  activationType,  P1D0] = parseArgs(...
    {'sparsityParam' 'weightDecayParam' 'sparsePenaltyWeight' 'activationType' },...
    {      -1             -1                     -1                  -1        },...
    varargin{:});

%%  1. sparsityParam
%     TODO : write info
    if P1D0.sparsityParam == 1
        paramsAutoEncode.sparsityParam = sparsityParam;
    end

%%  2. display
%     TODO : write info
    if P1D0.weightDecayParam == 1
        paramsAutoEncode.weightDecayParam = weightDecayParam;
    end

%%  3. logfile
%     TODO : write info
    if P1D0.sparsePenaltyWeight == 1
        paramsAutoEncode.sparsePenaltyWeight = sparsePenaltyWeight;
    end

%%  4. method
%     TODO : write info
    if P1D0.activationType == 1 
        paramsAutoEncode.activationType = activationType;
    end
end