function displayOpts = changeDisplayOpts(displayOpts, varargin)
%changeDisplayOpts The choises of what will be displayed and what will not
%   There are different information can be passes to user about what is
%   going on in the procedure. These can be turned on and off using a
%   displayInfo structure.
    
% the default values are set to -1. User will pass true(1) or false(0). So
% anything with a '-1' will be set according to displayLevel
    %P1D0 -> Passed assed paramaeter = 1, Default value used = 0
    [ layerNames,  calculation,  memOpts,  timeMode,  steps,  dataSize,   fileOperations,  figureOpts,  warningInfo,  defaultParamSet,  confusionMat,  P1D0 ] = parseArgs(...
    {'layerNames' 'calculation' 'memOpts' 'timeMode' 'steps' 'dataSize'  'fileOperations' 'figureOpts' 'warningInfo' 'defaultParamSet' 'confusionMat'},...
    {    -1             -1          -1        -1       -1       -1              -1             -1            -1             -1              -1       },...
    varargin{:});
            
%%  1. layerNames
%     Before proceeding to next step, we choose whether we want to display
%     which layer we are at in our algorithm.
%     e.g. - 'Layer Name : fullyconnected_1_1'
%'default' - 1 (show layer names)
%'minimal' - 1 (show layer names)
%'none' - 0 (dont show layer names)
%'nofigures_allstrings' - 1 (show layer names) 
%'all' - 1 (show layer names)
%'custom' - 1 (show layer names)
    if P1D0.layerNames == 1
        displayOpts.layerNames = layerNames;
    end

%%  2. calculation
%     What will be done next
%       e.g. - 'Data of will be projected.'  #when dataSize=false#
%       e.g. - 'Data of size [784 6000] will be projected with [784 200] sized weight matrix.'  #when dataSize=true#
%     What has been done previously
%       e.g. - 'Data has been projected.'  #when timeMode<3#
%       e.g. - 'Data has been projected in 0.17 cputime.'  #when timeMode>=3#
%'default' - 1 (show calculation)
%'minimal' - 0 (dont show calculation)
%'none' - 0 (dont show calculation)
%'nofigures_allstrings' - 1 (show calculation)
%'all' - 1 (show calculation)
%'custom' - 0 (dont show calculation)
    if P1D0.calculation == 1
        displayOpts.calculation = calculation;
    end

%%  3. memOpts
%     Matlabs memory usages - 'e.g. - Matlab uses(1448.03 MB)-Available(33275.87 MB)'
%'default' - 1 (show memOpts)
%'minimal' - 0 (dont show memOpts)
%'none' - 0 (dont show memOpts)
%'nofigures_allstrings' - 1 (show memOpts)
%'all' - 1 (show memOpts)
%'custom' - 0 (dont show memOpts)
    if P1D0.memOpts == 1 
        displayOpts.memOpts = memOpts;
    end

%%  4. timeMode
%     How much time passed during a calculation.
%     This is not a true false selection
%     timeMode = 
%     -1 : no time info will be displayed
%      0 : no time info will be displayed
%      1 : only iteration duration (feedforward, backward)
%      2 : each layer duration (convolution, pooling, fullyconnected, activation, softmax)
%      3 : detailed timing information within layers
%'default' - 0
%'minimal' - 1
%'none' - 0
%'nofigures_allstrings' - 2
%'all' - 3
%'custom' - 2
    if P1D0.timeMode == 1
        displayOpts.timeMode = timeMode;
    end

%%  5. steps
%     which step the algorithm is at
%     e.g. - '++++++++curStep(152)-Forward Propogation'
%'default' - 1
%'minimal' - 1
%'none' - 0
%'nofigures_allstrings' - 1
%'all' - 1
%'custom' - 1
    if P1D0.steps == 1
        displayOpts.steps = steps;
    end

%%  6. dataSize
%     e.g. - 'Data of size [784 6000] will be projected with [784 200] sized weight matrix.'
%'default' - 1
%'minimal' - 0
%'none' - 0
%'nofigures_allstrings' - 1
%'all' - 1
%'custom' - 1
    if P1D0.dataSize == 1
        displayOpts.dataSize = dataSize;
    end

%%  7. fileOperations
%     e.g. - 'Data is saved as the initial activation layer at J:\Temp\Train\Activation_01.mat in (0.14) cputime' #when timeMode>=3#
%     e.g. - 'Data is saved as the initial activation layer at J:\Temp\Train\Activation_01.mat' #when timeMode<3#
%'default' - 1
%'minimal' - 0
%'none' - 0
%'nofigures_allstrings' - 1
%'all' - 1
%'custom' - 1
    if P1D0.fileOperations == 1
        displayOpts.fileOperations = fileOperations;
    end

%%  8. figureOpts
%     I want to have options like :
%     1. display the data after layer iteration
%     2. save layer iterations for the last iteration
%     3. save layer iterations for all iterations
%?????????????????????????????????????????????????
%'default' - 1
%'minimal' - 0
%'none' - 0
%'nofigures_allstrings' - 0
%'all' - 1
%'custom' - 1
    if P1D0.figureOpts == 1
        displayOpts.figureOpts = figureOpts;
    end

%%  9. warningInfo
%     Pring warnings or not
%'default' - 0
%'minimal' - 0
%'none' - 0
%'nofigures_allstrings' - 1
%'all' - 1
%'custom' - 0
    if P1D0.warningInfo == 1
        displayOpts.warningInfo = warningInfo;
    end

%% 10. defaultParamSet
%      when an optional parameter is missing and default action is being
%      made it will be displayed or not.
%'default' - 1 (show defaultParamSet)
%'minimal' - 1 (show defaultParamSet)
%'none' - 0 (dont show defaultParamSet)
%'nofigures_allstrings' - 1 (show defaultParamSet) 
%'all' - 1 (show defaultParamSet)
%'custom' - 1 (show defaultParamSet)
    if P1D0.defaultParamSet == 1
        displayOpts.defaultParamSet = defaultParamSet;
    end
    
%% 11. confusionMat
%      this is about when the confusionMat will be displayed
%'default' - 'Every'
%'minimal' - 'Last'
%'none' - 'None'
%'nofigures_allstrings' - 'Every'
%'all' - 'Every'
%'custom' - 'Every'
    if P1D0.confusionMat == 1
        displayOpts.confusionMat = confusionMat;
    end

end