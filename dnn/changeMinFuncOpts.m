function paramsMinFunc = changeMinFuncOpts(paramsMinFunc, varargin)
%changeMinFuncOpts The choises of minFunc function
%   There are different cjoises when using minFunc as optimization
%   procedure. We can alter them after setting some default setups.
    
% the default values are set to -1. User will pass true(1) or false(0). So
% anything with a '-1' will be set according to displayLevel
    %P1D0 -> Passed assed paramaeter = 1, Default value used = 0
    [ maxIter_,  display_,  logfile_,  method_ , P1D0] = parseArgs(...
    {'maxIter'  'display'  'logfile'  'method' },...
    {    -1        -1         -1        -1     },...
    varargin{:});

%%  1. maxIter
%     Maximum number of iterations that minFucn can loop. This is different
%     than how many times it can run the cost and gradient function. Every
%     step can consist of multiple iterations of cost and gradient
%     optimization.
    if P1D0.maxIter_ == 1
        paramsMinFunc.maxIter = maxIter_;
    end

%%  2. display
%     While minFunc is running it can display how the evaluations are
%     evolving. This is the option to print out the evaluations or not.
    if P1D0.display_ == 1
        paramsMinFunc.display = display_;
    end

%%  3. logfile
%     While minFunc is running it can write how the evaluations are
%     evolving into a log file. If this is given as a fileName that can be
%     written into, minFunc function will write the evaluations into that
%     file.
    if P1D0.logfile_ == 1
        paramsMinFunc.logfile = logfile_;
    end

%%  4. method
%     default method is lbgfs. Look into minfuncs own documentation for
%     further information on this parameter.
    if P1D0.method_ == 1
        paramsMinFunc.Method = method_;
    end

end