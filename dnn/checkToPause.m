function [TF, pauseFileFullPath] = checkToPause(curMFileName, pauseFileFullPath, extraWarningInfo)
%checkToPause read the pauseFile to see if curMFileName is in it
%   Try and if found the fileName in the text file return TF=true
    global dnnCodePath;
    if ~exist('pauseFileFullPath','var') || isempty(pauseFileFullPath)
        if ~isempty(dnnCodePath)
            pauseFileFullPath = [dnnCodePath 'dnn' filesep 'pauseFileName.txt'];
        else
            pauseFileFullPath = 'pauseFileName.txt';
        end
    end
    
    TF = false;
    if ~exist(pauseFileFullPath,'file')
        return
    end

    try
        fid = fopen(pauseFileFullPath,'r');
        tline = fgets(fid);
        while ischar(tline) && TF==false
            TF = TF || strcmp(strtrim(tline),curMFileName);
            tline = fgets(fid);
        end
        fclose(fid);
    catch
        if exist('fid','var')
            try
                fclose(fid);
            catch
            end
        end
        TF = false;
    end

    if ~exist('extraWarningInfo','var') || isempty(extraWarningInfo)
        extraWarningInfo = '';
    else
        extraWarningInfo = ['<info:(' extraWarningInfo ')>'];
    end
    if TF==true
        disp(['WARNING - Paused in <' curMFileName '>' extraWarningInfo ';because it was in <' pauseFileFullPath '>']);
        eval('dbstop in checkToPause at 45');
    end
    eval('dbclear in checkToPause at 45');
end

