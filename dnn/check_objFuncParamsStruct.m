function [objFuncParamsStructCorrect, missingFields] = check_objFuncParamsStruct(objFuncParamsStruct, expectedFieldsCell)
%check_objFuncParamsStruct Summary of this function goes here
%   Detailed explanation goes here
    objFuncParamsStructCorrect = true;
    missingFields = '';
    for i = 1:length(expectedFieldsCell)
        %the expected field can be either a string or a cell
        %if it is a cell it means:
        %there are 2 cells in it and either 1 of them must exist in <objFuncParamsStruct>
        missingFields_Add = '';
        if isfield(objFuncParamsStruct,expectedFieldsCell{1,i})
            continue;
        elseif iscell(expectedFieldsCell{1,i})
            [objFuncParamsStructCorrect_InnerCell, missingFields_Add_InnerCell] = check_objFuncParamsStruct_InnerCell(objFuncParamsStruct, expectedFieldsCell{1,i});
            missingFields_Add = missingFields_Add_InnerCell;
            objFuncParamsStructCorrect = objFuncParamsStructCorrect && objFuncParamsStructCorrect_InnerCell;
        elseif ~isfield(objFuncParamsStruct,expectedFieldsCell{1,i})
            %check if the expected string is inside <objFuncParamsStruct> 
            objFuncParamsStructCorrect = false;
            missingFields_Add = expectedFieldsCell{1,i};
        end
        if ~strcmp(missingFields_Add,'')
            if strcmp(missingFields,'')
                missingFields = [missingFields_Add ', ' ];
            else
                missingFields = [missingFields missingFields_Add ', ']; %#ok<AGROW>
            end
        end
    end 
    if ~strcmp(missingFields,'')
        %remove the final ', '
        missingFields = ['[' missingFields(1:end-2) ']'];
    end
    %disp(['Following are the missing params = ' missingFields]);
end

function [objFuncParamsStructCorrect_InnerCell, missingFields_Add_InnerCell] = check_objFuncParamsStruct_InnerCell(objFuncParamsStruct, expectedFieldsCell)
    cellCount = length(expectedFieldsCell);
    atLeastOneCellExist = false;
    missingFieldsCurCell = '';
    for c = 1:cellCount
        if ~iscell(expectedFieldsCell{c})
            [atLeastOneCellExist_c, missingFields_c] = check_objFuncParamsStruct(objFuncParamsStruct, {expectedFieldsCell{c}});
        else
            [atLeastOneCellExist_c, missingFields_c] = check_objFuncParamsStruct(objFuncParamsStruct, expectedFieldsCell{c});
        end
        if ~atLeastOneCellExist_c
            if strcmp(missingFieldsCurCell,'')
                missingFieldsCurCell = ['<' missingFields_c(2:end-1) '>'];
            else
                missingFieldsCurCell = [missingFieldsCurCell '|<' missingFields_c(2:end-1) '>'];
            end
        end
        %the following(atLeastOneCellExist) will be true if at least one of the cell
        %exist completely
        atLeastOneCellExist = atLeastOneCellExist || atLeastOneCellExist_c;
    end
    if ~atLeastOneCellExist
        missingFields_Add_InnerCell = ['any (' missingFieldsCurCell ')'];
        objFuncParamsStructCorrect_InnerCell = false;
    else
        objFuncParamsStructCorrect_InnerCell = true;
        missingFields_Add_InnerCell = '';
    end
end