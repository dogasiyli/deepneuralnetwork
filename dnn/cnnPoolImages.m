function pooledFeatures = cnnPoolImages(poolDim, convolvedFeaturesOriginalSize, convolvedFeatures, poolType)
%cnnPool Pools the given convolved features
%
% Parameters:
%  poolDim - dimension of pooling region
%  convolvedFeatures - convolved features to pool (as given by cnnConvolve)
%                      convolvedFeatures(imageCol*imageRow*featureNum,imageNum)
%
% Returns:
%  pooledFeatures - matrix of pooled features in the form
%                   it was before --> pooledFeatures(featureNum, imageNum, poolRow, poolCol)
%                   now it is     --> pooledFeatures(poolRow, poolCol, featureNum, imageNum)
%     

% STEP-1 : Initialize the necessary variables
    [convolvedDim, numFilters, countSamples, sizeImg] = extractNecessaryParams(convolvedFeaturesOriginalSize, poolDim);

% STEP-2 : Apply pooling
    pooledFeatures = applyPooling(convolvedFeatures, convolvedDim, numFilters, countSamples, sizeImg, poolType, poolDim);
end

% STEP-1 : Initialize the necessary variables
function [convolvedDim, numFilters, countSamples, rcs] = extractNecessaryParams(convolvedFeaturesOriginalSize, poolDim)
    convolvedDim = convolvedFeaturesOriginalSize(2);
    numFilters = convolvedFeaturesOriginalSize(3);
    countSamples = convolvedFeaturesOriginalSize(4);
    rcs = floor(convolvedDim / poolDim);
end

% STEP-2 : Apply pooling
function pooledFeatures = applyPooling(convolvedFeatures, convolvedDim, numFilters, countSamples, sizeImg, poolType, poolDim)
    pooledFeatures = zeros(sizeImg, sizeImg, countSamples, numFilters);%this will be permuted later on
    
    %for each feature I concatenate all images
    %and convolve in that bigger image
    imF = ones(poolDim);
    convMap2D = mapDataForNextLayer( size(imF,1), size(imF,2), convolvedDim, countSamples, 1, false);
    for feat = 1:numFilters
        imI = reshape(convolvedFeatures((1+(convolvedDim^2)*(feat-1)):(convolvedDim^2)*feat,:),convolvedDim,convolvedDim*countSamples);
        switch poolType
            case 'max'
                poolResult = reshape(max(bsxfun(@times,reshape(imI(convMap2D(:)),size(convMap2D)),imF(:))),sizeImg,sizeImg,countSamples); 
            otherwise
                poolResult = reshape(sum(bsxfun(@times,reshape(imI(convMap2D(:)),size(convMap2D)),imF(:)),1),sizeImg,sizeImg,countSamples)./(numel(imF));  
        end
        pooledFeatures(:,:,:,feat) = reshape(poolResult(:),sizeImg, sizeImg, countSamples);
    end
    pooledFeatures = permute(pooledFeatures,[1 2 4 3]);
end
%     Oldschool applyPooling method
% 
%     This one lasts 0.931952 seconds for 8 images whereas
%     the new method runs in 0.291180 seconds 
%     tic();
%     poolResult = zeros(rcs,rcs);
%     for imageNum = 1:numImages
%       for featureNum = 1:numFilters
%           theCells = mat2cell(squeeze(convolvedFeatures(:,:,featureNum,imageNum)),ones(1,rcs)*poolDim,ones(1,rcs)*poolDim);
%           for r=1:rcs
%               for c=1:rcs
%                   poolResult(r,c) = mean(reshape(theCells{r,c},poolDim*poolDim,1));
%               end
%           end
%           pooledFeatures(:,:,featureNum,imageNum) = poolResult;
%       end
%     end
%     toc();