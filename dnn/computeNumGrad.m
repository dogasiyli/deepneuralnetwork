function [numgrad, problematicMatches] = computeNumGrad(J, theta, knownGrad)
% numgrad = computeNumericalGradient(J, theta)
% theta: a vector of parameters
% J: a function that outputs a real-number. Calling y = J(theta) will return the
% function value at theta. 
  
% Initialize numgrad with zeros
    numgrad = zeros(size(theta));
    problematicMatches = [];

% You should write code so that numgrad(i) is (the numerical approximation to) the 
% partial derivative of J with respect to the i-th input argument, evaluated at theta.  
% I.e., numgrad(i) should be the (approximately) the partial derivative of J with 
% respect to theta(i).
%                
% Hint: You will probably want to compute the elements of numgrad one at a time. 
    EPS = 1e-4;
    paramCount = length(theta);
    j=0;
    for i=1:paramCount
        theta_i_plus = theta;
        theta_i_minus = theta;
        theta_i_plus(i) = theta_i_plus(i) + EPS;
        theta_i_minus(i) = theta_i_minus(i) - EPS;
        numgrad(i) = (J(theta_i_plus) - J(theta_i_minus))/(2*EPS);
        %compare knownGrad and numgrad(i)
        try
            if exist('knownGrad','var')
                if abs(numgrad(i)-knownGrad(i))>EPS
                    disp([num2str(abs(numgrad(i)-knownGrad(i))) '>' num2str(eps) ' at parameter ' num2str(i) '. ' num2str(numgrad(i)) '~=' num2str(knownGrad(i)) ]);
                    problematicMatches = [problematicMatches;i numgrad(i) knownGrad(i)];
                else
                    j=j+1;
                    if mod(j,10)==0
                        disp(['ok(' num2str(j,'%03d') ')-' num2str(i) ' of ' num2str(paramCount) ' is ok..']);
                    end
                end
            end
        catch
            disp([num2str(i) ' of ' num2str(paramCount) ' is calculated..']);
        end
    end
end
