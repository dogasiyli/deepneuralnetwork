function [cost, W_grad, b_grad, time_Layer_ErrCalc] = computeSquaredErrorTerm( cnnnet, softMaxedData, labels, saveOpts, memoryMethod, activationID, displayOpts, curStep)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    weightDecayParam = 0.0001;%0;%
    layerCount = size(cnnnet,1)+1;%for two network layers this is 3
    time_Layer_ErrCalc = zeros(1,layerCount);
    cellCount = layerCount - layerTypeCount(cnnnet, 'activation', false);
    W_grad = cell(1,cellCount-1);
    b_grad = cell(1,cellCount-1);
    cost = 0;
    if (displayOpts.steps==1)
        disp(['++++++++curStep(' num2str(curStep) ')-Compute Squared Error Term++++++++'])
    end

    for layerID = layerCount-1:-1:1
        cost_cur = 0;
        if (displayOpts.layerNames==1)
            disp([displayOpts.ies{3} '++++Layer(' num2str(layerID) ' - ' num2str(layerCount) ')']);
        end
        time_Layer_ErrCalc(layerID) = cputime;

        switch cnnnet{layerID,1}.type
            case 'convolution'
                %cost_cur = only weight Decay cost
                [s_out, cost_cur, W_grad{1,activationID}, b_grad{1,activationID}, weightDecayCost{1,activationID}] = ...
                    computeError_Convolution(cnnnet, layerID, s_in, weightDecayParam, activationID, saveOpts, displayOpts, memoryMethod);
            case 'activation'
                %activation layer is connected with the previous convolution/pooling/fully connected layer
                %so whatever we will do by means of slack variable calculation
                %it will be done in the previous layer(hence next iteration due to going backwards in layers), not the activation layer
                if     (displayOpts.warningInfo==1)
                    disp([displayOpts.ies{4} 'Activation layer will be dismissed for slack and deltaWeight.']);
                end
                if strcmp(memoryMethod,'all_in_memory')
                    a = cnnnet{activationID,1}.activatedData;       
                elseif strcmp(memoryMethod,'write_to_disk')
                    a = loadActivation(saveOpts, activationID, layerID, displayOpts);
                end
                s_out = s_in.*cnnnet{layerID,1}.actDerivative(a);
                activationID = activationID-1;%5->4,3->2,2->1
            case 'pooling'
                %no cost in pooling
                s_out = computeError_Pooling(cnnnet{layerID,1}, s_in, displayOpts);
                activationID = activationID-1;%6->5
            case 'fullyconnected'
                %cost_cur = only weight Decay cost
                [s_out, cost_cur, W_grad{1,activationID}, b_grad{1,activationID}, weightDecayCost{1,activationID}] = ...
                    computeError_FullyConnected(cnnnet, layerID, s_in, saveOpts, weightDecayParam, activationID, displayOpts, memoryMethod);
            case 'softmax'
                [s_out, cost_cur, W_grad{1,activationID}, b_grad{1,activationID}, weightDecayCost{1,activationID}] = ...
                    computeError_SoftMax(cnnnet, layerID, softMaxedData, labels, weightDecayParam, activationID, saveOpts, displayOpts, memoryMethod);
            otherwise
                error('error');
        end
        cost = cost + cost_cur;
        s_in = s_out;
        time_Layer_ErrCalc(layerID) = cputime - time_Layer_ErrCalc(layerID);
    end  
end

%[s_out, cost_cur, W_grad{1,activationID}, b_grad{1,activationID}, weightDecayCost{1,activationID}] = 
%computeError_Convolution(cnnnet, layer, s_in, weightDecayParam, activationID, displayOpts, memoryMethod);
function [s_out, cost_cur, W_grad_cur, b_grad_cur, weightDecayCost_cur] = computeError_Convolution(cnnnet, layerID, s_in, weightDecayParam, activationID, saveOpts, displayOpts, memoryMethod)
    if strcmp(memoryMethod,'all_in_memory')
        a = cnnnet{activationID,1}.activatedData;
    else
        a = loadActivation(saveOpts, activationID, layerID, displayOpts);%[a4_out<--a5_in],[a2_out<--a3_in],[a1_out<--a2_in]
    end
    
    if (displayOpts.calculation==1)
        t = cputime;
        disp([displayOpts.ies{4} 'Calculating the slack variable for ' cnnnet{layerID,1}.name]);
    end
        s_out = calcSlack(s_in, 'convolution', 'convolutionCell', cnnnet{layerID,1});%[s3_out<--s4_in],[s1_out<--s2_in],[s0_out<--s1_in]
    if     (displayOpts.calculation==1 && displayOpts.timeMode>=1)
        t = cputime - t;
        disp([displayOpts.ies{4} 'The slack variable for ' cnnnet{layerID,1}.name ' calculated in ' num2str(t,'%4.2f') ' cputime.']);
    elseif (displayOpts.calculation==1 && displayOpts.timeMode <1)
        disp([displayOpts.ies{4} 'The slack variable for ' cnnnet{layerID,1}.name ' is calculated.']);
    end

    if (displayOpts.calculation==1)
        t = cputime;
        disp([displayOpts.ies{4} 'Calculating deltaWeight for ' cnnnet{layerID,1}.name]);
    end
    [W_grad_cur, b_grad_cur, weightDecayCost_cur]  = calcDeltaWeight( s_in, a, [], weightDecayParam, 'convolution', displayOpts,'convolutionCell',cnnnet{layerID,1});%[W_grad{1,4}],[W_grad{1,2}],[W_grad{1,1}]
    if     (displayOpts.calculation==1 && displayOpts.timeMode>=1)
        t = cputime - t;
        disp([displayOpts.ies{4} 'deltaWeight for ' cnnnet{layerID,1}.name ' calculated in ' num2str(t,'%4.2f') ' cputime.']);
    elseif (displayOpts.calculation==1 && displayOpts.timeMode <1)
        disp([displayOpts.ies{4} 'deltaWeight for ' cnnnet{layerID,1}.name ' is calculated.']);
    end
    cost_cur = weightDecayCost_cur;
    if strcmp(memoryMethod,'write_to_disk')
        clear a;
    end
end

%[s_out, cost_cur] = computeError_Pooling(cnnnet{layer,1}, s_in, displayOpts, memoryMethod);
function [s_out, cost_cur] = computeError_Pooling(C_PO, s_in, displayOpts)
    %this slack var will be just a temporary which will only pass through to reach the convolution layer
    %so we have to simulate the backpropogation of the slack variables here
    %s_in = (W{i+1}'*s_in).*sigmDeriva(a_in);
    if (displayOpts.calculation==1)
        t = cputime;
        disp([displayOpts.ies{4} 'Calculating the slack variable for ' C_PO.name]);
    end
    switch C_PO.poolType
        case 'mean'
            [ s_out, cost_cur ] = calcSlack(s_in, ['pool-' C_PO.poolType], 'poolCell', C_PO);%[s5_out<--s6_in]
        case 'max'
            error('not implemented yet');
            %need to load the calculated weight matrix
            %for max pooling
            %[tempFolder 'W_ii.mat']
    end
    if     (displayOpts.calculation==1 && displayOpts.timeMode>=1)
        t = cputime - t;
        disp([displayOpts.ies{4} 'The slack variable for ' C_PO.name ' calculated in ' num2str(t,'%4.2f') ' cputime.']);
    elseif (displayOpts.calculation==1 && displayOpts.timeMode <1)
        disp([displayOpts.ies{4} 'The slack variable for ' C_PO.name ' is calculated.']);
    end
end

%[s_out, cost_cur, W_grad{1,activationID}, b_grad{1,activationID}, weightDecayCost{1,activationID}] = 
%computeError_SoftMax(cnnnet{layer,1}, s_in, tempFolder_Train, weightDecayParam, activationID, displayOpts, memoryMethod)
function [s_out, cost_cur, W_grad_cur, b_grad_cur, weightDecayCost_cur] = computeError_SoftMax(cnnnet, layerID, softMaxedData, labels, weightDecayParam, activationID, saveOpts, displayOpts, memoryMethod)
    C_SM = cnnnet{layerID,1};
    %activation to use : a{layer}
    %act : sigm
    %actDeriva : sigmDeriva
    %groundTruth = full(sparse(labels, 1:countSamples, 1));
    %slack{layer} = (a{layer}-groundTruth).*sigmDeriva(a{layer});%Layer_n
    if strcmp(memoryMethod,'all_in_memory')
        a = cnnnet{activationID,1}.activatedData;
    else
        a = loadActivation(saveOpts, activationID, layerID, displayOpts);
    end
    
    W = C_SM.Weight;%reshape(C_SM.Weight(1:C_SM.countCategory*C_SM.countFeat), C_SM.countCategory, C_SM.countFeat);
    %biasParams = C_SM.BiasParams;
    if (displayOpts.calculation==1)
        t = cputime;
        disp([displayOpts.ies{4} 'Calculating the slack variable for ' C_SM.name]);
    end

    probsAll = bsxfun(@minus,softMaxedData,max(softMaxedData,[],1));
    probsAll = exp(probsAll);
    
    countSamples = length(labels);
    priorProbMat = ones(size(probsAll));
    try %#ok<TRYNC>
        if isfield(C_SM,'priorInfoUse') && ~strcmpi(C_SM.priorInfoUse,'none')
            labelHist = hist(labels,1:C_SM.countCategory);
            if strcmpi(C_SM.priorInfoUse,'sum')
                priorProbs = labelHist./sum(labelHist);%sum==1
            elseif strcmpi(C_SM.priorInfoUse,'max')
                priorProbs = labelHist./max(labelHist);%max==1 = makes more sense, equal to 'none' when sample counts almost equal
            end
            priorProbs = reshape(priorProbs, [], 1);%probs will be per row->class
            priorProbMat = repmat(priorProbs,1,countSamples);
        end
    end
    probsAll = probsAll.*priorProbMat;
    probsAll = bsxfun(@rdivide,probsAll,sum(probsAll));

    %if C_SM.countCategory==length(unique(labels))
    %    groundTruth = full(sparse([1 3 5], 1:countSamples, 1));
    groundTruth = zeros(C_SM.countCategory,countSamples);
    groundTruth(sub2ind([C_SM.countCategory,countSamples],reshape(labels,1,[]),1:countSamples)) = 1;
    
    [s_out, cost_cur] = calcSlack([], 'softmax','W', W,'groundTruth', groundTruth, 'probsAll', probsAll);%[s5_out<--s6_in]

    if     (displayOpts.calculation==1 && displayOpts.timeMode>=1)
        t = cputime - t;
        disp([displayOpts.ies{4} 'The slack variable for ' C_SM.name ' calculated in ' num2str(t,'%4.2f') ' cputime.']);
    elseif (displayOpts.calculation==1 && displayOpts.timeMode <1)
        disp([displayOpts.ies{4} 'The slack variable for ' C_SM.name ' is calculated.']);
    end                        

    %here we have a6 and s6 to calculate deltaW in softmax layer
    if (displayOpts.calculation==1)
        t = cputime;
        disp([displayOpts.ies{4} 'Calculating deltaWeight for ' C_SM.name]);
    end
    [W_grad_cur, b_grad_cur, weightDecayCost_cur]  = calcDeltaWeight( [], a, W, weightDecayParam, 'softmax', displayOpts, 'groundTruth', groundTruth, 'probsAll', probsAll);
    if     (displayOpts.calculation==1 && displayOpts.timeMode>=1)
        t = cputime - t;
        disp([displayOpts.ies{4} 'deltaWeight for ' C_SM.name ' calculated in ' num2str(t,'%4.2f') ' cputime.']);
    elseif (displayOpts.calculation==1 && displayOpts.timeMode <1)
        disp([displayOpts.ies{4} 'deltaWeight for ' C_SM.name ' is calculated.']);
    end
    cost_cur = cost_cur + weightDecayCost_cur;
    if strcmp(memoryMethod,'write_to_disk')
        clear a;
    end
end

%[s_out, cost_cur, W_grad{1,activationID}, b_grad{1,activationID}, weightDecayCost{1,activationID}] = 
%computeError_FullyConnected(cnnnet, layer, s_in, tempFolder_Train, weightDecayParam, activationID, displayOpts, memoryMethod)
function [s_out, cost_cur, W_grad_cur, b_grad_cur, weightDecayCost_cur] = ...
    computeError_FullyConnected(cnnnet, layerID, s_in, saveOpts, weightDecayParam, activationID, displayOpts, memoryMethod)
    if strcmp(memoryMethod,'all_in_memory')
        a = cnnnet{activationID,1}.activatedData;
    else
        a = loadActivation(saveOpts, activationID, layerID, displayOpts);%[a6_out]
    end
    
    W = cnnnet{layerID,1}.filtersWhitened;
    if (displayOpts.calculation==1)
        t = cputime;
        disp([displayOpts.ies{4} 'Calculating the slack variable for ' cnnnet{layerID,1}.name]);
    end
    s_out = calcSlack(s_in, 'fullyconnected','W',W);%[s5_out<--s6_in]
    if     (displayOpts.calculation==1 && displayOpts.timeMode>=1)
        t = cputime - t;
        disp([displayOpts.ies{4} 'The slack variable for ' cnnnet{layerID,1}.name ' calculated in ' num2str(t,'%4.2f') ' cputime.']);
    elseif (displayOpts.calculation==1 && displayOpts.timeMode <1)
        disp([displayOpts.ies{4} 'The slack variable for ' cnnnet{layerID,1}.name ' is calculated.']);
    end

    %here we have a6 and s6 to calculate deltaW in softmax layer
    if (displayOpts.calculation==1)
        t = cputime;
        disp([displayOpts.ies{4} 'Calculating deltaWeight for ' cnnnet{layerID,1}.name]);
    end
    [W_grad_cur, b_grad_cur, weightDecayCost_cur]  = calcDeltaWeight( s_in, a, W, weightDecayParam, 'fullyconnected', displayOpts);
    if     (displayOpts.calculation==1 && displayOpts.timeMode>=1)
        t = cputime - t;
        disp([displayOpts.ies{4} 'deltaWeight for ' cnnnet{layerID,1}.name ' calculated in ' num2str(t,'%4.2f') ' cputime.']);
    elseif (displayOpts.calculation==1 && displayOpts.timeMode <1)
        disp([displayOpts.ies{4} 'deltaWeight for ' cnnnet{layerID,1}.name ' is calculated.']);
    end

    cost_cur = weightDecayCost_cur;
    if strcmp(memoryMethod,'write_to_disk')
        clear a;
    end
end