function [ output_args ] = connect2cnnnets( cnnnet_initial, cnnnet2append, displayOpts)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
    if strcmp(cnnnet_initial(end,1).type,'softmax')
        cnnnet_new = cnnnet_initial(1:end-1);
    else
        cnnnet_new = cnnnet_initial;
    end
    layerCnt2Append = length(cnnnet2append);
    
    
    %combine them - bridge layer
    prevLayer = cnnnet_new(end,1);
    nextLayer = cnnnet2append(1,1);
    if strcmp(prevLayer.type,'activation') || strcmp(prevLayer.type,'pooling') 
        if strcmp(nextLayer.type,'softmax')
            cnnnet_new = appendNetworkLayer( cnnnet_new, 'softmax', displayOpts, 'initializeWeightMethod', 'Random', 'minFuncMaxIter', 0 ,'useAccuracyAsCost', prevLayer.useAccuracyAsCost);
            cnnnet_new(end,1).fwdPropAllBatches = false;
            cnnnet_new(end,1).inputSize_original = prevLayer.inputSize_original;
            cnnnet_new(end,1).fwdPropAllBatches = false;
            cnnnet_new(end,1).fwdPropAllBatches = false;
            cnnnet_new(end,1).fwdPropAllBatches = false;
            cnnnet_new(end,1).fwdPropAllBatches = false;
        elseif strcmp(nextLayer.type,'convolution')

        elseif strcmp(nextLayer.type,'fullyconnected')
            
        else
            error(['can not connect (' prevLayer.type ')-(' nextLayer.type ')']);
        end
    end
    
    
    for l=2:layerCnt2Append
        prevLayer = cnnnet2append(l-1,1);
        nextLayer = cnnnet2append(l,1);
        switch nextLayer.type
            case 'convolution'
            case 'fullyconnected'
            case 'softmax'
        end
    end
end

