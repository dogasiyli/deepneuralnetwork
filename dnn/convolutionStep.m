function [C_CV, convolvedVectorizedData, convolvedOriginalShapedData] = convolutionStep( C_CV, data, displayOpts, saveOpts, dataStruct, deletePrevLayerBatch)
%C_CV = Cell_ConVolution 
    %set the following some number if you want to test the convolution using for
    %loop on that many images
    testConvolutionImageCount = 0;

% STEP-1 : Check the input channel size and input size original
    C_CV = checkIOSizes(C_CV);

% STEP-2 : If the sizes aren't learnt/identified yet learn/identify them from data
    C_CV = learnIOSizes(C_CV, data);
    
% STEP-3 : If the filters aren't learnt yet learn them from data
    C_CV = learnFilters(C_CV, displayOpts, saveOpts, data, dataStruct);
    
% STEP-4 : Learn the convolution map for fast convolution of data
    C_CV = learnMappings(C_CV, displayOpts);

% STEP-5 : Apply convolution on data
    if nargout>1  
        %if number of output arguments is more than 1 construct/calculate convolvedVectorizedData
        [convolvedVectorizedData, C_CV] = convolveData(C_CV, data, displayOpts);    
    end

% STEP-6 : Mean and Variance Operations
    [C_CV, convolvedVectorizedData] = applyMeanVarOperations(C_CV, data, convolvedVectorizedData,displayOpts,saveOpts);
    
% STEP-7 : Convolve other batches for preparing them to following layers
% autoencoders and ELM filter learnings
% check 1. filtersAlreadyLearnt must be false
% check 2. if dataStruct exist do for all batches
    if C_CV.fwdPropAllBatches
        outputFilterStatsAllBatches = saveAllBatchesToActivatedDataFolder(C_CV, dataStruct, displayOpts, saveOpts, @convolveData, convolvedVectorizedData, deletePrevLayerBatch);
        C_CV.outputFilterStatsAllBatchesInitial = outputFilterStatsAllBatches;
        C_CV.fwdPropAllBatches = false;
    end
    
% STEP-8 : Create shaped data if wanted as an output
    %if number of output arguments is more than 2 construct/calculate convolvedOriginalShapedData
    if nargout>2 || testConvolutionImageCount>0
        convolvedOriginalShapedData = reshapeForOriginalSize(C_CV, convolvedVectorizedData);
    end

% STEP-9 : Test convolution with the for-loop version
    %below code also shows how convolution would be done with for loops
    if (testConvolutionImageCount>0 && exist('convolvedOriginalShapedData','var'))
        testConvolution(data, convolvedOriginalShapedData, C_CV,testConvolutionImageCount, displayOpts);
    end    
end

% STEP-0 : Check the input channel size and input size original
function C_CV = checkIOSizes(C_CV)
    if isfield(C_CV,'inputSize_original') && ~isfield(C_CV,'inputChannelSize')
        %if the inputChannelSize is not defined && inputSize_original is defined
        %this means that this convolution layer comes after a pooling layer
        %hence the original size has the input channel size in its 3rd parameter
        C_CV.inputChannelSize = C_CV.inputSize_original(3);%This came from the previous steps output dimension
    end
    if isfield(C_CV,'inputSize_original') && ~isfield(C_CV,'inputSize_vectorized')
        %if the inputSize_vectorized is not defined && inputSize_original is defined
        C_CV.inputSize_vectorized = [prod(C_CV.inputSize_original(1:end-1)) C_CV.inputSize_original(4)];
    end
    if isfield(C_CV,'outputSize_original') && ~isfield(C_CV,'outputSize_vectorized')
        %if the output is not defined && outputSize_original is defined
        C_CV.outputSize_vectorized = [prod(C_CV.outputSize_original(1:end-1)) C_CV.outputSize_original(4)];
    end
end

% STEP-1 : If the sizes aren't learnt/identified yet learn/identify them from data
function C_CV = learnIOSizes(C_CV, data)
% just checking if any size parameter exist will do the trick
% TODO - we should check if data is a struct due to many batches of data as files
%          following cell variables are created/set :
%          inputChannelSize, inputSize_original, inputSize_vectorized,
%          outputSize_original, outputSize_vectorized
    if (~isfield(C_CV,'outputSize_original') || size(data,2)~=C_CV.outputSize_original(4))
        [sizeImg, countSamples, sizeChn] = exploreDataSize(data, C_CV.inputChannelSize);
        Ri = sizeImg; Ci = sizeImg;
        Rf = C_CV.filterSize(1);
        Cf = C_CV.filterSize(2);
        C_CV.inputChannelSize = sizeChn;
        C_CV.inputSize_original = [sizeImg,sizeImg,sizeChn,countSamples];
        C_CV.inputSize_vectorized = [sizeImg*sizeImg*sizeChn,countSamples];

        Ro = 1+floor((Ri-Rf) / C_CV.stride(1));
        Co = 1+floor((Ci-Cf) / C_CV.stride(2));
        C_CV.outputSize_original = [Ro,Co,C_CV.countFilt,countSamples];
        C_CV.outputSize_vectorized = [Ro*Co*C_CV.countFilt,countSamples];
    end
end

% STEP-2 : If the filters aren't learnt yet learn them from data
function [C_CV, filtersAlreadyLearnt]= learnFilters(C_CV, displayOpts, saveOpts, data, dataStruct)
%  weight parameters are either learnt by autoencoder or assigned
%  randomly
    filtersAlreadyLearnt = isfield(C_CV,'filtersWhitened');
    if ~filtersAlreadyLearnt
        if (strcmp(C_CV.initializeWeightMethod,'Autoencoder'))
            C_CV = assignMissingAutoencoderParams(C_CV);
            if     (displayOpts.calculation==1 && displayOpts.dataSize==1)
                disp([displayOpts.ies{6} mat2str(C_CV.filterSize) ' sized ' num2str(C_CV.countFilt) ' filters will be learnt using autoencoder.']);
            elseif (displayOpts.calculation==1 && displayOpts.dataSize==0)
                disp([displayOpts.ies{6} 'Filters will be learnt using autoencoder.']);
            end
            C_CV = learnFiltersFromPatches(C_CV, displayOpts, saveOpts, data, dataStruct,'Autoencoder');
            C_CV.filtersWhitened = (C_CV.filterParams.ZCA*C_CV.filterParams.W); % The convolution need to be done by the whitened features
            C_CV.biasesWhitened = C_CV.filterParams.b - C_CV.filtersWhitened'*C_CV.filterParams.meanPatches; %This should be added for all convolved feature
        elseif (strcmp(C_CV.initializeWeightMethod,'SparseClassifier'))
            C_CV = assignMissingSparseClassifier(C_CV);
            if     (displayOpts.calculation==1 && displayOpts.dataSize==1)
                disp([displayOpts.ies{6} mat2str(C_CV.filterSize) ' sized ' num2str(C_CV.countFilt) ' filters will be learnt using SparseClassifier.']);
            elseif (displayOpts.calculation==1 && displayOpts.dataSize==0)
                disp([displayOpts.ies{6} 'Filters will be learnt using sparse classifier.']);
            end
            C_CV = learnFiltersFromPatches(C_CV, displayOpts, saveOpts, data, dataStruct,'SparseClassifier');
            C_CV.filtersWhitened = (C_CV.filterParams.ZCA*C_CV.filterParams.W); % The convolution need to be done by the whitened features
            C_CV.biasesWhitened = C_CV.filterParams.b - C_CV.filtersWhitened'*C_CV.filterParams.meanPatches; %This should be added for all convolved feature
        elseif strcmp(C_CV.initializeWeightMethod,'Random')
            %W = reshape(opttheta(1:countFilt * sizeVis), countFilt,sizeVis);
            W_size = [C_CV.inputChannelSize*prod(C_CV.filterSize), C_CV.countFilt];
            W = fill_W( C_CV.randInitMode, C_CV.inputSize_vectorized(1), C_CV.outputSize_vectorized(1), W_size);
            C_CV.filtersWhitened = W;
            C_CV.biasesWhitened = zeros(C_CV.countFilt,1);
        elseif (strcmp(C_CV.initializeWeightMethod,'ELM'))
            C_CV = assignMissingParams_ELM(C_CV);
            
            if     (displayOpts.calculation==1 && displayOpts.dataSize==1)
                disp([displayOpts.ies{6}  mat2str(C_CV.filterSize) ' sized ' num2str(C_CV.countFilt) ' filters will be learnt using ELM-autoencoder.']);
            elseif (displayOpts.calculation==1 && displayOpts.dataSize==0)
                disp([displayOpts.ies{6}  'Filters will be learnt using ELM-autoencoder.']);
            end
            C_CV = learnFiltersFromPatches(C_CV, displayOpts, saveOpts, data, dataStruct,'ELM');
            C_CV.filtersWhitened = (C_CV.filterParams.W*C_CV.filterParams.ZCA); % The convolution need to be done by the whitened features
            C_CV.biasesWhitened = C_CV.filterParams.b - C_CV.filtersWhitened*C_CV.filterParams.meanPatches; %This should be added for all convolved feature
        else
            if     (displayOpts.calculation==1 && displayOpts.dataSize==1)
                disp([displayOpts.ies{6} mat2str(C_CV.filterSize) ' sized ' num2str(C_CV.countFilt) ' filters will be learnt by ' C_CV.initializeWeightMethod]);
            elseif (displayOpts.calculation==1 && displayOpts.dataSize==0)
                disp([displayOpts.ies{6} 'Filters will be learnt by' C_CV.initializeWeightMethod]);
            end
            C_CV = learnFiltersFromPatches(C_CV, displayOpts, saveOpts, data, dataStruct,C_CV.initializeWeightMethod);
            C_CV.filtersWhitened = (C_CV.filterParams.ZCA*C_CV.filterParams.W); % The convolution need to be done by the whitened features
            C_CV.biasesWhitened = C_CV.filterParams.b - C_CV.filtersWhitened'*C_CV.filterParams.meanPatches; %This should be added for all convolved feature            
        end
        
        %now that the filters are learned
        %check if orthogonolizeWeightsInitially is true
        %I would like to cross check this orthogonalization with variance
        %normalization and see the effect, hence this orthonormalization
        %would go into applyMeanVarOperations
        
    end
end
function C_CV = learnFiltersFromPatches(C_CV, displayOpts, saveOpts, X, dataStruct, algorithmName)
    %calculate the number of patches that can be extracted for
    %feature learning
    %there can be multiple batches, hence the samples must be
    %taken from all batches of data
    patchSize = C_CV.filterSize;
    countFilt = C_CV.countFilt;
    inputChannelSize = C_CV.inputChannelSize;
    inputSize_original = C_CV.inputSize_original;
    strideParam = [];
    try 
        countBatch = dataStruct.countBatch.train;
    catch
        countBatch = 1;
    end
    
    dMIS = getDataMemoryInformationStruct(X, inputSize_original, patchSize, inputChannelSize, countBatch, displayOpts.memOpts, 8);
    dMIS.count_patchPerSample_planned = ceil(dMIS.count_patchPerSample_planned/dMIS.countBatch);
    patchCountPerSample = dMIS.count_patchPerSample_planned;
    if dMIS.count_patchPerSample_strideOne>dMIS.count_patchPerSample_planned
        %we have such a stride parameter that allows
        %"patchCountPerSampleToBeUsed" to be extracted from
        %all train data - this part of the algorithm can be enhanced
        %but leave it as random patch extraction for now
        samplingMethod = 1;
        patchCountPerSample = dMIS.count_patchPerSample_planned;
    else
        samplingMethod = 3;
        strideParam = 1;%we can use stride param=1 and get all possible patches from train data
    end
    %either {patchCountPerSample, strideParam, samplingMethod} needs to be passed in
    %if none is passed in samplingMethod=1
    %samplingMethod=1 : patchCountPerSample=1 is a good default
    %samplingMethod=2 : strideParam = convFiltDim(1) is also another good default
    %samplingMethod=3 : strideParam = 1 is the best if it is not fucking up the memory    
    C_CV.filterParams = struct;
    switch algorithmName
        case 'Autoencoder'
            paramStr = 'paramsAutoEncode';
            paramVal = C_CV.paramsAutoEncode;
        case 'SparseClassifier'
            paramStr = 'paramsSparseClassifier';
            paramVal = C_CV.paramsSparseClassifier;            
        case 'ELM'
            paramStr = 'paramsELM';
            paramVal = C_CV.paramsELM;
        otherwise
            paramStr = 'paramStr';%->'PCA'<default>,XOR_ELM,LDA,PCA_PerClass
            paramVal = algorithmName;
    end
    
    [~, meanPatch, ZCA_p, W, b, learnPatchResultStruct] = learnPatches(...
    patchSize ,countFilt, X, inputChannelSize...
    ,paramStr, paramVal...
    ,'dataStruct', dataStruct...
    ,'patchCountPerSample', patchCountPerSample...
    ,'dMIS', dMIS...
    ,'strideParam', strideParam...
    ,'samplingMethod', samplingMethod...
    ,'paramsCategory_MinFunc','showInfo_saveLog'...
    ,'displayOpts', displayOpts...
    ,'saveOpts', saveOpts...
    ,'curLayerID', C_CV.layerIndice);

    C_CV.filterParams.meanPatches = meanPatch;
    C_CV.filterParams.ZCA = ZCA_p;
    C_CV.filterParams.W = W';
    C_CV.filterParams.b = b;
    C_CV.learnPatchResultStruct = learnPatchResultStruct;
end
% STEP-3 : Learn the convolution map for fast convolution of data
function [C_CV] = learnMappings(C_CV, displayOpts)
    if ~isfield(C_CV,'W_ConvMap')
        if (displayOpts.calculation==1)
            disp([displayOpts.ies{6} 'Filter maps of layer <' C_CV.name '> are being constructed']);
        end
        time_filterCalculation = cputime;
            dIn = C_CV.inputSize_vectorized(1);%Ri*Ci*Di
            dOut = C_CV.outputSize_vectorized(1);%Ro*Co*filCnt
            %next line will be changed for non-square data
            [W_ConvMap, map_singleInput] = construct_W_ConvMap(C_CV, dIn, dOut, displayOpts);
%%Note 4 :
            C_CV.W_ConvMap = sparse(W_ConvMap);   
% colCount = max(W_ConvMap(:));
% rowCount = prod(C_CV.outputSize_original(1:2));
% ps = C_CV.inputChannelSize*prod(C_CV.filterSize);
%%Note-3 :
        time_filterCalculation = cputime - time_filterCalculation;
        if     (displayOpts.calculation==1 && displayOpts.timeMode>=3)
            disp([displayOpts.ies{6} 'Filter maps of layer <' C_CV.name '> have been constructed in ' num2str(time_filterCalculation,'%4.2f') ' cputime.']);
        elseif (displayOpts.calculation==1 && displayOpts.timeMode <3)
            disp([displayOpts.ies{6} 'Filter maps of layer <' C_CV.name '> have been constructed.']);
        end
        clear fil curFilMap csff c fp foundIndices W_ConvMap Conv2FiltW_Grad_Map map_singleInput filMultipleMap 
    end
end
function [W_ConvMap, map_singleInput,filMultipleMap] = construct_W_ConvMap(C_CV, dIn, dOut, displayOpts)
    map_singleInput = mapDataForConvolution( C_CV.filterSize(1)         ... patchSize, 
                                            ,C_CV.stride(1)             ... stride, 
                                            ,C_CV.inputSize_original(1) ... sizeImg, 
                                            ,C_CV.inputSize_original(4) ... countSamples,
                                            ,C_CV.inputChannelSize      ... inputDataChannelSize, 
                                            ,strcmp(displayOpts.displayLevel,'all'));  %tutorialMode

    filMultipleMap = reshape( 1:numel(C_CV.filtersWhitened)  ...
                             ,C_CV.filterSize(1)             ...Rf,
                             ,C_CV.filterSize(2)             ...Cf,
                             ,C_CV.inputChannelSize          ...Di,
                             ,C_CV.countFilt);              %  filCnt

    W_ConvMap = zeros(dIn,dOut);
    %how to calculate W_Conv by using the filters??
    %this will only be calculated once as a mapping parameter
    csff = size(map_singleInput,2);%colSizeForEachFilter
    for fil = 1:C_CV.countFilt
        curFilMap = reshape(filMultipleMap(:,:,:,fil),[],1);
        for c = 1:csff
            W_ConvMap(map_singleInput(:,c),c + csff*(fil-1)) = curFilMap;
        end
    end
end

% STEP-4 : Apply convolution on data
function [convolvedVectorizedData, C_CV] = convolveData(C_CV, data, displayOpts)
%%Note-1:
    [C_CV, W_Conv] = mapConvWeights(C_CV, displayOpts);
%%Note-2
    countSamples = size(data,numel(size(data)));
    [batchCount, batchIDs] = decideToConvolveInBatches(data, W_Conv, displayOpts);
    time_conv = cputime;
        if batchCount>1
            convolvedVectorizedData = rand(size(W_Conv,2),countSamples,'double');
            for batchData = 1:batchCount
               fr = batchIDs(batchData,1);
               to = batchIDs(batchData,2);
               convolvedVectorizedBatch = W_Conv'*reshape(data(:,fr:to),[],batchIDs(batchData,3));
               convolvedVectorizedData(:,fr:to) = convolvedVectorizedBatch;
            end
            clear fr to convolvedVectorizedBatch
        else
            convolvedVectorizedData = W_Conv'*reshape(data,[],countSamples);
        end
    time_conv = cputime - time_conv;
    if (displayOpts.calculation)
        if (displayOpts.timeMode>=3)
            disp([displayOpts.ies{6} 'Convolution is completed in ' num2str(time_conv) ' cputime.']);
        else
            disp([displayOpts.ies{6} 'Convolution is completed.']);
        end
    end
    clear time_conv;
    
%    [inputChannelStats, outputFilterStats, meanVecs, channelFilterVals] = calcInOutStats_Conv( data, convolvedVectorizedData, C_CV.inputSize_original, C_CV.outputSize_original);
%%Note-5
    Ro = C_CV.outputSize_original(1);
    Co = C_CV.outputSize_original(2);
    bAdd = C_CV.biasesWhitened';
    bAdd = C_CV.inputChannelSize*reshape(repmat(bAdd,Ro*Co,1),[],1);
    convolvedVectorizedData = bsxfun(@plus,convolvedVectorizedData,bAdd);
    
    if isfield(C_CV,'W_Conv_Mapped')
        sizeOfWConv = getVariableSize(W_Conv,'mb');
        if sizeOfWConv>1024
            C_CV = rmfield(C_CV,'W_Conv_Mapped');
        end
    end
end
function [C_CV, W_Conv] = mapConvWeights(C_CV, displayOpts)
    if isfield(C_CV,'W_Conv_Mapped')
        W_Conv = C_CV.W_Conv_Mapped;
        if (displayOpts.calculation)
            disp([displayOpts.ies{6} 'W_Conv_Mapped of size ' mat2str(size(W_Conv)) ' is loaded.']);
        end
        return
    end
    %~isfield(C_CV,'W_Conv_Mapped')
    if (displayOpts.calculation)
        disp([displayOpts.ies{6} 'W_Conv_Mapped is being constructed ']);
    end
    time_map = cputime;
        if (displayOpts.calculation), disp([displayOpts.ies{8} 'W_Conv values are assigned.']); end
        W_Conv = full(C_CV.W_ConvMap);
        if (displayOpts.calculation), disp([displayOpts.ies{8} 'W_Conv>0 map indices being found.']); end
        mapIndices = W_Conv>0;
        if (displayOpts.calculation), disp([displayOpts.ies{8} 'W_Conv(mapIndices) are being extracted.']); end
        mapIndices2 = W_Conv(mapIndices);
        if (displayOpts.calculation), disp([displayOpts.ies{8} 'mapValues are being assigned from whitened filter values.']); end
        mapValues = C_CV.filtersWhitened(mapIndices2);
        if (displayOpts.calculation), disp([displayOpts.ies{8} 'Found mapped values are assigned into W_Conv.']); end
        W_Conv(mapIndices) = mapValues;
        C_CV.W_Conv_Mapped = W_Conv;
        clear mapIndices mapIndices2 mapValues;
    time_map = cputime - time_map;
    if (displayOpts.calculation)
        if (displayOpts.timeMode>=3)
            disp([displayOpts.ies{6} 'W_Conv_Mapped construction is completed in ' num2str(time_map) ' cputime.']);
        else
            disp([displayOpts.ies{6} 'W_Conv_Mapped construction is completed.']);
        end
    end
end
function [batchCount, batchIDs] = decideToConvolveInBatches(data, W_Conv, displayOpts)
    varSize = dispVariableSize( data, 'data' );
    singleImageSize = varSize/size(W_Conv,1);
    memToUseAfterConv = sum(size(W_Conv))*singleImageSize;
    memAvail = displayMemory(false);
    memAvailDuringConv = memAvail - memToUseAfterConv + varSize;
    maxBatchSizeToConvolve = memAvailDuringConv/6;%50;%
    convolveInBatches = memToUseAfterConv>maxBatchSizeToConvolve;
    if (displayOpts.calculation)
        if (convolveInBatches)
            disp([displayOpts.ies{6} 'Convolution will be done in batches of ' num2str(maxBatchSizeToConvolve) ' MB.']);
        else
            disp([displayOpts.ies{6} 'Convolution will be in a single batch of ' num2str(memToUseAfterConv) ' MB.']);
        end
    end
    clear varSize singleImageSize memAvail
    if convolveInBatches
        %if the memory usage will be over something
        %do this in batches of data
        batchCount = max([ceil(memToUseAfterConv/maxBatchSizeToConvolve) 1]);
        batchIDs = getBatchIDs(size(data,2), batchCount);
    else
        batchCount = 1;
        batchIDs = [];
    end
end

% STEP-5 : Create shaped data if wanted as an output
function convolvedOriginalShapedData = reshapeForOriginalSize(C_CV, convolvedVectorizedData)
    Ro = C_CV.outputSize_original(1);
    Co = C_CV.outputSize_original(2);
    filCnt = C_CV.countFilt;
    countSamples = size(convolvedVectorizedData,2);
    convolvedOriginalShapedData = reshape(convolvedVectorizedData,Ro,Co,filCnt,countSamples);
end

% STEP-6 : Test convolution with the for-loop version
function testConvolution(data, convolvedOriginalShapedData, C_CV, testConvolutionImageCount, displayOpts)
    Rf = C_CV.filterSize(1);
    Cf = C_CV.filterSize(2);
    Ro = C_CV.outputSize_original(1);
    Co = C_CV.outputSize_original(2);
    countSamples = size(data,numel(size(data)));
    data = reshape(data,[C_CV.inputSize_original(1:end-1) countSamples]);  
    n = testConvolutionImageCount;
    slctdImgs = sort(randi(size(data,4),[1 n]));
    n_ConvolvedImages = zeros(size(convolvedOriginalShapedData(:,:,:,slctdImgs)));
    convSlct_R = 1:C_CV.stride(1):(C_CV.inputSize_original(1) - Rf + 1);
    convSlct_C = 1:C_CV.stride(2):(C_CV.inputSize_original(2) - Cf + 1);
    for imageNum = 1:testConvolutionImageCount
      for featureNum = 1:C_CV.countFilt
        %convW will have all channel filters
        convW = reshape(C_CV.filtersWhitened(featureNum,:),Rf,Cf,C_CV.inputChannelSize);
        % convolution of image with feature matrix for each channel
        convolvedImage = zeros(Ro, Co);
        for channel = 1:C_CV.inputChannelSize
          % Obtain the filter (patchDim x patchDim) needed during the convolution
          filterCur = convW(:,:,channel);
          % Flip the filterCur matrix because of the definition of
          % conv2, (this function will do it hence we do this rotation operation to cancel the one that will be applied in conv2)
          filterCur = rot90(squeeze(filterCur),2);
          % Obtain the current image to be convolved
          im = squeeze(data(:, :, channel, slctdImgs(imageNum)));
          % Convolve "filterCur" with "im", adding the result to convolvedImage
          % be sure to do a 'valid' convolution - means no padding
          convolvedImageUnStrided = conv2(im,filterCur,'valid'); 
          convolvedImage = convolvedImage + convolvedImageUnStrided(convSlct_R,convSlct_C) + C_CV.biasesWhitened(featureNum);
        end
        % Subtract the bias unit (correcting for the mean subtraction as well)
        % The convolved feature is the sum of the convolved values for all channels
        n_ConvolvedImages( :, :, featureNum, imageNum) = convolvedImage;
      end
    end    
    a1 = reshape(n_ConvolvedImages,Ro,Co,C_CV.countFilt,n);
    a2 = convolvedOriginalShapedData(:,:,:,slctdImgs);
    result = sum(abs(a1(:)-a2(:)))/numel(a1);
    if ~strcmp(displayOpts.displayLevel,'none')
        disp([displayOpts.ies{6} 'The sum of the error between looped-VS-vectorized convolution is ' num2str(result,'%4.2f')]);
        if result<1e-10
            disp(['The vectorized convolution code has been tested for ' num2str(n) ' random images and proved to be true.']);
        end
    end
    if result>=1e-10
        error(['The vectorized convolution code has been tested for ' num2str(n) ' random images but problems occured']);
    end
end

% STEP-7 : 
function [C_CV, convolvedVectorizedData] = applyMeanVarOperations(C_CV, data, convolvedVectorizedData, displayOpts, saveOpts)
    global minFuncStepInfo;
    %1. calculate the filter stats and set into the convolution cell
    C_CV.outputFilterStats = calculateMeanVar(convolvedVectorizedData, C_CV.outputSize_original);
    %add the outputFilterStats to filterStatsTrack
    
    if C_CV.appendMeanVarOfFilter
        globalOutputStruct('updateFilterStatsTrack',C_CV.outputFilterStats,'layerID',C_CV.layerIndice,'insertMode',1);
    end
    C_CV.appendMeanVarOfFilter = strcmp(C_CV.filterStatsTrackMode,'perBatchIteration');
    %if ~strcmp(C_CV.filterStatsTrackMode,'perBatchIteration'), C_CV.appendMeanVarOfFilter = false; end
    
    %2. check if mean or variance normalization will be applied
    if ~isempty(minFuncStepInfo) && isfield(minFuncStepInfo,'StepType') && ~strcmp(minFuncStepInfo.StepType,'MainIteration')
        C_CV.applyVarNorm = false;
        C_CV.applyBiasNorm = false;
    else
        %it is main iteration - the steplength must be 0 - as the first
        %run?
        if ~isempty(minFuncStepInfo) && isfield(minFuncStepInfo,'StepLength') && minFuncStepInfo.StepLength~=0
            if ~strcmp(C_CV.weightVarianceNormalizationMode,'iteration')
                C_CV.applyVarNorm = false;
            end
            if ~strcmp(C_CV.biasNormalizationMode,'iteration')
                C_CV.applyBiasNorm = false;
            end
        end        
    end
    applyVarBiasNorm = C_CV.applyVarNorm || C_CV.applyBiasNorm || C_CV.applyOrthonormalization;
    
    %3. apply mean variance normalization
    if applyVarBiasNorm
        paramsIn = struct( 'X_in',data,'X_out',convolvedVectorizedData...
                          ,'wvnValue',C_CV.weightVarianceNormalizationValue,'applyVarNorm',C_CV.applyVarNorm,'applyBiasNorm',C_CV.applyBiasNorm,'applyOrthonormalization',C_CV.applyOrthonormalization...
                          ,'displayOpts', displayOpts,'saveOpts', saveOpts...
                          ,'funObj', @convolveData...
                          ,'figureID',28);
        [convolvedVectorizedData, C_CV, weightVarianceVals] = applyFilterNormalization(paramsIn, C_CV);
        C_CV.weightVarianceValsInitial = weightVarianceVals;
    end
    
    if ~strcmp(C_CV.weightVarianceNormalizationMode,'iteration')
        C_CV.applyVarNorm=false;
    end
    if ~strcmp(C_CV.biasNormalizationMode,'iteration')
        C_CV.applyBiasNorm=false;
    end
    if C_CV.applyOrthonormalization
        C_CV.applyOrthonormalization = false;
    end
end

function C_CV = assignMissingSparseClassifier(C_CV)
    %set the default SparseClassifier parameters if some/all of the SparseClassifier parameters are not passed
    if ~isfield(C_CV,'paramsSparseClassifier')
        C_CV.paramsSparseClassifier = struct;
    end
    C_CV.paramsSparseClassifier = setParams_Missing_SparseClassifier(C_CV.paramsSparseClassifier);
end

function C_CV = assignMissingAutoencoderParams(C_CV)
    %set the default autoencoder parameters if some/all of the autoencoder parameters are not passed
    if ~isfield(C_CV,'paramsAutoEncode')
        C_CV.paramsAutoEncode = struct;
    end
    C_CV.paramsAutoEncode = setParams_Missing_Autoencoder(C_CV.paramsAutoEncode);
end

function C_CV = assignMissingParams_ELM(C_CV)
    %set the default ELM parameters if some/all of the ELM parameters are not passed
    if ~isfield(C_CV,'paramsELM')
        C_CV.paramsELM = struct;
    end
    C_CV.paramsELM = setParams_Missing_ELM(C_CV.paramsELM);
end

%% Note-1:
%1.This is what the following function would do traditionally
% W_Conv = zeros(size(C_CV.W_ConvMap));
% W_Conv(C_CV.W_ConvMap>0) = C_CV.filtersWhitened(C_CV.W_ConvMap(C_CV.W_ConvMap>0));
%2.We know that the weight matrix here is sparse
%  Hence turning it into sparse representation by the next line saves memory here
% W_Conv = sparse(W_Conv);
%3. For improving on creation of weight matrix with real filter values
%we can optimize by selecting the next method with sparsity to 3 times
%faster. 0.033 sec instead of 0.091 for first layer
% tic();

%% Note-2 :
%W_Conv(W_Conv>0) = C_CV.filtersWhitened(W_Conv(W_Conv>0));
% toc();
% tic();
%     W_Conv = zeros(size(C_CV.W_ConvMap));
%     W_Conv(C_CV.W_ConvMap>0) = C_CV.filtersWhitened(C_CV.W_ConvMap(C_CV.W_ConvMap>0));
%     W_Conv = sparse(W_Conv);
% toc();

%Here if W_Conv was not sparse but full it would be 6 times slower
%first layer 30 sec to 5 sec
%comment out and test below code
% tic();
%   convolvedVectorizedData = W_Conv'*reshape(data,[],countSamples);
% toc();
% W_Conv = full(W_Conv);
% tic();
%   convolvedVectorizedData = W_Conv'*reshape(data,[],countSamples);
% toc();

%% Note-3 :
% % a_in = repmat((1:rowCount)',1,ps);
% % a_in = reshape(a_in,[],1);
% % a_in = bsxfun(@plus,a_in,0:rowCount:rowCount*(C_CV.countFilt-1));
% % a_in = reshape(a_in,rowCount, colCount);
% bckPrpMp_a_in = reshape(bsxfun(@plus,reshape(repmat((1:rowCount)',1,ps),[],1),0:rowCount:(rowCount/ps)*(colCount-1)),rowCount, colCount);
% % s_o = reshape(map_singleInput',[],1);
% % s_o = repmat(s_o,1,colCount/size(map_singleInput,1));
% % s_o = reshape(s_o,rowCount, colCount);
% bckPrpMp_s_out= reshape(repmat(reshape(map_singleInput',[],1),1,colCount/ps),rowCount, colCount);
% bckPrpMp_s_out = zeros(rowCount, colCount);
% bckPrpMp_a_in = zeros(rowCount, colCount);
% for c = 1:colCount
%     dispstat(sprintf('Processing %d of %d%%',c,colCount),'timestamp'); 
%     [bckPrpMp_s_out(:,c), bckPrpMp_a_in(:,c)] = find(W_ConvMap==c);
% end
% C_CV.bckPrpMp.s_out =  bckPrpMp_s_out;
% C_CV.bckPrpMp.a_in  =  bckPrpMp_a_in;

%%Note 4 :
%C_CV.W_ConvMap = W_ConvMap;
%if I use a sparse 
%for the first conv layer it uses 11 mb (1205->1216) instead of 136 mb (1214->1350) memory
%for the first conv layer it uses 8 mb (1189->1197) instead of 143 mb (1197->1340) memory
%%C_CV.W_ConvMap = sparse(W_ConvMap);
%usage of W_ConvMap
%W_Conv = zeros(size(W_ConvMap));
%W_Conv(W_ConvMap>0) = filMultiple(W_ConvMap(W_ConvMap>0));
%convResult = reshape(W_Conv'*reshape(inMultiple,[],countSamples),Ro,Co,filCnt,countSamples);

%% Note-5 :
% some other whiten method we were not using
% filCnt = C_CV.countFilt;
% convolvedOriginalShapedData = reshape(convolvedVectorizedData,Ro,Co,filCnt,countSamples);
% %the below for loop uses 5 gb of memory for first layer of MNIST - how
% %to make this lower??
% for f = 1:filCnt
%     convolvedOriginalShapedData(:,:,f,:) = convolvedOriginalShapedData(:,:,f,:) + C_CV.biasesWhitened(f);
% end   
% convolvedVectorizedData = reshape(convolvedOriginalShapedData,size(convolvedVectorizedData));