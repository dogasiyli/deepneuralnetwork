function [a, cnnnet, time_spent_all] = convolution_ForwardPropogate(cnnnet, layer, data, varargin)
% STEP-0 : initializations 
    time_spent_all = cputime;
% STEP-1 : Set optional param defaults
    [ saveOpts,  displayOpts, memoryMethod, dataStruct, activationID, deletePrevLayerBatch] = getOptionalParams(varargin{:});
% STEP-2 : Learn and set input and output sizes
    cnnnet = arrangeLayerSizes(cnnnet, layer);
% STEP-3 : Give the user information about what will be done by printing on the screen
    countSamples = displayProcessInformation(data, cnnnet, layer, displayOpts);
% STEP-4 : Save initial activation if needed
    cnnnet = saveInitialData(cnnnet, data, activationID, layer, saveOpts, displayOpts, memoryMethod);
% STEP-5 : Apply convolution
    [cnnnet, a] = applyConvolution(cnnnet, data, layer, countSamples, displayOpts, saveOpts, dataStruct, deletePrevLayerBatch);
    clear data;
% STEP-6 : Write the timing information on screen      
    time_spent_all = timingInfo(cnnnet{layer,1}.outputSize_original, time_spent_all, displayOpts);
end

% Helper functions for readibility
% STEP-1 : getOptionalParams
function [ saveOpts,  displayOpts, memoryMethod, dataStruct, activationID, deletePrevLayerBatch] = getOptionalParams(varargin)
    %optional param defaults explanations : 
    %1. saveOpts         - needed to save the activation outputs of
    %                      pooling step to be able to backpropogate
    %                      saveInitialDataFolder = saveOpts.SubFolders.ActivatedData
    %2. displayOpts      - needed to be able to display what is
    %                      happening on the screen such as what
    %                      is the new size of data or how much
    %                      time spent for the pooling procedure    
    %P1D0 -> Passed assed paramaeter = 1, Default value used = 0
    [ saveOpts,  displayOpts,    memoryMethod,  dataStruct,  activationID,  deletePrevLayerBatch,  P1D0] = parseArgs(...
    {'saveOpts' 'displayOpts'   'memoryMethod' 'dataStruct' 'activationID' 'deletePrevLayerBatch'},...
    {    []           []       'all_in_memory'      []            1                true          },...
    varargin{:});
    %if displayOpts is not passed as a parameter then set is to 'all' by default 
    %to show the user what will happen in every detail.
    %also return it as a parameter because even if it is not passed in it
    %would be returned to be used for upcoming evaluations
    if (isempty(displayOpts))
        displayOpts = setDisplayOpts('all');
        if (displayOpts.defaultParamSet && P1D0.displayOpts == 0)
            disp([displayOpts.ies{4} 'display options is set to -all- by default.']);
        end
    end
    if (isempty(saveOpts))
        saveOpts = setSaveOpts('LayerConv_IterationEvery');
    end
    if ~exist(saveOpts.MainFolder,'dir')
        mkdir(saveOpts.MainFolder);
    end 
    if ~exist([saveOpts.MainFolder saveOpts.SubFolders.ActivatedData],'dir')
        mkdir([saveOpts.MainFolder saveOpts.SubFolders.ActivatedData]);
    end 
    
    %memoryMethod
    if (P1D0.memoryMethod==0)
        if (displayOpts.defaultParamSet)
            disp([displayOpts.ies{4} 'memoryMethod is set to default(all_in_memory).']);
        end
    end
end

% STEP-2 : Learn and set input and output sizes
function cnnnet = arrangeLayerSizes(cnnnet, layer)
    %the previous layer can be pooling,convolution or an activation layer of them
    %then following is in action
    if (layer>1)
        if ~isfield(cnnnet{layer,1},'inputSize_original') && isfield(cnnnet{layer-1,1},'outputSize_original')
            cnnnet{layer,1}.inputSize_original = cnnnet{layer-1,1}.outputSize_original;
            cnnnet{layer,1}.inputChannelSize = cnnnet{layer,1}.inputSize_original(3);
        end
    end
end

% STEP-3 : Give the user information about what will be done by printing on the screen
function countSamples = displayProcessInformation(a, cnnnet, layer, displayOpts)
    % if time spent needs to be passed here is how it goes
    countSamples = size(a, 2);
    if (displayOpts.calculation)
        if ( isfield(cnnnet{layer,1},'inputSize_original') && isfield(cnnnet{layer,1},'filterSize') && isfield(cnnnet{layer,1},'countFilt') && displayOpts.dataSize==1)
            disp([displayOpts.ies{4} 'Data of size ' mat2str([cnnnet{layer,1}.inputSize_original(1:end-1) countSamples]) ' will be convolved with ' mat2str(cnnnet{layer,1}.filterSize) ' sized ' num2str(cnnnet{layer,1}.countFilt) ' filters.']);
        else
            disp([displayOpts.ies{4} 'Data will be convolved.']);
        end
    end
end

% STEP-4 : Save initial activation if needed
function cnnnet = saveInitialData(cnnnet, a, activationID, layerID, saveOpts, displayOpts, memoryMethod)
    % save the initial data as activations{1} to a file if a folder name is passed
    % if this is the first layer we need to store the initial data as first activation data (a{1})
    if (layerID==1 && strcmp(memoryMethod,'write_to_disk'))
        saveProceedBool = strcmp(saveOpts.Params.Activations,'Every') ||...
                          strcmp(saveOpts.Params.Activations,'First');%it must be either Every or First    
        if ~saveProceedBool
            return
        end
        saveActivation(a, saveOpts, activationID, layerID, displayOpts );
        if isfield(cnnnet{activationID,1},'activatedData')
            cnnnet{activationID,1} = rmfield(cnnnet{activationID,1},'activatedData');
        end
    elseif strcmp(memoryMethod,'all_in_memory')
        cnnnet{activationID,1}.activatedData = a;
    end
end

% STEP-5 : Apply convolution
function [cnnnet,a] = applyConvolution(cnnnet, a, layer, countSamples, displayOpts, saveOpts, dataStruct, deletePrevLayerBatch)
    time_spent_convolve = cputime;
        [cnnnet{layer,1},a] = convolutionStep( cnnnet{layer,1}, a, displayOpts, saveOpts, dataStruct, deletePrevLayerBatch);
    time_spent_convolve = cputime-time_spent_convolve;
    if     (displayOpts.timeMode>=2 && displayOpts.calculation==1 && displayOpts.dataSize==1 && isfield(cnnnet{layer,1},'inputSize_original'))
        disp([displayOpts.ies{4} 'Data of size ' mat2str([cnnnet{layer,1}.inputSize_original(1:end-1) countSamples]) ' has been convolved with ' mat2str(cnnnet{layer,1}.filterSize) ' sized ' num2str(cnnnet{layer,1}.countFilt) ' filters in ' num2str(time_spent_convolve) ' cputime.']);
    elseif (displayOpts.timeMode>=2 && displayOpts.calculation==1 && displayOpts.dataSize==0)
        disp([displayOpts.ies{4} 'Data has been convolved in ' num2str(time_spent_convolve) ' cputime.']);
    end
end

% STEP-6 : Write the timing information on screen
function time_spent_all = timingInfo(outputSize_original, time_spent_all, displayOpts)
    time_spent_all = cputime - time_spent_all;
    if (displayOpts.dataSize==1 && displayOpts.timeMode>=2)
        disp([displayOpts.ies{4} 'After convolution data size has become ' mat2str(outputSize_original) ' in ' num2str(time_spent_all) ' cputime.']);
    elseif (displayOpts.dataSize~=1 && displayOpts.timeMode>=2)
        disp([displayOpts.ies{4} 'Convolution step completed in ' num2str(time_spent_all) ' cputime.']);
    elseif (displayOpts.dataSize==1 && displayOpts.timeMode<2)
        disp([displayOpts.ies{4} 'After convolution data size has become ' mat2str(outputSize_original) '.']);
    end
end
