function [Xapp, Yapp] = createAppendedData( X, Y ,appendSize )
%this takes X cols as input data samples
%attaches them one after another
%to create a time-series like data
    [countFeat,countSamples] = size(X);
    if appendSize>1
        %if we will append 'appendSize' number of samples one after another then
        Nout = countSamples-appendSize+1;
            Xapp = zeros(countFeat*appendSize,Nout);
            for i=1:appendSize
                rs = (i-1)*countFeat+1;
                rt = i*countFeat;
                Xapp(rs:rt,:) = X(:,i:i+Nout-1);
            end
        %Y is either labels or outputs of those samples
        %the results are the last Nout observations in Y
            [~,Nc] = size(Y);
            assert(countSamples==Nc,'input and output has different lengths');
            Yapp = Y(:,appendSize:end);
    else
        Xapp = X;
        Yapp = Y;
    end
end

