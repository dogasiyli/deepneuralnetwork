function [ deconvSet, activationValueIndicePair, mostActivFeatMaps] = createDeconvolutionSet( X, Y, cnnnet, displayOpts, saveOpts, probabilitiesOut)
    %X is data with size [countFeat by countSamples]
    %Y is labels with size [1 by countSamples]

    displayOpts.memOpts = 0;
    displayOpts.layerNames = 0;
    %there are 10 classes I know
    countCategory = size(probabilitiesOut,1);
    layerCount = size(cnnnet,1);
    deconvSet = cell(countCategory,layerCount);
    activationValueIndicePair = zeros(2,countCategory);
    mostActivFeatMaps = zeros(countCategory,size(cnnnet,1)+1);
    for i = 1:countCategory
        %find the most activated sample that belongs to this class
        [activationValueIndicePair(1,i), activationValueIndicePair(2,i)] = findMostActivatedTrue(i, Y, probabilitiesOut);
        indBest = activationValueIndicePair(1,i);
        [Z, mostActivFeatMaps(i,:)] = deconvolveSample( X(:,indBest),  cnnnet, displayOpts, saveOpts, ['Category(' num2str(i) ')-']);
        for j=1:layerCount
            deconvSet{i,j} = Z{j,1};
        end
    end
end