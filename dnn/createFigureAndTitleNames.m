function [titleStr, resultsFigureFileName] = createFigureAndTitleNames(dSCountBatchTrain, b, accuracyTrainDeep, resultsFigureSaveFolder)
    if exist('dSCountBatchTrain','var') && ~isempty(dSCountBatchTrain)
        if dSCountBatchTrain>1
            titleStr = ['Initial Step Train Accuracy-batchID(' num2str(b,'%02d') ') : ' num2str(accuracyTrainDeep,'%4.2f')];
            resultsFigureFileName = [resultsFigureSaveFolder 'Result_00_b' num2str(b,'%02d') '_acc(' num2str(accuracyTrainDeep,'%4.2f') ').png'];
        else
            titleStr = ['Initial Step Train Accuracy (batchID()) : ' num2str(accuracyTrainDeep,'%4.2f')];
            resultsFigureFileName = [resultsFigureSaveFolder 'Result_00_acc(' num2str(accuracyTrainDeep,'%4.2f') ').png'];                
        end
    else
        titleStr = ['Initial Step Train Accuracy(All Batches) : ' num2str(accuracyTrainDeep,'%4.2f')];
        resultsFigureFileName = [resultsFigureSaveFolder 'Result_00_All_acc(' num2str(accuracyTrainDeep,'%4.2f') ').png']; 
    end
end