function [testData, mostActivFeatMap] = deconvolveSample( sampleData, cnnnet, displayOpts, saveOpts, titleAddition)
    layerCount = size(cnnnet,1);
    testData = cell(layerCount+1,1);
    mostActivFeatMap = zeros(layerCount+1,1);
    activationID = 1;
    testData{1,1} = sampleData;
    for layer = 1:layerCount
        switch cnnnet{layer,1}.type
            case 'convolution'
                testData{layer+1,1} = convolution_ForwardPropogate( cnnnet, layer, testData{layer,1} ...
                                                                   ,'saveOpts' , saveOpts ...
                                                                   ,'displayOpts', displayOpts);
                %visualize the convolved data with imagesc
            case 'activation'
                activationID = activationID + 1;
                testData{layer+1,1} = activation_ForwardPropogate(  cnnnet, layer, testData{layer,1} ...
                                                                   ,'saveOpts' , saveOpts ...
                                                                   ,'displayOpts' , displayOpts ...
                                                                   ,'activationID', activationID);                                                                               
            case 'pooling'
                activationID = activationID + 1;
                testData{layer+1,1} = pooling_ForwardPropogate( cnnnet, layer, testData{layer,1} ...
                                                               ,'saveOpts' , saveOpts ...
                                                               ,'displayOpts' , displayOpts   ...
                                                               ,'activationID', activationID);
            case 'softmax'
                [testData{layer+1,1}, ~, predictionsOut, ~, probabilitiesOut] = softMax_ForwardPropogate( cnnnet, layer, testData{layer,1} ...
                                                                                                         ,'saveOpts' , saveOpts ...
                                                                                                         ,'displayOpts', displayOpts);
            case 'fullyconnected'
                testData{layer+1,1} = fullyConnected_ForwardPropogate( cnnnet, layer, testData{layer,1}...
                                                                      ,'saveOpts' , saveOpts ...
                                                                      ,'displayOpts', displayOpts);
            otherwise
                 error('You are suggesting a layer that is not present. Layers are - convolution, activation, pooling, softmax, fullyconnected');
        end
        poolOrConv_cur = strcmp(cnnnet{layer,1}.type,'convolution') || strcmp(cnnnet{layer,1}.type,'pooling');
        poolOrConv_prev = layer>1 && (strcmp(cnnnet{layer-1,1}.type,'convolution') || strcmp(cnnnet{layer-1,1}.type,'pooling'));
        if ( poolOrConv_cur || (poolOrConv_prev && strcmp(cnnnet{layer,1}.type,'activation')))
            [X_out, mostActivFeatMap(layer+1)] = plottableConvolvedFigure_SingleSample(testData{layer+1,1}, cnnnet{layer,1});
            if layer==layerCount-1
                drawConfusionMatWithNumbers( X_out, struct('figureID', {layerCount+2}, 'figureTitle' ,[titleAddition cnnnet{layer,1}.name]));
            else
                figure(layer);clf;imagesc(X_out);colorbar;
                title([titleAddition cnnnet{layer,1}.name]);
            end
        end
    end
end

