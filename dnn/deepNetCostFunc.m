function [ cost, grad , optionalOutputs] = deepNetCostFunc( theta, OPS)
%OPS = objFuncParamsStruct )%
    [objFuncParamsStructCorrect, missingFields] = check_objFuncParamsStruct(OPS, {'cnnnet',{'dataStruct',{'data','labels'}},'displayOpts','saveOpts','memoryMethod'});
    if objFuncParamsStructCorrect==false
        error(['There are missing fields in objective function parameter struct OPS ' missingFields]);
    end
    %optional parameters for the gradient function
    %if nothing is passed then cost must be calculated as it should be
    %also gradient must be calculated
    %but if the cost can be used as 1-accuracy or 100-accuracy then the
    %gradient and cost not need to be calculated
    OPS.useAccuracyAsCost = getStructField(OPS,'useAccuracyAsCost',false);
    OPS.calculateGradient = getStructField(OPS,'calculateGradient',true);
%% STEP 1: The  initialization and the update of weights in the cnnnet structure 
    global curStep; curStep = curStep +1;
    OPS.cnnnet = getInitialParams(OPS.cnnnet(:), theta, curStep, OPS.displayOpts);
    if (~exist('memoryMethod','var') || isempty(OPS.memoryMethod)), OPS.memoryMethod = 'all_in_memory'; end;%<'write_to_disk', 'all_in_memory'>

%% STEP 2: Compute Forward Propogation
    if ~isfield(OPS,'data') && isfield(OPS,'dataStruct')
        [cost, W_grad, b_grad, accuracyTrainDeep, confTrainDeep] = iterateAllBatches(OPS, curStep);
    else
        [cost, W_grad, b_grad, accuracyTrainDeep, confTrainDeep] = runSingleDataBatch(OPS, OPS.data, OPS.labels, curStep);  
    end
    grad = roll_grad_from_stacks( W_grad,  b_grad);
    
%% STEP 3 : Save network parameters
    saveNetworkStructure(OPS.cnnnet(:), OPS.dataStruct, OPS.saveOpts, OPS.displayOpts, curStep, accuracyTrainDeep);
    
%% STEP 4 : 
    optionalOutputs = assignOptionalObjectiveFunctionOutputs(OPS, struct('accuracy',accuracyTrainDeep));
end

function [cost_all, W_grad_all, b_grad_all, accuracyTrainDeep_Mean, confTrainDeep_All] = iterateAllBatches(OPS, curStep)
    dS = OPS.dataStruct;
    cnnnet = OPS.cnnnet(:);
    displayOpts = OPS.displayOpts;
    saveOpts = OPS.saveOpts;
    
    %this part will be done for each batch of train data and labels
    if (dS.countBatch.train>1)
        accuracyTrainDeep_Mean = 0;
        confTrainDeep_All = [];
        cost_all = 0;
        W_grad_all = [];
        b_grad_all = [];
    end    

    for b=1:dS.countBatch.train
        %% Load the batch for the loop
        dS = dS.updateDataStruct(dS, {'batchID_current', b});
        globalOutputStruct('updateEpochBatchParams',struct('batchCount',dS.countBatch.train,'batchID',b,'batchType','train'));
        if displayOpts.fileOperations
            disp([displayOpts.ies{4} 'Loading ' dS.fileName_current ''])
        end
        load(dS.fileName_current,'data','labels');
        data = reshape(data,dS.countFeat,[]);
        
        %% run the current batch and get the results
        [cnnnet, cost, W_grad, b_grad, accuracyTrainDeep, confTrainDeep] = runSingleDataBatch(OPS, data, labels, curStep);

        %% check how many batches the data has 
        accuracyTrainDeep_Mean = (accuracyTrainDeep_Mean*(b-1) + accuracyTrainDeep)/(b);
        cost_all = cost_all + cost;
        [confTrainDeep_All, W_grad_all, b_grad_all] = add2_all(confTrainDeep, W_grad, b_grad, confTrainDeep_All, W_grad_all, b_grad_all);
    end
    if (dS.countBatch.train>1)
        if ( ~strcmp(displayOpts.figureOpts.Show.Confusion,'None') &&  ~strcmp(saveOpts.Figures.Results,'None'))
            figureSaveFolder = [saveOpts.MainFolder saveOpts.SubFolders.ResultImages];
            callerFuncNameString = getCallerFuncName(2);
            minFuncAdditionalFileNameInfoStr = getMinFuncAdditionalFileNameInfoStr(curStep);
            figureFileName2Save = [figureSaveFolder 'Result_' minFuncAdditionalFileNameInfoStr '_b-ALL_acc(' num2str(accuracyTrainDeep_Mean,'%4.2f') ').png'];
            titleStr = { ['minFuncAdditionalInfoStr(' minFuncAdditionalFileNameInfoStr ')']...
                        ,['callerFunc(' callerFuncNameString ')']...
                        ,['Train Accuracy(All Batches)_{' num2str(curStep,'%02d') '}, Accuracy_{' num2str(accuracyTrainDeep_Mean,'%4.2f') '}']};
            
            layerCount = size(cnnnet,1)+1;%for two network layers this is 3
            h = drawConfusionMatWithNumbers( confTrainDeep_All, struct('figureID', {layerCount+1}, 'figureTitle' , {titleStr}));
            saveas(h,figureFileName2Save); 
        end
    end    
end

function [confTrainDeep_All, W_grad_all, b_grad_all] = add2_all(confTrainDeep, W_grad, b_grad, confTrainDeep_All, W_grad_all, b_grad_all)
    if isempty(confTrainDeep_All)
        confTrainDeep_All = confTrainDeep;
    else
        confTrainDeep_All = confTrainDeep_All + confTrainDeep;
    end
    if isempty(W_grad_all)
        W_grad_all = W_grad;
    else
        for i = 1:size(W_grad,2)
            if ~isempty(W_grad{1,i})
                W_grad_all{1,i} = W_grad_all{1,i} + W_grad{1,i};
            end
        end
    end
    if isempty(b_grad_all)
        b_grad_all = b_grad;
    else
        for i = 1:size(b_grad,2)
            if ~isempty(b_grad{1,i})
                b_grad_all{1,i} = b_grad_all{1,i} + b_grad{1,i};
            end
        end
    end
end

function [cnnnet, cost, W_grad, b_grad, accuracyTrainDeep, confTrainDeep] = runSingleDataBatch(OPS, data, labels, curStep)
    dS = OPS.dataStruct;
    cnnnet = OPS.cnnnet(:);
    displayOpts = OPS.displayOpts;
    saveOpts = OPS.saveOpts;
    memoryMethod = OPS.memoryMethod;
    %Forward Propogate
    [ cnnnet, softMaxedData, accuracyTrainDeep, confTrainDeep, activationID] = ...
                                                                forwardPropogateDeepNet( data, labels, cnnnet ...
                                                                                        ,'calculateBackPropMap', true ...1
                                                                                        ,'displayOpts', displayOpts ...1
                                                                                        ,'saveOpts', saveOpts...1
                                                                                        ,'dataStruct',dS...
                                                                                        ,'curStep', curStep...
                                                                                        ,'memoryMethod', memoryMethod);
    
    if OPS.useAccuracyAsCost
        cost = 100-accuracyTrainDeep;
    else
        %Compute Squared Error Term
        [cost, W_grad, b_grad] = computeSquaredErrorTerm( cnnnet, softMaxedData, labels, saveOpts, memoryMethod, activationID, displayOpts, curStep);

        %Display and Save Weight updates
        [~, ~, weightDiffResults] = get_WbCells_from_cnnnet(cnnnet, W_grad, b_grad, displayOpts, saveOpts, dS, curStep);
    end

    minFuncAdditionalInfoStr = minFuncStepInfo_GrabFigureTitle(curStep, cost, accuracyTrainDeep);
    callerFuncNameString = getCallerFuncName(2);
    layerCount = size(cnnnet,1)+1;%for two network layers this is 3
    show_save_train_step(layerCount, confTrainDeep, accuracyTrainDeep, curStep, displayOpts, saveOpts, dS, callerFuncNameString, minFuncAdditionalInfoStr);
end

function [cnnnet] = getInitialParams(cnnnet, theta, curStep, displayOpts)
    cnnnet = update_cnnnet_by_theta(theta, cnnnet, displayOpts);
%     %layerCount is tricky when it comes to slack, weight and activation
%     %calculations. an activation layer always appends a convolution/fullConnected or softMax layer hence the have to seem like 1 layer indeed
%     layerCount = size(cnnnet,1)+1;%for two network layers this is 3
%     %cellCount = layerCount - layerTypeCount(cnnnet, 'activation', false);  
    if (displayOpts.steps==1)
        disp(['++++++++' 'curStep(' num2str(curStep) ')-Forward Propogation++++++++'])
    end
    cnnnet = {cnnnet};
end

function saveNetworkStructure(cnnnet, dataStruct, saveOpts, displayOpts, curStep, accuracy)
    cnnnetSaveFolder = [saveOpts.MainFolder saveOpts.SubFolders.NetworkStructure];
    if ~exist(cnnnetSaveFolder,'dir')
        mkdir(cnnnetSaveFolder);
        if (displayOpts.fileOperations==1)
            disp(['Folder (' cnnnetSaveFolder ') is created because it didnt exist.']);
        end
    end
    minFuncAdditionalFileNameInfoStr = getMinFuncAdditionalFileNameInfoStr(curStep);
    if (dataStruct.countBatch.train>1)
        cnnnetSaveFileName = [cnnnetSaveFolder 'cnnnetStruct_' minFuncAdditionalFileNameInfoStr '_b(' num2str(dataStruct.batchID_current,'%02d') ')_accuracy(' num2str(accuracy,'%4.2f') ').mat'];
    else
        cnnnetSaveFileName = [cnnnetSaveFolder 'cnnnetStruct_' minFuncAdditionalFileNameInfoStr '_accuracy(' num2str(accuracy,'%4.2f') ').mat'];
    end
    for i=1:size(cnnnet,1)
        if isfield(cnnnet{i,1},'activatedData')
            cnnnet{i,1}.activatedData = [];
        end
    end    
    if (~isempty(cnnnetSaveFileName))
        %saving part
        if (displayOpts.fileOperations==1)
            disp([displayOpts.ies{4} 'Cnnet step id(' num2str(curStep) ') will be saved as (' cnnnetSaveFileName ')']);
        end
        save(cnnnetSaveFileName,'cnnnet');
        if (displayOpts.fileOperations==1)
            disp([displayOpts.ies{4} 'Cnnet step id(' num2str(curStep) ') has been saved as (' cnnnetSaveFileName ')']);
        end
    end
end

% this seems to be better
% W, Wgrad, b, bGrad [tempFolder 'Weight_Bias_ii.mat']
% a                  [tempFolder 'Activation_ii.mat']
% slack              [tempFolder 'Slack_ii.mat']

function show_save_train_step(layerCount, confTrainDeep, accuracyTrainDeep, curStep, displayOpts, saveOpts, dataStruct, callerFuncNameString, minFuncAdditionalInfoStr)
    %show the figure if one of the following conditions is met
    %1. not show confusion never
    %2. not save result figures never
    if ( ~strcmp(displayOpts.figureOpts.Show.Confusion,'None') &&  ~strcmp(saveOpts.Figures.Results,'None'))
        %set the file namne to save according to last or result
        figureSaveFolder = [saveOpts.MainFolder saveOpts.SubFolders.ResultImages];
        if (~exist(figureSaveFolder,'dir'))
            mkdir(figureSaveFolder);
        end        
        if (strcmp(saveOpts.Figures.Results,'Last'))
            if (dataStruct.countBatch.train>1)
                figureFileName2Save = [figureSaveFolder 'Result_b' num2str(dataStruct.batchID_current,'%02d') '_acc(' num2str(accuracyTrainDeep,'%4.2f') ').png'];
            else
                figureFileName2Save = [figureSaveFolder 'Result_acc(' num2str(accuracyTrainDeep,'%4.2f') ').png'];
            end
        elseif (strcmp(saveOpts.Figures.Results,'Every'))
            minFuncAdditionalFileNameInfoStr = getMinFuncAdditionalFileNameInfoStr(curStep);
            if (dataStruct.countBatch.train>1)
                figureFileName2Save = [figureSaveFolder 'Result_' minFuncAdditionalFileNameInfoStr '_b' num2str(dataStruct.batchID_current,'%02d') '_acc(' num2str(accuracyTrainDeep,'%4.2f') ').png'];
            else
                figureFileName2Save = [figureSaveFolder 'Result_' minFuncAdditionalFileNameInfoStr '_acc(' num2str(accuracyTrainDeep,'%4.2f') ').png'];
            end
        else
            figureFileName2Save = [];
        end
        
        if (dataStruct.countBatch.train>1)
            titleStr = ['Result_{' num2str(curStep,'%02d') '}, Acc_{' num2str(accuracyTrainDeep,'%4.2f') '}, BatchID_{' num2str(dataStruct.batchID_current,'%02d') '}'];
        else
            titleStr = ['Result_{' num2str(curStep,'%02d') '}, Acc_{' num2str(accuracyTrainDeep,'%4.2f') '}'];
        end
        if (~isempty(callerFuncNameString) && ~strcmp(callerFuncNameString,''))
            titleStr = {['callerFunc(' callerFuncNameString ')'],titleStr};
        end       
        if (~isempty(minFuncAdditionalInfoStr) && ~strcmp(minFuncAdditionalInfoStr,''))
            titleStr = {['minFuncAdditionalInfoStr(' minFuncAdditionalInfoStr ')'],titleStr{:}};
        end       
        if iscell(titleStr)
            h = drawConfusionMatWithNumbers( confTrainDeep, struct('figureID', {layerCount}, 'figureTitle' , {titleStr}));
        else
            h = drawConfusionMatWithNumbers( confTrainDeep, struct('figureID', {layerCount}, 'figureTitle' , titleStr));
        end        
        %is the figure only be displayed or also be saved
        if (~isempty(figureFileName2Save))
            %saving part
            if (displayOpts.fileOperations==1)
                disp([displayOpts.ies{4} 'Figure with id(' num2str(layerCount) ') will be saved as (' figureFileName2Save ')']);
            end
            saveas(h,figureFileName2Save);
            if (displayOpts.fileOperations==1)
                disp([displayOpts.ies{4} 'Figure with id(' num2str(layerCount) ') has been saved as (' figureFileName2Save ')']);
            end
        end
    end
end

function minFuncAdditionalFileNameInfoStr = getMinFuncAdditionalFileNameInfoStr(curStep)
    global minFuncStepInfo;
    minFuncAdditionalFileNameInfoStr = ['fe(' num2str(curStep,'%02d') ')'];
    if ~isempty(minFuncStepInfo) && isstruct(minFuncStepInfo) && isfield(minFuncStepInfo,'Iteration')
        minFuncAdditionalFileNameInfoStr = ['it(' num2str(minFuncStepInfo.Iteration,'%02d') ')_' minFuncAdditionalFileNameInfoStr];
    end              
end
%     switch memoryMethod
%         case 'all_in_memory'
%             %we will have the activations and slack variables in memory
%             W = cell(1,cellCount-1);%possible count of Weight parameter groups
%             b = cell(1,cellCount-1);%possible count of b parameter groups
%             a = cell(1,cellCount);%possible count of activations + input data
%             tempFolder_Train = [];
%             %tempFolder_Test = [];
%          case 'write_to_disk'
%             %we will write the activations and slack variables into disk
%             %then read from disk when necessary
%             global fastDiskTempPath;
%             tempFolder_Train = [fastDiskTempPath 'Train\'];%[];%
%             if ~isempty(tempFolder_Train) && ~exist(tempFolder_Train,'dir')
%                 mkdir(tempFolder_Train);
%             end
%             %tempFolder_Test = [fastDiskTempPath '\Test\'];%[];%
%             %if ~isempty(tempFolder_Test) && ~exist(tempFolder_Test,'dir')
%             %    mkdir(tempFolder_Test);
%             %end
%             %W      - [tempFolder 'W_ii.mat']     - no need to save because
%             %these are calculated per layer type in calcSlack function
%             %Wgrad  - [tempFolder 'Wgrad_ii.mat']
%             %b      - [tempFolder 'b_ii.mat']
%             %bGrad  - [tempFolder 'bGrad_ii.mat']
%             %slack  - [tempFolder 'slack_ii.mat']
%             %a      - [tempFolder 'a_ii.mat']
% 
%             %or
%             
%             % this seems to be better - this is being implemented
%             % W, Wgrad, b, bGrad [tempFolder 'Weight_Bias_ii.mat']
%             % a                  [tempFolder 'Activation_ii.mat'] - this
%             % part is implemented in convolution, activation and  layers
%             % slack              [tempFolder 'Slack_ii.mat']
%     end