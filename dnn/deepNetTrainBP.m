function [ cnnnet, outputCellArr, welmApplyRes] = deepNetTrainBP( cnnnet, varargin)
%% STEP 1: Initialize the necessary variables
    [ data,  labels,  dataStruct,  saveOpts,  paramsMinFunc,  batchParams, useAccuracyAsCost, pred_labels_train, curProjCode] = getOptionalParams(varargin{:});
    global minFuncStepInfo;
    saveOptsMainFolderBackup = saveOpts.MainFolder;
%% STEP 2: Train your sparse autoencoder with minFunc (L-BFGS).
    %[x,f,exitflag,output] = minFunc(funObj,x0,options,varargin)
    %For more info look at the end of file
    globalOutputStruct('updateEpochBatchParams',struct('generalType','deepNetTrainBP'));
    theta = stack_cnnnet_into_theta( cnnnet );   
    memoryMethod = 'write_to_disk';%<'write_to_disk', 'all_in_memory'>
    if strcmp(memoryMethod,'write_to_disk')
        saveOpts.Params.Activations='Every';
    end
    displayOpts = setDisplayOpts('none');
    welmApplyRes = [];
    if isfield(batchParams,'maxIter') && isfield(batchParams,'epochIter')
        [folderNames, fileNames, saveOpts] = getResultFileVariables( saveOpts, dataStruct, batchParams); %#ok<ASGLU>
        paramsMinFunc.maxIter = batchParams.maxIter;
        %Learn each batch until accuracy gets stuck
        %Then learn other batches.
        outputCellArr = cell(batchParams.epochIter,dataStruct.countBatch.train);
        welmApplyRes = cell(batchParams.epochIter,1);
        accPerBatch_Train = zeros(batchParams.epochIter,2*dataStruct.countBatch.train);
        for epochID=1:batchParams.epochIter
            dataStruct = dataStruct.updateDataStruct(dataStruct, {'type', 'train'});
            saveOpts.MainFolder = [saveOptsMainFolderBackup 'Train' filesep];
            
            [cnnnet, theta, applyWelm, welmStr] = welm_apply(cnnnet, theta, epochID);
            welmApplyRes{epochID,1} = struct;
            welmApplyRes{epochID,1}.applyWelm = applyWelm;
            welmApplyRes{epochID,1}.welmStr = welmStr;
            
            if isfield(batchParams,'dataShuffleMode') && ~strcmp(batchParams.dataShuffleMode,'none') && ~isempty(pred_labels_train)
                [distribMatNew, moveSummary] = planDataShuffle(pred_labels_train, batchParams.dataShuffleMode);
                [distribMatNew, shiftCntPerBatch] = distribMatNewClean( distribMatNew ); %#ok<NASGU>
                moveSummaryMat = shuffleData(dataStruct, distribMatNew);
                shuffleResultAcc = analyzeAccAfterShuffle( distribMatNew );
                disp('Shuffle result accuracies : ');
                disptable(shuffleResultAcc);
                globalOutputStruct('saveMoveSummary',struct('distribMatFolder',[saveOptsMainFolderBackup 'Train' filesep saveOpts.SubFolders.DistributionMat],'moveSummaryMat',moveSummaryMat,'distribMat',distribMatNew,'epochID',epochID));
                if isempty(moveSummary)
                    batchIDVec = randperm(dataStruct.countBatch.train);
                else
                    batchIDVec = randperm(max(moveSummary(:,2)));
                end
            else
                batchIDVec = randperm(dataStruct.countBatch.train);
            end
            disp(['batchID order = ' mat2str(batchIDVec)])
            batchCountForLoop = length(batchIDVec);
            try %#ok<TRYNC>
                if strcmpi(batchParams.batchLoopType,'single')
                    batchCountForLoop = 1;
                    batchIDVec = batchIDVec(1);
                end
            end
            for b = 1:batchCountForLoop
                batchID = batchIDVec(b);
                batchIDVecStr = strrep(mat2str(batchIDVec), [' ' num2str(batchID) ' '], ['<' num2str(batchID) '>']);
                disp(batchIDVecStr);                
                %check for C_CV.applyVarNorm || C_CV.applyBiasNorm || C_CV.applyOrthonormalization
                cnnnet = checkForVarianceBias_Conv(cnnnet,b);%{'none','initial','iteration'}
                
                globalOutputStruct('updateEpochBatchParams',struct('batchCount',dataStruct.countBatch.train,'batchID',batchID,'batchType','train','epochID_main',epochID,'epochID_sub',batchParams.epochIter));
                minFuncStepInfo.EpochID = epochID;
                minFuncStepInfo.BatchID = batchID;
                minFuncStepInfo.AdditionalLogTitle = ['EpochID(' num2str(epochID,'%d') ' of ' num2str(batchParams.epochIter,'%d') '),BatchID(' num2str(batchID,'%d') ' of ' num2str(dataStruct.countBatch.train,'%d') ')'];
                [cnnnet, theta, outputCellArr{epochID,batchID}] = iterate_a_batch(cnnnet, theta, data, labels, dataStruct, batchID, paramsMinFunc, displayOpts, saveOpts, memoryMethod, useAccuracyAsCost);
                checkToPause([mfilename '-afterBatchIteration']);
                resultsInfo = outputCellArr{epochID,batchID}.resultsInfo;%1_iteration, 2_funeval, 3_t_cur, 4_gdt, 5_f,6_acc,7_type
                accPerBatch_Train(epochID,(2*batchID-1):(2*batchID)) = resultsInfo([1 end],6)';
                
                %append epochID,batchID infront of resultsInfo and append
                %it to the train results file
                addline_resultsummaryfile( fileNames.trainResultSummary, epochID, batchID, resultsInfo);
            end
            checkToPause([mfilename '-afterEpoch']);
            diary('off');diary('on');
            addline_resultepochfile(fileNames.accPerBatch.Train, 'train', dataStruct, epochID, accPerBatch_Train(epochID,:));
            
            globalOutputStruct('updateAccPerBatch',[],'newRowAssign',true);
            
            saveOpts.MainFolder = saveOptsMainFolderBackup;
            
            [cnnnet, cnnnetResults_Train, ~, pred_labels_train] = deepNet_Test( dataStruct, cnnnet, displayOpts, saveOpts, 'train', '',epochID);
            cnnnetResults_curBatch = struct;
            cnnnetResults_curBatch.train = cnnnetResults_Train;
            if dataStruct.countBatch.train>1
                accuracyTrainDeep = cnnnetResults_Train.accuracy_Mean;
            else
                accuracyTrainDeep = cnnnetResults_Train.accuracy;
            end
            
            if dataStruct.countBatch.valid>0
                [ ~, cnnnetResults_Valid] = deepNet_Test( dataStruct, cnnnet, displayOpts, saveOpts, 'valid', '',epochID);
                cnnnetResults_curBatch.validation = cnnnetResults_Valid;
                if dataStruct.countBatch.valid>1
                    addline_resultepochfile(fileNames.accPerBatch.Valid, 'valid', dataStruct, epochID, cnnnetResults_Valid.accuracy_Arr);
                else
                    addline_resultepochfile(fileNames.accPerBatch.Valid, 'valid', dataStruct, epochID, cnnnetResults_Valid.accuracy);
                end
            end
            
            if dataStruct.countBatch.test>0
                [ ~, cnnnetResults_Test] = deepNet_Test( dataStruct, cnnnet, displayOpts, saveOpts, 'test', '',epochID);
                cnnnetResults_curBatch.test = cnnnetResults_Test;
                if dataStruct.countBatch.test>1
                    addline_resultepochfile(fileNames.accPerBatch.Test, 'test', dataStruct, epochID, cnnnetResults_Test.accuracy_Arr);
                else
                    addline_resultepochfile(fileNames.accPerBatch.Test, 'test', dataStruct, epochID, cnnnetResults_Test.accuracy);
                end
            end
            saveNetworkStructure(cnnnet, cnnnetResults_curBatch, saveOpts, displayOpts, epochID, accuracyTrainDeep, curProjCode);
        end         
    else
        %OPS = objFuncParamsStruct
        OPS = struct('cnnnet',{cnnnet},'dataStruct',dataStruct,'data',data,'labels',labels,'displayOpts',displayOpts,'saveOpts',saveOpts,'memoryMethod',memoryMethod,'useAccuracyAsCost',useAccuracyAsCost,'calculateGradient',true);
        checkToPause([mfilename '-mfdgVSmf-deepNetCostFunc'],[],'(compare minFunc_Doga vs minFunc)');
        [opt_theta,~,~,outputCellArr] = minFunc_Doga(@deepNetCostFunc, theta, paramsMinFunc, OPS);
        %[opt_theta,costFinal,exitflag,outputCellArr] = minFunc( @(p) deepNetCostFunc(p,OPS), theta, paramsMinFunc);
        cnnnet = update_cnnnet_by_theta(opt_theta, cnnnet, displayOpts);
    end
    minFuncStepInfo = [];
end

function saveNetworkStructure(cnnnet, cnnnetResults_curBatch, saveOpts, displayOpts, epochID, accuracyTrainDeep, curProjCode) %#ok<INUSL>
    global dropboxPath;
    global outputMatrixStructCNN;
    %for cancelling saveNetworkStructure function to run
    if strcmp(saveOpts.saveLevel,'none')
        cancelSaveNetworkStructure = 'saveOpts.saveLevel = <none>';
        if displayOpts.fileOperations
            disp([displayOpts.ies{4}  'Saving network structure cancelled because [' cancelSaveNetworkStructure ']']);
        end
        return
    end
    
    cnnnetSaveFolder = [dropboxPath outputMatrixStructCNN.generalInfo.dropboxSubFolderName saveOpts.SubFolders.NetworkStructure];
    if ~exist(cnnnetSaveFolder,'dir')
        mkdir(cnnnetSaveFolder);
        if (displayOpts.fileOperations==1)
            disp(['Folder (' cnnnetSaveFolder ') is created because it didnt exist.']);
        end
    end
    cnnnetSaveFileName = [cnnnetSaveFolder curProjCode '_cnnStr_eID' num2str(epochID,'%02d') '_acc' num2str(accuracyTrainDeep,'%4.2f') '.mat'];
    cnnnet = getCNNETreadyForNewData( cnnnet, 1, [], 1);     %#ok<NASGU>
    if (~isempty(cnnnetSaveFileName))
        %saving part
        if (displayOpts.fileOperations==1)
            disp([displayOpts.ies{4} 'Cnnnet epochID(' num2str(epochID) ') will be saved as (' cnnnetSaveFileName ')']);
        end
        save(cnnnetSaveFileName,'cnnnet','cnnnetResults_curBatch','-v7.3');
        if (displayOpts.fileOperations==1)
            disp([displayOpts.ies{4} 'Cnnnet epochID(' num2str(epochID) ') has been saved as (' cnnnetSaveFileName ')']);
        end
    end
end

function [ data,  labels,  dataStruct,  saveOpts,  paramsMinFunc,  batchParams, useAccuracyAsCost, pred_labels_train, curProjCode] = getOptionalParams(varargin)
  %% global variables
    global curStep;curStep = 0;
    global minFuncStepInfo;
    minFuncStepInfo = []; %#ok<NASGU>
    minFuncStepInfo = struct;
    minFuncStepInfo.FunEvals = 1;
    minFuncStepInfo.Iteration = 1;

  %% varargin parameters
    %P1D0 -> Passed assed parameter = 1, Default value used = 0
    [ data,  labels,  dataStruct,  displayOpts,  saveOpts,  paramsCategory_MinFunc,  paramsMinFunc,  batchParamsMode,  batchParams,  curProjCode,  useAccuracyAsCost,  dataShuffleMode,  pred_labels_train, P1D0] = parseArgs(...
    {'data' 'labels' 'dataStruct' 'displayOpts' 'saveOpts' 'paramsCategory_MinFunc' 'paramsMinFunc' 'batchParamsMode' 'batchParams' 'curProjCode' 'useAccuracyAsCost' 'dataShuffleMode' 'pred_labels_train'},...
    {  []      []         []           []           []        'showInfo_saveLog'           []               []              []      'curProjCode'        false           'uniform'              []         },...
    varargin{:});

    %if displayOpts is not passed as a parameter then set is to 'all' by
    %default to show the user what will happen in every detail.
    %also return it as a parameter because even if it is not passed in it
    %would be returned to be used for upcoming evaluations
    if (isempty(displayOpts))
        displayOpts = setDisplayOpts('none');
    end
    
    %if saveOpts is not passed as a parameter then set is to 'none' by
    %default not to save anything into disk unnecessarily.
    %also return it as a parameter because even if it is not passed in it
    %would be returned to be used for upcoming evaluations
    if (isempty(saveOpts))
        saveOpts = setSaveOpts('none');
        if (displayOpts.defaultParamSet && P1D0.saveOpts==0)
            disp([displayOpts.ies{4} 'save options is set to -none- by default.']);
        end
    end
    
    if (isempty(batchParamsMode) || isstruct(batchParamsMode))
        if isstruct(batchParamsMode)
            batchParams = batchParamsMode;
        end
        if isempty(batchParams) || ~isstruct(batchParams)
            batchParams = struct;
        end
        if ~isfield(batchParams,'maxIter')
            batchParams.maxIter = 20;
        end
        if ~isfield(batchParams,'epochIter')
            batchParams.epochIter = 10;
        end
        if ~isfield(batchParams,'batchLoopType')
            batchParams.batchLoopType = 'all';
        end
    elseif (batchParamsMode==1)
        batchParams = struct;
        batchParams.maxIter = 25;
        batchParams.epochIter = 4;  
        batchParams.batchLoopType = 'all';
    elseif (batchParamsMode==2)
        batchParams = struct;
        batchParams.maxIter = 5;
        batchParams.epochIter = 20; 
        batchParams.batchLoopType = 'all';
    elseif (batchParamsMode==3)
        batchParams = struct;
        batchParams.maxIter = 2;
        batchParams.epochIter = 200;
        batchParams.batchLoopType = 'all';
    elseif (batchParamsMode==4)
        batchParams = struct;
        batchParams.maxIter = 10;
        batchParams.epochIter = 20;
        batchParams.batchLoopType = 'all';
    elseif (batchParamsMode==5)
        batchParams = struct;
        batchParams.maxIter = 100;
        batchParams.epochIter = 200;
        batchParams.batchLoopType = 'all';
    elseif (batchParamsMode==6)
        batchParams = struct;
        batchParams.maxIter = 30;
        batchParams.epochIter = 200;
        batchParams.batchLoopType = 'single';
    else
        batchParams = struct;
        batchParams.maxIter = 20;
        batchParams.epochIter = 10;
        batchParams.batchLoopType = 'all';
    end
    batchParams.dataShuffleMode = dataShuffleMode;
    
    if (displayOpts.defaultParamSet && P1D0.batchParamsMode==0)
        disp([displayOpts.ies{4} 'batchParamsMode is set to -' num2str(batchParams.maxIter) ' iterations and ' num2str(batchParams.epochIter) ' epochs- by default.']);
    end                
    
    if isempty(paramsMinFunc)
        paramsMinFunc = setParams_minFunc(paramsCategory_MinFunc);
    end
    
    assert((P1D0.dataStruct==1 || (P1D0.data==1 && P1D0.labels==1)),'either dataStruct or data and labels must be passed as parameter to proceed');
    %dataStruct
    if (P1D0.dataStruct==0)
        dataStruct = struct;
        dataStruct.countCategory = length(unique(labels));
        dataStruct.countBatch.train = 1;
        dataStruct.countBatch.valid = 1;
        dataStruct.countBatch.test = 1;
        dataStruct.batchID_current = 1;
        dataStruct.countSamples = length(labels);
        dataStruct.countFeat = size(data);
        dataStruct.countFeat = prod(dataStruct.countFeat(1:end-1));
        if (displayOpts.defaultParamSet)
            disp([displayOpts.ies{4} 'dataStruct is created by default.']);
        end
        dataStruct.isDefault = true;
    end
    
    
    if isfield(paramsMinFunc,'logfile') && isempty(strfind(paramsMinFunc.logfile,filesep))
        paramsMinFunc.logfile = [saveOpts.MainFolder paramsMinFunc.logfile];
    end
end

function [cnnnet, opt_theta, outputCellArr] = iterate_a_batch(cnnnet, theta, data, labels, dataStruct, batchID, paramsMinFunc, displayOpts, saveOpts, memoryMethod, useAccuracyAsCost)
    if ~isfield(dataStruct, 'isDefault')
        dataStruct = dataStruct.updateDataStruct( dataStruct, {'batchID_current', batchID});
        if displayOpts.fileOperations
            disp([displayOpts.ies{4} 'Loading ' dataStruct.fileName_current ''])
        end
        load(dataStruct.fileName_current,'data','labels');
    end
    data = reshape(data,dataStruct.countFeat,[]);
    %OPS = objFuncParamsStruct
    OPS = struct('cnnnet',{cnnnet},'dataStruct',dataStruct,'data',data,'labels',labels,'displayOpts',displayOpts,'saveOpts',saveOpts,'memoryMethod',memoryMethod, 'useAccuracyAsCost',useAccuracyAsCost,'calculateGradient',true);
    checkToPause([mfilename '-mfdgVSmf-dnt-stackedAECostMulCNNNET_BP'],[],'(compare minFunc_Doga vs minFunc)');
    [opt_theta,~,~,outputCellArr] = minFunc_Doga(@stackedAECostMulCNNNET_BP, theta, paramsMinFunc, OPS);
    %[opt_theta,costFinal,exitflag,outputCellArr] = minFunc( @(p) stackedAECostMulCNNNET_BP(p,OPS), theta, paramsMinFunc);
   cnnnet = update_cnnnet_by_theta(opt_theta, cnnnet, displayOpts);
end

function cnnnet = checkForVarianceBias_Conv(cnnnet, batchListID)
    layerCount = size(cnnnet,1);
    for layer = 1:layerCount
        curCell = cnnnet{layer,1};
        switch curCell.type
            case 'convolution'
                %if type is batch and curStepType is batch then set to true else not
                %{'none','initial','epoch','batch','iteration'}
                switch curCell.weightVarianceNormalizationMode
                    case 'epoch'
                        curCell.applyVarNorm = batchListID==1;
                    case 'batch'
                        curCell.applyVarNorm = true;
                    otherwise
                        curCell.applyVarNorm = false;
                end
                switch curCell.biasNormalizationMode
                    case 'epoch'
                        curCell.applyBiasNorm = batchListID==1;
                    case 'batch'
                        curCell.applyBiasNorm = true;
                    otherwise
                        curCell.applyBiasNorm = false;
                end
                
                %if the batch is being changed and filterTrack mode is
                %perBatchInitial - set appendMeanVarOfFilter to true
                curCell.appendMeanVarOfFilter = strcmp(curCell.filterStatsTrackMode,'perBatchInitial');     
            case 'activation'
            case 'pooling'
            case 'softmax'
            case 'fullyconnected'
            otherwise
        end
        cnnnet{layer,1} = curCell;
    end    
end

function [cnnnet, theta, applyWelm, welmStr] = welm_apply(cnnnet, theta, epochID)
    global outputMatrixStructCNN;
    try
        if epochID<=1
            applyWelm = true;
            welmStr = 'ELM weights will be applied initially';
        else
            %get the train and validation columns
            trCols = outputMatrixStructCNN.accPerBatch.batchColumnsTr(end-1:end);
            vaCols = outputMatrixStructCNN.accPerBatch.batchColumnsVa(end-1:end);
            %tr(ELM-mean)
            rowToSlct = find(outputMatrixStructCNN.accPerBatch.Mat(:,1)==epochID-1,1,'last');
            trDif = outputMatrixStructCNN.accPerBatch.Mat(rowToSlct,trCols(2))-outputMatrixStructCNN.accPerBatch.Mat(rowToSlct,trCols(1));
            vaDif = outputMatrixStructCNN.accPerBatch.Mat(rowToSlct,vaCols(2))-outputMatrixStructCNN.accPerBatch.Mat(rowToSlct,vaCols(1));
            va_big_tr = vaDif>trDif;
            va_big_zero = vaDif > 0;
            applyWelm = va_big_tr & va_big_zero;
            if applyWelm
                welmStr = 'ELM weights will be applied because validation increase is both bigger than train increase and zero';
            else
                welmStr = ['ELM weights will NOT be applied because vaDif(' num2str(vaDif) '),trDif(' num2str(trDif) ')'];
            end
        end
    catch
        applyWelm = true;
        welmStr = 'ELM weights will be applied. The decision if did not go through.';
    end
    
    disp(welmStr);
    if applyWelm
        try              
            cnnnet{end, 1}.Weight = cnnnet{1, 1}.Welm';
            cnnnet{end, 1}.BiasParams = zeros(size(cnnnet{end, 1}.BiasParams));
            theta = stack_cnnnet_into_theta( cnnnet );
            disp('ELM weights applied to final layer successfully');
        catch
            disp('ELM weights couldnt be applied');
            welmStr = ['Elm NOT applied due to error-' welmStr];
        end
    end
end
%% More information area
  %% STEP 0 : Explain the function (trainCNNBackProp)
    %  [ cnnnet, output] = trainCNNBackProp( data, labels, cnnnet, paramsMinFunc, varargin )
    %  takes the data and trains CNN with it
    %
    %  Required Input Variables
    %  Input_01 : data
    %  - This is a REQUIRED input.
    %  - The data should be in such form that:
    %    1. [sizeVis,sampleSize] = size(data);
    %    2. all the values of each feature of each sample should be between(-1,1) or (0,1)
    %  Input_02 : cnnet
    %  - This is also a REQUIRED input
    %  - This structure param includes all the weights and configuration of
    %    the convolutional neural network
    %
    %  Optional Inputs
    %  Input_03 : paramsMinFunc
    %  - This is an optional input parameter that can be set to a default
    %    parameter
    %
    %  OUTPUT VARIABLES
    %
    %  Out_01 : Weights
    %  Out_02 : opttheta
    %  Out_03 : output
    %  Out_04 : firstLayerActivations

%STEP 0:
    %  trainAutoEncode(..., 'sparsityParam', sparsityParam )
    %       desired average activation of the hidden units.
    %       (This was denoted by the Greek alphabet rho, which looks like a lower-case "p",
    %       in the lecture notes). 
    %  trainAutoEncode(..., 'weightDecay', weightDecay )
    %       weight decay parameter
    %       lambda in lecture notes
    %  trainAutoEncode(..., 'sparsePenaltyWeight', sparsePenaltyWeight )
    %       weight of sparsity penalty term
    %       beta in lecture notes
%STEP 2: Train your sparse autoencoder with minFunc (L-BFGS).
    %For more info look at the end of file
    %[x,f,exitflag,output] = minFunc(funObj,x0,options,varargin)
    % Outputs:
    %   x is the minimum value found
    %   f is the function value at the minimum found
    %   exitflag returns an exit condition
    %   output returns a structure with other information
    % Supported Output Options
    %   iterations - number of iterations taken
    %   funcCount - number of function evaluations
    %   algorithm - algorithm used
    %   firstorderopt - first-order optimality
    %   message - exit message
    %   trace.funccount - function evaluations after each iteration
    %   trace.fval - function value after each iteration
