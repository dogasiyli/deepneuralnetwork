function [ cnnnet, resultsStruct, softMaxedData, cnnnetResults_Valid, cnnnetResults_Test, pred_labels_train] = deepNet_InitialTrain( dS, cnnnet, varargin)
%deepNet_InitialTrain The initialization of parameters can be different due
%to the cnnet strcture and architecture

%% STEP 1: Initialize (create and set) necessary variables
    [ displayOpts, saveOpts, softMaxOptimizeMaxIteration, memoryMethod, deletePrevLayerBatch] = getOptionalParams(varargin{:});
    if strcmp(memoryMethod,'write_to_disk')
        saveOpts.Params.Activations='Every';
    end
    
    %saveOptsMainFolderBackup = saveOpts.MainFolder; -> this is changed to [saveOpts.MainFolder 'Train\'];
    resultsFigureSaveFolder = setAndCreateFolders(displayOpts, saveOpts, 'Train');
    layerCount = size(cnnnet,1);
    batchCount = dS.countBatch.train;
    softMaxedData = [];
    pred_labels_train = [];
    
    globalOutputStruct('updateEpochBatchParams',struct('generalType','softMax_Init'));

%% 2. Initial training of all batches in training set
%     Also creating the last layer data and label arrays for optimizing the
%     last softMax layer
    if (batchCount>1)
        try
            resultsFigureFileNameEnding = ['initial_maxIter(' num2str(cnnnet{end, 1}.softMaxParams.paramsMinFunc.maxIter) ')'];
        catch
            resultsFigureFileNameEnding = 'initial';
        end
        dataTypeStr = 'train';
        [cnnnet, cnnnetResults_Initial, lastLayerActivation_data, lastLayerActivation_labels] = ...
            deepNet_InitialTrain_MultipleBatches(cnnnet, dS, dataTypeStr, resultsFigureSaveFolder, resultsFigureFileNameEnding, displayOpts, saveOpts, deletePrevLayerBatch);
        
        resultsStruct.accuracy_Arr_Initial = cnnnetResults_Initial.accuracy_Arr;
        resultsStruct.confMat_Cells_Initial = cnnnetResults_Initial.confMat_Cells;
        resultsStruct.accuracy_Mean_Initial = cnnnetResults_Initial.accuracy_Mean;
        resultsStruct.confMat_Mean_Initial = cnnnetResults_Initial.confMat_Mean;
        globalOutputStruct('assignSoftMaxCostAcc',struct('initVSfinal','Final','acc',cnnnetResults_Initial.accuracy_Mean));
        if prod(softMaxOptimizeMaxIteration)>0
            cnnnet = optimize_SoftMaxLayer(cnnnet, lastLayerActivation_data, lastLayerActivation_labels, displayOpts, saveOpts, softMaxOptimizeMaxIteration, false,dS);
            x = softMaxOptimizeMaxIteration(end);
            globalOutputStruct('updateAccPerBatch',[],'newRowAssign',true);
            [cnnnet, cnnnetResults_Train, ~, pred_labels_train] = deepNet_Test( dS, cnnnet, displayOpts, saveOpts, 'train', ['softMaxOpt-' num2str(x) ' of ' num2str(x)],[0 x x]);
            [ ~, cnnnetResults_Valid] = deepNet_Test( dS, cnnnet, displayOpts, saveOpts, 'valid', ['softMaxOpt-' num2str(x) ' of ' num2str(x)],[0 x x]);
            [ ~, cnnnetResults_Test] = deepNet_Test( dS, cnnnet, displayOpts, saveOpts, 'test', ['softMaxOpt-' num2str(x) ' of ' num2str(x)],[0 x x]);
            
            resultsStruct.confMat_Mean_Initial = cnnnetResults_Initial.confMat_Mean;
            resultsStruct.accuracy_Mean_Initial = cnnnetResults_Initial.accuracy_Mean;

            resultsStruct.accuracy_Arr = cnnnetResults_Train.accuracy_Arr;
            resultsStruct.confMat_Cells = cnnnetResults_Train.confMat_Cells;
            resultsStruct.accuracy_Mean = cnnnetResults_Train.accuracy_Mean;
            resultsStruct.confMat_Mean = cnnnetResults_Train.confMat_Mean;
            globalOutputStruct('assignSoftMaxCostAcc',struct('initVSfinal','Final','acc',cnnnetResults_Train.accuracy_Mean));
        else
            resultsStruct.accuracy_Arr = [];
            resultsStruct.confMat_Cells = [];
            resultsStruct.accuracy_Mean = [];
            resultsStruct.confMat_Mean = [];
            globalOutputStruct('updateAccPerBatch',[],'newRowAssign',true);
            [cnnnet, ~, ~, pred_labels_train] = deepNet_Test( dS, cnnnet, displayOpts, saveOpts, 'train', 'initial',0);
            [ ~, cnnnetResults_Valid] = deepNet_Test( dS, cnnnet, displayOpts, saveOpts, 'valid', 'initial',0);
            [ ~, cnnnetResults_Test] = deepNet_Test( dS, cnnnet, displayOpts, saveOpts, 'test', 'initial',0);  
        end
    else
        saveOptsMainFolderBackup = saveOpts.MainFolder;
        saveOpts.MainFolder = [saveOpts.MainFolder 'Train' filesep];
        initialSaveTheBatchesToActivatedDataFolder(dS, displayOpts, saveOpts);
        dS = dS.updateDataStruct(dS, {'batchID_current', 1});
        globalOutputStruct('updateEpochBatchParams',struct('batchCount',1,'batchID',1,'batchType','train'));
        if displayOpts.fileOperations
            disp([displayOpts.ies{4} 'Loading ' dS.fileName_current ''])
        end
        load(dS.fileName_current,'data','labels');
        data = reshape(data, dS.countFeat,[]); %#ok<NODEF>
        loadAndSaveForInitialLearning(1, 1, 1, displayOpts, saveOpts, data, labels, deletePrevLayerBatch);
        [ cnnnet, softMaxedData, accuracy, confMat] = ...
                                                  forwardPropogateDeepNet( data, labels, cnnnet ...
                                                                          ,'calculateBackPropMap', true ...
                                                                          ,'displayOpts', displayOpts ...
                                                                          ,'saveOpts', saveOpts ...
                                                                          ,'dataStruct', dS...
                                                                          ,'deletePrevLayerBatch', deletePrevLayerBatch);
        [titleStr, resultsFigureFileName] = createFigureAndTitleNames(batchCount, 1, accuracy, resultsFigureSaveFolder);
        h = drawConfusionDetailed( confMat, struct('figureID', {layerCount+1}, 'figureTitle' , titleStr));
        saveas(h,resultsFigureFileName);  

        resultsStruct.confMat = confMat;
        resultsStruct.accuracy = accuracy;
        saveOpts.MainFolder = saveOptsMainFolderBackup;
        cnnnet = deepNet_Test( dS, cnnnet, displayOpts, saveOpts, 'train', 'initial',0);
        [ ~, cnnnetResults_Valid] = deepNet_Test( dS, cnnnet, displayOpts, saveOpts, 'valid', 'initial',0);
        [ ~, cnnnetResults_Test] = deepNet_Test( dS, cnnnet, displayOpts, saveOpts, 'test', 'initial',0);  
    end
end

function [ displayOpts, saveOpts, softMaxOptimizeMaxIteration, memoryMethod, deletePrevLayerBatch] = getOptionalParams(varargin)
  %% varargin parameters
    %P1D0 -> Passed assed paramaeter = 1, Default value used = 0
    [ displayOpts,  saveOpts,  softMaxOptimizeMaxIteration,  memoryMethod,  deletePrevLayerBatch, P1D0] = parseArgs(...
    {'displayOpts' 'saveOpts' 'softMaxOptimizeMaxIteration' 'memoryMethod' 'deletePrevLayerBatch'},...
    {     []            []                 0                'all_in_memory'       true           },...
    varargin{:});

    %if displayOpts is not passed as a parameter then set is to 'all' by
    %default to show the user what will happen in every detail.
    %also return it as a parameter because even if it is not passed in it
    %would be returned to be used for upcoming evaluations
    if (isempty(displayOpts))
        displayOpts = setDisplayOpts('all');
        if (displayOpts.defaultParamSet && P1D0.displayOpts==0)
            disp([displayOpts.ies{4} 'display options is set to -all- by default.']);
        end
    end
    
    %if saveOpts is not passed as a parameter then set is to 'none' by
    %default not to save anything into disk unnecessarily.
    %also return it as a parameter because even if it is not passed in it
    %would be returned to be used for upcoming evaluations
    if (isempty(saveOpts))
        saveOpts = setSaveOpts('none');
        if (displayOpts.defaultParamSet && P1D0.saveOpts==0)
            disp([displayOpts.ies{4} 'save options is set to -none- by default.']);
        end
    end
               
    if (displayOpts.defaultParamSet && P1D0.softMaxOptimizeMaxIteration==0)
        disp([displayOpts.ies{4} 'softMaxOptimizeMaxIteration is set to <0> by default.']);
    end    

    if length(softMaxOptimizeMaxIteration)==1 && prod(softMaxOptimizeMaxIteration)>0
        softMaxOptimizeMaxIteration = [softMaxOptimizeMaxIteration 1];
    end    
end

function resultsFigureSaveFolder = setAndCreateFolders(displayOpts, saveOpts, dataTypeStr)%Train,Valid,Test
    saveOpts.MainFolder = [saveOpts.MainFolder dataTypeStr filesep];
    if ~isempty(saveOpts.MainFolder) && ~exist(saveOpts.MainFolder,'dir')
        if (displayOpts.fileOperations==1)
            disp([displayOpts.ies{2} 'folder(' saveOpts.MainFolder ') is created as tempFolder for ' dataTypeStr ' operations'])
        end
        mkdir(saveOpts.MainFolder);
    elseif (displayOpts.fileOperations==1)
            disp([displayOpts.ies{2} 'folder(' saveOpts.MainFolder ') was already created and will be used as tempFolder for ' dataTypeStr ' operations'])
    end
    resultsFigureSaveFolder = [saveOpts.MainFolder saveOpts.SubFolders.ResultImages];
    if (~exist(resultsFigureSaveFolder,'dir'))
        mkdir(resultsFigureSaveFolder);
    end
end
