function [cnnnet, cnnnetResults, lastLayerActivation_data, lastLayerActivation_labels] = ...
    deepNet_InitialTrain_MultipleBatches(cnnnet, dS, dataTypeStr, resultsFigureSaveFolder, resultsFigureFileNameEnding, displayOpts, saveOpts, deletePrevLayerBatch)

    lastLayerActivation_data = [];
    lastLayerActivation_labels = [];
    layerCount = size(cnnnet,1);
    
    if nargout>2
        memoryMethod = 'write_to_disk';
        saveOpts.Params.Activations = 'Last';
    else        
        memoryMethod = 'all_in_memory';
    end
    
    dS = dS.updateDataStruct(dS, {'type', dataTypeStr});
    batchCount = eval(['dS.countBatch.' dataTypeStr]);
    
    accuracy_Arr = zeros(1,batchCount);
    confMat_Cells = cell(1,batchCount);
    accuracy_Mean = 0;
    conf_Mean = [];
    saveOptsMainFolderBackup = saveOpts.MainFolder;
    saveOpts.MainFolder = [saveOpts.MainFolder 'Train' filesep];
    initialSaveTheBatchesToActivatedDataFolder(dS, displayOpts, saveOpts);
    for b=1:batchCount
        dS = dS.updateDataStruct(dS, {'batchID_current', b});
        globalOutputStruct('updateEpochBatchParams',struct('batchCount',batchCount,'batchID',b,'batchType',dataTypeStr));
        if displayOpts.fileOperations
            disp([displayOpts.ies{4} 'Loading ' dS.fileName_current ''])
        end
        load(dS.fileName_current,'data','labels');
        data = reshape(data,dS.countFeat,[]);
        saveOpts.batchID = b;
        [ cnnnet, ~, accuracyCurBatch, confCurBatch, activationID ] = ...
                                                          forwardPropogateDeepNet( data, labels, cnnnet ...
                                                                                  ,'calculateBackPropMap', true ...
                                                                                  ,'displayOpts', displayOpts ...
                                                                                  ,'saveOpts', saveOpts ...
                                                                                  ,'dataStruct', dS ...
                                                                                  ,'memoryMethod', memoryMethod ...
                                                                                  ,'deletePrevLayerBatch', deletePrevLayerBatch); %#ok<NODEF>
        saveOpts = rmfield(saveOpts, 'batchID');
        accuracy_Arr(b) = accuracyCurBatch;
        confMat_Cells{b} = confCurBatch;
        accuracy_Mean = (accuracy_Mean*(b-1) + accuracyCurBatch)/(b);
        if isempty(conf_Mean)
            conf_Mean = confCurBatch;
        else
            conf_Mean = conf_Mean + confCurBatch;
        end
        %[titleStr, resultsFigureFileName] = createFigureAndTitleNames(batchCount, b, accuracyCurBatch, resultsFigureSaveFolder);
        %resultsFigureFileName = [resultsFigureFileName '-' resultsFigureFileNameEnding '.png']; %#ok<AGROW>
        %h = drawConfusionDetailed( confCurBatch, struct('figureID', {layerCount+1}, 'figureTitle' , titleStr));
        %saveas(h,resultsFigureFileName);

        if strcmp(memoryMethod,'write_to_disk')
            layerID = size(cnnnet,1)-1;
            a = loadActivation(saveOpts, activationID, layerID, displayOpts);
            lastLayerActivation_data = [lastLayerActivation_data , a]; %#ok<AGROW>
            lastLayerActivation_labels = [lastLayerActivation_labels ; labels(:)]; %#ok<AGROW>
        end
    end
        
    [titleStr, resultsFigureFileName] = createFigureAndTitleNames([], [], accuracy_Mean, resultsFigureSaveFolder);
    h = drawConfusionDetailed( conf_Mean, struct('figureID', {layerCount+1}, 'figureTitle' , titleStr));
    saveas(h,resultsFigureFileName);
    
    cnnnetResults.accuracy_Arr = accuracy_Arr;
    cnnnetResults.confMat_Cells = confMat_Cells;
    cnnnetResults.accuracy_Mean = accuracy_Mean;
    cnnnetResults.confMat_Mean = conf_Mean;
    
    emptyTheActivatedDataFolder(saveOpts, '');
    
    saveOpts.MainFolder = saveOptsMainFolderBackup;
end
