function [ cnnnet, cnnnetResults, softMaxedData, pred_labels, lastLayerActivation_data, lastLayerActivation_labels] = deepNet_Test( dS, cnnnet, displayOpts, saveOpts, dataTypeStr, testInitFinalStr, epochID, deletePrevLayerBatch, emptyTheActivatedDataFolderBool)
%deepNet_Test The forward propogation of test or validation data
%   Detailed explanation goes here

%dataTypeStr - 'train','valid','test'
    if ~exist('deletePrevLayerBatch','var')
        deletePrevLayerBatch = true;
    end
    if ~exist('emptyTheActivatedDataFolderBool','var')
        emptyTheActivatedDataFolderBool = true;
    end

    [saveOpts, resultsFigureSaveFolder] = setAndCreateFolders(displayOpts, saveOpts, dataTypeStr);
    memoryMethod = 'write_to_disk';%<'write_to_disk', 'all_in_memory'>
    if strcmp(memoryMethod,'write_to_disk')
        saveOpts.Params.Activations='Every';
    end
    
    calculateBackPropMap = false;%strcmp(dataTypeStr,'train');
    layerCount = size(cnnnet,1);
    if length(epochID)==1
        epochStr = ['Epoc' num2str(epochID,'%03d') ''];
    else
        epochStr = ['Epoc' mat2str(epochID)];
    end
    
    lastLayerActivation_data = [];
    lastLayerActivation_labels = [];
    
    batchCount = eval(['dS.countBatch.' dataTypeStr]);
    dS = dS.updateDataStruct(dS, {'type', dataTypeStr});
    globalOutputStruct('updateEpochBatchParams',struct('batchCount',batchCount,'batchType',dataTypeStr));
    cnnnet = setVarianceBias_ConvFalse(cnnnet);
    pred_labels = struct;
    
    if (batchCount>1)
        accuracy_Arr = zeros(1,batchCount);
        confMat_Cells = cell(1,batchCount);
        accuracy_Mean = 0;
        conf_Mean = [];
        for b=1:batchCount
            dS = dS.updateDataStruct(dS, {'batchID_current', b});
            globalOutputStruct('updateMinFuncLog',struct('batchCount',batchCount,'batchID',b,'batchType',dataTypeStr));
            globalOutputStruct('updateEpochBatchParams',struct('batchID',b));

            if displayOpts.fileOperations
                disp([displayOpts.ies{4} 'Loading ' dS.fileName_current ''])
            end
            load(dS.fileName_current,'data','labels');
            data = reshape(data,dS.countFeat,[]);
            [ cnnnet, softMaxedData, accuracyCurBatch, confCurBatch, activationID ] = ...
                                                              forwardPropogateDeepNet( data, labels, cnnnet ...
                                                                                      ,'calculateBackPropMap', calculateBackPropMap ...
                                                                                      ,'displayOpts', displayOpts ...
                                                                                      ,'saveOpts', saveOpts ...
                                                                                      ,'dataStruct', dS...
                                                                                      ,'memoryMethod',memoryMethod...
                                                                                      ,'deletePrevLayerBatch', deletePrevLayerBatch); 
            [~,predCur] = max(softMaxedData);
            pred_labels(b).pred = predCur;
            pred_labels(b).labels = labels;
            pred_labels(b).softMaxedData = softMaxedData;
            [lastLayerActivation_data,lastLayerActivation_labels, a_data, a_labels] = appendLastLayerData(cnnnet, lastLayerActivation_data, lastLayerActivation_labels,displayOpts, saveOpts, labels);
            pred_labels(b).batchData = a_data;
            pred_labels(b).batchLabels = a_labels;
            accuracy_Arr(b) = accuracyCurBatch;
            confMat_Cells{b} = confCurBatch;
            accuracy_Mean = (accuracy_Mean*(b-1) + accuracyCurBatch)/(b);
            if isempty(conf_Mean)
                conf_Mean = confCurBatch;
            else
                conf_Mean = conf_Mean + confCurBatch;
            end
%             try %#ok<TRYNC>
%                 [titleStr, resultsFigureFileName] = createFigureAndTitleNames(epochStr, batchCount, b, accuracyCurBatch, resultsFigureSaveFolder, dataTypeStr, testInitFinalStr);
%                 h = drawConfusionDetailed( confCurBatch, struct('figureID', {layerCount+1}, 'figureTitle' , titleStr));
%                 saveas(h,resultsFigureFileName);
%             end
        end 
        try %#ok<TRYNC>
            [titleStr, resultsFigureFileName] = createFigureAndTitleNames(epochStr, [], [], accuracy_Mean, resultsFigureSaveFolder, dataTypeStr, testInitFinalStr);
            h = drawConfusionDetailed(conf_Mean, struct('figureID', {layerCount+1}, 'figureTitle' , titleStr));
            saveas(h,resultsFigureFileName);
            overallAcc = 100*sum(diag(conf_Mean))/sum(conf_Mean(:));
            disp([dataTypeStr '-Overall Accuracy:' num2str(overallAcc,'%4.2f')]);
            displayConfusionMatrix(conf_Mean, unique(lastLayerActivation_labels));
        end
        
        cnnnetResults.accuracy_Arr = accuracy_Arr;
        cnnnetResults.confMat_Cells = confMat_Cells;
        cnnnetResults.accuracy_Mean = accuracy_Mean;
        cnnnetResults.confMat_Mean = conf_Mean;
    else
        dS = dS.updateDataStruct(dS, {'batchID_current', 1});
        globalOutputStruct('updateMinFuncLog',struct('batchCount',1,'batchID',1,'batchType',dataTypeStr));
        if displayOpts.fileOperations
            disp([displayOpts.ies{4} 'Loading ' dS.fileName_current ''])
        end
        load(dS.fileName_current,'data','labels');
        data = reshape(data, dS.countFeat,[]);
        [ cnnnet, softMaxedData, accuracyCurBatch, confCurBatch, activationID ] = ...
                                                          forwardPropogateDeepNet( data, labels, cnnnet ...
                                                                                  ,'calculateBackPropMap', true ...
                                                                                  ,'displayOpts', displayOpts ...
                                                                                  ,'saveOpts', saveOpts ...
                                                                                  ,'dataStruct', dS...
                                                                                  ,'memoryMethod',memoryMethod...
                                                                                  ,'deletePrevLayerBatch', deletePrevLayerBatch);
        
        [~,predCur] = max(softMaxedData);
        pred_labels(1).pred = predCur;
        pred_labels(1).labels = labels;
        pred_labels(1).softMaxedData = softMaxedData;
        [lastLayerActivation_data,lastLayerActivation_labels, a_data, a_labels] = appendLastLayerData(cnnnet, lastLayerActivation_data, lastLayerActivation_labels,displayOpts, saveOpts, labels);
        pred_labels(1).batchData = a_data;
        pred_labels(1).batchLabels = a_labels;
        [titleStr, resultsFigureFileName] = createFigureAndTitleNames(epochStr, batchCount, 1, accuracyCurBatch, resultsFigureSaveFolder, dataTypeStr, testInitFinalStr);
        h = drawConfusionDetailed( confCurBatch, struct('figureID', {layerCount+1}, 'figureTitle' , titleStr));
        saveas(h,resultsFigureFileName);  
        
        cnnnetResults.confMat = confCurBatch;
        cnnnetResults.accuracy = accuracyCurBatch;
    end
    
    if strcmp(dataTypeStr,'train')
        disp([displayOpts.ies{4} 'Learning ELM weights for the last layer with the accumulated final layer activations.']);
        [remainingFeatIDs, irRelevantFeatsResultSummary] = findIrrelevantFeatures(lastLayerActivation_data', lastLayerActivation_labels, 'ELM_LDA');
        [Welm, ~, confMat_ELM, accuracy_ELM, rankDeficient] = getELM_Weights_SoftMax(lastLayerActivation_data', lastLayerActivation_labels, [], displayOpts);
        cnnnetResults.rankDeficient_ELM = rankDeficient;
        cnnnetResults.remainingFeatIDs = remainingFeatIDs;
        cnnnetResults.irRelevantFeatsResultSummary = irRelevantFeatsResultSummary;
        cnnnet{1,1}.Welm = Welm;
        removableFeats = 1:size(Welm,1);
        removableFeats(remainingFeatIDs) = [];
        cnnnet{1,1}.Welm_remainingFeatIDs = remainingFeatIDs;
        cnnnet{1,1}.Welm_removableFeats = removableFeats;
    else
        if displayOpts.calculation
            disp([displayOpts.ies{4} 'Testing ELM weights for the last layer with the accumulated final layer activations.']);
        end
        [~, ~, ~, accuracy_ELM, confMat_ELM] = analyzeActivationsElm(lastLayerActivation_data', lastLayerActivation_labels, cnnnet{1,1}.Welm, displayOpts.calculation);
        %also try the version where releantFeatures are the ones used
    end
    
    if isfield(cnnnet{1,1},'Welm_removableFeats')
        removableFeats = cnnnet{1,1}.Welm_removableFeats;
    end
    if isfield(cnnnet{1,1},'Welm_remainingFeatIDs')
        remainingFeatIDs = cnnnet{1,1}.Welm_remainingFeatIDs;
        [~, ~, ~, accuracy_ELM_reduced, confMat_ELM_reduced] = analyzeActivationsElm(lastLayerActivation_data(remainingFeatIDs,:)', lastLayerActivation_labels, cnnnet{1,1}.Welm(remainingFeatIDs,:), displayOpts.calculation);
        cnnnetResults.accuracy_ELM = accuracy_ELM_reduced;
        cnnnetResults.confMat_ELM = confMat_ELM_reduced;
        disp([dataTypeStr '-ELM_reduced(' num2str(size(cnnnet{1,1}.Welm,1)) '_to_' num2str(length(cnnnet{1,1}.Welm_remainingFeatIDs)) ') Accuracy:' num2str(accuracy_ELM_reduced,'%4.2f')]);
    end
    
    cnnnetResults.accuracy_ELM = accuracy_ELM;
    cnnnetResults.confMat_ELM = confMat_ELM;
    disp([dataTypeStr '-ELM Accuracy:' num2str(accuracy_ELM,'%4.2f')]);
    displayConfusionMatrix(confMat_ELM, unique(lastLayerActivation_labels));
    
    if emptyTheActivatedDataFolderBool
        emptyTheActivatedDataFolder(saveOpts, 'Activation_');
    end
    
    if (batchCount>1)
        globalOutputStruct('updateAccPerBatch',accuracy_Arr,'dataType',dataTypeStr,'accuracy_ELM',accuracy_ELM);        
    else
        globalOutputStruct('updateAccPerBatch',accuracyCurBatch,'dataType',dataTypeStr, 'accuracy_ELM',accuracy_ELM);
    end
end

function cnnnet = setVarianceBias_ConvFalse(cnnnet)
    layerCount = size(cnnnet,1);
    for layer = 1:layerCount
        curCell = cnnnet{layer,1};
        switch curCell.type
            case 'convolution'
                %if type is batch and curStepType is batch then set to true else not
                %{'none','initial','epoch','batch','iteration'}
                curCell.applyVarNorm = false;
                curCell.applyBiasNorm = false;
            case 'activation'
            case 'pooling'
            case 'softmax'
            case 'fullyconnected'
            otherwise
        end
        cnnnet{layer,1} = curCell;
    end    
end

function [saveOpts, resultsFigureSaveFolder] = setAndCreateFolders(displayOpts, saveOpts, dataTypeStr)
    tempFolder = saveOpts.MainFolder;
    
    dataTypeStr(1) = upper(dataTypeStr(1));
    saveOpts.MainFolder = [tempFolder dataTypeStr filesep];%[];%
    
    if ~isempty(saveOpts.MainFolder) && ~exist(saveOpts.MainFolder,'dir')
        if (displayOpts.fileOperations==1)
            disp([displayOpts.ies{2} 'folder(' saveOpts.MainFolder ') is created as tempFolder for <' dataTypeStr '> operations'])
        end
        mkdir(saveOpts.MainFolder);
    elseif (displayOpts.fileOperations==1)
            disp([displayOpts.ies{2} 'folder(' saveOpts.MainFolder ') was already created and will be used as tempFolder for <' dataTypeStr '> operations'])
    end
    resultsFigureSaveFolder = [saveOpts.MainFolder saveOpts.SubFolders.ResultImages];
    if (~exist(resultsFigureSaveFolder,'dir'))
        mkdir(resultsFigureSaveFolder);
    end
end

function [titleStr, resultsFigureFileName] = createFigureAndTitleNames(epochStr, dSCountBatchTrain, b, accuracyCurrenBatch, resultsFigureSaveFolder, dataTypeStr, testInitFinalStr)
    dataTypeStr(1) = upper(dataTypeStr(1));
    
    if exist('dSCountBatchTrain','var') && ~isempty(dSCountBatchTrain)
        if dSCountBatchTrain>1
            titleStr = [testInitFinalStr ' Step ' dataTypeStr ' Accuracy-batchID(' num2str(b,'%02d') ') : ' num2str(accuracyCurrenBatch,'%4.2f')];
            resultsFigureFileName = [resultsFigureSaveFolder 'Result_' epochStr '_b' num2str(b,'%02d') '_acc(' num2str(accuracyCurrenBatch,'%4.2f') ').png'];
        else
            titleStr = [testInitFinalStr ' Step ' dataTypeStr ' Accuracy (batchID(1)) : ' num2str(accuracyCurrenBatch,'%4.2f')];
            resultsFigureFileName = [resultsFigureSaveFolder 'Result_' epochStr '_acc(' num2str(accuracyCurrenBatch,'%4.2f') ').png'];                
        end
    else
        titleStr = [testInitFinalStr ' Step ' dataTypeStr ' Accuracy(All Batches) : ' num2str(accuracyCurrenBatch,'%4.2f')];
        resultsFigureFileName = [resultsFigureSaveFolder 'Result_' epochStr '_All_acc(' num2str(accuracyCurrenBatch,'%4.2f') ').png']; 
    end
end

function [lastLayerActivation_data,lastLayerActivation_labels, a_data, a_labels] = appendLastLayerData(cnnnet, lastLayerActivation_data, lastLayerActivation_labels,displayOpts, saveOpts, labels)
    memoryMethod = 'write_to_disk';
    activationID = 1;
    idSelected = 0;
    layerCount = size(cnnnet,1);
    for i = 1:layerCount
        switch cnnnet{i,1}.type
            case {'convolution','softmax','fullyconnected'}
            case {'activation','pooling'}
                activationID = activationID + 1;
        end
        if isfield(cnnnet{i,1},'ActivationLayerID') && cnnnet{i, 1}.LastActivationLayer
            assert(activationID == cnnnet{i,1}.ActivationLayerID);
            idSelected = activationID;
        end
        if isfield(cnnnet{i,1},'activatedData')
            memoryMethod = 'all_in_memory';
        end        
    end
    if idSelected~=0 && strcmp(memoryMethod,'all_in_memory')
        for i = 1:layerCount
            if isfield(cnnnet{i,1},'activatedData')
                if i==idSelected
                    a = cnnnet{idSelected,1}.activatedData;  
                end
                cnnnet{i,1}.activatedData = [];
            end
        end
    end

    if strcmp(memoryMethod,'write_to_disk')
        layerID = size(cnnnet,1)-1;
        a = loadActivation(saveOpts, activationID, layerID, displayOpts);
    end
    a_data = a;
    a_labels = labels(:);
    lastLayerActivation_data = [lastLayerActivation_data , a_data];
    lastLayerActivation_labels = [lastLayerActivation_labels ; a_labels];
end
