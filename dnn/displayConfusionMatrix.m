function displayConfusionMatrix(confMat, uniqueLabels)
    if size(uniqueLabels,2)==1
        uniqueLabels = uniqueLabels';
    end
    labelDisp = joinStrings(strread(num2str(uniqueLabels),'%s')','|' );
    disptable(confMat, labelDisp, labelDisp);
end