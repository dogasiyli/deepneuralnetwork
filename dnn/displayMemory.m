function [ memAvail, memMatlab ] = displayMemory(displayOnScreen, initialEmptyCharCount)
    if (~exist('displayOnScreen','var'))
        displayOnScreen = true;
    end
    if (~exist('initialEmptyCharCount','var'))
        initialEmptyCharCount = 0;
    end
    a = memory;
    memAvail = a.MemAvailableAllArrays/(1024*1024);
    memMatlab = a.MemUsedMATLAB/(1024*1024);
    if displayOnScreen
        initStrEmpty = '';
        if initialEmptyCharCount>0
            initStrEmpty = repmat(' ',1,initialEmptyCharCount);
        end
        disp([initStrEmpty 'Matlab uses(' num2str(memMatlab,'%4.2f') ' MB)-Available(' num2str(memAvail,'%4.2f') ' MB)'])
    end
end