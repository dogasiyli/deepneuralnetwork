function emptyTheActivatedDataFolder(saveOpts, fileNameInit)
    folderName = [saveOpts.MainFolder saveOpts.SubFolders.ActivatedData];
    if exist(folderName,'dir')
        delete([folderName fileNameInit '*.mat']);
    end
end