function [sizeImg, countSamples, sizeChn, countFeat] = exploreDataSize(data, sizeChn)
    % trainData can be in 3 different forms
    % 1. 2 dimensional
    %   [countFeat countSamples] = size(trainData)
    %   where countFeat = [sizeImg*sizeImg*sizeChn]
    %    example : mnist database
    %    columns are observations, rows are features
    %    if you know the sizeImg and sizeChn
    %    you can get sample_i as reshape(trainData(i,:),[sizeImg sizeImg sizeChn]);
    % 2. 3 dimensional
    %   [sizeImg sizeImg countSamples] = size(trainData);
    %    the data has 1 channel
    %
    % 3. 4 dimensional
    %    [sizeImg sizeImg sizeChn countSamples] = size(trainData)
    %    example output of a convolution layer or pooling layer
    switch numel(size(data))
        case 2
            [countFeat, countSamples] = size(data);
            assert(exist('sizeChn','var')~=0,'sizeChn must be given');
            sizeImg = sqrt(countFeat/sizeChn);
            if (mod(sizeImg,1)>0)
                sizeImg = -1;%this is not an image then :)
            end
            %assert(mod(sizeImg,1)==0,'the input images must be square');
        case 3
            if (~exist('sizeChn','var') || isempty(sizeChn))
                sizeChn = 1;
                [imageDimTest, sizeImg, countSamples] = size(data);
            else
                %only 1 single image where last dimension for countSamples
                %drops
                countSamples = 1;
                [imageDimTest, sizeImg, sizeChn] = size(data);
            end
            assert(imageDimTest==sizeImg,'the input images must be square');
            clear imageDimTest
            countFeat = numel(size(data));
        case 4
            [imageDimTest, sizeImg, sizeChn, countSamples] = size(data);
            assert(imageDimTest==sizeImg,'the input images must be square');
            clear imageDimTest  
            countFeat = sizeImg*sizeImg*sizeChn;
        otherwise
            error('unknown trainData format');
    end
end