function [W] = fill_W( initMode, n_in, n_out, W_size)
%fill_W Summary of this function goes here
%   Detailed explanation goes here
    switch initMode
        case 'SVD'
            r = max(W_size);
            W = randn(r,r);
            [S,~,~] = svd(W);
            W = S(1:W_size(1),1:W_size(2));
            W = W./max(abs(W(:)));
            W = 0.001*W;
        case 'Xavier'
            %A Filler based on the paper [Bengio and Glorot 2010]: Understanding
            %the difficulty of training deep feedforward neuralnetworks, but does not
            %use the fan_out value.

            %It fills the incoming matrix by randomly sampling uniform data from
            %[-stdW, stdW] where stdW = sqrt(3 / fan_in) where fan_in is the number
            %of input nodes.       

            %http://andyljones.tumblr.com/post/110998971763/an-explanation-of-xavier-initialization
            %which pointed me to website : 
            %https://github.com/BVLC/caffe/blob/737ea5e936821b5c69f9c3952d72693ae5843370/include/caffe/filler.hpp#L129-143
            varW = 3/n_in;
            %W = stdW*rand(convolutionCell.countFilt,convolutionCell.inputChannelSize*prod(convolutionCell.filterSize));
            W = initWeightByVariance(W_size, sqrt(varW), 0);
        case 'Glorot_Bengio'                    
            varW = 2/(n_in+n_out);
            W = initWeightByVariance(W_size, sqrt(varW), 0);
        case 'He_Rang_Zhen'%MSRAFiller
            varW = 2/(n_in);
            W = initWeightByVariance(W_size, sqrt(varW), 0);
        otherwise
        %case 'Experimental'           
            %the following gets values from gaussian distribution with standart deviation = stdW
            W = initWeightByVariance(W_size, sqrt(n_in), 0);
    end
end