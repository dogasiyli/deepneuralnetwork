function [sizeHid, OPS_out] = findHidSizeFromPCA(data, percentToPreserve, displayInfo)
%findHidSizeFromPCA This function calculates the number of principle
%components to preserve % of information using data
    [sampleCountInit, dimensionCount] = size(data);
    if sampleCountInit<dimensionCount
        data = data';
        [sampleCountInit, dimensionCount] = size(data);
        warning(['data is transposed to have ' num2str(sampleCountInit) ' samples in rows and ' num2str(dimensionCount) ' dimensions in cols']);
    end
    dataHuge = true;
    sampleCount = sampleCountInit;
    while dataHuge
        try
            [coeff,score,latent] = princomp(data);
            dataHuge = false;
        catch
            %data is too huge to apply svd
            %randomly select %75 of it and try again
            slct = randperm(sampleCount);
            sampleCount = floor(sampleCount*0.75);
            data = data(:,slct(1:sampleCount));
        end
    end
    latent = latent./sum(latent);
    percentToPreserveVec = cumsum(latent)*100;
    sizeHid = find(percentToPreserveVec>percentToPreserve,1,'first');
    if exist('displayInfo','var') && displayInfo
        display(['data preserved %' num2str(sum(latent(1:min(size(latent,1),sizeHid)))*100,'%4.2f') ]);  
    end
    if nargout > 1
        OPS_out.coeff = coeff;
        OPS_out.score = score;
        OPS_out.percentToPreserveVec = percentToPreserveVec;
    end
end

