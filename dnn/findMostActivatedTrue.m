function [indBest, maxVal] = findMostActivatedTrue(trueLabelIndex, labels, probabilitiesOut)
    %find the most activated samples index id
    %which is found truely
    
    %trueLabelIndex --> 1 to 10 to be given, which means 0 to 9 in reality
    %labels  --> 1 to 10 to be given, which means 0 to 9 in reality
    %the row of true class
    probOutCur = probabilitiesOut(trueLabelIndex,:);
    %make all the false class cols 0
    probOutCur(labels~=trueLabelIndex) = 0;
    %now get the maximum values index
    [maxVal,indBest] = max(probOutCur);
end