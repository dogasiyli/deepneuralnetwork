function [ cnnnet, X, accuracy, confMat, activationID, displayOpts, saveOpts, predictionsOut] = forwardPropogateDeepNet( X, Y, cnnnet, varargin)
% X = data of size (countFeat by countSamples)
% Y = labels of size (1 by countSamples)
    [ curStep,  displayOpts,  calculateBackPropMap,  saveOpts, dataStruct, memoryMethod, deletePrevLayerBatch] = getOptionalParams(Y, varargin{:});

    globalOutputStruct( 'updateWeightBiasDataTrack',[],'newRowAssign_data',true);
    [cnnnet, X, predictionsOut, activationID, timeInfo_Layers] = loopOverAllLayers(dataStruct, X, Y, cnnnet, calculateBackPropMap, displayOpts, saveOpts, curStep, memoryMethod, deletePrevLayerBatch);
    
    if (curStep==0 && displayOpts.timeMode>=1)
        disp(['Total initial forward propogate time = ' num2str(sum(timeInfo_Layers),'%4.2f') ' cputime.']);
        if (~strcmp(displayOpts.displayLevel,'none'))
            disp([displayOpts.ies{4} 'Initial confusion results of deep net']);
        end
    end
    [accuracy, confMat, ~] = calculateConfusion(predictionsOut, Y, ~strcmp(displayOpts.displayLevel,'none'), cnnnet{end,1}.countCategory);
end

function [ curStep,  displayOpts,  calculateBackPropMap,  saveOpts, dataStruct, memoryMethod, deletePrevLayerBatch] = getOptionalParams(Y, varargin)
    %P1D0 -> Passed assed paramaeter = 1, Default value used = 0
    [ curStep,  displayOpts,   calculateBackPropMap,  saveOpts,  dataStruct,   memoryMethod  ,  deletePrevLayerBatch, P1D0] = parseArgs(...
    {'curStep' 'displayOpts'  'calculateBackPropMap' 'saveOpts' 'dataStruct'  'memoryMethod'   'deletePrevLayerBatch'},...
    {    0           []               false               []         []      'all_in_memory'           true           },...
    varargin{:});

    %if displayOpts is not passed as a parameter then set is to 'all' by
    %default to show the user what will happen in every detail.
    %also return it as a parameter because even if it is not passed in it
    %would be returned to be used for upcoming evaluations
    if (isempty(displayOpts))
        displayOpts = setDisplayOpts('all');
        if (displayOpts.defaultParamSet && P1D0.displayOpts==0)
            disp([displayOpts.ies{4} 'display options is set to -all- by default.']);
        end
    end
    
    %if saveOpts is not passed as a parameter then set is to 'none' by
    %default not to save anything into disk unnecessarily.
    %also return it as a parameter because even if it is not passed in it
    %would be returned to be used for upcoming evaluations
    if (isempty(saveOpts))
        saveOpts = setSaveOpts('none');
        if (displayOpts.defaultParamSet && P1D0.saveOpts==0)
            disp([displayOpts.ies{4} 'save options is set to -none- by default.']);
        end
    end
    
    %inform the user about default parameter settings about
    %<calculateBackPropMap>
    if (displayOpts.defaultParamSet && P1D0.calculateBackPropMap==0)
        if (calculateBackPropMap)
            disp([displayOpts.ies{4} 'calculateBackPropMap is set to TRUE by default.']);
        else
            disp([displayOpts.ies{4} 'calculateBackPropMap is set to FALSE by default.']);
        end
    end
    
    %dataStruct
    if (P1D0.dataStruct==0)
        dataStruct = struct;
        dataStruct.countCategory = length(unique(Y));
        dataStruct.countBatch.train = 1;
        dataStruct.countBatch.valid = 1;
        dataStruct.countBatch.test = 1;
        dataStruct.batchID_current = 1;
        dataStruct.countSamples = length(Y);
        dataStruct.type = 'train';
        if (displayOpts.defaultParamSet)
            disp([displayOpts.ies{4} 'dataStruct is created by default.']);
        end
    end
    
    %memoryMethod
    if (displayOpts.defaultParamSet && P1D0.memoryMethod==0)
        disp([displayOpts.ies{4} 'memoryMethod is set to default(all_in_memory).']);
    end
end

function [cnnnet, X, predictionsOut, activationID, timeInfo_Layers] = loopOverAllLayers(dataStruct, X, Y, cnnnet, calculateBackPropMap, displayOpts, saveOpts, curStep, memoryMethod, deletePrevLayerBatch)
    layerCount = size(cnnnet,1);
    timeInfo_Layers = zeros(1,layerCount);
    activationID = 1;
    for layer = 1:layerCount
        if (displayOpts.layerNames==1)
            disp([displayOpts.ies{4} '++++Layer Name : ' cnnnet{layer,1}.name '++++']);
        end
        W = [];
        switch cnnnet{layer,1}.type
            case 'convolution'
                [X, cnnnet, timeInfo_Layers(layer), W] = applyConvolution(cnnnet, layer, X, activationID, displayOpts, saveOpts, curStep, memoryMethod, dataStruct, deletePrevLayerBatch);
            case 'activation'
                activationID = activationID + 1;
                [X, cnnnet, timeInfo_Layers(layer)] = applyActivation(cnnnet, layer, X, activationID, displayOpts, saveOpts, memoryMethod, dataStruct, deletePrevLayerBatch);
            case 'pooling'
                activationID = activationID + 1;
                [X, cnnnet, timeInfo_Layers(layer)] = applyPooling(cnnnet, layer, X, activationID, calculateBackPropMap, displayOpts, saveOpts, memoryMethod, dataStruct, deletePrevLayerBatch);
            case 'softmax'
                [X, cnnnet, timeInfo_Layers(layer), predictionsOut] = applySoftMax(cnnnet, layer, X, Y, displayOpts, saveOpts, memoryMethod, curStep, dataStruct, deletePrevLayerBatch);
            case 'fullyconnected'
                [X, cnnnet, timeInfo_Layers(layer), W] = applyFullyConnected(cnnnet, layer, X, activationID, displayOpts, saveOpts, memoryMethod, curStep, dataStruct, deletePrevLayerBatch);
            otherwise
                error('You are suggesting a layer that is not present.Layers are - convolution, activation, pooling, softmax');
        end
        globalOutputStruct( 'updateWeightBiasDataTrack',[],'layerType',cnnnet{layer,1}.type,'layerID',layer,'actData',X);
        displaySaveAndProceed(X, Y, dataStruct, W, cnnnet, layer, displayOpts, saveOpts, curStep);
    end
end

function [X, cnnnet, timeInfo_Layers_Cur, W] = applyConvolution(cnnnet, layer, X, activationID, displayOpts, saveOpts, curStep, memoryMethod, dataStruct, deletePrevLayerBatch)
    [X, cnnnet, timeInfo_Layers_Cur] = convolution_ForwardPropogate( cnnnet, layer              ...
                                                                    , X                         ...
                                                                    ,'dataStruct', dataStruct ...
                                                                    ,'displayOpts', displayOpts ...
                                                                    ,'saveOpts'   , saveOpts...
                                                                    ,'activationID', activationID...
                                                                    ,'memoryMethod', memoryMethod ...
                                                                    ,'deletePrevLayerBatch', deletePrevLayerBatch);
    W = cnnnet{layer}.filtersWhitened;
    if (saveOpts.Params.Weights.Conv==1)
        weightParamsSaveFolder = [saveOpts.MainFolder saveOpts.SubFolders.WeightParams];
        if (~exist(weightParamsSaveFolder,'dir'))
            mkdir(weightParamsSaveFolder);
        end
        if (strcmp(saveOpts.Params.Weights.Iteration,'Last'))
            fileName2Save = [weightParamsSaveFolder 'W_l(' num2str(layer,'%02d') ')_' cnnnet{layer}.name '.mat'];
        elseif (strcmp(saveOpts.Params.Weights.Iteration,'Every'))
            fileName2Save = [weightParamsSaveFolder 'W_i(' num2str(curStep,'%02d') ')_l(' num2str(layer,'%02d') ')_' cnnnet{layer}.name '.mat'];
        end
        if (displayOpts.fileOperations==1)
            disp([displayOpts.ies{4} cnnnet{layer}.type ' layer weights will be saved as (' fileName2Save ')']);
        end
        save(fileName2Save,'W');
        if (displayOpts.fileOperations==1)
            disp([displayOpts.ies{4} cnnnet{layer}.type ' layer weights has been saved as (' fileName2Save ')']);
        end
    end
end

function [X, cnnnet, timeInfo_Layers_Cur] = applyActivation(cnnnet, layer, X, activationID, displayOpts, saveOpts, memoryMethod, dataStruct, deletePrevLayerBatch)
    [X, cnnnet, timeInfo_Layers_Cur] = activation_ForwardPropogate( cnnnet, layer, X           ...
                                                                   ,'displayOpts' , displayOpts  ...
                                                                   ,'saveOpts'  , saveOpts   ...
                                                                   ,'activationID', activationID...
                                                                   ,'memoryMethod', memoryMethod...
                                                                   ,'dataStruct', dataStruct ...
                                                                   ,'deletePrevLayerBatch', deletePrevLayerBatch);
end

function [X, cnnnet, timeInfo_Layers_Cur] = applyPooling(cnnnet, layer, X, activationID, calculateBackPropMap, displayOpts, saveOpts, memoryMethod, dataStruct, deletePrevLayerBatch)
    [X, cnnnet, timeInfo_Layers_Cur] = pooling_ForwardPropogate( cnnnet, layer, X                 ...
                                                                ,'displayOpts' , displayOpts      ...
                                                                ,'saveOpts'  , saveOpts ...
                                                                ,'activationID', activationID                 ...
                                                                ,'memoryMethod', memoryMethod                 ...
                                                                ,'calculateBackPropMap', calculateBackPropMap...
                                                                ,'dataStruct', dataStruct ...
                                                                ,'deletePrevLayerBatch', deletePrevLayerBatch);
end

function [X, cnnnet, timeInfo_Layers_Cur, predictionsOut] = applySoftMax(cnnnet, layer, X, Y, displayOpts, saveOpts, memoryMethod, curStep, dataStruct, deletePrevLayerBatch)
    [X, cnnnet, predictionsOut, timeInfo_Layers_Cur] = softMax_ForwardPropogate( cnnnet, layer, X ...
                                                                                ,'saveOpts', saveOpts   ...
                                                                                ,'displayOpts', displayOpts   ...
                                                                                ,'memoryMethod', memoryMethod   ...
                                                                                ,'labels' , Y ...
                                                                                ,'deletePrevLayerBatch', deletePrevLayerBatch);
    if (saveOpts.Params.Weights.Soft)
        Weight = cnnnet{layer}.Weight; %#ok<NASGU>
        BiasParams = cnnnet{layer}.BiasParams; %#ok<NASGU>
        thetaParamsSaveFolder = [saveOpts.MainFolder saveOpts.SubFolders.WeightParams];
        if (~exist(thetaParamsSaveFolder,'dir'))
            mkdir(thetaParamsSaveFolder);
        end
        if (strcmp(saveOpts.Params.Weights.Iteration,'Last'))
            fileName2Save = [thetaParamsSaveFolder 'SoftTheta_l(' num2str(layer,'%02d') ')_' cnnnet{layer}.name '.mat'];
        elseif (strcmp(saveOpts.Params.Weights.Iteration,'Every'))
            fileName2Save = [thetaParamsSaveFolder 'SoftTheta_i(' num2str(curStep,'%02d') ')_l(' num2str(layer,'%02d') ')_' cnnnet{layer}.name '.mat'];
        end
        if (displayOpts.fileOperations==1)
            disp([displayOpts.ies{4} cnnnet{layer}.type ' layer weights will be saved as (' fileName2Save ')']);
        end
        save(fileName2Save,'Weight','BiasParams');
        if (displayOpts.fileOperations==1)
            disp([displayOpts.ies{4} cnnnet{layer}.type ' layer weights has been saved as (' fileName2Save ')']);
        end
    end    
end

function [X, cnnnet, timeInfo_Layers_Cur, W] = applyFullyConnected(cnnnet, layer, X, activationID, displayOpts, saveOpts, memoryMethod, curStep, dataStruct, deletePrevLayerBatch)
    [X, cnnnet, timeInfo_Layers_Cur] = fullyConnected_ForwardPropogate( cnnnet, layer, X ...
                                                                       ,'saveOpts', saveOpts   ...
                                                                       ,'displayOpts' , displayOpts...
                                                                       ,'activationID', activationID...
                                                                       ,'memoryMethod' , memoryMethod ...
                                                                       ,'deletePrevLayerBatch', deletePrevLayerBatch);
    W = cnnnet{layer}.filtersWhitened;
    if (saveOpts.Params.Weights.Full)
        b = cnnnet{layer}.biasesWhitened;
        weightSaveFolder = [saveOpts.MainFolder saveOpts.SubFolders.WeightParams];
        if (~exist(weightSaveFolder,'dir'))
            mkdir(weightSaveFolder);
        end        
        if (strcmp(saveOpts.Params.Weights.Iteration,'Last'))
            fileName2Save = [weightSaveFolder 'FullWB_l(' num2str(layer,'%02d') ')_' cnnnet{layer}.name '.mat'];
        elseif (strcmp(saveOpts.Params.Weights.Iteration,'Every'))
            fileName2Save = [weightSaveFolder 'FullWB_i(' num2str(curStep,'%02d') ')_l(' num2str(layer,'%02d') ')_' cnnnet{layer}.name '.mat'];
        end
        if (displayOpts.fileOperations==1)
            disp([displayOpts.ies{4} cnnnet{layer}.type ' layer weights will be saved as (' fileName2Save ')']);
        end
        save(fileName2Save,'W','b');
        if (displayOpts.fileOperations==1)
            disp([displayOpts.ies{4} cnnnet{layer}.type ' layer weights has been saved as (' fileName2Save ')']);
        end
    end
end

function displaySaveAndProceed(X, Y, dataStruct, W, cnnnet, layer, displayOpts, saveOpts, curStep)
    if (    ~strcmp(displayOpts.figureOpts.Show.Iterations,'None') ...
         && ((strcmp(cnnnet{layer,1}.type,'convolution') && displayOpts.figureOpts.Layers.Conv==1) || ~strcmp(cnnnet{layer,1}.type,'convolution')) ...
         && ((strcmp(cnnnet{layer,1}.type,'activation') && displayOpts.figureOpts.Layers.Act==1) || ~strcmp(cnnnet{layer,1}.type,'activation')) ...
         && ((strcmp(cnnnet{layer,1}.type,'pooling') && displayOpts.figureOpts.Layers.Pool==1) || ~strcmp(cnnnet{layer,1}.type,'pooling')) ...
         && ((strcmp(cnnnet{layer,1}.type,'softmax') && displayOpts.figureOpts.Layers.Soft==1) || ~strcmp(cnnnet{layer,1}.type,'softmax')) ...
         && ((strcmp(cnnnet{layer,1}.type,'fullyconnected') && displayOpts.figureOpts.Layers.Full==1) || ~strcmp(cnnnet{layer,1}.type,'fullyconnected')) ...
        )
        %TITLE SETTINGS AREA
        if (eval(['dataStruct.countBatch.' dataStruct.type '>1']))
            titleStr_figureName = [num2str(layer,'%02d') '-' 'layer name(' cnnnet{layer}.name '), stepID(' num2str(curStep,'%d') '), batchID(' num2str(dataStruct.batchID_current,'%02d') ')'];
            titleStr_Data = [{['batchID(' num2str(dataStruct.batchID_current,'%02d') '),' ' stepID(' num2str(curStep,'%d') ') - ' cnnnet{layer}.name]}...
                            ,{['minVal(' num2str(min(X(:)),'%6.4f') ')' ',maxVal(' num2str(max(X(:)),'%6.4f') ')'  ',difVal(' num2str(max(X(:))-min(X(:)),'%6.4f') ')']}];
        else
            titleStr_figureName = [ num2str(layer,'%02d') '-' 'layer name(' cnnnet{layer}.name '), stepID(' num2str(curStep,'%d') ')' ];
            titleStr_Data = [cnnnet{layer}.name 'minVal(' num2str(min(X(:)),'%6.4f') ')' ',maxVal(' num2str(max(X(:)),'%6.4f') ')'  ',difVal(' num2str(max(X(:))-min(X(:)),'%6.4f') ')'];
        end
        titleStr_Histogram = ['Histogram of values : ' cnnnet{layer}.name];
        h = figure(layer);
            clf;
            set(h,'name',titleStr_figureName,'numbertitle','off');
            if (exist('W','var') && ~isempty(W))
                rc = 3;
            else
                rc = 2;
            end
        %sort X according to labels for better visualization
        [Y, idx] = sort(Y);
        X = X (:,idx);
        %SUBPLOT AREA
        subplot(rc,1,1);imagesc(X);colorbar;
            xlabel('----All samples----');
            ylabel('----All dimensions vectorized----');
            title(titleStr_Data);
        subplot(rc,1,2);hist(X(:));
            xlabel('----The range of values in this layer----');ylabel('Number os samples in bins');
            title(titleStr_Histogram); 
        if (rc == 3 && exist('W','var')&& ~isempty(W))
            subplot(rc,1,3);imagesc(W);colorbar;
                set(gca,'XTick',ceil(linspace(1,size(W,2),min(8,size(W,2)))));
                set(gca,'YTick',ceil(linspace(1,size(W,1),min(8,size(W,1)))));
                switch (cnnnet{layer}.type)
                    case 'convolution'
                        xlabel({'Each column contains the weight parameters of a filter';...
                               ['Convolution filters = ' num2str(cnnnet{layer}.filterSize(1)) ' by ' num2str(cnnnet{layer}.filterSize(2)) ' filters for ' num2str(cnnnet{layer}.inputSize_original(3)) ' input(cols), ' num2str(cnnnet{layer}.outputSize_original(3)) ' output(rows) channels ']}...
                              );
                        ylabel('Filters of next layer');
                    case 'fullyconnected'
                        xlabel('Each cell contains the weight parameters of fullyconnected weights');
                end
                titleStr_WeightHist = [cnnnet{layer}.name '-W_{minVal(' num2str(min(W(:)),'%6.4f') ')},W_{maxVal(' num2str(max(W(:)),'%6.4f') ')},W_{difVal(' num2str(max(W(:))-min(W(:)),'%6.4f') ')}'];
                title(titleStr_WeightHist);
        end
        %FIGURE SAVING AREA
        if (~strcmp(saveOpts.Figures.Iterations,'None'))
            layerAndFilterFigureSaveFolder = [saveOpts.MainFolder saveOpts.SubFolders.LayersAndFilters];
            if (~exist(layerAndFilterFigureSaveFolder,'dir'))
                mkdir(layerAndFilterFigureSaveFolder);
            end
            if (strcmp(saveOpts.Figures.Iterations,'Last'))
                if (eval(['dataStruct.countBatch.' dataStruct.type '>1']))
                    figureFileName2Save = [layerAndFilterFigureSaveFolder 'Figure_b(' num2str(dataStruct.batchID_current,'%02d') ')_l(' num2str(layer,'%02d') ')_' cnnnet{layer}.name '.png'];
                else
                    figureFileName2Save = [layerAndFilterFigureSaveFolder 'Figure_l(' num2str(layer,'%02d') ')_' cnnnet{layer}.name '.png'];
                end
            elseif (strcmp(saveOpts.Figures.Iterations,'Every'))
                if (eval(['dataStruct.countBatch.' dataStruct.type '>1']))
                    figureFileName2Save = [layerAndFilterFigureSaveFolder 'Figure_b(' num2str(dataStruct.batchID_current,'%02d') ')_i(' num2str(curStep,'%02d') ')_l(' num2str(layer,'%02d') ')_' cnnnet{layer}.name '.png'];
                else
                    figureFileName2Save = [layerAndFilterFigureSaveFolder 'Figure_i(' num2str(curStep,'%02d') ')_l(' num2str(layer,'%02d') ')_' cnnnet{layer}.name '.png'];
                end
            end
            if (displayOpts.fileOperations==1)
                disp([displayOpts.ies{4} 'Figure with id(' num2str(layer) ') will be saved as (' figureFileName2Save ')']);
            end
            saveas(h,figureFileName2Save);
            if (displayOpts.fileOperations==1)
                disp([displayOpts.ies{4} 'Figure with id(' num2str(layer) ') has been saved as (' figureFileName2Save ')']);
            end
        end
    end
    if (displayOpts.memOpts==1)
        displayMemory(true, 4);
    end
    if (~strcmp(displayOpts.displayLevel,'none'))
        disp('++++++Proceeding To Next Layer++++++');
    end
end