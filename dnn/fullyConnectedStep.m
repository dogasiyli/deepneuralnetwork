function [C_FC, projectedVectorizedData] = fullyConnectedStep( C_FC, data, displayOpts, saveOpts, deletePrevLayerBatch)
%C_FC = Cell_FullyConnected

% STEP-1 : If the sizes aren't learnt/identified yet learn/identify them from data
    C_FC = learnIOSizes(C_FC, data);

% STEP-2 : If the filters aren't learnt yet learn them from data
    [C_FC] = learnFilters(C_FC, data, displayOpts, saveOpts);

% STEP-3 : If number of output arguments is more than 1 construct/calculate convolvedVectorizedData
    if nargout>1
        [projectedVectorizedData, C_FC] = forwPropData(C_FC, data, displayOpts);
    end

% STEP-4 : forward propogate all batches if necessary
    if C_FC.fwdPropAllBatches
        outputFilterStatsAllBatches = saveAllBatchesToActivatedDataFolder(C_FC, dataStruct, displayOpts, saveOpts, @forwPropData, projectedVectorizedData, deletePrevLayerBatch);
        C_CV.outputFilterStatsAllBatchesInitial = outputFilterStatsAllBatches;
        C_CV.fwdPropAllBatches = false;
    end
end

% STEP-1 : If the sizes aren't learnt/identified yet learn/identify them from data
function C_FC = learnIOSizes(C_FC, data)
    % just checking if any size parameter exist will do the trick
    % TODO - we should check if data is a struct due to many batches of data as files
    [~, countSamples, ~, countFeat] = exploreDataSize(data, 1);
    if isfield(C_FC,'inputSize_original')
        %check if it is consistent with inputSize_original
        assert(prod(C_FC.inputSize_original(1:end-1))==countFeat,'must be equal');
        C_FC.inputSize_original(end) = countSamples;
    else
        C_FC.inputSize_original = [1, 1, countFeat, countSamples];
    end
    if isfield(C_FC,'inputSize_vectorized')
        %check if it is consistent with inputSize_vectorized
        assert(C_FC.inputSize_vectorized(1)==countFeat,'must be equal');
        C_FC.inputSize_vectorized(end) = countSamples;
    else
        C_FC.inputSize_vectorized = [prod(C_FC.inputSize_original(1:end-1)), countSamples];
    end
    if (~isfield(C_FC,'outputSize_original') || ~isfield(C_FC,'outputSize_vectorized') || size(data,2)~=C_FC.outputSize_original(end))
        %assert(C_FC.inputSize_vectorized(1)==d,'The feature size of data given and the weight size expected do not fit');
        C_FC.outputSize_original = [1,1,C_FC.sizeHid, countSamples];
        C_FC.outputSize_vectorized = [C_FC.sizeHid, countSamples];
    end
end

% STEP-2 : If the filters aren't learnt yet learn them from data
function [C_FC] = learnFilters(C_FC, data, displayOpts, saveOpts)
    if ~isfield(C_FC,'filtersWhitened')
        if (strcmp(C_FC.initializeWeightMethod,'Autoencoder'))
            %if the autoencoder parameters are not passed (or some/any of them)
            C_FC = checkAutoencoderParams(C_FC);
                disp([mat2str([C_FC.inputSize_vectorized(1) C_FC.outputSize_vectorized(1)]) ' sized weights will be initialized using autoencoder.']);
                    C_FC.filterParams = struct;
                    [ ~,                                        ... opttheta
                      C_FC.filterParams.W,       ...
                      C_FC.filterParams.b,       ...
                      C_FC.filterParams.meanData,...
                      C_FC.filterParams.ZCA] =   ...
                ...
                autoencodeData( data                          ...
                               ,C_FC.sizeHid...
                               ,'paramsAutoEncode', C_FC.paramsAutoEncode...
                               ,'displayOpts', displayOpts...
                               ,'saveOpts',saveOpts);
                C_FC.filtersWhitened = (C_FC.filterParams.W*C_FC.filterParams.ZCA);
                C_FC.biasesWhitened = C_FC.filterParams.b - C_FC.filtersWhitened*C_FC.filterParams.meanData;
        elseif (strcmp(C_FC.initializeWeightMethod,'Random'))
            %W = reshape(opttheta(1:countFilt * sizeVis), countFilt,sizeVis);
            if displayOpts.warningInfo
                warning('Random initializations may not succeed at times. Small number of steps with small number of samples - an autoencoder makes more sense to start with');
            end
            W_size = [C_FC.outputSize_vectorized(1),C_FC.inputSize_vectorized(1)];
            W = fill_W( C_FC.randInitMode, C_FC.inputSize_vectorized(1), C_FC.outputSize_vectorized(1), W_size);
            
            C_FC.filtersWhitened = W;
            C_FC.biasesWhitened = zeros(C_FC.outputSize_vectorized(1),1);
        elseif (strcmp(C_FC.initializeWeightMethod,'ELM'))
            [ C_FC.filterParams.meanData, C_FC.filterParams.ZCA, data ] = whitenData(data);
            [W, b] = ELM_autoencoder( data...
                                     ,'sizeHid',C_FC.sizeHid...
                                     ,'paramsELM', C_FC.paramsELM...
                                     ,'displayOpts', displayOpts);             
            
            C_FC.filtersWhitened = W;
            C_FC.biasesWhitened = b;
        end
    end
end

% STEP-3 : Forward propogate this step
function [projectedVectorizedData, C_FC] = forwPropData(C_FC, data, displayOpts)
    if isfield(C_FC,'filterParams') && isfield(C_FC.filterParams,'ZCA')
        data = bsxfun(@minus, data, C_FC.filterParams.meanData);
        data = C_FC.filterParams.ZCA * data;
    end
    projectedVectorizedData = bsxfun(@plus,C_FC.filtersWhitened*data,C_FC.biasesWhitened);
end

function C_FC = checkAutoencoderParams(C_FC)
    if ~isfield(C_FC,'paramsAutoEncode')
        C_FC.paramsAutoEncode = struct;
    end
    C_FC.paramsAutoEncode = setParams_Missing_Autoencoder(C_FC.paramsAutoEncode);
    %C_FC.paramsAutoEncode.maxIter = 20;
end

%% DELETED CODES
% the below part is needed for backpropogation
% %     Conv2FiltW_Grad_Map = inf(prod(convolutionCell.outputSize_original(1:2)),max(W_ConvMap(:)));
% %     for fp = 1:max(W_ConvMap(:))
% %         foundIndices = find(W_ConvMap==fp)';
% %         Conv2FiltW_Grad_Map(:,fp) = foundIndices;
% %     end
% %     convolutionCell.Conv2FiltW_Grad_Map = Conv2FiltW_Grad_Map;