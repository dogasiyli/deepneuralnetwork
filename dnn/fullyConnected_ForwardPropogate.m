function [data, cnnnet, time_spent_all] = fullyConnected_ForwardPropogate(cnnnet, layer, data, varargin)    
% STEP-0 : initializations
    time_spent_all = cputime;
% STEP-1 : Set optional param defaults
    [displayOpts, saveOpts, memoryMethod, activationID, deletePrevLayerBatch] = getOptionalParams(varargin{:});
% STEP-2 : Learn and set input and output sizes
    cnnnet = arrangeLayerSizes(cnnnet, layer, data);
% STEP-3 : Give the user information about what will be done by printing on the screen
    displayProcessInformation(data, cnnnet, layer, displayOpts);
% STEP-4 : Save initial activation if needed
    cnnnet = saveInitialData(cnnnet, data, activationID, layer, saveOpts, displayOpts, memoryMethod);
% STEP-5 : Apply connected layer
    [cnnnet, data] = proceedLayer(cnnnet, layer, data, displayOpts, saveOpts, deletePrevLayerBatch);
% STEP-6 : Write the timing information on screen      
    time_spent_all = timingInfo(cnnnet{layer,1}.outputSize_original, time_spent_all, displayOpts);
end

% Helper functions for readibility
% STEP-1 : getOptionalParams
function [displayOpts, saveOpts, memoryMethod, activationID, deletePrevLayerBatch] = getOptionalParams(varargin)
    %for detailed explanation look at the bottom of file - "8.optional param defaults explanations"
    %P1D0 -> Passed assed paramaeter = 1, Default value used = 0
    [ displayOpts,  saveOpts,   memoryMethod,  activationID,  deletePrevLayerBatch, P1D0] = parseArgs(...
    {'displayOpts' 'saveOpts'  'memoryMethod' 'activationID' 'deletePrevLayerBatch'},...
    {     []           []     'all_in_memory'        0                 true        },...
    varargin{:});

    %if displayOpts is not passed as a parameter then set is to 'all' by
    %default to show the user what will happen in every detail.
    %also return it as a parameter because even if it is not passed in it
    %would be returned to be used for upcoming evaluations
    if (isempty(displayOpts))
        displayOpts = setDisplayOpts('all');
        if (displayOpts.defaultParamSet && P1D0.displayOpts==0)
            disp([displayOpts.ies{2} 'display options is set to -all- by default.']);
        end
    end
    if (isempty(saveOpts))
        saveOpts = setSaveOpts();
    end
    if ~exist(saveOpts.MainFolder,'dir')
        mkdir(saveOpts.MainFolder);
    end 
    if ~exist([saveOpts.MainFolder saveOpts.SubFolders.ActivatedData],'dir')
        mkdir([saveOpts.MainFolder saveOpts.SubFolders.ActivatedData]);
    end 
    %saveOpts
    if (P1D0.saveOpts ==0 && displayOpts.defaultParamSet)
        disp([displayOpts.ies{4} 'saveOpts is set to default(none).']);
    end
    %memoryMethod
    if (P1D0.memoryMethod==0)
        if (displayOpts.defaultParamSet)
            disp([displayOpts.ies{4} 'memoryMethod is set to default(all_in_memory).']);
        end
    end
end

% STEP-2 : Learn and set input and output sizes
function cnnnet = arrangeLayerSizes(cnnnet, layer, data)
    if (layer>1)
        % The previous layer can be-pooling-activation or even this can be initial layer
        % for pooling and convolution and fullyconnected
        if ~isfield(cnnnet{layer,1},'inputSize_original') && isfield(cnnnet{layer-1,1},'outputSize_original')
            cnnnet{layer,1}.inputSize_original = cnnnet{layer-1,1}.outputSize_original;
        end
    else
        dataSizeVec = size(data);
        if length(dataSizeVec)==4
            cnnnet{layer,1}.inputSize_original = dataSizeVec;
        else
            cnnnet{layer,1}.inputSize_original = [1 1 dataSizeVec];
        end
    end
    cnnnet{layer,1}.inputSize_vectorized = [prod(cnnnet{layer,1}.inputSize_original(1:end-1)) cnnnet{layer,1}.inputSize_original(end)];
end

% STEP-3 : Give the user information about what will be done by printing on the screen
function countSamples = displayProcessInformation(data, cnnnet, layer, displayOpts)
    countSamples = size(data, 2);
    if (displayOpts.calculation)
        if     (displayOpts.dataSize==1 && isfield(cnnnet{layer,1},'inputSize_original') && isfield(cnnnet{layer,1},'inputSize_vectorized') && isfield(cnnnet{layer,1},'sizeHid'))
                %for fullyconnected and softMax
                disp([displayOpts.ies{4} 'Data of size ' mat2str([cnnnet{layer,1}.inputSize_original(1:end-1) countSamples]) ' will be projected with ' mat2str([cnnnet{layer,1}.inputSize_vectorized(1) cnnnet{layer,1}.sizeHid]) ' sized weight matrix.']);
        elseif (displayOpts.dataSize==1 && isfield(cnnnet{layer,1},'inputSize_vectorized') && isfield(cnnnet{layer,1},'sizeHid'))
               disp([displayOpts.ies{4} 'Data of size ' mat2str([cnnnet{layer,1}.inputSize_vectorized(1) countSamples]) ' will be projected with ' mat2str([cnnnet{layer,1}.inputSize_vectorized(1) cnnnet{layer,1}.sizeHid]) ' sized weight matrix.']);
        elseif (displayOpts.dataSize==0)
               disp([displayOpts.ies{4} 'Data will be projected the weight matrix.']);
        end
    end
end

% STEP-4 : Save initial activation if needed
function cnnnet = saveInitialData(cnnnet, a, activationID, layerID, saveOpts, displayOpts, memoryMethod)
    % save the initial data as activations{1} to a file if a folder name is passed
    % if this is the first layer we need to store the initial data as first activation data (a{1})
    if (layerID==1 && strcmp(memoryMethod,'write_to_disk'))
        saveProceedBool = strcmp(saveOpts.Params.Activations,'Every') ||...
                          strcmp(saveOpts.Params.Activations,'First');%it must be either Every or First    
        if ~saveProceedBool
            return
        end
        saveActivation(a, saveOpts, activationID, layerID, displayOpts );
        if isfield(cnnnet{activationID,1},'activatedData')
            cnnnet{activationID,1} = rmfield(cnnnet{activationID,1},'activatedData');
        end
    elseif strcmp(memoryMethod,'all_in_memory')
        cnnnet{activationID,1}.activatedData = a;
    end
end

% STEP-5 : Apply connected layer
function [cnnnet, data] = proceedLayer(cnnnet, layer, data, displayOpts, saveOpts, deletePrevLayerBatch)
    time_spent_transform = cputime;
        [cnnnet{layer,1}, data] = fullyConnectedStep( cnnnet{layer,1}, data, displayOpts, saveOpts, deletePrevLayerBatch);    
    time_spent_transform = cputime - time_spent_transform;
    if     (displayOpts.calculation==1 && displayOpts.timeMode>=2)
        disp([displayOpts.ies{4}  'Data has been projected in ' num2str(time_spent_transform,'%4.2f') ' cputime.']);        
    elseif (displayOpts.calculation==1 && displayOpts.timeMode< 2)
        disp([displayOpts.ies{4} 'Data has been projected.']);        
    end
end

% STEP-6 : Write the timing information on screen
function time_spent_all = timingInfo(outputSize, time_spent_all, displayOpts)
    time_spent_all = cputime - time_spent_all;
    if     (displayOpts.dataSize==1 && displayOpts.timeMode>=2)
        disp([displayOpts.ies{4} 'After transformation data size has now become ' mat2str(outputSize) ' in ' num2str(time_spent_all) ' cputime.' ]);
    elseif (displayOpts.dataSize==1 && displayOpts.timeMode< 2)
        disp([displayOpts.ies{4} 'After transformation data size has now become ' mat2str(outputSize) '.' ]);
    end
end