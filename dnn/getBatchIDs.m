function [batchIDs, singleBatchSampleCountMax] = getBatchIDs(countSamples, batchSize)
       batchIDs = int32(linspace(1,countSamples,batchSize+1));
       batchIDs = [batchIDs(1:end-1)' batchIDs(2:end)'-1 (batchIDs(2:end)-batchIDs(1:end-1))'];
       batchIDs(end,:) = [batchIDs(end,1) countSamples countSamples-batchIDs(end,1)+1];
       singleBatchSampleCountMax = max(batchIDs(:,3));
end