function [ cnnnet, inSizeMB, outSizeMB ] = getCNNETreadyForNewData( cnnnet, fieldTreshold, inputSize_original, displayInfoLevel)
    inSizeMB = getVariableSize(cnnnet,'mb');
    if ~exist('displayInfoLevel','var')
        displayInfoLevel =0;
    end
    if ~exist('fieldTreshold','var')
        fieldTreshold = 2;
    end
    if ~exist('inputSize_original','var') || isempty(inputSize_original)
        inputSize_original = cnnnet{1,1}.inputSize_original;
    end
    layerCount = size(cnnnet,1);
    fieldsToDelete = {{'activatedData','W_ConvMap','W_Conv_Mapped','poolMap'},...
                      {'inputSize_original','inputSize_vectorized','outputSize_vectorized','outputSize_original','sizeChn'}};
    for i=1:layerCount
        %remove the params to re-learn
        for f = 1:fieldTreshold
            for j=1:length(fieldsToDelete{f})
                if isfield(cnnnet{i, 1},fieldsToDelete{f}{j})
                    if displayInfoLevel>=1
                        disp([fieldsToDelete{f}{j} '-is being deleted from layer(' num2str(i) ')']);
                    end
                    cnnnet{i, 1} = rmfield(cnnnet{i, 1},fieldsToDelete{f}{j});
                else
                    if displayInfoLevel>=2
                        disp([fieldsToDelete{f}{j} '-is not a member of layer(' num2str(i) ')']);
                    end
                end      
            end
        end
    end
    if fieldTreshold>=2
        cnnnet{1,1}.inputSize_original = inputSize_original;
    end
    outSizeMB = getVariableSize(cnnnet,'mb');
    if displayInfoLevel>=1
        disp(['inSizeMB(' num2str(inSizeMB,'%4.2f') ')--outSizeMB(' num2str(outSizeMB,'%4.2f') ')']);
    end
end

