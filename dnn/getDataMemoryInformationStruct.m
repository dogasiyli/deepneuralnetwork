function dMIS = getDataMemoryInformationStruct(X, inputSize_original, patchSize, inputChannelSize, countBatch, displayOnScreen, initialEmptyCharCount)
    if (~exist('displayOnScreen','var'))
        displayOnScreen = true;
    end
    if (~exist('initialEmptyCharCount','var'))
        initialEmptyCharCount = 0;
    end
    
    dMIS = struct;
    dMIS.countBatch = countBatch;

    dMIS.countSampleTotal = size(X,2)*dMIS.countBatch;
    dMIS.inputSizeTotal = [inputSize_original(1:end-1) dMIS.countSampleTotal];
    dMIS.patchSize = [patchSize inputChannelSize];
    dMIS.patchSizeTotal =  [dMIS.patchSize dMIS.countSampleTotal];
    
    
    [dMIS.mem2BC_singlePatchPerSample, dMIS.count_patchPerSample_maxPossible, dMIS.memAvailable, dMIS.memUsedByMatlab] = getPossibleMemSize( dMIS.patchSizeTotal, displayOnScreen, initialEmptyCharCount);
    dMIS.count_patchPerSample_planned = floor(dMIS.count_patchPerSample_maxPossible/4);
    dMIS.count_patchPerSample_planned = floor(dMIS.count_patchPerSample_planned/dMIS.countSampleTotal);
    dMIS.count_patchPerSample_planned = max(1,dMIS.count_patchPerSample_planned);
    dMIS.count_patchPerSample_strideOne = (inputSize_original(1)-patchSize(1)+1)*(inputSize_original(2)-patchSize(2)+1);
end