function [Welm, predictions, cmTrain, trainAccuracy, rankDeficient] = getELM_Weights_SoftMax(trainData, trainLabels, fullyConnectedWeights, displayOpts)
    %for the ELM approach to be more successful than softMax there are 2
    %things needed
    %1. rank(trainData)==size(trainData,1); %train data is assumed to be Nd
    %2. identityTest( pinvX*trainData, 10^-5 ); must not give warning (actually same as above)
    if ~exist('displayOpts','var') || isempty(displayOpts)
        displayOpts = setDisplayOpts('none');
    end
    rankTrain = rank(trainData);
    [countSamples,countFeat] = size(trainData);
    rankDeficient = (rankTrain<countFeat);
    if rankDeficient && displayOpts.warningInfo
        disp([displayOpts.ies{4} 'WARNING : Train data matrix is rank deficient. Meaning for ' num2str(countSamples,'%d') ' samples of train data <rank>' num2str(rankTrain,'%d') '<' num2str(countFeat) '<featureSize>']);
    end
    
    %H - the activations
    if min(size(trainLabels)) == 1
        Helm = full(sparse(trainLabels,1:length(trainLabels),ones(1,length(trainLabels))))';
        if exist('fullyConnectedWeights','var') && ~isempty(fullyConnectedWeights)
            %if pcaCoeff
            sigm = @(z_) 1.0 ./ (1.0 + exp(-z_));
            trainData = sigm(trainData*fullyConnectedWeights);
        end
        %we can relect the proper frames by
        %Helm = sigm(X*Welm);
        %What I need is a W such that after multiplying with gesture
        %samples it would give me this same classification result as H
        %use H as the H in ELM and calculate the weights
        Helm(Helm<1) = -1;
    else
        %Helm is passed - hidden node activations
        Helm = trainLabels;
        [~,trainLabels] = max(Helm,[],2);
    end

    %st = toc;disp(['Getting pseudo inverse of a ' num2str(size(trainData,1)) '_by_' num2str(size(trainData,2)) ' matrix.'])
    pinvX = pinv(trainData);%[dn = pinv(nd)]
    %en = toc;
    %disp(['Inverse completed in ' num2str(en-st,'%4.2f') ' seconds.']);
    if ~strcmp('none',displayOpts.displayLevel)
        disp([displayOpts.ies{4} 'Testing if it could calculate pseudo inverse of train data correctly']);
    end
    identityTest(pinvX*trainData, 10^-5, displayOpts); 
    %clear en st
    beta = pinvX*Helm;%[dh = dn*nh] --- beta = pinv(Helm)*X;%[hd = hn*nd]
    Welm = beta;%pinv(W_mem{3})';%
    
    %now lets test if Welm gives what we wanted
    %it practically should give us almost exactly the same labels as it was
    Helm = trainData*Welm;%[nh = nd*dh]
    Welm = 3*(Welm./max(Helm(:)));%makes Helm between almost 0 and 3
    
    if ~strcmp('none',displayOpts.displayLevel)
        disp('Train data analysis of ELM :');
    end
    %gestLabels_Helm, sampleCountTotal, sampleCountChanged, accuracyCalced, confMatResult
    [predictions, ~, ~, trainAccuracy, cmTrain] = analyzeActivationsElm(trainData, trainLabels, Welm, ~strcmp('none',displayOpts.displayLevel));
end