function [Welm, predictions, confMatTrain, accuracyTrain, rankDeficient] = getELM_Weights_SoftMax_V2(trainData, trainLabels, displayOpts)
    if (~exist('displayOpts','var'))
        displayOpts.warningInfo = 0;
        displayOpts.confusionMat = 'None';
    end
    %for the ELM approach to be more successful than softMax there are 2
    %things needed
    %1. rank(trainData)==size(trainData,2); %train data is assumed to be dN
    %2. identityTest( pinvX*trainData, 10^-5 ); must not give warning (actually same as above)
    rankTrain = rank(trainData');
    [countFeat,countSamples] = size(trainData);
    rankDeficient = (rankTrain<countFeat);
    if (rankDeficient && displayOpts.warningInfo==1)
        warning([displayOpts.ies{6} 'WARNING : Train data matrix is rank deficient. IOW-' num2str(countSamples,'%d') ' samples of train data rank(' num2str(rankTrain,'%d') ')<(' num2str(countFeat) ')featureSize']);
    end
    
    RidgeParam = 1e-8;
    Y = full(sparse(trainLabels,1:length(trainLabels),ones(1,length(trainLabels))))';
    Y(Y<1) = -1;
    A = trainData;
    Welm = (A*Y)'/(A*A'+RidgeParam*eye(countFeat)); %find output weights by solving the for regularised least mean square weights

    [~,predictions] = max(Welm*A);
    [accuracyTrain, confMatTrain] = calculateConfusion(predictions,trainLabels,~strcmp(displayOpts.confusionMat,'None'));
end