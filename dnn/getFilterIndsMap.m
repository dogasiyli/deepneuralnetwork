function [filterIndicesMap, filterCount, filterDimensionCount, filterTotalDimensionCount] = getFilterIndsMap(size_original)
    if length(size_original)==2
        %fully-connected
        filterCount = size_original(2);
        filterDimensionCount = 1;
    elseif length(size_original)==4
        %conv
        filterCount = size_original(3);
        filterDimensionCount = prod(size_original(1:2));
    end
    filterTotalDimensionCount = filterDimensionCount*filterCount;
    filterIndicesMap = 1:filterDimensionCount:(filterTotalDimensionCount+filterDimensionCount);
    filterIndicesMap = [filterIndicesMap(1:end-1)' filterIndicesMap(2:end)'-1 (filterIndicesMap(2:end)-filterIndicesMap(1:end-1))'];
end

