function [ RC_CurImage, leftTopMostPixIDs ] = getPatchPixIDs(patchInfoLSC, patchCount, patchSize, strideParam)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    %patchInfoLSC = Last Six Columns of the original
    % 5 6  1 and 2 are R and C of the real image that is padded with sth
    % 7    3 is the initial rowID where the real image reside
    % 8    4 is the    last rowID where the real image reside
    % 9    5 is the initial colID where the real image reside
    %10    6 is the    last colID where the real image reside

    if exist('patchCount','var') && ~isempty(patchCount)
        wR = patchInfoLSC(1)/patchInfoLSC(2);%rowSize_div_colSize weight of row
        %so patchCount is patchCount = pcR*pcC - so what are pcR and pcC
        allPossibleValuesOf_pC = getAllPossibleDivisors(patchCount);%all possible values for pC - patchCount Column
        pC_real = sqrt(patchCount/wR);
        [~, pC] = min(abs(allPossibleValuesOf_pC-pC_real));
        pC = allPossibleValuesOf_pC(pC);
        pR = patchCount/pC;

        %so horizontally image will be divided into pC pieces that touches on top of each other
        %vertically image will be divided into pR pieces that touches on top of each other

        r_CurImage = floor(linspace(patchInfoLSC(3),max(patchInfoLSC(3),patchInfoLSC(4)-patchSize(1)+1),pR));
        c_CurImage = floor(linspace(patchInfoLSC(5),max(patchInfoLSC(5),patchInfoLSC(6)-patchSize(2)+1),pC));
    elseif exist('strideParam','var') && ~isempty(strideParam)
        if length(strideParam)==1
            strideParam = [strideParam strideParam];
        end
        r_CurImage = patchInfoLSC(3):strideParam(1):patchInfoLSC(4)-patchSize(1)+1;
        c_CurImage = patchInfoLSC(5):strideParam(2):patchInfoLSC(6)-patchSize(2)+1;
    end
    
    
    
    [Ri,Ci] = meshgrid(r_CurImage,c_CurImage');
    RC_CurImage = [Ri(:),Ci(:)];%1st col rowIDs, 2nd col colIDs
    
    %row is smaller area
    rowsToDelete = RC_CurImage(:,1)<patchInfoLSC(:,3);
    assert(sum(rowsToDelete)==0,'no row should be deleted');
    %row is bigger area
    rowsToDelete = RC_CurImage(:,1)+patchSize(1)-1>patchInfoLSC(:,4);
    assert(sum(rowsToDelete)==0,'no row should be deleted');
    %col is smaller area
    rowsToDelete = RC_CurImage(:,2)<patchInfoLSC(:,5);
    assert(sum(rowsToDelete)==0,'no row should be deleted');
    %col is bigger area
    rowsToDelete = RC_CurImage(:,2)+patchSize(2)-1>patchInfoLSC(:,6);
    assert(sum(rowsToDelete)==0,'no row should be deleted');
    %RC_CurImage(rowsToDelete,:) = []; 
        
    if nargout>1
        leftTopMostPixIDs = sub2ind([patchInfoLSC(1),patchInfoLSC(2)],Ri(:), Ci(:));
    end
end

