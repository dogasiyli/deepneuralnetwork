function [ mem2BConsumed, maxSampleCountToFit, memAvail, memMatlab ] = getPossibleMemSize( dataSizeArr, displayOnScreen, initialEmptyCharCount)
    if (~exist('displayOnScreen','var'))
        displayOnScreen = true;
    end
    if (~exist('initialEmptyCharCount','var'))
        initialEmptyCharCount = 0;
    end
    [ memAvail, memMatlab ] = displayMemory(false);
    sampleCount = dataSizeArr(end);
    dataDimensionArr = dataSizeArr(1:end-1);
    dataDimensionProd = prod(dataDimensionArr);
    mem2BConsumed = (dataDimensionProd*sampleCount*8) / (1024*1024);
    maxSampleCountToFit = floor(sampleCount*(mod(memAvail,32*1024)/mem2BConsumed));
    maxMem2BConsumed = (dataDimensionProd*maxSampleCountToFit*8) / (1024*1024);
    if displayOnScreen
        initStrEmpty = '';
        if initialEmptyCharCount>0
            initStrEmpty = repmat(' ',1,initialEmptyCharCount);
        end
        disp([initStrEmpty 'Matlab uses(' num2str(memMatlab,'%4.2f') ' MB)-Available(' num2str(memAvail,'%4.2f') ' MB)'])
        disp([num2str(sampleCount) ' samples of ' mat2str(dataDimensionArr) ' will consume ' num2str(mem2BConsumed,'%6.4f') ' MB of memory.']);
        disp([num2str(maxSampleCountToFit) ' samples of ' mat2str(dataDimensionArr) ' will consume ' num2str(maxMem2BConsumed,'%6.4f') ' MB of memory.']);
    end    
end

