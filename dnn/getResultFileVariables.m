function [folderNames, fileNames, saveOpts] = getResultFileVariables( saveOpts, dataStruct, batchParams )
    saveOpts.SubFolders.WeightBiasComparisonTables = ['WeightBiasComparisonTables_deepNetTrainBP_epoch(' num2str(batchParams.maxIter) ')_iter(' num2str(batchParams.epochIter) ')' filesep];
    WeightBiasComparisonTablesSaveFolder = [saveOpts.MainFolder 'Train' filesep saveOpts.SubFolders.WeightBiasComparisonTables];
    i=1;
    while exist(WeightBiasComparisonTablesSaveFolder,'dir')
        saveOpts.SubFolders.WeightBiasComparisonTables = ['WeightBiasComparisonTables_epoch(' num2str(batchParams.maxIter) ')_iter(' num2str(batchParams.epochIter) ')_i(' num2str(i) ')' filesep];
        WeightBiasComparisonTablesSaveFolder = [saveOpts.MainFolder 'Train' filesep saveOpts.SubFolders.WeightBiasComparisonTables];
        i = i + 1;
    end
    mkdir(WeightBiasComparisonTablesSaveFolder);
    
    saveOpts.SubFolders.ResultTables = ['ResultTables_deepNetTrainBP_epoch(' num2str(batchParams.maxIter) ')_iter(' num2str(batchParams.epochIter) ')' filesep];
    ResultTablesSaveFolder = [saveOpts.MainFolder 'Train' filesep saveOpts.SubFolders.ResultTables];
    i=1;
    while exist(ResultTablesSaveFolder,'dir')
        saveOpts.SubFolders.ResultTables = ['ResultTables_deepNetTrainBP_epoch(' num2str(batchParams.maxIter) ')_iter(' num2str(batchParams.epochIter) ')_i(' num2str(i) ')' filesep];
        ResultTablesSaveFolder = [saveOpts.MainFolder 'Train' filesep saveOpts.SubFolders.ResultTables];
        i = i + 1;
    end
    clear i;

    folderNames = struct;
    folderNames.ResultTablesSaveFolder = struct;
    folderNames.ResultTablesSaveFolder.Train = ResultTablesSaveFolder;
        mkdir(folderNames.ResultTablesSaveFolder.Train);
    folderNames.ResultTablesSaveFolder.Valid = [saveOpts.MainFolder 'Valid' filesep saveOpts.SubFolders.ResultTables];
        mkdir(folderNames.ResultTablesSaveFolder.Valid);
    folderNames.ResultTablesSaveFolder.Test = [saveOpts.MainFolder 'Test' filesep saveOpts.SubFolders.ResultTables];
        mkdir(folderNames.ResultTablesSaveFolder.Test);
    
    folderNames.WeightBiasComparisonTablesSaveFolder_Train = WeightBiasComparisonTablesSaveFolder;
        
    fileNames = struct;
    fileNames.accPerBatch = struct;
    fileNames.accPerBatch.Train = [folderNames.ResultTablesSaveFolder.Train 'accPerBatchTrain.txt'];
    fileNames.accPerBatch.Valid = [folderNames.ResultTablesSaveFolder.Valid 'accPerBatchValid.txt'];
    fileNames.accPerBatch.Test = [folderNames.ResultTablesSaveFolder.Test 'accPerBatchTest.txt'];
    addline_resultepochfile(fileNames.accPerBatch.Train, 'train', dataStruct);
    addline_resultepochfile(fileNames.accPerBatch.Valid, 'valid', dataStruct);
    addline_resultepochfile(fileNames.accPerBatch.Test, 'test', dataStruct);
    
    fileNames.trainResultSummary = [folderNames.ResultTablesSaveFolder.Train 'trainResultSummary.txt'];
    addline_resultsummaryfile( fileNames.trainResultSummary);  
end

