function varSize = getVariableSize( variable, sizeType )
    b = whos('variable');
    switch sizeType
        case 'bytes'
            varSize = b.bytes;
        case 'kb'
            varSize = b.bytes/1024;
        case 'mb'
            varSize = b.bytes/(1024*1024);
        case 'gb'
            varSize = b.bytes/(1024*1024*1024);
        case 'tb'
            varSize = b.bytes/(1024*1024*1024*1024);
        otherwise
            varSize = b.bytes/(1024*1024);%return in mb
    end            
end