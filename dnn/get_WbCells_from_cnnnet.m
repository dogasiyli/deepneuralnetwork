function [W_cells, b_cells, weightDiffResults] = get_WbCells_from_cnnnet(cnnnet, W_grad, b_grad, displayOpts, saveOpts, dataStruct, curStep)  
    layerCount = size(cnnnet,1)+1;%for two network layers this is 3
    cellCount = layerCount - layerTypeCount(cnnnet, 'activation', false);
    cellIDCurrent = 0;
    nonEmptyWeightCellCount = 0;
    W_cells = cell(1,cellCount-1);
    b_cells = cell(1,cellCount-1);
    depth = size(cnnnet,1);
    weightDiffResults = [];
    for layer = 1:depth
        curCell = cnnnet{layer,1};
        switch curCell.type
            case 'convolution'
                cellIDCurrent = cellIDCurrent + 1;
                nonEmptyWeightCellCount = nonEmptyWeightCellCount + 1;
                W_cells{1,cellIDCurrent} = curCell.filtersWhitened;
                b_cells{1,cellIDCurrent} = curCell.biasesWhitened;
            case 'activation'
                %activation layers do not have weights and biases
            case 'pooling'
                cellIDCurrent = cellIDCurrent + 1;
                %pooling layers do not have weights and biases
            case 'softmax'
                cellIDCurrent = cellIDCurrent + 1;
                nonEmptyWeightCellCount = nonEmptyWeightCellCount + 1;
                W_cells{1,cellIDCurrent} = curCell.Weight;
                b_cells{1,cellIDCurrent} = reshape(curCell.BiasParams, curCell.countCategory, 1);
            case 'fullyconnected'
                cellIDCurrent = cellIDCurrent + 1;
                nonEmptyWeightCellCount = nonEmptyWeightCellCount + 1;
                W_cells{1,cellIDCurrent} = curCell.filtersWhitened;
                b_cells{1,cellIDCurrent} = curCell.biasesWhitened;
            otherwise
                error([curCell.type ' is not implemented yet'])
        end
    end
    
    r = 0;
    cellIDCurrent = 0;
    globalOutputStruct( 'updateWeightBiasDataTrack',[],'newRowAssign_wb',true);
    for layer = 1:depth
        curCell = cnnnet{layer,1};
        switch curCell.type
            case 'convolution'
                cellIDCurrent = cellIDCurrent + 1;
                r = r + 1;
                W = W_cells{1,cellIDCurrent};
                dW= W_grad{1,cellIDCurrent};
                b = b_cells{1,cellIDCurrent};
                db= b_grad{1,cellIDCurrent};
                globalOutputStruct( 'updateWeightBiasDataTrack',[],'layerType',curCell.type,'layerID',r,'W',W,'dW',dW,'b',b,'db',db);
            case 'pooling'
                cellIDCurrent = cellIDCurrent + 1;
                %pooling layers do not have weights and biases
            case 'softmax'
                cellIDCurrent = cellIDCurrent + 1;
                r = r + 1;
                W = W_cells{1,cellIDCurrent};
                dW= W_grad{1,cellIDCurrent};
                b = b_cells{1,cellIDCurrent};
                db= b_grad{1,cellIDCurrent};
                globalOutputStruct( 'updateWeightBiasDataTrack',[],'layerType',curCell.type,'layerID',r,'W',W,'dW',dW,'b',b,'db',db);
            case 'fullyconnected'
                cellIDCurrent = cellIDCurrent + 1;
                r = r + 1;
                W = W_cells{1,cellIDCurrent};
                dW= W_grad{1,cellIDCurrent};
                b = b_cells{1,cellIDCurrent};
                db= b_grad{1,cellIDCurrent};
                globalOutputStruct( 'updateWeightBiasDataTrack',[],'layerType',curCell.type,'layerID',r,'W',W,'dW',dW,'b',b,'db',db);
            otherwise
                %do nothing
        end
    end

    if saveOpts.Params.WeightDiff || ~strcmp(saveOpts.Figures.WeightDiff,'None')
        %figureOperations-01
        figureSaveFolder = [saveOpts.MainFolder saveOpts.SubFolders.WeightBiasComparisonImages];
        if (~exist(figureSaveFolder,'dir'))
            mkdir(figureSaveFolder);
        end
        r = 0;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %  weights  %  deltaWeights  %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %    bias   %    deltaBias   %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        cellIDCurrent = 0;
        
        %weight parameter operations-01
        if saveOpts.Params.WeightDiff
            weightDiffAppendStr = [num2str(curStep) ';'];
            weightDiffHeaderStr = 'StepID;';
        end
        
        %both figure and weight parameter operations-01
        for layer = 1:depth
            curCell = cnnnet{layer,1};
            plotAndSave = 0;
            switch curCell.type
                case 'convolution'
                    cellIDCurrent = cellIDCurrent + 1;
                    r = r + 1;
                    W = W_cells{1,cellIDCurrent};
                    dW= W_grad{1,cellIDCurrent};
                    b = b_cells{1,cellIDCurrent};
                    db= b_grad{1,cellIDCurrent};
                    if (~strcmp(saveOpts.Figures.WeightDiff,'None'))
                        h = figure(layerCount+r);clf;
                            rc = 2; cc = 2;
                            subplotW_conv(W, cnnnet{layer} ,rc, cc, 1, 1);
                            subplotdW_conv(dW, rc, cc, 1, 2);
                            subplot_b(b ,rc, cc, 2, 1);
                            subplot_db(db ,rc, cc, 2, 2);
                    end
                    plotAndSave = 1;
                case 'pooling'
                    cellIDCurrent = cellIDCurrent + 1;
                    %pooling layers do not have weights and biases
                case 'softmax'
                    cellIDCurrent = cellIDCurrent + 1;
                    r = r + 1;
                    W = W_cells{1,cellIDCurrent};
                    dW= W_grad{1,cellIDCurrent};
                    b = b_cells{1,cellIDCurrent};
                    db= b_grad{1,cellIDCurrent};
                    if (~strcmp(saveOpts.Figures.WeightDiff,'None'))
                        h = figure(layerCount+r);clf;
                        rc = 2; cc = 1;
                        if (~isempty(b) && ~isempty(db))
                            cc = 2;
                            subplot_b(b ,rc, cc, 2, 1);
                            subplot_db(db ,rc, cc, 2, 2);  
                        end
                        subplotW_softfull(W, curCell ,rc, cc, 1, 1);
                        subplotdW_softfull(dW ,rc, cc, 1, 2);
                    end
                    plotAndSave = 1;
                case 'fullyconnected'
                    cellIDCurrent = cellIDCurrent + 1;
                    r = r + 1;
                    W = W_cells{1,cellIDCurrent};
                    dW= W_grad{1,cellIDCurrent};
                    b = b_cells{1,cellIDCurrent};
                    db= b_grad{1,cellIDCurrent};
                    if (~strcmp(saveOpts.Figures.WeightDiff,'None'))
                        h = figure(layerCount+r);clf;
                        rc = 2; cc = 1;
                        if (~isempty(b) && ~isempty(db))
                            cc = 2;
                            subplot_b(b ,rc, cc, 2, 1);
                            subplot_db(db ,rc, cc, 2, 2);  
                        end
                        subplotW_softfull(W, curCell ,rc, cc, 1, 1);
                        subplotdW_softfull(dW ,rc, cc, 1, 2);
                    end
                    plotAndSave = 1;
                otherwise
                    %do nothing
            end
            
            %weight parameter operations-02
            if saveOpts.Params.WeightDiff && plotAndSave
                [weightDiffHeaderStr,weightDiffAppendStr] = getWbDiffStr(layer,W,dW,b,db,weightDiffHeaderStr,weightDiffAppendStr);
            end
            %figureOperations-02
            if (~strcmp(saveOpts.Figures.WeightDiff,'None') && plotAndSave==1)
                if (strcmp(saveOpts.Figures.WeightDiff,'Last'))
                    if (dataStruct.countBatch.train>1)
                        figureTitle = [num2str(layerCount+r) '-WeightBiasComparison_step(' num2str(curStep,'%03d') ')_batchID(' num2str(dataStruct.batchID_current,'%02d') ')_' curCell.name '.png'];
                        fileName2Save = [figureSaveFolder 'WeightBiasComparison_batchID(' num2str(dataStruct.batchID_current,'%02d') ')_' curCell.name '.png'];
                    else
                        figureTitle = [num2str(layerCount+r) '-WeightBiasComparison_step(' num2str(curStep,'%03d') ')_' curCell.name '.png'];
                        fileName2Save = [figureSaveFolder 'WeightBiasComparison_' curCell.name '.png'];             
                    end
                elseif (strcmp(saveOpts.Figures.WeightDiff,'Every'))
                    if (dataStruct.countBatch.train>1)
                        figureTitle = [num2str(layerCount+r) '-WeightBiasComparison_step(' num2str(curStep,'%03d') ')_batchID(' num2str(dataStruct.batchID_current,'%02d') ')_' curCell.name '.png'];
                        fileName2Save = [figureSaveFolder 'WeightBiasComparison_step(' num2str(curStep,'%03d') ')_batchID(' num2str(dataStruct.batchID_current,'%02d') ')_' curCell.name '.png'];
                    else
                        figureTitle = [num2str(layerCount+r) '-WeightBiasComparison_step(' num2str(curStep,'%03d') ')_' curCell.name '.png'];
                        fileName2Save = [figureSaveFolder 'WeightBiasComparison_step(' num2str(curStep,'%03d') ')_' curCell.name '.png'];            
                    end
                end

                if (displayOpts.fileOperations==1)
                    disp([displayOpts.ies{4} 'Figure with id(' num2str(layerCount+1) ') will be saved as (' fileName2Save ')']);
                end

                set(h,'name',figureTitle,'numbertitle','off');
                saveas(h,fileName2Save);

                if (displayOpts.fileOperations==1)
                    disp([displayOpts.ies{4} 'Figure with id(' num2str(layerCount+1) ') has been saved as (' fileName2Save ')']);
                end        
            end          
        end
        
        %weight parameter operations-03
        if saveOpts.Params.WeightDiff
            weightDiffResults.weightDiffAppendStr = weightDiffAppendStr(1:end-1);%remove the last ';'
            weightDiffResults.weightDiffHeaderStr = weightDiffHeaderStr(1:end-1);%remove the last ';'
            %so we need to write the values into the text file
            fileSaveFolder = [saveOpts.MainFolder saveOpts.SubFolders.WeightBiasComparisonTables];
            fileName2Save = [fileSaveFolder 'WeightBiasComparison.txt'];
            if ~exist(fileSaveFolder,'dir')
                mkdir(fileSaveFolder);
            end
            if exist(fileName2Save,'file')
                %the file exist so just append weightDiffAppendStr
                fid = fopen(fileName2Save, 'a');
                if (fid > 0)
                    fprintf(fid, '%s\n',weightDiffResults.weightDiffAppendStr);
                    fclose(fid);
                end           
            else
                %the file doesn't exist so append weightDiffHeaderStr before weightDiffAppendStr
                fid = fopen(fileName2Save, 'a');
                if (fid > 0)
                    fprintf(fid, '%s\n',weightDiffResults.weightDiffHeaderStr);
                    fprintf(fid, '%s\n',weightDiffResults.weightDiffAppendStr);
                    fclose(fid);
                end           
            end
        end
    end
end

function [weightDiffHeaderStr,weightDiffAppendStr] = getWbDiffStr(layer,W,dW,b,db,weightDiffHeaderStr,weightDiffAppendStr)
    lIS = ['L(' num2str(layer) ')'];%layerIdentifierStr
    if ~isempty(W)
        weightDiffStr_W  = [num2str(min( W(:)),'%6.4f') ';' num2str(max( W(:)),'%6.4f') ';' num2str(max( W(:))-min( W(:)),'%8.6e') ';'];
        weightDiffHeaderStr = [weightDiffHeaderStr lIS '-Wmin;' lIS '-Wmax;' lIS '-Wdif;']; % #ok<*AGROW>
        weightDiffAppendStr = [weightDiffAppendStr weightDiffStr_W];
    end
    if ~isempty(dW)
        weightDiffStr_dW = [num2str(min(dW(:)),'%8.6e') ';' num2str(max(dW(:)),'%8.6e') ';' num2str(max(dW(:))-min(dW(:)),'%8.6e') ';'];
        weightDiffHeaderStr = [weightDiffHeaderStr lIS '-dWmin;' lIS '-dWmax;' lIS '-dWdif;'];
        weightDiffAppendStr = [weightDiffAppendStr weightDiffStr_dW];
    end
    if ~isempty(b)
        weightDiffStr_b  = [num2str(min( b(:)),'%6.4f') ';' num2str(max( b(:)),'%6.4f') ';' num2str(max( b(:))-min( b(:)),'%8.6e') ';'];
        weightDiffHeaderStr = [weightDiffHeaderStr lIS '-bmin;' lIS '-bmax;' lIS '-bdif;'];
        weightDiffAppendStr = [weightDiffAppendStr weightDiffStr_b];
    end
    if ~isempty(db)
        weightDiffStr_db = [num2str(min(db(:)),'%8.6e') ';' num2str(max(db(:)),'%8.6e') ';' num2str( max(db(:))-min(db(:)),'%8.6e') ';'];
        weightDiffHeaderStr = [weightDiffHeaderStr lIS '-dbmin;' lIS '-dbmax;' lIS '-dbdif;'];
        weightDiffAppendStr = [weightDiffAppendStr weightDiffStr_db];
    end
end

function subplotW_conv(W, curCell ,rc, cc, r, c)
    subplotID = c + cc*(r-1);
    subplot(rc,cc,subplotID);imagesc(W);colorbar;
        set(gca,'XTick',ceil(linspace(1,size(W,2),min(8,size(W,2)))));
        set(gca,'YTick',ceil(linspace(1,size(W,1),min(8,size(W,1)))));
        xlabel(['Each weight parameter of a filter(' num2str(curCell.filterSize(1)) 'x' num2str(curCell.filterSize(2)) 'x' num2str(curCell.inputSize_original(3)) ')']);
        ylabel('Filters of next layer');
        titleStr_Weight = [curCell.name '-W_{minVal(' num2str(min(W(:)),'%6.4f') ')},W_{maxVal(' num2str(max(W(:)),'%6.4f') ')},W_{difVal(' num2str(max(W(:))-min(W(:)),'%8.6e') ')}'];
        title(titleStr_Weight);
end

function subplot_b(b ,rc, cc, r, c)
    subplotID = c + cc*(r-1);
    subplot(rc,cc,subplotID);imagesc(b);colorbar;
        set(gca,'YTick',ceil(linspace(1,size(b,1),min(8,size(b,1)))));
        set(gca,'XTick',[]);
        ylabel('Bias parameters');
        titleStr_b = ['b_{minVal(' num2str(min(b(:)),'%6.4f') ')},b_{maxVal(' num2str(max(b(:)),'%6.4f') ')},b_{difVal(' num2str(max(b(:))-min(b(:)),'%8.6e') ')}'];
        title(titleStr_b);
end

function subplotdW_conv(dW ,rc, cc, r, c)
    subplotID = c + cc*(r-1);
    subplot(rc,cc,subplotID);imagesc(dW);colorbar;
        set(gca,'XTick',ceil(linspace(1,size(dW,2),min(8,size(dW,2)))));
        set(gca,'YTick',ceil(linspace(1,size(dW,1),min(8,size(dW,1)))));
        xlabel('deltaWeight(dW)');
        titleStr_Weight = ['dW_{minVal(' num2str(min(dW(:)),'%8.6e') ')},dW_{maxVal(' num2str(max(dW(:)),'%8.6e') ')},dW_{difVal(' num2str(max(dW(:))-min(dW(:)),'%8.6e') ')}'];
        title(titleStr_Weight);
end

function subplot_db(db ,rc, cc, r, c)
    subplotID = c + cc*(r-1);
    subplot(rc,cc,subplotID);imagesc(db);colorbar;
        set(gca,'YTick',ceil(linspace(1,size(db,1),min(8,size(db,1)))));
        set(gca,'XTick',[]);
        ylabel('delta Bias parameters');
        titleStr_db = ['db_{minVal(' num2str(min(db(:)),'%8.6e') ')},db_{maxVal(' num2str(max(db(:)),'%8.6e') ')},db_{difVal(' num2str(max(db(:))-min(db(:)),'%8.6e') ')}'];
        title(titleStr_db);
end

function subplotW_softfull(W, curCell ,rc, cc, r, c)
    subplotID = c + cc*(r-1);
    subplot(rc,cc,subplotID);imagesc(W);colorbar;
        set(gca,'XTick',ceil(linspace(1,size(W,2),min(8,round(size(W,2)/4)))));
        set(gca,'YTick',ceil(linspace(1,size(W,1),min(8,size(W,1)))));
        xlabel(['Previous Layer Dimension(' num2str(size(W,2)) ')']);
        ylabel(['Next Layer Dimension(' num2str(size(W,1)) ')']);
        titleStr_Weight = [curCell.name '-W_{minVal(' num2str(min(W(:)),'%6.4f') ')},W_{maxVal(' num2str(max(W(:)),'%6.4f') ')},W_{difVal(' num2str(max(W(:))-min(W(:)),'%8.6e') ')}'];
        title(titleStr_Weight);
end

function subplotdW_softfull(dW ,rc, cc, r, c)
    subplotID = c + cc*(r-1);
    subplot(rc,cc,subplotID);imagesc(dW);colorbar;
        set(gca,'XTick',ceil(linspace(1,size(dW,2),min(8,round(size(dW,2)/4)))));
        set(gca,'YTick',ceil(linspace(1,size(dW,1),min(8,size(dW,1)))));
        xlabel('deltaWeight(dW)');
        titleStr_Weight = ['dW_{minVal(' num2str(min(dW(:)),'%8.6e') ')},dW_{maxVal(' num2str(max(dW(:)),'%8.6e') ')},dW_{difVal(' num2str(max(dW(:))-min(dW(:)),'%8.6e') ')}'];
        title(titleStr_Weight);
end
