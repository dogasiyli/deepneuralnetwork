function globalOutputStruct(operationIdentifier,rowOfParams,varargin) %#ok<INUSL>
    global outputMatrixStructCNN;
    switch operationIdentifier
        case 'initialize'%done 20160816
            %{called from :runExample_Func}
            %globalOutputStruct('initialize',[],varargin{:})
            initializeGlobalOutputStruct(varargin{:});
        otherwise
            if isempty(outputMatrixStructCNN)
                return
            end
            try
                %updateMinFuncLog-%{called from :softMax_Init, softMax_Cumulative, deepNetTrainBP}
                %globalOutputStruct('updateMinFuncLog',rowOfParams)
                %globalOutputStruct('updateEpochBatchParams',rowOfParamsStruct)
                %globalOutputStruct('updateWeightBiasDataTrack',[],varargin{:})
                eval([operationIdentifier '(rowOfParams,varargin{:});']);
            catch
            end
    end
end

function initializeGlobalOutputStruct(varargin)
%{called from :runExample_Func}
    global outputMatrixStructCNN;
    global dropboxPath;
    c = clock;% [year month day hour minute seconds]-->
    eval('outputMatrixStructCNN = [];');
    
    [ categoriesToLoad,  curProjCode,  softMaxInitialIteration,  softMaxOptimizeMaxIteration,   batchParamsMode,  architectureIdentifier,  dataSetName,  commitCode,  exampleID,  batchInfo,  cnnnet ] = parseArgs(...
    {'categoriesToLoad' 'curProjCode' 'softMaxInitialIteration' 'softMaxOptimizeMaxIteration'  'batchParamsMode' 'architectureIdentifier' 'dataSetName' 'commitCode' 'exampleID' 'batchInfo' 'cnnnet'},...
    {        []         'curProjCode'            []                           []                       []                    ''                 ''           ''           0          []         []   },...
    varargin{:});


    outputMatrixStructCNN = struct;
    %% General Info
    outputMatrixStructCNN.generalInfo = struct;
    %
    if ~isempty(categoriesToLoad)
        categoryStr = ['-C(' strrep(strrep(strrep(mat2str(categoriesToLoad),' ','_'),'[',''),']','') ')'];
    else
        categoryStr = '';
    end
    dropboxSubFolderName = [curProjCode '-'  'cC(' commitCode ')' categoryStr '-exID(' num2str(exampleID) ')\'];
    outputMatrixStructCNN.generalInfo.dropboxSubFolderName = dropboxSubFolderName;
    dropboxFolderFullName = [dropboxPath outputMatrixStructCNN.generalInfo.dropboxSubFolderName];
    if ~exist(dropboxFolderFullName,'dir')
        mkdir(dropboxFolderFullName);
    else
        try %#ok<TRYNC>
            rmdir(dropboxFolderFullName,'s');
        end
        mkdir(dropboxFolderFullName);
    end
    outputMatrixStructCNN.generalInfo.arcName = architectureIdentifier;
    outputMatrixStructCNN.generalInfo.categoriesToLoad = categoriesToLoad;
    
    outputMatrixStructCNN.generalInfo.curProjCode = curProjCode;
    %
    outputMatrixStructCNN.generalInfo.dataInfo_Name = dataSetName;%'MNIST'
    if ~isempty(batchInfo)
        outputMatrixStructCNN.generalInfo.dataInfo_Train = [num2str(sum(batchInfo.image_Count_Matrix_Train(:))) ' in ' num2str(batchInfo.batch_Count_Train) ' batches.'];%57000 in 7 batches
        outputMatrixStructCNN.generalInfo.dataInfo_Valid = [num2str(sum(batchInfo.image_Count_Matrix_Valid(:))) ' in ' num2str(batchInfo.batch_Count_Valid) ' batches.'];%3000 in 1 batch
        outputMatrixStructCNN.generalInfo.dataInfo_Test = [num2str(sum(batchInfo.image_Count_Matrix_Test(:))) ' in ' num2str(batchInfo.batch_Count_Test) ' batches.'];%10000 in 2 batches
    else
        outputMatrixStructCNN.generalInfo.dataInfo_Train = '?';%57000 in 7 batches
        outputMatrixStructCNN.generalInfo.dataInfo_Valid = '?';%3000 in 1 batch
        outputMatrixStructCNN.generalInfo.dataInfo_Test = '?';%10000 in 2 batches
    end
    %
    outputMatrixStructCNN.generalInfo.softInit_StepCount = softMaxInitialIteration;
    outputMatrixStructCNN.generalInfo.softInit_InitCost = [];%
    outputMatrixStructCNN.generalInfo.softInit_InitAcc = [];%
    outputMatrixStructCNN.generalInfo.softInit_FinalCost = [];%
    outputMatrixStructCNN.generalInfo.softInit_FinalAcc = [];%
    %
    outputMatrixStructCNN.generalInfo.softLastLayer_StepCount = softMaxOptimizeMaxIteration;
    outputMatrixStructCNN.generalInfo.softLastLayer_InitCost = [];%
    outputMatrixStructCNN.generalInfo.softLastLayer_InitAcc = [];%
    outputMatrixStructCNN.generalInfo.softLastLayer_FinalCost = [];%
    outputMatrixStructCNN.generalInfo.softLastLayer_FinalAcc = [];%
    %
    outputMatrixStructCNN.generalInfo.batch_Mode = batchParamsMode;%1-2-3-4
    outputMatrixStructCNN.generalInfo.batch_EpochCnt = [];
    outputMatrixStructCNN.generalInfo.batch_IterPerEpochCnt = [];    
    %
    outputMatrixStructCNN.generalInfo.computerName = getComputerName;
    outputMatrixStructCNN.generalInfo.dateStart = c;
    outputMatrixStructCNN.generalInfo.dateStartStr = sprintf('%04d%02d%02d-%02d:%02d:%02d',c(1),c(2),c(3),c(4),c(5),round(c(6)));
    outputMatrixStructCNN.generalInfo.dateEnd = '';%c = clock; % [year month day hour minute seconds]
    outputMatrixStructCNN.generalInfo.dateEndStr = '';%sprintf('%04d%02d%02d-%02d:%02d:%02d',c(1),c(2),c(3),c(4),c(5),round(c(6)));
    outputMatrixStructCNN.generalInfo.totalTime = '';%dateEnd-dateStart
    outputMatrixStructCNN.generalInfo.commitCode = commitCode;
    outputMatrixStructCNN.generalInfo.exampleID = exampleID;
    

    %% Iteration Info
    outputMatrixStructCNN.iterationInfo = struct;
    outputMatrixStructCNN.iterationInfo.generalType = '';%{possibleValues:softMax_Init, softMax_Cumulative, deepNetTrainBP}
    outputMatrixStructCNN.iterationInfo.batchID = 0;%{updateWhen calling:".updateDataStruct("}
    outputMatrixStructCNN.iterationInfo.batchCount = 0;%{updateWhen calling:".updateDataStruct("}
    outputMatrixStructCNN.iterationInfo.batchType = '';%{updateWhen calling:".updateDataStruct("}{possibleValues:'train','valid','test'}
    outputMatrixStructCNN.iterationInfo.epochID_main = 0;%{updateIn:deepNetTrainBP}
    outputMatrixStructCNN.iterationInfo.epochID_sub = 0;%{updateIn:optimize_SoftMaxLayer}
    
    
    %% WeightBiasDataTrack Params
    if ~isempty(cnnnet)
        createWeightBiasDataSummary(cnnnet);
        createWeightBiasDataTrack(cnnnet);
    else
        outputMatrixStructCNN.weightBiasDataSummary = [];
        outputMatrixStructCNN.weightBiasDataTrack = [];
    end
    
    %% FilterStatsTrack Params
    outputMatrixStructCNN.filterStatsTrack = struct;
    outputMatrixStructCNN.filterStatsTrack.excelColNames = strsplit('epochID;batchType;batchID;insertMode;filterID;min;mean;max;variance',';');
    
    %minFuncLog results
    createMinFuncLogStruct;
    
    %%
    createAccPerBatch(batchInfo);
end

%sheet-2
function createMinFuncLogStruct
    global outputMatrixStructCNN;
    %minfunc can be called several times
    %from softMaxInit with the initial train batch
    %from softMaxAll as in groups of 10 times of 40 iteration or etc. with
    %the final layer train data of all batches
    %from deepNetTrainBP as in epochID and BatchID
    %1.softMaxInit - epochID = 0 0, batchID=lastBatch
    %2.softMaxAll - epochID = 0 epochID_sub, batchID=0
    %3.deepNetTainBP - epochID = epochID_main epochCnt, batchID = b
    %so we need 3 columns for epochID
    minFuncRowCounter = 1;%this info will go to SNu in the first column 
    
    outputMatrixStructCNN.minFuncLog = struct;
    outputMatrixStructCNN.minFuncLog.minFuncLogCols = strsplit('SNu;EpochIDMain;EpochID2;BatchID;Timestamp;Iteration;FunEvals;StepLength;FunctionVal;OptCond;Accuracy',';');
    outputMatrixStructCNN.minFuncLog.minFuncRowCounter = minFuncRowCounter;
    outputMatrixStructCNN.minFuncLog.resultMatrix = zeros(1,11);
end
function updateMinFuncLog(rowOfParams_5to11) %#ok<DEFNU>
    %global dropboxPath;
    global outputMatrixStructCNN;
    %Called From:
    %1. minFunc_Doga-->outputInfo
    checkToPause([mfilename '-updateMinFuncLog'],[],'(test first time)');
    assert(length(rowOfParams_5to11)==7,'7 values needed to update minfuncLogParams');
    sNu_01 = outputMatrixStructCNN.minFuncLog.minFuncRowCounter;
    epochIDMain_02 = outputMatrixStructCNN.iterationInfo.epochID_main;
    EpochID2_03 = outputMatrixStructCNN.iterationInfo.epochID_sub;
    batchID_04 = outputMatrixStructCNN.iterationInfo.batchID;
    rowOfParams_All = [sNu_01 epochIDMain_02 EpochID2_03 batchID_04 reshape(rowOfParams_5to11,1,[])];
    outputMatrixStructCNN.minFuncLog.resultMatrix(outputMatrixStructCNN.minFuncLog.minFuncRowCounter,:) = rowOfParams_All;
    outputMatrixStructCNN.minFuncLog.minFuncRowCounter = outputMatrixStructCNN.minFuncLog.minFuncRowCounter + 1;
    
    %writeToExcelFile( [dropboxPath outputMatrixStructCNN.generalInfo.dropboxSubFolderName 'minFuncLog.xlsx'], outputMatrixStructCNN.minFuncLog.resultMatrix, 'columnNamesCells', outputMatrixStructCNN.minFuncLog.minFuncLogCols);
    %writeToExcelFile( [dropboxPath outputMatrixStructCNN.generalInfo.dropboxSubFolderName 'allLogs.xlsx'], outputMatrixStructCNN.minFuncLog.resultMatrix, 'sheetID', 2, 'columnNamesCells', outputMatrixStructCNN.minFuncLog.minFuncLogCols);
end

function updateEpochBatchParams(rowOfParamsStruct) %#ok<DEFNU>
    global outputMatrixStructCNN;
    checkToPause([mfilename '-updateEpochBatchParams'],[],'(test first time)');
    if isfield(rowOfParamsStruct,'batchID')
        %{deepNetCostFunc->iterateAllBatches,deepNetTrainBP,deepNet_InitialTrain,deepNet_InitialTrain_MultipleBatches}
        outputMatrixStructCNN.iterationInfo.batchID = rowOfParamsStruct.batchID;%{updateWhen calling:".updateDataStruct("}
    end
    if isfield(rowOfParamsStruct,'batchCount')
        %{deepNetCostFunc->iterateAllBatches,deepNetTrainBP,deepNet_InitialTrain,deepNet_InitialTrain_MultipleBatches}
        outputMatrixStructCNN.iterationInfo.batchCount = rowOfParamsStruct.batchCount;%{updateWhen calling:".updateDataStruct("}
    end
    if isfield(rowOfParamsStruct,'batchType')
        %{deepNetCostFunc->iterateAllBatches,deepNetTrainBP,deepNet_InitialTrain,deepNet_InitialTrain_MultipleBatches}
        outputMatrixStructCNN.iterationInfo.batchType = rowOfParamsStruct.batchType;%{updateWhen calling:".updateDataStruct("}{possibleValues:'train','valid','test'}
    end
    if isfield(rowOfParamsStruct,'epochID_main')
        %{deepNetTrainBP}
        outputMatrixStructCNN.iterationInfo.epochID_main = rowOfParamsStruct.epochID_main;%{updateIn:deepNetTrainBP}
    end
    if isfield(rowOfParamsStruct,'epochID_sub')
        %{deepNetTrainBP}
        outputMatrixStructCNN.iterationInfo.epochID_sub = rowOfParamsStruct.epochID_sub;%{updateIn:optimize_SoftMaxLayer}
    end
    if isfield(rowOfParamsStruct,'generalType')
        %{updateIn:deepNet_InitialTrain(softMax_Init),optimize_SoftMaxLayer(softMax_Cumulative),deepNetTrainBP(deepNetTrainBP)}
        outputMatrixStructCNN.iterationInfo.generalType = rowOfParamsStruct.generalType;
    end
end

%sheet-4
function createWeightBiasDataSummary(cnnnet)
    global outputMatrixStructCNN;
    rowCnt = 0;
    colCnt = 11;
    weightBiasDataSummaryMat = NaN(2,colCnt);
    layerCount = size(cnnnet,1);
    for layer = 1:layerCount
        switch cnnnet{layer,1}.type
            case 'convolution'
                weightBiasDataSummaryMat(rowCnt+1:rowCnt+2,1)=layer;
                weightBiasDataSummaryMat(rowCnt+1:rowCnt+2,2)=1;%for convolution
                weightBiasDataSummaryMat(rowCnt+1:rowCnt+2,3)=[0;3];%for initial and final
                rowCnt = rowCnt+2;
            case 'activation'
                weightBiasDataSummaryMat(rowCnt+1:rowCnt+2,1)=layer;
                weightBiasDataSummaryMat(rowCnt+1:rowCnt+2,2)=2;%for activation
                weightBiasDataSummaryMat(rowCnt+1:rowCnt+2,3)=[0;3];%for initial and final
                rowCnt = rowCnt+2;
            case 'pooling'
                weightBiasDataSummaryMat(rowCnt+1:rowCnt+2,1)=layer;
                weightBiasDataSummaryMat(rowCnt+1:rowCnt+2,2)=3;%for pooling
                weightBiasDataSummaryMat(rowCnt+1:rowCnt+2,3)=[0;3];%for initial and final
                rowCnt = rowCnt+2;
            case 'fullyconnected'
                weightBiasDataSummaryMat(rowCnt+1:rowCnt+2,1)=layer;
                weightBiasDataSummaryMat(rowCnt+1:rowCnt+2,2)=4;%for fullyConnected
                weightBiasDataSummaryMat(rowCnt+1:rowCnt+2,3)=[0;3];%for initial and final
                rowCnt = rowCnt+2;
            case 'softmax'
                weightBiasDataSummaryMat(rowCnt+1:rowCnt+4,1)=layer;
                weightBiasDataSummaryMat(rowCnt+1:rowCnt+4,2)=5;%for softMax
                weightBiasDataSummaryMat(rowCnt+1:rowCnt+4,3)=[0;1;2;3];%for initial,smi,sma,final
                rowCnt = rowCnt+4;
            otherwise
                error('You are suggesting a layer that is not present.Layers are - convolution, activation, pooling, softmax');
        end
    end
    outputMatrixStructCNN.weightBiasDataSummary = weightBiasDataSummaryMat;%only about weights and data outputs
end

%sheet-3(dataTrack),4(weightBiasTrack)
function createWeightBiasDataTrack(cnnnet)
    global outputMatrixStructCNN;
    rowCnt = 1;
    colCnt_D = 3;
    colCnt_W = 3;
    dataTrackMat = NaN(2,colCnt_D);
    weightBiasDataTrackMat = NaN(2,colCnt_W);
    layerCount = size(cnnnet,1);
    columnNamesCells_data = {'eID','eCnt','bID'};
    columnNamesCells_wb = {'eID','eCnt','bID'};
    %columnNamesCells_weightBiasDataTrackMat = setIterativeCellStr( 'Tr', outputMatrixStructCNN.accPerBatch.batchCntTr);

    for layer = 1:layerCount
        switch cnnnet{layer,1}.type
            case {'convolution','fullyconnected','softmax'}
                dataTrackMat(rowCnt,colCnt_D+(1:3))=layer;
                colCnt_D = colCnt_D + 3;
                columnNamesCells_data_add = {[cnnnet{layer,1}.name '(min)'],[cnnnet{layer,1}.name '(mean)'],[cnnnet{layer,1}.name '(max)']};
                
                weightBiasDataTrackMat(rowCnt,colCnt_W+(1:8))=layer;
                colCnt_W = colCnt_W + 8;
                layerStr = num2str(layer);
                columnNamesCells_wb_add = {[layerStr 'Wmin'],[layerStr 'Wmax'],[layerStr 'dWmin'],[layerStr 'dWmax'],[layerStr 'bmin'],[layerStr 'bmax'],[layerStr 'dbmin'],[layerStr 'dbmax']};
                columnNamesCells_wb = {columnNamesCells_wb{:} columnNamesCells_wb_add{:}}; %#ok<CCAT>
            case {'activation','pooling'}
                dataTrackMat(rowCnt,colCnt_D+(1:3))=layer;
                colCnt_D = colCnt_D + 3;
                columnNamesCells_data_add = {[cnnnet{layer,1}.name '(min)'],[cnnnet{layer,1}.name '(mean)'],[cnnnet{layer,1}.name '(max)']};
            otherwise
                error('You are suggesting a layer that is not present.Layers are - convolution, activation, pooling, softmax');
        end
        columnNamesCells_data = {columnNamesCells_data{:} columnNamesCells_data_add{:}}; %#ok<CCAT>
    end
    outputMatrixStructCNN.weightBiasDataTrack.dataMat = dataTrackMat;%only about data outputs
    outputMatrixStructCNN.weightBiasDataTrack.wbMat = weightBiasDataTrackMat;%only about weights and bias
    outputMatrixStructCNN.weightBiasDataTrack.rowID_data = 1;
    outputMatrixStructCNN.weightBiasDataTrack.rowID_wb = 1;
    outputMatrixStructCNN.weightBiasDataTrack.columnNamesCells_data = columnNamesCells_data_add;
    outputMatrixStructCNN.weightBiasDataTrack.columnNamesCells_wb = columnNamesCells_wb;
end
function updateWeightBiasDataTrack(rowOfParams, varargin) %#ok<INUSL,DEFNU>
    %global dropboxPath;
    global outputMatrixStructCNN;
    [ layerType,  layerID,  actData,  W,  dW,  b,  db,  newRowAssign_data,  newRowAssign_wb ] = parseArgs(...
    {'layerType' 'layerID' 'actData' 'W' 'dW' 'b' 'db' 'newRowAssign_data' 'newRowAssign_wb'},...
    {    ''         []         []    []   []  []   []         false              false      },...
    varargin{:});

    dataTrackMat = outputMatrixStructCNN.weightBiasDataTrack.dataMat;
    weightBiasTrackMat = outputMatrixStructCNN.weightBiasDataTrack.wbMat;
    rowID_data = outputMatrixStructCNN.weightBiasDataTrack.rowID_data;
    rowID_wb = outputMatrixStructCNN.weightBiasDataTrack.rowID_wb;
    if ~strcmp(layerType,'')
        %forwardPropogateDeepNet->loopOverAllLayers
        %get_WbCells_from_cnnnet for weight and bias
        if ~isempty(actData)
            colIDs = 3*layerID + (1:3);
            dataTrackMat(rowID_data,colIDs)= [min(actData(:)) mean(actData(:)) max(actData(:))];
        end
        if ~isempty(W) && ~isempty(dW)
            colIDs = 3+8*(layerID-1) + (1:8);%minW1,maxW2,mindeltaW3,maxdeltaW4,minb5,maxb6,mindeltab7,maxdeltab8
            weightBiasTrackMat(rowID_wb,colIDs(1:4))= [min(W(:)) max(W(:)) min(dW(:)) max(dW(:))];
        end
        if ~isempty(b) && ~isempty(db)
            colIDs = 3+8*(layerID-1) + (1:8);%minW1,maxW2,mindeltaW3,maxdeltaW4,minb5,maxb6,mindeltab7,maxdeltab8
            weightBiasTrackMat(rowID_wb,colIDs(5:8))= [min(b(:)) max(b(:)) min(db(:)) max(db(:))];
        end
        switch layerType
            case {'convolution','activation','pooling','fullyconnected','softmax'}
            otherwise
                error('You are suggesting a layer that is not present.Layers are - convolution, activation, pooling, softmax');
        end
    elseif newRowAssign_data
        %forwardPropogateDeepNet
        rowID_data = rowID_data + 1;
        colIDs = 1:3;
        epochIDMain = outputMatrixStructCNN.iterationInfo.epochID_main;
        epochIDSub = outputMatrixStructCNN.iterationInfo.epochID_sub;
        batchID = outputMatrixStructCNN.iterationInfo.batchID;
        dataTrackMat(rowID_data,colIDs)= [epochIDMain epochIDSub batchID];
        %writeToExcelFile( [dropboxPath outputMatrixStructCNN.generalInfo.dropboxSubFolderName 'allLogs.xlsx'], dataTrackMat, 'sheetID', 3, 'columnNamesCells', outputMatrixStructCNN.weightBiasDataTrack.columnNamesCells_data);
    elseif newRowAssign_wb
        %forwardPropogateDeepNet
        rowID_wb = rowID_wb + 1;
        colIDs = 1:3;
        epochIDMain = outputMatrixStructCNN.iterationInfo.epochID_main;
        epochIDSub = outputMatrixStructCNN.iterationInfo.epochID_sub;
        batchID = outputMatrixStructCNN.iterationInfo.batchID;
        weightBiasTrackMat(rowID_wb,colIDs)= [epochIDMain epochIDSub batchID];
        %writeToExcelFile( [dropboxPath outputMatrixStructCNN.generalInfo.dropboxSubFolderName 'allLogs.xlsx'], weightBiasTrackMat, 'sheetID', 4, 'columnNamesCells', outputMatrixStructCNN.weightBiasDataTrack.columnNamesCells_wb);
    end
    outputMatrixStructCNN.weightBiasDataTrack.dataMat = dataTrackMat;%only about weights and data outputs
    outputMatrixStructCNN.weightBiasDataTrack.wbMat = weightBiasTrackMat;
    outputMatrixStructCNN.weightBiasDataTrack.rowID_data = rowID_data;
    outputMatrixStructCNN.weightBiasDataTrack.rowID_wb = rowID_wb;
end

function assignSoftMaxCostAcc(rowOfParamsStruct) %#ok<DEFNU>
    global outputMatrixStructCNN;
    %rowOfParamsStruct.initVSfinal = 'Init' or 'Final'
    %rowOfParamsStruct.cost
    %rowOfParamsStruct.acc
    checkToPause([mfilename '-assignSoftMaxCostAcc'],[],'(test first time)');
    
    if (isfield(rowOfParamsStruct,'initVSfinal') && (isfield(rowOfParamsStruct,'cost') || isfield(rowOfParamsStruct,'acc')))
        switch outputMatrixStructCNN.iterationInfo.generalType
            case 'softMax_Init'
                initVSlastLayer = 'softInit';
            case 'softMax_Cumulative'
                initVSlastLayer = 'softLastLayer';
            otherwise
                warning('there seems to be a problem!!');
                return
        end
        if isfield(rowOfParamsStruct,'cost')
            eval(['outputMatrixStructCNN.generalInfo.' initVSlastLayer '_' rowOfParamsStruct.initVSfinal 'Cost = rowOfParamsStruct.cost;']);
        end
        if isfield(rowOfParamsStruct,'acc')
            eval(['outputMatrixStructCNN.generalInfo.' initVSlastLayer '_' rowOfParamsStruct.initVSfinal 'Acc = rowOfParamsStruct.acc;']);
        end
        % outputMatrixStructCNN.generalInfo.softInit_InitCost = [];%
        % outputMatrixStructCNN.generalInfo.softInit_InitAcc = [];%
        % outputMatrixStructCNN.generalInfo.softInit_FinalCost = [];%
        % outputMatrixStructCNN.generalInfo.softInit_FinalAcc = [];%
        % outputMatrixStructCNN.generalInfo.softLastLayer_InitCost = [];%
        % outputMatrixStructCNN.generalInfo.softLastLayer_InitAcc = [];%
        % outputMatrixStructCNN.generalInfo.softLastLayer_FinalCost = [];%
        % outputMatrixStructCNN.generalInfo.softLastLayer_FinalAcc = [];%
    end
end

%sheet-1
function createAccPerBatch(batchInfo)
    global outputMatrixStructCNN;
    %epochID_main, epochID_sub
    colCntTo = 2;

    outputMatrixStructCNN.accPerBatch = struct;
    outputMatrixStructCNN.accPerBatch.rowID =  1;
    outputMatrixStructCNN.accPerBatch.batchCntTr = batchInfo.batch_Count_Train;
    outputMatrixStructCNN.accPerBatch.batchCntVa = batchInfo.batch_Count_Valid;
    outputMatrixStructCNN.accPerBatch.batchCntTe = batchInfo.batch_Count_Test;
    
    %Train
    colCntFr = colCntTo+1;
    if outputMatrixStructCNN.accPerBatch.batchCntTr>1
        colCntTo = colCntFr+outputMatrixStructCNN.accPerBatch.batchCntTr+1;%1 more for mean and 1 for ELM
    else
        colCntTo = colCntFr+1;%1 more ELM
    end
    outputMatrixStructCNN.accPerBatch.batchColumnsTr = colCntFr:colCntTo;
    %Valid
    colCntFr = colCntTo+1;
    if outputMatrixStructCNN.accPerBatch.batchCntVa>1
        colCntTo = colCntFr+outputMatrixStructCNN.accPerBatch.batchCntVa+1;%1 more for mean and 1 for ELM
    else
        colCntTo = colCntFr+1;%1 more ELM
    end
    outputMatrixStructCNN.accPerBatch.batchColumnsVa = colCntFr:colCntTo;
    %Test
    colCntFr = colCntTo+1;
    if outputMatrixStructCNN.accPerBatch.batchCntTe>1
        colCntTo = colCntFr+outputMatrixStructCNN.accPerBatch.batchCntTe+1;%%1 more for mean and 1 for ELM
    else
        colCntTo = colCntFr+1;%1 more ELM
    end
    outputMatrixStructCNN.accPerBatch.batchColumnsTe = colCntFr:colCntTo;

    accPerBatchMat = NaN(2,colCntTo);
    accPerBatchMat(1,outputMatrixStructCNN.accPerBatch.batchColumnsTr) = 1:length(outputMatrixStructCNN.accPerBatch.batchColumnsTr);
    accPerBatchMat(1,outputMatrixStructCNN.accPerBatch.batchColumnsVa) = 1:length(outputMatrixStructCNN.accPerBatch.batchColumnsVa);
    accPerBatchMat(1,outputMatrixStructCNN.accPerBatch.batchColumnsTe) = 1:length(outputMatrixStructCNN.accPerBatch.batchColumnsTe);
    
    outputMatrixStructCNN.accPerBatch.Mat = accPerBatchMat;
    outputMatrixStructCNN.accPerBatch.rowID = 1;
    
    columnNamesCells_1 = {'eID','eCnt'};
    columnNamesCells_2 = setIterativeCellStr( 'Tr', outputMatrixStructCNN.accPerBatch.batchCntTr);
    columnNamesCells_3 = {'TrMn','TrELM'};
    columnNamesCells_4 = setIterativeCellStr( 'Va', outputMatrixStructCNN.accPerBatch.batchCntVa);
    columnNamesCells_5 = {'VaMn','VaELM'};
    columnNamesCells_6 = setIterativeCellStr( 'Te', outputMatrixStructCNN.accPerBatch.batchCntTe);
    columnNamesCells_7 = {'TeMn','TeELM'};
    outputMatrixStructCNN.accPerBatch.columnNamesCells = {columnNamesCells_1{:} columnNamesCells_2{:} columnNamesCells_3{:} columnNamesCells_4{:} columnNamesCells_5{:} columnNamesCells_6{:} columnNamesCells_7{:} }; %#ok<CCAT>
end
function updateAccPerBatch(rowOfParams, varargin) %#ok<DEFNU>
    global dropboxPath;
    global outputMatrixStructCNN;
    [ dataType,  newRowAssign,  accuracy_ELM ] = parseArgs(...
    {'dataType' 'newRowAssign' 'accuracy_ELM'},...
    {    ''         false             0      },...
    varargin{:});
    saveToDropBox = false;

    accPerBatchMat = outputMatrixStructCNN.accPerBatch.Mat;
    rowID = outputMatrixStructCNN.accPerBatch.rowID;
    if ~strcmp(dataType,'')
        if length(rowOfParams)>1
            rowOfParams = [reshape(rowOfParams,1,[]) mean(rowOfParams(:))];
        end
        switch dataType
            case 'train'
                accPerBatchMat(rowID,outputMatrixStructCNN.accPerBatch.batchColumnsTr) = [rowOfParams accuracy_ELM];
            case 'valid'
                accPerBatchMat(rowID,outputMatrixStructCNN.accPerBatch.batchColumnsVa) = [rowOfParams accuracy_ELM];
            case 'test'
                accPerBatchMat(rowID,outputMatrixStructCNN.accPerBatch.batchColumnsTe) = [rowOfParams accuracy_ELM];
                saveToDropBox = true;
            otherwise
                error('You are suggesting a dataType that is not present.Layers are - train, valid, test');
        end
    elseif newRowAssign
        %forwardPropogateDeepNet
        rowID = rowID + 1;
        colIDs = 1:2;
        epochIDMain = outputMatrixStructCNN.iterationInfo.epochID_main;
        epochIDSub = outputMatrixStructCNN.iterationInfo.epochID_sub;
        accPerBatchMat(rowID,colIDs)= [epochIDMain epochIDSub];
    end
    outputMatrixStructCNN.accPerBatch.Mat = accPerBatchMat;
    outputMatrixStructCNN.accPerBatch.rowID = rowID;
    if saveToDropBox
        %Global Output Matrix Cnn - gomc
        gomcFileName = [outputMatrixStructCNN.generalInfo.curProjCode '_gomc.mat' ];
        save([dropboxPath outputMatrixStructCNN.generalInfo.dropboxSubFolderName gomcFileName],'outputMatrixStructCNN');
        apbFileName = [outputMatrixStructCNN.generalInfo.curProjCode '_apb.xlsx' ];
        writeToExcelFile( [dropboxPath outputMatrixStructCNN.generalInfo.dropboxSubFolderName apbFileName], outputMatrixStructCNN.accPerBatch.Mat, 'columnNamesCells', outputMatrixStructCNN.accPerBatch.columnNamesCells);
        %writeToExcelFile( [dropboxPath outputMatrixStructCNN.generalInfo.dropboxSubFolderName 'allLogs.xlsx'], outputMatrixStructCNN.accPerBatch.Mat, 'sheetID', 1, 'columnNamesCells', outputMatrixStructCNN.accPerBatch.columnNamesCells);
    end
end

%no sheet just update a matrix in struct!!
function updateFilterStatsTrack(rowOfParams, varargin) %#ok<DEFNU>
    %global dropboxPath;
    global outputMatrixStructCNN;
    [ layerID,  insertMode ] = parseArgs(...
    {'layerID' 'insertMode'},...
    {    0           1     },...
    varargin{:});

    epochID = outputMatrixStructCNN.iterationInfo.epochID_main;
    batchID = outputMatrixStructCNN.iterationInfo.batchID;
    switch outputMatrixStructCNN.iterationInfo.batchType
        case 'train'
            batchTypeInt = 1;
        case 'valid'
            batchTypeInt = 2;
        case 'test'
            batchTypeInt = 3;
    end
    %insertModes:
    %1. normal
    %2. afterOrth
    %3. after wvn
    %4. after bn
    
    %first prepare the matrix to append
    [filterCount, colCnt] = size(rowOfParams);%filterCount by 4(min,mean,max,var)
    
    %additional columns to front - epochID,batchID,insertMode,filterID
    X_cur = [epochID*ones(filterCount,1), batchTypeInt*ones(filterCount,1), batchID*ones(filterCount,1), insertMode*ones(filterCount,1), reshape(1:filterCount,filterCount,1) rowOfParams];
    
    matFieldName = ['mattL' num2str(layerID,'%02d')];
    if isfield(outputMatrixStructCNN.filterStatsTrack,matFieldName)
        eval(['X_all = outputMatrixStructCNN.filterStatsTrack.' matFieldName ';']);
    else
        X_all = [];
    end
    X_all = [X_all;X_cur];
    eval(['outputMatrixStructCNN.filterStatsTrack.' matFieldName ' = X_all;']);
    
    %fileNameToWriteExcel = [dropboxPath outputMatrixStructCNN.generalInfo.dropboxSubFolderName 'filterStatsTrack_L' num2str(layerID,'%02d') '.xlsx'];
    %writeToExcelFile( fileNameToWriteExcel, X_all, 'sheetID', 1, 'columnNamesCells', outputMatrixStructCNN.filterStatsTrack.excelColNames);
end

function saveMoveSummary(rowOfParams) %#ok<DEFNU>
    global dropboxPath;
    global outputMatrixStructCNN;
    
    moveSummaryMat = rowOfParams.moveSummaryMat;
    distribMat = rowOfParams.distribMat; %#ok<NASGU>
    epochID = rowOfParams.epochID;
    distribMatFolder = rowOfParams.distribMatFolder;
    curProjCode = outputMatrixStructCNN.generalInfo.curProjCode;
    
    fileName = [dropboxPath outputMatrixStructCNN.generalInfo.dropboxSubFolderName curProjCode '_moveSummary.txt'];
    fid = fopen(fileName,'a+');
    moveSummaryMat = [reshape(1:size(moveSummaryMat,1),[],1),moveSummaryMat];
    moveSummaryMat = [epochID 1:size(moveSummaryMat,1) 0;moveSummaryMat];
    moveSummaryMat = [moveSummaryMat;NaN(1,size(moveSummaryMat,2))];
    strToWrite = mat2str(moveSummaryMat);
    strToWrite = strrep(strToWrite,' ','-');
    strToWrite = strrep(strToWrite,'[','');
    strToWrite = strrep(strToWrite,']','');
    strToWrite = strrep(strToWrite,';','\n');
    strToWrite = [strToWrite '\n'];
    fprintf(fid,strToWrite);
    fclose(fid);
    
    if ~exist(distribMatFolder,'dir')
        mkdir(distribMatFolder);
    end
    fileName = [distribMatFolder  'distribMat_epoch(' num2str(epochID,'%02d') ').mat'];
    save(fileName,'distribMat');
end










