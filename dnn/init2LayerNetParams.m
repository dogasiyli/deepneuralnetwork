function [theta, OPS_out] = init2LayerNetParams(initMode, sizeIn, sizOut, OPS_in)
%a two layer network is as follows :
%sizeIn, sizeHid, sizeOut
%W1 = [sizeHid,sizeIn]
%W2 = [sizeOut,sizeHid]
%b1 = [sizeHid]
%b2 = [sizeOut]
%theta = [W1(:) W2(:) b1(:) b2(:)]

%initModes : 
%1. Random
% a. HiddenSizeGiven
%    UsedWhen : when hiddensize is given 
%    Needs : varianceMethod and/or varianceValue - explained below
% b. PercentageToPreserve
%    UsedWhen : when we can not decide the hiddenSize value, and we want to
%    get this information from PCA. everything else is followed as in
%    hiddenSizeGiven. 
%    if 'sizeHidMax' is also given, we should be careful not to set hidSize 
%    to a value bigger than that 
%2. UseData
% a. PCA
%    UsedWhen : When PCA will be used to learn weights 
%    Needs : we need to get eiter hiddensize count or percentToPreserve
%    information. also we need data itself to calculate principle component
%    vectors.
% b. SparseAutoencoder -TBD
% c. ELMAutoencoder -TBD
% d. SparseClassifier -TBD
% e. Classifier -TBD
% f. LDA -TBD
%3. FromKnownParams
% a. ThetaKnown
%    UsedWhen : to check if theta params fit into network layout
%    Needs : [sizeIn,sizeHid,sizeOut] values
% b. WeightsKnown
%    UsedWhen : W1 is known. Maybe W2 known too. bias needs to be set to
%    zero. or W2 will be initialized either with W1' or randomly
%    Needs : how will W2 be initialized? if W1' nothing else needed. if it
%    will be initialized randomly check out the needs for a random
%    initialization.

%VarianceMethod and VarianceValue - check fill_W.m for extra info
%1. Xavier - needs n_in. 
%2. Glorot_Bengio - needs n_in and n_out 
%3. He_Rang_Zhen - needs n_in.

%finalizationMode :
%1. None - do nothing after initialization
%2. Orthogonolize - orthogonolize the weights

    switch initMode
        case 'Random'
            [W1, W2, b1, b2, OPS_out] = sampleRandom(OPS_in, sizeIn, sizOut);
        case 'UseData'
            %by uing data we can;
            %1. use autoencoder
            %2. use sparse classifier
            %3. use a 2 layer classifier
            %4. use pca
            %5. use lda
            [W1, W2, b1, b2, OPS_out] = sampleFromData(OPS_in, sizeIn, sizOut);
        case 'FromKnownParams'
            %either theta is known or some W1 is passed as param
            [W1, W2, b1, b2, OPS_out] = sampleFromKnownParams(OPS_in, sizeIn, sizOut);
    end
    
%% 3.Create theta variable
    % Convert weights and bias gradients to the vector form.
    % This step will "unroll" (flatten and concatenate together) all 
    % your parameters into a vector, which can then be used with minFunc. 
    theta = [W1(:) ; W2(:) ; b1(:) ; b2(:)];
    OPS_out.W1 = W1;
    OPS_out.W2 = W2;
    OPS_out.b1 = b1;
    OPS_out.b2 = b2;
end

function [W1, W2, b1, b2, OPS_out] = sampleFromData(OPS_in, sizeIn, sizOut)
    assert(isfield(OPS_in,'initModeData'),'<initModeData> must be passed as parameter!!');
    switch OPS_in.initModeData
        case 'Autoencoder'
        case 'LDA'
        case 'SparseClassifier'
        case 'Classifier'
        otherwise%'PCA'-default mode
            [W1, W2, b1, b2, OPS_out] = sampleFromData_PCA(OPS_in, sizeIn, sizOut);
    end
end
function [W1, W2, b1, b2, OPS_out] = sampleFromData_PCA(OPS_in, sizeIn, sizOut)
    assert(isfield(OPS_in,'data'),'You have to pass <data> for this mode')
    if isfield(OPS_in,'percentToPreserve')
        [sizHid, OPS_out] = findHidSizeFromPCA(OPS_in.data, OPS_in.percentToPreserve);
        OPS_out.percentPreserved = OPS_in.percentToPreserve;
    else
        OPS_out.percentPreserved = 99.9;
        [sizHid, OPS_out] = findHidSizeFromPCA(OPS_in.data, OPS_out.percentPreserved);
    end
    if isfield(OPS_in,'sizeHidMax') && sizHid>OPS_in.sizeHidMax
        sizHid = OPS_in.sizeHidMax;
    end
    OPS_out.percentPreserved = OPS_out.percentToPreserveVec(sizHid);
    W1 = OPS_out.coeff(:,1:sizHid)';
    W2 = W1';
    b1 = zeros(sizHid, 1);
    b2 = zeros(sizOut, 1);
    OPS_out.layerSizes = [sizeIn sizHid sizOut];
    OPS_out.info = ['PCA used for weight initialization sizeHid(' num2str(sizHid,'%d') '),percPres(' num2str(OPS_out.percentPreserved,'%4.2f') ')'];
end

%completed-not tested
function [W1, W2, b1, b2, OPS_out] = sampleFromKnownParams(OPS_in, sizeIn, sizOut)
    OPS_out = struct;
    if isfield(OPS_in,'initialWeights')
        %initial weight means this:
        %theta = [W(hSxvS),W(vSxhS),b(hS),b(vS)]
        %initial will only give me the first W
        %so that I will check the size of it if exists
        %if it doesn't exist then I will totally randomly initialize them all
        [sizHid, sizVis] = size(OPS_in.initialWeights);
        if isfield(OPS_in,'sizeHid')
            assert(sizHid==OPS_in.sizeHid,'These must be same');
        end
        assert(sizVis==sizeIn, 'initial weight is problematic');
        W1 = OPS_in.initialWeights;
        W2 = W1';
        b1 = zeros(sizHid, 1);
        b2 = zeros(sizOut, 1);
        OPS_out.info = 'initial weights used';        
    elseif isfield(OPS_in,'initialTheta')
        %initial theta means we already know theta
        thetaIn = OPS_in.initialTheta;
        %theta = [W(hSxvS),W(oSxhS),b(hS),b(oS)]
        sizHid = round((length(thetaIn)-sizOut)/(1+sizOut+sizeIn));
        if isfield(OPS_in,'sizeHid')
            assert(sizHid==OPS_in.sizeHid,'These must be same');
        end
        fr = 1;to =    fr + sizHid*sizeIn -1;
        W1 = reshape(thetaIn(fr:to), sizHid, sizeIn);% sizHid x sizVis
        fr = to+1;to = fr + sizHid*sizOut -1;
        W2 = reshape(thetaIn(fr:to), sizOut, sizHid);% sizOut x sizHid
        fr = to+1;to = fr + sizHid - 1;        
        b1 = reshape(thetaIn(fr:to),sizHid,1);       % sizHid x  1
        fr = to+1;to = fr + sizOut - 1;        
        b2 = reshape(thetaIn(fr:to),sizOut,1);       % sizOut x  1
        OPS_out.info = 'initial theta is used';
    else
        error('Either <initialWeights> or <initialTheta> must be passed to use this method');
    end
    OPS_out.layerSizes = [sizeIn sizHid sizOut];
end

%completed-not tested
function [W1, W2, b1, b2, OPS_out] = sampleRandom(OPS_in, sizeIn, sizOut)
    assert(isfield(OPS_in,'sizeHid')  || (isfield(OPS_in,'percentToPreserve') && isfield(OPS_in,'data')),'You have to either pass <sizeHid> or <percentToPreserve> for this mode')
    if isfield(OPS_in,'percentToPreserve') && isfield(OPS_in,'data')
        [sizHid, OPS_out] = findHidSizeFromPCA(OPS_in.data, OPS_in.percentToPreserve);
        if isfield(OPS_in,'sizeHidMax') && sizHid>OPS_in.sizeHidMax
            sizHid = OPS_in.sizeHidMax;
            OPS_out.percentPreserved = OPS_out.percentToPreserveVec(sizHid);
        end
        OPS_in.sizeHid = sizHid;
    else
        OPS_out = struct;
    end
    %now we definitely have "OPS_in.sizeHid"
    sizHid = OPS_in.sizeHid;
    W1_size = [sizHid,sizeIn];
    W2_size = [sizOut,sizHid];

    %we need to check varianceMode and varianceValue
    assert(isfield(OPS_in,'varianceMode'),'You have to pass <varianceMode>');
    if ~isfield(OPS_in,'W2SetTransposeOfW1')
        %W2 is either transpose of W1 or randomly sampled
        OPS_in.W2SetTransposeOfW1 = sizeIn==sizOut;
    end
    switch OPS_in.varianceMode
        case {'Xavier','He_Rang_Zhen'}
            assert(isfield(OPS_in,'n_in'),'You have to pass <n_in>');
            W1 = fill_W( OPS_in.varianceMode, OPS_in.n_in, [], W1_size);
            if ~OPS_in.W2SetTransposeOfW1
                W2 = fill_W( OPS_in.varianceMode, OPS_in.n_in, [], W2_size);
            end
        case 'Glorot_Bengio'
            assert(isfield(OPS_in,'n_in') && isfield(OPS_in,'n_out'),'You have to pass <n_in> & <n_out>');
            W1 = fill_W( OPS_in.varianceMode, OPS_in.n_in, OPS_in.n_out, W1_size);
            if ~OPS_in.W2SetTransposeOfW1
                W2 = fill_W( OPS_in.varianceMode, OPS_in.n_out, OPS_in.n_in, W2_size);
            end
        case 'KnownVariance'
            assert(isfield(OPS_in,'varianceVal'),'You have to pass <varianceVal>');
            W1 = initWeightByVariance(W1_size, sqrt(OPS_in.varianceVal), 0);
            if ~OPS_in.W2SetTransposeOfW1
                W2 =  initWeightByVariance(W2_size, sqrt(OPS_in.varianceVal), 0);
            end
    end
    OPS_out.info = ['<' OPS_in.varianceMode '> used'];
    if OPS_in.W2SetTransposeOfW1
        W2 = W1';
    end
    %I know W1 and W2
    b1 = zeros(sizHid, 1);
    b2 = zeros(sizOut, 1);   
    OPS_out.layerSizes = [sizeIn sizHid sizOut];
end






