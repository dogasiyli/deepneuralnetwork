function W = initWeightByVariance(W_size, stdW, meanValue)
    W = meanValue + stdW*randn(W_size);
end