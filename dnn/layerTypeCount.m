function cnt = layerTypeCount(cnnnet, layerType, ignoreLast)
    %This function counts the given layerType in the cnnnet_out structure.
    
    if (~exist('ignoreLast','var') || isempty(ignoreLast))
        ignoreLast = true;
    end
    
    layerCount = size(cnnnet,1);
    
    %initially the function was meant to calculate the layers except the
    %last one. hence when ignoreLast is not given it will be thought as if
    %it is true;
    if ignoreLast
        layerCount = layerCount - 1;
    end
    
    cnt = 0;
    for i=1:layerCount
        if (strcmp(layerType,cnnnet{i,1}.type))
            cnt = cnt + 1;
        end
    end
end