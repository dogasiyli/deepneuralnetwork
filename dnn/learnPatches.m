function [sizeVis, meanPatch, ZCA_p, W, b, learnPatchResultStruct] = learnPatches( ...
    convFiltDim, countFilt,   ...
    data,        ...
    sizeChn,     ...
    varargin)%paramsMinFunc,  paramsELM,  paramsAutoEncode, acquirePatchesFunc,  displayOpts, saveOpts
    %either {patchCountPerSample, strideParam, samplingMethod} needs to be passed in
    %if none is passed in samplingMethod=1
    %samplingMethod=1 : patchCountPerSample=1 is a good default
    %samplingMethod=2 : strideParam = convFiltDim(1) is also another good default
    %samplingMethod=3 : strideParam = 1 is the best if it is not fucking up the memory
    
    %patchCountPerSample or
    %strideParam needs to be used
    
% STEP-1 : Set optional param defaults
    [  convFiltDim ...
      ,paramsMinFunc,  paramsELM,  paramsAutoEncode, paramsSparseClassifier, paramStr ...
      ,acquirePatchesFunc ...
      ,applyZCAWhitening ...
      ,dataStruct, dMIS...
      ,patchCountPerSample, strideParam, samplingMethod ...
      ,displayOpts, saveOpts, curLayerID] = getOptionalParams(convFiltDim, varargin{:});
  
    learnPatchResultStruct = struct;
% STEP-2 : Acquire patches
    if ~isempty(acquirePatchesFunc)
        [patches, sizeVis] = acquirePatchesFunc(data);
    else
        [patches, patchLabels, sizeVis, acquirePatchSummary] = acquirePatches_BySamplingMethod(samplingMethod, data, dMIS, dataStruct, sizeChn, patchCountPerSample, convFiltDim, displayOpts, saveOpts, curLayerID);
        learnPatchResultStruct.acquirePatchSummary = acquirePatchSummary;
    end
    learnPatchResultStruct.patchSize = size(patches);
    if displayOpts.memOpts
        disp([mat2str(learnPatchResultStruct.patchSize) ' will consume ' num2str((numel(patches)*8) / (1024*1024),'%6.4f') ' MB of memory.']);
    end
    
% STEP-3 : Mean-normalize  and ZCA_Whiten patches
    [patches, meanPatch] = meanNormalizePatches(patches);
    [ZCA_patches, ZCA_p] = ZCA_whiten_patches(patches, [], ~applyZCAWhitening, ~strcmp(displayOpts.displayLevel,'none'));

% STEP-4 : Learn weight params according to given method
    OPS_in = struct;
    OPS_in.sizeHid = countFilt;   
    if ~isempty(paramsAutoEncode)
        vararginParams = {'paramsMinFunc',  paramsMinFunc,'paramsAutoEncode', paramsAutoEncode,'saveOpts', saveOpts,'displayOpts', displayOpts};
        [~, W, b] = autoencodeData( ZCA_patches, countFilt, vararginParams{:});
        learnPatchResultStruct.UsedMethod = 'Autoencoder';
    elseif ~isempty(paramsSparseClassifier)
        learnPatchResultStruct.UsedMethod = 'SparseClassifier';
        switch paramsSparseClassifier.thetaInitMode
            case 'Random'
                OPS_in.varianceMode = 'KnownVariance';
                OPS_in.varianceVal = 1;
            case 'UseData'
                OPS_in.initModeData = paramsSparseClassifier.initModeData;%'LDA' is the default
                switch OPS_in.initModeData
                    case 'PCA'
                        OPS_in.sizeHidMax = countFilt;
                    case 'LDA'
                    case 'XOR_ELM'
                    case 'SparseClassifier'
                    case 'Classifier'
                end
        end
        vararginParams = {'OPS_in',  OPS_in, 'paramsMinFunc',  paramsMinFunc,'paramsSparseClassifier', paramsSparseClassifier,'saveOpts', saveOpts,'displayOpts', displayOpts};
        [~, W, b] = trainSparseClassifier( ZCA_patches, patchLabels, countFilt, vararginParams{:});
    elseif ~isempty(paramsELM)
        learnPatchResultStruct.UsedMethod = 'ELM_autoencoder';
        vararginParams = {'sizeHid',countFilt,'paramsELM', paramsELM,'displayOpts', displayOpts};
        [W, b] = ELM_autoencoder( ZCA_patches, vararginParams{:});
    elseif ~isempty(paramStr) && strcmpi(paramStr,'XOR_ELM')
        [W, b, learnPatchResultStruct] = learnFrom_XOR_ELM(ZCA_patches, patchLabels, countFilt);
    elseif ~isempty(paramStr) &&  strcmpi(paramStr,'PCA_PerClass')
        [W, b, learnPatchResultStruct] = learnFrom_PCAPC(ZCA_patches, patchLabels, countFilt);
    elseif ~isempty(paramStr) &&  strcmpi(paramStr,'PCA')
        [W, b, learnPatchResultStruct] = learnFrom_PCA(ZCA_patches, countFilt);
    elseif ~isempty(paramStr) &&  strcmpi(paramStr,'AE_PB')
        global minFuncStepInfo;
        learnPatchResultStruct.UsedMethod = 'AE_PB';
        %apply pca for countFilt/category count
        
        W = rand(countFilt,size(ZCA_patches,1));
        b = zeros(countFilt,1);
        
        initMode = 'UseData';
        sizeIn = size(ZCA_patches,1);
        sizOut = sizeIn;
        disp([mat2str(size(ZCA_patches)) ' sized patches will be used for PCA weight learning!'])
        OPS_in = struct('data', ZCA_patches', 'sizeHidMax', countFilt,'initModeData','PCA');
        [~, OPS_out] = init2LayerNetParams(initMode, sizeIn, sizOut, OPS_in);
        disp('The PCA operation explanation is:')
        disp(OPS_out.info);
        W(1:OPS_out.layerSizes(2),:) = OPS_out.W1;
        b(1:OPS_out.layerSizes(2)) = OPS_out.b1;
        
        learnPatchResultStruct.PCA_percentPreserved = OPS_out.percentPreserved;
        learnPatchResultStruct.PCAOutInfo = OPS_out.info;
        
        
        %vararginParams = {'paramsMinFunc',  paramsMinFunc,'paramsAutoEncode', paramsAutoEncode,'saveOpts', saveOpts,'displayOpts', displayOpts};
        OPS_in = struct;
        OPS_in.initialWeights = W;%autoencodeData( ZCA_patches, countFilt, vararginParams{:});
        %now use W as the initial weights and run AE for all batch-patches
        epochCnt = 2;
        patchCountPerSample = dMIS.count_patchPerSample_strideOne;
        patchInfo_SingleImage = [dMIS.inputSizeTotal([1 2]) 1 dMIS.inputSizeTotal(1) 1 dMIS.inputSizeTotal(2)];
        paramsAutoEncode.thetaInitMode = 'FromKnownParams';
        paramsMinFunc = setParams_minFunc('showInfo_saveLog');
        paramsMinFunc.maxIter = 10;
        paramsMinFunc.maxFunEvals = 20;
        logNameTxtAdd = ['L(' num2str(curLayerID) ')'];
        paramsMinFunc = setLogFile(paramsMinFunc, displayOpts, saveOpts, logNameTxtAdd);
        costArr = zeros(epochCnt, dataStruct.countBatch.train);
        iterCnt = zeros(epochCnt, dataStruct.countBatch.train);
        minFuncStepInfo = []; %#ok<NASGU>
        minFuncStepInfo = struct;
        minFuncStepInfo.Iteration = 1;
        minFuncStepInfo.FunEvals = 1;
        
        e = 0;
        keepOn = true;
        while keepOn && e<epochCnt
            e = e+1;
            batchIDVec = randperm(dataStruct.countBatch.train);
            for b=batchIDVec
                batchIDVecStr = strrep(mat2str(batchIDVec), [' ' num2str(b) ' '], ['<' num2str(b) '>']);
                disp(batchIDVecStr);
                %this only loads the current layer/current batch
                [X, labels_cur] = loadAndSaveForInitialLearning(curLayerID, b, dataStruct.countBatch.train, displayOpts, saveOpts);
                [~, leftTopMostPixIDs_SingleImage] = getPatchPixIDs(patchInfo_SingleImage, [], convFiltDim, 1);
                patches_cur = acquirePatchesToLearn_StrideOne(X, sizeChn, patchCountPerSample, convFiltDim, leftTopMostPixIDs_SingleImage, labels_cur);
                clear X;
                patches_cur = meanNormalizePatches(patches_cur, meanPatch);
                if applyZCAWhitening
                    patches_cur = ZCA_whiten_patches(patches_cur, ZCA_p, [], ~strcmp(displayOpts.displayLevel,'none'));
                end
                vararginParams = {'OPS_in',OPS_in,'paramsMinFunc',  paramsMinFunc,'paramsAutoEncode', paramsAutoEncode,'saveOpts', saveOpts,'displayOpts', displayOpts...
                                  ,'applySparsity', true, 'useActSecondLayer', true};
                %or use trainAutoencoder
                minFuncStepInfo.EpochID = e;
                minFuncStepInfo.BatchID = b;
                try % it is ok if not succeded
                    [W, opttheta, outputCellArr] = trainAutoEncoder( patches_cur, vararginParams{:});
                    costArr(e,b) = outputCellArr.cost;
                    iterCnt(e,b) = outputCellArr.iterations;
                    clear OPS_in;
                    OPS_in = struct;
                    OPS_in.initialTheta = opttheta;
                catch
                    disp('XoXoXoX-Some error occured just skipping this-XoXoXoX');
                end
            end
            if e>1
                costDif = sum(costArr(e-1,:)) - sum(costArr(e,:));
                iterCntSum = sum(iterCnt(e,:)>2);
                keepOn = iterCntSum>0 && costDif>0;
            end
            if ~keepOn
                disp(['iterCntSum(' num2str(iterCntSum) '),costDif(' num2str(costDif) ')']);
            end
        end
        minFuncStepInfo = [];
    else%if ~isempty(paramStr) &&  strcmpi(paramStr,'PCA')
        %apply pca
    end
end

% STEP-1 : Set optional param defaults
function [ convFiltDim, ...
           paramsMinFunc,  paramsELM,  paramsAutoEncode, paramsSparseClassifier, paramStr, ...
           acquirePatchesFunc, ...
           applyZCAWhitening, ...
           dataStruct, dMIS,...
           patchCountPerSample, strideParam, samplingMethod, ...
           displayOpts, saveOpts, curLayerID] = getOptionalParams(convFiltDim, varargin)
    %P1D0 -> Passed assed paramaeter = 1, Default value used = 0
    [ paramStr,  paramsAutoEncode,   paramsSparseClassifier,   displayOpts,  saveOpts,  paramsCategory_MinFunc,     paramsMinFunc,  paramsELM,  acquirePatchesFunc,  applyZCAWhitening,  dataStruct,  patchCountPerSample,  strideParam,  samplingMethod,  dMIS,  curLayerID, P1D0] = parseArgs(...
    {'paramStr' 'paramsAutoEncode'  'paramsSparseClassifier'  'displayOpts' 'saveOpts' 'paramsCategory_MinFunc'    'paramsMinFunc' 'paramsELM' 'acquirePatchesFunc' 'applyZCAWhitening' 'dataStruct' 'patchCountPerSample' 'strideParam' 'samplingMethod' 'dMIS' 'curLayerID'},...
    {     []            []                      []                  []            []       'showNone_noLog'              []              []              []                   false           []               []               []              []          []        1      },...
    varargin{:});
    %if displayOpts is not passed as a parameter then set is to 'all' by default 
    %to show the user what will happen in every detail.
    %also return it as a parameter because even if it is not passed in it
    %would be returned to be used for upcoming evaluations
    if (isempty(displayOpts))
        displayOpts = setDisplayOpts('all');
        if (displayOpts.defaultParamSet && P1D0.displayOpts==0)
            disp([displayOpts.ies{4} 'display options is set to -all- by default.']);
        end
    end
    %% it is either paramsELM or paramsAutoEncode
    
    %paramsELM will be used only if it is passed as parameter
    %default is an autoencoder learning
    ELM1_AE2_SC3_LDPCA4 = 2;%default is AE=2
    
    if P1D0.paramsELM==1
        ELM1_AE2_SC3_LDPCA4 = 1;
    elseif P1D0.paramsSparseClassifier==1
        ELM1_AE2_SC3_LDPCA4 = 3;
    elseif ~isempty(paramStr)
        ELM1_AE2_SC3_LDPCA4 = 4;        
    end
    
    if (ELM1_AE2_SC3_LDPCA4==2)
    %if AE is selected as default
        if P1D0.paramsAutoEncode==0
            %if no Autoencoder param is passed then initialize the paramsAutoEncode
            paramsAutoEncode = setParams_Autoencoder();
        end
        if isempty(paramsMinFunc)
            %paramsMinFunc must be filled with values
            paramsMinFunc = setParams_minFunc(paramsCategory_MinFunc);
        end
        paramsMinFunc = setLogFile(paramsMinFunc, displayOpts, saveOpts, 'AE');
        if (P1D0.paramsAutoEncode==1 && isfield(paramsAutoEncode,'maxIter'))
            %enforce maximum iteration count in paramsAutoEncode into paramsMinFunc
            paramsMinFunc.maxIter = paramsAutoEncode.maxIter;
        elseif (P1D0.paramsMinFunc==1 && isfield(paramsMinFunc,'maxIter'))
            %enforce maximum iteration count in paramsMinFunc into paramsAutoEncode
            paramsAutoEncode.maxIter = paramsMinFunc.maxIter;
        end
    elseif (ELM1_AE2_SC3_LDPCA4==3)
        %sparseClassifier is selected
        if P1D0.paramsSparseClassifier==0
            %if no SparseClassifier param is passed then initialize the paramsSparseClassifier
            paramsSparseClassifier = setParams_sparseClassifier();
        end
        if isempty(paramsMinFunc)
            %paramsMinFunc must be filled with values
            paramsMinFunc = setParams_minFunc(paramsCategory_MinFunc);
        end
        paramsMinFunc = setLogFile(paramsMinFunc, displayOpts, saveOpts, 'SC');

        if (P1D0.paramsSparseClassifier==1 && isfield(paramsSparseClassifier,'maxIter'))
            %enforce maximum iteration count in paramsAutoEncode into paramsMinFunc
            paramsMinFunc.maxIter = paramsSparseClassifier.maxIter;
        elseif (P1D0.paramsMinFunc==1 && isfield(paramsMinFunc,'maxIter'))
            %enforce maximum iteration count in paramsMinFunc into paramsAutoEncode
            paramsSparseClassifier.maxIter = paramsMinFunc.maxIter;
        end        
    end
    
    %% 
    if (displayOpts.defaultParamSet==1)
        %display the default parameters assigned
        if (ELM1_AE2_SC3_LDPCA4==2)
            if (P1D0.paramsAutoEncode==0 && ~isempty(paramsAutoEncode) && isstruct(paramsAutoEncode))
                disp([displayOpts.ies{4} 'Autoencoder options are not passed as function parameter. Hence default values will be used.']);
                disp([displayOpts.ies{4} 'sparsityParam(' num2str(paramsAutoEncode.sparsityParam) ')/weightDecayParam(' num2str(paramsAutoEncode.weightDecayParam) ')/sparsePenaltyWeight(' num2str(paramsAutoEncode.sparsePenaltyWeight)  ')/activationType(' paramsAutoEncode.activationType ')']);
            end
            if (P1D0.paramsMinFunc==0)
                disp([displayOpts.ies{4} 'MinFuncOptions options are not passed as function parameter. Hence default values will be used.']);
                disp([displayOpts.ies{4} 'maxIter(' num2str(paramsMinFunc.maxIter) ')/display(' paramsMinFunc.display ')/logfile(' paramsMinFunc.logfile  ')/Method(' paramsMinFunc.Method ')']);
            end
            if (P1D0.applyZCAWhitening==0)
                disp([displayOpts.ies{4} 'ZCA Whitening will be dismissed by default.']);
            end
        end
    end

    %the convolution filter dimension must be two dimensional for R and C
    %here we make it 2 dimensional if it is given as 1 dim
    if (numel(convFiltDim)==1)
        convFiltDim = [convFiltDim convFiltDim];
    end
    
    %either {patchCountPerSample, strideParam, samplingMethod} needs to be passed in
    %if none is passed in samplingMethod=1
    %samplingMethod=1 : patchCountPerSample=1 is a good default
    %samplingMethod=2 : strideParam = convFiltDim(1) is also another good default
    %samplingMethod=3 : strideParam = 1 is the best if it is not fucking up the memory
    if (P1D0.patchCountPerSample==0 && P1D0.strideParam==0 && P1D0.samplingMethod==0)
        %this is case 1 - nothing is passed
        samplingMethod=1;
        patchCountPerSample = 1;
    elseif (P1D0.patchCountPerSample==0 && P1D0.strideParam==0 && P1D0.samplingMethod==1)
        %this is case 2 - only samplingMethod is passed
        switch samplingMethod
            case 1
                patchCountPerSample = 1;
            case 2
                strideParam = convFiltDim(1);
            case 3
                strideParam = 1;%this needs to be tested such that it would not mess with memory when it is used as 1
        end
    elseif (P1D0.patchCountPerSample==1)
        %this is case 3 - patchCountPerSample is passed
        %the question is whether to use strideParam to fulfill this sample
        %count requirement or to randomly extract such number of sample
        %patches??
        %if this is passed it means do the job by random extraction
        %but we need to check if patchCountPerSample is more than the
        %sample count that can be extracted by strideParam=1, if this is
        %the case then we have to use strideParam=1
        strideParam = [];
        samplingMethod = 4;%patchCountPerSample is used
    elseif (P1D0.strideParam==1)
        %this is case 4 - strideParam is passed
        %we say that patch extraction will be done by stride parameter
        %usage instead of random extraction.
        patchCountPerSample = 0;
        if strideParam==1
            samplingMethod = 3;
        elseif strideParam==convFiltDim(1)
            samplingMethod = 2;
        else
            samplingMethod = 5;%strideParam is used
        end
    end
end
function paramsMinFunc = setLogFile(paramsMinFunc, displayOpts, saveOpts, logNameTxtAdd)
    if ~isempty(paramsMinFunc.logfile) && ~strcmp(paramsMinFunc.logfile,'') && ~isempty(saveOpts)
        %paramsMinFunc must be filled with values
        fileID=0;
        fileNameEnd = ['minFuncLog_learnPatches_' logNameTxtAdd '_' num2str(fileID,'%02d') '.txt'];
        fileNameAll = [saveOpts.MainFolder fileNameEnd];
        try 
            while exist(fileNameAll,'file')
                fileID = fileID+1;
                fileNameEnd = ['minFuncLog_learnPatches_' logNameTxtAdd '_' num2str(fileID,'%02d') '.txt'];
                fileNameAll = [saveOpts.MainFolder fileNameEnd];
            end
        catch
        end
        if displayOpts.fileOperations
            disp([displayOpts.ies{4} '<' fileNameEnd '> is created under <' saveOpts.MainFolder '> for minfunc logs.']);
        end
        paramsMinFunc.logfile = fileNameAll;
    end
end

% STEP-2 : Acquire patches
function [patches, patchLabels, sizeVis, acquirePatchSummary] = acquirePatches_BySamplingMethod(samplingMethod, data, dMIS, dataStruct, sizeChn, patchCountPerSample, convFiltDim, displayOpts, saveOpts, curLayerID)
    acquirePatchSummary = struct;
    switch samplingMethod
        case 1%patchCountPerSample = 1;
            [patches, patchLabels, sizeVis] = acquirePatchesToLearn_Random(data, dMIS, dataStruct, sizeChn, patchCountPerSample, convFiltDim, displayOpts, saveOpts, curLayerID);
        case 2%strideParam = convFiltDim(1);
            error('Not implemented yet');
        case 3%strideParam = 1
            [patches, patchLabels, sizeVis] = getPatches_Case_3(dMIS, dataStruct, data, sizeChn, convFiltDim, displayOpts, saveOpts, curLayerID);
        case 4%patchCountPerSample = something
            %we need to check if
            %patchCountPerSample>patchCountPerSampleWhenStrideParam==1
            %if so then use case 3
            if ~isempty(dMIS)
                if isempty(patchCountPerSample) || dMIS.count_patchPerSample_strideOne < patchCountPerSample
                    patchCountPerSample = [];
                    strideParam = 1;
                    samplingMethod = 3;
                    [patches, patchLabels, sizeVis, patchCntPerBatch] = getPatches_Case_3(dMIS, dataStruct, data, sizeChn, convFiltDim, displayOpts, saveOpts, curLayerID);
                    acquirePatchSummary.dMIS = dMIS;
                    acquirePatchSummary.patchCntPerBatch = patchCntPerBatch;
                else
                    [patches, patchLabels, sizeVis] = acquirePatchesToLearn_Random(data, dMIS, dataStruct, sizeChn, patchCountPerSample, convFiltDim, displayOpts, saveOpts, curLayerID);
                end
            end
        case 5%strideParam = something
            %we need to check max possible value of strideParam
            error('Not implemented yet');
    end  
    acquirePatchSummary.samplingMethod = samplingMethod;
end
function [patches, labels, sizeVis] = acquirePatchesToLearn_Random(X, dMIS, dataStruct, sizeChn, patchCountPerSample, convFiltDim, displayOpts, saveOpts, curLayerID)
    [~, ~, sizeChn, ~] = exploreDataSize(X, sizeChn);
    if exist('dataStruct','var') && ~isempty(dataStruct)
        patches = [];
        labels = [];
        teacherMode = true;
        for b=1:dMIS.countBatch
            [X, labels_cur] = loadAndSaveForInitialLearning(curLayerID, b, dataStruct.countBatch.train, displayOpts, saveOpts);
            [patches_cur, imageIDs_cur, labels_cur] = acquireRandomPatches(patchCountPerSample, convFiltDim, X, sizeChn, teacherMode, labels_cur);
            patches = [patches,patches_cur];
            labels = [labels,labels_cur];
            clear patches_cur labels_cur imageIDs_cur
        end
    elseif ~exist('dataStruct','var') && exist('data','var')
        patches = acquireRandomPatches(patchCountPerSample, convFiltDim, data, sizeChn);
    end    
    
    sizeVis = sizeChn*prod(convFiltDim);
end

function [patches, labels, sizeVis, patchCntPerBatch] = getPatches_Case_3(dMIS, dataStruct, X, sizeChn, patchSize, displayOpts, saveOpts, curLayerID)
    %1rowCnt,2colCnt,3rowStr,4rowEnd,5colStr,6colEnd
    strideParam = 1;
    patchInfo_SingleImage = [dMIS.inputSizeTotal([1 2]) 1 dMIS.inputSizeTotal(1) 1 dMIS.inputSizeTotal(2)];
    patchCountPerSample = (dMIS.inputSizeTotal(1)-dMIS.patchSize(1)+1)*(dMIS.inputSizeTotal(2)-dMIS.patchSize(2)+1);
    [RC_SingleImage, leftTopMostPixIDs_SingleImage] = getPatchPixIDs(patchInfo_SingleImage, [], dMIS.patchSize([1 2]),strideParam);
    patchCntPerBatch = [];
    if ~isempty(dataStruct) %&& dMIS.countBatch>1
        patches = [];
        labels = [];
        patchCntPerBatch = zeros(dMIS.countBatch,1);
        for b=1:dMIS.countBatch
            [X, labels_cur] = loadAndSaveForInitialLearning(curLayerID, b, dataStruct.countBatch.train, displayOpts, saveOpts);
            [patches_cur, imageIDs_cur, labels_cur] = acquirePatchesToLearn_StrideOne(X, sizeChn, patchCountPerSample, patchSize, leftTopMostPixIDs_SingleImage, labels_cur);
            patches = [patches,patches_cur];
            labels = [labels,labels_cur];
            patchCntPerBatch(b) = length(labels_cur);
            clear patches_cur labels_cur imageIDs_cur
        end
%     else
%         [patches, imageIDs, labels] = acquirePatchesToLearn_StrideOne(X, sizeChn, patchCountPerSample, patchSize, leftTopMostPixIDs_SingleImage);
    end
    sizeVis = sizeChn*prod(patchSize);
end

function [patches, imageIDs, labelsSelected] = acquirePatchesToLearn_StrideOne(data, sizeChn, patchCountPerSample, patchSize, leftTopMostPixIDs_SingleImage, labels)
    [sizeImg, countSamples, sizeChn, countFeat] = exploreDataSize(data, sizeChn);
    imageIDs = bsxfun(@plus,1:countSamples,zeros(patchCountPerSample,1));
    labelsSelected = labels(imageIDs);
    leftTopMostPixIDs = repmat(reshape(leftTopMostPixIDs_SingleImage,[],1),1,countSamples);
    [patches, invalidPatchIDs] = acquirePatches( patchSize, data, sizeChn, imageIDs(:)', leftTopMostPixIDs(:)', true);
    patches(:,invalidPatchIDs) = [];
    
    
    imageIDs = reshape(imageIDs,1,[]);
    imageIDs(invalidPatchIDs) = [];
    
    labelsSelected = reshape(labelsSelected,1,[]);
    labelsSelected(invalidPatchIDs) = [];
end

function [W, b, learnPatchResultStruct] = learnFrom_PCA(ZCA_patches, countFilt)
    learnPatchResultStruct = struct;
    learnPatchResultStruct.UsedMethod = 'PCA';
    %apply pca for countFilt/category count

    W = rand(countFilt,size(ZCA_patches,1));
    b = zeros(countFilt,1);

    initMode = 'UseData';
    sizeIn = size(ZCA_patches,1);
    sizOut = sizeIn;
    disp([mat2str(size(ZCA_patches)) ' sized patches will be used for PCA weight learning!'])
    OPS_in = struct('data', ZCA_patches', 'sizeHidMax', countFilt,'initModeData','PCA');
    [~, OPS_out] = init2LayerNetParams(initMode, sizeIn, sizOut, OPS_in);
    disp('The PCA operation explanation is:')
    disp(OPS_out.info);
    W(1:OPS_out.layerSizes(2),:) = OPS_out.W1;
    b(1:OPS_out.layerSizes(2)) = OPS_out.b1;

    learnPatchResultStruct.PCA_percentPreserved = OPS_out.percentPreserved;
    learnPatchResultStruct.PCAOutInfo = OPS_out.info;
end
function [W, b, learnPatchResultStruct] = learnFrom_PCAPC(ZCA_patches, patchLabels, countFilt)
    learnPatchResultStruct = struct;
    learnPatchResultStruct.UsedMethod = 'PCA_PerClass';
    %apply pca according to the labels of patches
    %for countFilt/category count

    %better technique would be to have acquire patches from classes at
    %every step for more efficient memory usage

    W = rand(countFilt,size(ZCA_patches,1));
    b = zeros(countFilt,1);

    initMode = 'UseData';
    sizeIn = size(ZCA_patches,1);
    sizOut = sizeIn;
    categories = unique(patchLabels);
    categoryCount = length(categories);
    classNodeInterval = getBatchIDs(countFilt, categoryCount);
    disp([mat2str(size(ZCA_patches)) ' sized patches will be used for PCA_PerClass weight learning!'])
    disp(['Class categories = ' mat2str(categories)]);
    disp(['HiddenNodes to be learnt from each category = ' mat2str(classNodeInterval(:,3))]);

    learnPatchResultStruct.PCA_PerClass = struct;
    learnPatchResultStruct.PCA_PerClass.classNodeInterval = classNodeInterval;

    percentPreserved = zeros(categoryCount,1);
    numOfPatches = zeros(categoryCount,1);
    for i = 1:categoryCount
        c = categories(i);
        classNodeCount = classNodeInterval(i,3);
        slct = patchLabels==c;
        numOfPatches(i) = sum(slct(:));
        disp([num2str(numOfPatches(i)) ' samples from class(' num2str(c) ') will be used for pca leaning.']);
        classPatches = ZCA_patches(:,slct)';
        OPS_in = struct('data', classPatches, 'sizeHidMax', classNodeCount,'initModeData','PCA');
        [~, OPS_out] = init2LayerNetParams(initMode, sizeIn, sizOut, OPS_in);

        disp(['For category(' num2str(c) ') the PCA operation explanation is:'])
        disp(OPS_out.info);
        W(classNodeInterval(i,1):classNodeInterval(i,2),:) = OPS_out.W1;
        b(classNodeInterval(i,1):classNodeInterval(i,2)) = OPS_out.b1;

        percentPreserved(i) = OPS_out.percentPreserved;
    end
    learnPatchResultStruct.PCA_PerClass.percentPreserved = percentPreserved;
    learnPatchResultStruct.PCA_PerClass.numOfPatches = numOfPatches;
end
function [W, b, learnPatchResultStruct] = learnFrom_XOR_ELM(ZCA_patches, patchLabels, countFilt)
    learnPatchResultStruct = struct;
    learnPatchResultStruct.UsedMethod = 'XOR_ELM';
    disp([mat2str(size(ZCA_patches)) ' sized patches will be used for XOR_ELM weight learning!'])
    W = rand(countFilt,size(ZCA_patches,1));
    b = zeros(countFilt,1);
    %apply XOR_ELM on ZCA_patches
    dS = struct;
    dS.dataTr = ZCA_patches;
    dS.labelsTr = patchLabels;
    maxIterCnt = 2*ceil(log2(countFilt));
    [resultTable, W_2Layer, labelCrossMat, confMatCell] = apply_XOR_Test(dS, maxIterCnt, countFilt, false, []);
    disptable(resultTable,'hidNodeCnt|TrELM|TrOrigMapped|Tr2Layer')

    [remainingFeatIDs, resultSummary] = findIrrelevantFeatures(ZCA_patches'*W_2Layer{end,1}{1}, patchLabels, 'ELM_LDA');
    sizeAvailable = min([countFilt,length(remainingFeatIDs),resultTable(end,1)]);
    confMat = confMatCell{end,1};
    colsSum = sum(confMat)';
    diagConf = diag(confMat);
    colsTable = [diagConf colsSum];
    [sortedTable, idxToSlct] = sortrows(colsTable,-[1 2]);
    disp('Root label confusion matrix :')
    labelDisp = joinStrings(strread(num2str(labelCrossMat(:,end)'),'%s')','|' );
    disptable(confMat, labelDisp, labelDisp);

    learnPatchResultStruct.XOR_ELM_Results = struct;
    learnPatchResultStruct.XOR_ELM_Results.labelCrossMat = labelCrossMat;
    learnPatchResultStruct.XOR_ELM_Results.confMatCell = confMatCell;
    learnPatchResultStruct.XOR_ELM_Results.confMat = confMat;

    learnPatchResultStruct.fetaureCountLearned = sizeAvailable;
    W(1:sizeAvailable,:) = W_2Layer{end,1}{1}(:,idxToSlct(1:sizeAvailable))';
    W = orthonormalizeWeightMat( W' )';

    %use XOR_ELM labeling to increase class count
    %try different lda methods here
    %1. select different patches and keep on 2Class-ELM/LDA
    %2. use the focus option as getting patches from the hot areas
    %3. use XOR_LDA ??
    %NOTE-same things can be applied to ELM_Classify_Patches too  
end
