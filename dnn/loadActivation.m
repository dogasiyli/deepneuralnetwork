function a = loadActivation(saveOpts, activationID, layerID, displayOpts)
    a = [];
    folderNameForActivationData = [saveOpts.MainFolder saveOpts.SubFolders.ActivatedData];
    if (displayOpts.fileOperations==1)
        t = cputime;
        disp([displayOpts.ies{4} 'Loading-Layer(' num2str(layerID) ')-Activation(' num2str(activationID,'%d') ')']);
    end
    load([folderNameForActivationData 'Activation_' num2str(activationID,'%02d') '.mat'],'a');%[a6_out]
    if     (displayOpts.fileOperations==1 && displayOpts.timeMode>=1)
        t = cputime - t;
        disp([displayOpts.ies{4} 'Activation_' num2str(activationID,'%02d') '.mat loaded in ' num2str(t,'%4.2f') ' cputime.']);
    elseif (displayOpts.fileOperations==1 && displayOpts.timeMode <1)
        disp([displayOpts.ies{4} 'Activation_' num2str(activationID,'%02d') '.mat is loaded.']);
    end
end

