function [X, labels] = loadAndSaveForInitialLearning(curLayerID, batchID, countBatch, displayOpts, saveOpts, X, labels, deletePrevLayerBatch)
    %loads and saves the data according to the layer and batch ID
    
    if ~exist('deletePrevLayerBatch','var') ||  isempty(deletePrevLayerBatch)
        deletePrevLayerBatch = true;
    end
    
    %% 1. Create folder name using - saveOpts
    folderName = [saveOpts.MainFolder saveOpts.SubFolders.ActivatedData];
    if ~exist(folderName,'dir')
        mkdir(folderName);
    end
    %% 2. Create fileName using - batchID, countBatch, curLayerID and folderName
    fileNameEnd = ['batch(' num2str(batchID,'%02d') ' of ' num2str(countBatch,'%02d') ').mat'];

    %loadFileName 
    loadFileNameInit = ['layer(' num2str(curLayerID-1,'%02d') ')_'];%load from previous layer
    loadFileName = [folderName loadFileNameInit fileNameEnd];
    
    %% 3. Load cur layer or save cur layer and delete prev layer if necessary
    if exist('X','var') && exist('labels','var') && ~isempty(X) && ~isempty(labels)
        %saveFileName 
        saveFileNameInit = ['layer(' num2str(curLayerID,'%02d') ')_'];
        saveFileName = [folderName saveFileNameInit fileNameEnd];
        if displayOpts.fileOperations
            disp([displayOpts.ies{4} 'Saving ' saveFileNameInit fileNameEnd ' for initial learning of filter params.'])
        end
        save(saveFileName,'X','labels','-v7.3');
        
        if deletePrevLayerBatch && curLayerID>1
            if displayOpts.fileOperations
                disp([displayOpts.ies{4} 'Deleting ' loadFileNameInit fileNameEnd '.'])
            end
            delete(loadFileName);
        end
    else
        if displayOpts.fileOperations
            disp([displayOpts.ies{4} 'Loading ' loadFileNameInit fileNameEnd ' for initial learning of filter params.'])
        end
        load(loadFileName,'X','labels');
    end
end

