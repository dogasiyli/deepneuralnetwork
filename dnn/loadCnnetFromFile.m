function [cnnnetInitial, initFileMode, allVariables] = loadCnnetFromFile(initialCNNNETFile)
    if isempty(initialCNNNETFile) || ~exist(initialCNNNETFile,'file')
        cnnnetInitial = [];
        initFileMode = 'noCNNETfile';
        return
    end
    %file exist
    vars = whos('-file',initialCNNNETFile);
    
    %< 'opt_cnnnet' or 'cnnnet' or 'cnnnetInitial' or 'cnnnet_appended'>
    if ismember('opt_cnnnet', {vars.name})
        initFileMode = 'opt_cnnnet';
    elseif ismember('cnnnet', {vars.name})
        initFileMode = 'cnnnet';
    elseif ismember('cnnnet_appended', {vars.name})
        initFileMode = 'cnnnet_appended';
    elseif ismember('cnnnetInitial', {vars.name})
        initFileMode = 'cnnnetInitial';
    else
        initFileMode = 'noCNNETfile';
        cnnnetInitial = [];
        disp('file have incorrect variables:');
        disp({vars.name});
        disp('hence cnnnetInitial = []');
        allVariables = [];
        return;
    end
    load(initialCNNNETFile);
    allVariables.NameString = ['''' strjoin({vars.name},''',''') ''''];
    allVariables.FileWhosOut = vars;
    allVariables.Variables = struct;
    for i=1:length(vars)
        eval(['allVariables.Variables.' vars(i,1).name ' = ' vars(i,1).name ';']);
    end
    
    eval(['cnnnetInitial = ' initFileMode ';']);
    eval(['clear ' initFileMode ';']);
    disp([initFileMode ' is loaded from <' initialCNNNETFile '>']);
    disp(['Loaded files are : ' allVariables.NameString])
    
    if strcmpi(initFileMode,'cnnnet_appended')
        for i=1:length(cnnnetInitial)
            cnnnetInitial{i, 1}.fwdPropAllBatches = true; %#ok<*AGROW>
            if isfield(cnnnetInitial{i, 1},'weightVarianceNormalizationMode')
                cnnnetInitial{i, 1}.weightVarianceNormalizationMode = 'initial';
                cnnnetInitial{i, 1}.applyVarNorm = true;
            end
            if isfield(cnnnetInitial{i, 1},'biasNormalizationMode')
                if dS.countBatch.train==1
                    cnnnetInitial{i, 1}.biasNormalizationMode = 'initial';
                else
                    cnnnetInitial{i, 1}.biasNormalizationMode = 'batch';
                end
                cnnnetInitial{i, 1}.applyBiasNorm = true;
            end
            if isfield(cnnnetInitial{i, 1},'applyOrthonormalization')
                cnnnetInitial{i, 1}.applyOrthonormalization = true;
            end
        end
    end    
end

