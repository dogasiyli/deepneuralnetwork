function [map_singleInput, outImgSize, convMap2D] = mapDataForConvolution( patchSize, stride, sizeImg, countSamples, inputDataChannelSize, tutorialMode)
%TODO : this function works for only square images, patches and stride
%       it must be flexible - rectangular images, patches and strides
    if ~exist('tutorialMode','var')
      tutorialMode =  false;
    end
    if length(sizeImg)==1
        sizeImg = [sizeImg sizeImg];
    end
    if length(patchSize)==1
        patchSize = [patchSize patchSize];
    end
    if length(stride)==1
        stride = [stride stride];
    end
        

    ri = sizeImg(1);%Row size of Intput images
    ci = sizeImg(2);%Col size of Intput images
    rf = patchSize(1);%Row size of Filter images
    cf = patchSize(2);%Col size of Filter images
%   ro = sizeImg-patchSize+1;%Row size of Output images
%   co = sizeImg-patchSize+1;%Col size of Output images
    ro = 1+floor((ri-rf) / stride(1)); 
    co = 1+floor((ci-cf) / stride(2));
    outImgSize = [ro co];
    %data is dxcountSamples
    countFeat = sizeImg(1)*sizeImg(2)*inputDataChannelSize;
    if (tutorialMode)
        patch_1_1 = bsxfun(@plus,repmat((1:ri:cf*ri),rf,1),(0:cf-1)');
        leftTopMostPixIDs = (1:stride(1):ro*stride(1))';%first column of input image (single depth or first depth dimension of image)
        leftTopMostPixIDs = repmat(leftTopMostPixIDs,1,co);%for all rows of output image this line and the next
        leftTopMostPixIDs = bsxfun (@plus,leftTopMostPixIDs,(1:ri*stride(2):ri*co*stride(2))-1);%here all the lefttopmost pixels for single depth or first depth dimension of image 
        % A = repmat(A,1,sizeChn);%repeat first left top most patch for each channel
        % eachChannelTotalSize = sizeImg^2;%calculate the shift amount of index values for each dimension
        % B = eachChannelTotalSize*repmat((1:sizeChn)'-1,1,patchSize^2)';%
        % A = A+B;%
        % the same 4 lines can be done faster by
        patch_allDims_singleImage = bsxfun(@plus,patch_1_1(:),sizeImg(1)*sizeImg(2)*((1:inputDataChannelSize)-1));
        map_singleInput = bsxfun(@plus,leftTopMostPixIDs(:)',patch_allDims_singleImage(:))-1;
    else
        patch_1_1 = bsxfun(@plus,repmat((1:ri:cf*ri),rf,1),(0:cf-1)');
        leftTopMostPixIDs = bsxfun(@plus,repmat((1:stride(1):ro*stride(1))',1,co),(1:ri*stride(2):ri*co*stride(2))-1);
        patch_allDims_singleImage = bsxfun(@plus,patch_1_1(:),sizeImg(1)*sizeImg(2)*((1:inputDataChannelSize)-1));
        map_singleInput = bsxfun(@plus,leftTopMostPixIDs(:)',patch_allDims_singleImage(:))-1;
    end
    if (nargout>2)
        convMap2D = reshape(bsxfun(@plus,1:countFeat:countSamples*countFeat,map_singleInput(:))-1,rf*cf*inputDataChannelSize,ro*co*countSamples);
    end
    %mapped_data = data(ind2sub(size(data),convMap2D));
end