function [convMap2D,map_singleInput] = mapDataForNextLayer( patchSize, stride, sizeImg, countSamples, inputDataChannelSize, tutorialMode)
    if ~exist('tutorialMode','var')
      tutorialMode =  false;
    end

    ri = sizeImg;%Row size of Intput images
    ci = sizeImg;%Col size of Intput images
    rf = patchSize;%Row size of Filter images
    cf = patchSize;%Row size of Filter images
%   ro = sizeImg-patchSize+1;%Row size of Output images
%   co = sizeImg-patchSize+1;%Col size of Output images
    co = 1+round((ci-cf) / stride);
    ro = 1+round((ri-rf) / stride);  
    %data is dxN
    countFeat = sizeImg^2*inputDataChannelSize;
    if (tutorialMode)
        singleInputImg = reshape(1:ri*ci,ri,ci);
        patch_1_1 = bsxfun(@plus,repmat((1:ri:cf*ri),rf,1),(0:cf-1)');
        leftTopMostPixIDs = (1:stride:ro*stride)';%first column of input image (single depth or first depth dimension of image)
        leftTopMostPixIDs = repmat(leftTopMostPixIDs,1,co);%for all rows of output image this line and the next
        leftTopMostPixIDs = bsxfun (@plus,leftTopMostPixIDs,(1:ri*stride:ri*ci)-1);%here all the lefttopmost pixels for single depth or first depth dimension of image 
        % A = repmat(A,1,sizeChn);%repeat first left top most patch for each channel
        % eachChannelTotalSize = sizeImg^2;%calculate the shift amount of index values for each dimension
        % B = eachChannelTotalSize*repmat((1:sizeChn)'-1,1,patchSize^2)';%
        % A = A+B;%
        % the same 4 lines can be done faster by
        patch_allDims_singleImage = bsxfun(@plus,patch_1_1(:),sizeImg^2*((1:inputDataChannelSize)-1));
        map_singleInput = bsxfun(@plus,leftTopMostPixIDs(:)',patch_allDims_singleImage(:))-1;
        convMap2D = reshape(bsxfun(@plus,1:countFeat:countSamples*countFeat,map_singleInput(:))-1,rf*cf*inputDataChannelSize,ro*co*countSamples);
        %mapped_data = data(ind2sub(size(data),map_all));
    else
        patch_1_1 = bsxfun(@plus,repmat((1:ri:cf*ri),rf,1),(0:cf-1)');
        leftTopMostPixIDs = bsxfun(@plus,repmat((1:stride:co*stride)',1,ro),(1:ri*stride:ri*ci)-1);
        patch_allDims_singleImage = bsxfun(@plus,patch_1_1(:),sizeImg^2*((1:inputDataChannelSize)-1));
        map_singleInput = bsxfun(@plus,leftTopMostPixIDs(:)',patch_allDims_singleImage(:))-1;
        convMap2D = reshape(bsxfun(@plus,1:countFeat:countSamples*countFeat,map_singleInput(:))-1,rf*cf*inputDataChannelSize,ro*co*countSamples);
        %mapped_data = data(ind2sub(size(data),map_all));
    end
end