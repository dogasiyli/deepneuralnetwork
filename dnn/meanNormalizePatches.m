function [patches, meanPatch] = meanNormalizePatches(patches, meanPatch)
%Mean-normalize patches
    if nargin<2 || isempty(meanPatch)
        meanPatch = mean(patches,2);
    end
    patches = bsxfun(@minus,patches,meanPatch);
end