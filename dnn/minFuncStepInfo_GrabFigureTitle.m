function [minFuncAdditionalInfoStr] = minFuncStepInfo_GrabFigureTitle(curStep, cost, lastAccuracyCalced) 
    global minFuncStepInfo;
    if (isfield(minFuncStepInfo,'NewIteration') && minFuncStepInfo.NewIteration == true)
        minFuncStepInfo.NewIteration = false;
    end
    
    if (exist('lastAccuracyCalced','var') && ~isempty(lastAccuracyCalced))
        minFuncStepInfo.Accuracy = lastAccuracyCalced;
    end
    
    minFuncAdditionalInfoStr = ['Cost_{(' num2str(cost,'%6.4g') ')}'];
    if ~isempty(minFuncStepInfo) && isstruct(minFuncStepInfo) && isfield(minFuncStepInfo,'StepType')
%         minFuncStepInfo.FunEvals = minFuncStepInfo.FunEvals + 1;
%         minFuncStepInfo.StepType = 'WolfLineInit';%'MainIteration';%'WolfLineStep';%'ArmijoBacktrack';%
%         minFuncStepInfo.StepLength = t;
        if ~isempty(minFuncStepInfo) && isstruct(minFuncStepInfo) && isfield(minFuncStepInfo,'Iteration')
            minFuncAdditionalInfoStr = ['Iteration_{(' num2str(minFuncStepInfo.Iteration,'%02d') ')}' minFuncAdditionalInfoStr];
        end
        minFuncAdditionalInfoStr = [minFuncAdditionalInfoStr 'FunEvals_{(' num2str(curStep,'%02d') ')}'];
        if isfield(minFuncStepInfo,'StepType')
            stepTypeStr = strrep(minFuncStepInfo.StepType,'_','\_');
            minFuncAdditionalInfoStr = [minFuncAdditionalInfoStr 'StepType_{(' stepTypeStr ')}'];
        end
        if isfield(minFuncStepInfo,'StepLength')
            minFuncAdditionalInfoStr = [minFuncAdditionalInfoStr 'StepLength_{(' num2str(minFuncStepInfo.StepLength,'%6.4g') ')}'];
        end
    end  
end