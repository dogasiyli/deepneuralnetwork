function [normalizationCell,dataNormalized] = normalizationStep( normalizationCell, trainData )

% normalizationCell is a struct

% Parameters that should be given
% normalizationCell.name = 'norm_1_1';                      %just a naming convention
% normalizationCell.type = 'normalization';                 %this shoould exactly match
% normalizationCell.type2 = 'Mean-Variance Normalization';  %this is important

% Parameters to be calculated
% if type2 is 'Mean-Variance Normalization'
% normalizationCell.mean
% normalizationCell.std

switch (normalizationCell.type2)
    case 'Mean-Variance Normalization'
        %I tried this normalization in the initial step for MNIST
        %it failed miserably for the validation part
        if ~isfield(normalizationCell,'mean') || isempty(normalizationCell.mean) 
            normalizationCell.mean = mean(trainData,2);
        end
        if ~isfield(normalizationCell,'std') || isempty(normalizationCell.std) 
            normalizationCell.std = std(trainData,0,2);
        end
        if (nargout>1)
            dataNormalized = bsxfun(@rdivide,bsxfun(@minus,trainData,normalizationCell.mean),normalizationCell.std);
            dataNormalized(isnan(dataNormalized)) = 0;
        end
    case 'Local Response Normalization'
        error('Not implemented normalization type');
    case 'Local Contrast Normalization'
        error('Not implemented normalization type');
    otherwise
        error('Not implemented normalization type');
end

end

