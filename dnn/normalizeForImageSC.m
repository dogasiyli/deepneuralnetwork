function im = normalizeForImageSC(im)
    minVal = min(im(:));
    im = im-minVal;
    maxVal = max(im(:));
    im = im./maxVal;
end