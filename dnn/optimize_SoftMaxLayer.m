function cnnnet = optimize_SoftMaxLayer(cnnnet, data, labels, displayOpts, saveOpts, softMaxOptimizeMaxIteration, useAccuracyAsCost, dS)
    global outputMatrixStructCNN;
    if length(softMaxOptimizeMaxIteration)>1 || prod(softMaxOptimizeMaxIteration)>0
        if ~exist('dS','var')
            softMaxOptimizeMaxIteration = [prod(softMaxOptimizeMaxIteration) 1];
        end
        %for the last softmax layer - we will learn using all train
        %batches' last fully connected representations
        paramsMinFunc = setParams_minFunc();
        globalOutputStruct('updateEpochBatchParams',struct('generalType','softMax_Cumulative'));
        for i=1:softMaxOptimizeMaxIteration(2)
            globalOutputStruct('updateEpochBatchParams',struct('batchCount',0,'batchID',0,'batchType','train','epochID_main',i-1,'epochID_sub',softMaxOptimizeMaxIteration(2)));
            %test the validation and test data
            if exist('dS','var')
                globalOutputStruct('updateAccPerBatch',[],'newRowAssign',true);
                [cnnnet, cnnnetResults_Train] = deepNet_Test( dS, cnnnet, displayOpts, saveOpts, 'train', ['softMaxOpt-' num2str(i-1) ' of ' num2str(softMaxOptimizeMaxIteration(2))],[0 i-1 softMaxOptimizeMaxIteration(2)]);
                [ ~, cnnnetResults_Valid] = deepNet_Test( dS, cnnnet, displayOpts, saveOpts, 'valid', ['softMaxOpt-' num2str(i-1) ' of ' num2str(softMaxOptimizeMaxIteration(2))],[0 i-1 softMaxOptimizeMaxIteration(2)]);
                [ ~, cnnnetResults_Test] = deepNet_Test( dS, cnnnet, displayOpts, saveOpts, 'test', ['softMaxOpt-' num2str(i-1) ' of ' num2str(softMaxOptimizeMaxIteration(2))],[0 i-1 softMaxOptimizeMaxIteration(2)]);
            end            
            saveOptsMainFolderBackup = saveOpts.MainFolder;
            saveOpts.MainFolder = [saveOpts.MainFolder 'Train\'];    
            paramsMinFunc.maxIter = softMaxOptimizeMaxIteration(1);
            minFuncLogFileFull = [saveOpts.MainFolder 'minFuncLog_softOpt_' num2str(i) ' of ' num2str(softMaxOptimizeMaxIteration(2)) '_<' num2str(i*softMaxOptimizeMaxIteration(2)) '>.txt'];
            
            theta = [cnnnet{end,1}.Weight(:);cnnnet{end,1}.BiasParams(:)];
            varargin = { 'theta', theta  ....
                        ,'lambda', 0.0001...
                        ,'paramsMinFunc', paramsMinFunc...
                        ,'displayOpts', displayOpts...
                        ,'saveOpts', saveOpts...
                        ,'useAccuracyAsCost',useAccuracyAsCost...
                        ,'minFuncLogFileFull',minFuncLogFileFull};

            optimizedParamsStruct = softMaxTrain(data, labels, varargin{:});
            cnnnet{end,1}.Weight = optimizedParamsStruct.optWeight;
            cnnnet{end,1}.BiasParams = optimizedParamsStruct.optBias;
            
            %now that the parameters are optimized for sOMI(1) iterations
            saveOpts.MainFolder = saveOptsMainFolderBackup;
        end
        globalOutputStruct('updateEpochBatchParams',struct('epochID_main',softMaxOptimizeMaxIteration(2)));
    end
end