function [W_ort, WtW] = orthonormalizeWeightMat(W)

%the weight matrix can be either square or rectangular
%if W is square : (test -- "W = orth(randn(5,5));wwt = W*W', wtw = W'*W")
%W*W' = W'*W = eye(size(W,1))
%if W is rectangle :
%   if size(W,1)>size(W,2) : (test -- "W = orth(randn(8,5));wwt = W*W', wtw = W'*W")
%       wwt - is not identity
%       wtw - is identity
%   elseif size(W,2)>size(W,1) : (test -- "W = orth(randn(5,8));wwt = W*W', wtw = W'*W")
%       wwt and wtw are both identity
%
%
% This is because : orth function spans the columns of W,
% when the row size is bigger than col size W*W' is not identity
% but the main idea is to set the column vectors to be prependicular to
% each other, hence the function always return such a W_ort matrix that
% W_ort' * W_ort is identity - columns are multiplied with each other

    W_ort = orth(W);
    if sum(size(W_ort)-size(W_ort))~=0
        warning('you gotta check this problem out');
        pause;
    end
    if nargout>1
        WtW = W_ort'*W_ort;
    end
end

