function [distribMatNew, moveSummary] = planDataShuffle(pred_labels, dataShuffleMode)
    batchCount = length(pred_labels);
    categories = [];
    categoryCount = [];
    analysisPerBatch = [];
    
    for b = 1:batchCount
        pred_Cur = pred_labels(b).pred;
        labels_Cur = pred_labels(b).labels;
        if isempty(categories)
            categories = unique([pred_Cur labels_Cur]);
            categoryCount = length(categories);
        else
            categoriesCur = unique([pred_Cur labels_Cur]);
            checkDel = sum(categoriesCur-categoriesCur);
            assert(checkDel==0,'categories in different batches do not macth');
        end
        if isempty(analysisPerBatch)
            analysisPerBatch = zeros(categoryCount, batchCount, 3);
        end
        
        cm = confusionmat(labels_Cur, pred_Cur);
        for ic = 1:categoryCount
            c = categories(ic);
            totalSampleCount = sum(cm(c,:));
            trueSampleCount = cm(c,c);
            falseSampleCount = totalSampleCount - trueSampleCount;
            analysisPerBatch(ic,b,:) = [totalSampleCount trueSampleCount falseSampleCount]; %#ok<AGROW>
        end
    end
    
    switch dataShuffleMode
        case 'adaboost'
            [distribMat, distribMatNew] = formOldDistributionMat(pred_labels, categories);
            [distribMatNew, moveSummary] = adaboostShuffle(pred_labels, categories, distribMatNew);
        case 'uniform'
            %1. create the distribMat
            [distribMat, distribMatNew] = formOldDistributionMat(pred_labels, categories);
            
            %2.
            [distribMatSorted, errorIdx] = sortrows(distribMat,[3 4]);
            errorIdxSorted = errorIdx;
            for kr = 1:categoryCount
                for kp = 1:categoryCount
                    slct = distribMatSorted(:,3)==kr & distribMatSorted(:,4)==kp;
                    errToSort = distribMatSorted(slct,7+kp-1);
                    [~,part_kr_kp_idx] = sort(errToSort,'descend');
                    
                    idx_org = find(slct);
                    idx_err_srt = idx_org(part_kr_kp_idx);
                    distribMatSorted(idx_org,:) = distribMatSorted(idx_err_srt,:);
                    errorIdxSorted(idx_org) = errorIdx(idx_err_srt);
                end
            end
            distribMatNew = distribMatNew(errorIdxSorted,:);
            
            %check errorIdxSorted if any d�plicate or missing value
            assert(sum(sort(errorIdxSorted)-(1:max(errorIdxSorted))')==0,'there is a shuffle problem')
            %3. now we have to find the new batchIDs and sampleIDs
            batchSampleCntStrt = ones(batchCount,1);
            for kr = 1:categoryCount
                slct = distribMatSorted(:,3)==kr;
                idxToSet = find(slct);
                numOfSamplesOfCurClass = sum(slct==1);
                numOfSamplesToDistribute = analysisPerBatch(kr,:,1);
                assert((sum(numOfSamplesToDistribute)-numOfSamplesOfCurClass)==0,'these must be same')
                
                newBatchIDs_Vec = NaN(numOfSamplesOfCurClass,1);
                newSampleIDs_Vec = NaN(numOfSamplesOfCurClass,1);
                lastSampleID = 1;
                to=0;
                while sum(numOfSamplesToDistribute)>0
                    minSampleCnt = min(numOfSamplesToDistribute(numOfSamplesToDistribute>0));
                    availableBatchIDs = unique(find(numOfSamplesToDistribute>0));
                    newBatchIDs = reshape(repmat(availableBatchIDs,1,minSampleCnt),[],1);
                    
                    abc = length(availableBatchIDs);
                    newSampleIDs = zeros(abc,minSampleCnt);%lastSampleID:lastSampleID+minSampleCnt-1;
                    for b = availableBatchIDs
                        newSampleIDs(b,:) = batchSampleCntStrt(b):(batchSampleCntStrt(b)+minSampleCnt-1);
                    end
                    newSampleIDs(sum(newSampleIDs,2)==0,:) = [];
                    newSampleIDs = reshape(newSampleIDs,[],1);
                    
                    fr = to+1;
                    to = fr + length(availableBatchIDs)*minSampleCnt - 1;
                    numOfSamplesToDistribute(availableBatchIDs) = numOfSamplesToDistribute(availableBatchIDs) - minSampleCnt;
                    newBatchIDs_Vec(fr:to) = newBatchIDs;
                    newSampleIDs_Vec(fr:to) = newSampleIDs;
                    lastSampleID = lastSampleID+minSampleCnt;
                    batchSampleCntStrt(availableBatchIDs) = batchSampleCntStrt(availableBatchIDs)  +  minSampleCnt;
                end
                assert(sum(numOfSamplesToDistribute)==0,'this must be 0')
                distribMatNew(idxToSet,1) = newBatchIDs_Vec;
                distribMatNew(idxToSet,2) = newSampleIDs_Vec;
            end
            moveSummary = createMoveSummaryFromDistribMatNew(distribMatNew);
        case 'distCluster'
            %1. create the distribMat
            [distribMat, distribMatNew] = formOldDistributionMat(pred_labels, categories);
            
            %2.
            dataRepresentative = [];
            lablRepresentative = [];
            predRepresentative = [];
            for b = 1:batchCount
                dataRepresentative = [dataRepresentative pred_labels(b).batchData];
                lablRepresentative = [lablRepresentative;pred_labels(b).batchLabels];
                predRepresentative = [predRepresentative;pred_labels(b).pred'];
            end
            D_rep = pdist2_fast(dataRepresentative',dataRepresentative');
            %Z = linkage(dataRepresentative','average', 'euclidean')
            D_vec = reshape(D_rep(tril(ones(size(D_rep))==1,-1)),1,[]);
            Z = linkage(D_vec,'ward');%'complete'%
            clusterCount = batchCount;
            T = cluster(Z,'maxclust',clusterCount);
            T_hist = hist(T,1:clusterCount);
            disp('Histogram of clusters : ')
            disp(mat2str(T_hist));
            
            [ distribMatNew, batchMoveSummary_Rfr_Cto] = planClustersIntoBatches( dataRepresentative, lablRepresentative, predRepresentative, T, distribMatNew );
            moveSummary = createMoveSummaryFromDistribMatNew(distribMatNew);
    end 
    
    %now set the 1st and 2nd columns for distribMatNew if they are empty
    nonChangedRows = distribMatNew(:,1)==0;
    M = distribMatNew(nonChangedRows,:);
    distribMatNew(nonChangedRows,[1 2]) = M(:,[3 4]);
    distribMatNew = sortrows(distribMatNew, [1 2]);    
end

function moveSummary = createMoveSummaryFromDistribMatNew(distribMatNew)
    %moveSummary = [batchIDFrom batchIDTo numberOfSamples categoryID];
    categoryCount = length(unique(distribMatNew(:,5)));%from known category ids
    batchCount = max(distribMatNew(:,1));%from new batchID column
    moveSummary = zeros(batchCount*batchCount,4);
    for bf = 1:batchCount
        for bt = bf+1:batchCount
            for kr = 1:categoryCount
                slct = distribMatNew(:,1)==bt & distribMatNew(:,3)==bf & distribMatNew(:,5)==kr;
                movedSampleCount = sum(slct==1);
                if movedSampleCount>0
                    moveSummary((bf-1)*batchCount+bt,:) = [bf bt movedSampleCount kr];
                end
            end
        end
    end
    moveSummary(sum(moveSummary,2)==0,:) = [];
end

function [distribMatNew, moveSummary] = adaboostShuffle(pred_labels, categories, distribMatNew)
    moveSummary = [];
    categoryCount = length(categories);
    batchCount = length(pred_labels);
    for bt = 1 : batchCount-1
        %which samples will go from 'bf' to 'bt'
        %TEST-ROW-analysisBT_original = squeeze(analysisPerBatch(:,bt,:));
        %TEST-ROW-totalSampleCount_BT_original = sum(analysisBT_original(:,1));
        %TEST-ROW-pred_BT = pred_labels(bt).pred;
        %TEST-ROW-labels_BT = pred_labels(bt).labels;
        %TEST-ROW-confMat_BT_original = confusionmat(labels_BT,pred_BT);
        analysisBT = analyzeBatchCategoryCnts(distribMatNew, bt);%TEST-RETURN-[analysisBT, totalSampleCount_BT, confMat_BT]
        [moveableFalseSamplesCnt] = calcMovableFalseSamplesPerBatch(distribMatNew);
        %false samples will stay as is
        
        bf = find(sum(moveableFalseSamplesCnt,2)>0,1,'last');
        trueSampleCnt_bt = sum(analysisBT(:,2));
        movableSampleCnt = sum(sum(moveableFalseSamplesCnt(bt+1 : batchCount,:)));
        while (bf>bt && trueSampleCnt_bt>0 && movableSampleCnt>0)
            movableSampleCnt_bf = sum(moveableFalseSamplesCnt(bf,:));
            if movableSampleCnt_bf>0%dont go in if there is no false that can be transferred
                %analyze batchID==bf
                [analysisBF, totalSampleCount_BF, confMat_BF] = analyzeBatchCategoryCnts(distribMatNew, bf);%TEST-ROW-
                for ic = 1:categoryCount
                    c = categories(ic);
                    cntFals_bf_c = moveableFalseSamplesCnt(bf,c);
                    cntTrue_bt_c = analysisBT(c,2);
                    cnt_bf_bt = min(cntFals_bf_c,cntTrue_bt_c);
                    if cnt_bf_bt>0
                        [distribMatNew, moveSummary_Cur] = moveSamples(distribMatNew, bf, bt, c, cnt_bf_bt);
                        moveSummary = [moveSummary;moveSummary_Cur]; %#ok<AGROW>
                        %update the matrixes
                        analysisBT = analyzeBatchCategoryCnts(distribMatNew, bt);%[analysisBT, totalSampleCount_BT, confMat_BT]
                        [moveableFalseSamplesCnt] = calcMovableFalseSamplesPerBatch(distribMatNew);
                        [analysisBF, totalSampleCount_BF, confMat_BF] = analyzeBatchCategoryCnts(distribMatNew, bf);%TEST-ROW-
                    end
                end
            end
            trueSampleCnt_bt = sum(analysisBT(:,2));
            bf = bf - 1;
        end
    end
end

function [dMN, moveSummary] = moveSamples(dMN, batchIDFrom, batchIDTo, categoryID, numberOfSamples)
    %the false samples in batchIDFrom
    rowsFr = dMN(:,1)==0 & dMN(:,3)==batchIDFrom & dMN(:,5)==categoryID &  dMN(:,6)~=dMN(:,5);
    cntFr = sum(rowsFr==1);
    indsFr = find(rowsFr);
    
    %the correct samples in batchIDTo
    rowsTo = dMN(:,1)==0 & dMN(:,3)==batchIDTo & dMN(:,5)==categoryID &  dMN(:,6)==dMN(:,5);
    cntTo = sum(rowsTo==1);
    indsTo = find(rowsTo);
        
    assert(numberOfSamples==min(cntFr,cntTo));
    
    if cntFr>numberOfSamples
        %select the first "numberOfSamples" samples from rowsFr
        indsFr(numberOfSamples+1:end) = [];
    end
    if cntTo>numberOfSamples
        %select the first "numberOfSamples" samples from rowsTo 
        indsTo(numberOfSamples+1:end) = [];     
    end
    Mfr = dMN(indsFr,:);
    Mto = dMN(indsTo,:);
    
    Mfr(:,[1 2]) = dMN(indsTo,[3 4]);
    Mto(:,[1 2]) = dMN(indsFr,[3 4]);
    
    dMN(indsFr,:) = Mfr;
    dMN(indsTo,:) = Mto;
    
    moveSummary = [batchIDFrom batchIDTo numberOfSamples categoryID];
end

function [moveableFalseSamplesCnt] = calcMovableFalseSamplesPerBatch(distribMatNew)
    %distribMatNew columns = {1_newBatchID,2_newSampleID,3_oldBatchID,4_oldSampleID,5_realLabel,6_predictedLabel}
    categories = unique(distribMatNew(:,5));
    categoryCount = length(categories);
    batchIDs = unique(distribMatNew(:,3));
    batchCount = length(batchIDs);
    moveableFalseSamplesCnt = zeros(batchCount, categoryCount);
    distribMatOriginal = distribMatNew(distribMatNew(:,1)==0,:);
    for b = 1:batchCount
        M = distribMatOriginal(distribMatOriginal(:,3)==b,:);
        cm = zeros(categoryCount);
        for c_r = 1:categoryCount%real rows
            for c_p = 1:categoryCount%prediction columns
                cm(c_r,c_p) = sum(M(:,5)==c_r & c_p==M(:,6));
            end
            moveableFalseSamplesCnt(b,c_r) = sum(cm(c_r,:)) - cm(c_r,c_r);
        end
    end
end

function [analysisOfBatch, totalSampleCount, confMat] = analyzeBatchCategoryCnts(distribMatNew, batchID)
    %distribMatNew columns = {1_newBatchID,2_newSampleID,3_oldBatchID,4_oldSampleID,5_realLabel,6_predictedLabel}
    categories = unique(distribMatNew(:,5));
    categoryCount = length(categories);
        
    %I have to divide distribMatNew into two
    %1. newly transferred samples (first column~=0)
    slct = distribMatNew(:,1)==batchID;
    distribMatNewlyTransferred = distribMatNew(slct,:);
    %2. not transferred samples (first column==0)
    slct = (distribMatNew(:,3)==batchID & distribMatNew(:,1)==0);
    distribMatOriginal = distribMatNew(slct,:);
    
    analysisOfBatch = zeros(categoryCount,3);
    confMat = zeros(categoryCount,categoryCount);
    for i = 1:categoryCount
        c  = categories(i);
        if ~isempty(distribMatOriginal)
            slctOrigAll = distribMatOriginal(:,5)==c;
            predictOriginal = distribMatOriginal(slctOrigAll,6);
        else
            predictOriginal = [];
            slctOrigAll = 0;
        end
        if ~isempty(distribMatNewlyTransferred)
            slctNTAll = distribMatNewlyTransferred(:,5)==c;
            predictNT = distribMatNewlyTransferred(slctNTAll,6);
        else
            predictNT = [];
            slctNTAll = 0;
        end
        sampleCount_c_total = sum(slctOrigAll==1) + sum(slctNTAll==1);
        sampleCount_c_true = sum(predictOriginal==c) + sum(predictNT==c);
        sampleCount_c_false = sum(predictOriginal~=c) + sum(predictNT~=c);
        analysisOfBatch(c,:) = [sampleCount_c_total, sampleCount_c_true, sampleCount_c_false];
        predict_c = [predictOriginal;predictNT];
        confVec = hist(predict_c,categories);
        confMat(c,:) = confVec;
    end
    
    totalSampleCount = sum(analysisOfBatch(:,1));
end

function [distribMat, distribMatNew] = formOldDistributionMat(pred_labels, categories)
    softMaxToProbs = @(M_) bsxfun(@rdivide,exp(M_),sum(exp(M_)));
    batchCount = length(pred_labels);
    categoryCount = length(categories);
    distribMat = zeros([],6+categoryCount);
    to = 0;
    for b = 1 : batchCount
        fr = to + 1;
        pred_Cur = pred_labels(b).pred;
        labels_Cur = pred_labels(b).labels;
        totalSampleCount = length(pred_Cur);
        to = fr + totalSampleCount - 1;
        distribMat(fr:to,1) = b*ones(totalSampleCount,1);%01_batchID
        distribMat(fr:to,2) = reshape(1:totalSampleCount,[],1);%02_sampleID
        distribMat(fr:to,3) = reshape(labels_Cur,[],1);%03_realLabel
        distribMat(fr:to,4) = reshape(pred_Cur,[],1);%04_predictedLabel
        distribMat(fr:to,5) = reshape(pred_Cur==labels_Cur,[],1);%05_predictionTF
        distribMat(fr:to,6) = false(totalSampleCount,1);%06_usedInNewDistribution
        distribMat(fr:to,7:7+categoryCount-1) = softMaxToProbs((pred_labels(b).softMaxedData))';%7:7+K_predictionProbs
    end
    
    rowCnt = size(distribMat,1);
    distribMatNew = zeros(rowCnt,6);
    distribMatNew(:,[3 4 5 6]) = distribMat(:,[1 2 3 4]);
end

