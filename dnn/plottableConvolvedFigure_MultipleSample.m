function [X_out, label_inds, h] = plottableConvolvedFigure_MultipleSample(X, outputSize_original, figureID, titleStr)
    Ro = outputSize_original(1);
    Co = outputSize_original(2);
    F  = outputSize_original(3);
    %X is countSamples by 1 cell
    countSamples = size(X,1);

    rc = countSamples;
    cc = F;
    X_out = zeros(rc*Ro,cc*Co);
    for r = 1:countSamples
        imagesToPlot = X{r,1};  
        %imagesToPlot has size (Ro*Co*F,1)
        imagesToPlot = reshape(imagesToPlot,Ro*Co,F);
        rArea = (r-1)*Ro+1 : r*Ro;
        for c=1:F
            imToPlot = imagesToPlot(:,c);
            imToPlot = reshape(imToPlot,Ro,Co);          
            cArea = (c-1)*Co+1 : c*Co;
            X_out(rArea,cArea) = imToPlot;
        end
    end
    for c=1:F
        cArea = (c-1)*Co+1 : c*Co;
        imToPlot = X_out(:,cArea);
        imToPlot = normalizeForImageSC(imToPlot);           
        X_out(:,cArea) = imToPlot;
    end    
    
    if (exist('figureID','var') && ~isempty(figureID) && figureID>0)
        h = figure(figureID);clf;
        imagesc(X_out);

        label_inds.X = Co/2 : Co : F*Co-Co/2;
        label_inds.Y = Ro/2 : Ro : countSamples*Ro-Ro/2;
%         label_inds.X = linspace(Co/2, F*Co, min(F,20));
%         label_inds.Y = Ro : Ro : countSamples*Ro-Ro;

        XLabelCount_ToBe = getAllPossibleDivisors(F);
        XLabelCount_ToBe(XLabelCount_ToBe>12) = [];
        XLabelCount_ToBe = XLabelCount_ToBe(end);
        XLabelSlct = round(linspace(1,F,XLabelCount_ToBe));
        
        label_inds.X = label_inds.X(XLabelSlct);
        
        
        
        XTickLabel_strcell = textscan(num2str(XLabelSlct),'%s');
        YTickLabel_strcell = textscan(num2str(1:countSamples),'%s');
        XTickLabel_strcell = XTickLabel_strcell{1,1};
        YTickLabel_strcell = YTickLabel_strcell{1,1};
        


        set(gca,'XTick',label_inds.X);
        set(gca,'YTick',label_inds.Y);
        set(gca,'XTickLabel',XTickLabel_strcell);
        set(gca,'YTickLabel',YTickLabel_strcell);
        xlabel('Filters');
        ylabel('Categories');
        if (exist('titleStr','var') && ~isempty(titleStr) && ~strcmp(titleStr,''))
            title(titleStr);
        end
        colorbar;
    else
        h = [];
    end
end