function [X_out, mostActivFeatMap] = plottableConvolvedFigure_SingleSample(X, convolutionCell)
    Ro = convolutionCell.outputSize_original(1);
    Co = convolutionCell.outputSize_original(2);
    F  = convolutionCell.outputSize_original(3);
    countSamples = size(X,2);%must be 1 sample for now
    assert(countSamples==1,'only 1 sample can be visualized');
    
    %the output figure must be visible in blocks
    rc = ceil(sqrt(F));
    cc = ceil(F/rc);
    
    X_out = zeros(rc*Ro,cc*Co);
    bestMeanActiv = 0;
    mostActivFeatMap = 0;
    for f=1:F
        [r,c] = ind2sub([rc,cc],f);
        rArea = (r-1)*Ro+1 : r*Ro;
        cArea = (c-1)*Co+1 : c*Co;
        imToPlot = X( (f-1)*Ro*Co+1 : f*Ro*Co  );
        imToPlot = reshape(imToPlot,Ro,Co);
        meanActivFeatMap = mean(imToPlot(imToPlot>0));
        if Ro*Co>1
            imToPlot = normalizeForImageSC(imToPlot);
        end
        if bestMeanActiv<meanActivFeatMap
            bestMeanActiv = meanActivFeatMap;
            mostActivFeatMap = f;
        end
        X_out(rArea,cArea) = imToPlot;
    end
    if Ro*Co==1
        emptyVal = -max(X_out(:));
    else
        emptyVal = -0.1*max(X_out(:));        
    end
    for f=F+1:rc*cc
        [r,c] = ind2sub([rc,cc],f);
        rArea = (r-1)*Ro+1 : r*Ro;
        cArea = (c-1)*Co+1 : c*Co;
        X_out(rArea,cArea) = emptyVal;
    end
end