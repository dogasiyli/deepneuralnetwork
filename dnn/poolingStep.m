function [C_PO, pooledVectorizedData, pooledOriginalShapedData] = poolingStep( C_PO, data, displayOpts, saveOpts, dataStruct, calculateBackPropMap, deletePrevLayerBatch)
%C_PO = Cell_POol

    %set the following some number if you want to test the convolution using for
    %loop on that many images
    testPoolingImageCount = 0;

    if (~exist('calculateBackPropMap','var')) || (~exist('data','var'))
        calculateBackPropMap = false;
    end

    %this variable is set to true - not to pool data at once
    %because it uses too much memory for nothing
    poolInBatches = true;

% STEP-0 : Check the input channel size and input size original
    C_PO = checkIOSizes(C_PO);

% STEP-1 : If the sizes aren't learnt/identified yet learn/identify them from data
    C_PO = learnIOSizes(C_PO, data);

% STEP-2 : Learn the convolution and backpropogation mapping filters
    C_PO = learnFilters(C_PO, displayOpts);

    %if number of output arguments is more than 1 construct/calculate pooledVectorizedData
    if nargout>1
% STEP-3 : Apply pooling
        [pooledVectorizedData, C_PO] = applyPooling(C_PO, data, displayOpts, poolInBatches, calculateBackPropMap);
        if nargout>2
            pooledOriginalShapedData = reshape(pooledVectorizedData,C_PO.outputSize_original);
        end
% STEP-4 : Test pooling   
        testPoolingOperation(C_PO, data, pooledVectorizedData, testPoolingImageCount, displayOpts);       
    end
    
% STEP-5 : Pool other batches for preparing them to following layers
    if C_PO.fwdPropAllBatches
        outputFilterStatsAllBatches = saveAllBatchesToActivatedDataFolder(C_PO, dataStruct, displayOpts, saveOpts, @applyPooling, pooledVectorizedData, deletePrevLayerBatch);
        C_PO.outputFilterStatsAllBatchesInitial = outputFilterStatsAllBatches;
        C_PO.fwdPropAllBatches = false;
    end
end

% STEP-0 : Check the input channel size and input size original
function C_PO = checkIOSizes(C_PO)
    if isfield(C_PO,'inputSize_original') && ~isfield(C_PO,'sizeChn')
        C_PO.sizeChn = C_PO.inputSize_original(3);%This came from the previous steps output dimension
    end
end

% STEP-1 : If the sizes aren't learnt/identified yet learn/identify them from data
function C_PO = learnIOSizes(C_PO, data)
    % just checking if any size parameter exist will do the trick
    if (~isfield(C_PO,'outputSize_original') || size(data,2)~=C_PO.outputSize_original(4))
        [sizeImg, countSamples, sizeChn, ~] = exploreDataSize(data, C_PO.sizeChn);
        Ri = sizeImg; Ci = sizeImg;
        Rf = C_PO.poolSize(1);
        Cf = C_PO.poolSize(2);
        C_PO.sizeChn = sizeChn;
        C_PO.inputSize_original = [sizeImg,sizeImg,sizeChn,countSamples];
        C_PO.inputSize_vectorized = [sizeImg*sizeImg*sizeChn,countSamples];

        paddingOp = sum(abs(C_PO.padInputWithZeros));
        strideOp  = sum(abs(C_PO.stride));

        if (~paddingOp && ~strideOp)
            assert(mod(Ri/Rf,1)==0,['Stride or padding options probplematic in ' C_PO.name ' layer.<' num2str(Ri) 'can not divide ' num2str(Rf) '>'  ])
            assert(mod(Ci/Cf,1)==0,['Stride or padding options probplematic in ' C_PO.name ' layer.<' num2str(Ci) 'can not divide ' num2str(Cf) '>'  ])
            Ro = round(Ri/Rf);
            Co = round(Ci/Cf);
        elseif (~paddingOp && strideOp)
            error('Stride in pool operation not implemented yet')
        elseif (paddingOp && ~strideOp)
            error('Padding in pool operation not implemented yet')
        else % (paddingOp && strideOp)
            error('Neither stride nor pool operation not implemented yet')
        end

        C_PO.outputSize_original = [Ro,Co,C_PO.sizeChn,countSamples];
        C_PO.outputSize_vectorized = [Ro*Co*C_PO.sizeChn,countSamples];
    end
end

% STEP-2 : If the filters aren't learnt yet learn them from data
% TODO : Utilize usage of displayOpts
function [C_PO, poolMapAlreadyLearned] = learnFilters(C_PO, displayOpts)
    poolMapAlreadyLearned = isfield(C_PO,'poolMap');
    if ~poolMapAlreadyLearned
        % dIn = C_PO.inputSize_vectorized(1);%Ri*Ci*Di
        % dOut = C_PO.outputSize_vectorized(1);%Ro*Co*filCnt
        %next line will be changed for non-square data
        map_singleInput = mapDataForConvolution(C_PO.poolSize(1),         ... patchSize, 
                                                C_PO.poolSize(1),         ... stride, %it is like convolution but stride is equal to poolSize
                                                C_PO.inputSize_original(1), ... sizeImg, 
                                                C_PO.inputSize_original(4), ... countSamples,
                                                C_PO.sizeChn,           ... inputDataChannelSize, 
                                                false);                             %tutorialMode

        %mean pooling for MNIST layer 1
        %[26 26 32 30000] to [13 13 32 30000] --> took 9.4915 seconds. / memory usage 6083 mb to 12700 mb
        %[12 12 32 30000] to [6 6 32 30000] --> took 1.385 seconds. / memory usage 2196 mb to 2449 mb
        Ro = C_PO.outputSize_original(1);
        Co = C_PO.outputSize_original(2);
        ps = prod(C_PO.poolSize);
        Di = C_PO.sizeChn;
        poolMapInc = map_singleInput(1:ps,1);
        C_PO.poolMapInc = poolMapInc-poolMapInc(1);
        map_singleInput_res = reshape(map_singleInput',[Ro*Co,ps,Di]);
        map_singleInput_res = reshape(permute(map_singleInput_res,[1 3 2]),[],1);
        C_PO.poolMap = map_singleInput_res(1:Ro*Co*Di);
    end
end

% STEP-3 : Apply pooling
% TODO : check if C_PO.poolMethod - all are working - 3 of them.
% TODO : Utilize usage of displayOpts
function [pooledVectorizedData, C_PO] = applyPooling(C_PO, data, displayOpts, poolInBatches, calculateBackPropMap)
    if ~exist('poolInBatches','var')
        poolInBatches = true;
    end
    if ~exist('calculateBackPropMap','var')
        calculateBackPropMap = false;
    end
    countSamples = size(data,2);

    Ro = C_PO.outputSize_original(1);
    Co = C_PO.outputSize_original(2);
    ps = prod(C_PO.poolSize);
    Di = C_PO.sizeChn;%L1-6083mb/L2-2196mb

    if (poolInBatches)
       varSize = 2*numel(data)/131072;%size in megabytes 4 times
       memAvail = displayMemory(false);
       batchCount = max([ceil(varSize/memAvail) ps]);
    end

    if (poolInBatches)
      [batchIDs, singleBatchSampleCountMax] = getBatchIDs(countSamples,batchCount);
       poolVectorizedBatch = zeros(Ro*Co*Di,singleBatchSampleCountMax,ps,'double');
       pooledVectorizedData = zeros(Ro*Co*Di,countSamples,'double');
    else
       pooledVectorizedData = zeros(Ro*Co*Di,countSamples,ps,'double');
    end

    if calculateBackPropMap
       %if more than 127 filters are being used next should be
       %'int16'
       C_PO.backPropInd = zeros(Ro*Co*Di,countSamples,'int8');
    end

    if poolInBatches
       for batchData = 1:batchCount
           fr = batchIDs(batchData,1);
           to = batchIDs(batchData,2);
           for p=1:ps
               %for each pixel of the pooling we have 1 layer
               %for example for a pooling layer of 2x2 there are 4 pixels
               %to be pooled
               %hence
               poolVectorizedBatch(:,1:batchIDs(batchData,3),p) = data(C_PO.poolMapInc(p)+C_PO.poolMap,fr:to);
           end
           switch (C_PO.poolType)%L1-11031mb/L2-3246mb
               case 'mean'
                   pooledVectorizedData(:,fr:to) = squeeze(mean(poolVectorizedBatch(:,1:batchIDs(batchData,3),:),3));
               case 'max'
                   %these lines are tested by TEST-1 at the end of this file
                   if ~calculateBackPropMap
                       [pooledVectorizedData(:,fr:to),~] = max(poolVectorizedBatch(:,1:batchIDs(batchData,3),:),[],3);
                   else
                       [pooledVectorizedData(:,fr:to),C_PO.backPropInd(:,fr:to)] = max(poolVectorizedBatch(:,1:batchIDs(batchData,3),:),[],3);
                   end
           end
       end
       clear fr to
    else
       for p=1:ps
           %for each pixel of the pooling we have 1 layer
           %for example for a pooling layer of 2x2 there are 4 pixels
           %to be pooled
           %hence
           pooledVectorizedData(:,:,p) = data(C_PO.poolMapInc(p)+C_PO.poolMap,:);
       end
    end

    switch (C_PO.poolType)%L1-11031mb/L2-3246mb
       case 'mean'%L1-12700mb during evaluation,L1-7322mb after evaluation / L2-2449mb after evaluation
           if (~poolInBatches)
               pooledVectorizedData = squeeze(mean(pooledVectorizedData,3));
           else
               %it is already done above
           end
       case 'max'
           if (~poolInBatches)
               if ~calculateBackPropMap
                   pooledVectorizedData = max(pooledVectorizedData,[],3);
               else
                   [pooledVectorizedData,C_PO.backPropInd] = max(pooledVectorizedData,[],3);
               end
           end
    end
end

% STEP-4 : Test pooling  
function testPoolingOperation(C_PO, data, pooledVectorizedData, testPoolingImageCount, displayOpts)
    if (testPoolingImageCount<=0)
        return;
    end
    Ro = C_PO.outputSize_original(1);
    Co = C_PO.outputSize_original(2);
    filCnt = C_PO.sizeChn;
    data = reshape(data,C_PO.inputSize_original);  
    %n=10;
    slctdImgs = sort(randi(size(data,4),[1 testPoolingImageCount]));
    n_PooledImages = zeros(Ro,Co,filCnt,testPoolingImageCount);
    poolResult = zeros(Ro,Co);
    for imageNum = 1:testPoolingImageCount
      for featureNum = 1:C_PO.sizeChn
          theCells = mat2cell(squeeze(data(:,:,featureNum,slctdImgs(imageNum))),ones(1,Ro)*C_PO.poolSize(1),ones(1,Co)*C_PO.poolSize(2));
          for r=1:Ro
              for c=1:Co
                switch C_PO.poolType
                    case 'max'
                        poolResult(r,c) = max(reshape(theCells{r,c},prod(C_PO.poolSize),1));
                    otherwise
                        poolResult(r,c) = mean(reshape(theCells{r,c},prod(C_PO.poolSize),1)); 
                end                      
              end
          end
          n_PooledImages(:,:,featureNum,imageNum) = poolResult;
      end
    end
    a1 = reshape(n_PooledImages,Ro,Co,C_PO.sizeChn,testPoolingImageCount);
    a2 = reshape(pooledVectorizedData(:,slctdImgs),size(n_PooledImages));%pooledOriginalShapedData(:,:,:,slctdImgs);
    resultErrorTreshold = 1e-10;
    resultError = sum(abs(a1(:)-a2(:)));
    if resultError<resultErrorTreshold
        if strcmp(displayOpts.displayLevel,'all')
            disp(['The pooling code has been tested for ' num2str(testPoolingImageCount) ' random images and proved to be true']);
            disp(['Sum of the error is ' num2str(resultError,'%4.2f')]);
        end
    else
        if strcmp(displayOpts.displayLevel,'all')
            disp(['Sum of the error is ' num2str(resultError,'%4.2f')]);
        end
        error(['The pooling code has been tested for ' num2str(testPoolingImageCount) ' random images but problems occured']);
        %you can use the following lines to check what should be wrong
        %in the computations
        % firstInputImage = reshape(data(:,:,:,slctdImgs(1)),C_PO.inputSize_original(1:end-1));
        % firstInputImage_pooled_correct = reshape(a1(:,:,:,1),[],1);
        % firstInputImage_pooled_fastButIncorrect = reshape(a2(:,:,:,1),[],1);
    end
end

%% TEST-1
%countFeat = 3;%dimension of 1 sample
%countSamples = 5;%number of samples
%f = 2;%number of previous conv filter
% b = rand(countFeat,countSamples,f)
%     b(:,:,1) =
%         0.2265    0.2713    0.4192    0.0985    0.3331
%         0.9552    0.0655    0.0552    0.9593    0.1113
%         0.0077    0.0096    0.1728    0.5135    0.2058
%     b(:,:,2) =
%         0.4602    0.2203    0.1910    0.6432    0.7467
%         0.2554    0.7395    0.9802    0.0599    0.4788
%         0.0647    0.4599    0.6269    0.4116    0.9325
% [aval,aind] = max(b,[],3)
%     aval =
%         0.4602    0.2713    0.4192    0.6432    0.7467
%         0.9552    0.7395    0.9802    0.9593    0.4788
%         0.0647    0.4599    0.6269    0.5135    0.9325
%     aind =
%          2     1     1     2     2
%          1     2     2     1     2
%          2     2     2     1     2


%% C_PO.poolMethod - deleted code part
%-----------in learnFilters subfunction
%     C_PO.poolMethod = 3;
%     switch (C_PO.poolMethod)
%         case 1
%             filMultipleMap = reshape(1:(prod(C_PO.poolSize)*C_PO.sizeChn*C_PO.sizeChn),  ...
%                                      C_PO.poolSize(1),        ...Rf,
%                                      C_PO.poolSize(2),        ...Cf,
%                                      C_PO.sizeChn,        ...Di,
%                                      C_PO.sizeChn);       %  filCnt
%             %although this poolMap is going to be sparse
%             %first initializing it as zeros and then converting the result to
%             %sparse is very efficient (6 to 7 times faster)
%             poolMap = zeros(dIn,dOut);
%             %how to calculate W_Conv by using the filters??
%             %this will only be calculated once as a mapping parameter
%             for fil = 1:C_PO.sizeChn
%                 curFilMap = reshape(filMultipleMap(:,:,:,fil),[],1);
%                 csff = size(map_singleInput,2);%colSizeForEachFilter
%                 for c = 1:csff
%                     poolMap(map_singleInput(:,c),c + csff*(fil-1)) = curFilMap;
%                 end
%             end
% 
%             % 5975 to 6868 mb but when using sparse 6007 to 6026
%             C_PO.poolMap = sparse(poolMap);
%             %usage of poolMap
%             %poolWeights = zeros(size(poolMap));
%             %poolWeights(poolMap>0) = 1;
%             %poolResult = reshape(poolWeights'*reshape(data,[],countSamples),Ro,Co,filCnt,countSamples)./prod(C_PO.poolSize);
%             %this poolMap seems to be bad for time and memory purposes!!!
%             %in MNIST it takes 200 seconds to pool the second pool (poolMap size - 21632x30000)
%             clear fil curFilMap csff c poolMap filMultipleMap
%         case 2
%             C_PO.poolMap = reshape(map_singleInput',[],1);
%             %usage of poolMap
%             %data = reshape(data,[],C_PO.inputSize_vectorized(2));
%             %data = data(poolMap,:);
%             %tic();data = reshape(data,[prod(C_PO.outputSize_original([1 2])), prod(C_PO.poolSize), C_PO.inputSize_original(3),C_PO.inputSize_original(4)]);toc();
%             %tic();data = permute(data,[1 3 4 2]);toc();
%             %then mean(data,4) or max(data,[],4)
%             %but only the permutation consumes a lot memory and also takes
%             %almost 10 seconds whereas the third case in total takes 9
%             %seconds :)
%     end
%-----------in applyPooling subfunction
%     switch (C_PO.poolMethod)
%         case 1
%             error('This part should be deleted?? Check it out if deletion is necessary');
%             pooledOriginalShapedData = cnnPoolImages(C_PO.poolSize(1), C_PO.inputSize_original, data, C_PO.poolType);
%             pooledVectorizedData = reshape(pooledOriginalShapedData,[],countSamples);
%         case 2
%             error('This part should be deleted?? Check it out if deletion is necessary');
%             Ro = C_PO.outputSize_original(1);
%             Co = C_PO.outputSize_original(2);
%             pooledOriginalShapedData = reshape(data,[],countSamples);
%             if (~testPooling), clear data; end
%             pooledOriginalShapedData = pooledOriginalShapedData(C_PO.poolMap,:);
%             pooledOriginalShapedData = reshape(pooledOriginalShapedData,[Ro*Co, prod(C_PO.poolSize), C_PO.inputSize_original(3),C_PO.inputSize_original(4)]);toc();
%             pooledOriginalShapedData = permute(pooledOriginalShapedData,[1 3 4 2]);toc();
%             switch (C_PO.poolType)
%                 case 'mean'
%                     pooledOriginalShapedData = mean(pooledOriginalShapedData,4);
%                 case 'max'
%                     if ~calculateBackPropMap
%                         pooledOriginalShapedData = max(pooledOriginalShapedData,[],4);
%                     else
%                         [pooledOriginalShapedData,C_PO.backPropInd] = max(pooledOriginalShapedData,[],4);
%                     end
%             end
%     end