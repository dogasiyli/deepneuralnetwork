function [a, cnnnet, time_spent_all] = pooling_ForwardPropogate(cnnnet, layer, data, varargin)
% STEP-1 : initializations
    time_spent_all = cputime;
% STEP-2 : set and get optional param defaults
    [ displayOpts, saveOpts,  activationID,  calculateBackPropMap, memoryMethod, dataStruct, deletePrevLayerBatch] = getOptionalParams(varargin{:});
% STEP-3 : Learn and set input and output sizes
    cnnnet = arrangeLayerSizes(cnnnet, layer);
% STEP-4 : Give the user information about what will be done by printing on the screen
    displayProcessInformation(data, cnnnet, layer, displayOpts);
% STEP-5 : Apply pooling
    [cnnnet, a] = applyPooling(cnnnet, layer, data, calculateBackPropMap, displayOpts, saveOpts, dataStruct, deletePrevLayerBatch);
% STEP-6 : Save the activations for bacpropogation
   cnnnet = pool_saveAct(cnnnet, a, activationID, layer, saveOpts, displayOpts, memoryMethod);    
% STEP-7 : Write the timing information on screen    
    time_spent_all = timingInfo(cnnnet{layer,1}.outputSize_original, time_spent_all, displayOpts);
end

% STEP-2 : set and get optional param defaults
function [ displayOpts,  saveOpts, activationID,  calculateBackPropMap, memoryMethod, dataStruct, deletePrevLayerBatch] = getOptionalParams(varargin)
    %P1D0 -> Passed assed paramaeter = 1, Default value used = 0
    [ displayOpts,  saveOpts,  activationID,  calculateBackPropMap,   memoryMethod  , dataStruct,  deletePrevLayerBatch, P1D0] = parseArgs(...
    {'displayOpts' 'saveOpts' 'activationID' 'calculateBackPropMap'  'memoryMethod'  'dataStruct' 'deletePrevLayerBatch'},...
    {      []          []         0                  false          'all_in_memory'        []             true          },...
    varargin{:});
    %optional param defaults explanations:
    %1. saveOpts             - needed to save the activation outputs of
    %                          pooling step to be able to backpropogate
    %2. displayOpts          - needed to be able to display what is
    %                          happening on the screen such as what
    %                          is the new size of data or how much
    %                          time spent for the pooling procedure
    %3. activationID         - for saving the activations on the
    %                          disk to be able to backpropogate 
    %                          we need to store the id of activations.
    %                          At layer_i we can be dealing with a_<i-2>
    %4. calculateBackPropMap - for max pooling we also need to
    %                          calculate the backpropogation weight matrix 
    %                          at W_<activationID-1>

    %if displayOpts is not passed as a parameter then set is to 'all' by
    %default to show the user what will happen in every detail.
    %also return it as a parameter because even if it is not passed in it
    %would be returned to be used for upcoming evaluations
    if (isempty(displayOpts))
        displayOpts = setDisplayOpts('all');
        if (displayOpts.defaultParamSet && P1D0.displayOpts==0)
            disp([displayOpts.ies{4} 'display options is set to -all- by default.']);
        end
    end
    if (isempty(saveOpts))
        saveOpts = setSaveOpts();
    end
    if ~exist(saveOpts.MainFolder,'dir')
        mkdir(saveOpts.MainFolder);
    end 
    if ~exist([saveOpts.MainFolder saveOpts.SubFolders.ActivatedData],'dir')
        mkdir([saveOpts.MainFolder saveOpts.SubFolders.ActivatedData]);
    end 

    
    %memoryMethod
    if (P1D0.memoryMethod==0)
        if (displayOpts.defaultParamSet)
            disp([displayOpts.ies{4} 'memoryMethod is set to default(all_in_memory).']);
        end
    end
    
    if (displayOpts.defaultParamSet)
        %TODO
        %calculateBackPropMap
        
        %saveOpts
        if (P1D0.saveOpts ==0)
            disp([displayOpts.ies{4} 'saveOpts is set to default(none).']);
        end
    end
end

% STEP-3 : Learn and set input and output sizes
%Tested 2016_03_02
function cnnnet = arrangeLayerSizes(cnnnet, layer)
    %the previous layer can be pooling,convolution or an activation layer of them
    %then following is in action
    if (layer>1)
        if ~isfield(cnnnet{layer,1},'inputSize_original') && isfield(cnnnet{layer-1,1},'outputSize_original')
            cnnnet{layer,1}.inputSize_original = cnnnet{layer-1,1}.outputSize_original;
        end
    end
end

% STEP-4 : Give the user information about what will be done by printing on the screen
%Tested 2016_03_02
function countSamples = displayProcessInformation(data, cnnnet, layer, displayOpts)
    % if time spent needs to be passed here is how it goes
    countSamples = size(data, 2);
    if     (displayOpts.calculation==1 && displayOpts.dataSize==1 && isfield(cnnnet{layer,1},'inputSize_original'))
        disp([displayOpts.ies{4} 'Data of size ' mat2str([cnnnet{layer,1}.inputSize_original(1:end-1) countSamples]) ' will be pooled using <(' cnnnet{layer,1}.poolType ')' cnnnet{layer,1}.type '>.']);       
    elseif (displayOpts.calculation==1 && displayOpts.dataSize==0)
        disp([displayOpts.ies{4} 'Data will be pooled using <(' cnnnet{layer,1}.poolType ')' cnnnet{layer,1}.type '>.']);        
    elseif (displayOpts.calculation==0 && displayOpts.dataSize==0 && ~strcmp(displayOpts.displayLevel,'none'))
        disp([displayOpts.ies{4} 'Data will be pooled.']);        
    end
end

% STEP-5 : Apply pooling
%Tested 2016_03_02
function [cnnnet, data] = applyPooling(cnnnet, layer, data, calculateBackPropMap, displayOpts, saveOpts, dataStruct, deletePrevLayerBatch)
% what really functions here is the line :
% [cnnnet{layer,1}, data] = poolingStep(cnnnet{layer,1}, data, displayOpts, calculateBackPropMap);
% others are all related to display options 
    time_spent_pool = cputime;
        [cnnnet{layer,1}, data] = poolingStep(cnnnet{layer,1}, data, displayOpts, saveOpts, dataStruct, calculateBackPropMap, deletePrevLayerBatch);
    time_spent_pool = cputime - time_spent_pool;
    if (displayOpts.calculation==1)
        countSamples = size(data, 2);
        if     (displayOpts.timeMode< 2 && displayOpts.dataSize==0)
            disp([displayOpts.ies{4} 'Data has been pooled using <(' cnnnet{layer,1}.poolType ')' cnnnet{layer,1}.type '> function.']);
        elseif (displayOpts.timeMode>=2 && displayOpts.dataSize==0)
            disp([displayOpts.ies{4} 'Data has been pooled using <(' cnnnet{layer,1}.poolType ')' cnnnet{layer,1}.type '> function in ' num2str(time_spent_pool,'%4.2f') ' cputime.']);
        elseif (displayOpts.timeMode>=2 && displayOpts.dataSize==1 && isfield(cnnnet{layer,1},'inputSize_original') && isfield(cnnnet{layer,1},'outputSize_original'))
            disp([displayOpts.ies{4} 'Data of size ' mat2str([cnnnet{layer,1}.inputSize_original(1:end-1) countSamples]) ' has been pooled and became ' mat2str([cnnnet{layer,1}.outputSize_original(1:end-1) countSamples]) ' using <(' cnnnet{layer,1}.poolType ')' cnnnet{layer,1}.type '> function in ' num2str(time_spent_pool,'%4.2f') ' cputime.']);
        elseif (displayOpts.timeMode< 2 && displayOpts.dataSize==1 && isfield(cnnnet{layer,1},'inputSize_original') && isfield(cnnnet{layer,1},'outputSize_original'))
            disp([displayOpts.ies{4} 'Data of size ' mat2str([cnnnet{layer,1}.inputSize_original(1:end-1) countSamples]) ' has been pooled and became ' mat2str([cnnnet{layer,1}.outputSize_original(1:end-1) countSamples]) ' using <(' cnnnet{layer,1}.poolType ')' cnnnet{layer,1}.type '> function.']);
        end
    end
end

% STEP-6 : Save the activations for bacpropogation
%Tested 2016_03_02
function cnnnet = pool_saveAct(cnnnet, a, activationID, layerID, saveOpts, displayOpts, memoryMethod)
    %First check if the folder to save is passed as a parameter and the activationID is bigger than 0
    if (activationID>0 && strcmp(memoryMethod,'write_to_disk'))
        %saveNot_preventDueToNone = strcmp(saveOpts.Params.Activations,'None');
        %save_nextLayerInitializationMethod = size(cnnnet,1)>layerID && isfield(cnnnet{layerID+1,1},'initializeWeightMethod') && ~strcmp(cnnnet{layerID+1,1}.initializeWeightMethod,'Random');
        save_onlyLastLayerNeeded = (strcmp(saveOpts.Params.Activations,'Last') && cnnnet{layerID,1}.LastActivationLayer);%if last will be saved
        save_everythingShallBeSaved = (strcmp(saveOpts.Params.Activations,'Every'));%each of them will be saved  
        saveProceedBool = save_onlyLastLayerNeeded || save_everythingShallBeSaved;                    
        if ~saveProceedBool
            return
        end
        clear save_onlyLastLayerNeeded save_everythingShallBeSaved saveProceedBool
        saveActivation(a, saveOpts, activationID, layerID, displayOpts );
        if isfield(cnnnet{activationID,1},'activatedData')
            cnnnet{activationID,1} = rmfield(cnnnet{activationID,1},'activatedData');
        end
    elseif strcmp(memoryMethod,'all_in_memory')
        cnnnet{activationID,1}.activatedData = a;
    end
end

% STEP-7 : Write the timing information on screen
%Tested 2016_03_02
function time_spent_all = timingInfo(outputSize_original, time_spent_all, displayOpts)
    time_spent_all = cputime-time_spent_all;
    if     (displayOpts.timeMode>=2 && displayOpts.dataSize==1)
        disp([displayOpts.ies{4} 'After pooling data size has now become ' mat2str(outputSize_original) ' in ' num2str(time_spent_all,'%4.2f') ' cputime.']);
    elseif (displayOpts.timeMode< 2 && displayOpts.dataSize==1)
        disp([displayOpts.ies{4} 'After pooling data size has now become ' mat2str(outputSize_original) ' in ' num2str(time_spent_all,'%4.2f') ' cputime.']);
    elseif (displayOpts.timeMode>=2 && displayOpts.dataSize==0)
        disp([displayOpts.ies{4} 'Data is pooled in ' num2str(time_spent_all,'%4.2f') ' cputime.']);
    end
end
