function [cnnnet, resultMat, featIndsSorted, featIDsToDel, accuracyCalced_Init, expectedAccuracy] = reduceLastLayerWeights( cnnnet, lastLayerActivation_data, lastLayerActivation_labels, percentAllowedToDecrease)
%UNT�TLED2 Summary of this function goes here
%   Detailed explanation goes here

layerCount = size(cnnnet,1);
W_opt = cnnnet{layerCount,1}.Weight;
error('analyseResults function should also take bias params for the last layer into account');
[~, accuracyCalced_Init] = analyzeResults([], lastLayerActivation_labels, [], W_opt, false, lastLayerActivation_data);

%now what if I delete some of the data and features???
%can I delete some hidden layer outputs and get the almost same accuracy?
maxFeatCountToDelete = 100;

    W_sum_feats = sum(abs(W_opt));
    data_sum_feats = sum(abs(lastLayerActivation_data),2);

    W_sum_feats = W_sum_feats./max(W_sum_feats(:));
    data_sum_feats = data_sum_feats./max(data_sum_feats(:));
    
    featEvalResults = W_sum_feats.*data_sum_feats';
    [featValsSorted, featIndsSorted] = sort(featEvalResults);
 
    resultMat = zeros(maxFeatCountToDelete,3);
    for i=1:maxFeatCountToDelete
        %now delete W_opt and lastLAyerActData and save accuracy results
        featIDsToDel = featIndsSorted(1:i);
        [curW, curData] = getRemovedDataAndWeights(featIDsToDel, W_opt, lastLayerActivation_data);  
        [~, accuracyCalced] = analyzeResults([], lastLayerActivation_labels, [], curW, false, curData);
        resultMat(i,:) = [i accuracyCalced accuracyCalced_Init-accuracyCalced];
    end
    rowToSelect = find((accuracyCalced_Init-resultMat(:,2))>percentAllowedToDecrease, 1, 'first');
    featIDsToDel = sort(featIndsSorted(1:rowToSelect));
    curW = getRemovedDataAndWeights(featIDsToDel, W_opt); 
    expectedAccuracy = resultMat(rowToSelect-1,2);
    
    %remove the weights in the softmax layer
    cnnnet{layerCount,1}.Weight = curW(:);
    
    prevConvLayerID = layerCount-3;
    %change the filter count in previous convolution layer
    cnnnet{prevConvLayerID,1}.countFilt = size(curW,2);
    %remove the weights in previous convolution layer
    cnnnet{prevConvLayerID,1}.filterParams.W(featIDsToDel,:) = [];
    cnnnet{prevConvLayerID,1}.filtersWhitened(featIDsToDel,:) = [];
    %remove the biases in previous convolution layer
    cnnnet{prevConvLayerID,1}.filterParams.b(featIDsToDel,:) = [];
    cnnnet{prevConvLayerID,1}.biasesWhitened(featIDsToDel,:) = [];
    %change the outputsize original value
    cnnnet{prevConvLayerID,1}.outputSize_original(3) = size(curW,2);
    %change the outputsize vectorized value
    cnnnet{prevConvLayerID,1}.outputSize_vectorized = [prod(cnnnet{prevConvLayerID,1}.outputSize_original(1:end-1)) cnnnet{prevConvLayerID,1}.outputSize_original(4)];
    %remove the params to re-learn
    if isfield(cnnnet{prevConvLayerID, 1},'W_ConvMap')
        cnnnet{prevConvLayerID, 1} = rmfield(cnnnet{prevConvLayerID, 1},'W_ConvMap');
    end
    if isfield(cnnnet{prevConvLayerID, 1},'W_Conv_Mapped')
        cnnnet{prevConvLayerID, 1} = rmfield(cnnnet{prevConvLayerID, 1},'W_Conv_Mapped');
    end
    if isfield(cnnnet{prevConvLayerID+1, 1},'activatedData')
        cnnnet{prevConvLayerID+1, 1} = rmfield(cnnnet{prevConvLayerID+1, 1},'activatedData');
    end
    
    prevPoolLayerID = layerCount-1;
    %change the filter count in previous convolution layer
    cnnnet{prevPoolLayerID,1}.sizeChn = size(curW,2);
    %change the outputsize original value
    cnnnet{prevPoolLayerID,1}.inputSize_original(3) = size(curW,2);
    cnnnet{prevPoolLayerID,1}.outputSize_original(3) = size(curW,2);
    %change the outputsize vectorized value
    cnnnet{prevPoolLayerID,1}.outputSize_vectorized(1) = prod(cnnnet{prevPoolLayerID,1}.outputSize_original(1:end-1));
    cnnnet{prevPoolLayerID,1}.inputSize_vectorized(1) = prod(cnnnet{prevPoolLayerID,1}.inputSize_original(1:end-1));
    %remove the params to re-learn
    if isfield(cnnnet{prevPoolLayerID, 1},'poolMap')
        cnnnet{prevPoolLayerID, 1} = rmfield(cnnnet{prevPoolLayerID, 1},'poolMap');
    end
    
    finalSoftMaxLayerID = layerCount;
    %change the filter count in final softMAx layer
    cnnnet{finalSoftMaxLayerID,1}.countFeat = size(curW,2);
    %change the inputsize original/vectorized value
    cnnnet{finalSoftMaxLayerID,1}.inputSize_original(3) = size(curW,2);
    cnnnet{finalSoftMaxLayerID,1}.inputSize_vectorized(1) = prod(cnnnet{finalSoftMaxLayerID,1}.inputSize_original(1:end-1));
end

function [curW, curData] = getRemovedDataAndWeights(featIDsToDel, W_opt, lastLayerActivation_data)
    curW = W_opt;
    curW(:,featIDsToDel) = [];
    if nargout>1 && nargin>2
        curData = lastLayerActivation_data;
        curData(featIDsToDel,:) = [];
    elseif nargout>1 && nargin<=2
        curData=[];
        error('what the!!');
    end
end