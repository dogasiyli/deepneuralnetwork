function [ grad ] = roll_grad_from_stacks( W_grad,  b_grad)
    grad = [];
    for i = 1:size(W_grad,2)
        grad = [grad;W_grad{i}(:)];
    end
    for i = 1:size(b_grad,2)
        grad = [grad;b_grad{i}(:)];
    end
end

