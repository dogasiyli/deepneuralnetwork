function [ predictionsOut, accuracyTestDeep, confTestDeep, probabilitiesOut ] = runTestData( testData, testLabels, cnnnet, dataStruct, displayOpts, saveOpts)
    layerCount = size(cnnnet,1);
    time_Layer_test = zeros(1,layerCount);
    activationID = 1;
    for layer = 1:layerCount
        if (displayOpts.layerNames==1)
            disp([displayOpts.ies{4} '++++Layer Name : ' cnnnet{layer,1}.name '++++']);
        end
        switch cnnnet{layer,1}.type
            case 'convolution'
                [testData, cnnnet, time_Layer_test(layer)] = convolution_ForwardPropogate(  cnnnet, layer, testData ...
                                                                                           ,'displayOpts', displayOpts...
                                                                                           ,'saveOpts',    saveOpts);
            case 'activation'
                activationID = activationID + 1;
                [testData, cnnnet, time_Layer_test(layer)] = activation_ForwardPropogate(  cnnnet, layer, testData     ...
                                                                                          ,'saveOpts' , saveOpts ...
                                                                                          ,'displayOpts' , displayOpts ...
                                                                                          ,'activationID', activationID);             
            case 'pooling'
                activationID = activationID + 1;
                [testData, cnnnet, time_Layer_test(layer)] = pooling_ForwardPropogate( cnnnet, layer, testData       ...
                                                                                      ,'saveOpts' , saveOpts   ...
                                                                                      ,'displayOpts' , displayOpts   ...
                                                                                      ,'activationID', activationID);
            case 'softmax'
                [testData, cnnnet, predictionsOut, time_Layer_test(layer), probabilitiesOut] = softMax_ForwardPropogate( cnnnet, layer, testData     ...
                                                                                                                         ,'saveOpts' , saveOpts ...
                                                                                                                         ...,'labels', testLabels...
                                                                                                                         ,'displayOpts', displayOpts);
            case 'fullyconnected'
                [testData, cnnnet, time_Layer_test(layer)] = fullyConnected_ForwardPropogate( cnnnet, layer, testData ...
                                                                                             ,'saveOpts', saveOpts...
                                                                                             ,'displayOpts', displayOpts);
            otherwise
                 error('You are suggesting a layer that is not present. Layers are - convolution, activation, pooling, softmax, fullyconnected');
        end
        displayMemory;  
        disp('--------Next Layer---------');
    end
    if (displayOpts.timeMode>=1)
        disp(['Test data has been propogated in total of ' num2str(sum(time_Layer_test),'%4.2f') ' cputime.']);
    end
    if (~strcmp(displayOpts.confusionMat,'None'))
        disp('Test results of deep net');
    end
    [accuracyTestDeep, confTestDeep] = calculateConfusion(predictionsOut, testLabels, ~strcmp(displayOpts.confusionMat,'None'),cnnnet{end,1}.countCategory);
end

