function time_spent_save = saveActivation(a, saveOpts, activationID, layerID, displayOpts )
    folderNameForActivationData = [saveOpts.MainFolder saveOpts.SubFolders.ActivatedData];
    if (displayOpts.fileOperations)
        disp([displayOpts.ies{4} 'Layer(' num2str(layerID) ') activations (a_' num2str(activationID,'%d') ') is being saved']);
    end
    time_spent_save = cputime;
        save([ folderNameForActivationData 'Activation_' num2str(activationID,'%02d') '.mat'],'a','-v7.3');
    time_spent_save = cputime - time_spent_save;    
    if     (displayOpts.fileOperations==1 && displayOpts.timeMode>=2)
        disp([displayOpts.ies{4} 'Activation layer(' num2str(layerID) ') activations (a_' num2str(activationID,'%d') ') is saved at ' folderNameForActivationData 'Activation_' num2str(activationID,'%02d') '.mat in (' num2str(time_spent_save,'%4.2f') ') cputime.']);
    elseif (displayOpts.fileOperations==1 && displayOpts.timeMode< 2)
        disp([displayOpts.ies{4} 'Activation layer(' num2str(layerID) ') activations (a_' num2str(activationID,'%d') ') is saved at ' folderNameForActivationData 'Activation_' num2str(activationID,'%02d') '.mat']);
    end 
end

