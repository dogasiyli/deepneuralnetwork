function outputFilterStats = saveAllBatchesToActivatedDataFolder(C_layerCurrent, dataStruct, displayOpts, saveOpts, funObj, initialBatchOutData, deletePrevLayerBatch)
    curLayerID = C_layerCurrent.layerIndice;
    if exist('dataStruct','var') 
        countBatch = dataStruct.countBatch.train;
    else
        countBatch = 1;
    end
    if isfield(C_layerCurrent,'countFilt')
        filterCount = C_layerCurrent.countFilt;
    elseif isfield(C_layerCurrent,'outputSize_original')
        filterCount = C_layerCurrent.outputSize_original(end-1);
    end
    
    outputFilterStats = zeros(filterCount,4,countBatch);
    for batchID=1:countBatch
        globalOutputStruct('updateEpochBatchParams',struct('batchCount',countBatch,'batchID',batchID));
        %load the data to convolve/pool/act/fullyconn
        [X_in, labels] = loadAndSaveForInitialLearning(curLayerID, batchID, countBatch, displayOpts, saveOpts);
        if batchID==1
            X_out = initialBatchOutData;
        else
            X_out = feval(funObj, C_layerCurrent, X_in, displayOpts);
        end
        outputFilterStats(:,:,batchID) = calculateMeanVar(X_out, C_layerCurrent.outputSize_original);
        globalOutputStruct('updateFilterStatsTrack',squeeze(outputFilterStats(:,:,batchID)),'layerID',curLayerID,'insertMode',1);
        %save the convolved data
        loadAndSaveForInitialLearning(curLayerID, batchID, countBatch, displayOpts, saveOpts, X_out, labels, deletePrevLayerBatch);
    end
    %get it back to batchID=1
    globalOutputStruct('updateEpochBatchParams',struct('batchCount',countBatch,'batchID',1));
end