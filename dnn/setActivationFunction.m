function [act, actDerivative, actCatalizor] = setActivationFunction(activationFunctionName)
    actCatalizor = [];%only for tanh
    switch (activationFunctionName) 
        %z = W'x + b
        case 'tanh'
            %f(z) = tanh(z) = (e^z - e^-z)/(e^z + e^-z)
            %f'(z) = 1 ? (f(z))^2
            actCatalizor = @(expZ_,expZMinus_) (expZ_-expZMinus_) ./ (expZ_+expZMinus_);
            act = @(z_) activatonCell.actCatalizor(exp(z_),exp(-z_));
            actDerivative = @(a_) a_.*(1-a_);
        case 'relu'
            act = @(z_) max(0,z_);
            actDerivative = @(a_) double(a_>0);
            %f(z)  --> if z<0 make it 0
            %f'(z)  --> if z>0 1 else 0
        case 'relu_eps'
            %http://mochajl.readthedocs.io/en/latest/user-guide/neuron.html
            act = @(z_) max(eps,z_);
            actDerivative = @(a_) double((a_-eps)>0);
            %f(z)  --> if z<eps make it 0
            %f'(z)  --> if z+eps>0 1 else 0
        case 'fastSigmoid'
            %f(z)  --> f(x) = x / (1 + abs(x))
            %f'(z)  --> if z>0 1 else 0
            act = @(z_) z_ ./ (1.0 + abs(z_));
            actDerivative = @(a_) a_.*(1-a_);
        %case 'sigm'
            %f(z) = 1 / (1 + exp(-z))
            %f'(z) = f(z).*(1 - f(z))
        otherwise
            act = @(z_) 1.0 ./ (1.0 + exp(-z_));
            actDerivative = @(a_) a_.*(1-a_);
    end
end