function displayOpts = setDisplayOpts(displayLevel)
%   displayLevel can be passed into the function.
%   anything in varargin overwrites what ever is in given in the
%   displayLevel option.
    if (~exist('displayLevel','var') || isempty(displayLevel))
        %1. 'default'
        %2. 'minimal'
        %3. 'none'
        %4. 'nofigures_allstrings'
        %5. 'all'
        %6. 'custom'
        displayLevel = 'default';
    end

    displayOpts.displayLevel = displayLevel;
    switch displayLevel
        case 'minimal'
            displayOpts.layerNames = 1;
            displayOpts.calculation = 0;  
            displayOpts.memOpts = 0;
            displayOpts.timeMode = 1; 
            displayOpts.steps = 1;
            displayOpts.dataSize = 0;
            displayOpts.fileOperations = 0;
            displayOpts.warningInfo = 0;
            displayOpts.defaultParamSet = 0;
            displayOpts.confusionMat = 'Last';%'None','Last','Every'

            figureOpts.Show.Iterations = 'None';%'None','Every'
            figureOpts.Show.Confusion = 'Result';%'None','Result','Every'
            figureOpts.Layers.Conv = false;
            figureOpts.Layers.Full = false;
            figureOpts.Layers.Soft = false;
            figureOpts.Layers.Act = false;
            figureOpts.Layers.Pool = false;
        case 'none'
            displayOpts.layerNames = 0;
            displayOpts.calculation = 0;  
            displayOpts.memOpts = 0;
            displayOpts.timeMode = 0; 
            displayOpts.steps = 0;
            displayOpts.dataSize = 0;
            displayOpts.fileOperations = 0;
            displayOpts.warningInfo = 0;
            displayOpts.defaultParamSet = 0;
            displayOpts.confusionMat = 'None';%'None','Last','Every'

            figureOpts.Show.Iterations = 'None';%'None','Every'
            figureOpts.Show.Confusion = 'None';%'None','Result','Every'
            figureOpts.Layers.Conv = false;
            figureOpts.Layers.Full = false;
            figureOpts.Layers.Soft = false;
            figureOpts.Layers.Act = false;
            figureOpts.Layers.Pool = false;
        case 'nofigures_allstrings'
            displayOpts.layerNames = 1;
            displayOpts.calculation = 1;  
            displayOpts.memOpts = 1;
            displayOpts.timeMode = 2; 
            displayOpts.steps = 1;
            displayOpts.dataSize = 1;
            displayOpts.fileOperations = 1;
            displayOpts.warningInfo = 1;
            displayOpts.defaultParamSet = 1;
            displayOpts.confusionMat = 'Every';%'None','Last','Every'

            figureOpts.Show.Iterations = 'None';%'None','Every'
            figureOpts.Show.Confusion = 'None';%'None','Result','Every'
            figureOpts.Layers.Conv = false;
            figureOpts.Layers.Full = false;
            figureOpts.Layers.Soft = false;
            figureOpts.Layers.Act = false;
            figureOpts.Layers.Pool = false;
        case 'all'
            displayOpts.layerNames = 1;
            displayOpts.calculation = 1;  
            displayOpts.memOpts = 1;
            displayOpts.timeMode = 3; 
            displayOpts.steps = 1;
            displayOpts.dataSize = 1;
            displayOpts.fileOperations = 1;
            displayOpts.warningInfo = 1;
            displayOpts.defaultParamSet = 1;
            displayOpts.confusionMat = 'Every';%'None','Last','Every'

            figureOpts.Show.Iterations = 'Every';%'None','Every'
            figureOpts.Show.Confusion = 'Every';%'None','Result','Every'
            figureOpts.Layers.Conv = true;
            figureOpts.Layers.Full = true;
            figureOpts.Layers.Soft = true;
            figureOpts.Layers.Act = true;
            figureOpts.Layers.Pool = true;
        case 'confusionfigures_allstrings'
            displayOpts.layerNames = 1;
            displayOpts.calculation = 1;  
            displayOpts.memOpts = 1;
            displayOpts.timeMode = 2; 
            displayOpts.steps = 1;
            displayOpts.dataSize = 1;
            displayOpts.fileOperations = 1;
            displayOpts.warningInfo = 1;
            displayOpts.defaultParamSet = 1;
            displayOpts.confusionMat = 'Every';%'None','Last','Every'

            figureOpts.Show.Iterations = 'Every';%'None','Every'
            figureOpts.Show.Confusion = 'Every';%'None','Result','Every'
            figureOpts.Layers.Conv = false;
            figureOpts.Layers.Full = false;
            figureOpts.Layers.Soft = false;
            figureOpts.Layers.Act = false;
            figureOpts.Layers.Pool = false;
        case 'custom'
            displayOpts.layerNames = 1;
            displayOpts.calculation = 0;  
            displayOpts.memOpts = 0;
            displayOpts.timeMode = 2; 
            displayOpts.steps = 1;
            displayOpts.dataSize = 1;
            displayOpts.fileOperations = 1;
            displayOpts.warningInfo = 0;
            displayOpts.defaultParamSet = 1;
            displayOpts.confusionMat = 'Every';%'None','Last','Every'

            figureOpts.Show.Iterations = 'Every';%'None','Every'
            figureOpts.Show.Confusion = 'Every';%'None','Result','Every'
            figureOpts.Layers.Conv = true;
            figureOpts.Layers.Full = true;
            figureOpts.Layers.Soft = true;
            figureOpts.Layers.Act = true;
            figureOpts.Layers.Pool = true;
        %case 'default'
        otherwise
            displayOpts.layerNames = 1;
            displayOpts.calculation = 1;  
            displayOpts.memOpts = 1;
            displayOpts.timeMode = 0; 
            displayOpts.steps = 1;
            displayOpts.dataSize = 1;
            displayOpts.fileOperations = 1;
            displayOpts.warningInfo = 0;
            displayOpts.defaultParamSet = 1;
            displayOpts.confusionMat = 'Every';%'None','Last','Every'
            displayOpts.displayLevel = 'default';

            figureOpts.Show.Iterations = 'Every';%'None','Every'
            figureOpts.Show.Confusion = 'Every';%'None','Result','Every'
            figureOpts.Layers.Conv = true;
            figureOpts.Layers.Full = true;
            figureOpts.Layers.Soft = true;
            figureOpts.Layers.Act = true;
            figureOpts.Layers.Pool = true;
    end
    displayOpts.figureOpts = figureOpts;
    displayOpts.ies = {'.';'..';'...';'....';'.....';'......';'.......';'........';'.........';'..........'};
end