function columnNamesCells = setIterativeCellStr( initialStr, cellCount )
    columnNamesCells = repmat({[initialStr 'XX']},1,cellCount);
    for i=1:cellCount
        columnNamesCells{1,i} = strrep(columnNamesCells{1,i},'XX',num2str(i,'%02d'));
    end
end

