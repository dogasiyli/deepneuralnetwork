function [ paramsELM ] = setParams_ELM(paramsCategory)
%setParams_minFunc set minFunc options according to custom designs
%   different minfunc options can be wanted.
%   we will set different designs here
    if (~exist('paramsCategory','var') || isempty(paramsCategory))
        paramsCategory = 'default';
    end
    paramsELM = struct;
    paramsELM.paramsCategory = paramsCategory;
    
    %Same for all now - 
    paramsELM.scaling = 2.0; %scaling of input weights - 2 in this case
    paramsELM.ridgeParam = 1e-8; %ridge regression parameter - 1e-8 for this case
    switch paramsCategory  
        case 'Standard'
            paramsELM.randomWeightInitialization = 'Standard';
            paramsELM.useReceptiveFields = false;
        case 'CIW'
            paramsELM.randomWeightInitialization = 'ComputedInputWeights';
            paramsELM.useReceptiveFields = true;
            paramsELM.rf.minMaskSize = 10;%parameter for  receptive fields(e.g. 10)
            paramsELM.rf.border = 3;%parameter for receptive fields(e.g. 3)
        case 'Constrained'
            paramsELM.randomWeightInitialization = 'ConstrainedWeights';
            paramsELM.useReceptiveFields = false;
        otherwise%custom - optimum conditions(default)
            paramsELM.randomWeightInitialization = 'ConstrainedWeights';
            paramsELM.useReceptiveFields = false;
    end
end