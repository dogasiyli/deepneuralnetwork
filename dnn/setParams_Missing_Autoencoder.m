function paramsAutoEncode = setParams_Missing_Autoencoder(paramsAutoEncode)
    %set the default autoencoder parameters if some/all of the autoencoder parameters are not passed
    if ~exist('paramsAutoEncode','var') || isempty(paramsAutoEncode) || ~isstruct(paramsAutoEncode)
        paramsAutoEncode = struct;
    end
    if ~isfield(paramsAutoEncode,'thetaInitMode')
        paramsAutoEncode.thetaInitMode = 'UseData';% the method for initializing theta. <'UseData';'Random'>
    end
    if ~isfield(paramsAutoEncode,'initModeData') && strcmp(paramsAutoEncode.thetaInitMode,'UseData')
        paramsAutoEncode.initModeData = 'PCA';% the method for initializing theta when [thetaInitMode=='UseData']. <'PCA'/'LDA'/'SparseClassifier'/'Classifier'>
    end  
    if ~isfield(paramsAutoEncode,'sparsityParam')
        paramsAutoEncode.sparsityParam = 0.1;% desired average activation of the hidden units.
    end
    if ~isfield(paramsAutoEncode,'weightDecayParam')            
        paramsAutoEncode.weightDecayParam = 3e-3; % weight decay parameter
    end
    if ~isfield(paramsAutoEncode,'sparsePenaltyWeight')            
        paramsAutoEncode.sparsePenaltyWeight = 3; % weight of sparsity penalty term
    end
    if ~isfield(paramsAutoEncode,'activationType')            
        paramsAutoEncode.activationType = 'sigmoid';       
    end
    if ~isfield(paramsAutoEncode,'maxIter')            
        paramsAutoEncode.maxIter = 100;        
    end
end