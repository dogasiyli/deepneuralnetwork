function paramsELM = setParams_Missing_ELM(paramsELM)
    %set the default autoencoder parameters if some/all of the autoencoder parameters are not passed
    if ~exist('paramsELM','var') || isempty(paramsELM)
        paramsELM = struct;
    end
    if ~isfield(paramsELM,'randomWeightInitialization')
        paramsELM.randomWeightInitialization = 'Standard';
    end
    if ~isfield(paramsELM,'useReceptiveFields')            
        paramsELM.useReceptiveFields = false;
    end
    if ~isfield(paramsELM,'scaling')            
        paramsELM.scaling = 2.0;
    end
    if ~isfield(paramsELM,'ridgeParam')            
        paramsELM.ridgeParam = 1e-8;
    end
    if paramsELM.useReceptiveFields
        if ~isfield(paramsELM,'sparsePenaltyWeight')            
            paramsELM.rf.minMaskSize = 10;%parameter for  receptive fields(e.g. 10)
        end
        if ~isfield(paramsELM,'activationType')            
            paramsELM.rf.border = 3;%parameter for receptive fields(e.g. 3)  
        end        
    end
end