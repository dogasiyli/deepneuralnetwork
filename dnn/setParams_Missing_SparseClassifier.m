function paramsSparseClassifier = setParams_Missing_SparseClassifier(paramsSparseClassifier)
    %set the default autoencoder parameters if some/all of the autoencoder parameters are not passed
    if ~exist('paramsSparseClassifier','var') || isempty(paramsSparseClassifier) || ~isstruct(paramsSparseClassifier)
        paramsSparseClassifier = struct;
    end
    if ~isfield(paramsSparseClassifier,'thetaInitMode')
        paramsSparseClassifier.thetaInitMode = 'Random';% the method for initializing theta. <'UseData';'Random'>
    end
    if ~isfield(paramsSparseClassifier,'initModeData') && strcmp(paramsSparseClassifier.thetaInitMode,'UseData')
        paramsSparseClassifier.initModeData = 'LDA';% the method for initializing theta when [thetaInitMode=='UseData']. <'PCA'/'LDA'/'SparseClassifier'/'Classifier'>
    end
    if ~isfield(paramsSparseClassifier,'sparsityParam')
        paramsSparseClassifier.sparsityParam = 0.001;% desired average activation of the hidden units.
    end
    if ~isfield(paramsSparseClassifier,'weightDecayParam')            
        paramsSparseClassifier.weightDecayParam = 3e-3; % weight decay parameter
    end
    if ~isfield(paramsSparseClassifier,'sparsePenaltyWeight')            
        paramsSparseClassifier.sparsePenaltyWeight = 1; % weight of sparsity penalty term
    end
    if ~isfield(paramsSparseClassifier,'activationType')            
        paramsSparseClassifier.activationType = 'sigmoid';       
    end
    if ~isfield(paramsSparseClassifier,'maxIter')            
        paramsSparseClassifier.maxIter = 30;        
    end
end