function paramsAutoEncode = setParams_autoEncoder(paramsCategory_AutoEncode)
%setParams_Autoencoder   sets the autoencoder parameters.
%   TODO : extra info will be written here
%  'sizeHid'
%  - It would be a good idea to set the hidden size parameter
%  - If not set it will be set to the half size of the visible size
%  - Basically this parameter tells us :
%  - How many features to be learned
%
%  'sparsityParam'
%       desired average activation of the hidden units.
%       (This was denoted by the Greek alphabet rho, which looks like a lower-case "p",
%       in the lecture notes). 
%  'weightDecay'
%       weight decay parameter
%       lambda in lecture notes
%  'sparsePenaltyWeight'
%       weight of sparsity penalty term
%       beta in lecture notes
%  'maxIter'???
    
    if (~exist('paramsCategory_AutoEncode','var') || isempty(paramsCategory_AutoEncode))
        paramsCategory_AutoEncode = 'custom';
    end
    
    paramsAutoEncode = struct;
    paramsAutoEncode.paramsCategory_AutoEncode = paramsCategory_AutoEncode;
    paramsAutoEncode.thetaInitMode = 'UseData';%,'Random';
    paramsAutoEncode.initModeData = 'PCA';%,'PCA'/'LDA'/'SparseClassifier'/'Classifier';
    switch paramsCategory_AutoEncode
        case 'custom'
            paramsAutoEncode.sparsityParam = 0.01;
            paramsAutoEncode.weightDecayParam = 0.0001;  
            paramsAutoEncode.sparsePenaltyWeight = 3;
            paramsAutoEncode.activationType = 'sigmoid'; 
        otherwise
            paramsAutoEncode.sparsityParam = 0.01;
            paramsAutoEncode.weightDecayParam = 0.0001;  
            paramsAutoEncode.sparsePenaltyWeight = 3;
            paramsAutoEncode.activationType = 'sigmoid';             
    end
end