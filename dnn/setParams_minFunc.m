function [ paramsMinFunc ] = setParams_minFunc(paramsCategory)
%setParams_minFunc set minFunc options according to custom designs
%   different minfunc options can be wanted.
%   we will set different designs here
    if (~exist('paramsCategory','var') || isempty(paramsCategory))
        paramsCategory = 'showInfo_saveLog';
    end
    paramsMinFunc = struct;
    paramsMinFunc.paramsCategory = paramsCategory;
    paramsMinFunc.maxFunEvals = 5000;
    switch paramsCategory  
        case {'showInfo_saveLog','default'}%optimum conditions(default)
            paramsMinFunc.maxIter = 200;
            paramsMinFunc.display = 'on';
            paramsMinFunc.logfile = 'minFuncLog.txt';
            paramsMinFunc.Method = 'lbfgs';
        case 'showInfo_noLog'
            paramsMinFunc.maxIter = 200;
            paramsMinFunc.display = 'on';
            paramsMinFunc.logfile = [];
            paramsMinFunc.Method = 'lbfgs';   
        case 'showNone_noLog'
            paramsMinFunc.maxIter = 200;
            paramsMinFunc.display = 'off';
            paramsMinFunc.logfile = [];
            paramsMinFunc.Method = 'lbfgs';   
        case 'runShort_showInfo'
            paramsMinFunc.maxIter = 10;
            paramsMinFunc.display = 'on';
            paramsMinFunc.logfile = [];
            paramsMinFunc.Method = 'lbfgs';   
        case 'runLong_showNone_saveLog'
            paramsMinFunc.maxIter = 400;
            paramsMinFunc.display = 'off';
            paramsMinFunc.logfile = 'minFuncLog.txt';
            paramsMinFunc.Method = 'lbfgs';   
        otherwise%custom
            paramsMinFunc.maxIter = 200;
            paramsMinFunc.display = 'off';
            paramsMinFunc.logfile = 'minFuncLog.txt';
            paramsMinFunc.Method = 'lbfgs';  
            paramsMinFunc.paramsCategory = ['custom(' paramsCategory ')'];
    end
end

