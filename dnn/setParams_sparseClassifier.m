function paramsSparseClassifier = setParams_sparseClassifier(paramsCategory_SparseClassifier)
%setParams_Autoencoder   sets the autoencoder parameters.
%   TODO : extra info will be written here
%  'sizeHid'
%  - It would be a good idea to set the hidden size parameter
%  - If not set it will be set to the half size of the visible size
%  - Basically this parameter tells us :
%  - How many features to be learned
%
%  'sparsityParam'
%       desired average activation of the hidden units.
%       (This was denoted by the Greek alphabet rho, which looks like a lower-case "p",
%       in the lecture notes). 
%  'weightDecay'
%       weight decay parameter
%       lambda in lecture notes
%  'sparsePenaltyWeight'
%       weight of sparsity penalty term
%       beta in lecture notes
%  'maxIter'???
    
    if (~exist('paramsCategory_SparseClassifier','var') || isempty(paramsCategory_SparseClassifier))
        paramsCategory_SparseClassifier = 'custom';
    end
    
    paramsSparseClassifier = struct;
    paramsSparseClassifier.paramsCategory_SparseClassifier = paramsCategory_SparseClassifier;
    paramsSparseClassifier.thetaInitMode = 'Random';%,'UseData';
    %paramsSparseClassifier.initModeData = 'LDA';% the method for initializing theta when [thetaInitMode=='UseData']. <'PCA'/'LDA'/'SparseClassifier'/'Classifier'>
    paramsSparseClassifier.maxIter = 30;
    switch paramsCategory_SparseClassifier
        case 'custom'
            paramsSparseClassifier.sparsityParam = 0.001;
            paramsSparseClassifier.weightDecayParam = 0.0001;  
            paramsSparseClassifier.sparsePenaltyWeight = 1;
            paramsSparseClassifier.activationType = 'sigmoid'; 
        otherwise
            paramsSparseClassifier.sparsityParam = 0.001;
            paramsSparseClassifier.weightDecayParam = 0.0001;  
            paramsSparseClassifier.sparsePenaltyWeight = 1;
            paramsSparseClassifier.activationType = 'sigmoid';             
    end
end