function saveOpts = setSaveOpts(saveLevel, saveMainFolder)
%setSaveOpts 
    global fastDiskTempPath;
%   saveLevel can be passed into the function.
    if (~exist('saveLevel','var') || isempty(saveLevel))
        %1. 'LayerEvery_IterationEvery'
        %2. 'LayerEvery_IterationLast'
        %3. 'LayerConv_IterationLast'
        %4. 'LayerConv_IterationEvery'
        %5. 'minimal'
        %6. 'none'
        %7. 'custom'
        saveLevel = 'none';
    end
    saveOpts.saveLevel = saveLevel;
    switch saveLevel
        case 'LayerEvery_IterationEvery'
            saveOpts.Params.Weights.Conv = true;
            saveOpts.Params.Weights.Full = true;
            saveOpts.Params.Weights.Soft = true;
            saveOpts.Params.Weights.Iteration = 'Every';%'None','Last','Every'
            saveOpts.Params.Activations = 'Every';%'None','First','Last','Every'
            saveOpts.Params.WeightDiff = true;        
            saveOpts.Figures.Iterations = 'Every';%'None','Last','Every'
            saveOpts.Figures.Results = 'Every';%'None','Last','Every'
            saveOpts.Figures.WeightDiff = 'Every';%'None','Last','Every'
        case 'LayerEvery_IterationLast'
            saveOpts.Params.Weights.Conv = true;
            saveOpts.Params.Weights.Full = true;
            saveOpts.Params.Weights.Soft = true;
            saveOpts.Params.Weights.Iteration = 'Last';%'None','Last','Every'
            saveOpts.Params.Activations = 'Every';%'None','First','Last','Every'
            saveOpts.Params.WeightDiff = true;        
            saveOpts.Figures.Iterations = 'Last';%'None','Last','Every'
            saveOpts.Figures.Results = 'Last';%'None','Last','Every'
            saveOpts.Figures.WeightDiff = 'Last';%'None','Last','Every'
        case 'LayerConv_IterationLast'
            saveOpts.Params.Weights.Conv = true;
            saveOpts.Params.Weights.Full = false;
            saveOpts.Params.Weights.Soft = false;
            saveOpts.Params.Weights.Iteration = 'Last';%'None','Last','Every'
            saveOpts.Params.Activations = 'None';%'None','First','Last','Every'
            saveOpts.Params.WeightDiff = true;
            saveOpts.Figures.Iterations = 'Last';%'None','Last','Every'
            saveOpts.Figures.Results = 'Last';%'None','Last','Every'
            saveOpts.Figures.WeightDiff = 'Last';%'None','Last','Every'
        case 'LayerConv_IterationEvery'
            saveOpts.Params.Weights.Conv = true;
            saveOpts.Params.Weights.Full = false;
            saveOpts.Params.Weights.Soft = false;
            saveOpts.Params.Weights.Iteration = 'Every';%'None','Last','Every'
            saveOpts.Params.Activations = 'None';%'None','First','Last','Every'
            saveOpts.Params.WeightDiff = true;
            saveOpts.Figures.Iterations = 'Every';%'None','Last','Every'
            saveOpts.Figures.Results = 'Every';%'None','Last','Every'
            saveOpts.Figures.WeightDiff = 'Every';%'None','Last','Every'
        case 'minimal'
            saveOpts.Params.Weights.Conv = false;
            saveOpts.Params.Weights.Full = false;
            saveOpts.Params.Weights.Soft = false;
            saveOpts.Params.Weights.Iteration = 'None';%'None','Last','Every'
            saveOpts.Params.Activations = 'None';%'None','First','Last','Every'
            saveOpts.Params.WeightDiff = true;
            saveOpts.Figures.Iterations = 'Last';%'None','Last','Every'
            saveOpts.Figures.Results = 'Last';%'None','Last','Every'
            saveOpts.Figures.WeightDiff = 'Last';%'None','Last','Every'
        case 'none'
            saveOpts.Params.Weights.Conv = false;
            saveOpts.Params.Weights.Full = false;
            saveOpts.Params.Weights.Soft = false;
            saveOpts.Params.Weights.Iteration = 'None';%'None','Last','Every'
            saveOpts.Params.Activations = 'None';%'None','First','Last','Every'
            saveOpts.Params.WeightDiff = true;
            saveOpts.Figures.Iterations = 'None';%'None','Last','Every'
            saveOpts.Figures.Results = 'None';%'None','Last','Every'
            saveOpts.Figures.WeightDiff = 'None';%'None','Last','Every'
        case 'custom'
            saveOpts.Params.Weights.Conv = true;
            saveOpts.Params.Weights.Full = false;
            saveOpts.Params.Weights.Soft = false;
            saveOpts.Params.Weights.Iteration = 'Last';%'None','Last','Every'
            saveOpts.Params.Activations = 'None';%'None','First','Last','Every'
            saveOpts.Params.WeightDiff = true;
            saveOpts.Figures.Iterations = 'None';%'None','Last','Every'
            saveOpts.Figures.Results = 'Every';%'None','Last','Every'
            saveOpts.Figures.WeightDiff = 'Every';%'None','Last','Every'
        case 'WeightDiff_ResultEvery_ActivationLast'
            saveOpts.Params.Weights.Conv = false;
            saveOpts.Params.Weights.Full = false;
            saveOpts.Params.Weights.Soft = false;
            saveOpts.Params.Weights.Iteration = 'Last';%'None','Last','Every'
            saveOpts.Params.Activations = 'Last';%'None','First','Last','Every'
            saveOpts.Params.WeightDiff = true;
            saveOpts.Figures.Iterations = 'None';%'None','Last','Every'
            saveOpts.Figures.Results = 'Every';%'None','Last','Every'
            saveOpts.Figures.WeightDiff = 'None';%'None','Last','Every'
        %case 'default'
        case 'ResultEvery_ActivationLast'
            saveOpts.Params.Weights.Conv = false;
            saveOpts.Params.Weights.Full = false;
            saveOpts.Params.Weights.Soft = false;
            saveOpts.Params.Weights.Iteration = 'Last';%'None','Last','Every'
            saveOpts.Params.Activations = 'Last';%'None','First','Last','Every'
            saveOpts.Params.WeightDiff = false;
            saveOpts.Figures.Iterations = 'None';%'None','Last','Every'
            saveOpts.Figures.Results = 'Every';%'None','Last','Every'
            saveOpts.Figures.WeightDiff = 'None';%'None','Last','Every'
        %case 'default'
        otherwise
            saveOpts.Params.Weights.Conv = true;
            saveOpts.Params.Weights.Full = false;
            saveOpts.Params.Weights.Soft = false;
            saveOpts.Params.Weights.Iteration = 'Last';%'None','Last','Every'
            saveOpts.Params.Activations = 'None';%'None','First','Last','Every'
            saveOpts.Params.WeightDiff = true;
            saveOpts.Figures.Iterations = 'None';%'None','Last','Every'
            saveOpts.Figures.Results = 'Every';%'None','Last','Every'
            saveOpts.Figures.WeightDiff = 'Every';%'None','Last','Every'
            saveOpts.saveLevel = 'default';
    end
    saveOpts.SubFolders.WeightParams = 'WeightParams' + filesep;
    saveOpts.SubFolders.ResultImages = 'ResultImages' + filesep;
    saveOpts.SubFolders.ResultTables = 'ResultTables' + filesep;
    saveOpts.SubFolders.WeightBiasComparisonImages = 'WeightBiasComparisonImages' + filesep;
    saveOpts.SubFolders.WeightBiasComparisonTables = 'WeightBiasComparisonTables' + filesep;
    saveOpts.SubFolders.LayersAndFilters = 'LayersAndFilterImages' + filesep;
    saveOpts.SubFolders.ActivatedData = 'ActivatedData' + filesep;
    saveOpts.SubFolders.NetworkStructure = 'NetworkStructure' + filesep;
    saveOpts.SubFolders.DistributionMat = 'DistribMat' + filesep;
    if (~exist('saveMainFolder','var') || isempty(saveMainFolder))
        if ~isempty(fastDiskTempPath)
            saveMainFolder = [fastDiskTempPath 'DNN_FOLDER' filesep];
        else
            global dnnCodePath;
            saveMainFolder = dnnCodePath;
        end
    end
    if ~exist(saveMainFolder,'dir') && ~strcmp(saveLevel,'none')
        mkdir(saveMainFolder);
        disp(['Folder (' saveMainFolder ') is created because it didnt exist.']);
    end
    saveOpts.MainFolder = saveMainFolder;
end