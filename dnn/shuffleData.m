function moveSummary = shuffleData(dS, distribMat)
    global fastDiskTempPath;
    tempFolder = [fastDiskTempPath 'shuffleDataTempFolder\'];
    if ~exist(tempFolder,'dir')
        mkdir(tempFolder);
    end
    
    %distribMatNew columns = {1_newBatchID,2_newSampleID,3_oldBatchID,4_oldSampleID,5_realLabel,6_predictedLabel}
    batchIDs = unique(distribMat(:,3));
    batchCount = length(batchIDs);
    moveSummary = zeros(batchCount,batchCount+1);
    for b_new = 1:batchCount
        dS = dS.updateDataStruct( dS, {'batchID_current', b_new});
        load(dS.fileName_current,'data','labels');
        dataSizeLoaded = size(data);
        data = reshape(data,dS.countFeat,[]);
        
        dataNew = data;
        labelsNew = labels;
        b_new_filename = [tempFolder 'b' num2str(b_new,'%02d') '.mat'];
        
        batchIDOldVec = [b_new (1:b_new-1) (b_new+1:batchCount)];
        for boi = 1:batchCount
            b_old = batchIDOldVec(boi);            
            if boi==1 && b_old==b_new
                %select the samples that are from b_new==b_old
                slct_bNew = distribMat(:,1)==b_new;
                distribMat_bNew = distribMat(slct_bNew,:);
                %but were not in b_new==b_old
                idxNewCheck = find(distribMat_bNew(:,3)~=b_new);
                %set them to NaN
                idxNew = distribMat_bNew(idxNewCheck,2);
                assert(sum(idxNew-idxNewCheck)==0,'these must be equal');
                
                %disp(['READY(' num2str(b_new) ')-CNT(' num2str(length(idxNew)) ')']);
                moveSummary(b_old,1) = length(idxNew);
                dataNew(:,idxNew) = NaN;
                labelsNew(idxNew) = NaN;
            else
                %get in here if there is any data from b_old to b_new
                sampleCnt_bo2bn = find(distribMat(:,1)==b_new & distribMat(:,3)==b_old); %#ok<EFIND>
                if isempty(sampleCnt_bo2bn)
                    %disp(['FR(' num2str(b_old) ')-TO(' num2str(b_new) ')-CNT(' num2str(0) ')-Nothing to move :)']);
                    continue
                end
                
                dS = dS.updateDataStruct( dS, {'batchID_current', b_old});
                load(dS.fileName_current,'data','labels');
                dataOld = reshape(data,dS.countFeat,[]);
                labelsOld = labels;
                
                %select the samples that were from b_old
                slct_bOld = distribMat(:,3)==b_old;
                distribMat_bOld = distribMat(slct_bOld,:);
                %but now are in b_new
                slct_bOld = distribMat_bOld(:,1)==b_new;
                idxOld = distribMat_bOld(slct_bOld,4);
                
                %select the samples that were from b_new
                slct_bNew = distribMat(:,1)==b_new;
                distribMat_bNew = distribMat(slct_bNew,:);
                %but now are not in b_new
                slct_bNew = distribMat_bNew(:,3)==b_old;
                idxNew = distribMat_bNew(slct_bNew,2);
                
                %disp(['FR(' num2str(b_old) ')-TO(' num2str(b_new) ')-CNT(' num2str(length(idxNew)) ')']);  
                moveSummary(b_old,b_new+1) = length(idxNew);
                dataNew(:,idxNew) = dataOld(:,idxOld);
                labelsNew(idxNew) = labelsOld(idxOld);                     
            end
        end
        
        %now check '' and '' - they should not have NaN values
        dN = isnan(dataNew);
        lN = isnan(labelsNew);
        if (sum(dN(:)) + sum(lN(:)) ==0)
            dataSizeLoaded(end) = size(dataNew,2);
            data = reshape(dataNew,dataSizeLoaded);
            labels = labelsNew;
            save(b_new_filename,'data','labels','-v7.3');
        else
            error('WTF');
        end
    end
    
    %now if we are here with no errors we can replace the files
    %delete 
    for b = 1:batchCount
        b_new_filename = [tempFolder 'b' num2str(b,'%02d') '.mat'];
        dS = dS.updateDataStruct( dS, {'batchID_current', b});
        b_old_filename = dS.fileName_current;
        [s,m] = copyfile(b_new_filename, b_old_filename, 'f');
        assert(s==1,m);
        delete(b_new_filename);
    end
    rmdir(tempFolder);
end

