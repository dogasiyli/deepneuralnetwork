function [cost, grad, optionalOutputs] = softMaxCost(theta, OPS)%)%, lambda, data, labelsVec, saveOpts, displayOpts
    % countCategory - the number of classes 
    % inputSize - the size countSamples of the input vector
    % lambda - weight decay parameter
    % data - the countSamples x countFeat input matrix,
    %        where each row data(i,:) corresponds to an observation
    % labelsVec - an countSamples x 1 matrix containing the labels corresponding for the input data
    %
%OPS = objFuncParamsStruct )%
    [objFuncParamsStructCorrect, missingFields] = check_objFuncParamsStruct(OPS, {'lambda','data','labelsVec','saveOpts','displayOpts'});
    if objFuncParamsStructCorrect==false
        error(['There are missing fields in objective function parameter struct OPS ' missingFields]);
    end
    OPS.useAccuracyAsCost = getStructField(OPS,'useAccuracyAsCost',false);
    OPS.calculateGradient = getStructField(OPS,'calculateGradient',true);    
    
    countCategory = length(unique(OPS.labelsVec));
    [countFeat, sampleCount] = size(OPS.data);

    % Unroll the parameters from theta
    W = reshape(theta(1:countCategory*countFeat), countCategory, countFeat);
    biasParams = reshape(theta(1+countCategory*countFeat : end), countCategory, 1);

    groundTruth = full(sparse(OPS.labelsVec, 1:sampleCount, 1));
    %cost = 0;
    %thetagrad = zeros(countCategory, inputSize);
    
    global curStepSoftMax;
    curStepSoftMax = curStepSoftMax +1;

    %% ---------- YOUR CODE HERE --------------------------------------
    %  Instructions: 
    %                You need to compute thetagrad and cost.
    %                The groundTruth matrix might come in handy.

    %% 1.Assign sigmoid and derivative of sigmoid function
    %JQ_ = @(y_,x_,theta_) (bsxfun(@times,y_,log(htx(theta_,x_))) + bsxfun(@times,1-y_,log(1-htx(theta_,x_))))
    % sigm = @(z_) 1.0 ./ (1.0 + exp(-z_));
    % sigmDeriva = @(a_) a_.*(1-a_);
    workMode = 3;
    switch workMode
        case 1%This is erroneous!!!
            sigm = @(z_) 1.0 ./ (1.0 + exp(-z_));
            A1 = sigm(W*OPS.data);
            A2 = bsxfun(@minus,A1,max(A1,[],1));
            A3 = exp(A2);
            Y_1 = bsxfun(@rdivide,A3,sum(A3));%probsAll
            %for instabilities when there is a zero probability class
            Y_1(Y_1==0) = eps;
            Y = log(Y_1);%logProbsAll;           
        case 2%Less memory version of case 3
            A = W*OPS.data + repmat(biasParams,1,sampleCount);
            A = bsxfun(@minus,A,max(A,[],1));
            A = exp(A);
            Y_1 = bsxfun(@rdivide,A,sum(A));%probsAll
            %for instabilities when there is a zero probability class
            Y_1(Y_1==0) = eps;
            Y = log(Y_1);%logProbsAll;           
        case 3%More memory but break-point friendly version of case 2
            A1 = W*OPS.data + repmat(biasParams,1,sampleCount);
            A2 = bsxfun(@minus,A1,max(A1,[],1));
            A3 = exp(A2);
            Y_1 = bsxfun(@rdivide,A3,sum(A3));%probsAll
            %for instabilities when there is a zero probability class
            Y_1(Y_1==0) = eps;
            Y = log(Y_1);%logProbsAll;           
        case 4%Inline function version of case 2 and 3
            makeMaxZeroOthersNegative = @(a_) bsxfun(@minus,a_,max(a_,[],1));
            multiplyXAndConvert = @(theta_,bias_,x_) makeMaxZeroOthersNegative(theta_*x_ + repmat(bias_,1,size(x_,2)));
            calcProbabilities = @(X_) bsxfun(@rdivide,exp(X_),sum(exp(X_)));
            
            X = multiplyXAndConvert(W,biasParams,OPS.data);%theta*data;%
            Y_1 = calcProbabilities(X);%probsAll
            %for instabilities when there is a zero probability class
            Y_1(Y_1==0) = eps;
            Y = log(Y_1);%logProbsAll;
    end    
    
    [~,predictedLabels] = max(Y_1);
    if isfield(OPS,'countCategory')
        [curAccuracy, curConfMat] = calculateConfusion(predictedLabels, OPS.labelsVec, false, OPS.countCategory);
    else
        [curAccuracy, curConfMat] = calculateConfusion(predictedLabels, OPS.labelsVec, false);
    end
    global minFuncStepInfo;
    minFuncStepInfo.Accuracy = curAccuracy;

    checkToPause(mfilename,[],'softMaxCost(before-calculating output params)');
    if OPS.useAccuracyAsCost  
        cost = 100-curAccuracy;
    else
        % Compute the cost and gradient for softmax regression.
        cost = (-1/sampleCount)*sum(Y(:).*groundTruth(:));
        % Apply "Weight Decay" to both
        weightDecayCost = (OPS.lambda/2)*sum(W(:).^2);
        cost = cost + weightDecayCost;
    end
    if OPS.calculateGradient
        % Compute the cost and gradient for softmax regression.
        weightgrad = (-1/sampleCount).*(groundTruth-Y_1)*OPS.data';
        b_grad = -sum(Y,2)./sampleCount;
        % Apply "Weight Decay" to both
        weightDecayThetagrad = OPS.lambda*W;
        % Unroll the gradient matrices into a vector for minFunc
        grad = [weightgrad(:) + weightDecayThetagrad(:);b_grad(:)];
    else
        grad = [];
    end
       
    if OPS.calculateGradient
        % Visualize the process
        visualizeProcess(OPS, theta, weightgrad, curConfMat, curStepSoftMax, curAccuracy, workMode);
        paramsFileName2Save = get_paramsFileName2Save(OPS.saveOpts, curStepSoftMax, curAccuracy);
        try
            if ~strcmp(paramsFileName2Save,'')
                switch workMode
                    case 1%This is erroneous!!!  
                        save(paramsFileName2Save,'A1','A2','A3','Y_1','Y','lambda','theta','data','cost','weightDecayCost','thetagrad','weightDecayThetagrad');
                    case 2%Less memory version of case 3
                        save(paramsFileName2Save,'A','Y_1','Y','lambda','theta','data','cost','weightDecayCost','thetagrad','weightDecayThetagrad');          
                    case 3%More memory but break-point friendly version of case 2
                        save(paramsFileName2Save,'A1','A2','A3','Y_1','Y','lambda','theta','data','cost','weightDecayCost','thetagrad','weightDecayThetagrad');
                    case 4%Inline function version of case 2 and 3
                        save(paramsFileName2Save,'X','Y_1','Y','lambda','theta','data','cost','weightDecayCost','thetagrad','weightDecayThetagrad');          
                end                
            end
        catch
            %not an important aspect - just warn if some bogus stuff
            %happens
            %disp('!!!!!WARNING - Couldnt save softmax params!!!!!');
        end
    end
    optionalOutputs = assignOptionalObjectiveFunctionOutputs(OPS, struct('accuracy',curAccuracy));
end

function visualizeProcess(OPS, theta, thetagrad, curConfMat, curStepSoftMax, curAccuracy, workMode)
    if isfield(OPS,'saveOpts') && ~isempty(OPS.saveOpts)
        callerFuncNameString = getCallerFuncName(3);
        
        callerFuncNameString = [callerFuncNameString '-Accuracy(' num2str(curAccuracy,'%4.2f') ')'];
        
        h = subplotWdW_conv(theta, thetagrad, curConfMat, curStepSoftMax, curAccuracy, callerFuncNameString);
        figureSaveFolder = [OPS.saveOpts.MainFolder 'softmax' filesep OPS.saveOpts.SubFolders.WeightBiasComparisonImages];
        if (~exist(figureSaveFolder,'dir'))
            mkdir(figureSaveFolder);
        end        
        fileName2Save = [figureSaveFolder 'WeightBiasComparison_SoftMax_i(' num2str(curStepSoftMax,'%03d') ')_Acc(' num2str(curAccuracy,'%4.2f') ')_M' num2str(workMode) '.png'];
            saveas(h,fileName2Save);
    end
end
function paramsFileName2Save = get_paramsFileName2Save(saveOpts, curStepSoftMax, curAccuracy)
    paramsFileName2Save = '';
    try %#ok<TRYNC>
        paramsSaveFolder = [saveOpts.MainFolder 'softmax' filesep saveOpts.SubFolders.WeightParams];
        if strcmp(saveOpts.Params.Weights.Iteration,'Last')
            paramsFileName2Save = [paramsSaveFolder 'paramsSoftMaxCost.mat'];
        elseif strcmp(saveOpts.Params.Weights.Iteration,'Every')
            paramsFileName2Save = [paramsSaveFolder 'paramsSoftMaxCost_i(' num2str(curStepSoftMax,'%03d') ')_Acc(' num2str(curAccuracy,'%4.2f') ').mat'];
        end
    end
end

% subplotWdW_conv(theta, thetagrad);
function h = subplotWdW_conv(W, dW, curConfMat, curStepSoftMax, curAccuracy, callerFuncNameString)
%     if (~strcmp(saveOpts.Figures.Results,'None'))
        global minFuncStepInfo;
        
        h = figure(65);clf;
        rc = 3; cc = 1;
        subplotW_softfull(W ,rc, cc, 1, 1, curStepSoftMax, callerFuncNameString);
        subplotdW_softfull(dW ,rc, cc, 1, 2);
        
        if ~isempty(minFuncStepInfo) && isfield(minFuncStepInfo,'AccuracyDif')
            figureTitle = ['Accuracy(' num2str(curAccuracy,'%4.2f') '), AccuracyDif(' num2str(minFuncStepInfo.AccuracyDif,'%4.2f') ')'];
       else
            figureTitle = ['Accuracy(' num2str(curAccuracy,'%4.2f') ')'];
        end
        vararginstruct = struct('figureTitle',figureTitle,'figureID', {65}, 'subFigureArr', {[3,1,3]});
        if max(curConfMat(:)>=100)
            vararginstruct.normalizeRowOrCol = 'row';
        end
        drawConfusionMatWithNumbers( curConfMat, vararginstruct);
%     end
end

function subplotW_softfull(W ,rc, cc, r, c, curStepSoftMax, callerFuncNameString)
    subplotID = c + cc*(r-1);
    subplot(rc,cc,subplotID);imagesc(W);colorbar;
        set(gca,'XTick',ceil(linspace(1,size(W,2),min(8,round(size(W,2)/4)))));
        set(gca,'YTick',ceil(linspace(1,size(W,1),min(8,size(W,1)))));
        xlabel(['Previous Layer Dimension(' num2str(size(W,2)) ')']);
        ylabel(['Next Layer Dimension(' num2str(size(W,1)) ')']);
        titleStr_Weight = ['SoftMax_{s(' num2str(curStepSoftMax,'%03d') ')}-W_{minVal(' num2str(min(W(:)),'%6.4f') ')},W_{maxVal(' num2str(max(W(:)),'%6.4f') ')},W_{difVal(' num2str(max(W(:))-min(W(:)),'%6.4f') ')}'];
        if (~isempty(callerFuncNameString) && ~strcmp(callerFuncNameString,''))
            titleStr_Weight = {['callerFunc(' callerFuncNameString ')'],titleStr_Weight};
        end        
        title(titleStr_Weight);
end

function subplotdW_softfull(dW ,rc, cc, r, c)
    subplotID = c + cc*(r-1);
    subplot(rc,cc,subplotID);imagesc(dW);colorbar;
        set(gca,'XTick',ceil(linspace(1,size(dW,2),min(8,round(size(dW,2)/4)))));
        set(gca,'YTick',ceil(linspace(1,size(dW,1),min(8,size(dW,1)))));
        xlabel('deltaWeight(dW)');
        titleStr_Weight = ['dW_{minVal(' num2str(min(dW(:)),'%6.4f') ')},dW_{maxVal(' num2str(max(dW(:)),'%6.4f') ')},dW_{difVal(' num2str(max(dW(:))-min(dW(:)),'%6.4f') ')}'];
        title(titleStr_Weight);
end

