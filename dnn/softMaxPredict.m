function [pred, probsAll, transformedData] = softMaxPredict(data, weightParams, biasParams)
% data(NxM)             : input matrix, 
%                         where each column data(:, i) corresponds to a single test set
% thetaOut_matrix (CxM) : weight matrix
%                         where C is category count
% The prediction matrix probsAll, where probsAll(i) is argmax_c P(y(c) | x(i)).
    htx = @(M_) bsxfun(@rdivide,exp(M_),sum(exp(M_)));
    if (nargout==3)
        transformedData = weightParams*data + repmat(biasParams,1,size(data,2));
        probsAll = htx(transformedData);
    else
        probsAll = htx(weightParams*data + repmat(biasParams,1,size(data,2)));
    end
    [~,pred] = max(probsAll);
    %pred(pred==size(probsAll,1))=0; ??????????????????
end

