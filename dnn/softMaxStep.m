function [C_SM, predictionsOut, probabilitiesOut, transformedDataOut] = softMaxStep( C_SM, data, labels, displayOpts, saveOpts, deletePrevLayerBatch)
%C_SM : Cell_SoftMax

%% STEP 0 : Explain the function 
%   softMaxCell is a struct with parameters below
%   this layer has to be at the very end of the deep neural network

% C_SM.name = 'soft_1_7';
% C_SM.type = 'softmax';
% C_SM.activationType = 'sigmoid';%can be sigmoid, tanh or relu -> activation function option
% C_SM.inputSize = input feature size;
% C_SM.countCategory = 10;% also the hidden size - number of hidden units
% %  softMaxParams-Below you can see relevant parameters values that will
% %  allow the softMaxParams to run; you can change the parameters below.
% C_SM.softMaxParams = struct;
% C_SM.softMaxParams.softmaxLambda = 1e-4;% I do not remember what the hack this one was about
%
% C_SM.softMaxParams.paramsMinFunc = struct;
% C_SM.softMaxParams.paramsMinFunc.maxIter = 400;
%
% C_SM.Weights
% C_SM.BiasParams
% C_SM.outputSize_original
% C_SM.outputSize_vectorized
%
% C_SM.initializeWeightMethod = 'Random'; <this can be either random or ELM>

%% STEP 1 : Initializations
%  Find the parameters that arent defined and define them
    C_SM = setHelperParams(C_SM, data, labels, displayOpts);
%% STEP 2 : Do the actual learning
    C_SM = applyLearning(C_SM, data, labels, displayOpts, saveOpts);

%% STEP 3 : Calculate the outputs
    switch nargout
        case 2
            predictionsOut = softMaxPredict(data, C_SM.Weights, C_SM.BiasParams);
        case 3
            [predictionsOut, probabilitiesOut] = softMaxPredict(data, C_SM.Weights, C_SM.BiasParams);
        case 4
            [predictionsOut, probabilitiesOut, transformedDataOut] = softMaxPredict(data, C_SM.Weight, C_SM.BiasParams);
    end    
end

function softMaxCell = setHelperParams(softMaxCell, data, labels, displayOpts)
    [countFeat, countSamples] = size(data);
    %if the countCategory is not yet found
    if ~isfield(softMaxCell,'countCategory')
        softMaxCell.countCategory = length(unique(labels));
    end
    %if the countSamples or countFeat is not yet found
    if ~isfield(softMaxCell,'countSamples') || ~isfield(softMaxCell,'countFeat')
        if (countSamples < countFeat && displayOpts.warningInfo)
            warning(['Cols(' num2str(countSamples) ') must be observations, rows(' num2str(countFeat) ') must be features. Hence check if data needs to be transposed at softMaxStep']);
        end
    end
    softMaxCell.countSamples = countSamples;
    softMaxCell.countFeat = countFeat;            
    %
    if isfield(softMaxCell,'inputSize_original')
        softMaxCell.inputSize_vectorized = [prod(softMaxCell.inputSize_original(1:end-1)),softMaxCell.countSamples];
    end        
    %
    softMaxCell.outputSize_original = [1, 1, softMaxCell.countCategory, softMaxCell.countSamples];
    softMaxCell.outputSize_vectorized = [softMaxCell.countCategory, softMaxCell.countSamples];
end

function C_SM = applyLearning(C_SM, data, labels, displayOpts, saveOpts)
    if ~isfield(C_SM,'Weight')
        if (displayOpts.calculation==1)
            disp([displayOpts.ies{6} num2str(C_SM.countCategory) ' categories will be learnt using softmax layer.']);
        end
        if strcmp(C_SM.initializeWeightMethod,'ELM')
            error('bias parameter must be added here');
            if (displayOpts.calculation==1)
                disp([displayOpts.ies{6} 'Initializing softMax layer using ELM']);
            end            
            if (displayOpts.warningInfo==1)
                disp([displayOpts.ies{6} 'Here we have some problem :(']);
                pause;
            end
            %using all the data for initialization is useless
            %hence use only some potion of the data for Welm initialization
            t = cputime;
                Y = full(sparse((labels)',1:length(labels),ones(1,length(labels))))';
                Welm = (data*Y)'/((data*data')+(1e-8*eye(size(data,1))));
            t = cputime-t;
            if      (displayOpts.calculation==1 && displayOpts.timeMode>=2)
                disp([displayOpts.ies{6} 'ELM training is completed in ' num2str(t,'%4.2f') ' cpuTime']);
            elseif (displayOpts.calculation==1 && displayOpts.timeMode>=2)
                disp([displayOpts.ies{6} 'ELM training is completed.']);
            end                        
            theta = reshape(Welm,numel(Welm),[]);
        else
            theta = [];
        end
        if (displayOpts.calculation==1)
            disp([displayOpts.ies{6} 'softMax layer parameters will be learnt.']);
        end         
        C_SM.softMaxParams.paramsMinFunc.logfile = ['minFuncLogFile_' C_SM.name '.txt'];
        t = cputime;
        optimizedParamsStruct = softMaxTrain( data, labels...these are the necessary variables
                                     ,'theta', theta...
                                     ,'lambda', C_SM.softMaxParams.softmaxLambda...
                                     ,'paramsMinFunc',C_SM.softMaxParams.paramsMinFunc...
                                     ,'saveOpts', saveOpts...
                                     ,'useAccuracyAsCost',false...
                                     ,'countCategory',C_SM.countCategory...
                                     ,'displayOpts', displayOpts);
        C_SM.Weight = optimizedParamsStruct.optWeight;
        C_SM.BiasParams = optimizedParamsStruct.optBias;
        if C_SM.softMaxParams.paramsMinFunc.maxIter>0
            actionStr = 'skipped';
        else
            actionStr = 'completed';
        end
        t = cputime-t;
        if      (displayOpts.calculation==1 && displayOpts.timeMode>=2)
            disp([displayOpts.ies{6} 'SoftMax training is ' actionStr ' in ' num2str(t,'%4.2f') ' cpuTime']);
        elseif (displayOpts.calculation==1 && displayOpts.timeMode>=2)
            disp([displayOpts.ies{6} 'SoftMax training is ' actionStr '.']);
        end
    end
end