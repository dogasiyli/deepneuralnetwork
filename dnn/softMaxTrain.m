function optimizedParamsStruct = softMaxTrain(data, labelsVec, varargin) 

%varargin Parameters are :
%theta          - theta can be initalized randomly if not given
%lambda         - lambda can be initalized if not given (1e-4)
%paramsMinFunc - %paramsMinFunc can be initialized if not given (400 iteration, no display, lbgfs method)
%paramsCategory_MinFunc - instead of paramsMinFunc this param can be passed so that paramsMinFunc is filled according to paramsCategory_MinFunc
                                   
%% STEP 0 : Explain the function
%softmaxTrain is a softmax model training function with the given parameters on the given
% data. 
% Returns 
% 1. thetaOut_vectorized: a vector containing the trained parameters for the model.
% 2. thetaOut_matrix: a properly shaped matrix containing the trained parameters for the model.
%
% Required inputs
% 1. data   -> [countFeat, sampleCount] = size(data);
% 2. labels -> countCategory = length(unique(labels));
%              countSamples by 1 vector containing the class labels for the
%              corresponding inputs. labels(c) is the class label for
%              the cth input
%
%
% Extracted information
% 1. countCategory
% 2. sampleCount
% 3. countFeat
%
% Optional inputs
% 1. theta  -> 
% 2. lambda -> weight decay parameter
% 3. paramsMinFunc ->
%           maxIter -> 200 default
%           display -> 'off' default
%           Method  -> 'lbgfs' default
%

% STEP 1 : Initializations
%    addpath(genpath('..\'));
    global curStepSoftMax;
    curStepSoftMax = 0;
% STEP-2 : Set optional param defaults
    [theta, lambda, paramsMinFunc, displayOpts, saveOpts, data, useAccuracyAsCost, countCategory, countFeat] = getVararginParams(data, labelsVec, varargin{:});
% STEP 3 : Run 'minFunc' to minimize the function
    if paramsMinFunc.maxIter>0
        %OPS = objFuncParamsStruct   
        %[data_mean0, meanData] = meanNormalizePatches(data);
        %OPS = struct('lambda',lambda,'data',data_mean0,'labelsVec',labelsVec,'displayOpts',displayOpts,'saveOpts',saveOpts,'useAccuracyAsCost',useAccuracyAsCost,'calculateGradient',true);
        OPS = struct('lambda',lambda,'data',data,'labelsVec',labelsVec,'displayOpts',displayOpts,'saveOpts',saveOpts,'useAccuracyAsCost',useAccuracyAsCost,'calculateGradient',true,'countCategory',countCategory);
        %checkToPause([mfilename '-mfdgVSmf-softMaxCost'],[],'(compare minFunc_Doga vs minFunc)');
        [optTheta,~,~,outputCellArr] = minFunc_Doga(@softMaxCost, theta, paramsMinFunc, OPS);
        %[thetaOut_vectorized,costFinal,exitflag,outputParams] = minFunc( @(p) softMaxCost(p,OPS), theta, paramsMinFunc);
    else
        optTheta = theta;
    end
        clear curStepSoftMax;
    optimizedParamsStruct = struct;
    optimizedParamsStruct.optTheta = optTheta;
    optimizedParamsStruct.optWeight = reshape(optTheta(1:countCategory*countFeat), countCategory, countFeat);
    optimizedParamsStruct.optBias = reshape(optTheta(1+countCategory*countFeat:end), countCategory, 1);
end

% STEP-1 : Initialize the necessary variables
function [sampleCount, countCategory, countFeat] = extractNecessaryParams(data, labelsVec, countCategory, displayOpts)
    if isempty(countCategory)
        countCategory = length(unique(labelsVec));
    end
    [countFeat, sampleCount] = size(data);
    if (countFeat > sampleCount && displayOpts.warningInfo)
        warning(['Cols(' num2str(sampleCount) ') must be observations, rows(' num2str(countFeat) ') must be features. Hence check if data needs to be transposed at softMaxStep']);
    end
end

% STEP-2 : Set optional param defaults
function [theta, lambda, paramsMinFunc, displayOpts, saveOpts, data, useAccuracyAsCost, countCategory, countFeat] = getVararginParams(data, labelsVec, varargin)
    %P1D0 -> Passed assed paramaeter = 1, Default value used = 0
    [ theta,  lambda,  paramsCategory_MinFunc,   paramsMinFunc,  displayOpts,  saveOpts,  useAccuracyAsCost,  minFuncLogFileFull,  countCategory, P1D0] = parseArgs(...
    {'theta' 'lambda' 'paramsCategory_MinFunc'  'paramsMinFunc' 'displayOpts' 'saveOpts' 'useAccuracyAsCost' 'minFuncLogFileFull' 'countCategory'},...
    {   []    0.0001       'showNone_noLog'            []            -1           []           false                   []                []      },...
    varargin{:});

    if (~isstruct(displayOpts) && displayOpts==-1)
        displayOpts = setDisplayOpts();
    end
    if ~isstruct(saveOpts)
        saveOpts = setSaveOpts('none');
    end
    [sampleCount, countCategory, countFeat] = extractNecessaryParams(data, labelsVec, countCategory, displayOpts);
    if isempty(theta)
        %TODO - this part must be done using fill_W
        W  = fill_W( 'SVD', size(data,1), countCategory, [size(data,1), countCategory]);
        b = zeros(countCategory,1);
        %theta = 1/countFeat * randn(countCategory * countFeat, 1);
        theta = [W(:);b(:)];
        if (displayOpts.defaultParamSet==1)
            disp([displayOpts.ies{4} 'theta is initialized randomly.']);
        end
        clear W b
    end
    if isempty(paramsMinFunc)
        paramsMinFunc = setParams_minFunc(paramsCategory_MinFunc);
    end
    if ~isempty(minFuncLogFileFull)
        paramsMinFunc.logfile = minFuncLogFileFull;
    elseif isfield(saveOpts,'MainFolder') && isfield(paramsMinFunc,'logfile') && ~isempty(paramsMinFunc.logfile) && ~strcmp(paramsMinFunc.logfile,'')
        paramsMinFunc.logfile = [saveOpts.MainFolder paramsMinFunc.logfile];
    end
    if (displayOpts.defaultParamSet==1)
        if (P1D0.lambda==0)
            disp([displayOpts.ies{4} 'softMax_lambda parameter set to default value(' num2str(lambda) ')']);
        end
        if (P1D0.lambda==0)
            disp([displayOpts.ies{4} 'useAccuracyAsCost parameter set to default value(' useAccuracyAsCost ')']);
        end
        if (P1D0.paramsMinFunc==0 && P1D0.paramsCategory_MinFunc==0)
            %only if both of them are default - there is a default action to inform
            disp([displayOpts.ies{4} 'paramsMinFunc is not passed. It will be initialized using (' paramsCategory_MinFunc ') settings.']);
        end
    end
end