function [data, cnnnet, predictionsOut, time_spent_all, probabilitiesOut] = softMax_ForwardPropogate(cnnnet, layer, data, varargin)
%0.Initialize time
    time_spent_all = cputime;
%1.Set optional param defaults
    [labels,  displayOpts, saveOpts, memoryMethod, deletePrevLayerBatch] = getOptionalParams(varargin{:});
%2.Learn and set input and output sizes
    cnnnet = arrangeLayerSizes(cnnnet, layer);
%3.Give the user information about what will be done by printing on the screen
    displayProcessInformation(data, cnnnet{layer,1}, displayOpts);
%4.Learn or just forward propogate the weights
    [cnnnet{layer,1}, predictionsOut, data, probabilitiesOut] = softMaxLearn(cnnnet{layer,1}, data, labels, displayOpts, saveOpts, deletePrevLayerBatch);
%5.Write the timing information on screen    
    time_spent_all = timingInfo(time_spent_all, displayOpts);
end

%Helper functions for readibility
function [ labels,  displayOpts, saveOpts, memoryMethod, deletePrevLayerBatch] = getOptionalParams(varargin)
    %optional param defaults explanations : 
    %1. labels                       - labels of each sample
    %
    %2. displayInfo                  - needed to be able to display what is
    %                                  happening on the screen such as what
    %                                  is the new size of data or how much
    %                                  time spent for the pooling procedure    
    %If labels don't exist just make an empty entry
    %P1D0 -> Passed assed paramaeter = 1, Default value used = 0
    [ labels,  displayOpts,  saveOpts,   memoryMethod,  deletePrevLayerBatch, P1D0] = parseArgs(...
    {'labels' 'displayOpts' 'saveOpts'  'memoryMethod' 'deletePrevLayerBatch'},...
    {   []          []          []     'all_in_memory'        true           },...
    varargin{:});

    %if displayOpts is not passed as a parameter then set is to 'all' by
    %default to show the user what will happen in every detail.
    %also return it as a parameter because even if it is not passed in it
    %would be returned to be used for upcoming evaluations
    if (isempty(displayOpts))
        displayOpts = setDisplayOpts('all');
        if (displayOpts.defaultParamSet && P1D0.displayOpts==0)
            disp([displayOpts.ies{4} 'Display options is set to -all- by default.']);
        end
    end
    
    %memoryMethod
    if (P1D0.memoryMethod==0)
        if (displayOpts.defaultParamSet)
            disp([displayOpts.ies{4} 'memoryMethod is set to default(all_in_memory).']);
        end
    end
end

function cnnnet = arrangeLayerSizes(cnnnet, layer)
    %the previous layer can be 
    %pooling-convolution-fullyconnected
    if (layer>1)
        %for pooling and convolution and fullyconnected
        if ~isfield(cnnnet{layer,1},'inputSize_original') && isfield(cnnnet{layer-1,1},'outputSize_original')
            cnnnet{layer,1}.inputSize_original = cnnnet{layer-1,1}.outputSize_original;
        end
    end
    cnnnet{layer,1}.inputSize_vectorized = [prod(cnnnet{layer,1}.inputSize_original(1:end-1)) cnnnet{layer,1}.inputSize_original(end)];
end

function countSamples = displayProcessInformation(data, C_SM, displayOpts)
    % if time spent needs to be passed here is how it goes
    countSamples = size(data, 2);
    if (displayOpts.calculation)
        if (displayOpts.dataSize)
            disp([displayOpts.ies{4} 'Data of size ' mat2str([C_SM.inputSize_vectorized(1:end-1) countSamples]) ' will be softmaxed.']);
        else
            disp([displayOpts.ies{4} 'Data will be softmaxed.']);
        end
    end
end

function [C_SM, predictionsOut, data, probabilitiesOut] = softMaxLearn(C_SM, data, labels, displayOpts, saveOpts, deletePrevLayerBatch)
    softMaxLearningWillOccur = ~isfield(C_SM,'theta');

    if     (displayOpts.calculation==1 &&  softMaxLearningWillOccur)
        disp([displayOpts.ies{4} 'Learning data in softmax layer for ' num2str(length(unique(labels))) ' categories.']);        
    elseif (displayOpts.calculation==1 && ~softMaxLearningWillOccur)
        disp([displayOpts.ies{4} 'Data will be softmaxed.']);        
    end

    
    time_spent_transform = cputime;
        [C_SM, predictionsOut, probabilitiesOut, data] = softMaxStep( C_SM, data, labels, displayOpts, saveOpts, deletePrevLayerBatch);
    time_spent_transform = cputime - time_spent_transform;

    if     (displayOpts.calculation &&  softMaxLearningWillOccur && displayOpts.timeMode>=2)
        disp([displayOpts.ies{4} 'Learning in softmax for ' num2str(length(unique(labels))) ' categories finished in ' num2str(time_spent_transform,'%4.2f') ' cputime.']);        
    elseif (displayOpts.calculation &&  softMaxLearningWillOccur && displayOpts.timeMode <2)
        disp([displayOpts.ies{4} 'Learning in softmax for ' num2str(length(unique(labels))) ' categories finished.']);
    elseif (displayOpts.calculation && ~softMaxLearningWillOccur && displayOpts.timeMode>=2)
        disp([displayOpts.ies{4} 'Data was softmaxed in ' num2str(time_spent_transform,'%4.2f') ' cputime.']);
    elseif (displayOpts.calculation && ~softMaxLearningWillOccur && displayOpts.timeMode <2)
        disp([displayOpts.ies{4} 'Data was softmaxed.']);
    end
end

function time_spent_all = timingInfo(time_spent_all, displayOpts)
    time_spent_all = cputime - time_spent_all;
    if (displayOpts.timeMode>=2)
        disp([displayOpts.ies{4} 'Soft max lasted in ' num2str(time_spent_all) ' cputime.' ]);
    end
end