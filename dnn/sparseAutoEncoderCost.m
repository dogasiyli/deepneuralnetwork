function [cost, grad, optionalOutputs] = sparseAutoEncoderCost(theta, OPS)%sizeVis, sizeHid, paramsAutoEncode, data,act, actDerivative, displayOpts, saveOpts
    % sizeVis: the number of input units (sV) 
    % sizeHid: the number of hidden units (sH) 
    % lambda: weight decay parameter
    % sparsityParam: The desired average activation for the hidden units (denoted in the lecture
    %                           notes by the greek alphabet rho, which looks like a lower-case "p").
    % sparsePenaltyWeight: weight of sparsity penalty term
    % data: Our 64x10000 matrix containing the training data.  So, data(:,i) is the i-th training example. 

    % The input theta is a vector (because minFunc expects the parameters to be a vector). 
    % We first convert theta to the (W1, W2, b1, b2) matrix/vector format, so that this 
    % follows the notation convention of the lecture notes. 
    
    global curStepsparseAutoEncode;
    curStepsparseAutoEncode = curStepsparseAutoEncode +1;
    
%OPS = objFuncParamsStruct )%
    [objFuncParamsStructCorrect, missingFields] = check_objFuncParamsStruct(OPS, {'sizeVis','sizeHid','paramsAutoEncode','data','act','actDerivative','displayOpts','saveOpts'});
    if objFuncParamsStructCorrect==false
        error(['There are missing fields in objective function parameter struct OPS ' missingFields]);
    end
    OPS.useAccuracyAsCost = getStructField(OPS,'useAccuracyAsCost',false);
    OPS.calculateGradient = getStructField(OPS,'calculateGradient',true);  
    OPS.applySparsity = getStructField(OPS,'applySparsity',true);  
    OPS.useActSecondLayer = getStructField(OPS,'useActSecondLayer',true);  
    
    X = OPS.data;
    sizeHid = OPS.sizeHid;
    sizeVis = OPS.sizeVis;
    paramsAutoEncode = OPS.paramsAutoEncode;

    W1 = reshape(theta(1:sizeHid*sizeVis), sizeHid, sizeVis);                   % 25 x 64
    W2 = reshape(theta(sizeHid*sizeVis+1:2*sizeHid*sizeVis), sizeVis, sizeHid); % 64 x 25
    b1 = theta(2*sizeHid*sizeVis+1:2*sizeHid*sizeVis+sizeHid);                  % 25 x  1
    b2 = theta(2*sizeHid*sizeVis+sizeHid+1:end);                                % 64 x  1

    % Cost and gradient variables (your code needs to compute these values). 
    % Here, we initialize them to zeros. 
    m  = size(X,2);

    %Forward Propogation
    a2 = OPS.act(bsxfun(@plus,W1*X,b1));%Layer_2
    if OPS.useActSecondLayer
        a3 = OPS.act(bsxfun(@plus,W2*a2,b2));%Layer_3
    else
        a3 = bsxfun(@plus,W2*a2,b2);%Layer_3
    end
    %Squared Error Term
    deriv3 = (a3-X);
    if OPS.useActSecondLayer
        slack3 = deriv3.*OPS.actDerivative(a3);%Layer_3
    else
        slack3 = deriv3;%.*OPS.actDerivative(a3);%Layer_3
    end
    slack2  = (W2'*slack3).*OPS.actDerivative(a2);%Layer_2

    %applySparsity
    if OPS.applySparsity
        roBase = paramsAutoEncode.sparsityParam*ones(sizeHid,1);
        roFound = mean(a2,2);
        sparsityMat = calcSparseMat(roBase, roFound);
        sparsitySlackAdd = calcSparseSlack(a2, OPS.actDerivative, paramsAutoEncode.sparsePenaltyWeight, sparsityMat);
        sparsityCost = calcSparseCost(roBase, roFound);
        
        slack2  = bsxfun(@plus,slack2,sparsitySlackAdd); % [<25x64 * 64x10000> .* <25x10000>] = <25x10000>
    end
    slack2  = slack2.*OPS.actDerivative(a2);%Layer_2

    %Derivatives
    W2grad = (slack3*a2') ./m;
    b2grad = sum(slack3,2) ./m;
    %Apply weight decay here
    W2grad = W2grad + paramsAutoEncode.weightDecayParam*W2;
    weightDecayCost_2 = (paramsAutoEncode.weightDecayParam/2)*sum(W2(:).^2);

    W1grad = (slack2*X') ./m;
    b1grad = sum(slack2,2)./m;
    %Apply weight decay here
    W1grad = W1grad + paramsAutoEncode.weightDecayParam*W1;
    weightDecayCost_1 = (paramsAutoEncode.weightDecayParam/2)*sum(W1(:).^2);
    
    cost = sum(sum(deriv3.^2))/(2*m);%(0.5)*(1/m)*(deriv3(:)'*deriv3(:));
    cost = cost + weightDecayCost_1+weightDecayCost_2;%applyWeightDecay
    if OPS.applySparsity
        cost = cost + paramsAutoEncode.sparsePenaltyWeight*sparsityCost;  %applySparsity
    end
    %-------------------------------------------------------------------
    % After computing the cost and gradient, 
    % we will convert the gradients back to a vector format (suitable for minFunc).  
    % Specifically, we will unroll gradient matrices into a vector.
    grad = [W1grad(:) ; W2grad(:) ; b1grad(:) ; b2grad(:)];
    
    % Visualize the process
    if isfield(OPS,'saveOpts') && ~isempty(OPS.saveOpts) && ~strcmpi(OPS.saveOpts.Figures.WeightDiff,'None')
        callerFuncNameString = getCallerFuncName(4);        
        minFuncAdditionalInfoStr = minFuncStepInfo_GrabFigureTitle(curStepsparseAutoEncode, cost);
        
        h = figure(65);clf;
        rc = 2; cc = 2;
        subplotW_softfull(W1 ,rc, cc, 1, 1, callerFuncNameString, minFuncAdditionalInfoStr);
        subplotdW_softfull(W1grad ,rc, cc, 1, 2);
        subplotW_softfull(W2 ,rc, cc, 2, 1);
        subplotdW_softfull(W2grad ,rc, cc, 2, 2);

        figureSaveFolder = [OPS.saveOpts.MainFolder 'sparseAutoEncoder' filesep ];
        if (~exist(figureSaveFolder,'dir'))
            mkdir(figureSaveFolder);
        end        
        fileName2Save = [figureSaveFolder 'sparseAutoEncoderWeightComparison_i(' num2str(curStepsparseAutoEncode,'%03d') ')_cost(' num2str(cost,'%6.4g') ').png'];
            saveas(h,fileName2Save);
    end    
    optionalOutputs = assignOptionalObjectiveFunctionOutputs(OPS, struct('hidLayeractivations',a2));
end

function sparsitySlackAdd = calcSparseSlack(a2, actDerFunc, sparsePenaltyWeight, sparsityMat)
    sparsitySlackAdd = bsxfun(@times,actDerFunc(a2),(sparsePenaltyWeight*sparsityMat));
end
function sparsityMat = calcSparseMat(r, rh)
    sparsityMat = (((1-r)./(1-rh)) - (r./rh));
end

function sparsityCost = calcSparseCost(r, rh)
    sparsityCost =  (log(r./rh).*r) + (log((1-r)./(1-rh)).*(1-r));
    sparsityCost = real(sum(sparsityCost));
end

function subplotW_softfull(W ,rc, cc, r, c, callerFuncNameString, minFuncAdditionalInfoStr)
    subplotID = c + cc*(r-1);
    subplot(rc,cc,subplotID);imagesc(W);colorbar;
        set(gca,'XTick',ceil(linspace(1,size(W,2),min(8,round(size(W,2)/4)))));
        set(gca,'YTick',ceil(linspace(1,size(W,1),min(8,size(W,1)))));
        xlabel(['Previous Layer Dimension(' num2str(size(W,2)) ')']);
        ylabel(['Next Layer Dimension(' num2str(size(W,1)) ')']);
        titleStr_Weight = {['W_{minVal(' num2str(min(W(:)),'%6.4f') ')},W_{maxVal(' num2str(max(W(:)),'%6.4f') ')},W_{difVal(' num2str(max(W(:))-min(W(:)),'%6.4f') ')}']};
        if exist('callerFuncNameString','var') && ~isempty(callerFuncNameString)
            titleStr_Weight = {['callerFunc(' callerFuncNameString ')'],titleStr_Weight{:}};
        end
        if exist('minFuncAdditionalInfoStr','var') && ~isempty(minFuncAdditionalInfoStr)
            titleStr_Weight = {minFuncAdditionalInfoStr, titleStr_Weight{:}};
        end
        title(titleStr_Weight);
end

function subplotdW_softfull(dW ,rc, cc, r, c)
    subplotID = c + cc*(r-1);
    subplot(rc,cc,subplotID);imagesc(dW);colorbar;
        set(gca,'XTick',ceil(linspace(1,size(dW,2),min(8,round(size(dW,2)/4)))));
        set(gca,'YTick',ceil(linspace(1,size(dW,1),min(8,size(dW,1)))));
        xlabel('deltaWeight(dW)');
        titleStr_Weight = ['dW_{minVal(' num2str(min(dW(:)),'%6.4f') ')},dW_{maxVal(' num2str(max(dW(:)),'%6.4f') ')},dW_{difVal(' num2str(max(dW(:))-min(dW(:)),'%6.4f') ')}'];
        title(titleStr_Weight);
end
%     W_grad  = calcDeltaWeight( s_prev, a_next, W, lambda, layerIdentifier, displayOpts, varargin);
%     b1grad = mean(slack1,2);
%     b2grad = mean(slack2,2);
%     weightDecayCost_1 = (lambda/2)*sum(W1(:).^2;
%     weightDecayCost_2 = (lambda/2)*sum(W2(:).^2);
%     cost = cost + (lambda/2)*(sum(sum(W1.^2))+sum(sum(W2.^2)));%applyWeightDecay
    
