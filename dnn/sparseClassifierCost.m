function [cost, grad, optionalOutputs] = sparseClassifierCost(theta, OPS)%sizeVis, sizeHid, paramsAutoEncode, data,labels,act, actDerivative, displayOpts, saveOpts
    % sizeVis: the number of input units (sV) 
    % sizeHid: the number of hidden units (sH) 
    % lambda: weight decay parameter
    % sparsityParam: The desired average activation for the hidden units (denoted in the lecture
    %                           notes by the greek alphabet rho, which looks like a lower-case "p").
    % sparsePenaltyWeight: weight of sparsity penalty term
    % data: Our 64x10000 matrix containing the training data.  So, data(:,i) is the i-th training example. 

    % The input theta is a vector (because minFunc expects the parameters to be a vector). 
    % We first convert theta to the (W1, W2, b1, b2) matrix/vector format, so that this 
    % follows the notation convention of the lecture notes. 
    
    global curStepSparseClassifier;
    curStepSparseClassifier = curStepSparseClassifier +1;
    
%OPS = objFuncParamsStruct )%
    [objFuncParamsStructCorrect, missingFields] = check_objFuncParamsStruct(OPS, {'sizeVis','sizeHid','paramsSparseClassifier','data','labels','act','actDerivative','displayOpts','saveOpts'});
    if objFuncParamsStructCorrect==false
        error(['There are missing fields in objective function parameter struct OPS ' missingFields]);
    end
    OPS.useAccuracyAsCost = getStructField(OPS,'useAccuracyAsCost',false);
    OPS.calculateGradient = getStructField(OPS,'calculateGradient',true);  
    
    X = OPS.data;
    Y = OPS.labels;
    sizeVis = OPS.sizeVis;
    sizeHid = OPS.sizeHid;
    sizeOut = length(unique(Y));
    Y = full(sparse((OPS.labels)',1:length(OPS.labels),ones(1,length(OPS.labels))));
    paramsSparseClassifier = OPS.paramsSparseClassifier;

    to = sizeHid*sizeVis;
    W1 = reshape(theta(1:to), sizeHid, sizeVis); % 8 x 27
    theta = theta(to+1:end);
    
    to = sizeOut*sizeHid;
    W2 = reshape(theta(1:to), sizeOut, sizeHid); % 2 x 8
    theta = theta(to+1:end);
    
    b1 = theta(1:sizeHid);                       % 8 x  1
    theta = theta(sizeHid+1:end);
    
    b2 = theta(1:sizeOut);                       % 2 x  1
    assert(length(theta)==sizeOut)

    % Cost and gradient variables (your code needs to compute these values). 
    % Here, we initialize them to zeros. 
    m  = size(X,2);

    %Forward Propogation
    a2 = OPS.act(bsxfun(@plus,W1*X,b1));%Layer_2
    a3 = OPS.act(bsxfun(@plus,W2*a2,b2));%Layer_3

    %Squared Error Term
    deriv3 = (a3-Y);
    slack3 = deriv3.*OPS.actDerivative(a3);%Layer_3
    slack2  = (W2'*slack3).*OPS.actDerivative(a2);%Layer_2

    %applySparsity
        roBase = paramsSparseClassifier.sparsityParam*ones(sizeHid,1);
        roFound = mean(a2,2);
        sparsityMat = calcSparseMat(roBase, roFound);
        sparsitySlackAdd = calcSparseSlack(a2, OPS.actDerivative, paramsSparseClassifier.sparsePenaltyWeight, sparsityMat);
        sparsityCost = calcSparseCost(roBase, roFound);
        
    slack2  = bsxfun(@plus,slack2,sparsitySlackAdd); % [<25x64 * 64x10000> .* <25x10000>] = <25x10000>
    slack2  = slack2.*OPS.actDerivative(a2);%Layer_2

    %Derivatives
    W2grad = (slack3*a2') ./m;
    b2grad = sum(slack3,2) ./m;
    %Apply weight decay here
    W2grad = W2grad + paramsSparseClassifier.weightDecayParam*W2;
    weightDecayCost_2 = (paramsSparseClassifier.weightDecayParam/2)*sum(W2(:).^2);

    W1grad = (slack2*X') ./m;
    b1grad = sum(slack2,2)./m;
    %Apply weight decay here
    W1grad = W1grad + paramsSparseClassifier.weightDecayParam*W1;
    weightDecayCost_1 = (paramsSparseClassifier.weightDecayParam/2)*sum(W1(:).^2);
    
    cost = sum(sum(deriv3.^2))/(2*m);%(0.5)*(1/m)*(deriv3(:)'*deriv3(:));
    cost = cost + weightDecayCost_1+weightDecayCost_2;%applyWeightDecay
    cost = cost + paramsSparseClassifier.sparsePenaltyWeight*sparsityCost;  %applySparsity

    %-------------------------------------------------------------------
    % After computing the cost and gradient, 
    % we will convert the gradients back to a vector format (suitable for minFunc).  
    % Specifically, we will unroll gradient matrices into a vector.
    grad = [W1grad(:) ; W2grad(:) ; b1grad(:) ; b2grad(:)];
    [sampleActivationMat] = calcHidNodeActStats(a2, OPS.labels, 0.001);
    [~,groupHat] = max(a3);
    confMatCur = confusionmat(OPS.labels, groupHat);
    accCur = sum(diag(confMatCur))/sum(confMatCur(:));
    
    % Visualize the process
    if isfield(OPS,'saveOpts') && ~isempty(OPS.saveOpts) && ~strcmp(OPS.saveOpts.saveLevel,'none')
        callerFuncNameString = getCallerFuncName(4);        
        minFuncAdditionalInfoStr = minFuncStepInfo_GrabFigureTitle(curStepSparseClassifier, cost);
        
        h = figure(65);clf;
        rc = 3; cc = 2;
        subplotW_softfull(W1 ,rc, cc, 1, 1, callerFuncNameString, minFuncAdditionalInfoStr);
        subplotdW_softfull(W1grad ,rc, cc, 1, 2);
        subplotW_softfull(W2 ,rc, cc, 2, 1);
        subplotdW_softfull(W2grad ,rc, cc, 2, 2);
        subplotsamplActMat(sampleActivationMat ,rc, cc, 3, 1);
        subplotConfMat(confMatCur ,rc, cc, 3, 2);

        figureSaveFolder = [OPS.saveOpts.MainFolder 'sparseAutoEncoder' filesep ];
        if (~exist(figureSaveFolder,'dir'))
            mkdir(figureSaveFolder);
        end        
        fileName2Save = [figureSaveFolder 'sparseAutoEncoderWeightComparison_i(' num2str(curStepSparseClassifier,'%03d') ')_cost(' num2str(cost,'%6.4g') ').png'];
            saveas(h,fileName2Save);
    end
    optionalOutputs = assignOptionalObjectiveFunctionOutputs(OPS, struct('hidLayeractivations',a2,'accuracy',accCur));
    optionalOutputs.sampleActivationMat = sampleActivationMat;
end

function [sampleActivationMat] = calcHidNodeActStats(a2, labels, treshAct)
    uniqLabels = unique(labels);
    hidNodeCnt = size(a2, 1);
    labelCnt = length(uniqLabels);
    sampleActivationMat = zeros(labelCnt, hidNodeCnt);
    for r_l = 1:labelCnt
        slct = labels==r_l;
        for c_hn = 1:hidNodeCnt
            sampleActivationMat(r_l,c_hn) = sum(a2(c_hn,slct)>treshAct);
        end
    end
end

function sparsitySlackAdd = calcSparseSlack(a2, actDerFunc, sparsePenaltyWeight, sparsityMat)
    sparsitySlackAdd = bsxfun(@times,actDerFunc(a2),(sparsePenaltyWeight*sparsityMat));
end
function sparsityMat = calcSparseMat(r, rh)
    sparsityMat = (((1-r)./(1-rh)) - (r./rh));
end

function sparsityCost = calcSparseCost(r, rh)
    sparsityCost =  (log(r./rh).*r) + (log((1-r)./(1-rh)).*(1-r));
    sparsityCost = real(sum(sparsityCost));
end

function subplotW_softfull(W ,rc, cc, r, c, callerFuncNameString, minFuncAdditionalInfoStr)
    subplotID = c + cc*(r-1);
    subplot(rc,cc,subplotID);imagesc(W);colorbar;
        set(gca,'XTick',ceil(linspace(1,size(W,2),min(8,round(size(W,2)/4)))));
        set(gca,'YTick',ceil(linspace(1,size(W,1),min(8,size(W,1)))));
        xlabel(['Previous Layer Dimension(' num2str(size(W,2)) ')']);
        ylabel(['Next Layer Dimension(' num2str(size(W,1)) ')']);
        titleStr_Weight = {['W_{minVal(' num2str(min(W(:)),'%6.4f') ')},W_{maxVal(' num2str(max(W(:)),'%6.4f') ')},W_{difVal(' num2str(max(W(:))-min(W(:)),'%6.4f') ')}']};
        if exist('callerFuncNameString','var') && ~isempty(callerFuncNameString)
            titleStr_Weight = {['callerFunc(' callerFuncNameString ')'],titleStr_Weight{:}};
        end
        if exist('minFuncAdditionalInfoStr','var') && ~isempty(minFuncAdditionalInfoStr)
            titleStr_Weight = {minFuncAdditionalInfoStr, titleStr_Weight{:}};
        end
        title(titleStr_Weight);
end

function subplotdW_softfull(dW ,rc, cc, r, c)
    subplotID = c + cc*(r-1);
    subplot(rc,cc,subplotID);imagesc(dW);colorbar;
        set(gca,'XTick',ceil(linspace(1,size(dW,2),min(8,round(size(dW,2)/4)))));
        set(gca,'YTick',ceil(linspace(1,size(dW,1),min(8,size(dW,1)))));
        xlabel('deltaWeight(dW)');
        titleStr_Weight = ['dW_{minVal(' num2str(min(dW(:)),'%6.4f') ')},dW_{maxVal(' num2str(max(dW(:)),'%6.4f') ')},dW_{difVal(' num2str(max(dW(:))-min(dW(:)),'%6.4f') ')}'];
        title(titleStr_Weight);
end

function subplotsamplActMat(sampleActivationMat ,rc, cc, r, c)
    subplotID = c + cc*(r-1);
    subplot(rc,cc,subplotID);imagesc(sampleActivationMat);colorbar;
        set(gca,'XTick',ceil(linspace(1,size(sampleActivationMat,2),min(8,round(size(sampleActivationMat,2)/4)))));
        set(gca,'YTick',ceil(linspace(1,size(sampleActivationMat,1),min(8,size(sampleActivationMat,1)))));
        xlabel('sampleActivationMat(sam)');
end

function subplotConfMat(confMatCur ,rc, cc, r, c)
    subplotID = c + cc*(r-1);
    accCur = sum(diag(confMatCur))/sum(confMatCur(:));
    subplot(rc,cc,subplotID);imagesc(confMatCur);colorbar;
        set(gca,'XTick',ceil(linspace(1,size(confMatCur,2),min(8,round(size(confMatCur,2)/4)))));
        set(gca,'YTick',ceil(linspace(1,size(confMatCur,1),min(8,size(confMatCur,1)))));
        title(['Accuracy(' num2str(accCur,'%4.2f') ')']);
end