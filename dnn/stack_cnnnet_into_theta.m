function [ theta ] = stack_cnnnet_into_theta( cnnnet )
    layerCount = size(cnnnet,1);
    activationID = 1;
    W_all = [];
    b_all = [];
    for layer = 1:layerCount
        curCell = cnnnet{layer,1};
        switch curCell.type
            case 'convolution'
                W_all = [W_all ; curCell.filtersWhitened(:)];
                b_all = [b_all ; curCell.biasesWhitened(:)];
            case 'activation'
            case 'pooling'
            case 'softmax'
                W_all = [W_all ; curCell.Weight(:)];
                b_all = [b_all ; curCell.BiasParams(:)];
                %b not used in softmax
            case 'fullyconnected'
                W_all = [W_all ; curCell.filtersWhitened(:)];
                b_all = [b_all ; curCell.biasesWhitened(:)];
            otherwise
                error('error');
        end
    end
    theta = [W_all ; b_all];
end

