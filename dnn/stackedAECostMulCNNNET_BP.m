function [ cost, grad, optionalOutputs] = stackedAECostMulCNNNET_BP(theta, OPS)%,cnnnet, dataStruct, data, labels,displayOpts, saveOpts, memoryMethod)
%OPS = objFuncParamsStruct )%
    [objFuncParamsStructCorrect, missingFields] = check_objFuncParamsStruct(OPS, {'cnnnet',{'dataStruct',{'data','labels'}},'displayOpts','saveOpts'});
    if objFuncParamsStructCorrect==false
        error(['There are missing fields in objective function parameter struct OPS ' missingFields]);
    end
    OPS.useAccuracyAsCost = getStructField(OPS,'useAccuracyAsCost',false);
    OPS.calculateGradient = getStructField(OPS,'calculateGradient',true);    
    
    calculateBackPropMap = true;    
    global curStep;
    if isempty(curStep)
        curStep = 1;
    else
        curStep = curStep +1;
    end
    global minFuncStepInfo;
    
%% STEP 1: The  initialization of weights, activation cells etc 
    %sigm = @(z_) 1.0 ./ (1.0 + exp(-z_));
    %sigmDeriva = @(a_) a_.*(1-a_);
    %smwv = theta(196801:198800);%this line is for error check with breakpoint
    if ~isnan(theta)
        %if theta not needed to be updated - then it is passed as NaN
        cnnnet = update_cnnnet_by_theta(theta, OPS.cnnnet(:), OPS.displayOpts);
    end
    
    %layerCount is tricky when it comes to slack, weight and activation
    %calculations. an activation layer always appends a convolution/fullConnected or softMax layer hence the have to seem like 1 layer indeed
    layerCount = size(cnnnet,1)+1;%for two network layers this is 3
    %cellCount = layerCount - layerTypeCount(cnnnet, 'activation', false);
    if (~isfield(OPS,'memoryMethod') || isempty(OPS.memoryMethod)), OPS.memoryMethod = 'all_in_memory'; end;%<'write_to_disk', 'all_in_memory'>

%% STEP 2: Start to fill in the variables
%     countSamples = size(data, 2);
%% STEP 3: Compute Forward Propogation
% the forward propogation is already implemented
% the question is to rather write the slack variables in disk drive for
% more free space in the RAM. if so, we can do it by setting a temp folder
% somewhere, define names to use temporarily, write the slack variables and
% activations there
%The needed activations for backpropogation are : after pooling and
%activation layers plus the initial layer(maybe not needed)
    if (OPS.displayOpts.steps==1)
        disp(['++++++++' 'curStep(' num2str(curStep) ')-Forward Propogation++++++++'])
    end
    vararginParams = {'calculateBackPropMap', calculateBackPropMap,'displayOpts', OPS.displayOpts,'saveOpts', OPS.saveOpts,'curStep', curStep,'memoryMethod',OPS.memoryMethod};
    if isfield(OPS,'dataStruct')
        vararginParams = {vararginParams{:},'dataStruct',OPS.dataStruct};
    end
    
    [ cnnnet, softMaxedData, accuracyTrainDeep, confTrainDeep, activationID] = forwardPropogateDeepNet( OPS.data, OPS.labels, cnnnet, vararginParams{:});
    minFuncStepInfo.Accuracy = accuracyTrainDeep;
    minFuncStepInfo.ConfMat = confTrainDeep;

    checkToPause(mfilename,[],'after-forwardPropogateDeepNet');
    if OPS.calculateGradient
    %% STEP 4: Compute Squared Error Term
        [cost, W_grad, b_grad] = computeSquaredErrorTerm( cnnnet, softMaxedData, OPS.labels, OPS.saveOpts, OPS.memoryMethod, activationID, OPS.displayOpts, curStep);
        try
            if isfield(OPS,'dataStruct')
                get_WbCells_from_cnnnet(cnnnet, W_grad, b_grad, OPS.displayOpts, OPS.saveOpts, OPS.dataStruct, curStep);
            else
                get_WbCells_from_cnnnet(cnnnet, W_grad, b_grad, OPS.displayOpts, OPS.saveOpts, [], curStep);
            end
        catch
            W_grad = [];
            b_grad = [];
        end
    else
        W_grad = [];
        b_grad = [];
    end                                                                    
    if OPS.useAccuracyAsCost  
        cost = 100-accuracyTrainDeep;
    end
%% STEP 5 : display confusion matrix
    if ( ~strcmp(OPS.displayOpts.figureOpts.Show.Confusion,'None') &&  ~strcmp(OPS.saveOpts.Figures.Results,'None'))
        callerFuncNameString = getCallerFuncName(1);
        minFuncAdditionalInfoStr = minFuncStepInfo_GrabFigureTitle(curStep, cost, accuracyTrainDeep);
        show_save_train_step(confTrainDeep, accuracyTrainDeep, curStep, layerCount, OPS.displayOpts, OPS.saveOpts, OPS.dataStruct, callerFuncNameString, minFuncAdditionalInfoStr);
    end
    lastActivationLayerID = 0;
    for i=size(cnnnet,1):-1:1
        if cnnnet{i, 1}.LastActivationLayer
            lastActivationLayerID = cnnnet{i, 1}.ActivationLayerID;
        end
        if isfield(cnnnet{i,1},'activatedData') && lastActivationLayerID~=i
            cnnnet{i,1}.activatedData = [];
        end
    end
    if OPS.calculateGradient
    %% STEP 6: Roll gradient vector
        %[ theta ] = stack_cnnnet_into_theta( cnnnet );
        grad = roll_grad_from_stacks( W_grad,  b_grad);
    else
        grad = [];
    end
%% STEP 7 : 
    OPS.cnnnet = {cnnnet};
    optionalOutputs = assignOptionalObjectiveFunctionOutputs(OPS, struct('accuracy',accuracyTrainDeep));
end

function show_save_train_step(confTrainDeep, accuracyTrainDeep, curStep, layerCount, displayOpts, saveOpts, dataStruct, callerFuncNameString, minFuncAdditionalInfoStr)
    %show the figure if one of the following conditions is met
    %1. not show confusion never
    %2. not save result figures never
    if ( ~strcmp(displayOpts.figureOpts.Show.Confusion,'None') &&  ~strcmp(saveOpts.Figures.Results,'None'))
        %set the file namne to save according to last or result
        figureSaveFolder = [saveOpts.MainFolder saveOpts.SubFolders.ResultImages];
        if (~exist(figureSaveFolder,'dir'))
            mkdir(figureSaveFolder);
        end        
        if (strcmp(saveOpts.Figures.Results,'Last'))
            if (dataStruct.countBatch.train>1)
                figureFileName2Save = [figureSaveFolder 'Result_b' num2str(dataStruct.batchID_current,'%02d') '_acc(' num2str(accuracyTrainDeep,'%4.2f') ').png'];
            else
                figureFileName2Save = [figureSaveFolder 'Result_acc(' num2str(accuracyTrainDeep,'%4.2f') ').png'];
            end
        elseif (strcmp(saveOpts.Figures.Results,'Every'))
            minFuncAdditionalFileNameInfoStr = getMinFuncAdditionalFileNameInfoStr(curStep);
            if (dataStruct.countBatch.train>1)
                figureFileName2Save = [figureSaveFolder 'Result_' minFuncAdditionalFileNameInfoStr '_b' num2str(dataStruct.batchID_current,'%02d') '_acc(' num2str(accuracyTrainDeep,'%4.2f') ').png'];
            else
                figureFileName2Save = [figureSaveFolder 'Result_' minFuncAdditionalFileNameInfoStr '_acc(' num2str(accuracyTrainDeep,'%4.2f') ').png'];
            end
        else
            figureFileName2Save = [];
        end
        
        if (dataStruct.countBatch.train>1)
            titleStr = ['Result_{' num2str(curStep,'%02d') '}, Acc_{' num2str(accuracyTrainDeep,'%4.2f') '}, BatchID_{' num2str(dataStruct.batchID_current,'%02d') '}'];
        else
            titleStr = ['Result_{' num2str(curStep,'%02d') '}, Acc_{' num2str(accuracyTrainDeep,'%4.2f') '}'];
        end
        if (~isempty(callerFuncNameString) && ~strcmp(callerFuncNameString,''))
            callerFuncNameString = strrep(callerFuncNameString,'_','\_');
            titleStr = {['callerFunc(' callerFuncNameString ')'],titleStr};
        end       
        if (~isempty(minFuncAdditionalInfoStr) && ~strcmp(minFuncAdditionalInfoStr,''))
            titleStr = {['minFuncAdditionalInfoStr(' minFuncAdditionalInfoStr ')'],titleStr{:}};
        end       
        if iscell(titleStr)
            h = drawConfusionDetailed( confTrainDeep, struct('figureID', {layerCount}, 'figureTitle' , {titleStr}));
        else
            h = drawConfusionDetailed( confTrainDeep, struct('figureID', {layerCount}, 'figureTitle' , titleStr));
        end        
        %is the figure only be displayed or also be saved
        if (~isempty(figureFileName2Save))
            %saving part
            if (displayOpts.fileOperations==1)
                disp([displayOpts.ies{4} 'Figure with id(' num2str(layerCount) ') will be saved as (' figureFileName2Save ')']);
            end
            saveas(h,figureFileName2Save);
            if (displayOpts.fileOperations==1)
                disp([displayOpts.ies{4} 'Figure with id(' num2str(layerCount) ') has been saved as (' figureFileName2Save ')']);
            end
        end
    end
end

function minFuncAdditionalFileNameInfoStr = getMinFuncAdditionalFileNameInfoStr(curStep)
    global minFuncStepInfo;
    minFuncAdditionalFileNameInfoStr = ['fe(' num2str(curStep,'%02d') ')'];
    if ~isempty(minFuncStepInfo) && isstruct(minFuncStepInfo) && isfield(minFuncStepInfo,'Iteration')
        minFuncAdditionalFileNameInfoStr = ['it(' num2str(minFuncStepInfo.Iteration,'%02d') ')_' minFuncAdditionalFileNameInfoStr];
    end              
end
%     switch memoryMethod
%         case 'all_in_memory'
%             %we will have the activations and slack variables in memory
%             W = cell(1,cellCount-1);%possible count of Weight parameter groups
%             b = cell(1,cellCount-1);%possible count of b parameter groups
%             a = cell(1,cellCount);%possible count of activations + input data
%             tempFolder_Train = [];
%             %tempFolder_Test = [];
%          case 'write_to_disk'
%             %we will write the activations and slack variables into disk
%             %then read from disk when necessary
%             global fastDiskTempPath;
%             tempFolder_Train = [fastDiskTempPath 'Train\'];%[];%
%             if ~isempty(tempFolder_Train) && ~exist(tempFolder_Train,'dir')
%                 mkdir(tempFolder_Train);
%             end
%             %tempFolder_Test = [fastDiskTempPath '\Test\'];%[];%
%             %if ~isempty(tempFolder_Test) && ~exist(tempFolder_Test,'dir')
%             %    mkdir(tempFolder_Test);
%             %end
%             %W      - [tempFolder 'W_ii.mat']     - no need to save because
%             %these are calculated per layer type in (calcSlack) function
%             %Wgrad  - [tempFolder 'Wgrad_ii.mat']
%             %b      - [tempFolder 'b_ii.mat']
%             %bGrad  - [tempFolder 'bGrad_ii.mat']
%             %slack  - [tempFolder 'slack_ii.mat']
%             %a      - [tempFolder 'a_ii.mat']
% 
%             %or
%             
%             % this seems to be better - this is being implemented
%             % W, Wgrad, b, bGrad [tempFolder 'Weight_Bias_ii.mat']
%             % a                  [tempFolder 'Activation_ii.mat'] - this
%             % part is implemented in convolution, activation and  layers
%             % slack              [tempFolder 'Slack_ii.mat']
%     end