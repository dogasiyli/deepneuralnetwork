function [ W, opttheta, outputCellArr, firstLayerActivations] = trainAutoEncoder( data, varargin)       
  %% STEP 0 : Explain the function (TRAINAUTOENCODE)
    %  [ X,opttheta] = trainAutoEncode( data, initParams, maxIter )
    %  takes the data and trains an autoencoder using it
    %
    %  Input Variables
    %
    %  Input_01 : data
    %  - This is the only REQUIRED input.
    %  - The data should be in such form that:
    %    1. [sizeVis,countSamples] = size(data);
    %    2. all the values of each feature of each sample should be between(-1,1)
    %       or (0,1)
    %
    %  Optional Inputs
    %
    %  OUTPUT VARIABLES
    %
    %  Out_01 : Weights
    %  Out_02 : opttheta
    %  Out_03 : output
    %  Out_04 : firstLayerActivations

% STEP 1: Initialize the necessary variables
    [sizeHid, theta, paramsAutoEncode, paramsMinFunc, applySparsity, useActSecondLayer, displayOpts, saveOpts] = getVararginParams(data, varargin{:});

% STEP-2 : extractVariables
    [sizeVis, act, actDerivative] = extractVariables(data, paramsAutoEncode);
    
% STEP 3: Train your sparse autoencoder with minFunc
    global curStepsparseAutoEncode;
    curStepsparseAutoEncode = 0;
    %OPS = objFuncParamsStruct
    OPS = struct( 'sizeVis',sizeVis,'sizeHid',sizeHid...
                 ,'paramsAutoEncode',paramsAutoEncode...
                 ,'data',data...
                 ,'act',act,'actDerivative',actDerivative...
                 ,'applySparsity', applySparsity, 'useActSecondLayer', useActSecondLayer...
                 ,'displayOpts',displayOpts,'saveOpts',saveOpts);
    checkToPause([mfilename '-mfdgVSmf-sparseAutoEncoderCost'],[],'(compare minFunc_Doga vs minFunc)');
    [opttheta,~,~,outputCellArr] = minFunc_Doga(@sparseAutoEncoderCost, theta, paramsMinFunc, OPS);
    %[opttheta,costFinal,exitflag,outputParams] = minFunc( @(p) sparseAutoEncoderCost(p,OPS), theta, paramsMinFunc);
% STEP-4: Visualization 
    %W = reshape(opttheta(sizeHid*sizeVis+1:2*sizeHid*sizeVis), sizeVis, sizeHid)';
    W = reshape(opttheta(1:sizeHid*sizeVis), sizeHid, sizeVis);
    if nargout==4
        OPS.optionalOutputs.hidLayeractivations = [];
        [~,~,OPS.optionalOutputs] = sparseAutoEncoderCost(opttheta,OPS);
        firstLayerActivations = OPS.optionalOutputs.hidLayeractivations;
    end
end

% STEP-1: Initialize the necessary variables
function [sizeHid, theta, paramsAutoEncode, paramsMinFunc, applySparsity, useActSecondLayer, displayOpts, saveOpts] = getVararginParams(data, varargin)
    %P1D0 -> Passed assed parameter = 1, Default value used = 0
    [   paramsAutoEncode,  paramsCategory_MinFunc,     paramsMinFunc,  displayOpts,  saveOpts,  OPS_in,  applySparsity,  useActSecondLayer, P1D0] = parseArgs(...
    {  'paramsAutoEncode' 'paramsCategory_MinFunc'    'paramsMinFunc' 'displayOpts' 'saveOpts' 'OPS_in' 'applySparsity' 'useActSecondLayer' },...
    {         -1              'showNone_noLog'               []            -1           []        []          true            false         },...
    varargin{:});

    if (~isstruct(displayOpts) && displayOpts==-1)
        displayOpts = setDisplayOpts();
    end
    paramsAutoEncode = setParams_Missing_Autoencoder(paramsAutoEncode);
    
    sizeIn = size(data,1);
    sizeOut = sizeIn;
    if isempty(OPS_in)
        OPS_in = struct;
    else
        if isfield(OPS_in,'applySparsity')
            applySparsity = OPS_in.applySparsity;
        end
        if isfield(OPS_in,'useActSecondLayer')
            useActSecondLayer = OPS_in.useActSecondLayer;
        end
    end
    switch paramsAutoEncode.thetaInitMode
        case 'Random'
            OPS_in.data = data;
            cond1 = isfield(OPS_in,'sizeHid');
            cond2 = isfield(OPS_in,'percentToPreserve');
            cond3 = isfield(OPS_in,'data');
            assert(cond1 || (cond2 && cond3), 'This must be satisfied');
            cond4 = isfield(OPS_in,'varianceMode');
            assert(cond4, 'This must be satisfied');
        case 'UseData'
            OPS_in.data = data;
            cond1 = isfield(OPS_in,'data');
            assert(cond1, 'This must be satisfied');            
            cond2 = isfield(OPS_in,'initModeData');
            assert(cond2, 'This must be satisfied');            
        case 'FromKnownParams'
            %either theta is known or some W1 is passed as param
            %'initialTheta'
    end
    [theta, OPS_out] = init2LayerNetParams(paramsAutoEncode.thetaInitMode, sizeIn, sizeOut, OPS_in);
    sizeHid = OPS_out.layerSizes(2);
    if (displayOpts.defaultParamSet==1)
        disp([displayOpts.ies{4} 'theta is initialized(' OPS_out.info ').']);
    end

    if isempty(paramsMinFunc)
        paramsMinFunc = setParams_minFunc(paramsCategory_MinFunc);
    end
    %if paramsAutoEncode is passed as parameter and has maxIter as a field
    %then assign the maxIter to paramsMinFunc parameter too
    if (P1D0.paramsAutoEncode==1 && isfield(paramsAutoEncode,'maxIter'))
        paramsMinFunc.maxIter = paramsAutoEncode.maxIter;
    end
    %if paramsMinFunc is passed as parameter and has maxIter as a field
    %then assign the maxIter to autoencoder parameter too
    if (P1D0.paramsMinFunc==1 && isfield(paramsMinFunc,'maxIter'))
        paramsAutoEncode.maxIter = paramsMinFunc.maxIter;
    end
    
    if (displayOpts.defaultParamSet==1)
        if (P1D0.paramsAutoEncode==0)
            disp([displayOpts.ies{4} 'Autoencoder options are not passed as function parameter. Hence default values will be used.']);
            disp([displayOpts.ies{4} 'sparsityParam(' num2str(paramsAutoEncode.sparsityParam) ')/weightDecayParam(' num2str(paramsAutoEncode.weightDecayParam) ')/sparsePenaltyWeight(' num2str(paramsAutoEncode.sparsePenaltyWeight)  ')/activationType(' paramsAutoEncode.activationType ')']);
        end
        if (P1D0.paramsMinFunc==0 && P1D0.paramsCategory_MinFunc==0)
            %only if both of them are default - there is a default action to inform
            disp([displayOpts.ies{4} 'paramsMinFunc is not passed. It will be initialized using (' paramsCategory_MinFunc ') settings.']);
        end
    end
end

% STEP-2 : extractVariables
function [sizeVis, act, actDerivative] = extractVariables(data, paramsAutoEncode)
    sizeVis = size(data,1);
    %[act, actDerivative] = setActivationFunction(paramsAutoEncode.activationType);
    %ReLu can cause problems.. set sigmoid until finding a solution to that problem..
    [act, actDerivative] = setActivationFunction('sigm');
end
%% STEP 2 explanations: Train your sparse autoencoder with minFunc
    %[x,f,exitflag,output] = minFunc(funObj,x0,options,varargin)
    % Outputs:
    %   x is the minimum value found
    %   f is the function value at the minimum found
    %   exitflag returns an exit condition
    %   output returns a structure with other information
    % Supported Output Options
    %   iterations - number of iterations taken
    %   funcCount - number of function evaluations
    %   algorithm - algorithm used
    %   firstorderopt - first-order optimality
    %   message - exit message
    %   trace.funccount - function evaluations after each iteration
    %   trace.fval - function value after each iteration

%% DELETED CODE
% % STEP 0: Global parameters
%     global minFuncPath;
%     assert(~isempty(minFuncPath),'You have to initialize the minFuncPath variable by running <runFirst.m>');
