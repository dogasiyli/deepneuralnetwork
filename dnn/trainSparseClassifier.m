function [ W, b, opttheta] = trainSparseClassifier( data, labels, sizeHid, varargin)   
% STEP 1: Initialize the necessary variables
    [theta, paramsSparseClassifier, paramsMinFunc, displayOpts, saveOpts] = getVararginParams(data, labels, sizeHid, varargin{:});

% STEP-2 : extractVariables
    [sizeVis, act, actDerivative] = extractVariables(data, paramsSparseClassifier);
    
% STEP 3: Train your sparse autoencoder with minFunc
    global curStepSparseClassifier;
    curStepSparseClassifier = 0;
    %OPS = objFuncParamsStruct
    OPS = struct( 'sizeVis',sizeVis,'sizeHid',sizeHid...
                 ,'paramsSparseClassifier',paramsSparseClassifier...
                 ,'data',data,'labels',labels...
                 ,'act',act,'actDerivative',actDerivative...
                 ,'displayOpts',displayOpts,'saveOpts',saveOpts);
    checkToPause([mfilename '-mfdgVSmf-trainSparseClassifier'],[],'(compare minFunc_Doga vs minFunc)');
    [opttheta,~,~,outputCellArr] = minFunc_Doga(@sparseClassifierCost, theta, paramsMinFunc, OPS);
    %[opttheta,costFinal,exitflag,outputParams] = minFunc( @(p) sparseAutoEncoderCost(p,OPS), theta, paramsMinFunc);
% STEP-4: Visualization 
    sizeOut = length(unique(labels));
    W = reshape(opttheta(1:sizeHid*sizeVis), sizeHid, sizeVis);%Weights = reshape(opttheta(1:sizeHid*sizeVis), sizeHid, sizeVis);
    b = opttheta((sizeHid*(sizeVis+sizeOut))+1:(sizeHid*(sizeVis+sizeOut))+ sizeHid);
    OPS.optionalOutputs.hidLayeractivations = [];
    [~,~,OPS.optionalOutputs] = sparseClassifierCost(opttheta,OPS);
    a2 = OPS.optionalOutputs.hidLayeractivations;
    
    opttheta2 = opttheta;
    a2m = a2;
    meanAct = zeros(sizeHid,1);
    for h = 1 : sizeHid
        meanAct(h) = mean(a2m(h,a2m(h,:)>0));
    end
    b = opttheta2((sizeHid*(sizeVis+sizeOut))+1:(sizeHid*(sizeVis+sizeOut))+ sizeHid);
    opttheta2((sizeHid*(sizeVis+sizeOut))+1:(sizeHid*(sizeVis+sizeOut))+ sizeHid) = b-meanAct;
    [~,~,OPS.optionalOutputs] = sparseClassifierCost(opttheta2,OPS);
    a2m = OPS.optionalOutputs.hidLayeractivations;
    
    opttheta3 = opttheta2;
    maxAct = max(a2m,[],2)/10000;
    W2 = W./1.8;%W2 = bsxfun(@rdivide,W,maxAct);
    opttheta3(1:sizeHid*sizeVis) = W2(:); 
    [~,~,OPS.optionalOutputs] = sparseClassifierCost(opttheta3,OPS);
    a2w = OPS.optionalOutputs.hidLayeractivations;

    %try ELM on this new activations
    [accuracyCalced, predictedLabels, W_randoms_with_bias, Welm, A, confMatResult] = ELM_training(a2, labels, 'sizeHid', 20,'displayOpts',displayOpts);
end

% STEP-1: Initialize the necessary variables
function [theta, paramsSparseClassifier, paramsMinFunc, displayOpts, saveOpts] = getVararginParams(data, labels, sizeHid, varargin)
    %P1D0 -> Passed assed paramaeter = 1, Default value used = 0
    [ paramsSparseClassifier,  paramsCategory_MinFunc,     paramsMinFunc,  displayOpts,  saveOpts,  OPS_in, P1D0] = parseArgs(...
    {'paramsSparseClassifier' 'paramsCategory_MinFunc'    'paramsMinFunc' 'displayOpts' 'saveOpts' 'OPS_in'},...
    {          -1                 'showNone_noLog'               []            -1           []        []   },...
    varargin{:});

    if (~isstruct(displayOpts) && displayOpts==-1)
        displayOpts = setDisplayOpts();
    end
    paramsSparseClassifier = setParams_Missing_SparseClassifier(paramsSparseClassifier);
    
    
    sizeIn = size(data,1);
    sizeOut = length(unique(labels));
    if isempty(OPS_in)
        OPS_in = struct;
    end
    OPS_in.data = data;
    OPS_in.labels = labels;
    OPS_in.sizeHid = sizeHid;
    switch paramsSparseClassifier.thetaInitMode
        case 'Random'
            cond1 = isfield(OPS_in,'sizeHid');
            cond2 = isfield(OPS_in,'percentToPreserve');
            cond3 = isfield(OPS_in,'data');
            assert(cond1 || (cond2 && cond3), 'This must be satisfied');
            cond4 = isfield(OPS_in,'varianceMode');
            assert(cond4, 'This must be satisfied');
        case 'UseData'
            cond1 = isfield(OPS_in,'data');
            assert(cond1, 'This must be satisfied');            
            cond2 = isfield(OPS_in,'initModeData');
            assert(cond2, 'This must be satisfied');            
    end
    [theta, OPS_out] = init2LayerNetParams(paramsSparseClassifier.thetaInitMode, sizeIn, sizeOut, OPS_in);
    sizeHid = OPS_out.layerSizes(2);
    if (displayOpts.defaultParamSet==1)
        disp([displayOpts.ies{4} 'theta is initialized(' OPS_out.info ').']);
    end
    
    
    if isempty(paramsMinFunc)
        paramsMinFunc = setParams_minFunc(paramsCategory_MinFunc);
    end
    %if paramsSparseClassifier is passed as parameter and has maxIter as a field
    %then assign the maxIter to paramsMinFunc parameter too
    if (P1D0.paramsSparseClassifier==1 && isfield(paramsSparseClassifier,'maxIter'))
        paramsMinFunc.maxIter = paramsSparseClassifier.maxIter;
    end
    %if paramsMinFunc is passed as parameter and has maxIter as a field
    %then assign the maxIter to autoencoder parameter too
    if (P1D0.paramsMinFunc==1 && isfield(paramsMinFunc,'maxIter'))
        paramsSparseClassifier.maxIter = paramsMinFunc.maxIter;
    end
    
    if (displayOpts.defaultParamSet==1)
        if (P1D0.paramsSparseClassifier==0)
            disp([displayOpts.ies{4} 'SparseClassifier options are not passed as function parameter. Hence default values will be used.']);
            disp([displayOpts.ies{4} 'sparsityParam(' num2str(paramsSparseClassifier.sparsityParam) ')/weightDecayParam(' num2str(paramsSparseClassifier.weightDecayParam) ')/sparsePenaltyWeight(' num2str(paramsSparseClassifier.sparsePenaltyWeight)  ')/activationType(' paramsSparseClassifier.activationType ')']);
        end
        if (P1D0.paramsMinFunc==0 && P1D0.paramsCategory_MinFunc==0)
            %only if both of them are default - there is a default action to inform
            disp([displayOpts.ies{4} 'paramsMinFunc is not passed. It will be initialized using (' paramsCategory_MinFunc ') settings.']);
        end
    end
end

% STEP-2 : extractVariables
function [sizeVis, act, actDerivative] = extractVariables(data, paramsSparseClassifier)
    sizeVis = size(data,1);
    [act, actDerivative] = setActivationFunction(paramsSparseClassifier.activationType);
end