% clear all;
% clc;
% load('H:\DeepNetTemp\Train\trainDataLabels.mat');
% %[rank(trainData'), size(trainData,1)]%train data is assumed to be dN
% [Welm, ~, cmTrain, trainAccuracy] = getELM_Weights_SoftMax_V2(trainData', trainLabels);
% load('H:\DeepNetTemp\Test\testDataLabels.mat');
% disp('Test data analysis of ELM :');
% [~, ~, ~, testAccuracy, cmTest] = analyzeActivationsElm(testData', testLabels, Welm);
function [accuracyTrain, accuracyTest, conf_Train, conf_Test, W_randoms, Welm] = tryELMOnActivations(trainData, trainLabels, testData, testLabels, varargin)
    [ sizeHid,     paramsELM,  displayOpts] = getOptionalParams(trainData, varargin);
    
    [accuracyTrain, ~, W_randoms, Welm, ~, conf_Train] = ...
        ELM_training(trainData, trainLabels,...
                     'sizeHid', sizeHid,...
                     'paramsELM', paramsELM,...
                     'displayOpts', displayOpts);
    % load('H:\DeepNetTemp\Test\testDataLabels.mat');
    disp('Test data analysis of ELM :');
    [~, accuracyTest, conf_Test] = analyzeResults(testData, testLabels, W_randoms, Welm, displayInfo);
end

% STEP-1 : Set optional param defaults
function [ sizeHid,     paramsELM,  displayOpts] = getOptionalParams(data, varargin)
    %Optional inputs with default values
    %1. sizeHid - number of hidden layer neurons -  1600 for this case
    %2. paramsELM - ELM parameters
    %3. displayOpts - if want to get extra information displayed on screen
    %P1D0 -> Passed assed paramaeter = 1, Default value used = 0
    [   sizeHid,     paramsELM,  displayOpts, P1D0] = parseArgs(...
    {  'sizeHid'    'paramsELM' 'displayOpts' },...
    { size(data,1)      -1            -1      },...
    varargin{:});

    if (~isstruct(displayOpts) && displayOpts==-1)
        displayOpts = setDisplayOpts();
    end
    if (~isstruct(paramsELM) && paramsELM==-1)
        paramsELM = setParams_ELM('Standard');
    end
    if (displayOpts.defaultParamSet==1)
        %display the default parameters selected
        if (P1D0.sizeHid==0)
            disp([displayOpts.ies{4} 'sizeHid of the hidden layer is set to default as the equal dimension of data(' num2str(sizeHid) ').']);
        end
        if (P1D0.displayOpts==0)
            disp([displayOpts.ies{4} 'displayOpts options are not passed as function parameter. Hence default values will be used.']);
        end
        if (P1D0.paramsELM==0)
            disp([displayOpts.ies{4} 'ELM Parameters are not passed as function parameter. Hence default values will be used.']);
            disp([displayOpts.ies{4} 'randomWeightInitialization(' paramsELM.randomWeightInitialization ')/useReceptiveFields(' paramsELM.useReceptiveFields ')']);
        end
    end
end