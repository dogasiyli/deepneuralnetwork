function cnnnet = update_cnnnet_by_theta(theta, cnnnet, displayOpts)  
%% 1. Analyze the network structure of cnnnet
    %cnnnet = cell(7,1); --> cnnnet is initialied somewhat like this
    depth = size(cnnnet,1);
    for layer = 1:depth
        curCell = cnnnet{layer,1};
        switch curCell.type
            case 'convolution'
                %convolutionCell.filtersWhitened = rand(convolutionCell.inputChannelSize*prod(convolutionCell.filterSize),convolutionCell.countFilt);
                %Weight - convolutionCell.filtersWhitened
                w_len = curCell.inputChannelSize*prod(curCell.filterSize)*curCell.countFilt;
                W = reshape(theta(1:w_len),[prod(curCell.filterSize)*curCell.inputChannelSize, curCell.countFilt]);
                if (exist('displayOpts','var') && ~isempty(displayOpts) && strcmp(displayOpts.displayLevel,'all'))
                    difOfConvSum = sum(abs(curCell.filtersWhitened(:)-W(:)));
                    disp([displayOpts.ies{6} 'Total parameter difference of convolution weights(' num2str(layer) ') = ' num2str(difOfConvSum,'%9.7f')]);
                end
                curCell.filtersWhitened = W;
                theta = theta(w_len+1:end);
                if isfield(curCell,'W_Conv_Mapped')
                    curCell = rmfield(curCell,'W_Conv_Mapped');
                end
            case 'activation'
                %activation layers do not have weights and biases
            case 'pooling'
                %pooling layers do not have weights and biases
            case 'softmax'
                w_len = numel(curCell.Weight);
                W = theta(1:w_len);
                if (exist('displayOpts','var') && ~isempty(displayOpts) && strcmp(displayOpts.displayLevel,'all'))
                    difOfSoftMaxSum = sum(abs(curCell.Weight(:)-W(:)));
                    disp([displayOpts.ies{6} 'Total parameter difference of softmax weights(' num2str(layer) ') = ' num2str(difOfSoftMaxSum,'%9.7f')]);
                end
                curCell.Weight = reshape(W, curCell.countCategory, curCell.countFeat);
                theta = theta(w_len+1:end);
            case 'fullyconnected'
                w_len = numel(curCell.filtersWhitened);
                W = theta(1:w_len);
                if (exist('displayOpts','var') && ~isempty(displayOpts) && strcmp(displayOpts.displayLevel,'all'))
                    difOfFullyConnectedSum = sum(abs(curCell.filtersWhitened(:)-W(:)));
                    disp([displayOpts.ies{6} 'Total parameter difference of fully connected layer weights(' num2str(layer) ') = ' num2str(difOfFullyConnectedSum,'%9.7f')]);
                end
                curCell.filtersWhitened = reshape(W,size(curCell.filtersWhitened));
                theta = theta(w_len+1:end);
            otherwise
                error([curCell.type ' is not implemented yet'])
        end
        cnnnet{layer,1} = curCell;
    end    
   for layer = 1:depth
        curCell = cnnnet{layer,1};
        switch curCell.type
            case 'convolution'
                %convolutionCell.biasesWhitened = zeros(convolutionCell.countFilt, 1);
                %Bias   - convolutionCell.biasesWhitened
                b_len = curCell.countFilt;
                b = reshape(theta(1:b_len),[curCell.countFilt, 1]);
                curCell.biasesWhitened = b;
                theta = theta(b_len+1:end);
            case 'activation'
                %activation layers do not have weights and biases
            case 'pooling'
                %pooling layers do not have weights and biases
            case 'softmax'
                b_len = curCell.countCategory;
                b = reshape(theta(1:b_len),curCell.countCategory,1);
                curCell.BiasParams = b;
                theta = theta(b_len+1:end);
                assert(isempty(theta),'theta should have been empty by now')
                theta = [];
            case 'fullyconnected'
                b_len = size(curCell.biasesWhitened,1);
                b = reshape(theta(1:b_len),size(curCell.biasesWhitened));
                if (exist('displayOpts','var') && ~isempty(displayOpts) && strcmp(displayOpts.displayLevel,'all'))
                    difOfFullyConnectedSum = sum(abs(curCell.biasesWhitened(:)-b(:)));
                    disp([displayOpts.ies{6} 'Total parameter difference of fully connected layer bias params(' num2str(layer) ') = ' num2str(difOfFullyConnectedSum,'%9.7f')]);
                end
                curCell.biasesWhitened = b;
                theta = theta(b_len+1:end);
            otherwise
                error([curCell.type ' is not implemented yet'])
        end
        cnnnet{layer,1} = curCell;
    end        
end