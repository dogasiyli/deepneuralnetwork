function [ X_upsampled, learntParams ] = upsample( X_pooled, poolSize, pooled_size_original)
    %input size can be - X_pooled = reshape(1:prod([2 2 3 5]), [2 2 3 5]);
    %[Rp Cp F countSamples] or
    %[Rp Cp countSamples]
    
    %example run :
    %poolSize = [2 2];
    %X_in = reshape(1:prod([2 2 3 5]), [2 2 3 5]);
    %[X_upsampled, learntParams ] = upsample( X_in, poolSize);
    %X_upsampled_reshaped = reshape(X_upsampled,learntParams.upsampled_size_vectorized);
    
    
    size_Input = size(X_pooled);
    upSampleMode = length(size_Input);
    switch upSampleMode
        case 2%[Rp*Cp countSamples] or [Rp*Cp*F countSamples]
            %this needs to have pooled_size_original
            if nargin<3
                error('unknown structure of pooled images');
            end
            X_pooled = reshape(X_pooled, pooled_size_original);
            [ X_upsampled, learntParams ] = upsample( X_pooled, poolSize);
        case 3%[Rp Cp countSamples]
            Rp = size_Input(1);
            Cp = size_Input(2);
            countSamples = size_Input(3);
            ps = prod(poolSize);
            %a. create initial indices of sample(1), feature(1)
            a = reshape(1:Rp*Cp,Rp,Cp);
            a = kron(a,ones(poolSize));
            %b. vectorize a into matrix_single_image
            msi = reshape(a, Rp*Cp*ps,1);
            %c. create a populating vector for multiple images
            pvm = 0:max(msi):max(msi)*(countSamples-1);
            %f. apply the multiple image population
            mso = bsxfun(@plus, msi, pvm);
            %g. map the input as output using mso
            X_upsampled = X_pooled(mso);
            %h. also create the reshaped version if needed
            %Xo_w = reshape(Xo,Ro,Co,countSamples);
            if nargout>1
                learntParams.pooled_size_original = [Rp Cp countSamples];
                learntParams.pooled_size_vectorized = [Rp*Cp countSamples];
                learntParams.upsampled_size_original = [Rp*poolSize(1) Cp*poolSize(2) countSamples];
                learntParams.upsampled_size_vectorized = [Rp*poolSize(1)*Cp*poolSize(2) countSamples];
            end
        case 4%[Rp Cp F countSamples] <EXPLANATORY CODING HERE>
            Rp = size_Input(1);
            Cp = size_Input(2);
            F = size_Input(3);
            countSamples = size_Input(4);
            
            ps = prod(poolSize);
            %Ru = Rp * poolSize(1);
            %Cu = Cp * poolSize(2);
            %Xu = zeros(Ru,Cu,F,countSamples);
            
            %a. create initial indices of sample(1), feature(1)
            a = reshape(1:Rp*Cp,Rp,Cp);
            a = kron(a,ones(poolSize));
            %b. vectorize a
            a = reshape(a, Rp*Cp*ps,1);
            %c. create a populating vector for a single image
            pvs = 0:Rp*Cp:Rp*Cp*(F-1);
            %d. apply the single image population
            msi = bsxfun(@plus, a, pvs);
            %d. vectorize msi
            msi= reshape(msi, [],1);
            %e. create a populating vector for multiple images
            pvm = 0:max(msi):max(msi)*(countSamples-1);
            %f. apply the multiple image population
            mso = bsxfun(@plus, msi, pvm);
            %g. map the input as output using mso
            X_upsampled = X_pooled(mso);
            %h. also create the reshaped version if needed
            %Xo_w = reshape(Xu,Ru,Cu,F,countSamples);
            if nargout>1
                learntParams.pooled_size_original = [Rp Cp F countSamples];
                learntParams.pooled_size_vectorized = [Rp*Cp*F countSamples];
                learntParams.upsampled_size_original = [Rp*poolSize(1) Cp*poolSize(2) F countSamples];
                learntParams.upsampled_size_vectorized = [Rp*poolSize(1)*Cp*poolSize(2)*F countSamples];
            end
        otherwise
            error('unknown structure to upsample');
    end

end

