function [ deconvSet, activationValueIndicePair, mostActivFeatMaps] = visualizeCNNActivationResults( X, Y, cnnnet, displayOpts, saveOpts, probabilitiesOut, vararginStruct)
    %X is data with size [dimension by countSamples]
    %Y is labels with size [1 by countSamples]
    %X_out, label_inds,h] = plottableConvolvedFigure_MultipleSample(deconvSet(:,layer+1), cnnnet{layer+1,1}.inputSize_original,layer,['Most activated output samples of layer_{(' cnnnet{layer,1}.name ')}']);
    [ deconvSet, activationValueIndicePair, mostActivFeatMaps] = createDeconvolutionSet( X, Y, cnnnet, displayOpts, saveOpts, probabilitiesOut);
    batchID = 1;
    if exist('vararginStruct','var') && isstruct(vararginStruct)
        if isfield(vararginStruct,'batchID')
            batchID = vararginStruct.batchID;
        end
    end
    layerCount = size(cnnnet,1);
    for layer = 1:layerCount
        %for plotting this layer must be either convolution, activation or pooling
        %on the other hand we need to skip if NOT
        %this layer is a convolution or a pooling layer
        %or 
        poolOrConv_cur = strcmp(cnnnet{layer,1}.type,'convolution') || strcmp(cnnnet{layer,1}.type,'pooling');
        poolOrConv_prev = layer>1 && (strcmp(cnnnet{layer-1,1}.type,'convolution') || strcmp(cnnnet{layer-1,1}.type,'pooling'));
        if ~( poolOrConv_cur || (poolOrConv_prev && strcmp(cnnnet{layer,1}.type,'activation')))
            continue;
        end
        switch cnnnet{layer,1}.type
            case 'convolution'
                [~, ~, h] = plottableConvolvedFigure_MultipleSample(deconvSet(:,layer+1), cnnnet{layer+1,1}.inputSize_original,layer,['Most activated output samples of layer_{(' cnnnet{layer,1}.name ')}']);
            case 'activation'
                [~, ~, h] = plottableConvolvedFigure_MultipleSample(deconvSet(:,layer+1), cnnnet{layer+1,1}.inputSize_original,layer,['Most activated output samples of layer_{(' cnnnet{layer,1}.name ')}']);
            case 'pooling'
                [~, ~, h] = plottableConvolvedFigure_MultipleSample(deconvSet(:,layer+1), cnnnet{layer+1,1}.inputSize_original,layer,['Most activated output samples of layer_{(' cnnnet{layer,1}.name ')}']);
            case 'softmax'
            case 'fullyconnected'
            otherwise
                error('You are suggesting a layer that is not present.Layers are - convolution, activation, pooling, softmax');
        end
        saveFolder = [saveOpts.MainFolder 'CNN_Visualizations' filesep];
        if ~exist(saveFolder,'dir')
            if (displayOpts.fileOperations)
                disp([displayOpts.ies{4,1} 'Folder(' saveFolder ') is created to save activation results']);
            end
            mkdir(saveFolder);
        end
        saveas(h,[saveFolder 'L(' num2str(layer,'%02d') ')_Most activated output samples of layer_{(' cnnnet{layer,1}.name ')}_BatchID' num2str(batchID,'%02d') '.png']);
    end
end

