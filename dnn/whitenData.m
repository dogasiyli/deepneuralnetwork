function [ meanData, ZCA_p, data ] = whitenData( data, ZCA_epsilon )
    if ~exist('ZCA_epsilon','var')
        ZCA_epsilon = 1e-5;
    end
    countFeat = size(data,1);
    meanData = mean(data, 2);
    data = bsxfun(@minus,data,meanData);
    [U,S,~] = svd(data * data' / countFeat);
    ZCA_p = U * diag(1./sqrt(diag(S) + ZCA_epsilon)) * U' ;
    data = ZCA_p * data;
end

