function [clusterToHandShapeMap, filteredLabels, distToKnownHandShape, toCheckMat] = getClusterToHandshapesMap(distMat, detailedLabels_ColCentroids, distAllowedPerMedoid, sampleIndsVec, sortedClusterLabels)
    k_uniqCount = length(unique(sortedClusterLabels));
    uniqHandShapes = unique(detailedLabels_ColCentroids(:,end));
    clusterToHandShapeMap = zeros(k_uniqCount,1);
    unmappedLabelVec = zeros(size(sortedClusterLabels));
    mappedLabelVec = zeros(size(sortedClusterLabels));
    distToKnownHandShape = zeros(2, length(sortedClusterLabels));
    discardedImMat = zeros(k_uniqCount,max(uniqHandShapes));
    for j = 1:k_uniqCount %cols   
        allImIDs_curCluster = sampleIndsVec(ismember(sortedClusterLabels,j));
        clusterSampleCount = length(allImIDs_curCluster);
        [minDistToClosest, selectedClusterOfAll] = min(distMat(:,allImIDs_curCluster));
        assignableLabelsOfAll = detailedLabels_ColCentroids(selectedClusterOfAll,end);%per closest center selection of sign
        
        assignableLabelsUniq = unique(assignableLabelsOfAll);
        if length(assignableLabelsUniq)==1
            selectedHandShapeID = assignableLabelsUniq;
        else
            [sortedDist, sortedDistIdx] = sort(minDistToClosest);
            accumulatedHandshapePowerResult = accumarray(assignableLabelsOfAll(sortedDistIdx),linspace(1,0,clusterSampleCount)');
            [sortedAccum, idxAccum] = sort(accumulatedHandshapePowerResult,'descend');
            selectedHandShapeID = idxAccum(1);
        end
        
        selectedHandShapeRows = detailedLabels_ColCentroids(:,end)==selectedHandShapeID;
        distsAllowedPart = distAllowedPerMedoid(selectedHandShapeRows);
        allowedDistToKnownHandShape = max(distsAllowedPart);%distsAllowedPart(rowIDx);
        for imID = allImIDs_curCluster
            %after selecting the selectedHandShapeID
            %I need to get the min distance of each sample to the known selectedHandShapeID
            %from the known handshapes inventory
            rowsOfDistMat = distMat(selectedHandShapeRows,imID);
            [distToSet, rowIDx] = min(rowsOfDistMat);
            if distToSet < allowedDistToKnownHandShape
                distToKnownHandShape(:,imID) = [distToSet;0];
                mappedLabelVec(imID) = selectedHandShapeID;
                unmappedLabelVec(imID) = j;     
            else
                discardedImMat(j,selectedHandShapeID) = discardedImMat(j,selectedHandShapeID) + 1;
                distToKnownHandShape(:,imID) = [0;distToSet];
            end            
        end
        clusterToHandShapeMap(j) = selectedHandShapeID;
    end
    [filteredLabels] = filterLabelsByLength(mappedLabelVec, 3);
    toCheckMat = [filteredLabels' mappedLabelVec' unmappedLabelVec'];
end