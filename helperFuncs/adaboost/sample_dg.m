function relevantFeats = sample_dg(dataAll,labelsAll,maxIteration)
%
%
%
  [W_3D, W, infoGain, relevantFeats, accPerMethod, bestMethodID_not1, methodIDPref, confMatAll]= ldaDG(dataAll, labelsAll);
  dataAll = dataAll(:,relevantFeats);
  

  blue = dataAll(labelsAll==1,:);
  red = dataAll(labelsAll==2,:);
  dataclass = labelsAll';
  dataclass(dataclass==1) = -1;
  dataclass(dataclass==2) = +1;
  
  datafeatures=[blue;red];
  

 % Show the data
  figure, subplot(2,2,1), hold on; axis equal;
  plot(blue(:,1),blue(:,2),'b.'); plot(red(:,1),red(:,2),'r.');
  title('Training Data');
  
 % Use Adaboost to make a classifier
  [classestimate,model]=adaboost('train',datafeatures,dataclass,maxIteration);

 % Training results
 % Show results
  blue=datafeatures(classestimate==-1,:);
  red=datafeatures(classestimate==1,:);
  I=zeros(161,161);
  for i=1:length(model)
      if(model(i).dimension==1)
          if(model(i).direction==1), rec=[-80 -80 80+model(i).threshold 160];
          else rec=[model(i).threshold -80 80-model(i).threshold 160 ];
          end
      else
          if(model(i).direction==1), rec=[-80 -80 160 80+model(i).threshold];
          else rec=[-80 model(i).threshold 160 80-model(i).threshold];
          end
      end
      rec=round(rec);
      y=rec(1)+81:rec(1)+81+rec(3); x=rec(2)+81:rec(2)+81+rec(4);
      I=I-model(i).alpha; I(x,y)=I(x,y)+2*model(i).alpha;    
  end
 subplot(2,2,2), imshow(I,[]); colorbar; axis xy;
 colormap('jet'), hold on
 plot(blue(:,1)+81,blue(:,2)+81,'bo');
 plot(red(:,1)+81,red(:,2)+81,'ro');
 title('Training Data classified with adaboost model');

 % Show the error verus number of weak classifiers
 error=zeros(1,length(model)); for i=1:length(model), error(i)=model(i).error; end 
 subplot(2,2,3), plot(error); title('Classification error versus number of weak classifiers');

 % Classify the testdata with the trained model
  testclass = adaboost('apply',datafeatures,model);

 % Show result
  blue=datafeatures(testclass==-1,:); 
  red=datafeatures(testclass==1,:);
  confMatResult = confusionmat(dataclass,testclass);

 % Show the data
  subplot(2,2,4), hold on
  plot(blue(:,1),blue(:,2),'b*');
  plot(red(:,1),red(:,2),'r*');
  axis equal;
  title('Test Data classified with adaboost model');
end