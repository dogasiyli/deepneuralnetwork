function [signTable, numOfFrames_Table, numOfVideos_Table, numOfUsers_Table] = analayzeListOfFiles( fullFileName, srcTable, signTable)
%analayzeListOfFiles analyzes the list of files that is extracted by loop_2CreateNeuralNetFolders
%   The txt file has lines as
%   FOLDROOT\<nameOfHandShape>\s049_u02_r01_LH_034.png ** <nameOfHandShape>   ** <idOfHandShape>
%   this list doesnt have 'singleStill' and 'bothStill'

    numOfFrames_Table = NaN;
    numOfVideos_Table = NaN;
    numOfUsers_Table = NaN;
    
    if exist('srcTable','var') && exist('signTable','var') && nargin>2
        assert(size(srcTable,2)==6,'srcTable must have 6 columns');
        numOfImages = size(srcTable,1);
        uniqueHandShapeIDs = unique(srcTable(:,6));
        assert(size(uniqueHandShapeIDs,1)==size(signTable,1),'uniqueHandShapeIDs count must be equal to signTable row count');
        cnt_handShapes = length(uniqueHandShapeIDs);
    else
        try
            fid = fopen(fullFileName);
            lineCells = textscan(fid,'%s %s %s  %s %d','Delimiter','**','TreatAsEmpty','~');
            fclose(fid);
        catch err
            disp(['Problem while reading data from <' fullFileName '>']);
            disp(['fid(' num2str(fid) '), err(' err.message ')'])
            return
        end
        lineCells = lineCells(:,[1 3 5]);
        numOfImages = size(lineCells{1},1);    
        [uniqueHandShapeIDs,uniqueSignRowIDs] = unique(lineCells{1,3});
        uniqueSignNames = lineCells{1,2}(uniqueSignRowIDs(:));
        cnt_handShapes = length(uniqueHandShapeIDs);

        %signID,signName -> 2 cols, how many rows?
        signTable = cell(cnt_handShapes, 2);
        for i=1:cnt_handShapes
            signTable{i,1} = num2str(uniqueHandShapeIDs(i));
            signTable{i,2} = strtrim(uniqueSignNames{i});
        end    

        srcTable = zeros(numOfImages,6);
        %signID,userID,repeatID,handID,frameID,handShapeID
        for i=1:numOfImages
            curLine = lineCells{1}{i};
            handShapeID = lineCells{3}(i);
            delimiteredLine = strsplit(curLine,{'\','/'});
            fileName = delimiteredLine{end};
            frameInfo = strsplit(fileName,{'_','.'});
            signID = str2double(frameInfo{1}(2:end));
            userID = str2double(frameInfo{2}(2:end));
            reptID = str2double(frameInfo{3}(2:end));
            handInt = handStrToInt(frameInfo{4});
            frameID = str2double(frameInfo{5}(2:end));
            %                  1       2        3       4        5          6
            srcTable(i,:) = [signID, userID, reptID, handInt, frameID, handShapeID];
        end
    end
    
    uniqueSignIDs = unique(srcTable(:,1));
    cnt_signIDs = length(uniqueSignIDs);
    
    [numOfFrames_Mat, numOfFrames_Table] = prepare_numOfFrames(signTable, srcTable, uniqueSignIDs, uniqueHandShapeIDs);
    assert(sum(sum(numOfFrames_Mat(2:end,2:end)))==numOfImages,'sum of table must be equal to numOfImages')
    
    [numOfVideos_Mat, numOfVideos_Table, missingVideosTable] = prepare_numOfVideos(signTable, srcTable, uniqueSignIDs, uniqueHandShapeIDs);
    
end

function handInt = handStrToInt(handStr)
    switch(handStr)
        case 'LH'
            handInt = 1;
        case 'RH'
            handInt = 2;
        case 'BH'
            handInt = 3;
        otherwise
            error(['unrecognized handStr(' handStr ')'])
    end
end

function [numOfFrames_Mat, numOfFrames_Table] = prepare_numOfFrames(signTable, srcTable, uniqueSignIDs, uniqueHandShapeIDs)
    cnt_signIDs = length(uniqueSignIDs);
    cnt_handShapes = length(uniqueHandShapeIDs);
    %perSıgn, I need to have number of frames that matches handShape
    numOfFrames_Mat = zeros(cnt_signIDs+1,cnt_handShapes+1);
    numOfFrames_Table = cell(cnt_signIDs+2,cnt_handShapes+1);
    numOfFrames_Table{2,1} = 'signID';
    for r = 2:cnt_signIDs+1
        signID = uniqueSignIDs(r-1);
        numOfFrames_Mat(r,1) = signID;
        numOfFrames_Table{r+1,1} = num2str(signID);
        for c = 2:cnt_handShapes+1
            handShapeID = uniqueHandShapeIDs(c-1);
            numOfFrames_Mat(1,c) = handShapeID;
            numOfFrames_Table{1,c} = signTable{handShapeID,1};
            numOfFrames_Table{2,c} = signTable{handShapeID,2};
        end
        for c = 2:cnt_handShapes+1
            handShapeID = uniqueHandShapeIDs(c-1);
            %now I need the count of hand shapeIDs
            %which belongs to <signID>
            selectedRows = srcTable(:,1)==signID & srcTable(:,6)==handShapeID;
            cnt_frames = sum(selectedRows==1);
            numOfFrames_Mat(r,c) = cnt_frames;
            numOfFrames_Table{r+1,c} = num2str(cnt_frames);
        end
    end
end

function [numOfVideos_Mat, numOfVideos_Table, missingVideosTable] = prepare_numOfVideos(signTable, srcTable, uniqueSignIDs, uniqueHandShapeIDs)
    cnt_signIDs = length(uniqueSignIDs);
    cnt_handShapes = length(uniqueHandShapeIDs);
    %perSıgn, I need to have number of frames that matches handShape
    numOfVideos_Mat = zeros(cnt_signIDs+1,cnt_handShapes+2);
    numOfVideos_Table = cell(cnt_signIDs+2,cnt_handShapes+2);
    numOfVideos_Table{2,1} = 'signID';
    numOfVideos_Table{2,2} = 'videoCount';
    missingVideosTable = zeros(0,4);%signID,handShapeID,userID,repID
    for c = 3:cnt_handShapes+2
        handShapeID = uniqueHandShapeIDs(c-2);
        numOfVideos_Mat(1,c) = handShapeID;
        numOfVideos_Table{1,c} = signTable{handShapeID,1};
        numOfVideos_Table{2,c} = signTable{handShapeID,2};
    end
    
    for r = 2:cnt_signIDs+1
        signID = uniqueSignIDs(r-1);        
        selectedRows = srcTable(:,1)==signID;
        srcTable_sub = srcTable(selectedRows,:);
        userRepCol = srcTable_sub(:,2)*100 + srcTable_sub(:,3);
        uniqUserRep_Vec = unique(userRepCol);
        
        numOfVideos_Mat(r,1) = signID;
        numOfVideos_Mat(r,2) = length(uniqUserRep_Vec);
        numOfVideos_Table{r+1,1} = num2str( numOfVideos_Mat(r,1));
        numOfVideos_Table{r+1,2} = num2str(numOfVideos_Mat(r,2));
        
        for c = 3:cnt_handShapes+2
            handShapeID = uniqueHandShapeIDs(c-2);
            %now I need the count of hand shapeIDs
            %which belongs to <signID>
            selectedRows = srcTable_sub(:,6)==handShapeID;
            uniqUserRep_Vec_sub = unique(userRepCol(selectedRows));
            cnt_Videos = length(uniqUserRep_Vec_sub);
            numOfVideos_Mat(r,c) = cnt_Videos;
            numOfVideos_Table{r+1,c} = num2str(numOfVideos_Mat(r,c));
            if cnt_Videos<numOfVideos_Mat(r,2) && cnt_Videos>0
                %which videos are missing
                M = prod(reshape(uniqUserRep_Vec,[],1) - reshape(uniqUserRep_Vec_sub,1,[]),2);
                %existingVideoIDs = uniqUserRep_Vec(M==0);
                nonExistingVideoIDs = uniqUserRep_Vec(M~=0);
                for i = 1:length(nonExistingVideoIDs)
                    mv = nonExistingVideoIDs(i);
                    repID = mod(mv,100);
                    mv = mv - repID;
                    userID = mv/100;
                    missingVideosTable = [missingVideosTable;signID,handShapeID,userID,repID]; %#ok<AGROW>
                end
            end
        end
    end
end