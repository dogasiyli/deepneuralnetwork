function [ shuffleResultAcc ] = analyzeAccAfterShuffle( dMN )

    batchCount = max(dMN(:,1));%from new batchID column
    shuffleResultAcc = zeros(batchCount+1,3);
    for b = 1:batchCount
       tsc_o =  sum(dMN(:,3)==b & dMN(:,5)==dMN(:,6));
       fsc_o =  sum(dMN(:,3)==b & dMN(:,5)~=dMN(:,6));
       acc_o =  tsc_o/(tsc_o+fsc_o);

       tsc_n =  sum(dMN(:,1)==b & dMN(:,5)==dMN(:,6));
       fsc_n =  sum(dMN(:,1)==b & dMN(:,5)~=dMN(:,6));
       acc_n = tsc_n/(tsc_n+fsc_n);
       
       shuffleResultAcc(b,:) = [b acc_o acc_n];
    end
    
    shuffleResultAcc(batchCount+1,2) = mean(shuffleResultAcc(1:batchCount,2));
    shuffleResultAcc(batchCount+1,3) = mean(shuffleResultAcc(1:batchCount,3));
    
    %disp('Shuffle result accuracies : ');
    %disptable(shuffleResultAcc);
end

