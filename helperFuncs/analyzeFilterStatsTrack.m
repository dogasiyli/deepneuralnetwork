function [ filterBehaviourCodeMat_StrCell, filterBehaviourCodeMat_Summary, insertModes, missingAnalysis] = analyzeFilterStatsTrack(M, batchCounts)
    figID = 1;
% We will analyze the evolution of each filter statistics
% by fixing the batchID, 

    [numOfRowsTotal, numOfCols] = size(M);
    assert(numOfCols==9,'this must be 9')
    %Col names :1_epochID,2_batchType,3_batchID,4_insertMode,5_filterID,6_min,7_mean,8_max,9_variance
    insertModes = unique(M(:,4))';%before cropping see the different insertModes used in training
    batchIDs = unique(M(:,3))';
    batchTypes = unique(M(:,2))';%1_Train,2_Validation,3_Test
    %remove train from batchTypes
    batchTypes(batchTypes==1) = [];
    batchTypeCnt = length(batchTypes);
    
    %1. Ignore epoch-0
    M = M(M(:,1)~=0,:);
    %2. Ignore any insertModes other than 1
    M = M(M(:,4)==1,:);
        
    filterIDs = unique(M(:,5))';
    
    filterBehaviours = {'alwaysIncreasing';'seldomDecrease';'peaky';'constantDrop';'vanishing'};
    filterBehaviourCodeMat_perBatch = zeros(length(filterIDs),length(batchIDs));
    filterBehaviourCodeMat_perBatchType = zeros(length(filterIDs),batchTypeCnt);
    filterBehaviourCodeMat_Summary = zeros(length(filterIDs),5);
    missingAnalysis = [];
    colorOfFilters = rand(length(filterIDs),3);
    if ~isempty(figID)
        h=figure(figID);clf;hold on;
    end
    filterBehaviourFoundStr = filterBehaviours;
    for f = filterIDs
        %crop specified filterID from M
        M_f = M(M(:,5)==f,:);
        %now M has the information for
        %all batchIDs
        %all batchTypes
        
        %for a correct analysis I would fix the batchID and type
        filterBehaviourCodeMat = zeros(length(batchIDs),batchTypeCnt);
        filterBehaviour_firstDrop = zeros(length(batchIDs),batchTypeCnt);
        filterBehaviour_constantDrop = zeros(length(batchIDs),batchTypeCnt);
        filterBehaviour_belowZero = zeros(length(batchIDs),batchTypeCnt);
        for b=batchIDs
            M_f_b = M_f(M_f(:,3)==b,:);%crop specified batchID
            for btInd=1:batchTypeCnt
                bt = batchTypes(btInd);
                %so here we can analyze
                %filter(f)'s evolution for batch(b) of batchType(bt)
                
                %I expect a consistent behaviour 
                %across batchTypes when batchID is fixed - for batchID(b) this filter is filterBehaviours{i}
                %across batchIDs when batchType is fixed - for batchType(bt) this filter is filterBehaviours{i}
                M_f_b_bt = M_f_b(M_f_b(:,2)==bt,:);%crop specified batchType
                if ~isempty(M_f_b_bt)
                    [fbID, firstDrop, constantDrop, belowZero] = findFilterBehaviourID(M_f_b_bt);
                else
                    fbID=NaN;
                    firstDrop=NaN;
                    constantDrop=NaN;
                    belowZero=NaN;
                    if batchCounts(bt)>=b
                        missingAnalysis = [missingAnalysis;f b bt];
                    end
                end
                filterBehaviourCodeMat_perBatch(f,b) = fbID;
                filterBehaviourCodeMat_perBatchType(f,btInd) = fbID;
                filterBehaviourCodeMat(b,btInd) = fbID;
                filterBehaviour_firstDrop(b,btInd) = firstDrop;
                filterBehaviour_constantDrop(b,btInd) = constantDrop;
                filterBehaviour_belowZero(b,btInd) = belowZero;
            end
        end
        %filterBehaviourCodeMat must have all same value
        filterBehaviourCodeCur = getMostOccuredVal(filterBehaviourCodeMat);
        if strcmp(filterBehaviours{filterBehaviourCodeCur,1},filterBehaviourFoundStr{filterBehaviourCodeCur,1})
            filterBehaviourFoundStr{filterBehaviourCodeCur,1} = [filterBehaviours{filterBehaviourCodeCur,1} '-(' num2str(f) ')'];
        else
            filterBehaviourFoundStr{filterBehaviourCodeCur,1} = [filterBehaviourFoundStr{filterBehaviourCodeCur,1}(1:end-1) '-' num2str(f) ')'];
        end
        firstDrop_cur = getMostOccuredVal(filterBehaviour_firstDrop);
        constantDrop_cur = getMostOccuredVal(filterBehaviour_constantDrop);
        belowZero_cur = getMostOccuredVal(filterBehaviour_belowZero);
        filterBehaviourCodeVec = [f filterBehaviourCodeCur firstDrop_cur constantDrop_cur belowZero_cur];
        filterBehaviourCodeMat_Summary(f,:) = filterBehaviourCodeVec;
        %figure plot stuff
        if ~isempty(figID)
            for b=batchIDs
                M_f_b = M_f(M_f(:,3)==b,:);%crop specified batchID
                for btInd=1:batchTypeCnt
                    bt = batchTypes(btInd);
                    M_f_b_bt = M_f_b(M_f_b(:,2)==bt,:);%crop specified batchType
                    if ~isempty(M_f_b_bt)
                        fbID = filterBehaviourCodeMat(b,btInd);
                        firstDrop = filterBehaviour_firstDrop(b,btInd);
                        constantDrop = filterBehaviour_constantDrop(b,btInd);
                        belowZero = filterBehaviour_belowZero(b,btInd);    
                        filterBehaviourCurMat = [0 fbID firstDrop constantDrop belowZero];
                        plotFilterStatsTrackAnalyse(filterBehaviourCodeVec, M_f_b_bt, filterBehaviourCurMat, figID, colorOfFilters, filterBehaviourFoundStr{filterBehaviourCodeCur,1});
                    end
                end
            end         
        end
    end
    filterBehaviourCodeMat_StrCell = filterBehaviours(filterBehaviourCodeMat_Summary(:,2));
end

function mostOccuredVal = getMostOccuredVal(filterBehaviourCodeMat)
    nnv=~isnan(filterBehaviourCodeMat);
    filterBehaviour_UniqueCodes = unique(filterBehaviourCodeMat(nnv))';
    if length(filterBehaviour_UniqueCodes)==1
        mostOccuredVal = filterBehaviour_UniqueCodes;
    else
        filterBehaviour_Hist = hist(filterBehaviourCodeMat(nnv),filterBehaviour_UniqueCodes);
        [~,slct] = max(filterBehaviour_Hist);
        mostOccuredVal = filterBehaviour_UniqueCodes(slct);
    end
    if isempty(mostOccuredVal)
        mostOccuredVal = NaN;
    end
end

function [fbID, firstDrop, constantDrop, belowZero] = findFilterBehaviourID(M_b_bt)
    batchID = unique(M_b_bt(:,3));
    batchType = unique(M_b_bt(:,2));
    assert(length(batchID)==1,'wrong number of batchID. Must be 1')
    assert(length(batchType)==1,'wrong number of batchType. Must be 1')
    
    %sort according to epochID(col=1)
    M_b_bt = sortrows(M_b_bt,1);
    %remove additional 1's at the beginning
    initRow = find(M_b_bt(:,1)==1,1,'last');
    M_b_bt = M_b_bt(initRow:end,:);
    
    maxCol = M_b_bt(:,8);
    movingDif = [0;maxCol(2:end)-maxCol(1:end-1)];
    %now check if max(col=8) is ALWAYS INCREASING
    if sum(movingDif<0)==0%'alwaysIncreasing' -1-
        fbID = 1;
        
        firstDrop = findFirstDrop(movingDif);
        assert(isnan(firstDrop),'there shouldnt be any negative values in movingdif');
        
        constantDrop = findConstantDrop(maxCol, movingDif, 1);
        assert(isnan(constantDrop),'there shouldnt be constantDrop in alwaysIncreasing');
        
        belowZero = findBelowZero(maxCol);
        assert(isnan(belowZero),'there shouldnt be any negative values in maxCol');
        return
    %else there is a drop somewhere
    else
        firstDrop = findFirstDrop(movingDif);
        assert(~isnan(firstDrop),'there should be at least one negative value in movingdif');
        belowZero = findBelowZero(maxCol);
        
        %'vanishing' -5- if final max value is below zero
        if ~isnan(belowZero)
            fbID = 5;
            
            constantDrop = findConstantDrop(maxCol, movingDif, 1);
            assert(~isnan(constantDrop),'there should be at least 1 drop from positive to negative');
            
            if sum(maxCol(belowZero:end)>0)>0
                warning('there shouldnt be any negative value in maxCol after belowZeroPoint.paused');
                pause;
            end
            return
        end
        
        %'constantDrop' -4- if there is a constant drop at the end (at least for 3 values)
        constantDrop = findConstantDrop(maxCol, movingDif, 3);
        if ~isnan(constantDrop)
            fbID = 4;
            return
        end
        
        dropEpochPercent = sum(movingDif<0)/length(movingDif);
        %'seldomDecrease' -2- if no constant drop and less than %20 is dropping
        if dropEpochPercent<0.25
            fbID = 2;
        %'peaky' -3- the only option left - a lot of drops scattered
        else
            fbID = 3;
        end
    end
end

function firstDrop = findFirstDrop(movingdif)
    firstDrop = find(movingdif<0,1,'first');
    if isempty(firstDrop)
        firstDrop = NaN;
    end
end

function belowZero = findBelowZero(maxCol)
    belowZero = find(maxCol<0,1,'first');
    if isempty(belowZero)
        belowZero = NaN;
    end
end

function constantDrop = findConstantDrop(maxCol, movingDif, treshEpochCount)
    movingDif(maxCol<0) = min(movingDif)-1;%if it is already below zero increase is misleading
    
    increasEffectVec = linspace(1,10,length(movingDif));
    increasEffectVec = increasEffectVec./sum(increasEffectVec);
    meanMove = increasEffectVec*movingDif;
    
    lastIncreaseID = find(movingDif>0,1,'last');
    if isempty(lastIncreaseID)
        lastIncreaseID = 1;
    end
    lastDecreaseID = find(movingDif<=meanMove,1,'last');
    if isempty(lastDecreaseID)
        lastDecreaseID = length(movingDif);
    end
    if lastDecreaseID-lastIncreaseID>treshEpochCount || meanMove<0
        constantDrop = lastIncreaseID+1;
    else
        constantDrop = NaN;
    end
end


