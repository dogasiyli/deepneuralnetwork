function [ labelSwitches_uniq, labelSwitches_alll ] = analyzeLabelsOfSign(signID, optParamStruct)
    %optParamStruct = struct('saveScreenOutputFolder', 'srcFold');
    %[ labelSwitches_uniq, labelSwitches_alll ] = analyzeLabelsOfSign(signID);
    %[ labelSwitches_uniq, labelSwitches_alll ] = analyzeLabelsOfSign(signID, struct('saveScreenOutputFolder', 'srcFold'));
    %optParamStruct = struct('srcFold',[],'userList',2:7,'maxRptCnt', 20)
    %optParamStruct = struct('saveScreenOutputFolder','srcFold')
    %optParamStruct_analyzeLabelsOfSign = struct('saveScreenOutputFolder','srcFold','removePreviousSignSummaryFile',true)
    %optParamStruct_analyzeLabelsOfSign = struct('srcFold','D:\toWEB','saveScreenOutputFolder','srcFold','removePreviousSignSummaryFile',true)
    if ~exist('optParamStruct','var')
        optParamStruct=[];
    end
    srcFold = getStructField(optParamStruct, 'srcFold', getVariableByComputerName('srcFold'));
    userList = getStructField(optParamStruct, 'userList', 2:7);
    maxRptCnt = getStructField(optParamStruct, 'maxRptCnt', 20);
    saveScreenOutputFolder = getStructField(optParamStruct, 'saveScreenOutputFolder', srcFold);
    removePreviousSignSummaryFile = getStructField(optParamStruct, 'removePreviousSignSummaryFile', false);
    
    [surList, listElemntCnt] = createSURList(srcFold, 2, struct('signIDList',signID,'userID',userList,'maxRptCnt',maxRptCnt));
    [possibleSequenceFlows, labelSummary, problematicVids] = summarizeLabels(signID, struct('srcFold',srcFold,'userList',userList,'maxRptCnt', maxRptCnt));
    
    labelSwitches_alll = cell(3,listElemntCnt);
    labelSwitches_uniq = cell(3,4);
    
    for h=1:3
        %h=1-lh,h=2-rh,h=3-bh
        for r = 1:listElemntCnt
            switch h
                case 1
                    labelSwitches_alll{h,r} = strcat(char(possibleSequenceFlows.LH{r}(:,1) + 'a' - 1)');
                case 2
                    labelSwitches_alll{h,r} = strcat(char(possibleSequenceFlows.RH{r}(:,1) + 'a' - 1)');
                case 3
                    labelSwitches_alll{h,r} = strcat(char(possibleSequenceFlows.BH{r}(:,1) + 'a' - 1)');
            end
        end
        [labelSwitches_uniq{h,1},labelSwitches_uniq{h,2},labelSwitches_uniq{h,3}]  = unique(labelSwitches_alll(h,:));
        labelSwitches_uniq{h,4} = hist(labelSwitches_uniq{h,3},unique(labelSwitches_uniq{h,3}));
    end
    
    %now summarize what has happened
    
    if ~strcmp(saveScreenOutputFolder,'')
        toFolder = [saveScreenOutputFolder filesep num2str(signID)];
        if ~exist(toFolder,'dir')
            mkdir(toFolder);
        end
        
        diaryFileName = [toFolder filesep 'signLabelsSummary_' num2str(signID,'%03d') '.txt'];
        if (removePreviousSignSummaryFile && exist(diaryFileName,'file')==2)
            delete(diaryFileName);
        end
        
        diary(diaryFileName);
        tryNewLinePrint(2);
        disp('xoxoxoxoxoxoxoxoxoxox');
        disp(datestr(now))
    end
    
    disp(['sign(' num2str(signID) ') - labelling analyze summary']);
    if ~isempty(problematicVids)
        warning('There are some problematic videos');
        disp(problematicVids);
    end

    printSequenceSummaries(surList, labelSwitches_uniq);
    
    disp('----LabelCountSummary----');
    [listSummaryTotal, listSummarySpecial, listByUser, listByRepitition] = getLabelSummaryTable(labelSummary);
    disp('-------------------------');
    
    
    printClusterNameSummary(srcFold, signID );
    
    if ~strcmp(saveScreenOutputFolder,'')
        tryNewLinePrint(2);
        disp('xoxoxoxoxoxoxoxoxoxox');
        diary('off');
    end
    
    %at srcFold place save the following
    %signID, surList, labelSummary, labelSwitches_uniq, listSummaryTotal, listSummarySpecial, listByUser, listByRepitition
    try
        fileName = [srcFold filesep num2str(signID) filesep 'labelSummary.mat'];
        if exist(fileName, 'file'), saveBackupIfExist(fileName, true); end
        save(fileName, 'signID', 'surList', 'labelSummary', 'labelSwitches_uniq', 'listSummaryTotal', 'listSummarySpecial', 'listByUser', 'listByRepitition');
    catch err
        disp(['couldnt save labelSummary.mat - err(' err.message ')']);
        return
    end
    latex4Sign(signID, struct('srcFold',srcFold));
end

function tryNewLinePrint(times)
    try
        for i=1:times
            disp(newline)
        end
    catch
    end
end

