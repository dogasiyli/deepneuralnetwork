function [matchedColIds, matchedRowIds] = argmaxRowsToCol(confMat, dispLevel)
    %   max(mcI,[],2) will give
    %   max(mrI) - will give 
    if ~exist('dispLevel','var') || isempty(dispLevel)
        dispLevel= -1;
    end

    confMat_Disp = confMat;
    [rowCnt, colCnt] = size(confMat);
    
    %for this function we have to have rowCnt>=colCnt
    %or we can do it here by ourselves
    transposeFirst = (rowCnt<colCnt);
    if transposeFirst
        confMat = confMat';
        [rowCnt, colCnt] = size(confMat);        
    end
    
    
    %maximize sum of selected values 
    matchedColIds = zeros(rowCnt, colCnt);
    matchedRowIds = zeros(rowCnt, colCnt);
    remRowCnt = 1;
    while (remRowCnt>0)%(any(matchedColIds==0))
        [idxMat, maxVal] = findIn2DMat(confMat, max(confMat(:)));
        theRow = idxMat(1,2);
        theCol = idxMat(1,3);
        matchedColIds(theRow, theCol) = theCol;
        matchedRowIds(theRow, theCol) = theRow;
        confMat(theRow,:) = -inf;
        
        remRowCnt = sum(max(matchedColIds,[],2)==0);
        remColCnt = sum(max(matchedRowIds)==0);
        if (remRowCnt<=remColCnt)
           confMat(:,theCol) = -inf;            
        end
    end
    if transposeFirst
        matchedRowIds = matchedRowIds';
        matchedColIds = matchedColIds';
    end
    
    if dispLevel>0
        if transposeFirst
            disp(['The passed confusion matrix is transposed first. Bacause before --> rowCount(' num2str(colCnt) ')<colCount(' num2str(rowCnt) ')']);            
        end
        disp('Passed confMat:')
        disptable(confMat_Disp);            
        disp('Matching row indices:')
        disptable(matchedRowIds);
        disp('Matching col indices:')
        disptable(matchedColIds);
    end  
end