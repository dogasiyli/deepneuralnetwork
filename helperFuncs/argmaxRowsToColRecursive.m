function [matchedColIds, matchedRowIds, confMatRet] = argmaxRowsToColRecursive(confMat, dispLevel)
    %   max(mcI,[],2) will give
    %   max(mrI) - will give 
    if ~exist('dispLevel','var') || isempty(dispLevel)
        dispLevel= -1;
    end

    confMat_Disp = confMat;
    [rowCnt, colCnt] = size(confMat);
    
    %for this function we have to have rowCnt>=colCnt
    %or we can do it here by ourselves
    transposeFirst = (rowCnt<colCnt);
    if transposeFirst
        confMat = confMat';
        [rowCnt, colCnt] = size(confMat);        
    end
       
    %maximize sum of selected values 
    matchedColIds = zeros(rowCnt, colCnt);
    matchedRowIds = zeros(rowCnt, colCnt);
    matchedColIdsCell = cell(rowCnt, 1);
    matchedRowIdsCell = cell(1, colCnt);

    [idxMat, maxVal] = findIn2DMat(confMat, max(confMat(:)));
    theRow = idxMat(1,2);
    theCol = idxMat(1,3);
    matchedColIds(theRow, theCol) = theCol;
    matchedRowIds(theRow, theCol) = theRow;
    %matchedColIdsCell{theRow,1} = unique([matchedColIdsCell{theRow,1} theCol]);
    %matchedRowIdsCell{1,theCol} = unique([matchedRowIdsCell{1,theCol} theRow]);
    confMatRet = NaN(size(confMat));
    confMatRet(theRow,theCol) = confMat(theRow,theCol);
    confMat(theRow,:) = -inf;
    if size(confMat,1)>=2
        theRowIDs = 1:rowCnt;
        theRowIDs(theRow) = [];
        if rowCnt-1<=colCnt
            confMat(:,theCol) = -inf;
        end
        confMat_rec = confMat(theRowIDs,:);
        [matchedColIds_rec, matchedRowIds_rec, confMatRet_rec] = argmaxRowsToCol(confMat_rec, 1);
        %now the solution to the sub problem is in the above solution
        %we need to re-map the solution to our bigger problem and return
        %the answer
        matchedColIds(theRowIDs, :) = matchedColIds_rec;
        matchedRowIds(theRowIDs, :) = matchedRowIds_rec;
        confMatRet(theRowIDs, :) = confMatRet_rec;
%         idxZero = findIn2DMat(confMatRet, 0, []);
%         if ~isempty(idxZero)
%             %remove the zero valued elements
%             matchedColIds(idxZero(1,1)) = 0;
%             matchedRowIds(idxZero(1,1)) = 0;
%             confMatRet(idxZero(1,1)) = NaN;
%         end
    end

    if transposeFirst
        matchedRowIds = matchedRowIds';
        matchedColIds = matchedColIds';
        confMatRet = confMatRet';
    end
    
    %confMatRet can not have more than 1 value assigned at any row or col
    
    
    if dispLevel>0 && size(confMat,1)>1
        if transposeFirst
            disp(['The passed confusion matrix is transposed first. Bacause before --> rowCount(' num2str(colCnt) ')<colCount(' num2str(rowCnt) ')']);            
        end
        disp('Passed confMat:')
        disptable(confMat_Disp);            
        disp('Matching row indices:')
        disptable(matchedRowIds);
        disp('Matching col indices:')
        disptable(matchedColIds);
        disp('Current selected confMat:')
        disptable(confMatRet);
    end
%     if transposeFirst
%         matchedColIdsCell = matchedColIdsCell';
%         matchedRowIdsCell = matchedRowIdsCell';
%     end    
end