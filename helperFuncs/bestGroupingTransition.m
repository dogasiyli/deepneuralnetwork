function bestGroupingTransition(M)
    %M is k rows and G cols
    [K,G] = size(M);
    M_rowNorm = bsxfun(@rdivide,M,sum(M,2));
    M_colNorm = bsxfun(@rdivide,M,sum(M,1));
    
    X1 = max(M_rowNorm,M_colNorm);
    [maxVals,inds] = max(X1');
    kVec = 1:K;
    gVec = inds;
    groupingMat = zeros(K,G);
    groupingMat(sub2ind([K G],kVec, gVec)) = 1;
    disp(groupingMat);
end

