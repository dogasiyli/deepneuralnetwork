function [h] = boxPlotManuel(y, OPS)
%y = [-2 0.5 3.4 0.3; -1.4 0.8 3.1 0.2; 1 2 3.5 0.08; -2.5 -0.1 1.0 0.1; -1 0 1 0.08]
%[h] = boxPlotManuel(y, struct('figureID',3,'xlabelStr','filterIDs','titleStr','title'));

%OPS is Optional Parameter Struct
%OPS.lineLength
%OPS.figureID
%OPS.titleStr
%OPS.xlabelStr

%y is a matrix
%rows are categories
%cols are - 4 cols
%col1 : min
%col2 : mean
%col3 : max
%col4 : var
    if ~exist('OPS','var') || isempty(OPS)
        OPS = struct;%an empty struct
    end

    if isfield(OPS,'lineLength')
        ll = OPS.lineLength;
    else
        ll=0.25;
        %ll = 0.5;%touches to the nex horizantally
    end

    if isfield(OPS,'figureID')
        figureID = OPS.figureID;
    else
        figureID=[];
    end
    if ~isempty(figureID)
        h=figure(figureID);clf;
    else %it can already be called outside of matlab
        h = 0;
    end
    hold on;

    categoryCount = size(y,1);
    
    ytickVals = [min(y(:,1)) inf mean(y(:,2)) -inf max(y(:,3))];
    for c = 1:categoryCount
        %first calculate necessary values
        %uwv 'Upper Whisker'- max value y(c,3)
        %lwv 'Lower Whisker' - min value y(c,1)
        %mv 'Mean' - y(c,2)
        %ubv 'Upper Box Value'  - mv + 3*y(c,4)
        %lbv 'Lower Box Value' - mean value y(c,2) - 3*var 3*y(c,4)
        uwv = y(c,3);
        lwv = y(c,1);
        mv = y(c,2);
        stdVal = sqrt(y(c,4));
        ubv = mv + 3*stdVal;
        lbv = mv - 3*stdVal;
        %now plot the values
        plotCurrentCategory(c,ubv,lbv,mv,uwv,lwv,ll);
        
        ytickVals(2) = min(ytickVals(2),lbv);
        ytickVals(4) = max(ytickVals(4),ubv);
    end
    set(gca,'XTick',round(linspace(1,c,4)));
    ytickVals_gca = sort(ytickVals);
    if abs(ytickVals_gca(end)-ytickVals_gca(end-1))<abs(ytickVals_gca(3))
        ytickVals_gca = ytickVals_gca([1 2 3 5]);
    end
    if abs(ytickVals_gca(1)-ytickVals_gca(2))<abs(ytickVals_gca(3))
        ytickVals_gca = ytickVals_gca([1 3:end]);
    end
    set(gca,'YTick',ytickVals_gca);
    
    plot([0 c+1],[ytickVals(1) ytickVals(1)],'k');
    plot([0 c+1],[ytickVals(2) ytickVals(2)],'b');
    plot([0 c+1],[ytickVals(3) ytickVals(3)],'r');
    plot([0 c+1],[ytickVals(4) ytickVals(4)],'b');
    plot([0 c+1],[ytickVals(5) ytickVals(5)],'k');
    
    if isfield(OPS,'xlabelStr')
        xlabel(OPS.xlabelStr);
    end 
    if isfield(OPS,'titleStr')
        title(OPS.titleStr);
    end
    ylim([1.1*min(ytickVals) max(ytickVals)*1.1]);
    xlim([0.5 categoryCount+0.5]);
end

function plotCurrentCategory(x,ubv,lbv,mv,uwv,lwv,ll)
    %1. crossed edge lines
    plot([x-ll, x+ll],[uwv,uwv],'Color','k','Linestyle','-');%top tap
    plot([x-ll, x+ll],[lwv,lwv],'Color','k','Linestyle','-');%bottom tap
    plot([x, x],[lwv,uwv],'Color','k','Linestyle',':');%dotty edge
    
    %2. draw the rectangle
    rectangle('Position', [x-ll, lbv, 2*ll, ubv-lbv], 'LineStyle', '-', 'EdgeColor', 'b');
    
    %3. draw the red mean line
    plot([x-ll, x+ll],[mv, mv], 'Color', 'r');
end

