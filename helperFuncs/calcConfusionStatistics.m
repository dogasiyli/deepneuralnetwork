function [ confMatStats ] = calcConfusionStatistics(confMat, selectedCategories)
    %https://en.wikipedia.org/wiki/Confusion_matrix
    %confMat-rows are actual(true) classes
    %confMat-cols are predicted classes
    
    %for all categories a confusion matrix can be re-arranged per category,
    %for example for category "i";
    %----------  -----------------------------
    % TP | FN |  |      TP     | Type2 Error |
    % FP | TN |  | Type1 Error |      TN     |
    %----------  -----------------------------
    %--------------------------------------------------------------------------
    %       c_i is classified as ci         |  c_i are classified as non-ci   |
    % other classes falsly predicted as c_i | non-c_i is classified as non-ci |
    %--------------------------------------------------------------------------
    %TP : true positive - c_i is classified as ci
    %FN : false negative - c_i are classified as non-ci
    %FP : false positive - other classes falsly predicted as c_i
    %TN : true negative - non-c_i is classified as non-ci
    categoryCount = size(confMat,1);
    assert(categoryCount==size(confMat,2),'problem with confusion matrix');
    
    if exist('selectedCategories','var')
        selectedCategories(selectedCategories>categoryCount) = [];
    else
        selectedCategories = 1:categoryCount;
    end
    categoryCount = length(selectedCategories);    
    
    %disp('Columns of confusion mat is predictions, rows are ground truth.');
    
    confMatStats = struct([]);
    
    sampleCounts_All = sum(confMat,2);
    
    for i =1:categoryCount
        c = selectedCategories(i);
        totalPredictionOfCategory = sum(confMat(:,c));
        totalCountOfCategory = sampleCounts_All(c);
        totalCountOfAll = sum(confMat(:));
        TP = confMat(c,c);
        FN = sum(confMat(:,c))-TP;
        FP = sum(confMat(c,:))-TP;
        TN = totalCountOfAll-(TP+FN+FP);
        
        ACC = (TP+TN)/totalCountOfAll;%accuracy
        TPR = TP/(TP+FN);%true positive rate, sensitivity
        TNR = TN/(FP+TN);%true negative rate, specificity
        PPV = TP/(TP+FP);%positive predictive value, precision
        NPV = TN/(TN+FN);%negative predictive value
        FPR = FP/(FP+TN);%false positive rate, fall out
        FDR = FP/(FP+TP);%false discovery rate
        FNR = FN/(FN+TP);%false negative rate, miss rate
        
        F1  = (2*TP)/(2*TP+FP+FN);%harmonic mean of precision and sensitivity
        MCC = (TP*TN-FP*FN)/sqrt((TP+FP)*(TP+FN)*(TN+FP)*(TN+FN));%matthews correlation coefficient
        INFORMEDNESS = TPR + TNR - 1;
        MARKEDNESS = PPV + NPV - 1;
        
        %
        confMatStats(c,1).totalCountOfAll = totalCountOfAll;
        confMatStats(c,1).totalCountOfCategory = totalCountOfCategory;
        confMatStats(c,1).totalPredictionOfCategory = totalPredictionOfCategory;

        %
        confMatStats(c,1).TruePositive = TP;
        confMatStats(c,1).FalseNegative = FN;
        confMatStats(c,1).FalsePositive = FP;
        confMatStats(c,1).TrueNegative = TN;

        %
        confMatStats(c,1).Accuracy = ACC;
        confMatStats(c,1).Sensitivity = TPR;
        confMatStats(c,1).Specificity = TNR;
        confMatStats(c,1).Precision = PPV;
        confMatStats(c,1).Negative_Predictive_Value = NPV;
        confMatStats(c,1).False_Positive_Rate = FPR;
        confMatStats(c,1).False_Discovery_Rate = FDR;
        confMatStats(c,1).False_Negative_Rate = FNR;
        
        %
        confMatStats(c,1).F1_Score = F1;
        confMatStats(c,1).Matthews_Correlation_Coefficient = MCC;
        confMatStats(c,1).Informedness = INFORMEDNESS;
        confMatStats(c,1).Markedness = MARKEDNESS;
    end
    if categoryCount==1
        confMatStats = confMatStats(selectedCategories,1);
    end
end

% ACC;
% Accuracy is not a reliable metric for the real performance of a classifier, 
% because it will yield misleading results if the data set is unbalanced 
% (that is, when the number of samples in different classes vary greatly). 
% For example, if there were 95 cats and only 5 dogs in the data set, 
% the classifier could easily be biased into classifying all the samples as cats. 
% The overall accuracy would be 95%, but in practice the classifier would have 
% a 100% recognition rate for the cat class but a 0% recognition rate for the dog class.
