function d = calcDistanceOfSurfNormPatches_Master(im01_M, im02_M, xyzCnt)
    %set zero magnitude vecs of im01 to -im02 rows
    zeroMag_01 = im01_M(:,4)==0;
    zeroMag_02 = im02_M(:,4)==0;
    
    %if a patch has zero magnitude set it to the compared patches reverse
    %to get distance equal to -1
    im01_M(zeroMag_01,1:3) = -im02_M(zeroMag_01,1:3);
    im02_M(zeroMag_02,1:3) = -im01_M(zeroMag_02,1:3);
    
    %if xyzCnt==3 then all 3 directions are taken into account
    %if xyzCnt==2 then only X and Y directions are taken into account and Z is ignored
    d = im01_M(:, 1:xyzCnt).*im02_M(:, 1:xyzCnt);
    d = -sum(d,2);%similarity [-1 +1] -1 is opposite, +1 is same
    %I need 
    % -same to be -1 (decreases the distance of two patch)
    % -opposite to be +1 (increases the distance of two image patch)
    % -irrelevant to be 0 not to have any impact on the calculation
    %dVec = -d;
end