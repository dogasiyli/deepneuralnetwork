function runningBinaryClassificationScores = calcRunningStats(expectedLabel, assignedLabels)
    n = length(assignedLabels);
    boolVec = assignedLabels==expectedLabel;
    truePositiveCountVec = cumsum(boolVec==1);
    %falsePositiveCountVec = zeros(size(boolVec));
    falseNegativeCountVec = cumsum(boolVec==0);
    runningFscoreVec  = (2*truePositiveCountVec)./(2*truePositiveCountVec+falseNegativeCountVec);%harmonic mean of precision and sensitivity
    runningAccVec  = truePositiveCountVec./cumsum(ones(size(assignedLabels)));
    runningBinaryClassificationScores = [reshape(runningAccVec,1,[]);reshape(runningFscoreVec,1,[])];
end

