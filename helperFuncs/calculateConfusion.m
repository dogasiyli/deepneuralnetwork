function [accuracyCalced, confMatResult, sampleCountChanged, label_map] = calculateConfusion(predictedLabels, givenLabels, displayResult, categoryCount, OPS)
    if ~exist('OPS', 'var') || ~isstruct(OPS)
        OPS = [];
    end
    if (~exist('displayResult','var'))
        displayResult = false;
    end
    if exist('categoryCount','var')
        error('function has been changed - remove categoryCount from input');
    end
    [categoryCount, all_categories] = get_category_count(predictedLabels, givenLabels);
%     categories	= getStructField(OPS, 'categories', []);
%     if isempty(categories)
%         categories = compose('c%d',1:categoryCount);
%         %categories = cellstr(num2str(all_categories));
%     end
    
    sampleCountTotal = length(givenLabels);
    sampleCountChanged = sum(predictedLabels(:)~=givenLabels(:));
    accuracyCalced = 100 - 100*sampleCountChanged/sampleCountTotal;
    
    label_map = [reshape(1:categoryCount,[],1) reshape(all_categories,[],1)];
    given_mapped = map_veca_vecb(givenLabels, all_categories, 1:categoryCount);
    pred_mapped = map_veca_vecb(predictedLabels, all_categories, 1:categoryCount);

    confMatResult = confusionmat(given_mapped,pred_mapped,'order',1:categoryCount);
    if (displayResult)
        disp([ '(' num2str((sampleCountChanged),'%d') '/' num2str((sampleCountTotal),'%d') ')' num2str((accuracyCalced),'%4.2f') ' percent accuracy'])
        displayConfusionMatrix(confMatResult, unique(givenLabels));
    end
end

function [categoryCount, all_categories] = get_category_count(predictedLabels, givenLabels)
    given_categories = unique(givenLabels);
    predictied_categories = unique(predictedLabels);
    all_categories = unique([given_categories predictied_categories]);
    categoryCount = length(all_categories);
end