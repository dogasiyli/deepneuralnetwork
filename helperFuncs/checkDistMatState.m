function [distMat, stateStr] = checkDistMatState( distMat )
%checkDistMatState Checks the distMat
%   if it is a square matrix and all distances are other than inf or zero
%   stateStr = {'empty','notSquare','complete','hasNans'};
    [r, c] = size(distMat);
    if isempty(distMat)
        stateStr = 'empty';
        return
    elseif (r~=c)
        stateStr = 'notSquare';
        return
    end
    N = r;
    clear r c;
    
    D_rightUpperTriangle = triu(ones(N),+1);
    distMatNew = D_rightUpperTriangle;
    distMatNew(distMatNew ==1) = NaN;
    distMatNew(distMat~=0) = distMat(distMat~=0);
    
    nanInds = find(isnan(distMatNew)); %#ok<EFIND>
    if isempty(nanInds)
        stateStr = 'complete';
    else
        stateStr = 'hasNans';
    end    
    distMat = distMatNew;
end

