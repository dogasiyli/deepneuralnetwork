function check_reproducibility(known_rand_val, tol)
    if nargin==1
        tol = 10^-4;
    end
    randval = rand;
    if abs(randval-known_rand_val) < tol
        disp([num2str(randval,'%5.3f') '=? ' num2str(known_rand_val,'%5.3f') ' .Experiments will be reproducible'])
    else
        warning([num2str(randval,'%5.3f') '~=' num2str(known_rand_val,'%5.3f') ' .Experiments wont be reproducible'])
    end
end