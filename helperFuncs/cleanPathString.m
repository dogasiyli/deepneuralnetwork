function pathString = cleanPathString( pathString )
    if ismac
        % Code to run on Mac platform
    elseif isunix
        % Code to run on Linux platform
        %filesep = /
        fileSep_false = '\';
    elseif ispc
        % Code to run on Windows platform
        %%filesep = \
        fileSep_false = '/';
    else
        disp('Platform not supported')
    end    
    pathString = strrep(pathString, fileSep_false, filesep);
    while contains(pathString, [filesep filesep])
        pathString = strrep(pathString, [filesep filesep], filesep);
    end
end

