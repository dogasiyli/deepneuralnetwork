function [cnnnet, combinedCNNNETFile, batch_Count_Vec_new] = combineTwoNet( net1fn, net2fn, combStr, reOrganize, categoriesToLoad, validBatchID, batch_Count_Vec, enforceRecreateBatches)
    global fastDiskTempPath;
    
    cnnnet_01 = loadCnnetFromFile(net1fn);
    cnnnet_02 = loadCnnetFromFile(net2fn);
    displayOpts = setDisplayOpts('all');
    
    layerCnt_01 = length(cnnnet_01);
    layerCnt_02 = length(cnnnet_02);
    
    assert(layerCnt_01==layerCnt_02,'these two has to be same');
    cnnnet = [];

    combinedCNNNETImagesFolder = [fastDiskTempPath combStr '\wmV'];
    mkdir(combinedCNNNETImagesFolder);
    combinedCNNNETFile = [fastDiskTempPath 'combinedCnnnet_' combStr '.mat'];
    
    for l=1:layerCnt_01
        curLayer01 = cnnnet_01{l,1};
        curLayer02 = cnnnet_02{l,1};
        assert(strcmp(curLayer01.type,curLayer02.type),'these two has to be same');
        layerType = curLayer01.type;
        switch layerType
            case 'convolution'
                countFilt01 = curLayer01.countFilt;
                countFilt02 = curLayer02.countFilt;
                countFilt = countFilt01 + countFilt02;
                
                inputChannelSize01 = curLayer01.inputChannelSize;
                inputChannelSize02 = curLayer02.inputChannelSize;      
                if isempty(cnnnet)
                    assert(inputChannelSize01==inputChannelSize02,'these two has to be same');
                    inputChannelSize = inputChannelSize01;
                else
                    inputChannelSize = inputChannelSize01 + inputChannelSize02;
                end
                
                filterSize01 = curLayer01.filterSize;
                filterSize02 = curLayer02.filterSize;
                assert(sum(filterSize01-filterSize02)==0,'these two has to be same');
                filterSize = filterSize01;
                
                stride01 = curLayer01.stride;
                stride02 = curLayer02.stride;
                assert(sum(stride01-stride02)==0,'these two has to be same');
                stride = stride01;
                
                activationType01 = curLayer01.activationType;
                activationType02 = curLayer02.activationType;
                assert(strcmp(activationType01,activationType02),'these two has to be same');
                activationType = activationType01;
                                                        
                filtersWhitened_01 = curLayer01.filtersWhitened;
                biasesWhitened_01 = curLayer01.biasesWhitened;
                filtersWhitened_02 = curLayer02.filtersWhitened;
                biasesWhitened_02 = curLayer02.biasesWhitened;
                
                if isempty(cnnnet)
                    filtersWhitened = [filtersWhitened_01,filtersWhitened_02];
                else
                    filtersWhitened = [[filtersWhitened_01, zeros(inputChannelSize01*prod(filterSize),countFilt02)];...
                                       [zeros(inputChannelSize02*prod(filterSize),countFilt01), filtersWhitened_02]];
                end
                h = figure(128);clf;hold on;
                subplot(2,2,1);imagesc(filtersWhitened_01);title('filtersWhitened_-1');
                subplot(2,2,2);imagesc(filtersWhitened_02);title('filtersWhitened-02');
                subplot(2,2,[3 4]);imagesc(filtersWhitened);title([combStr '-combined']);
                drawnow;
                saveas(h,[combinedCNNNETImagesFolder '\L' num2str(l,'%02d') '.png']);
                
                biasesWhitened = [biasesWhitened_01;biasesWhitened_02];
                assert(sum(size(filtersWhitened)-[inputChannelSize*prod(filterSize) countFilt])==0,'these two has to be same');
                assert(sum(size(biasesWhitened)-[countFilt 1])==0,'these two has to be same');
                
                cnnnet = appendNetworkLayer(  cnnnet, 'convolution', displayOpts...
                                            , 'countFilt', countFilt ...
                                            , 'inputChannelSize', inputChannelSize ...
                                            , 'filterSize', filterSize ...
                                            , 'stride', stride ...
                                            , 'activationType', activationType ...
                                            , 'initializeWeightMethod', 'Random' ...
                                            , 'bnMode', 'batch' ...
                                            , 'wvnMode', 'epoch' ...
                                            , 'applyOrthonormalization', false );
                
                curLayerCount = size(cnnnet,1);
                cnnnet{curLayerCount,1}.filtersWhitened = filtersWhitened;
                cnnnet{curLayerCount,1}.biasesWhitened = biasesWhitened;
                cnnnet{curLayerCount,1}.applyBiasNorm=false;
                cnnnet{curLayerCount,1}.applyVarNorm=false;
            case 'activation'
                cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            case 'pooling'
                poolSize01 = curLayer01.poolSize;
                poolSize02 = curLayer02.poolSize;
                assert(sum(poolSize01-poolSize02)==0,'these two has to be same');
                poolSize = poolSize01;                
                
                poolType01 = curLayer01.poolType;
                poolType02 = curLayer02.poolType;
                assert(strcmp(poolType01,poolType02),'these two has to be same');
                poolType = poolType01;
                
                cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', poolSize, 'poolType', poolType );
            case 'softmax'
                cnnnet = appendNetworkLayer( cnnnet, 'softmax', displayOpts, 'initializeWeightMethod', 'Random', 'minFuncMaxIter', 0);
                if isfield(cnnnet_01{1, 1},'Welm_removableFeats')
                    Welm_removableFeats_net1 = cnnnet_01{1, 1}.Welm_removableFeats;
                    if ~isempty(Welm_removableFeats_net1)
                        disp('Removable features of net-1 : ')
                        disp(mat2str(Welm_removableFeats_net1))
                    end               
                end
                if isfield(cnnnet_02{1, 1},'Welm_removableFeats')
                    Welm_removableFeats_net2 = cnnnet_02{1, 1}.Welm_removableFeats;
                    if ~isempty(Welm_removableFeats_net2)
                        disp('Removable features of net-2 : ')
                        disp(mat2str(Welm_removableFeats_net2))
                    end
                end
            case 'fullyconnected'
                error('not implemented yet');
            otherwise
                error('You are suggesting a layer that is not present.Layers are - convolution, activation, pooling, softmax');
        end
        cnnnet{l,1}.fwdPropAllBatches = false;
    end
    
    N_testCombination = 8;%test the combination with N samples
    testCombinationWithDummySamples(N_testCombination, cnnnet_01, cnnnet_02, cnnnet);
    
    % categoriesToLoad = [1 2];
    % validBatchID = 5;
    % batch_Count_Vec = [10 5 4];
    % enforceRecreateBatches = true;
    if nargin>4
        dS = checkRecreateCifarBatches( categoriesToLoad, validBatchID, batch_Count_Vec, enforceRecreateBatches);   
        [ cnnnet, cnnnetResults, softMaxedData, pred_labels] = deepNet_Test( dS, cnnnet, setDisplayOpts('none'), setSaveOpts('none'), 'train', combStr, 0, true);
        cnnnet{end, 1}.Weight = cnnnet{1, 1}.Welm';
        cnnnet{end, 1}.BiasParams = zeros(size(cnnnet{end, 1}.BiasParams));
        [ cnnnet, cnnnetResults_train, softMaxedData, pred_labels] = deepNet_Test( dS, cnnnet, setDisplayOpts('none'), setSaveOpts('none'), 'train', combStr, 1, true);
        [ cnnnet, cnnnetResults_valid] = deepNet_Test( dS, cnnnet, setDisplayOpts('none'), setSaveOpts('none'), 'valid', combStr, 1, true);
        [ cnnnet, cnnnetResults_test] = deepNet_Test( dS, cnnnet, setDisplayOpts('none'), setSaveOpts('none'), 'test', combStr, 1, true);
        
        cnnnetResults_curBatch = struct;
        cnnnetResults_curBatch.train = cnnnetResults_train;
        cnnnetResults_curBatch.valid = cnnnetResults_valid;
        cnnnetResults_curBatch.test = cnnnetResults_test;
        cnnnet = getCNNETreadyForNewData( cnnnet, 1, [], 1);  
    else
        cnnnetResults_curBatch = [];
    end
    save(combinedCNNNETFile,'cnnnet','cnnnetResults_curBatch');
    if reOrganize && nargin > 4
        batch_Count_Vec = [12 6 6];
        [dS_New, batch_Count_Vec_new] = grabFalseSamplesReOrganize(combinedCNNNETFile, categoriesToLoad, batch_Count_Vec, 'uniform', true);
    else
        batch_Count_Vec_new = [];
    end
end

function testCombinationWithDummySamples(N, cnnnet_01, cnnnet_02, cnnnet_0102)
    global fastDiskTempPath;
    if N<=0
        return
    end
    inputSize_original = cnnnet_01{1, 1}.inputSize_original;
    inputSize_original(end) = N;
    
    countCategory = cnnnet_01{end, 1}.countCategory;
    
    dataTr = rand(inputSize_original);
    labelsTr = randi(countCategory,1,N);
    
    displayOpts = setDisplayOpts('none');
    saveMainFolder = [fastDiskTempPath 'testCombineNetsWI\'];
    if exist(saveMainFolder,'dir')
        rmdir(saveMainFolder,'s');
    end
    saveOpts = setSaveOpts('none', saveMainFolder);
    loadFolderForDummyData = [saveMainFolder 'data\'];
    dS = create_dS_frData(dataTr, labelsTr, 'loadFolder', loadFolderForDummyData);
    

    saveOpts.MainFolder = [saveMainFolder 'cnnnet_01\'];
    [cnnnet_01, ~, softMData_01_tr, ~, data_LL_01_tr, labels_LL_01_tr] = deepNet_Test(dS,cnnnet_01,displayOpts,saveOpts, 'train', 'cnnet01_{tr}', 1, false, false);
    saveOpts.MainFolder = [saveMainFolder 'cnnnet_02\'];
    [cnnnet_02, ~, softMData_02_tr, ~, data_LL_02_tr, labels_LL_02_tr] = deepNet_Test(dS,cnnnet_02,displayOpts,saveOpts, 'train', 'cnnet02_{tr}', 1, false, false);

    dtStr = {'Train'};
    for c = 1:2
        for i=1:6
            for dt = 1
                loadFileName = [saveMainFolder 'cnnnet_' num2str(c,'%02d') '\' dtStr{dt} '\ActivatedData\Activation_' num2str(i,'%02d') '.mat'];
                load(loadFileName);
                evalStr = ['a_L' num2str(i,'%02d') '_CNN' num2str(c,'%02d') '_' dtStr{dt} ' = a;'];
                disp(loadFileName)
                disp(evalStr)
                eval(evalStr);
            end
        end
    end
    clear a loadFileName evalStr c i dt;
    
    saveOpts.MainFolder = [saveMainFolder 'cnnnet_0102\'];
    [cnnnet_0102, ~, softMData_0102_tr, ~, data_LL_0102_tr, labels_LL_0102_tr] = deepNet_Test(dS,cnnnet_0102,displayOpts,saveOpts, 'train', 'cnnet0102_{tr}', 1, false, false);
    
    sumDifResults = NaN(6,1);
    for i=2:6
        for dt = 1
                loadFileName = [saveMainFolder 'cnnnet_0102\' dtStr{dt} '\ActivatedData\Activation_' num2str(i,'%02d') '.mat'];
                load(loadFileName);
                aCombStr = ['a_L' num2str(i,'%02d') '_CNNComb0102_' dtStr{dt}];
                evalStr = [aCombStr ' = a;'];
                disp(loadFileName)
                disp(evalStr)
                eval(evalStr);
                
                a1Str = ['a_L' num2str(i,'%02d') '_CNN' num2str(1,'%02d') '_' dtStr{dt}];
                a2Str = ['a_L' num2str(i,'%02d') '_CNN' num2str(2,'%02d') '_' dtStr{dt}];
                %eval2str = ['sumDifResults(i,dt) = sum(sum(a_L06_CNNComb0102_Valid-[a_L06_CNN01_Valid;a_L06_CNN02_Valid]));'];
                eval2str = ['sumDifResults(i,dt) = sum(sum(' aCombStr '-[' a1Str ';' a2Str ']));'];
                disp(eval2str)
                eval(eval2str);                
        end
    end
    clear a loadFileName evalStr aCombStr a1Str a2Str i dt;
    disp(sumDifResults);  
    rmdir(saveMainFolder,'s');
end