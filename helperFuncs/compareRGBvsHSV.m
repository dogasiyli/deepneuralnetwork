function imHSV = compareRGBvsHSV( imRGB, figID)
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here
    imHSV = rgb2hsv(imRGB);
    if exist('figID','var')
        figure(figID);clf;
        imRGBPlot = imRGB;imRGBPlot(:,:,2:3) = 0;
        subplot(3,2,1);imshow(imRGBPlot);title('R channel');
        imRGBPlot = imRGB;imRGBPlot(:,:,[1 3]) = 0;
        subplot(3,2,2);imshow(imRGBPlot);title('G channel');
        imRGBPlot = imRGB;imRGBPlot(:,:,1:2) = 0;
        subplot(3,2,3);imshow(imRGBPlot);title('B channel');
        subplot(3,2,4);imshow(imHSV(:,:,1)./max(imHSV(:)));title('H channel');
        subplot(3,2,5);imshow(imHSV(:,:,2)./max(imHSV(:)));title('S channel');
        subplot(3,2,6);imshow(imHSV(:,:,3)./max(imHSV(:)));title('V channel');
    end

end

