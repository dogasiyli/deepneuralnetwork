function [ confusion ] = computeConfusionMatrix( labelsGiven,labelsCalculated )

    assert(length(labelsGiven)==length(labelsCalculated),'can not compute confusion matrix of different length label vectors');
    %labelsGiven and labelsCalculated must be any number in 1:uniqueLabelCount
    labelList = unique([labelsGiven(:);labelsCalculated(:)]);
    
    uniqueLabelCount = length(labelList);
    
    confusionMatrix = zeros(uniqueLabelCount,uniqueLabelCount);
    
    for motID_R=1:uniqueLabelCount%real value
        for motID_C=1:uniqueLabelCount%classified value
            slctd = labelsGiven(:)==motID_R & labelsCalculated(:)==motID_C;
            confusionMatrix(motID_R,motID_C) = sum(slctd==1);
        end
    end

    confusePercent = bsxfun(@rdivide,confusionMatrix,sum(confusionMatrix,2));

    confusion.Matrix  = confusionMatrix;
    confusion.Percent = confusePercent;
    confusion.Accuracy = sum(diag(confusionMatrix))/sum(confusionMatrix(:));
end