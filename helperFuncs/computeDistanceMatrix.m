function [distMat, minDistElements, maxDistElements] = computeDistanceMatrix(X, Y, minmaxDistSampleIDRetrieveCount)
%getDistanceMatrix Retrieves the distance matrix of X and Y, additionally
%finds more information
%   X - is N by D matrix
%   Y - is M by D matrix. if empty it means to get Y=X
%   minDistSampleIDRetrieveCount - number of row and col match that has the
%   minimum values in distMat

    %distMat is expected to be N by N
    %rows represent set X
    %cols represent set Y
    if isempty(Y)
        Y = X;
    end
    distMat = pdist2_fast(X, Y);
    X_equal_Y = sum(diag(distMat))==0;
    
    distMat_search = distMat;
    if X_equal_Y
        distMat_search(triu(ones(size(distMat_search)))) = inf;
        %when we sort this distMat, we get the best match of crops
    end
    
    %when we sort this distMat, we get the best match of crops
    [distValsBlocks, distInds] = sort(distMat_search(:)); 
    minmaxDistSampleIDRetrieveCount = min(minmaxDistSampleIDRetrieveCount,length(distInds));

    %best match crop is the minimum element of this 276 by 276 distance matrix
    minDist = distValsBlocks(1);    

    %then we find the row and col of this min distance value
    %row represents the ID of X image crop area
    %col represents the ID of Y image crop area
    %here we take <boxCount> number of best crops
    [rowInds, colInds] = ind2sub(size(distMat),distInds(1:minmaxDistSampleIDRetrieveCount));
    minDistElements = struct;
    minDistElements.minDist = minDist;
    minDistElements.rowInds = rowInds;
    minDistElements.colInds = colInds;
    minDistElements.values = distMat(distInds(1:minmaxDistSampleIDRetrieveCount));
    
    if nargout>2
        %calculate maxDistElements
        distMat_search = distMat;
        distMat_search(1==triu(ones(size(distMat_search)))) = -inf;
        [distValsBlocks, distInds] = sort(distMat_search(:),'descend');
        maxDist = distValsBlocks(1);
        [rowInds, colInds] = ind2sub(size(distMat),distInds(1:minmaxDistSampleIDRetrieveCount));
        maxDistElements = struct;
        maxDistElements.maxDist = maxDist;
        maxDistElements.rowInds = rowInds;
        maxDistElements.colInds = colInds;
        maxDistElements.values = distMat(distInds(1:minmaxDistSampleIDRetrieveCount));        
    else
        maxDistElements = [];
    end
    
end