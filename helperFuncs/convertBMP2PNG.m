function convertBMP2PNG(mainFolderName)
    [fileNames, numOfFiles] = getFileList(mainFolderName, '.bmp', '', false);
    for i=1:numOfFiles
        bmpFileName = [mainFolderName filesep fileNames{i}];
        a = imread(bmpFileName);
        pngFileName = strrep(bmpFileName,'.bmp','.png');
        imwrite(a, pngFileName);        
    end
end

