function n = convertPNG2JPG(rootFold, goRecursivelyDeep, removePng, toSpecificFolder, keepHierarchy)
%V01 RR, goRecurs=true, rmvPng=false, toSpecFold="XX", keepHiera=true
%copies all images as jpg from RR into folder XX keeping hierarchy
%V02 RR, goRecurs=true, rmvPng=false, toSpecFold="XX", keepHiera=false
%copies all images as jpg from RR into folder XX without keeping hierarchy

    if ~exist('keepHierarchy','var') || isempty(keepHierarchy)
        keepHierarchy = false;
    end   
    if ~isempty(toSpecificFolder)
        folderToWriteTo = toSpecificFolder;
    else
        folderToWriteTo = rootFold;
    end
    createFolderIfNotExist(folderToWriteTo, true);

    [fileNames, numOfFiles] = getFileList(rootFold, '.png', '', false);
    n = 0;
    for i=1:numOfFiles
        pngFileName = [rootFold filesep fileNames{i}];
        a = imread(pngFileName);
        jpgFileName = [folderToWriteTo filesep fileNames{i}];
        jpgFileName = strrep(jpgFileName,'.png','.jpg');
        imwrite(a, jpgFileName, 'Quality', 100);    
        if removePng
            delete(pngFileName)
        end
        n = n + 1;
    end
    if goRecursivelyDeep
        subFolderNames = getFolderList(rootFold, false, false);
        for i=1:length(subFolderNames)
            if keepHierarchy
                toSpecificFolder_sub = [folderToWriteTo filesep subFolderNames{i}];
            else
                toSpecificFolder_sub = folderToWriteTo;
            end
            rootFold_sub = [rootFold filesep subFolderNames{i}];
            n_sub = convertPNG2JPG(rootFold_sub, goRecursivelyDeep, removePng, toSpecificFolder_sub, keepHierarchy);
            n = n + n_sub;
        end
    end
end

