%https://www.mathworks.com/matlabcentral/answers/115838-count-occurrences-of-string-in-a-single-cell-array-how-many-times-a-string-appear
function [strResult,uniqStrings,occStrings] = countUniqueCells(cellVec)
    [uniqStrings, ~, J]=unique(cellVec);
    occStrings = histc(J, 1:numel(uniqStrings));
    
    [~, sortIdx] = sort(occStrings,'descend');
    strResult = '';
    if iscell(uniqStrings)
        for i = sortIdx'
            strResult = [strResult uniqStrings{1,i}, '(' num2str(occStrings(i)) '),'];
        end
        strResult = strResult(1:end-1);
    end
end

