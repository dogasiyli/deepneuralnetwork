function [retVal, messageString] = createFolderIfNotExist(folderName, dispInfo)
    if ~exist('dispInfo','var') || isempty(dispInfo)
        dispInfo = false;
    end   
    retVal = false;
    try
        if ~exist(folderName ,'dir')
            if dispInfo
                disp(['creating folder(' folderName ')']);
            end
            mkdir(folderName);
            retVal = true;
            if dispInfo
                messageString = ['folder(' folderName ') created'];
            end
        else
            retVal = true;
            if dispInfo
                messageString = ['folder(' folderName ') was already created'];
            end
        end
    catch err
        messageString = err.message;
    end
end