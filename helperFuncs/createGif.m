function createGif(input_folder_name, starts_with, file_type, total_time, pause_count)
    if ~exist('total_time','var') || isempty(total_time)
        total_time = 3;
    end
    if ~exist('pause_count','var') || isempty(pause_count)
        pause_count = 5;
    end
    [fileNames, ~] = getFileList(input_folder_name, file_type, starts_with, false);
    out_file_name = [input_folder_name filesep starts_with 'animated.gif'];
    
    N = length(fileNames);
    delayTimeValue = total_time/N;
    wait_frame = 1;
    fileNames = sort(fileNames);
    for i = 1:N
        fn = [input_folder_name filesep fileNames{i}];
        im = imread(fn);
        [rs, cs, ch] = size(im);
        rs_num = round(rs/2);
        numIm = getNumberImage( i, rs_num, rs_num);
        numIm = repmat(numIm, 1, 1, 3);
        
        im_new = zeros(rs+rs_num, cs, ch, 'uint8');
        im_new(1:rs_num,1:rs_num,:) = round(numIm);
        im_new(1+rs_num:end,:,:) = im;
        
        [imind,cm] = rgb2ind(im_new,256);
        
        if ceil((pause_count-1)*((i+1)/N))>wait_frame || i==N
            wait_frame = wait_frame + 1;
            dtc = 1;
            disp(['Pause at ' num2str(i) ' of ' num2str(N)])
        else
            dtc = delayTimeValue;
        end
        
        if (i == 1)
            imwrite(imind,cm,out_file_name,'gif', 'Loopcount',inf,'DelayTime',1); 
        else 
            imwrite(imind,cm,out_file_name,'gif','WriteMode','append','DelayTime',dtc); 
        end
    end
end

