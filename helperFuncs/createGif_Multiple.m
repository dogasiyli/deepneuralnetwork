function createGif_Multiple(folder_names, starts_with, file_type, total_time, pause_count, out_folder)
    if ~exist('total_time','var') || isempty(total_time)
        total_time = 3;
    end
    if ~exist('pause_count','var') || isempty(pause_count)
        pause_count = 5;
    end
    if ~exist('out_folder','var') || isempty(out_folder)
        out_folder = [];
    end
    
    folder_counts = length(folder_names);
    file_counts = zeros(1, folder_counts);
    file_names = {[]};
    for i=1:folder_counts
        input_folder_name = folder_names{i};
        if isempty(out_folder)
            parent_folder = strsplit(input_folder_name, filesep);
            out_folder = [filesep fullfile(parent_folder{1:end-1})];
            disp(['out_folder : ' out_folder])
        end
        [file_names{i}, ~] = getFileList(input_folder_name, file_type, starts_with, false);
        file_names{i} = sort(file_names{i});
        file_counts(i) = length(file_names{i});
        disp([input_folder_name, ',', num2str(file_counts(i))])
    end
    
    out_file_name = [out_folder filesep starts_with 'animated.gif'];
    N = max(file_counts);
    delayTimeValue = total_time/N;
    wait_frame = 1;
    
    rs_max = -1;
    cs_max = -1;
    ch_set = -1;
    for i = 1:N
        acquired_images = {[]};
        for f_i=1:folder_counts
            if i<=file_counts(f_i)
                fn = [input_folder_name filesep file_names{f_i}{i}];
                if exist(fn, 'file')
                    im = imread(fn);
                    [rs, cs, ch] = size(im);
                    rs_max = max([rs_max, rs]);
                    cs_max = max([cs_max, cs]);
                    if ch_set==-1
                        ch_set = ch;
                    end
                    acquired_images{f_i} = im;
                else
                    acquired_images{f_i} = zeros(rs_max, cs_max, ch_set, 'uint8');
                end
                
            end
        end
        for f_i=1:folder_counts
            acquired_images{f_i} = imresize(acquired_images{f_i}, [rs_max, cs_max]);
        end
        rs_num = round(rs_max/2);
        numIm = getNumberImage( i, rs_num, rs_num);
        numIm = repmat(numIm, 1, 1, ch);
        
        %create the image palette to fill in
        im_new = zeros(folder_counts*rs_max+rs_num, cs_max, ch, 'uint8');
        %add the iteration id on top left corner
        im_new(1:rs_num,1:rs_num,:) = round(numIm);
        for f_i=1:folder_counts
            fr_r = 1+rs_num+(f_i-1)*rs_max;
            to_r = fr_r + rs_max - 1;
            im_new(fr_r:to_r,:,:) = acquired_images{f_i};
        end
        [imind,cm] = rgb2ind(im_new,256);
        if ceil((pause_count-1)*((i+1)/N))>wait_frame || i==N
            wait_frame = wait_frame + 1;
            dtc = 1;
            disp(['Pause at ' num2str(i) ' of ' num2str(N)])
        else
            dtc = delayTimeValue;
        end
        
        if (i == 1)
            imwrite(imind,cm,out_file_name,'gif', 'Loopcount',inf,'DelayTime',1); 
        else 
            imwrite(imind,cm,out_file_name,'gif','WriteMode','append','DelayTime',dtc); 
        end
    end
end

