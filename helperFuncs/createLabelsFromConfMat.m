function [ clusterLabels, gtLabels ] = createLabelsFromConfMat( cMat )
    N = sum(cMat(:));
    clusterLabels = zeros(1,N);
    gtLabels = zeros(1,N);
    [clusterCount,gtCount] = size(cMat);
    fr = 1;
    for g = 1:gtCount
        for c = 1:clusterCount
           to = fr +  cMat(c,g)-1;
           clusterLabels(fr:to) = c;
           gtLabels(fr:to) = g;
           fr=to+1;
        end
    end
    cMatNew = confusionmat(clusterLabels,gtLabels);  
end

