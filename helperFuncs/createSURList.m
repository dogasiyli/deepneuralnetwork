function [surList, listElemntCnt] = createSURList(srcFold, createMode, infoStrct)
%createSURList According to given parameters this function will create an
%suv list to parse through.. This function is depending on HospiSign
%Dataset
%   There are different modes of list creation
%   Mode-1 : signIDList, userList, maxRptID
%   Mode-2 : signIDList, userID, maxRptCnt

    switch createMode
        case 1
            signIDList = infoStrct.signIDList;
            userList = infoStrct.userList;
            maxRptID = infoStrct.maxRptID;
            surList = getSUVListMode01(srcFold, signIDList, userList, maxRptID);
        case 2
            signIDList = infoStrct.signIDList;
            userList = infoStrct.userID;
            maxRptCnt = infoStrct.maxRptCnt;
            if ~isfield(infoStrct,'minRepCnt')
                minRepCnt = 0;
            else
                minRepCnt = infoStrct.minRepCnt;
            end
            surList = getSUVListMode02(srcFold, signIDList, userList, maxRptCnt, minRepCnt);
    end
    listElemntCnt = size(surList,1);
end

function surList = getSUVListMode01(srcFold, signIDList, userList, maxRptID)
    surList = [];
    for s = signIDList
        for u = userList
            for r = 1:maxRptID
                theFoldName = srcFold;
                %into sign folder
                theFoldName = [theFoldName filesep num2str(s) ]; %#ok<*AGROW>
                if ~exist(theFoldName, 'dir')
                    continue
                end
                %into user-repeat folder
                theFoldName = [theFoldName filesep 'User_' num2str(u) '_' num2str(r)];
                if ~exist(theFoldName, 'dir')
                    continue
                end
                surList = [surList;s u r];
            end
        end
    end
end

function surList = getSUVListMode02(srcFold, signIDList, userList, maxRptCnt, minRepCnt)
    surList = [];
    for s = signIDList
        for u = userList
            repCnt = 0;
            tryCnt = 0;
            r = 0;
            while repCnt<maxRptCnt && tryCnt<50
                tryCnt = tryCnt+1;
                theFoldName = srcFold;
                %into sign folder
                theFoldName = [theFoldName filesep num2str(s) ]; %#ok<*AGROW>
                if ~exist(theFoldName, 'dir')
                    continue
                end
                %into user-repeat folder
                r = r + 1;
                theFoldName = [theFoldName filesep 'User_' num2str(u) '_' num2str(r)];
                if ~exist(theFoldName, 'dir')
                    %if it exists it will go on
                    continue
                end
                if repCnt>=minRepCnt
                    surList = [surList;s u r];
                else
                    disp(['Skipping sur(' mat2str([s u r]) ') - because repCnt(' num2str(repCnt) ')<(' num2str(minRepCnt) ')minRepCnt']);
                end
                %only increase repitition count if any r is added
                repCnt = repCnt+1;
            end
        end
    end
end
