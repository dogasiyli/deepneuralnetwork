function dS = create_dS_frData(data, labels, varargin)
    global bigDataPath;
    %P1D0 -> Passed assed paramaeter = 1, Default value used = 0
    [ loadFolder,   data_valid,  labels_valid,   data_test,  labels_test,  P1D0] = parseArgs(...
    {'loadFolder'  'data_valid' 'labels_valid'  'data_test' 'labels_test'  },...
    { bigDataPath       []             []           []             []      },...
    varargin{:});

    dS = struct;
% 1.params that do not change
% 	a. countCategory -> total number of class IDs that the whole data has.
    dS.countCategory = length(unique(labels));
        dS.categoryIDsToLoad = unique(labels);
        dS.categoryNamesToLoad = unique(labels);
%   b. countBatch.{train,validation,test} -> it is a struct itself too
    dS.countBatch.train = 1;
    dS.countBatch.valid = double(~isempty(data_valid) && ~isempty(labels_valid));
    dS.countBatch.test = double(~isempty(data_test) && ~isempty(labels_test));
    dS.countBatch.act = [];%the number of batches in an activation batch
% 	c. batchInfo -> empty for this kinda dS.
        %load('D:\Datasets\dataset5\batchInfo.mat');
    %dS.batchInfo = [];%batchInfo;
%   d. fileNameStruct -> an inline function to get fileName to load
    dS.fileNameStruct = '<data-act>_batch_<train-valid-test>_<batchID>-<batchCount>.mat';
%   e. getFileName -> an inline function to get fileName to load
    dS.getFileName = @(data_act_STR, type_STR, batchID_INT, batchCnt_INT) [data_act_STR '_batch_' type_STR '_' num2str(batchID_INT,'%02d') '-' num2str(batchCnt_INT,'%02d') '.mat'];
%   f. getFileName_Current -> an inline function to get the current fileName to load
    dS.getFileName_Current = @(dS) [dS.loadFolder dS.data_act '_batch_' dS.type '_' num2str(dS.batchID_current,'%02d') '-' num2str(dS.batchCnt_current,'%02d') '.mat'];
%      e.g. data_batch_train_01.mat
%   g. getLabelVec
    dS.getLabelVec = @(labels_) labels_(:)';
%   h. getLabelArr
    dS.getLabelArr = @(labels_) full(sparse(labels_, 1:length(labels_), 1));
% 	i. singleImageShape -> [R,C,F]
%   j. imageShapeIdentifier-> 'square' or 'rectangle' - only square will be allowed initially
% 	k. countFeat -> prod(R,C,F)
    if length(size(data))==4
        dS.singleImageShape = size(data);
        dS.singleImageShape = dS.singleImageShape(1:3);
        if dS.singleImageShape(1)==dS.singleImageShape(2)
            dS.imageShapeIdentifier = 'square';
        else
            dS.imageShapeIdentifier = 'rectangle';
        end
        dS.countFeat = prod(dS.singleImageShape);
    else
        dS.singleImageShape = size(data,1);
        dS.imageShapeIdentifier = 'vector';
        dS.countFeat = size(data,1);
    end
%   l. updateDataStruct
    dS.updateDataStruct = @(X_, Y_) updateDataStruct(X_, Y_{:});
    

% 2.params that will change accordingly
% 	a. type -> train, validation, test
    dS.type = 'train';
%   b. data_act -> 'data' or 'act'
    dS.data_act = 'data';
% 	c. loadFolder -> the exact folder that the train/valid/test data/act is in.
    if (~strcmp(loadFolder(end),'\'))
        loadFolder = [loadFolder '\'];
    end
    if (~exist(loadFolder,'dir'))
        mkdir(loadFolder);
    end
    dS.loadFolder = loadFolder;%'D:\Datasets\CIFAR10\';%will become the temporary folder for activations
    
% 3.params that change per batch
%   a. batchID_current -> initially it will be 1, then it will increment until numOfBatches
    dS.batchID_current = 1;
    dS.batchCnt_current = dS.countBatch.train;
    dS.fileName_current = dS.getFileName_Current(dS);
    
%now save the given data andlabels to further use them
    save(dS.fileName_current,'data','labels','-v7.3');
    if ~isempty(data_valid) && ~isempty(labels_valid)
        dS = dS.updateDataStruct(dS, {'type', 'valid'});
        data = data_valid;
        labels = labels_valid;
        save(dS.fileName_current,'data','labels','-v7.3');
        clear data_valid labels_valid
    end
    if ~isempty(data_test) && ~isempty(labels_test)
        dS = dS.updateDataStruct(dS, {'type', 'test'});
        data = data_test;
        labels = labels_test;
        save(dS.fileName_current,'data','labels','-v7.3');
        clear data_test labels_test
    end  
    dS = dS.updateDataStruct(dS, {'type', 'train'});
    
%   b. fileName_current
    dS.fileName_current = dS.getFileName_Current(dS);
% 	c. countSamples -> total number of samples in current batch
        load(dS.fileName_current);
        data_size_vec  = size(data);
    if length(data_size_vec)==4 
        dS.singleImageShape = data_size_vec(1:3);%normally already manually written above
        dS.countSamples = data_size_vec(4);
    else
        dS.singleImageShape = data_size_vec(1);%normally already manually written above
        dS.countSamples = data_size_vec(2);
    end
        clear data_size_vec;
    dS.countFeat = prod(dS.singleImageShape);%normally already manually written above
end