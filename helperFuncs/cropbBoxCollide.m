function intersectPercent = cropbBoxCollide(cropArea_X, cropArea_Y)
    %return how much do the two crops intersect in percentage
    %e.g. intersectPercent = cropbBoxCollide([5 10 20 20], [50 12 20 20]);
    bigBoxRY_start = min(cropArea_X(2),cropArea_Y(2));
    bigBoxCX_start = min(cropArea_X(1),cropArea_Y(1));
    
    bigBoxRY_final = max(sum(cropArea_X([2 4])),sum(cropArea_Y([2 4])));
    bigBoxCX_final = max(sum(cropArea_X([1 3])),sum(cropArea_Y([1 3])));

    RY_01 = cropArea_X(2)-bigBoxRY_start+1 : cropArea_X(2)-bigBoxRY_start+cropArea_X(4);
    CX_01 = cropArea_X(1)-bigBoxCX_start+1 : cropArea_X(1)-bigBoxCX_start+cropArea_X(3);
    RY_02 = cropArea_Y(2)-bigBoxRY_start+1 : cropArea_Y(2)-bigBoxRY_start+cropArea_Y(4);
    CX_02 = cropArea_Y(1)-bigBoxCX_start+1 : cropArea_Y(1)-bigBoxCX_start+cropArea_Y(3);

    bigIm = zeros(bigBoxRY_final-bigBoxRY_start,bigBoxCX_final-bigBoxCX_start);
    bigIm(RY_01,CX_01) = 1;
    bigIm(RY_02,CX_02) = bigIm(RY_02,CX_02) + 1;
    collidePixCount = sum(sum(bigIm==2));
    %unCollidePixCount = sum(sum(bigIm==1));
    %emptyPixCount = sum(sum(bigIm==0));
    %intersectPercent = collidePixCount/(collidePixCount+unCollidePixCount);
    allPixCount = numel(bigIm);
    intersectPercent = collidePixCount/allPixCount;
end