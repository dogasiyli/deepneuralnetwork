function [ groupedDeepHandAssignments ] = analyzeDeepHandHospiSignCrossLabels( confMat )
%analyzeDeepHandHospiSignCrossLabels analyzes the confusion mat of
%hospiSign versus deepHand labels
%   rows will be hospiSign labels
%   cols will be deepHand labels
%   first col will be no-label-deepHand
%   includingBlankLabels - iBL
%   withOutBlankLabels - woBL

    hospLabelCnt = size(confMat,1);
    confMat_woBL = confMat(:,2:end);%hospLabelCnt_by_60
    
    dhl_Sum_iBL = sum(confMat);%1x61 - DeepHandLabel sum INCLUDING blank labels of deep hand
    dhl_Sum_woBL = sum(confMat(:,2:end));%1x60 - DeepHandLabel sum WITHOUT blank labels of deep hand
    hslAssigned_iBL = sum(confMat,2);%hospLabelCntx1 - HospiSignLabel sum INCLUDING blank labels of deep hand
    hslAssigned_woBL = sum(confMat_woBL,2);%hospLabelCntx1 - HospiSignLabel sum WITHOUT blank labels of deep hand
    
    unassignedDeepHandLabelIDs = find(sum(confMat_woBL)==0);
    disp(['There are ' num2str(length(unassignedDeepHandLabelIDs),'%d') ' unassigned deep hand labels']);

    %accuracy point of view shows -> normalize by sum over columns
    %meaning : the distribution of each hospiSign labels over each deepHandLabel
    hslAcc = confMat_woBL./hslAssigned_woBL;
    
    %from recall point of view
    %how healthy are the frames labelled as a deepHandLabel
    %in other words - if I use the label that deepHand system gives me
    %can I trust this decision according to hospiSign label assignment?
    dhRcl = confMat_woBL./dhl_Sum_woBL;

    
    hslAcc_IDs = [1:60;hslAcc];
    hslAcc_IDs = [reshape(0:hospLabelCnt,hospLabelCnt+1,1),hslAcc_IDs];
    disp(['Removing unassigned deep hand labels from  <hslAcc_IDs>: ' mat2str(unassignedDeepHandLabelIDs)]);
    hslAcc_IDs(:,1+unassignedDeepHandLabelIDs) = [];
    hslAcc_IDs_removeEps = hslAcc_IDs;
    hslAcc_IDs_removeEps(hslAcc_IDs_removeEps<0.01) = 0;
    colsSumsToClear = sum(hslAcc_IDs_removeEps(2:end,:));
    colsToClear = find(colsSumsToClear<=0.1);
    lowPercentAssignedDeepHandLabelIDs = hslAcc_IDs_removeEps(1,colsToClear);
    disp(['Deep hand labels to delete due to low percentage on all hospiSignLabels are : ' mat2str(lowPercentAssignedDeepHandLabelIDs)]);
    hslAcc_IDs_removeEps(:,colsToClear) = [];
    
    disp(['There are ' num2str(size(hslAcc_IDs_removeEps,2),'%d') ' deep hand labels that can be assigned to hospiSign labels according to high percentage of assignment']);
    %find which deep hand labels are assigned to which hospisign labels
    [~, hsl_dhl_id] = max(hslAcc_IDs_removeEps(2:end,2:end),[],2);
    hsl_dhl_id = hslAcc_IDs_removeEps(1,(1+hsl_dhl_id)');
    uniqHSLIDs = unique(hsl_dhl_id);
    uniqHSL_count = length(uniqHSLIDs);
    disp(['Deep handIDs that are assigned hospiSign hand shapes are ' mat2str(uniqHSLIDs) '.']);
    groupedDeepHandAssignments = []; %#ok<*NASGU>
    for i=1:uniqHSL_count
        deepHandLabel = uniqHSLIDs(i);
        hospSignIDs_assigned = find(hsl_dhl_id==deepHandLabel)';
        disp(['--deepHandID(' num2str(uniqHSLIDs(i)) ') - hospiSignIDs(' mat2str(hospSignIDs_assigned) ').']);
        hospSignIDs_percent = hslAcc(hospSignIDs_assigned,deepHandLabel);
        hospSignIDs_recall = dhRcl(hospSignIDs_assigned,deepHandLabel);
        hospSignIDs_frameCount_ass = confMat_woBL(hospSignIDs_assigned,deepHandLabel);
        hospSignIDs_frameCount_tot = hslAssigned_woBL(hospSignIDs_assigned);
        hospSignIDs_frameCount_all = hslAssigned_iBL(hospSignIDs_assigned);
        hospSignIDs_mat = [hospSignIDs_assigned hospSignIDs_percent hospSignIDs_recall hospSignIDs_frameCount_ass hospSignIDs_frameCount_tot hospSignIDs_frameCount_all];
        hospSignIDs_mat = sortrows(hospSignIDs_mat,-2);
        
        for j = 1:size(hospSignIDs_mat,1)
            disp(['----hospiSignID(' num2str(hospSignIDs_mat(j,1),'%d') ') - acc(' num2str(hospSignIDs_mat(j,2),'%4.2f') ') - recall(' num2str(hospSignIDs_mat(j,3),'%4.2f') ') - frameCount(' num2str(hospSignIDs_mat(j,4),'%d') ' of ' num2str(hospSignIDs_mat(j,5),'%d') ' from ' num2str(hospSignIDs_mat(j,6),'%d') ')']);
        end
        
        groupedDeepHandAssignments = [groupedDeepHandAssignments; deepHandLabel*ones(size(hospSignIDs_mat,1),1) hospSignIDs_mat]; %#ok<AGROW>
    end
    
    disptable(groupedDeepHandAssignments,'deepHandID|hospiSignID|accuracy|recall|fcAssigned|fcLabelled|fcAll');
    
    hospSign_accBasedResults = struct;
    hospSign_accBasedResults.accuracyBasedRowTable = hslAcc;
    hospSign_accBasedResults.accuracyBasedTableWithIDs_onlyAssignedDeepHandCols = hslAcc_IDs;
    hospSign_accBasedResults.accuracyBasedTableWithIDs_onlyReasonableDeepHandCols = hslAcc_IDs_removeEps;
    hospSign_accBasedResults.unassignedDeepHandLabelIDs = unassignedDeepHandLabelIDs;
    hospSign_accBasedResults.lowPercentAssignedDeepHandLabelIDs = lowPercentAssignedDeepHandLabelIDs;
    
%*****************RECALL*******************************%    

    dhRcl_IDs = [1:60;dhRcl];
    dhRcl_IDs = [reshape(0:hospLabelCnt,hospLabelCnt+1,1),dhRcl_IDs];
    disp(['Removing unassigned deep hand labels from  <dhRcl_IDs>: ' mat2str(unassignedDeepHandLabelIDs)]);
    dhRcl_IDs(:,1+unassignedDeepHandLabelIDs) = [];
    dhRcl_IDs_removeEps = dhRcl_IDs;
    dhRcl_IDs_removeEps(dhRcl_IDs_removeEps<0.01) = 0;
    rowSumsToClear = sum(dhRcl_IDs_removeEps(:,2:end),2);
    rowsToClear = find(rowSumsToClear<=0.1);
    lowPercentAssignedHospiSignLabelIDs = dhRcl_IDs_removeEps(rowsToClear,1);
    disp(['HospiSignLabels to delete due to low percentage on all deepHandLabels are : ' mat2str(lowPercentAssignedHospiSignLabelIDs)]);
    dhRcl_IDs_removeEps(rowsToClear,:) = [];

    
    %find which hospiSignLabels are assigned to which deepHandLabels
    [dhl_hsl_rcl, dhl_hsl_id] = max(dhRcl_IDs_removeEps(2:end,2:end));
    dhl_hsl_id = dhRcl_IDs_removeEps((1+dhl_hsl_id),1);
    uniqDHLAssignedHospiSignIDs = unique(dhl_hsl_id);
    uniqDHL_count = length(uniqDHLAssignedHospiSignIDs);
    unAssignedHospiSignIDs = zeros(1,hospLabelCnt);
    unAssignedHospiSignIDs(uniqDHLAssignedHospiSignIDs) = 1;
    unAssignedHospiSignIDs = find(unAssignedHospiSignIDs==0);
    disp(['hospiSignIDs that are NOT assigned deepHandIDs are ' mat2str(unAssignedHospiSignIDs) '.']);
    disp(['There are ' num2str(length(uniqDHLAssignedHospiSignIDs),'%d') ' hospiSignLabels that can be assigned to deepHandLabels according to high recall of assignment']);
    disp(['hospiSignIDs that are assigned deepHandIDs are ' mat2str(uniqDHLAssignedHospiSignIDs) '.']);
    
end

