function combinedImage = combineSummaryResults( summaryImageCells, combineSize )
    uniqDeepHand_count = size(summaryImageCells,1);
    combinedImage = zeros(combineSize*uniqDeepHand_count,combineSize*4,3);
    for i=1:uniqDeepHand_count
        rFrom = (i-1)*combineSize + 1;
        rTo = rFrom + combineSize - 1;
        for c = 1 : 4
            cFrom = (c-1)*combineSize + 1;
            cTo = cFrom + combineSize - 1;
            imToAdd = summaryImageCells{i,c};
            resizedIm = imresize(imToAdd, [combineSize combineSize]);
            combinedImage(rFrom:rTo,cFrom:cTo,:) = resizedIm;
        end
    end
end

