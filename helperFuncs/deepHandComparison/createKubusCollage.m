function createKubusCollage(imagefolder, rowCnt, colCnt, figureID, OPS)
    if ~exist('OPS', 'var') || ~isstruct(OPS)
        OPS = [];
    end
    
    orderIDs                = getStructField(OPS, 'orderIDs', []);
    selectedImNameListCell  = getStructField(OPS, 'selectedImNameListCell', '');
    saveFigInit             = getStructField(OPS, 'saveFigInit', '');
    useSubTitles            = getStructField(OPS, 'useSubTitles', true);
    zealousCropOpt          = getStructField(OPS, 'zealousCropOpt', true);
    imageType               = getStructField(OPS, 'imageType', '.png');
    resizeEachTo            = getStructField(OPS, 'resizeEachTo', []);
    fig_interpreter         = getStructField(OPS, 'fig_interpreter', 'tex'); %'none'
    disp_img_sizes          = getStructField(OPS, 'disp_img_sizes', false);
    dpi                     = getStructField(OPS, 'dpi', 600);

    if exist('selectedImNameListCell','var') && iscell(selectedImNameListCell)
        %check the names in selectedImNameListCell
        fl = selectedImNameListCell;
    else
        fl = getFileList(imagefolder,imageType,'',false);  
    end
    imageCount = length(fl);
    if ~strcmp(saveFigInit,'')
        i = 1;
        while i<=imageCount
            if startswithDG(fl{i},saveFigInit)
                fl(i) = [];
                imageCount = imageCount-1;
            else
                i = i + 1;
            end
        end
    end
    
    maxImageCountPerFrame = rowCnt*colCnt;
    imOutCnt = ceil(imageCount/maxImageCountPerFrame);
    if ~exist('orderIDs','var') || isempty(orderIDs)
        orderIDs = 1:imageCount;
    end
    figSaveNames = cell(1,imOutCnt);
    for i = 1:imOutCnt
        beg_f = (i-1)*maxImageCountPerFrame+1;
        end_f = min(i*maxImageCountPerFrame,imageCount);
        fileIDs =  beg_f:end_f;
        h = figure(figureID+i-1);
        clf;
        for f=fileIDs
            subplot(rowCnt, colCnt, f-beg_f+1);
            a = imread([imagefolder filesep fl{orderIDs(f)}]);
            if disp_img_sizes
                disp(size(a))
            end
            if ~isempty(resizeEachTo)
                a = imresize(a, resizeEachTo);
            end
            imshow(a);
            if useSubTitles
                titleStr = strrep(fl{orderIDs(f)},imageType,'');
                titleStr = strrep(titleStr, upper(imageType),'');
                ht = title(titleStr, 'Interpreter', fig_interpreter);
                %set(ht, 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center');
            end
        end
        if ~strcmpi(saveFigInit,'')
            if imOutCnt>1
                figSaveNames{1,i} = [imagefolder filesep saveFigInit '_' num2str(figureID+i,'%d') imageType];
            else
                figSaveNames{1,i} = [imagefolder filesep saveFigInit imageType];
            end
            maximizeFigureWindow(h);
            try
                %f = gcf;
                exportgraphics(h,figSaveNames{1,i},'Resolution',dpi)
            catch
                saveas(h,figSaveNames{1,i});
            end
            if zealousCropOpt
                zealousCrop( figSaveNames{1,i}, struct('overWrite', true));
            end
        end
    end
end

