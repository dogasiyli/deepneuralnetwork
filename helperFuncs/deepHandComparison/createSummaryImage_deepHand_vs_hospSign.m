function createSummaryImage_deepHand_vs_hospSign(groupedDeepHandAssignments, imagesRootFold )

    hospiSignImageFolderMain = [imagesRootFold filesep 'figures_hospisign' filesep];

    uniqDeepHandIDs = unique(groupedDeepHandAssignments(:,1));
    uniqDeepHand_count = length(uniqDeepHandIDs);
    
    imCollage_imSizeMax = [50 50];
    surroundWithColorStruct = struct('thicknessParam',0.2,'colorParam',[0 0 0]);
    
    summaryImageCells = cell(uniqDeepHand_count,4);
    
    for i=1:uniqDeepHand_count
        deepHandID = uniqDeepHandIDs(i);
        deepHandID_rows = find(groupedDeepHandAssignments(:,1)==deepHandID);
        numberOfImagesToAdd = length(deepHandID_rows);
        
        deepHandIm = imread([imagesRootFold filesep 'figures_deephand' filesep num2str(deepHandID) '.png']);
        
        deepHandIm = double(deepHandIm)./double(max(deepHandIm(:)));
        deepHandIm = deepHandIm.*255;
        deepHandIm = repmat(deepHandIm,[1 1 3]);
        
        figure(1);clf;imshow(deepHandIm);
        
        [imComb_hospSignFrames, imComb_paletteRCSize_hospSignFrames] = imCollageFuncs( 'createPalette', struct('imCnt',numberOfImagesToAdd,'channelCnt',3,'imSizeMax',2*imCollage_imSizeMax));
        [imComb_accRcl, imComb_paletteRCSize_accRcl] = imCollageFuncs( 'createPalette', struct('imCnt',numberOfImagesToAdd,'channelCnt',3,'imSizeMax',2*imCollage_imSizeMax));
        [imComb_numbers, imComb_paletteRCSize_numbers] = imCollageFuncs( 'createPalette', struct('imCnt',numberOfImagesToAdd,'channelCnt',3,'imSizeMax',2*imCollage_imSizeMax));
        for j=1:numberOfImagesToAdd
            %get the row
            theRowOfInfo = groupedDeepHandAssignments(deepHandID_rows(j),:);
            rColor = 1-theRowOfInfo(3);
            gColor = theRowOfInfo(3);
            surroundWithColorStruct.colorParam = [rColor gColor 0];
            surroundWithColorStruct.thicknessParam = 0.2*theRowOfInfo(3);
            
            %get the image to add here
            hospiSignFigID = theRowOfInfo(2);
            fl = getFileList(hospiSignImageFolderMain, '.png', num2str(hospiSignFigID,'%02d'), false);
            imHospSign  = imread([hospiSignImageFolderMain  fl{1}]);
            %figure(2);clf;imshow(imHospSign);
            
            %get accuracy and recall image here
            image_Acc = getNumberImage(ceil(theRowOfInfo(3)*100), 150, 150, [50 50]);
            image_Rcl = getNumberImage(ceil(theRowOfInfo(4)*100), 150, 150, [50 50]);
            imageAccRcl = [image_Acc zeros(size(image_Acc)) image_Rcl];
            imageAccRcl = cat(3, rColor*imageAccRcl, gColor*imageAccRcl, 0*imageAccRcl);   
            %figure(3);clf;imshow(imageAccRcl);
            
            %get assigned,labelled,total frameCount images here
            image_Ass = getNumberImage(theRowOfInfo(5), 150, 150, [50 50]);
            image_Lab = getNumberImage(theRowOfInfo(6), 150, 150, [50 50]);
            image_Tot = getNumberImage(theRowOfInfo(7), 150, 150, [50 50]);
            
            imageAssLabTot = [image_Ass zeros([50 50]) image_Lab;zeros([50 50]) image_Tot zeros([50 50])];
            imageAssLabTot = cat(3, rColor*imageAssLabTot, gColor*imageAssLabTot, zeros(size(imageAssLabTot)));   
            %figure(4);clf;imshow(imageAssLabTot);
            
            imComb_hospSignFrames = imCollageFuncs('insertIntoPalette', struct('imageID',j,'paletteRCSize',imComb_paletteRCSize_hospSignFrames,'imPalette',imComb_hospSignFrames,'imageToAdd',imHospSign,'boxSize',2*imCollage_imSizeMax,'surroundWithColorStruct',surroundWithColorStruct));
            imComb_accRcl = imCollageFuncs('insertIntoPalette', struct('imageID',j,'paletteRCSize',imComb_paletteRCSize_accRcl,'imPalette',imComb_accRcl,'imageToAdd',imageAccRcl,'boxSize',2*imCollage_imSizeMax,'surroundWithColorStruct',surroundWithColorStruct));
            imComb_numbers = imCollageFuncs('insertIntoPalette', struct('imageID',j,'paletteRCSize',imComb_paletteRCSize_numbers,'imPalette',imComb_numbers,'imageToAdd',imageAssLabTot,'boxSize',2*imCollage_imSizeMax,'surroundWithColorStruct',surroundWithColorStruct));
%             figure(5);clf;imshow(imComb_hospSignFrames);
%             figure(6);clf;imshow(imComb_accRcl);
%             figure(7);clf;imshow(imComb_numbers);
        end
        summaryImageCells{i,1} = deepHandIm;
        summaryImageCells{i,2} = imComb_hospSignFrames;
        summaryImageCells{i,3} = imComb_accRcl;
        summaryImageCells{i,4} = imComb_numbers;
        figure(5);clf;imshow(imComb_hospSignFrames);
        figure(6);clf;imshow(imComb_accRcl);
        figure(7);clf;imshow(imComb_numbers);
    end
    combineSize = 800;  
    combinedImage = combineSummaryResults(summaryImageCells, combineSize);
    figure(8);clf;imshow(combinedImage);
    imwrite(combinedImage, [imagesRootFold filesep 'combinedImage.jpeg']);
end

