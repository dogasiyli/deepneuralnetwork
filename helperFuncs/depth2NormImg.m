function nor = depth2NormImg( imDep )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    figure;
    [rc, cc] = size(imDep);
    subplot(2,1,1);
    imagesc(imDep);
    title('depth image');
    
    nor = zeros(rc, cc);
    for rowID = 2:rc-1
        for colID = 2:cc-1
            %dzdx = (imDep(y, x+1) - imDep(y, x-1)) / 2.0;
            %dzdy = (imDep(y+1, x) - imDep(y-1, x)) / 2.0;
            dzdRow = double((imDep(rowID+1, colID) - imDep(rowID-1, colID)) / 2.0);
            dzdCol = double((imDep(rowID, colID+1) - imDep(rowID, colID-1)) / 2.0);

            d = double([-dzdRow, -dzdCol, 1.0]);

            n = norm(d);
            nor(rowID, colID) = n;
        end
    end
    subplot(2,1,2);
    imagesc(nor);
    title('normals');
end

