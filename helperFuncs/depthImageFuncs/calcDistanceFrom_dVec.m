function d = calcDistanceFrom_dVec(dVec)
% dVec is a matrix [bc x N]
% bc - box count for pixel box distances count
% N is the number of images
% if a box distance is -inf it means that two images have zero magnitude
% vectors and this equality will be dismissed
    dVec(dVec==-inf) = 0;
    d = sum(dVec);
    
    %if I divide by the number of valid pixels - it may be meaningless
    %think that there are 2 images with only 1 valid box and they match
    %and think that this is the only box out of 400 boxes
    %validPixMat = double(dVec~=-inf);
    %validPixCnt = sum(validPixMat);
    %d = bsxfun(@rdivide, d, validPixCnt);
end