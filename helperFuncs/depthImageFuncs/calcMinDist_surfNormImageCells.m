function [mnDS, distMat, mxDS] = calcMinDist_surfNormImageCells(imCell01, imCell02, method, im02_M, im01_M, distMethod)
    if method==0
        distMat = calcDistMat_0_regular(imCell01, imCell02, distMethod);    
    elseif method==2
        distMat = calcDistMat_2_vectorized(imCell01, imCell02, distMethod);    
    elseif method==3
        distMat = calcDistMat_3_mapped(imCell01, imCell02, im01_M, im02_M, distMethod);    
    else
        %this is the preferred one
        distMat = calcDistMat_1_vectorized(imCell01, imCell02, im01_M, im02_M, distMethod);    
    end
    if nargout==3
        %find min and set
        [mnDS, mxDS] = getMatrixValueBounds(distMat, false);
        %mnDS.im01 = imCell01(mnDS.rowID,:);
        %mnDS.im02 = imCell02(mnDS.colID,:);
        mnDS.cropIDs = [mnDS.rowID,mnDS.colID];
        %mxDS.im01 = imCell01(mxDS.rowID,:);
        %mxDS.im02 = imCell02(mxDS.colID,:);
        mxDS.cropIDs = [mxDS.rowID,mxDS.colID]; 
    else
        mnDS = getMatrixValueBounds(distMat, false);
        mnDS.cropIDs = [mnDS.rowID,mnDS.colID];        
    end
    if mnDS.d==0
        mnDS.d = eps;
    end
end

function distMat = calcDistMat_3_mapped(imCell01, imCell02, mapCell01, mapCell02, distMethod)
    cropCnt01 = size(imCell01,1);
    cropCnt02 = size(imCell02,1);
    D_SingleImg = size(imCell01,2);
    boxCnt_SingleImg = D_SingleImg/4;
    
    tv = zeros(5,1);
    tic;
    tv(1) = toc;
    %im01_M = imCell01(mapCell01); % %? time consuming
    %tv(2) = toc;
    %im02_M = imCell02(mapCell02); % %? time consuming
    %tv(3) = toc;
    switch distMethod
        case {'cosineXYZ','c3'}
            %following gives the cosine distance using x y and z
            dVec = calcDistanceOfSurfNormPatches(imCell01(mapCell01), imCell02(mapCell02), 3); % %72 time consuming
        case {'cosineXY','c2'}
            %following gives the cosine distance using x and y, ignoring z
            dVec = calcDistanceOfSurfNormPatches(imCell01(mapCell01), imCell02(mapCell02), 2);
        otherwise
            error(['unknown distanceType(' distMethod ')']);
    end
    tv(2) = toc;
    dVec = reshape(dVec,boxCnt_SingleImg,[]); % %0 time consuming
    tv(3) = toc;
    d = calcDistanceFrom_dVec(dVec); % %1 time consuming
    tv(4) = toc;
    distMat = reshape(d,cropCnt01,cropCnt02);  % %0 time consuming
    tv(5) = toc;
    tvDist = tv(2:end)-tv(1:end-1);
    disptable([(1:length(tvDist))' map2_a_b(tvDist,0,100) tvDist])
end
function distMat = calcDistMat_2_vectorized(imCell01, imCell02, distMethod)
    cropCnt01 = size(imCell01,1);
    cropCnt02 = size(imCell02,1);
    D_SingleImg = size(imCell01,2);
    boxCnt_SingleImg = D_SingleImg/4;
    
    %I can compare a vector of images to another vector of images
    %so I can replicate imCell01's all images to compare with imCell02's
    
    %I need to replicate imCell01 images to
    %cropCnt02_1, cropCnt02_2, cropCnt02_3
    %I need to replicate imCell02 images to
    %cropCnt01*(1, 2, 3, .. ,cropCnt02)
    idx01 = repmat(1:cropCnt01,cropCnt02,1);
    idx02 = repmat(1:cropCnt02,cropCnt01,1)';
    
    %tv = zeros(9,1);
    
    %tic;
    %tv(1) = toc;
    %im01_M = zeros(boxCnt_SingleImg, 4, cropCnt01*cropCnt02);
    im01_M = reshape(imCell01(idx01(:),:)', boxCnt_SingleImg, 4, cropCnt01*cropCnt02); % %100 time consuming
    %tv(2) = toc;
    im01_M = reshape(permute(im01_M,[2,1,3]),size(im01_M,2),[])'; % %45 time consuming
    %tv(3) = toc;
    %im02_M = zeros(boxCnt_SingleImg, 4, cropCnt01*cropCnt02);
    im02_M = reshape(imCell02(idx02(:),:)', boxCnt_SingleImg, 4, cropCnt01*cropCnt02); % %87 time consuming
    %tv(4) = toc;
    im02_M = reshape(permute(im02_M,[2,1,3]),size(im02_M,2),[])'; % %38 time consuming
    %tv(5) = toc;
    switch distMethod
        case {'cosineXYZ','c3'}
            %following gives the cosine distance using x y and z
            dVec = calcDistanceOfSurfNormPatches(im01_M, im02_M, 3);
        case {'cosineXY','c2'}
            %following gives the cosine distance using x and y, ignoring z
            dVec = calcDistanceOfSurfNormPatches(im01_M, im02_M, 2);
        otherwise
            error(['unknown distanceType(' distMethod ')']);
    end
    %tv(6) = toc;
    dVec = reshape(dVec,boxCnt_SingleImg,[]); % %0 time consuming
    %tv(7) = toc;
    d = calcDistanceFrom_dVec(dVec); % %1 time consuming
    %tv(8) = toc;
    distMat = reshape(d,cropCnt01,cropCnt02);  % %0 time consuming
    %tv(9) = toc;
    %tvDist = tv(2:end)-tv(1:end-1);
    %disptable([(1:length(tvDist))' map2_a_b(tvDist,0,100) tvDist])
end
function distMat = calcDistMat_1_vectorized(imCell01, imCell02, mapCell01, mapCell02, distMethod)
    cropCnt01 = size(imCell01,1);
    cropCnt02 = size(imCell02,1);
    D_SingleImg = size(imCell01,2);
    boxCnt_SingleImg = D_SingleImg/4;
    if isempty(mapCell02)
        im02_M = reshape(imCell02', boxCnt_SingleImg, 4, cropCnt02);
        %next line is taken from
        %https://www.mathworks.com/matlabcentral/answers/295692-concatenate-vertically-along-the-3rd-dimension-of-a-matrix
        im02_M = reshape(permute(im02_M,[2,1,3]),size(im02_M,2),[])';
    else
        im02_M = imCell02(mapCell02);
    end
    distMat = zeros(cropCnt01, cropCnt02);
    for i=1:cropCnt01
        %calc distances of imCell01(i,:) against all others in imCell02
        %im01_M = repmat(imCell01(i,:),1,cropCnt02);
        %im01_M = reshape(im01_M, boxCnt_SingleImg, 4, cropCnt02);
        %im01_M = reshape(im01_M, boxCnt_SingleImg, 4, cropCnt02);
        %im01_M = reshape(permute(im01_M,[2,1,3]),size(im01_M,2),[])';
        if isempty(mapCell01)
            im01_M = reshape(permute(reshape(repmat(imCell01(i,:),1,cropCnt02), boxCnt_SingleImg, 4, cropCnt02),[2,1,3]),4,[])';
        else
            im01_M = imCell01(i,:);
            im01_M = im01_M(mapCell01);
        end
        
        switch distMethod
            case {'cosineXYZ','c3'}
                %following gives the cosine distance using x y and z
                dVec = calcDistanceOfSurfNormPatches(im01_M, im02_M, 3);
            case {'cosineXY','c2'}
                %following gives the cosine distance using x and y, ignoring z
                dVec = calcDistanceOfSurfNormPatches(im01_M, im02_M, 2);
            otherwise
                error(['unknown distanceType(' distMethod ')']);
        end
        dVec = reshape(dVec,[],cropCnt02);
        d = calcDistanceFrom_dVec(dVec);
        distMat(i,:) = d; 
    end
end
function distMat = calcDistMat_0_regular(imCell01, imCell02, distMethod)
    cropCnt01 = size(imCell01,1);
    cropCnt02 = size(imCell02,1);
    distMat = zeros(cropCnt01, cropCnt02);
    for i=1:cropCnt01
        for j=1:cropCnt02
            im01_M = reshape(imCell01{i}(i,:), [], 4);
            im02_M = reshape(imCell02{j}(j,:), [], 4);
            switch distMethod
                case {'cosineXYZ','c3'}
                    %following gives the cosine distance using x y and z
                    dVec = calcDistanceOfSurfNormPatches(im01_M, im02_M, 3)';
                case {'cosineXY','c2'}
                    %following gives the cosine distance using x and y, ignoring z
                    dVec = calcDistanceOfSurfNormPatches(im01_M, im02_M, 2)';
                otherwise
                    error(['unknown distanceType(' distMethod ')']);
            end
            d = calcDistanceFrom_dVec(dVec);
            distMat(i,j) = d;
        end
    end
end

