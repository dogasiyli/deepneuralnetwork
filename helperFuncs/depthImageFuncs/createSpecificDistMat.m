%[ distMat, medoidHist_K, clusterLabelsCell, handsStruct, confAccBestVec] = 
function [distMat, handsStruct, fileSaveName] = createSpecificDistMat(srcFold, surSlctListStruct, distMatInfoStruct, OPS)%optParamStruct
    %surSlctListStruct = struct('labelingStrategyStr','fromAssignedLabels','single0_double1', 0, 'signIDList',[7 13 49 69 74 85 86 87 88 586 593 636],'userList',3,'maxRptID',1);
    %distMatInfoStruct = struct('featureName','sn','cropMethod','sc','distanceMethod','c3');
    %featureName-<sn = surfaceNormal>,<hg = hogCrop>,<dp = depthIm>,<rg =rgbIm>
    %cropMethod-<sc = singleCrop>,<mc = multiCrop>
    %distanceMethod-<cX or eX>
    %pV = createSpecificDistMat('D:\FromUbuntu\Doga', surSlctListStruct, distMatInfoStruct);
    
    if ~exist('OPS','var')
        OPS = [];
    end
    
    hogVersion = getStructField(OPS, 'hogVersion', 1);
    enforceRecreate = getStructField(OPS, 'enforceRecreate', false);
    useMethodID = getStructField(OPS, 'useMethodID', 1);
    
    
    single0_double1 = surSlctListStruct.single0_double1;    
    signIDList = surSlctListStruct.signIDList; 
    userList = surSlctListStruct.userList; 
    maxRptID = surSlctListStruct.maxRptID;
    labelingStrategyStr = surSlctListStruct.labelingStrategyStr;
    
    featName = distMatInfoStruct.featureName;
    cropMethod = distMatInfoStruct.cropMethod;
    distMethod = distMatInfoStruct.distanceMethod;%{'c3','c2','e3','e2'}
    
    fileSaveName = createFileSaveName_DistMat(srcFold, surSlctListStruct, distMatInfoStruct, hogVersion);
    filesLoaded = false;
    
    if exist(fileSaveName.dataFile,'file')
        disp(['Loading(handsStruct, problematicVids, surSlctListStruct, distMatInfoStruct) from (' fileSaveName.dataFile ')']);
        load(fileSaveName.dataFile, 'handsStruct', 'problematicVids', 'surSlctListStruct', 'distMatInfoStruct')
        disp(['Loaded(' fileSaveName.dataFile ')']);
        filesLoaded = exist('handsStruct','var');
    end
    
    if ~filesLoaded
        if strcmpi(featName,'sn' )%& cropMethod=='sc' & distMethod=='c3'
            %surface normal features can be acquired by the following function
            [handsStruct,problematicVids] = createClusterableDataSet_sn(srcFold, signIDList, userList, maxRptID, single0_double1, labelingStrategyStr); 
            assert(isempty(problematicVids),['There are problematic videos = ' mat2str(problematicVids)]);
            %handsStruct.dataCells will have Nx1 cells, each cell will have NNx1600 feature matrix
            %handsStruct.imCropCells will have Nx1 cells, each cell will have NNx4 crop area information 
            %handsStruct.detailedLabels will have 
            if strcmpi(cropMethod,'sc')
                N = size(handsStruct.dataCells,1);
                for i=1:N
                    handsStruct.dataCells{i} = {handsStruct.dataCells{i}(1,:)};
                    handsStruct.imCropCells{i} = {handsStruct.imCropCells{i}(1,:)};
                end
            end
            disp(['Saving(handsStruct, problematicVids, surSlctListStruct, distMatInfoStruct) into (' fileSaveName.dataFile ')']);
            save(fileSaveName.dataFile, 'handsStruct', 'problematicVids', 'surSlctListStruct', 'distMatInfoStruct');
            disp(['Saved(' fileSaveName.dataFile ')']);
        elseif strcmpi(featName,'hf' )%& cropMethod=='sc' & distMethod=='c3'
            %surface normal features can be acquired by the following function
            [handsStruct,problematicVids] = createClusterableDataSet_hf(srcFold, signIDList, userList, maxRptID, single0_double1, labelingStrategyStr, hogVersion); 
            assert(isempty(problematicVids),['There are problematic videos = ' mat2str(problematicVids)]);
            %handsStruct.dataCells will have Nx1 cells, each cell will have NNx1600 feature matrix
            %handsStruct.imCropCells will have Nx1 cells, each cell will have NNx4 crop area information 
            %handsStruct.detailedLabels will have 
            if strcmpi(cropMethod,'sc')
                N = size(handsStruct.dataCells,1);
                for i=1:N
                    handsStruct.dataCells{i} = {handsStruct.dataCells{i}(:,1)}';
                    handsStruct.imCropCells{i} = {handsStruct.imCropCells{i}(1,:)}';
                end
            end
            disp(['Saving(handsStruct, problematicVids, surSlctListStruct, distMatInfoStruct) into (' fileSaveName.dataFile ')']);
            save(fileSaveName.dataFile, 'handsStruct', 'problematicVids', 'surSlctListStruct', 'distMatInfoStruct','-v7.3');
            disp(['Saved(' fileSaveName.dataFile ')']);
        end
    end
    %if strcmpi(featName,'sn')%& cropMethod=='sc' & distMethod=='c3'
    if strcmpi(cropMethod,'sc') || strcmpi(featName,'hf')
        useMethodID = 0;
    end
    stateStr = 'empty';
    n = length(handsStruct.dataCells);
    if exist(fileSaveName.distanceMatrixFile,'file')
        disp(['Loading(distMat, cropIDs, mnDS, mxDS) from (' fileSaveName.distanceMatrixFile ')']);
        load(fileSaveName.distanceMatrixFile, 'distMat', 'cropIDs', 'mnDS', 'mxDS', 'detailedLabels');
        disp(['Loaded(' fileSaveName.distanceMatrixFile ')']);
        [distMat, stateStr] = checkDistMatState( distMat ); %#ok<NODEF>
    end
    if exist('distMat','var') && size(distMat,1)~=n
        stateStr = 'notCompatible';
        enforceRecreate = true;
    end
    switch stateStr
        case {'empty', 'notSquare', 'hasNans','notCompatible'}
            OPS_createDistMat_surfNormCells = struct('enforceRecreate',enforceRecreate,'useMethodID',useMethodID,'distMethod',distMethod);
            distMat = createDistMat_surfNormCells(featName, handsStruct, fileSaveName.knownDistancesFileName, fileSaveName.distanceMatrixFile, OPS_createDistMat_surfNormCells);
    end
    %end
end