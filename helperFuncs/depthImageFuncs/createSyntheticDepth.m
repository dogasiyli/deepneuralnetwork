function [ dImWZ, dImColor ] = createSyntheticDepth( sizeMat, centerMat, minMaxVarMat, blockCnt, useGroupIDInfo)
%createSyntheticDepth crates a synthetic depth image for checking surface
%normal computations
%   sizeMat is 3 rows and 2 columns
%   centerMat is 2 rows and 2 columns
%   minMaxMat is 3 rows and 3 columns
    if ~exist('sizeMat','var') || isempty(sizeMat)
        sizeMat = [50 50;40 30;10 10];
    end
    if ~exist('centerMat','var') || isempty(centerMat)
        centerMat = [25 25;25 10];
    end
    if ~exist('minMaxVarMat','var') || isempty(minMaxVarMat)
        minMaxVarMat = [1700 2000 5; 1450 1500 3; 1250 1350 3];
    end
    if ~exist('blockCnt','var') || isempty(blockCnt)
        blockCnt = [20 20];
    end
    if ~exist('useGroupIDInfo','var') || isempty(useGroupIDInfo)
        %if useGroupIDInfo is not passed or
        %is empty or
        %then set to false
        useGroupIDInfo = false;
    end

    %create the templates
    backGrnd_01 = minMaxVarMat(1,3)*randn(sizeMat(1,:));
    torso_02 = minMaxVarMat(2,3)*randn(sizeMat(2,:));
    hand_01 = fspecial('gaussian',sizeMat(3,:), minMaxVarMat(3,3));

    %set min max values
    backGrnd_01(:) = map2_a_b(backGrnd_01(:), minMaxVarMat(1,1), minMaxVarMat(1,2));
    torso_02(:)    = map2_a_b(torso_02(:)   , minMaxVarMat(2,1), minMaxVarMat(2,2));
    hand_01(:)     = minMaxVarMat(3,1) + minMaxVarMat(3,2) - map2_a_b(hand_01(:)    , minMaxVarMat(3,1), minMaxVarMat(3,2));

    [bb_torso, fitsIn_torso] = getBoundBox(sizeMat(1,:), sizeMat(2,:), centerMat(1,:));
    [bb_hand, fitsIn_hand] = getBoundBox(sizeMat(1,:), sizeMat(3,:), centerMat(2,:));

    backGrnd_01(bb_torso(1):bb_torso(2),bb_torso(3):bb_torso(4)) = torso_02;

    diskFilter = fspecial('disk',floor(max(sizeMat(3,:)/2)-1));
    bb_palm = getBoundBox(sizeMat(3,:), size(diskFilter));
    handSlct = hand_01(bb_palm(1):bb_palm(2),bb_palm(3):bb_palm(4));
    handSlct(diskFilter<eps) = 0;

    handArea =  backGrnd_01(bb_hand(1):bb_hand(2),bb_hand(3):bb_hand(4));
    
    palmArea = handArea(bb_palm(1):bb_palm(2),bb_palm(3):bb_palm(4));
    palmArea(handSlct>eps) = handSlct(handSlct>eps);
    handArea(bb_palm(1):bb_palm(2),bb_palm(3):bb_palm(4)) = palmArea;
    
    backGrnd_01(bb_hand(1):bb_hand(2),bb_hand(3):bb_hand(4)) = handArea;

    [dImWZ, dImColor, pixelGroupIDs, grpMedian, fillHist] = reAdjustDepthImHand(backGrnd_01, 3);
    figID = 217;
    rc = 3;
    cc = 2;
    subplotVec = [rc cc 1 2 3 4];
    plotFigs(backGrnd_01, dImWZ, dImColor, pixelGroupIDs, figID, subplotVec);
    subPlotID = 5;
    if ~useGroupIDInfo
        pixelGroupIDs = [];
    end
    
    optionalParamsStruct = struct('figVec',[figID rc cc subPlotID 3 2],'viewPoint', [0 90]);
    exploreImportantBlocks(dImWZ, pixelGroupIDs, blockCnt, optionalParamsStruct);
end

function plotFigs(cropIm, cropImFilled, imColor, grpIDs, figID, subplotVec)
    figure(figID);clf; hold on;

    subplot(subplotVec(1), subplotVec(2), subplotVec(3));
    imagesc(cropIm);colorbar;
    set(gca,'XTick',[1 size(cropIm,2)]);
    set(gca,'YTick',[1 size(cropIm,1)]);
    title('greyIm');
    
    subplot(subplotVec(1), subplotVec(2), subplotVec(4));
    imagesc(cropImFilled);colorbar;
    set(gca,'XTick',[1 size(cropIm,2)]);
    set(gca,'YTick',[1 size(cropIm,1)]);
    title('0s filled');

    subplot(subplotVec(1), subplotVec(2), subplotVec(5));
    image(imColor);
    set(gca,'XTick',[1 size(cropIm,2)]);
    set(gca,'YTick',[1 size(cropIm,1)]);
    
    subplot(subplotVec(1), subplotVec(2), subplotVec(6));
    hist(grpIDs(:),1:3);
    set(gca,'YTick',[]);
    set(gca,'XTickLabel',{'Hand-Red','Body-Green','Bckgrnd-Blue'});
    xtickangle(45);
end

