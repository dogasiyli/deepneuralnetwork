function gridFitStruct = depthImTogridFitStruct(depthImFilled_gridFit)
    [rowCount_y,colCount_x] = size(depthImFilled_gridFit);  
    gridFitStruct = struct;
    gridFitStruct.x = 1:colCount_x;
    gridFitStruct.y = 1:rowCount_y;
    gridFitStruct.z = depthImFilled_gridFit;
end