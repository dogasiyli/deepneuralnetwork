function [faceNorms, quiverMat, rgbImgAsFaceNorms] = exploreImportantBlocks(cropImFilled, pixelGroupIDs, blockCnts, optionalParamsStruct, useGroupIDInfo)
    vwp = getOptionalParamsFromStruct(optionalParamsStruct, 'viewPoint', [0 90], true);
    normCalcMethod = getOptionalParamsFromStruct(optionalParamsStruct, 'normCalcMethod', 'haar-like', true);
    figVec = getOptionalParamsFromStruct(optionalParamsStruct, 'figVec', []);
    removeFromDisplayTresh = getOptionalParamsFromStruct(optionalParamsStruct, 'removeFromDisplayTresh', []);
    if ~exist('useGroupIDInfo','var') || isempty(useGroupIDInfo) || length(useGroupIDInfo)<3
        %if useGroupIDInfo is not passed or
        %is empty or
        %is false
        %then do not use group info
        useGroupIDInfo = [1 0 0];
    end  
    if useGroupIDInfo(1)==1
        rejectPatchMethod = 'none';
        rejectStr = 'use every patch';
    elseif useGroupIDInfo(2)==1
        rejectPatchMethod = 'includeBackground';
        rejectStr = 'reject if patch includes bacground';
    else%if useGroupIDInfo(3)==1
        rejectPatchMethod = 'notIncludeHand';
        rejectStr = 'reject if patch doesnt include hand';
    end
    
    rgbImgAsFaceNorms_figID =[];
    if ~isempty(figVec) && figVec(1)>0
        rgbImgAsFaceNorms_figID = figVec(4)+88;
    end    
    OPS = struct('normCalcMethod',normCalcMethod,'rejectPatchMethod',rejectPatchMethod,'pixelGroupIDs',pixelGroupIDs,'removeFromDisplayTresh',removeFromDisplayTresh,'rgbImgAsFaceNorms_figID',rgbImgAsFaceNorms_figID);
    [faceNorms, quiverMat, blockAreaSize, rgbImgAsFaceNorms] = calcFaceNormsOfImage( depthImg, blockCnts, OPS);
    
    if ~isempty(figVec) && figVec(1)>0
        titleParams = struct('blockCnts',blockCnts,'blockAreaSize',blockAreaSize,'rejectPatchMethod',rejectPatchMethod,'rejectStr',rejectStr);
        p_LineWidth = 1;
        p_MarkerSize = 1;
        if length(figVec)==6
            p_LineWidth = figVec(5);
            p_MarkerSize= figVec(6);  
        end
        visualizeNormals(cropImFilled, quiverMat, titleParams, struct('vwp', vwp,'figVec', figVec, 'p_LineWidth', p_LineWidth, 'p_MarkerSize', p_MarkerSize));
    end
end
% 
% function plotSurfNorms(imForSC, plotPoints, faceNorms)
%     figure(121);clf;
%     subplot(2,1,1);
%     imagesc(imForSC);
%     
%     subplot(2,1,2);
%     ptCnt = size(faceNorms,1);
%     for i=1:ptCnt
%         if 
%     end
% end
