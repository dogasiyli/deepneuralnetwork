function [cropImFilled, imColor, grpIDs, grpMedian, fillHist] = reAdjustDepthImHand(cropIm, boxSur)
    [cropImFilled, fillHist]= fill0s(cropIm, boxSur);
    [imColor, grpIDs, grpMedian] = setRGBKmeans(cropImFilled);
end