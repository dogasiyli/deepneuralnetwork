function [M2, M2_ks, M3, M1] = dispStateOfFiles_hospiSign(optParamStruct)
    %'\\DOGAMSILAPTOP\DgMsiLapD\FromUbuntu\Doga'
    %'C:\FromUbuntu\HospiSign\HospiMOV'
    %go through folders in srcFold
    %if the folder exists, go into the folder
    %check hogCropMat and surfNormCropMat existence
    %    
    if ~exist('optParamStruct','var')
        optParamStruct = [];
    end
    
    srcFold = getStructField(optParamStruct, 'srcFold', getVariableByComputerName('srcFold'));
    backupFoldRoot = getStructField(optParamStruct, 'backupFold', getVariableByComputerName('backupFold'));
    signIDList = getStructField(optParamStruct, 'signIDList', [7,13,49,69,74,85,86,87,88,89,96,112,125,221,222,247,269,273,277,278,290,296,310,335,339,353,396,403,404,424,428,450,521,533,535,536,537,583,586,593,636]);
    userList = getStructField(optParamStruct, 'userList', 2:7);%2:5??
    printOut = getStructField(optParamStruct, 'printOut', false);
    maxRowPrint = getStructField(optParamStruct, 'maxRowPrint', 120);
    
    disp(['Source folder used = ' srcFold]);
    disp(['Backup folder used = ' backupFoldRoot]);
    
    M1_s = getFromSrcFold(srcFold, signIDList, userList);
    M2_s = get_M2_from_M1(M1_s);
    %M3_s = get_M3_from_M1(M1_s);
    
    M1_b = getFromBackupFold(backupFoldRoot, signIDList, userList);
    M2_b = get_M2_from_M1(M1_b);
    %M3_b = get_M3_from_M1(M1_b);
    
    M2 = combine_M2(M2_s, M2_b);
    M2_ks = getKnownSigns(M2);
    M1 = combine_M1(M1_s, M1_b);
    M3 = get_M3_from_M1(M1);
    
    if exist('printOut','var') && printOut
        disp('0(not exist) - 1(sourceExist) - 2(backupExist) - 3(bothExist)');
        if size(M1,1)<=maxRowPrint
            disptable(M1, 'signID|userID|repitionID|hogCropMatSize|surfNormCropMatSize|LH|RH|BH');
        end
        if size(M2,1)<=maxRowPrint
            disptable(M2, 'signID|foldCnt_s|hfCrCnt_s|snCrCnt_s|foldCnt_b|hfCrCnt_b|snCrCnt_b');
        end
        if size(M3,1)<=maxRowPrint
            disptable(M3, 'signID|userID|folderCount_user|hogCropMatCount_user|surfNormCropMat_user');
        end
    end
end

function M2_ks = getKnownSigns(M2)%knownSigns
    expectedSignList = [7;13;49;69;74;85;86;87;88;89;96;112;125;221;222;247;269;273;277;278;290;296;310;335;339;353;396;403;404;424;428;450;521;533;535;536;537;583;586;593;636];
    M2_ks = zeros(length(expectedSignList),size(M2,2));
    M2_ks(:,1) = expectedSignList;
    for i=1:size(M2,1)
        rowVals = M2(i,:);
        signID = rowVals(1);
        whichRowInd = find(expectedSignList==signID);
        if ~isempty(whichRowInd)
            M2_ks(whichRowInd,:) = rowVals;
        end
    end
end

function M2 = combine_M2(M2_s, M2_b)
    signIDList = unique([M2_s(:,1);M2_b(:,1)]);
    M2 = zeros(length(signIDList),7);
    for i=1:length(signIDList)
        signID = signIDList(i);
        M2_s_c = M2_s(M2_s(:,1)==signID,2:end);
        if isempty(M2_s_c)
            M2_s_c = zeros(1,3);
        end
        M2_b_c = M2_b(M2_b(:,1)==signID,2:end);
        if isempty(M2_b_c)
            M2_b_c = zeros(1,3);
        end
        M2(i,:) = [signID M2_s_c M2_b_c];
    end
end

function M1 = combine_M1(M1_s, M1_b)
    signIDList = unique([M1_s(:,1);M1_b(:,1)]);
    M1 = [];
    for i=1:length(signIDList)
        signID = signIDList(i);
        M1_s_c = M1_s(M1_s(:,1)==signID,:);
        M1_b_c = M1_b(M1_b(:,1)==signID,:);
        userIDList = unique([M1_s_c(:,2);M1_b_c(:,2)]);
        for j=1:length(userIDList)
            userID = userIDList(j);
            M1_s_c_u = M1_s_c(M1_s_c(:,2)==userID,:);
            M1_b_c_u = M1_b_c(M1_b_c(:,2)==userID,:);
            repeatIDList = unique([M1_s_c_u(:,3);M1_b_c_u(:,3)]);
            for k=1:length(repeatIDList)
               repID = repeatIDList(k);
               M1_s_c_u_r = M1_s_c_u(M1_s_c_u(:,3)==repID,:);
               if isempty(M1_s_c_u_r)
                   vals_s = zeros(1,8);
               else
                   vals_s = M1_s_c_u_r;
               end
               M1_b_c_u_r = M1_b_c_u(M1_b_c_u(:,3)==repID,:);
               if isempty(M1_b_c_u_r)
                   vals_b = zeros(1,8);
               else
                   vals_b = M1_b_c_u_r;
               end
               
               hogCropMatSize = getCombinedVal(vals_s(1,4), vals_b(1,4));
               surfNormCropMatSize = getCombinedVal(vals_s(1,5), vals_b(1,5));
               LH = getCombinedVal(vals_s(1,6), vals_b(1,6));
               RH = getCombinedVal(vals_s(1,7), vals_b(1,7));
               BH = getCombinedVal(vals_s(1,8), vals_b(1,8));
               M1_add = [signID userID repID hogCropMatSize surfNormCropMatSize LH RH BH];
               M1 = [M1;M1_add];
            end
        end
    end
end

function combinedVal = getCombinedVal(val_s, val_b)
    if (val_s==0 && val_b==0)
        combinedVal = 0;
    elseif (val_s>0 && val_b==0)
        combinedVal = 1;
    elseif (val_s==0 && val_b>0)
        combinedVal = 2;
    else%if (val_s>0 && val_b>0)
        combinedVal = 3;
    end
end

function M2 = get_M2_from_M1(M1)
    %Table1 columns will have [M1]
    %1. signID
    %2. userID
    %3. repitionID
    %4. hogCropMat size in mb if exists
    %5. surfNormCropMat size in mb if exists
    %[signID userID repitionID hogCropMatSize surfNormCropMatSize];

    %Table2 columns will have [M2]
    %1. signID
    %2. folderCount
    %3. hogCropMat count
    %4. surfNormCropMat count
    %[signID folderCount_sign hogCropMatCount_sign surfNormCropMat_sign];
    uniqSigns = unique(M1(:,1));
    M2 = zeros(length(uniqSigns),4);
    for i = 1:length(uniqSigns)
        signID = uniqSigns(i);
        M1_s = M1(M1(:,1)==signID,:);
        folderCount_sign = size(M1_s,1);
        hogCropMatCount_sign = sum(M1_s(:,4)>0);
        surfNormCropMat_sign = sum(M1_s(:,5)>0);
        M2(i,:) = [signID folderCount_sign hogCropMatCount_sign surfNormCropMat_sign];
    end
end

function M3 = get_M3_from_M1(M1)
    %Table1 columns will have [M1]
    %1. signID
    %2. userID
    %3. repitionID
    %4. hogCropMat size in mb if exists
    %5. surfNormCropMat size in mb if exists
    %[signID userID repitionID hogCropMatSize surfNormCropMatSize];

    %Table3 columns will have [M3]
    %1. signID
    %2. userID
    %3. folderCount
    %4. hogCropMat count
    %5. surfNormCropMat count
    %[signID userID folderCount_user hogCropMatCount_user surfNormCropMat_user];
                
    uniqSigns = unique(M1(:,1));
    M3 = zeros(length(uniqSigns),5);
    k = 1;
    for i = 1:length(uniqSigns)
        signID = uniqSigns(i);
        M1_s = M1(M1(:,1)==signID,:);
        uniqUsers = unique(M1_s(:,2));
        for j = 1:length(uniqUsers)
            userID = uniqUsers(j);
            M1_su = M1_s(M1_s(:,2)==userID,:);
            folderCount_user = size(M1_su,1);
            hogCropMatCount_user = sum(M1_su(:,4)>0);
            surfNormCropMat_user = sum(M1_su(:,5)>0);
            M3(k,:) = [signID userID folderCount_user hogCropMatCount_user surfNormCropMat_user];
            k = k + 1;
        end
    end
end

function M1 = getFromBackupFold(backupFoldRoot, signIDList, userList)
    M1 = [];
    backupFolder_feats = getExactBackupFolder(backupFoldRoot, 'Feats');
    backupFolder_labels = getExactBackupFolder(backupFoldRoot, 'Labels');
    backupSubFoldersLabelsAll = getFolderList(backupFolder_labels, false, true);
    
    labelFilesList = zeros(size(backupSubFoldersLabelsAll,1),6);
    for i=1:size(backupSubFoldersLabelsAll,1)
        fullFileName = backupSubFoldersLabelsAll{i,1};
        initCharInds = strfind(fullFileName,'labelFiles_');
        endOfFileName = fullFileName(11+initCharInds:end);
        [s,u,r] = getSURFromStr(endOfFileName, '_');
        [~, BH] = getFileList(fullFileName, 'CS_BH.txt', '');
        [~, LH] = getFileList(fullFileName, 'CS_LH.txt', '');
        [~, RH] = getFileList(fullFileName, 'CS_RH.txt', '');
        labelFilesList(i,:) = [s,u,r,LH,RH,BH];
    end
    
    for signID = signIDList      
        for userID = userList
            hog_crop_FileName_init = ['hog_crop_s' num2str(signID,'%03d') '_u' num2str(userID)];%'_r' num2str(repID,'%02d')
            surfNorm_crop_FileName_init = ['surfNorm_crop_s' num2str(signID,'%03d') '_u' num2str(userID)];%'_r' num2str(repID,'%02d')
            
            [fileNames_hf, numOfFiles_hf] = getFileList(backupFolder_feats,'.mat', hog_crop_FileName_init);
            [fileNames_sn, numOfFiles_sn] = getFileList(backupFolder_feats,'.mat', surfNorm_crop_FileName_init);
            labelFilesList_cur = labelFilesList(labelFilesList(:,1)==signID & labelFilesList(:,2)==userID,3:end);
            %go over the lists
            %find related files
            %sum the counts
            
            rMeaningful = zeros(1,20);
            M1_add = [repmat([signID userID], 20,1) (1:20)' zeros(20,5)];
            
            %Table1 columns will have [M1]
            %1. signID
            %2. userID
            %3. repitionID
            %4. hogCropMat size in mb if exists
            %5. surfNormCropMat size in mb if exists
            %6. LH
            %7. RH
            %8. BH
            %[signID userID repitionID hogCropMatSize surfNormCropMatSize LH RH BH];

            for i=1:numOfFiles_hf
                fullFileName = fileNames_hf{i,1};
                endOfFileName = fullFileName(10:end-4);
                [s,u,r] = getSURFromStr(endOfFileName, '_');
                assert(s==signID && u==userID,'mismatch');
                srcAtt = dir([backupFolder_feats filesep fullFileName]);
                hogCropMatSize = srcAtt.bytes;
                M1_add(r,4) = hogCropMatSize;
                rMeaningful(r) = 1;
            end
            for i=1:numOfFiles_sn
                fullFileName = fileNames_sn{i,1};
                endOfFileName = fullFileName(15:end-4);
                [s,u,r] = getSURFromStr(endOfFileName, '_');
                assert(s==signID && u==userID,'mismatch');
                srcAtt = dir([backupFolder_feats filesep fullFileName]);
                surfNormCropMatSize = srcAtt.bytes;
                M1_add(r,5) = surfNormCropMatSize;
                rMeaningful(r) = 1;
            end
            for i=1:size(labelFilesList_cur,1)
                r = labelFilesList_cur(i,1);
                M1_add(r,[6 7 8]) = labelFilesList_cur(i,2:end);
                rMeaningful(r) = 1;
            end
            
            M1_add = M1_add(rMeaningful==1,:);
            M1 = [M1; M1_add];
        end
    end
end

function [s,u,r] = getSURFromStr(s03d_u01d_r02d, delim)
    try
            x = split(s03d_u01d_r02d,delim);

            s = split(x(1,:),'s');
            s = str2double(s(2,:));

            u = split(x(2,:),'u');
            u = str2double(u(2,:));

            r = split(x(3,:),'r');
            r = str2double(r(2,:));
    catch
            x = strsplit(s03d_u01d_r02d,delim);

            s = strsplit(x{1},'s');
            s = str2double(s{2});

            u = strsplit(x{2},'u');
            u = str2double(u{2});

            r = strsplit(x{3},'r');
            r = str2double(r{2});        
    end
end

function M1 = getFromSrcFold(srcFold, signIDList, userList)
    M1 = [];
    for signID = signIDList
        user_folder = [srcFold filesep num2str(signID)];
        if ~exist(user_folder,'dir')
            continue
        end        
        for userID = userList             
            for repID = 1:20
                user_folder = [srcFold filesep num2str(signID) filesep 'User_' num2str(userID) '_' num2str(repID) filesep];
                if ~exist(user_folder,'dir')
                    continue
                end
                
                %Table1 columns will have [M1]
                %1. signID
                %2. userID
                %3. repitionID
                %4. hogCropMat size in mb if exists
                %5. surfNormCropMat size in mb if exists
                %[signID userID repitionID hogCropMatSize surfNormCropMatSize];
                
                if exist([user_folder 'hog_crop.mat'],'file')
                    srcAtt = dir([user_folder 'hog_crop.mat']);
                    hogCropMatSize = srcAtt.bytes;
                else
                    hogCropMatSize = 0;                    
                end
                if exist([user_folder 'surfNorm_crop.mat'],'file')
                    srcAtt = dir([user_folder 'surfNorm_crop.mat']);
                    surfNormCropMatSize = srcAtt.bytes;
                else
                    surfNormCropMatSize = 0;                    
                end
                labelsFolder = [user_folder 'labelFiles' filesep];
                labelExistVec = zeros(1,3);
                if exist(labelsFolder,'dir')
                    labelExistVec(1) = double(exist([labelsFolder 'labelsCS_LH.txt'],'file')==2);
                    labelExistVec(2) = double(exist([labelsFolder 'labelsCS_RH.txt'],'file')==2);
                    labelExistVec(3) = double(exist([labelsFolder 'labelsCS_BH.txt'],'file')==2);
                end
                M1 = [M1; signID userID repID hogCropMatSize surfNormCropMatSize labelExistVec];
            end
        end
    end
end