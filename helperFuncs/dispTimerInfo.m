function [ startTime, prevTime, possibleEndTime, curTime, str2ret ] = dispTimerInfo( startTime, prevTime, curIdx, totalIdx, dispLevel)
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here
    possibleEndTime = []; 
    if curIdx==1
        %first time this function is called
        tic;
        startTime = toc;
        prevTime = startTime;
        str2ret = ['--startTime(' datestr(datetime('now')) ')'];
    end
    curTime = toc;    
    if curIdx~=1
        tocCurStr = getTimeAsString(curTime-prevTime);
        tocTillNowStr = getTimeAsString(curTime);
        iterCntOver = curIdx-1;
        iterCntRemain = (totalIdx-curIdx);
        expSec = iterCntRemain*(curTime/iterCntOver);
        possibleEndTime = getTimeAsString(expSec);
        try
            whenToEnd = datestr(datetime('now')+expSec/86400);
        catch
            whenToEnd = datestr(now+expSec/86400);
        end
        if dispLevel==1
            disp(['--till(' whenToEnd '),tocCur(' tocCurStr '),tillNow(' tocTillNowStr '),expTime(' possibleEndTime ')']);
            str2ret = ['--till(' whenToEnd '),tocCur(' tocCurStr '),tillNow(' tocTillNowStr '),expTime(' possibleEndTime ')'];
        elseif dispLevel==10
            str2ret = ['--till(' whenToEnd '),tocCur(' tocCurStr '),tillNow(' tocTillNowStr '),expTime(' possibleEndTime ')'];
        elseif dispLevel==11
            str2ret = ['i' num2str(curIdx) ' of ' num2str(totalIdx) ' is completed--tocCur(' tocCurStr '),tillNow(' tocTillNowStr '),expTime(' possibleEndTime ')'];
        elseif dispLevel>1
            disp(['i' num2str(curIdx) ' of ' num2str(totalIdx) ' is completed--tocCur(' tocCurStr '),tillNow(' tocTillNowStr '),expTime(' possibleEndTime ')']);
            str2ret = ['i' num2str(curIdx) ' of ' num2str(totalIdx) ' is completed--tocCur(' tocCurStr '),tillNow(' tocTillNowStr '),expTime(' possibleEndTime ')'];
        end
    end
    prevTime = curTime;
end