function varSize = dispVariableSize( variable, variableName, displayVarSizeBool )
    if (~exist('displayVarSizeBool','var') || isempty(displayVarSizeBool))
        displayVarSizeBool = false;
    end
    b = whos('variable');
    varSize = b.bytes/(1024*1024);
    if (displayVarSizeBool)
        disp([variableName ' uses(' num2str(varSize,'%4.2f') ' MB)']);
    end
end

