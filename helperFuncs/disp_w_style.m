function disp_w_style(str_to_disp, styleStr, underlined)
    if ~exist('styleStr','var')
        styleStr = 'cyan';
    end
    if ~exist('underlined','var')
        underlined = false;
    end
    if underlined && ischar(styleStr) && ~strcmpi(styleStr(1),'_')
        styleStr = ['_' styleStr];
    elseif underlined && isnumeric(styleStr) && sum(styleStr)>0
        styleStr = -styleStr;
    end
    cprintf(styleStr, [str_to_disp ' '])
    disp(' ')
end

