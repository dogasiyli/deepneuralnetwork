function displayErrorMessage(err)
    disp(['error occured. err(' err.message ')'])
    for i=length(err.stack):-1:1
        displayStructureSummary(['err.stack(' num2str(i) ')'],err.stack(i),2);
    end
end

