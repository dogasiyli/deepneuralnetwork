function [ freemem ] = displayFreeMemLinux()
    [r,w] = unix('free | grep Mem');
    stats = str2double(regexp(w, '[0-9]*', 'match'));
    memsize = stats(1)/1e6;
    freemem = (stats(3)+stats(end))/1e6;
    disp([num2str(freemem) ' free out of ' num2str(memsize)])
end

