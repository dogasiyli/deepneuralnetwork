function displayStructureSummary(structName, structToSummarize, recurCnt)
%displayStructureSummary A struct can have many field types. This function
%will print what is there in the struct in a formatted way
    if ~exist('recurCnt','var') || isempty(recurCnt)
        recurCnt = 1;%to give empty spaces inFront
    end
    if recurCnt>12
        disp('Recursion-tree is too big to follow');
    end
    try
        if isempty(structToSummarize) || ~isstruct(structToSummarize)
            disp([structName ' is not a struct or is empty']);
        end
        namesOFields = fieldnames(structToSummarize);
        fieldCount = size(namesOFields,1);
        disp([repmat('.',1,recurCnt) structName ' has (' num2str(fieldCount) ') fields']);
        for f = 1:fieldCount
            curParam = getfield(structToSummarize, namesOFields{f}); %#ok<GFLD>
            classOfParam = class(curParam);
            if isstruct(curParam)
                disp([repmat('.',1,recurCnt+1) 'field(' num2str(f) '-)' namesOFields{f} ' is a struct']);
                displayStructureSummary(structToSummarize, recurCnt+3);
            elseif ischar(curParam)
                disp([repmat('.',1,recurCnt+1) 'field(' num2str(f) ')-' namesOFields{f} ' = ' curParam '<type[char]>']);
            elseif isnumeric(curParam)
                sizeOfParam = size(curParam);
                elementCnt = prod(sizeOfParam);
                if elementCnt==1
                    disp([repmat('.',1,recurCnt+1) 'field(' num2str(f) '-' namesOFields{f} ') = ' num2str(curParam)]);  
                elseif min(sizeOfParam)==1 && length(sizeOfParam)==2
                    %a vector
                    disp([repmat('.',1,recurCnt+1) 'field(' num2str(f) '-' namesOFields{f} ') is a vector of size' mat2str(sizeOfParam)]);
                    if max(sizeOfParam)>20
                        %display first 20
                        disp([repmat('.',1,recurCnt+2) namesOFields{f} '(1:20) = ' mat2str(reshape(curParam,1,[]))]);
                    else
                        %display the vector
                        disp([repmat('.',1,recurCnt+2) namesOFields{f} ' = ' mat2str(reshape(curParam,1,[]))]);
                    end
                else
                    disp([repmat('.',1,recurCnt+1) 'field(' num2str(f) '-' namesOFields{f} ') is a multiDimensionalArray of size(' mat2str(sizeOfParam) ') and type(' classOfParam ')']);                      
                end
            elseif isstring(curParam)
                disp([repmat('.',1,recurCnt+1) 'field(' num2str(f) '-' namesOFields{f} ') = ' curParam '<type[string]>']);
            elseif istable(curParam)
                disp([repmat('.',1,recurCnt+1) 'field(' num2str(f) '-' namesOFields{f} ') = <type[table]> : ']);
                disp(curParam)
            else
                disp([repmat('.',1,recurCnt+1) 'field(' num2str(f) '-' namesOFields{f} ') is type(' classOfParam ')']);  
            end
        end
    catch err
        disp(['Error occured while trying to display struct info err(' err.message ')']);
    end
end

