function [ dMVec, dMQuant ] = distMatQuantize_hospiSign( dMVec, dMQuant, dispWarnMsg)
%distMatQuantize We have a way of saving distance of 2 frames. Normally it
%can be stored in a 14 column vector. but it can also be saved in 1 index
%that represents which 2 images and 3 distance information (distance, cropIJ and cropJI)
%   a known distance vector is : 
%   [cols 1- 5] - im1(signID,userID,repeatID,l1_r2_b3,frameID) 
%   [cols 6-10] - im2(signID,userID,repeatID,l1_r2_b3,frameID) 
%   [col 11] - distanceType Information (surfaceNormal-mc-1, surfaceNormal-sc-2, ...)
%   [col 12] - distance Value
%   [col 13] - cropID_ij
%   [col 14] - cropID_ji

%   an unsigned integer can be max = 18,446,744,073,709,551,615 - 20 digits
%   hence a double can also reach this value
%   a unique identifier for 1 image representation can be :
%   URHSSSFFF - U*10^8 + R*10^7 + H*10^6 + S*10^3 + F - which gives a 9 digit double
%   so if we concetanate these two into small_ij,bigger_ij - gives a 18 digit double
%   hence we can save 10 columns into 1

    if ~exist('dispWarnMsg','var')
        dispWarnMsg = false;
    end
    if isempty(dMQuant) && isempty(dMVec)
        %this is for testing the function on a computer
        %[cols 1- 5] - im1(7,2,5,2,9) next frame im2(13,3,7,1,100)
        %[cols 6-10] - im2(13,3,7,1,99) next frame im1(7,2,5,2,10)
        %[col 11] - 1 distanceType Information (surfaceNormal-mc-1, surfaceNormal-sc-2, ...)
        %[col 12] - distance Value
        %[col 13] - 58
        %[col 14] - 65
        dMVec = [7,2,5,2,9,13,3,7,1,99,1,-205.413,58,65   ;   13,3,7,1,100,7,2,5,2,10,1,+88.413,11,22];
        dMQuant = getQuantFromVec(dMVec);
        dMVecCheck = getVecFromQuant(dMQuant);
    elseif isempty(dMQuant)
        dMQuant = getQuantFromVec(dMVec,dispWarnMsg);
    elseif isempty(dMVec)
        dMVec = getVecFromQuant(dMQuant);
    else
        error('what do you expect this function to do?');
    end
end

function dMVecCheck = getVecFromQuant(dMQuant)
    r = size(dMQuant.frameInfo,1);
    c = size(dMQuant.additionalCols,2);
    dMQuantPart1 = dMQuant.frameInfo;
    dMVecCheck = zeros(r,c+10);
    if c>1
        dMVecCheck(:,11:end) = dMQuant.additionalCols;
    end
    [dMVecCheck(:,10), dMQuantPart1] = getPartOfQuantization(dMQuantPart1, 3);%col10 - image2-frameID 16-17-18
    [dMVecCheck(:, 6), dMQuantPart1] = getPartOfQuantization(dMQuantPart1, 3);%col06 - image2-signID 13-14-15
    [dMVecCheck(:, 9), dMQuantPart1] = getPartOfQuantization(dMQuantPart1, 1);%col09 - image2-l1_r2_b3 12
    [dMVecCheck(:, 8), dMQuantPart1] = getPartOfQuantization(dMQuantPart1, 1);%col08 - image2-repeatID 11
    [dMVecCheck(:, 7), dMQuantPart1] = getPartOfQuantization(dMQuantPart1, 1);%col07 - image2-userID 10
    [dMVecCheck(:, 5), dMQuantPart1] = getPartOfQuantization(dMQuantPart1, 3);%col05 - image1-frameID 7-8-9 [371013100]
    [dMVecCheck(:, 1), dMQuantPart1] = getPartOfQuantization(dMQuantPart1, 3);%col01 - image1-signID 4-5-6
    [dMVecCheck(:, 4), dMQuantPart1] = getPartOfQuantization(dMQuantPart1, 1);%col04 - image1-l1_r2_b3 3
    [dMVecCheck(:, 3), dMQuantPart1] = getPartOfQuantization(dMQuantPart1, 1);%col03 - image1-repeatID 2
    [dMVecCheck(:, 2), dMQuantPart1] = getPartOfQuantization(dMQuantPart1, 1);%col02 - image1-userID 1
    
    assertValZero = sum(dMQuantPart1);
    assert(assertValZero==0,['the value(' num2str(assertValZero ) ') must be zero']);
end

function [acquiredVal, dMQuantValueRemain] = getPartOfQuantization(dMQuantValue, modVal)
    strVals = num2str(dMQuantValue);
    acquiredVal = uint64(str2num(strVals(:,end-modVal+1:end)));
    dMQuantValueRemain = uint64((dMQuantValue-acquiredVal)./10^modVal);
    %dMQuantValueRemain = floor(dMQuantValue./10^modVal);
    %acquiredVal = dMQuantValue - 10^modVal*dMQuantValueRemain;
end

function dMQuant = getQuantFromVec(dMVec,dispWarnMsg)
    %userID - repeatID - lh1_rh2_bh3 - signID - frameID
    s1 = dMVec(:,2)*10^8 + dMVec(:,3)*10^7 + dMVec(:,4)*10^6 + dMVec(:,1)*10^3 + dMVec(:,5);
    s2 = dMVec(:,7)*10^8 + dMVec(:,8)*10^7 + dMVec(:,9)*10^6 + dMVec(:,6)*10^3 + dMVec(:,10);
    %no values can match here
    numOfMatching = sum(s1==s2);
    if numOfMatching~=0
        idx = find(s1==s2);
        if dispWarnMsg
            warning(['There are(' num2str(numOfMatching) ') matching distance entries and they are removed']);
        end
        disptable(dMVec(idx,:));
        dMVec(idx,:) = [];
    end
    %if s1 is smaller it goes to slot 1
    %if s2 is smaller it goes to slot 1
    s1Front = find(s1<s2);
    s2Front = find(s2<s1);
    
    if ~isempty(s2Front) && dispWarnMsg
        disp(['Some rows have the second images in the wrong order!! <' num2str(length(s2Front)) '>']);
    end
    
    [r,c] = size(dMVec);
    dMQuant = struct;
    dMQuant.frameInfo =zeros(r,1,'uint64');
    dMQuant.additionalCols = zeros(r,c-10);
    dMQuant.frameInfo(s1Front,:) = uint64(s1(s1Front))*10^9 + uint64(s2(s1Front));
    dMQuant.frameInfo(s2Front,:) = uint64(s2(s2Front))*10^9 + uint64(s1(s2Front));
    if c>10
        dMVecAdd = dMVec(:,11:end);
        dMQuant.additionalCols(s1Front,:) = dMVecAdd(s1Front,:);
        dMQuant.additionalCols(s2Front,:) = dMVecAdd(s2Front,:);
    else
        dMQuant.additionalCols = [];
    end
end