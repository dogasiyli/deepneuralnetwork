function [dMN, shiftCntPerBatch] = distribMatNewClean( dMN )
    batchCount = max(dMN(:,1));%from new batchID column
    shiftCntPerBatch = zeros(batchCount,1);
    for b=1:batchCount
        %nsi_R = find(dMN(:,1)==b & dMN(:,1)==dMN(:,3) & dMN(:,2)~=dMN(:,4),1,'first');
        rnb = find(dMN(:,1)==b);% the rows of new batchID==b
        
        dMB_b = dMN(rnb,:);
        dMB_b = sortrows(dMB_b,[3 4]);
        dMB_bn =  NaN(size(dMB_b));
        bsc = size(dMB_b,1);

        uc_st = find(dMB_b(:,3)==b,1,'first');
        uc_en = find(dMB_b(:,3)==b,1,'last');
        ucrL = uc_en-uc_st+1;% UnChangedRows of the rows of new batchID==b
        ucr = reshape(uc_st:uc_en,[],1);
        ucsi = sort(dMB_b(ucr,4));
        
        dcr = 1:bsc;% DefinitelyChangedRows of the rows of new batchID==b
        dcr(ucr) = [];
        dcrL = length(dcr);
        dsci = reshape(1:bsc,[],1);
        dsci(ucsi) = [];
        
        suc = dMB_b(ucr,:);%SamplesUnChanged
        sdc = dMB_b(dcr,:);%SamplesDefinitelyChanged
        
        dMB_bn(:,1) = b;
        dMB_bn(ucsi,2:6) = dMB_b(ucr,[4 3 4 5 6]);
        
        dMB_bn(dsci,2) = reshape(dsci,[],1);
        dMB_bn(dsci,3:6) = dMB_b(dcr,3:6);
        
        dMN(rnb,:) = dMB_bn;
    end
end

