function [resultStruct] = divideSamplesInto2ClusterByDistance(X)
%divideSamplesInto2ClusterByDistance Divides X into 2 such groups that the
%2 groups both have better minDist in the group
%   X - is N by D matrix

% maxDistVec = [old_max_dist max_dist_gr1 max_dist_gr2]

    [distMat, ~, maxDistElements] = computeDistanceMatrix(X, [], 1);
    %I would put maxDistElements.rowInds and maxDistElements.colInds to
    %seperate groups
    %groupsStructArr = struct([]);
    groupsStructArr(1) = initializeGroup(X, maxDistElements.rowInds);
    groupsStructArr(2) = initializeGroup(X, maxDistElements.colInds);
    
    %now I would go over the distMat,
    %find the element with the minimum distance and set it to the sample
    %whichever it is in
    N = size(X,1);
    assignedClusterVec = zeros(1,N);
    assignedClusterVec(groupsStructArr(1).centerSampleID) = 1;
    assignedClusterVec(groupsStructArr(2).centerSampleID) = 2;
    distMatSearch = distMat([groupsStructArr(1).centerSampleID;groupsStructArr(2).centerSampleID],:);
    distMatSearch([1 2],[groupsStructArr(1).centerSampleID groupsStructArr(2).centerSampleID]) = inf;
    
    [minVal,minInds] = min(distMatSearch(:));
    while minVal<inf
        [selectedGroupID, sampleIndToBeAssigned] = ind2sub(size(distMatSearch),minInds);
        assignedClusterVec(sampleIndToBeAssigned) = selectedGroupID;
        groupsStructArr(selectedGroupID).indices = [groupsStructArr(selectedGroupID).indices sampleIndToBeAssigned];
        groupsStructArr(selectedGroupID).maxDist = max(groupsStructArr(selectedGroupID).maxDist,minVal);
        distMatSearch(:,sampleIndToBeAssigned) = inf;
        [minVal,minInds] = min(distMatSearch(:));
    end
    
    resultStruct = struct;
    resultStruct.maxDistVec = [max(distMat(:)) [groupsStructArr.maxDist]];
    resultStruct.numSampVec = [N length(groupsStructArr(1).indices) length(groupsStructArr(2).indices)];
    resultStruct.groupsStructArr = groupsStructArr;
end

function groupStruct = initializeGroup(X, centerSampleIndice)
    groupStruct = struct;
    groupStruct.centerSampleID = centerSampleIndice;
    groupStruct.centerVec = X(centerSampleIndice,:);
    groupStruct.indices = centerSampleIndice;
    groupStruct.maxDist = 0;
end

% function D = getDistanceToGroup(sampleX, groupStruct)
% % now a sample can be checked against all samples of the group
% % or only the centerVec of the group
% % we will check the distance to centerVec
%     D = pdist2_fast(sampleX,groupStruct.centerVec);
% end