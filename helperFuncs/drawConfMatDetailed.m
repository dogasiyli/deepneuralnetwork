function [ h ] = drawConfMatDetailed(X, OPS)
    categories	  = getStructField(OPS, 'categories', []);
    remove_blanks = getStructField(OPS, 'remove_blanks', false);
    row_sorting = getStructField(OPS, 'row_sorting', 'none');
    col_sorting = getStructField(OPS, 'col_sorting', 'none');
    add_ticks_rowcnt = getStructField(OPS, 'add_ticks_rowcnt', true);
    add_ticks_colcnt = getStructField(OPS, 'add_ticks_colcnt', true);
    draw_circles     = getStructField(OPS, 'draw_circles', true);
    
%% 1. Initialize the figure
    h = init_fig(OPS);
    
%% 2. Set the colorMap if it is passed as parameter
    set_colormap(h, OPS);
     
%% 5. categories
    category_count = max(size(X));
    if isempty(categories)
        categories = compose('c%d',1:category_count);
    end
    x_categories = categories;
    y_categories = categories;
    if remove_blanks
        x_col_empty = sum(X,1)==0;
        y_row_empty = sum(X,2)==0;
        x_cols = find(~x_col_empty);
        y_rows = find(~y_row_empty);
    else
        x_cols = 1:size(X,2);
        y_rows = 1:size(X,1);
    end
    if strcmpi(row_sorting,'sum')
        sum_r = sum(X(y_rows,:),2);
        [~, inds_r] = sort(sum_r,'descend');
        inds_r = y_rows(inds_r);
    elseif strcmpi(row_sorting,'max')
        max_r = max(X(y_rows,:),[],2);
        [~, inds_r] = sort(max_r,'descend');
        inds_r = y_rows(inds_r);
    else
        inds_r = y_rows;
    end
    if strcmpi(col_sorting,'sum')
        sum_c = sum(X(:,x_cols));
        [~, inds_c] = sort(sum_c,'descend');
        inds_c = x_cols(inds_c);
    elseif strcmpi(col_sorting,'max')
        max_c = max(X(:,x_cols),[],1);
        [~, inds_c] = sort(max_c,'descend');
        inds_c = x_cols(inds_c);
    else
        inds_c = x_cols;
    end
    x_categories = x_categories(inds_c);
    y_categories = y_categories(inds_r);
    X = X(inds_r, inds_c);
    if add_ticks_rowcnt
        sum_r = sum(X,2);
        for yi = 1:length(y_categories)
            y_categories{yi} = [y_categories{yi} '(' num2str(sum_r(yi)) ')'];
        end
    end
    if add_ticks_colcnt
        sum_c = sum(X);
        for xi = 1:length(x_categories)
            x_categories{xi} = [x_categories{xi} '(' num2str(sum_c(xi)) ')'];
        end        
    end
    
 %% 3. Plot imagesc...   
    imagesc(X);hold on;
    figureTitle = fig_title_ops(OPS, h);

%% 4. Row or column normalization      
    allInt = sum(mod(X(:),1)>0)==0 || (isfield(OPS, 'allInt') && ~isempty(OPS.allInt) && (OPS.allInt==1));
    if (isfield(OPS, 'normalizeRowOrCol') && ~isempty(OPS.normalizeRowOrCol)) || allInt
        if isfield(OPS, 'normalizeRowOrCol') && strcmp(OPS.normalizeRowOrCol,'row')
            X = bsxfun(@rdivide,X,sum(X,2));
            X = round(100*X);
            if ~isempty(figureTitle)
                figureTitle = [figureTitle '(Row normalized)'];
            end
        elseif isfield(OPS, 'normalizeRowOrCol') && strcmp(OPS.normalizeRowOrCol,'col')
            X = bsxfun(@rdivide,X,sum(X,1));
            X = round(100*X);
            if ~isempty(figureTitle)
                figureTitle = [figureTitle '(Col normalized)'];
            end
        end 
    end

%% 5. Set the text cell array and print them on the figure with roper colors
    midValue = mean(get(gca,'CLim'));
    maxValue = max(get(gca,'CLim'));
    cmap = colormap;
    correct_cnt = 0;
    X(isnan(X))=0;
    for x = 1:length(inds_c)
        xCat = categories{inds_c(x)};
        for y = 1:length(inds_r)
            yCat = categories{inds_r(y)};
            correct_block = strcmpi(xCat,yCat);
            cur_val = X(y,x);
            cmap_row = 1+round((size(cmap,1)-1)*(cur_val/maxValue));
            inverse_color = 1-cmap(cmap_row,:);

            font_size = 6 + 6*(cur_val/maxValue);
            if correct_block
                if draw_circles
                    draw_circle(x,y,.4,inverse_color);
                end
                correct_cnt = correct_cnt + cur_val;
            end
            if cur_val==0
                continue
            end
            if ~correct_block
                text_color = [1 0 0];
            else
                text_color = inverse_color;
            end
            textStr = num2str(cur_val);
            text(x,y,textStr,'HorizontalAlignment','center', 'Color', text_color, 'FontSize', font_size);
        end
    end

%% 6. Set the axes tick marks and x, y labels
    acc_found = correct_cnt/sum(X(:));
    if ~isempty(figureTitle)
        figureTitle = [figureTitle '-acc(%' num2str(100*acc_found,'%4.2f') ')'];
    else
        figureTitle = ['-acc(%' num2str(100*acc_found,'%4.2f') ')'];
    end
    title(figureTitle);
    set(gca,'XTick',1:length(x_categories));
    set(gca,'XTickLabel',x_categories);
    set(gca,'YTick',1:length(y_categories));
    set(gca,'YTickLabel',y_categories);
    set(gca,'TickLength',[0 0]);
    ylabel('KNOWN group IDs');
    xlabel('PREDICTED group IDs');
    set(gca,'XTickLabelRotation',45);
    colorbar; 
end

function h = init_fig(OPS)
    if ~isfield(OPS,'subFigureArr') || length(OPS.subFigureArr)~=3
        OPS.subFigureArr = 0;
    end
    if (isfield(OPS,'figureID') && ~isempty(OPS.figureID))
        h = figure(OPS.figureID);
    else
        h = figure;
    end
    if (length(OPS.subFigureArr)~=3)
        clf;  
    else
        %use clf before calling this function if this figure will be a
        %subplot. because using clf here deletes all other subplots.
        subplot(OPS.subFigureArr(1),OPS.subFigureArr(2),OPS.subFigureArr(3));
    end
end

function set_colormap(h, OPS)
    if ~isfield(OPS,'colorMapMat')
        OPS.colorMapMat = flipud(winter);%This one is good for default
    end
    try
        %if any stupid map is passed just skip to default
        colormap(h, OPS.colorMapMat);%# Change the colormap
    catch
        colormap(h, flipud(winter));%# This one is good for default
    end
end

function figureTitle = fig_title_ops(OPS, h)
    if (isfield(OPS,'figureTitle') && ~isempty(OPS.figureTitle))
        figureTitle = OPS.figureTitle;
        if iscell(figureTitle)
            set(h,'name',figureTitle{1,end},'numbertitle','off');
        else
            set(h,'name',figureTitle,'numbertitle','off');
        end
    else
        figureTitle = 'Confusion Matrix';
    end
end

function h = draw_circle(x,y,r, color)
    th = 0:pi/50:2*pi;
    xunit = r * cos(th) + x;
    yunit = r * sin(th) + y;
    h = plot(xunit, yunit, 'Color', color);
end