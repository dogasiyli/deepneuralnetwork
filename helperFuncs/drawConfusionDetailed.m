function [ h ] = drawConfusionDetailed( X, vararginStruct)
%vararginStruct.{'figureID','colorMapMat','figureTitle'}
%% 1. Initialize the figure
    if (isfield(vararginStruct,'figureID') && ~isempty(vararginStruct.figureID) && vararginStruct.figureID>0)
        h = figure(vararginStruct.figureID);
    else
        h = figure;
    end
    clf;
    maximizeFigureWindow(h);
    
%% 2. Set the colorMap if it is passed as parameter
    if ~isfield(vararginStruct,'colorMapMat')
        vararginStruct.colorMapMat = flipud(winter);%This one is good for default
    end
    try
        %if any stupid map is passed just skip to default
        colormap(h, vararginStruct.colorMapMat);%# Change the colormap
    catch
        colormap(h, flipud(winter));%# This one is good for default
    end   
    
 %% 3. Title
    if (isfield(vararginStruct,'figureTitle') && ~isempty(vararginStruct.figureTitle))
        figureTitle = vararginStruct.figureTitle;
        if iscell(figureTitle)
            set(h,'name',figureTitle{1,end},'numbertitle','off');
        else
            set(h,'name',figureTitle,'numbertitle','off');
        end
    else
        figureTitle = 'Confusion Matrix';
    end
    if (isfield(vararginStruct,'tickLabelCellStruct') && ~isempty(vararginStruct.tickLabelCellStruct))
        tickLabelCellStruct = vararginStruct.tickLabelCellStruct;
    else
        tickLabelCellStruct =[];
    end
    if (isfield(vararginStruct,'labelStrStruct') && ~isempty(vararginStruct.labelStrStruct))
        labelStrStruct = vararginStruct.labelStrStruct;
        xLabelStr = labelStrStruct.x;
        yLabelStr = labelStrStruct.y;
    else
        xLabelStr = '';
        yLabelStr = 'KNOWN-Ground Truth';
    end


%% 4. Row and column normalization         
    X_rowNorm = bsxfun(@rdivide,X,sum(X,2));
    X_rowNorm(isnan(X_rowNorm)) = 0;
    figTit_rowNorm = 'Recall-Row normalized';
    textStr_rowNorm = getTextStrings(X_rowNorm*100, 'percent2');

    X_colNorm = bsxfun(@rdivide,X,sum(X,1));
    X_colNorm(isnan(X_colNorm)) = 0;
    figTit_colNorm = 'Precision-Col normalized';
    textStr_colNorm = getTextStrings(X_colNorm*100, 'percent2');

    textStr_numbers = getTextStrings(X, 'integer');

%     rcCnt = size(X,1);
%     [rCnt, cCnt] = size(X);
%     M = 2*rcCnt + 1;
%     pushSubplot(X, textStr_numbers, M, 1:rCnt, 1:cCnt, [figureTitle '(number of samples)'], 'PREDICTED', 'KNOWN-Ground Truth');
%     pushSubplot(X_rowNorm, textStr_rowNorm, M, rCnt+1:2*rCnt, 1:cCnt, figTit_rowNorm, 'PREDICTED', 'KNOWN-Ground Truth');
%     pushSubplot(X_colNorm, textStr_colNorm, M, 1:rCnt, cCnt+1:2*cCnt, figTit_colNorm, 'PREDICTED', 'KNOWN-Ground Truth');
    
    acc = 100*sum(diag(X))/sum(X(:));
    figureTitle = ({'Num of samples      Recall(TP/All_{Known})',['Precision(TP/All_{Predicted})     Acc(' num2str(acc,'%4.2f') ')']});
    plotWithPrecRecallBoxes(X, textStr_numbers, textStr_rowNorm, textStr_colNorm, figureTitle, xLabelStr, yLabelStr, tickLabelCellStruct);
end

function pushSubplot(X, textStr, M, subRows, subCols, figureTitle, xlabelStr, ylabelStr)
    [rowCount, colCount] = size(X);
    subplot(M,M,sub2ind([M,M],subRows, subCols));
    imagesc(X);hold on;
    title(figureTitle);
%% 5. Set the text cell array and print them on the figure with roper colors
    
    [x_cnt,y_cnt] = meshgrid(1:rowCount,1:colCount);   %# Create x and y coordinates for the strings
    hStrings = text(x_cnt(:),y_cnt(:),textStr(:),'HorizontalAlignment','Center','VerticalAlignment','Middle','Interpreter','latex','FontSize',8);%# Plot the strings
    midValue = mean(get(gca,'CLim'));  %# Get the middle value of the color range
    textColors = repmat(X(:) > midValue,1,3);  %# Choose white or black for the
                                               %#   text color of the strings so
                                               %#   they can be easily seen over
                                               %#   the background color
    set(hStrings,{'Color'},num2cell(textColors,2));  %# Change the text colors

%% 6. Set the axes tick marks and x, y labels    
    set(gca,'XTick',1:rowCount...
        ...,'XTickLabel',{'A','B','C','D','E'}
           ,'YTick',1:colCount...
        ...,'YTickLabel',{'A','B','C','D','E'}
           ,'TickLength',[0 0]);
    ylabel(ylabelStr);
    xlabel(xlabelStr);
    colorbar; 
end

function plotInSingleBox(X, textStr_numbers, textStr_rowNorm, textStr_colNorm, figureTitle, xlabelStr, ylabelStr)
    [rowCount, colCount] = size(X);
    imagesc(X);hold on;
    title(figureTitle, 'HorizontalAlignment', 'left');
%% 5. Set the text cell array and print them on the figure with roper colors
    
    [x_cnt,y_cnt] = meshgrid(1:rowCount,1:colCount);   %# Create x and y coordinates for the strings
    x_num = x_cnt-0.3;
    y_num = y_cnt-0.3;
    
    x_row_recall = x_cnt + 0.3;
    y_row_recall = y_cnt - 0.3;
    
    x_col_precision = x_cnt - 0.3;
    y_col_precision = y_cnt + 0.3;
    
    hStrings_n = text(x_num(:),y_num(:),textStr_numbers(:),'HorizontalAlignment','Right','VerticalAlignment','Bottom','Interpreter','latex','FontSize',16);%# Plot the strings
    hStrings_r = text(x_row_recall(:),y_row_recall(:),textStr_rowNorm(:),'HorizontalAlignment','Center','VerticalAlignment','Middle','Interpreter','latex','FontSize',16);%# Plot the strings
    hStrings_c = text(x_col_precision(:),y_col_precision(:),textStr_colNorm(:),'HorizontalAlignment','Center','VerticalAlignment','Middle','Interpreter','latex','FontSize',16);%# Plot the strings
    midValue = mean(get(gca,'CLim'));  %# Get the middle value of the color range
    textColors = repmat(X(:) > midValue,1,3);  %# Choose white or black for the
                                               %#   text color of the strings so
                                               %#   they can be easily seen over
                                               %#   the background color
    set(hStrings_n,{'Color'},num2cell(textColors,2));  %# Change the text colors
    set(hStrings_r,{'Color'},num2cell(textColors,2));  %# Change the text colors
    set(hStrings_c,{'Color'},num2cell(textColors,2));  %# Change the text colors

%% 6. Set the axes tick marks and x, y labels    
    set(gca,'XTick',1:rowCount...
        ...,'XTickLabel',{'A','B','C','D','E'}
           ,'YTick',1:colCount...
        ...,'YTickLabel',{'A','B','C','D','E'}
           ,'TickLength',[0 0]);
    ylabel(ylabelStr);
    xlabel(xlabelStr);
    colorbar; 
end

function plotWithPrecRecallBoxes(X, textStr_numbers, textStr_rowNorm, textStr_colNorm, figureTitle, xlabelStr, ylabelStr, tickLabelCellStruct)
    [rowCount, colCount] = size(X);
    subplot(rowCount+1,colCount+1,sub2ind([rowCount+1,colCount+1],1:rowCount, 1:colCount));
    imagesc(X);hold on;
    title(figureTitle, 'HorizontalAlignment', 'left');
%% 5. Set the text cell array and print them on the figure with roper colors
    
    [x_cnt,y_cnt] = meshgrid(1:rowCount,1:colCount);   %# Create x and y coordinates for the strings
    x_num = x_cnt-0.3;
    y_num = y_cnt-0.3;
    
    x_row_recall = x_cnt + 0.3;
    y_row_recall = y_cnt - 0.3;
    
    x_col_precision = x_cnt - 0.3;
    y_col_precision = y_cnt + 0.3;
    
    txtSize = 16 - rowCount;
    try
        hStrings_n = text(x_num(:),y_num(:),textStr_numbers(:),'HorizontalAlignment','Right','VerticalAlignment','Bottom','Interpreter','latex','FontSize',txtSize);%# Plot the strings
        hStrings_r = text(x_row_recall(:),y_row_recall(:),textStr_rowNorm(:),'HorizontalAlignment','Center','VerticalAlignment','Middle','Interpreter','latex','FontSize',txtSize);%# Plot the strings
        hStrings_c = text(x_col_precision(:),y_col_precision(:),textStr_colNorm(:),'HorizontalAlignment','Center','VerticalAlignment','Middle','Interpreter','latex','FontSize',txtSize);%# Plot the strings
        midValue = mean(get(gca,'CLim'));  %# Get the middle value of the color range
        textColors = repmat(X(:) > midValue,1,3);  %# Choose white or black for the
                                                   %#   text color of the strings so
                                                   %#   they can be easily seen over
                                                   %#   the background color
        set(hStrings_n,{'Color'},num2cell(textColors,2));  %# Change the text colors
        set(hStrings_r,{'Color'},num2cell(textColors,2));  %# Change the text colors
        set(hStrings_c,{'Color'},num2cell(textColors,2));  %# Change the text colors
    catch err
        disp(['error occured on fantasy stuff..err(' err.message ')'])
    end

%% 6. Set the axes tick marks and x, y labels    
    set(gca,'XTick',1:rowCount...
        ...,'XTickLabel',{'A','B','C','D','E'}
           ,'YTick',1:colCount...
        ...,'YTickLabel',{'A','B','C','D','E'}
           ,'TickLength',[0 0]);
    if exist('tickLabelCellStruct','var') && isstruct(tickLabelCellStruct)
        if isfield(tickLabelCellStruct,'x')
            xtl = tickLabelCellStruct.x;
            if length(xtl)==rowCount
                try
                    xticklabels(xtl);
                catch
                    set(gca,'XTickLabel',xtl);
                end
            end
        end
        if isfield(tickLabelCellStruct,'y')
            ytl = tickLabelCellStruct.y;
            if length(xtl)==colCount
                try
                    yticklabels(ytl);
                catch
                    set(gca,'YTickLabel',ytl);
                end
            end
        end        
    end
    ylabel(ylabelStr);
    xlabel(xlabelStr);
    %colorbar;
    
    slc = find(eye(rowCount,colCount));
    
    subplot(rowCount+1,colCount+1,sub2ind([rowCount+1,colCount+1], (rowCount+1)*ones(colCount,1),(1:colCount)'));
    X_rowNorm = bsxfun(@rdivide,X,sum(X,2));
    X_rowNorm = diag(X_rowNorm);
    X_rowNorm(isnan(X_rowNorm)) = 0;
    imagesc(X_rowNorm);hold on;
    hStrings_r = text(ones(1,rowCount),1:rowCount,textStr_rowNorm(slc),'HorizontalAlignment','Center','VerticalAlignment','Middle','Interpreter','latex','FontSize',16);%# Plot the strings
    textColors_r = repmat(X_rowNorm(:) > median(X_rowNorm),1,3);
    set(hStrings_r,{'Color'},num2cell(textColors_r,2));
    title('Recall')
    try
        xticks([]);
        yticks(1:length(X_rowNorm));
    catch
        set(gca,'XTickLabel',[]);
        set(gca,'YTickLabel',1:length(X_rowNorm));
    end
    
    subplot(rowCount+1,colCount+1,sub2ind([rowCount+1,colCount+1],1:rowCount, (colCount+1)*ones(1,rowCount)));
    X_colNorm = bsxfun(@rdivide,X,sum(X,1));
    X_colNorm = diag(X_colNorm);
    X_colNorm(isnan(X_colNorm)) = 0;
    imagesc(X_colNorm');hold on;
    hStrings_c = text(1:rowCount,ones(1,rowCount),textStr_colNorm(slc),'HorizontalAlignment','Center','VerticalAlignment','Middle','Interpreter','latex','FontSize',16);%# Plot the strings
    textColors_c = repmat(X_colNorm(:) > median(X_colNorm),1,3);
    set(hStrings_c,{'Color'},num2cell(textColors_c,2));
    xlabel('Precision of classes')
    try
        xticks(1:length(X_colNorm));
        yticks([]);
    catch
        set(gca,'XTickLabel',1:length(X_colNorm));
        set(gca,'YTickLabel',[]);
    end
end

function textStrings = getTextStrings(X, modSlct)
    switch modSlct
        case 'integer'
            textStrings = num2str(X(:),'%d');  %# Create strings from the matrix values        
            textStrings = strtrim(cellstr(textStrings));  %# Remove any space padding
            textStrings(strcmp(textStrings(:), '0')) = {' '};
        case {'float4','percent2'}
            textStrings = num2str(X(:),'%0.2f');  %# Create strings from the matrix values
            textStrings = strtrim(cellstr(textStrings));  %# Remove any space padding
            textStrings(strcmp(textStrings(:), '0.00')) = {'   '}; 
    end
end
