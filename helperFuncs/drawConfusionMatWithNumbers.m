function [ h ] = drawConfusionMatWithNumbers(X, OPS)
    categories	= getStructField(OPS, 'categories', []);
%% 1. Initialize the figure
    if ~isfield(OPS,'subFigureArr') || length(OPS.subFigureArr)~=3
        OPS.subFigureArr = 0;
    end
    if (isfield(OPS,'figureID') && ~isempty(OPS.figureID))
        h = figure(OPS.figureID);
    else
        h = figure;
    end
    if (length(OPS.subFigureArr)~=3)
        clf;  
    else
        %use clf before calling this function if this figure will be a
        %subplot. because using clf here deletes all other subplots.
        subplot(OPS.subFigureArr(1),OPS.subFigureArr(2),OPS.subFigureArr(3));
    end
    
%% 2. Set the colorMap if it is passed as parameter
    if ~isfield(OPS,'colorMapMat')
        OPS.colorMapMat = flipud(winter);%This one is good for default
    end
    if (length(OPS.subFigureArr)~=3)
        try
            %if any stupid map is passed just skip to default
            colormap(h, OPS.colorMapMat);%# Change the colormap
        catch
            colormap(h, flipud(winter));%# This one is good for default
        end
    end    
    
 %% 3. Plot imagesc...   
    imagesc(X);hold on;
    if (isfield(OPS,'figureTitle') && ~isempty(OPS.figureTitle))
        figureTitle = OPS.figureTitle;
        if iscell(figureTitle)
            set(h,'name',figureTitle{1,end},'numbertitle','off');
        else
            set(h,'name',figureTitle,'numbertitle','off');
        end
    else
        figureTitle = 'Confusion Matrix';
    end


%% 4. Row or column normalization      
    allInt = sum(mod(X(:),1)>0)==0 || (isfield(OPS, 'allInt') && ~isempty(OPS.allInt) && (OPS.allInt==1));
    if (isfield(OPS, 'normalizeRowOrCol') && ~isempty(OPS.normalizeRowOrCol)) || allInt
        if isfield(OPS, 'normalizeRowOrCol') && strcmp(OPS.normalizeRowOrCol,'row')
            X = bsxfun(@rdivide,X,sum(X,2));
            X = round(100*X);
            if ~isempty(figureTitle)
                figureTitle = [figureTitle '(Row normalized)'];
            end
        elseif isfield(OPS, 'normalizeRowOrCol') && strcmp(OPS.normalizeRowOrCol,'col')
            X = bsxfun(@rdivide,X,sum(X,1));
            X = round(100*X);
            if ~isempty(figureTitle)
                figureTitle = [figureTitle '(Col normalized)'];
            end
        end
        textStrings = num2str(X(:),'%d');  %# Create strings from the matrix values        
        textStrings = strtrim(cellstr(textStrings));  %# Remove any space padding
        textStrings(strcmp(textStrings(:), '0')) = {' '};
    else       
        textStrings = num2str(X(:),'%0.2f');  %# Create strings from the matrix values
        textStrings = strtrim(cellstr(textStrings));  %# Remove any space padding
        textStrings(strcmp(textStrings(:), '0.00')) = {'   '};
    end
    if ~isempty(figureTitle)
        title(figureTitle);
    end

%% 5. Set the text cell array and print them on the figure with roper colors
    rowCount = size(X,1);
    [x,y] = meshgrid(1:rowCount);   %# Create x and y coordinates for the strings
    hStrings = text(x(:),y(:),textStrings(:),...      %# Plot the strings
                    'HorizontalAlignment','center');
    midValue = mean(get(gca,'CLim'));  %# Get the middle value of the color range
    textColors = repmat(X(:) > midValue,1,3);  %# Choose white or black for the
                                               %#   text color of the strings so
                                               %#   they can be easily seen over
                                               %#   the background color
    set(hStrings,{'Color'},num2cell(textColors,2));  %# Change the text colors

%% 6. Set the axes tick marks and x, y labels
    if isempty(categories)
        categories = compose('c%d',1:rowCount);
    end
    set(gca,'XTick',1:rowCount...
           ,'XTickLabel',categories...
           ,'YTick',1:rowCount...
           ,'YTickLabel',categories...
           ,'TickLength',[0 0]);
    ylabel('KNOWN group IDs');
    xlabel('PREDICTED group IDs');
    set(gca,'XTickLabelRotation',45);
    colorbar; 
end

