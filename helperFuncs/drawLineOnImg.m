function curFrame = drawLineOnImg(curFrame, ptFr, ptTo, edgeColor3, edgeThickness)
    %normally the line is from ptFrom to (ptFrom + boxSize)
    %but the line thickness makes it to draw 4 boxes
    
    %ptFr[xc yr], ptTo[xc yr]
    XC = 1;%X-Columns of image
    YR = 2;%Y-Rows of image
    
    [rowCntFr, colCntFr, channelSize] = size(curFrame);
    ptFr = min(max(round(ptFr),ones(size(ptFr))),[colCntFr, rowCntFr]);
    ptTo = min(max(round(ptTo),ones(size(ptTo))),[colCntFr, rowCntFr]);
    
    yRowCnt = abs(ptTo(YR)-ptFr(YR))+1;
    xColCnt = abs(ptTo(XC)-ptFr(XC))+1;
    
    %suppose we have a rectangle of edgedBlock[rowCnt,colCnt]
    edgedBlock = getEdgedBlock(yRowCnt,xColCnt);
    edgedBlock = mirrorBlock(edgedBlock, ptFr, ptTo);
    
    colorMul = 1;
    if max(curFrame(:)>1)
        colorMul = 255;
    end
    
    tempFr = min(ptFr,ptTo);
    ptTo = max(ptFr,ptTo);
    ptFr = tempFr;
    clear tempFr;
    
    curFrameBW = zeros(rowCntFr, colCntFr);
    curFrameBW(ptFr(YR):ptTo(YR), ptFr(XC):ptTo(XC)) = edgedBlock;
    if (edgeThickness>1)
        curFrameBW = imdilate(curFrameBW, ones(1,edgeThickness),'same');
        curFrameBW = imdilate(curFrameBW, ones(1,edgeThickness)','same');
    end
    for i=1:channelSize
        curBlock = squeeze(curFrame(:, :, i));
        curBlock(curFrameBW==1) = colorMul*edgeColor3(i);
        curFrame(:, :, i) = curBlock;
    end
end

function edgedBlock = getEdgedBlock(yRowCnt,xColCnt)
    %suppose we have a rectangle of [rowCnt,colCnt]
    edgedBlock = zeros(yRowCnt,xColCnt);
    
    %https://www.mathworks.com/matlabcentral/answers/28366-geometric-diagonal-of-a-non-squared-matrix
    nPix = max(yRowCnt,xColCnt);
    ryj = round(linspace(1,yRowCnt,nPix));
    cxi = round(linspace(1,xColCnt,nPix));
    edgedBlock(sub2ind([yRowCnt,xColCnt],ryj,cxi)) = 1;
end

function edgedBlock = mirrorBlock(edgedBlock, ptFr, ptTo)
    XC = 1;%X-Columns of image
    YR = 2;%Y-Rows of image
    xcMirrored = (ptTo(XC)-ptFr(XC))>0;
    yrMirrored = (ptTo(YR)-ptFr(YR))>0;
    if xcMirrored
        edgedBlock = fliplr(edgedBlock);
    end
    if yrMirrored
        edgedBlock = flipud(edgedBlock);
    end    
end