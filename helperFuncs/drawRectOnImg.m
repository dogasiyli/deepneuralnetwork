function curFrame = drawRectOnImg(curFrame, ptFrom, boxSize, edgeColor3, edgeThickness)
    %normally the line is from ptFrom to (ptFrom + boxSize)
    %but the line thickness makes it to draw 4 boxes
    pt_lt_01 = ptFrom;
    pt_rt_11 = ptFrom + [boxSize(1) 0];
    pt_rb_10 = ptFrom + boxSize;
    pt_lb_00 = ptFrom + [0 boxSize(2)];
    
    curFrame = drawLineOnImg(curFrame, pt_lt_01, pt_rt_11, edgeColor3, edgeThickness);
    curFrame = drawLineOnImg(curFrame, pt_rt_11, pt_rb_10, edgeColor3, edgeThickness);
    curFrame = drawLineOnImg(curFrame, pt_lb_00, pt_rb_10, edgeColor3, edgeThickness);
    curFrame = drawLineOnImg(curFrame, pt_lt_01, pt_lb_00, edgeColor3, edgeThickness);
    
end

% function curFrame = drawLineOnImg(curFrame, ptFr, ptTo, edgeColor3, edgeThickness, horiz_vert)
%     %normally the line is from ptFrom to (ptFrom + boxSize)
%     %but the line thickness makes it to draw 4 boxes
%     ptFr = round(ptFr);
%     ptTo = round(ptTo);
%     if strcmp(horiz_vert , 'horizontal')
%         ptFr(2) = min(ptFr(2) - floor(edgeThickness/2), size(curFrame,1)-floor(edgeThickness/2));
%         ptFr(2) = max(1, ptFr(2));
%         ptTo(2) = ptTo(2) + floor(edgeThickness/2);
%     end
%     if strcmp(horiz_vert , 'vertical')
%         ptFr(1) = min(ptFr(1) - floor(edgeThickness/2), size(curFrame,2)-floor(edgeThickness/2));
%         ptFr(1) = max(1, ptFr(1));
%         ptTo(1) = ptTo(1) + floor(edgeThickness/2);
%     end
%     ptFr(2) = min(ptFr(2), size(curFrame,1));
%     ptTo(2) = min(ptTo(2), size(curFrame,1));
%     ptTo(1) = min(ptTo(1), size(curFrame,2));
%     ptFr(1) = min(ptFr(1), size(curFrame,2));
%     
%     colorMul = 1;
%     if max(curFrame(:)>1)
%         colorMul = 255;
%     end
%     
%     for i=1:size(curFrame,3)
%         curFrame(ptFr(2):ptTo(2), ptFr(1):ptTo(1), i) = colorMul*edgeColor3(i);
%     end
% end