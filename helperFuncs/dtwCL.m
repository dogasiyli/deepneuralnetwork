function [vecIDs, distanceMin, D] = dtwCL(costMat, addLog, queryVec, seqVec, winSize) %[minDistVectors,warpedVecIDs]=
%% Sample
% A = [-0.87    -0.84    -0.85    -0.82    -0.23    1.95    1.36    0.60    0.0     -0.29;...
%      -0.88    -0.91    -0.84    -0.82    -0.24    1.92    1.41    0.51    0.03    -0.18];
% B = [-0.60    -0.65    -0.71    -0.58    -0.17    0.77    1.94;...
%      -0.46    -0.62    -0.68    -0.63    -0.32    0.74    1.97];
%
% Bwarped = B([1 2 3 4 5 6 6 6 6 7])
% Awarped = A([])
%
% degVecBase: base vector, row-based
% degVec2Compare: vector to compare, row-based
% winSize: window parameter
%      if s(i) is matched with t(j) then |i-j|<=w
% d: resulting distance

%% Inputs:
% queryVec : contains a feature vector at each row 
%            size is Tq x F  (Time of queryVec and Feature size
% seqVec   : contains a feature vector at each row 
%            size is Ts x F  (Time of seqVec and Feature size
%
% this function seeks for the best slice in seqVec that matches to queryVec

%% Restrictions on warping
%R1:Monotonicity - $ i_s-1   ? i_s $ and $ j_s-1 ? j_s $.
%   The allignment path does not go back
%   Guarantees that the features are not repeated in the allignment

%R2:Continuity -- $ i_s � i_s-1  ? 1 $ and  $ j_s � j_s-1 ? 1 $
%   The allignment path does not jump in "time" index.
%   Guarantees that the allignment does not omit important features

%R3:Boundry conditions -- $ i_1  = 1, i_k = n $ and  $ j_1 = 1,  j_k = m $
%   The allignment path starts at the bottom and ends at the top right
%   Guarantees that the alignment does not consider partially one of the sequences.

%R4:Warping Window -- $ |i_s � j_s| ? r $, where $ r > 0 $ is the window length.
%   A good alignment path is unlikely to wander too far from the diagonal.
%   Guarantees that the alignment does not try to skip different features and gets stuck at similar features. 

%R5:Slope Constraint -- in each direction there are limits 
%   The alignment path should not be too steep or too shallow
%   Prevents that very short parts of the sequences are matched to very long ones.

    if exist('winSize','var')==0
        winSize=Inf;
    elseif isempty(winSize)
        winSize=Inf;
    end

    %createCost Matrix if not given
    if exist('costMat','var')==0
        ns=size(queryVec,1);
        nt=size(seqVec,1);
        costMatCalculateType = 'degrees';
        if strcmp(costMatCalculateType,'degrees')
            costMat = abs(repmat(queryVec',1,nt)-repmat(seqVec,ns,1));
            costMat(costMat>180) = 360-costMat(costMat>180);
        end
    end

    if ~exist('addLog','var')
        addLog=false;
    end

    ns = size(costMat,1);
    nt = size(costMat,2);

    winSize=max(winSize, abs(ns-nt)); % adapt window size


    %% initialization
    D=zeros(ns+1,nt+1)+Inf; % cache matrix
    D(1,1)=0;



    %% begin dynamic programming
    for i=1:ns
        for j=max(i-winSize,1):min(i+winSize,nt)
            try
                if (addLog)
                    D(i+1,j+1)=log(costMat(i,j)+1)+min( [D(i,j+1), D(i+1,j), D(i,j)] );
                else
                    D(i+1,j+1)=costMat(i,j)+min( [D(i,j+1), D(i+1,j), D(i,j)] );
                end
            catch
                if (addLog)
                    D(i+1,j+1)=log(costMat(i,j)+1)+min( [D(i,j+1), D(i+1,j), D(i,j)] );
                else
                    D(i+1,j+1)=costMat(i,j)+min( [D(i,j+1), D(i+1,j), D(i,j)] );
                end
            end
        end
    end
    distanceMin=D(ns+1,nt+1);

% %warpedBaseVecIDs will be the indices as in
% %if degVecBase(i) is matched to degVec2Compare(j)
% %then warpedBaseVecIDs(i)=j
% %[~,warpedBaseVecIDs] = min(D(2:end,2:end),[],2);
% 
% %warpedCompVecIDs will be the indices as in
% %if degVec2Compare(i) is matched to degVecBase(j)
% %then warpedCompVecIDs(i)=j
% %[~,warpedCompVecIDs] = min(D(2:end,2:end));

    vecIDs = backtrackDTW2(D, costMat);
% warpedBaseVecIDs = vecIDs.Row;
% warpedCompVecIDs = vecIDs.Col;
% wayBack = vecIDs.wayBack;
% % minDistanceBase = abs(queryVec-seqVec(warpedBaseVecIDs));
% % minDistanceBase(minDistanceBase>180) = 360-minDistanceBase(minDistanceBase>180);
% % 
% % minDistanceSample = abs(seqVec-queryVec(warpedCompVecIDs));
% % minDistanceSample(minDistanceSample>180) = 360-minDistanceSample(minDistanceSample>180);
% % 
% % warpedVecIDs.Base = warpedBaseVecIDs';
% % warpedVecIDs.Sample = warpedCompVecIDs;
% % 
% % minDistVectors.Base = minDistanceBase;
% % minDistVectors.Sample = minDistanceSample;

end

function vecIDs = backtrackDTW(D)
   [rC,cC] = size(D);
   vecID_r = zeros(rC,1);
   vecID_c = zeros(cC,1);
   r=rC;c=cC;
   wayBack = [rC,cC];
   i = 1;
   while r>1
       while c>1
           wayBack(i,:) = [r c];
           i = i + 1;
           vecID_r(r) = c;
           vecID_c(c) = r;
           if (D(r-1,c-1)==min(min(D(r-1:r,c-1:c))))
               r=r-1;c=c-1;
           elseif (D(r-1,c)==min(min(D(r-1:r,c-1:c))))
               r=r-1;
           elseif (D(r,c-1)==min(min(D(r-1:r,c-1:c))))
               c=c-1;
           end   
       end
   end
   vecIDs.Row = [vecID_r(2:end)-1];
   vecIDs.Col = [vecID_c(2:end)-1];
   vecIDs.wayBack = wayBack(end:-1:1,:);
end

function vecIDs = backtrackDTW2(D, costMat)
   [rC,cC] = size(D);
   vecID_r = zeros(rC-1,1);
   vecID_r_Val = inf(rC-1,1);
   vecID_c = zeros(cC-1,1);
   vecID_c_Val = inf(cC-1,1);
   wayBack = [rC-1,cC-1];
   i = 1;
   r=rC;c=cC;
   while r>1
       while c>1
           wayBack(i,:) = [r-1 c-1];
           i = i + 1;
           vecID_r(r-1) = c-1;
           vecID_c(c-1) = r-1;
           vecID_r_Val(r-1) = min(vecID_r_Val(r-1),costMat(r-1,c-1));
           vecID_c_Val(c-1) = min(vecID_c_Val(c-1),costMat(r-1,c-1));
           if (D(r-1,c-1)==min(min(D(r-1:r,c-1:c))))
               r=r-1;c=c-1;
           elseif (D(r-1,c)==min(min(D(r-1:r,c-1:c))))
               r=r-1;
           elseif (D(r,c-1)==min(min(D(r-1:r,c-1:c))))
               c=c-1;
           end   
       end
   end
   vecIDs.Row = vecID_r;
   vecIDs.Row_Val = vecID_r_Val;
   vecIDs.Row_Mean = mean(vecID_r_Val);
   vecIDs.Col = vecID_c;
   vecIDs.Col_Val = vecID_c_Val;
   vecIDs.Col_Mean = mean(vecID_c_Val);
   vecIDs.wayBack = wayBack(end:-1:1,:);
end



