function [imGrey, fillHist] = fill0s(imGrey, boxSur)
    %set all the values bigger than 1900 to 0
    imGrey(imGrey>1900) = 0;
    %get the size of image
    [rc,cc] = size(imGrey);
    %to divide the image into vector parts find the common divisors
    divVals = floor(linspace(boxSur,rc,10));
    %now get the best divisor that is bigger than or equal to the
    %boxSur<rounding> variable
    idx = find(divVals>=boxSur,1);
    %set it to j so that j can be incremented
    j=idx;
    %remember how many holes are filled into a matrix
    fillHist = [];
    
    %while there still exists a zero value in imgrey
    %and j can still be incremented
    while min(imGrey(:))==0 && j<=length(divVals)
        %find the divison value
        k = min(j,length(divVals));
        rowLen = divVals(k);
        %reshape the image into vector array
        rowLast = rc-mod(rc,rowLen);
        imGreyNew = imGrey(1:rowLast,:);
        imGreyNew = reshape(imGreyNew,rowLen,[]);
        %find the column ids with zeros
        colsToFill = find(prod(imGreyNew)==0);
        z = 0;
        boxRowCnt = rowLast/rowLen;
        boxColCnt = cc;
        %there is a hypothetical box area
        %think of each vec as pixels, then each pixel will create another
        %image with size [boxRowCnt by boxColCnt]  
        for c = colsToFill
            [boxR, boxC] = ind2sub([boxRowCnt boxColCnt],c);
            boxC = boxC-2:boxC+2;
            boxC(boxC<1) = [];
            boxC(boxC>boxColCnt) = [];
            boxR = ones(size(boxC))*boxR;
            cVec = sub2ind([boxRowCnt boxColCnt],boxR,boxC);
            curMat = imGreyNew(:,cVec);
            
            zeroInds = curMat==0;
            nonzeroInds = curMat~=0;
            if sum(nonzeroInds(:))>1
                minValToFill = min(min(curMat(curMat>0)));
                curMat(zeroInds) = minValToFill;
                imGreyNew(:,cVec) = curMat;
            else
                z = z + 1;
            end
            %imGrey(imGrey(:,c)==0,c) = mean(imGrey(imGrey(:,c)~=0,c));
        end
        fillHist = [fillHist;[k rowLen length(colsToFill) z]]; %#ok<AGROW>
        imGreyNew = reshape(imGreyNew,rowLast,cc);
        imGrey(1:rowLast,:) = imGreyNew;
        if length(colsToFill)==z
            j=j+1;
        end
    end
    imGrey(imGrey==0) = max(imGrey(:));
end

function imGrey = fill0s_old03(imGrey, boxSur)
    imGrey(imGrey>1900) = 0;
    [rc,cc] = size(imGrey);
    divVals = divisors(rc);
    idx = find(divVals>=boxSur,1);
    j=idx;
    fillHist = [];
    while min(imGrey(:))==0 && j<=length(divVals)
        k = min(j,length(divVals));
        rowLen = divVals(k);
        imGrey = reshape(imGrey,rowLen,[]);
        colsToFill = find(prod(imGrey)==0);
        z = 0;
        for c = colsToFill
            curVec = imGrey(:,c);
            zeroInds = curVec==0;
            nonzeroInds = curVec~=0;
            if sum(nonzeroInds)>1
                curVec(zeroInds) = mean(curVec(nonzeroInds));
                imGrey(:,c) = curVec;
            else
                z = z + 1;
            end
            %imGrey(imGrey(:,c)==0,c) = mean(imGrey(imGrey(:,c)~=0,c));
        end
        fillHist = [fillHist;[k rowLen length(colsToFill) z]]; %#ok<AGROW>
        imGrey = reshape(imGrey,rc,cc);
        j=j+1;
    end
    imGrey(imGrey==0) = max(imGrey(:));
end

function imGrey = fill0s_old02(imGrey, boxSur)
    imGrey(imGrey>1900) = 0;
    [rc,cc] = size(imGrey);
    divVals = divisors(rc);
    idx = find(divVals>=boxSur,1);
    j=idx;
%     fillHist = [];
    while min(imGrey(:))==0 && j<=length(divVals)
        k = min(j,length(divVals));
        rowLen = divVals(k);
        imGrey = reshape(imGrey,rowLen,[]);
        colsToFill = find(prod(imGrey)==0);
        z = 0;
        for c = colsToFill
%             curVec = imGrey(:,c);
%             zeroInds = curVec==0;
%             nonzeroInds = curVec~=0;
%             if sum(nonzeroInds)>1
%                 curVec(zeroInds) = mean(curVec(nonzeroInds));
%                 imGrey(:,c) = curVec;
%             else
%                 z = z + 1;
%             end
            imGrey(imGrey(:,c)==0,c) = mean(imGrey(imGrey(:,c)~=0,c));
        end
%         fillHist = [fillHist;[k rowLen length(colsToFill) z]]; %#ok<AGROW>
        imGrey = reshape(imGrey,rc,cc);
        j=j+1;
    end
    imGrey(imGrey==0) = max(imGrey(:));
end

function imGrey = fill0s_old01(imGrey, minVecLen, fillVal)
    [rc,cc] = size(imGrey);
    divVals = divisors(rc);
    idx = find(divVals>=minVecLen,1);
    rowLen = divVals(idx);
    
    imGrey = reshape(imGrey,rowLen,[]);
    if ~exist('fillVal','var')
        colMaxs = max(imGrey);
    else
        colMaxs = fillVal * ones(size(imGrey,2));
    end
    colsToFill = find(prod(imGrey)==0);
    for c = colsToFill
        imGrey(imGrey(:,c)==0,c) = colMaxs(c);
    end
    imGrey = reshape(imGrey,rc,cc);
end