function [ distMat, cropIDs, foundSampleCount] = fillDistMatWithKnownInformation_HospiSign( distMat, cropIDs, searchMat, detailedLabels, knownDistances_col11, knownDistances )
    %searchMat has 5 columns and K calculations to put into distMat
    % col1 - inds of distMat
    % col2 - rowID of distMat (im_i)
    % col3 - colID of distMat (im_j)
    % col4 - numOfFrames im_i has
    % col5 - numOfFrames im_j has
    K = size(searchMat,1);

    %detailedLabels - signID, userID, frameID, lh1_rh2_bh3, frameID
    % N rows where distMat is N by N
    N = size(detailedLabels,1);
    foundSampleCount = 0;
    
    distMatTotalImageCount = size(distMat, 1);
    distMatHasInfoCount = sum(sum(triu(ones(N),1)));
    unknownDistanceCount = sum(sum(isnan(distMat),1));
    knownDistanceCount = distMatHasInfoCount - unknownDistanceCount;
    disp(['For N(' num2str(distMatTotalImageCount) ') images there are a total of C(' num2str(distMatHasInfoCount) ') calculations']);
    disp(['+(' num2str(knownDistanceCount) ') already known and in distmat -(' num2str(unknownDistanceCount) ') are not in distMat']);

    %so searchMat's rows would be - 
    srcMatRows = [detailedLabels(searchMat(:,2),1:5) detailedLabels(searchMat(:,3),1:5)];
    if isempty(knownDistances)
        %do nothing
        %disp('no known information exists in knownDistances matrix');
        return
    elseif ~isstruct(knownDistances) && size(knownDistances,2)>=10
        disp('Converting knownDistances matrix to squeezed version')
        [~,knownDistances] = distMatQuantize_hospiSign(knownDistances, []);
    elseif isstruct(knownDistances) && isfield(knownDistances,'frameInfo') && isfield(knownDistances,'additionalCols')
        %knownDistances already converted to squeezed version
        if isempty(knownDistances.frameInfo) && isempty(knownDistances.additionalCols)
            disp('no known information exists in knownDistances matrix');
            return            
        end
    else
        %knownDistances should be converted to squeezed version
        [~,knownDistances] = distMatQuantize_hospiSign(knownDistances, []);
    end    
    
    [~,srcMat_Q] = distMatQuantize_hospiSign(srcMatRows, []);
    
    srcMat_frameInfo = srcMat_Q.frameInfo;
    knwRow_frameInfo = knownDistances.frameInfo;

    [Lia, Locb] = ismember(srcMat_frameInfo, knwRow_frameInfo);
    foundSampleCount = sum(Lia==1);
    disp([num2str(foundSampleCount) ' of ' num2str(K) ' distances is found in knownDistancesMatFile']);
    zero2eps = 0;
    %if Locb is not zero it means srcMatRows(i) is at knownDistances(Locb(i))
    for i=1:K
        if Locb(i)~=0
            foundRowStrct = struct;
            foundRowStrct.frameInfo = knownDistances.frameInfo(Locb(i));
            foundRowStrct.additionalCols = knownDistances.additionalCols(Locb(i),:);
            foundRow = distMatQuantize_hospiSign([], foundRowStrct);
            searchRow = [srcMatRows(i,:) knownDistances_col11];
            if sum(abs(searchRow-foundRow(1:11)))==0
                d_i = searchMat(i,2);
                d_j = searchMat(i,3);
                distMat(d_i, d_j) = foundRow(12);
                cropIDs(d_i, d_j) = foundRow(13);
                cropIDs(d_j, d_i) = foundRow(14);
            else
                searchRow = [detailedLabels(searchMat(i,3),1:5) detailedLabels(searchMat(i,2),1:5) knownDistances_col11];
                if sum(abs(searchRow-foundRow(1:11)))==0
                    %eval('dbstop in fillDistMatWithKnownInformation_HospiSign at 40;');
                    d_i = searchMat(i,2);
                    d_j = searchMat(i,3);
                    distMat(d_i, d_j) = foundRow(12);
                    cropIDs(d_i, d_j) = foundRow(13);
                    cropIDs(d_j, d_i) = foundRow(14);
                else
                    %eval('dbstop in fillDistMatWithKnownInformation_HospiSign at 47;');
                    warning('sth wrong');
                end
            end
            if distMat(d_i, d_j)==0
                distMat(d_i, d_j) = eps;
                zero2eps = zero2eps + 1;
            end
        end
    end
    if zero2eps>0
        disp([num2str(zero2eps) ' of ' num2str(sum(Lia==1)) ' distances is changed from zero to eps']);
    end    
end

