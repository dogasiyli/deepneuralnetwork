function [filteredLabels] = filterLabelsByLength(mappedLabelVec, minBlockLengthAccepted)
%filterLabelsByLength Bound the big blocks
%   now I need to find the blocks of 3 and more
%   from begin to end if any 3 same ID comes make it a beginning of new label
%   any label begins with at least 3 labels and ends  stops before a new
%   label with the last occurance of itself
    
    uniqLabels = unique(mappedLabelVec);
    filteredLabels = zeros(size(mappedLabelVec));
    
    frID = find(mappedLabelVec~=0,1);  
    blocks = zeros(0,3);
    
    %[clusterCellString, labelCount] = getClusterSummaryAsString(mappedLabelVec);
    while frID<length(mappedLabelVec)
        curLabel = mappedLabelVec(frID);
        %find start of hsID
        blockLength = find(mappedLabelVec(frID:end)~=curLabel,1)-1;
        if ~isempty(blockLength)
            %there is a block between frID to toID-1
            toID = frID+blockLength-1;
        else
            toID = length(mappedLabelVec);
        end
        if blockLength>=minBlockLengthAccepted
           blocks = addToBlocks(blocks,curLabel,frID,toID);
        end        
        frID = toID+1;
    end
    for i=1:size(blocks,1)
        filteredLabels(blocks(i,2):blocks(i,3)) = blocks(i,1);
    end
end

function blocks = addToBlocks(blocks,curLabel,frID,toID)
    if isempty(blocks)
        blocks = [curLabel frID toID];
    elseif (blocks(end,1)==curLabel)
        blocks(end,3) = toID;
    else
        blocks = [blocks;curLabel frID toID];
    end
end

