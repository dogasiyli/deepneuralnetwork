function [summaryString, cycleMat] = findCyclicMotion(signID, labelSwitches_uniq )
    %iterate through all sequence string
    %for all label couples of 2 check if
    %121 or 212 exists
    %change 121s until none left and 212 until none left
    %if there exists it means there is a cyclic motion
    %then report how many cyclic motion found and how would coupling them
    %effect the unique string situation
    
  
    cycleMat = [];
    anyChangeDetected = false;
    handNames = {'left hand','right hands','both hands'};
    summaryString = '';
    for h=1:3
        %hands left, right, both
        allSequenceStrings = labelSwitches_uniq{h,1};
        seqCounts = labelSwitches_uniq{h,4};
        labels = unique([allSequenceStrings{1,:}]);
        numOfLabels = length(labels);
        noChangeDetectedYet = true;
        for i=1:numOfLabels-1
            for j = i+1 : numOfLabels
                %is there a cycle in allSequenceStrings, seqCounts check
                %by combining labels(i) and labels(j)
                [changeOfCounts, newSequenceStrings, newSeqCounts] = checkCycle(allSequenceStrings, seqCounts, [labels(i) labels(j)]);
                if isstruct(changeOfCounts)
                    if noChangeDetectedYet
                        noChangeDetectedYet = false;
                        anyChangeDetected = true;
                        summaryString = [summaryString  newlinefunc() '   As can be seen from \reftable{Table:OrderOfSign' num2str(signID,'%03d') '}, '];  %#ok<*AGROW>
                        summaryString = [summaryString  newlinefunc() '   When analyzing the ' handNames{h} ' sequence summary, following cycles were found :'];
                    end
                    
                    summaryString = [summaryString  newlinefunc() '   grouping labels(\textit{' labels(i) '},\textit{' labels(j) '}) made the new sequence string -' changeOfCounts.bestSeqStr '- where x represents loop(\textit{' [labels(i) labels(j)] '})'];
                    summaryString = [summaryString  newlinefunc() '   adds ' num2str(changeOfCounts.fcInc,'%d') ' similar sequences and makes ' num2str(changeOfCounts.fcNow,'%d') ' videos having the same sequence of labels'];
                    summaryString = [summaryString  newlinefunc() '   increases the percentage of same seq from \%' int2str((100*changeOfCounts.fcPercWas)) ' to \%' int2str((100*changeOfCounts.fcPercNow))];
                    cycleMat = [cycleMat; signID h i j changeOfCounts.fcWas changeOfCounts.fcInc changeOfCounts.fcNow changeOfCounts.fcPercWas changeOfCounts.fcPercInc changeOfCounts.fcPercNow];
                end
            end
        end
    end
    if ~anyChangeDetected
        summaryString = ['When analyzing the sign(' num2str(signID) ') sequence summaries, no cycles were found.'];
    end
end

function [changeOfCounts, newSequenceStrings, newSeqCounts] = checkCycle(allSequenceStrings, seqCounts, combineLetters)
    c1 = combineLetters(1);
    c2 = combineLetters(2);
    N = length(seqCounts);
    str1 = [c1 c2 c1];
    str2 = [c2 c1 c2];
    cycleFound = false;
    for i=1:N
        makeX = false;
        str1_cyc_pos = -1;
        str2_cyc_pos = -1; %#ok<NASGU>
        while (~isempty(str1_cyc_pos) && ~isempty(str1_cyc_pos))
            str1_cyc_pos = strfind(allSequenceStrings{1,i},str1);
            str2_cyc_pos = strfind(allSequenceStrings{1,i},str2);
            if ~isempty(str1_cyc_pos)
                allSequenceStrings{1,i} = [allSequenceStrings{1,i}(1:str1_cyc_pos(1)-1) c1 allSequenceStrings{1,i}(3+str1_cyc_pos(1):end)];
                cycleFound = true;
                makeX = true;
                continue
            end
            if ~isempty(str2_cyc_pos)
                allSequenceStrings{1,i} = [allSequenceStrings{1,i}(1:str2_cyc_pos(1)-1) c2 allSequenceStrings{1,i}(3+str2_cyc_pos(1):end)];
                cycleFound = true;
                makeX = true;
                continue
            end
        end
        if makeX
            allSequenceStrings{1,i} = strrep(allSequenceStrings{1,i},[c1 c2],'x');
            allSequenceStrings{1,i} = strrep(allSequenceStrings{1,i},[c2 c1],'x');
            allSequenceStrings{1,i} = strrep(allSequenceStrings{1,i},c1,'x');
            allSequenceStrings{1,i} = strrep(allSequenceStrings{1,i},c2,'x');
        end
    end
    
    if ~cycleFound
        changeOfCounts = [];
        newSequenceStrings = [];
        newSeqCounts = [];
        return
    else
        for i=1:N
            allSequenceStrings{1,i} = strrep(allSequenceStrings{1,i},[c1 c2],'x');
            allSequenceStrings{1,i} = strrep(allSequenceStrings{1,i},[c2 c1],'x');
        end        
    end
    
    labelSwitches_uniq = cell(1,4);
    [newSequenceStrings,labelSwitches_uniq{1,2},labelSwitches_uniq{1,3}]  = unique(allSequenceStrings);
    newSeqCounts = hist(labelSwitches_uniq{1,3},unique(labelSwitches_uniq{1,3})); 
    for i=1:length(newSeqCounts)
        newSeqCounts(i) = sum(seqCounts(labelSwitches_uniq{1,3}==i));
    end
    [mostSeqCnt, mostSeqID] = max(newSeqCounts);
    
    changeOfCounts = max(newSeqCounts) - max(seqCounts);
    if changeOfCounts>0
        changeOfCounts = struct;
        changeOfCounts.fcWas = max(seqCounts);
        changeOfCounts.fcNow = max(newSeqCounts);
        changeOfCounts.bestSeqStr = newSequenceStrings{mostSeqID};
        changeOfCounts.fcInc = max(newSeqCounts) - max(seqCounts);
        changeOfCounts.fcPercWas = max(seqCounts)/sum(seqCounts);
        changeOfCounts.fcPercNow = max(newSeqCounts)/sum(seqCounts);
        changeOfCounts.fcPercInc = changeOfCounts.fcPercNow - changeOfCounts.fcPercWas;
    end
end