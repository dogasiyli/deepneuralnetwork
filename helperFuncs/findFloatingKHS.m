function [summaryString, floatingMat] = findFloatingKHS(signID, labelSwitches_uniq)
    %iterate through all sequence string
    %for all labels check if
    %when removing 1 label increases the count of similar sequences
    %then report how would removing them
    %effect the unique string situation
    
  
    floatingMat = [];
    anyChangeDetected = false;
    handNames = {'left hand','right hands','both hands'};
    summaryString = '';
    for h=1:3
        %hands left, right, both
        allSequenceStrings = labelSwitches_uniq{h,1};
        seqCounts = labelSwitches_uniq{h,4};
        labels = unique([allSequenceStrings{1,:}]);
        numOfLabels = length(labels);
        noChangeDetectedYet = true;
        for i=2:numOfLabels-1
            %is removing this label increasing
            [changeOfCounts, newSequenceStrings, newSeqCounts] = checkFloatingLabel(allSequenceStrings, seqCounts, labels(i));
            if isstruct(changeOfCounts)
                if noChangeDetectedYet
                    noChangeDetectedYet = false;
                    anyChangeDetected = true;
                    summaryString = [summaryString  newlinefunc() '   As can be seen from \reftable{Table:OrderOfSign' num2str(signID,'%03d') '}, '];  %#ok<*AGROW>
                    summaryString = [summaryString  newlinefunc() '   When analyzing the ' handNames{h} ' sequence summary; '];
                end

                summaryString = [summaryString  newlinefunc() '   removal of label(\textit{' labels(i) '}) made the new sequence string -' changeOfCounts.bestSeqStr];
                summaryString = [summaryString  newlinefunc() '   which adds ' num2str(changeOfCounts.fcInc,'%d') ' similar sequences and makes ' num2str(changeOfCounts.fcNow,'%d') ' videos having the same sequence of labels'];
                summaryString = [summaryString  newlinefunc() '   increases the percentage of same seq from \%' int2str((100*changeOfCounts.fcPercWas)) ' to \%' int2str((100*changeOfCounts.fcPercNow))];
                floatingMat = [floatingMat; signID h i changeOfCounts.fcWas changeOfCounts.fcInc changeOfCounts.fcNow changeOfCounts.fcPercWas changeOfCounts.fcPercInc changeOfCounts.fcPercNow];
            end
        end
    end
    if ~anyChangeDetected
        summaryString = ['When analyzing the sign(' num2str(signID) ') sequence summaries, no cycles were found.'];
    end
end

function [changeOfCounts, newSequenceStrings, newSeqCounts] = checkFloatingLabel(allSequenceStrings, seqCounts, c1)
    c1 = c1(1);
    N = length(seqCounts);
    for i=1:N
        allSequenceStrings{1,i} = strrep(allSequenceStrings{1,i},c1,'');
    end
    
    labelSwitches_uniq = cell(1,4);
    [newSequenceStrings,labelSwitches_uniq{1,2},labelSwitches_uniq{1,3}]  = unique(allSequenceStrings);
    newSeqCounts = hist(labelSwitches_uniq{1,3},unique(labelSwitches_uniq{1,3})); 
    for i=1:length(newSeqCounts)
        newSeqCounts(i) = sum(seqCounts(labelSwitches_uniq{1,3}==i));
    end
    [mostSeqCnt, mostSeqID] = max(newSeqCounts);
    
    changeOfCounts = max(newSeqCounts) - max(seqCounts);
    if changeOfCounts>1
        changeOfCounts = struct;
        changeOfCounts.fcWas = max(seqCounts);
        changeOfCounts.fcNow = max(newSeqCounts);
        changeOfCounts.bestSeqStr = newSequenceStrings{mostSeqID};
        changeOfCounts.fcInc = max(newSeqCounts) - max(seqCounts);
        changeOfCounts.fcPercWas = max(seqCounts)/sum(seqCounts);
        changeOfCounts.fcPercNow = max(newSeqCounts)/sum(seqCounts);
        changeOfCounts.fcPercInc = changeOfCounts.fcPercNow - changeOfCounts.fcPercWas;
    end
end