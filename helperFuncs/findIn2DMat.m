function [ idxMat, valueToSarch ] = findIn2DMat(mat2D, valueToSarch, equal1_notequal0, dispLevel)
%findIn2DMat A straight forward way to find a value in 2d matrix and
%display it
    if ~exist('equal1_notequal0','var') || isempty(equal1_notequal0)
        equal1_notequal0 = 1;
    end
    if equal1_notequal0==1
        if isnan(valueToSarch)
            idx = find(isnan(mat2D));
        else
            idx = find(mat2D==valueToSarch);
        end
    else
        if isnan(valueToSarch)
            idx = find(~isnan(mat2D));
        else
            idx = find(mat2D~=valueToSarch);
        end
    end
    [x_idx,y_idx] = ind2sub(size(mat2D),idx);
    idxMat = [idx x_idx y_idx];
    if exist('dispLevel','var') && ~isempty(dispLevel)
        if isempty(idxMat)
            if equal1_notequal0==1
                disp(['Value(' num2str(valueToSarch) ') is not found in the 2D matrix passed']);
            else
                disp(['NotEqualToValue(' num2str(valueToSarch) ') is not found in the 2D matrix passed']);
            end
            return
        end
        if equal1_notequal0==1
            disp(['Value(' num2str(valueToSarch) ') in the 2D matix passed is found at:']);
        else
            disp(['NotEqualToValue(' num2str(valueToSarch) ') in the 2D matix passed is found at:']);
        end        
        disptable(idxMat, 'idx|rowID|colID');
    end
end

