function [summaryString, interchangeMat] = findInterchangableKHS(signID, labelSwitches_uniq )
    %iterate through all sequence string
    %for all label couples of 2 check if
    %when making l1 and l2 = x
    %if it increases the similar sequences then
    %report how would coupling them
    %effect the unique string situation
    
  
    interchangeMat = [];
    anyChangeDetected = false;
    handNames = {'left hand','right hands','both hands'};
    summaryString = '';
    for h=1:3
        %hands left, right, both
        allSequenceStrings = labelSwitches_uniq{h,1};
        seqCounts = labelSwitches_uniq{h,4};
        labels = unique([allSequenceStrings{1,:}]);
        numOfLabels = length(labels);
        noChangeDetectedYet = true;
        for i=2:numOfLabels-1 %no need for a
            for j = i+1 : numOfLabels
                %is there a interchangableCouple in allSequenceStrings, seqCounts check
                %by combining labels(i) and labels(j)
                [changeOfCounts, newSequenceStrings, newSeqCounts] = checkIKHS(allSequenceStrings, seqCounts, [labels(i) labels(j)]);
                if isstruct(changeOfCounts)
                    if noChangeDetectedYet
                        noChangeDetectedYet = false;
                        anyChangeDetected = true;
                        summaryString = [summaryString  newlinefunc() '   As can be seen from \reftable{Table:OrderOfSign' num2str(signID,'%03d') '}, '];  %#ok<*AGROW>
                        summaryString = [summaryString  newlinefunc() '   When analyzing the ' handNames{h} ' sequence summary, possible interchangable key-hand-shapes are listed :'];
                    end
                    
                    summaryString = [summaryString  newlinefunc() '   if labels(\textit{' labels(i) '},\textit{' labels(j) '}) are grouped into one key-hand-shape, the new sequence string -' changeOfCounts.bestSeqStr '- where x represents both labels'];
                    summaryString = [summaryString  newlinefunc() '   this grouping adds ' num2str(changeOfCounts.fcInc,'%d') ' similar sequences and makes ' num2str(changeOfCounts.fcNow,'%d') ' videos having the same sequence of labels'];
                    summaryString = [summaryString  newlinefunc() '   increases the percentage of same seq from \%' int2str((100*changeOfCounts.fcPercWas)) ' to \%' int2str((100*changeOfCounts.fcPercNow))];
                    interchangeMat = [interchangeMat; signID h i j changeOfCounts.fcWas changeOfCounts.fcInc changeOfCounts.fcNow changeOfCounts.fcPercWas changeOfCounts.fcPercInc changeOfCounts.fcPercNow];
                end
            end
        end
    end
    if ~anyChangeDetected
        summaryString = ['When analyzing the sign(' num2str(signID) ') sequence summaries, no possible interchangable-key-handshapes were found.'];
    end
end

function [changeOfCounts, newSequenceStrings, newSeqCounts] = checkIKHS(allSequenceStrings, seqCounts, combineLetters)
    c1 = combineLetters(1);
    c2 = combineLetters(2);
    N = length(seqCounts);
    ikhsFound = false;
    for i=1:N
        ikhs_pos = -1;
        %remove init a from begin and end
        f_B = allSequenceStrings{1,i}(1);
        f_L = allSequenceStrings{1,i}(end);
        if f_B == 'a'
            allSequenceStrings{1,i} = allSequenceStrings{1,i}(2:end);
        else
            f_B = '';
        end
        if f_L == 'a'
            allSequenceStrings{1,i} = allSequenceStrings{1,i}(1:end-1);
        else
            f_L = '';
        end
        allSequenceStrings{1,i} = strrep(allSequenceStrings{1,i},c1,'x');
        allSequenceStrings{1,i} = strrep(allSequenceStrings{1,i},c2,'x');
        while ~isempty(ikhs_pos)
            ikhs_pos = strfind(allSequenceStrings{1,i},'xx');
            if ~isempty(ikhs_pos)
                allSequenceStrings{1,i} = strrep(allSequenceStrings{1,i},'xx','x');
                ikhsFound = true;
                continue
            end
        end
        %add removed init a to begin and end
        allSequenceStrings{1,i} = [f_B allSequenceStrings{1,i} f_L];
    end
    
    if ~ikhsFound
        changeOfCounts = [];
        newSequenceStrings = [];
        newSeqCounts = [];
        return   
    end
    
    labelSwitches_uniq = cell(1,4);
    [newSequenceStrings,labelSwitches_uniq{1,2},labelSwitches_uniq{1,3}]  = unique(allSequenceStrings);
    newSeqCounts = hist(labelSwitches_uniq{1,3},unique(labelSwitches_uniq{1,3})); 
    for i=1:length(newSeqCounts)
        newSeqCounts(i) = sum(seqCounts(labelSwitches_uniq{1,3}==i));
    end
    [mostSeqCnt, mostSeqID] = max(newSeqCounts);
    
    changeOfCounts = max(newSeqCounts) - max(seqCounts);
    if changeOfCounts>1
        changeOfCounts = struct;
        changeOfCounts.fcWas = max(seqCounts);
        changeOfCounts.fcNow = max(newSeqCounts);
        changeOfCounts.bestSeqStr = newSequenceStrings{mostSeqID};
        changeOfCounts.fcInc = max(newSeqCounts) - max(seqCounts);
        changeOfCounts.fcPercWas = max(seqCounts)/sum(seqCounts);
        changeOfCounts.fcPercNow = max(newSeqCounts)/sum(seqCounts);
        changeOfCounts.fcPercInc = changeOfCounts.fcPercNow - changeOfCounts.fcPercWas;
    end
end