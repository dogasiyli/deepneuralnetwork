function [ remainingFeatIDs, resultSummary] = findIrrelevantFeatures(trainData, trainLabels, method)
    rankDeficient_b = true;
    [numOfSamples, featCount] = size(trainData);
    remainingFeatIDs = 1:featCount;
    resultSummary = struct;
    i=0;
    while rankDeficient_b
        i = i+1;
        try
            switch method
                case 'ELM'
                    [accELM, relevFeats_cur, irrelevFeats_cur, confMatELM, rankDeficient_b, resultSummary] = reduceELM(trainData, trainLabels, resultSummary, i);
                    [accLDA, ~, ~, confMatLDA, ~] = reduceLDA(trainData, trainLabels, resultSummary, i, remainingFeatIDs);
                case 'LDA'
                    [accLDA, relevFeats_cur, irrelevFeats_cur, confMatLDA, rankDeficient_b, resultSummary] = reduceLDA(trainData, trainLabels, resultSummary, i, remainingFeatIDs);
                    [accELM, ~, ~, confMatELM, ~] = reduceELM(trainData, trainLabels, resultSummary, i);
                case 'ELM_LDA'
                    if mod(i,2)==1
                        [accELM, relevFeats_cur, irrelevFeats_cur, confMatELM, rankDeficient_b, resultSummary] = reduceELM(trainData, trainLabels, resultSummary, i);
                        [accLDA, ~, ~, confMatLDA, ~] = reduceLDA(trainData, trainLabels, resultSummary, i, remainingFeatIDs);
                    else
                        [accLDA, relevFeats_cur, irrelevFeats_cur, confMatLDA, rankDeficient_b, resultSummary] = reduceLDA(trainData, trainLabels, resultSummary, i, remainingFeatIDs);
                        [accELM, ~, ~, confMatELM, ~] = reduceELM(trainData, trainLabels, resultSummary, i);
                    end
                case 'LDA_ELM'
                    if mod(i,2)==0
                        [accELM, relevFeats_cur, irrelevFeats_cur, confMatELM, rankDeficient_b, resultSummary] = reduceELM(trainData, trainLabels, resultSummary, i);
                        [accLDA, ~, ~, confMatLDA, ~] = reduceLDA(trainData, trainLabels, resultSummary, i, remainingFeatIDs);
                    else
                        [accLDA, relevFeats_cur, irrelevFeats_cur, confMatLDA, rankDeficient_b, resultSummary] = reduceLDA(trainData, trainLabels, resultSummary, i, remainingFeatIDs);
                        [accELM, ~, ~, confMatELM, ~] = reduceELM(trainData, trainLabels, resultSummary, i);
                    end
            end
            resultSummary(i).featCount = length(remainingFeatIDs);
            resultSummary(i).confMat = confMatELM;
            resultSummary(i).confMatLDA = confMatLDA;
            resultSummary(i).accELM = accELM;
            resultSummary(i).accLDA = accLDA;
            resultSummary(i).relevFeats = remainingFeatIDs(relevFeats_cur);            
        catch
            confMatDummy = NaN(length(unique(trainLabels)));
            resultSummary(i).featCount = length(remainingFeatIDs);
            resultSummary(i).confMat = confMatDummy;
            resultSummary(i).confMatLDA = confMatDummy;
            resultSummary(i).accELM = NaN;
            resultSummary(i).accLDA = NaN;
            resultSummary(i).relevFeats = remainingFeatIDs(relevFeats_cur);
            rankDeficient_b = false;
        end

        if rankDeficient_b
            resultSummary(i).irrelevFeatsReal = remainingFeatIDs(irrelevFeats_cur);
            trainData = trainData(:,relevFeats_cur);
            remainingFeatIDs(irrelevFeats_cur) = [];
        end
    end
end

function [accCur, relevFeats_cur, irrelevFeats_cur, confMatCur, rankDeficient_b, resultSummary] = reduceELM(trainData, trainLabels, resultSummary, i)
    [WelmCur, predCur, confMatCur, accCur, rankDeficient_b] = getELM_Weights_SoftMax(trainData, trainLabels);
    resultSummary(i).Welm = WelmCur(:,1);
    relevFeats_cur = find(abs(WelmCur(:,1))>0.001);
    irrelevFeats_cur = find(abs(WelmCur(:,1))<=0.001);
    rankDeficient_b = rankDeficient_b || ~isempty(irrelevFeats_cur);
end
function [accCur, relevFeats_cur, irrelevFeats_cur, confMatCur, rankDeficient_b, resultSummary] = reduceLDA(trainData, trainLabels, resultSummary, i, remainingFeatIDs)
    [W_3D, W, infoGain, relevFeats_cur, accPerMethod, bestMethodID_not1, methodIDPref, confMatAll] = ldaDG(trainData, trainLabels);
    accCur = accPerMethod(1)*100;
    confMatCur = confMatAll{methodIDPref(1),1};
    rankDeficient_b = (length(relevFeats_cur)~=length(remainingFeatIDs));
    resultSummary(i).bestMethodID = bestMethodID_not1;
    resultSummary(i).W_3D = W_3D;
    resultSummary(i).W = W;
    resultSummary(i).infoGain = infoGain;
    resultSummary(i).trainConfMatResult = confMatCur;
    irrelevFeats_cur = 1:length(remainingFeatIDs);
    irrelevFeats_cur(relevFeats_cur) = [];
end