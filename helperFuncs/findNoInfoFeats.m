function [irrelevantFeatCount, relevantFeats] = findNoInfoFeats( Sw, Sb)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    rCnt = length(Sw);
    irrelevantFeats = zeros(rCnt,5);%row,col,both
    irrelevantFeats(:,1) = (sum(Sw)==0)';%[1 by rCnt]
    irrelevantFeats(:,2) = sum(Sw,2)==0;%[rCnt by 1]
    irrelevantFeats(:,3) = (sum(Sb)==0)';%[1 by rCnt]
    irrelevantFeats(:,4) = sum(Sb,2)==0;%[rCnt by 1]
    irrelevantFeats(:,5) = sum(irrelevantFeats(:,1:4),2)==4;
    irrelevantFeatCount = sum(irrelevantFeats(:,5));
    relevantFeats = find(irrelevantFeats(:,5)==0);
end

