function [D_fixed, dWas] = fixDistanceMatrix(D)
    %D can be upper triangle or lower triangle
    %we want D to be symmetrixat lowerLeft and upperRight
    [d1,d2] = size(D);
    if d1~=d2
        error(['D(' num2str(d1) ' by ' num2str(d2) ') is not a distance matrix']);
    else
        d = d1;
        clear d1 d2
    end
    D_upperTriangle = D-triu(D,+1);
    D_upperTriangle(isnan(D_upperTriangle(:))) = 0;
    D_upperTriangle = sum(sum(abs(D_upperTriangle)))<eps;
    
    D_lowerTriangle = D-tril(D,-1);
    D_lowerTriangle(isnan(D_lowerTriangle(:))) = 0;
    D_lowerTriangle = sum(sum(abs(D_lowerTriangle)))<eps;
    if D_upperTriangle
        D_fixed = triu(D,+1) + triu(D,+1)';
        dWas = 'upper triangle distance matrix';
    elseif D_lowerTriangle
        D_fixed = tril(D,-1) + tril(D,-1)';
        dWas = 'lower triangle distance matrix';
    else
        D_fixed = D;
        dWas = 'correct distance matrix';
    end
    D_fixed(sub2ind([d,d],1:d,1:d)) = 0;% reduce chance of numerical problems
end