function [X, dataMode, sampleIndsVec, initIndicesOfBlocks] = generateDataFromDistMat(D, d, labels, sampleIndsVec, tryCMDScale)
%generateDataFromDistMat use cmdscale to generate data for 2d or 3d
% X = d(dimension) by n(number of samples) data
    [D, dWas] = fixDistanceMatrix(D); %#ok<ASGLU>
    if nargin<4 || ~exist('tryCMDScale','var') || isempty(tryCMDScale)
        tryCMDScale = false;
    end
    if tryCMDScale
        try
            X = cmdscale(D)';
            [p,n] = size(X);
            if d>p
                %the required dimension is bigger than smallest dimension cmdscale can give
                %hence do nothing
            else%d<p
                %the required dimension is less than smallest dimension cmdscale can give
                %so when we return X it will be bigger than expected
                %hence we will only return first d columns
                X = X(1:d,:);
            end
            dataMode = 'samples';
            sampleIndsVec = 1:n;
            initIndicesOfBlocks = 1;
        catch
            skipCMDScale = true;
        end
    else
        skipCMDScale = true;
    end
    if skipCMDScale
        [d1,d2] = size(D);
        assert(d1==d2,'not a valid distance matrix');
        if nargin<4 || ~exist('sampleIndsVec','var') || isempty(sampleIndsVec)
            sampleIndsVec = 1:d1;
        end
        %return X as an upper triangle distance matrix
        %by sorting members according to classes
        dataMode = 'uppertriangle';
        %according to labels swap the samples of X
        [~, idx] = sort(labels);
        [X, sampleIndsVec]= sortIndsDistMat(triu(D,+1), idx, sampleIndsVec);
        sortedLabels = labels(idx);
        uniqLabels = unique(sortedLabels);
        initIndicesOfBlocks = zeros(1,length(uniqLabels));
        for i=uniqLabels
            idOfClass_i = find(sortedLabels==i);
            %notIdOfClass_i = find();
            X(sortedLabels==i,sortedLabels~=i) = 0;
            %through the sorted X grab and resort elements such that 
            %either sum or max distance to others are on top
            curBlok = X(idOfClass_i,idOfClass_i);
            curBlokSum = sum(curBlok) + sum(curBlok,2)';
            [distSumVals, blok_idx] = sort(curBlokSum);
            [curBlok, curBlokSampleIndsVec]= sortIndsDistMat(curBlok, blok_idx, 1:length(idOfClass_i));
            
            sampleIndsVec_curBlok = sampleIndsVec(idOfClass_i);
            sampleIndsVec(idOfClass_i) = sampleIndsVec_curBlok(curBlokSampleIndsVec);
            X(idOfClass_i,idOfClass_i) = curBlok;
            
            initIndicesOfBlocks(uniqLabels==i) = sampleIndsVec(idOfClass_i(1));
        end
    end
end

