function [ pvCell, knownKHSlist ] = generateImagesForNeuralNet(signIDList, optParamStruct)

    if ~exist('optParamStruct', 'var') || ~isstruct(optParamStruct)
        optParamStruct = [];
    end
    if ~exist('signIDList', 'var') || isempty(signIDList)
        signIDList = getSignListAll();
    end
    pvCell = {[]};
    runSteps                    = getStructField(optParamStruct, 'runSteps', [true true]);
    srcFold                     = getStructField(optParamStruct, 'srcFold', getVariableByComputerName('srcFold'));
    userList                    = getStructField(optParamStruct, 'userList', 2:7);
    saveNewCroppedImagesTo      = getStructField(optParamStruct, 'saveToFoldRoot', '');
    saveNamedImagesTo           = getStructField(optParamStruct, 'saveNamedImagesTo', '');
    saveClusterImagesTo         = getStructField(optParamStruct, 'saveClusterImagesTo', '');
    framesOpts                  = getStructField(optParamStruct, 'framesOpts', []);
    resizeImVec                 = getStructField(optParamStruct, 'resizeImVec', [132 92]);
    single0_both1               = getStructField(optParamStruct, 'single0_both1', [0 1]);
    useClusterNameAsFolder      = getStructField(optParamStruct, 'useClusterNameAsFolder', false);
    combineSingleBoth           = getStructField(optParamStruct, 'combineSingleBoth', false);
    exportBP_ofBothHands        = getStructField(optParamStruct, 'exportBP_ofBothHands', false);   
    exportImagesMode            = getStructField(optParamStruct, 'exportImagesMode', 'exportByKHS');   
    exportStillHands            = getStructField(optParamStruct, 'exportStillHands', false);   
    exportOnlyLabelledHands     = getStructField(optParamStruct, 'exportOnlyLabelledHands', false);   
    dominantHandSearchModel     = getStructField(optParamStruct, 'dominantHandSearchModel', 'labels');%labels, stillFrames, skeleton
    pcaSizesArray               = getStructField(optParamStruct, 'pcaSizesArray', [256 512]);
    
    
    numOfSigns = length(signIDList);
    [saveNewCroppedImagesTo, saveNamedImagesTo, saveClusterImagesTo]= assign_saveNewCroppedImagesTo(saveNewCroppedImagesTo, saveNamedImagesTo, saveClusterImagesTo, resizeImVec, numOfSigns);
    framesOpts = assign_framesOpts(framesOpts);
    
    optParamStruct_cropVidFrames = struct('srcFold',srcFold,...
                                          'saveToFoldRoot',saveNewCroppedImagesTo,...
                                          'makeZippedFolder',false,...
                                          'removeUnzippedFolder',false,...
                                          'framesOpts', framesOpts,...
                                          'mirrorOnSaveStruct',struct('mirrorLH',true,'mirrorRH',false,'mirrorBH',false),...
                                          'multipliers',struct('bigHandFaceRatio',[9/8 3/2],'smlHandFaceRatio',[1, 2/3]),...
                                          'resizeImVec', resizeImVec,... 
                                          'enforceReCreateNew', false);
    

    
    %% 1. First we need to create crops to a destinaton
    pv_createCrops = [];
    if runSteps(1)==true
        for s = signIDList
            [surList, listElemntCnt] = createSURList(srcFold, 1, struct('signIDList',s,'userList',userList,'maxRptID',20));
            for l = 1:listElemntCnt
                try
                    u = surList(l,2);
                    r = surList(l,3);
                    cropFolderExist = cropVidFrames(s,u,r,optParamStruct_cropVidFrames);
                    [surfImArr_cell, ~] = getSurfNormFeatsofHands([s, u, r], struct('srcFold',srcFold,'skipStillFrames',true,'skipBothHands',true,'scales2Use',1.0,'makeEmptyFrames',false));
                catch
                    pv_createCrops = [pv_createCrops;s u r];
                end
            end
        end
    end
    pvCell{1} = pv_createCrops;
    clear pv_createCrops

    %% 2. Then we need to put images into folders
    if runSteps(2)
        switch exportImagesMode
            case 'exportByKHS'
                [pv_copyImages, numberOfFrames_cell_comb] = exportByKHS(signIDList, srcFold, saveClusterImagesTo, single0_both1, saveNewCroppedImagesTo, saveNamedImagesTo, userList, useClusterNameAsFolder, exportBP_ofBothHands, combineSingleBoth);
                pvCell{2} = pv_copyImages;
                numberOfFramesTable = cell2table(numberOfFrames_cell_comb,'VariableNames',{'nu', 'khsName', 'frameCnt', 'errFrameCnt'});
                numberOfFramesTable_name = ['frameCountlist_' num2str(min(signIDList)) '_' num2str(max(signIDList)) '.csv'];
                disp(['saving ' numberOfFramesTable_name])
                writetable(numberOfFramesTable,[saveNamedImagesTo filesep numberOfFramesTable_name])
            case 'exportByVideos'
                pv_createVideos = [];
                csev = [0 0 0 0];
                dh = [0 0];
                saveNamedImagesTo = strrep(saveNamedImagesTo, 'neuralNetHandImages', 'neuralNetHandVideos');
                createFolderIfNotExist(saveNamedImagesTo, true);
                %0 - dunno, 1-~cropFolderExist, 2-somelabelFilesMissing, 3-dominantHandID=0
                knownKHSlist = {};
                svfl_all = [];
                labelVecs_all = [];
                surfImArr_all = [];
                skel_all = [];
                for si = 1:length(signIDList)
                    [labelVecs_sign, svfl_sign, surfImArr_sign, skel_sign, knownKHSlist, csev, dh, pv_createVideos_curSign] = run4sign(signIDList, si, srcFold, userList, optParamStruct_cropVidFrames, saveNewCroppedImagesTo, dominantHandSearchModel, saveNamedImagesTo, exportStillHands, exportOnlyLabelledHands, knownKHSlist, csev, dh);
                    pv_createVideos = [pv_createVideos;pv_createVideos_curSign];
                    
                    uniq_labelVecs_sign = unique(labelVecs_sign);
                    uniq_labelVecs_sign(uniq_labelVecs_sign==0) = [];
                    usedKhs = knownKHSlist(uniq_labelVecs_sign,:) %#ok<NASGU>
                    labelVecs_all = [labelVecs_all labelVecs_sign]; %#ok<AGROW>
                    svfl_all = [svfl_all;svfl_sign]; %#ok<AGROW>
                    surfImArr_all = [surfImArr_all;surfImArr_sign]; %#ok<AGROW>
                    skel_all = [skel_all;skel_sign]; %#ok<AGROW>
                    assert(size(labelVecs_all,2)==size(surfImArr_all,1), "sizes dont match")
                    assert(size(labelVecs_all,2)==size(skel_all,1), "sizes dont match")        
                end
                disp(['csev = ' mat2str(csev)])
                disp(['dh = ' mat2str(dh)])
                pvCell{2} = pv_createVideos;
                knownKHSlist
                save([saveNamedImagesTo filesep 'labelVecs_' num2str(numOfSigns) '.mat'], 'labelVecs_all', 'knownKHSlist');
                save([saveNamedImagesTo filesep 'pv_createVideos_' num2str(numOfSigns) '.mat'], 'pv_createVideos');
                clear pv_createVideos   
                disp(['num of nan values in surfImArr_all = ' num2str(sum(isnan(surfImArr_all(:))))])
                surfImArr_all(isnan(surfImArr_all)) = 0;
                try
                    save([saveNamedImagesTo filesep 'snFeats_' num2str(numOfSigns) '.mat'], 'surfImArr_all', 'labelVecs_all', 'knownKHSlist');
                catch
                    save([saveNamedImagesTo filesep 'snFeats_' num2str(numOfSigns) '-v7.3.mat'], 'surfImArr_all', 'labelVecs_all', 'knownKHSlist', '-v7.3');
                end
                
                save([saveNamedImagesTo filesep 'skeletonFeats_' num2str(numOfSigns) '.mat'], 'skel_all', 'labelVecs_all', 'knownKHSlist');
                dlmwrite([saveNamedImagesTo filesep 'labelCSV_' num2str(numOfSigns) '.txt'], svfl_all, 'delimiter',',');
                
                getPcaOfData(surfImArr_all, pcaSizesArray, saveNamedImagesTo, 'surfImArr_all', labelVecs_all, knownKHSlist);
                knownKHSTable = cell2table(knownKHSlist,'VariableNames',{'nu', 'khsName', 'signs', 'signIDs'});
                knownKHSTable_name = ['knownKHSlist_' num2str(min(signIDList)) '_' num2str(max(signIDList)) '.csv'];
                disp(['saving ' knownKHSTable_name])
                writetable(knownKHSTable,[saveNamedImagesTo filesep knownKHSTable_name])
        end
    end

    if ~isempty(pvCell{1,2})
        pvCell_table = cell2table(pvCell{1,2},'VariableNames',{'sign', 'user', 'vid', 'errID', 'errExp'});
        pvCell_name = ['errTable_' num2str(min(signIDList)) '_' num2str(max(signIDList)) '.csv'];
        disp(['saving ' pvCell_name])
        writetable(pvCell_table,[saveNamedImagesTo filesep pvCell_name])   
    end
end

function skeletonMat = load_skel_from_sur(srcFold, sur)
    skeletonFile = [srcFold filesep num2str(sur(1)) filesep 'User_' num2str(sur(2)) '_' num2str(sur(3)) filesep 'skeleton.mat'];
    skeletonMat = loadSkeletonFileIfExist(skeletonFile);
end

function [labelVecs_sign, svfl_sign, surfImArr_sign, skel_sign, knownKHSlist, csev, dh, pv_createVideos] = run4sign(signIDList, si, srcFold, userList, optParamStruct_cropVidFrames, saveNewCroppedImagesTo, dominantHandSearchModel, saveNamedImagesTo, exportStillHands, exportOnlyLabelledHands, knownKHSlist, csev, dh)
    s = signIDList(si);
    pv_createVideos = [];
    svfl_sign = [];
    labelVecs_sign = [];
    surfImArr_sign = [];
    skel_sign = [];
    [surList, listElemntCnt] = createSURList(srcFold, 1, struct('signIDList',s,'userList',userList,'maxRptID',20));
    clusterNames_single = getClusterNamesAssignedFromTxt(srcFold, struct('signIDList',s,'single0_double1',0));
    v = 0;
    for l = 1:listElemntCnt
        try
            u = surList(l,2);
            r = surList(l,3);
            cropFolderExist = cropVidFrames(s,u,r,optParamStruct_cropVidFrames);
            try
                [surfImArr_cell, ~] = getSurfNormFeatsofHands([s, u, r], struct('srcFold',srcFold,'skipStillFrames',true,'skipBothHands',true,'scales2Use',1.0,'makeEmptyFrames',false));
            catch err
                pv_createVideos = [pv_createVideos;s u r 4 {err}]; 
                disp(['skipping due to error in surfImArr_cell ', mat2str([s u r 4])])
                displayErrorMessage(err);
                continue                
            end
            labelFolder = [srcFold filesep num2str(s) filesep 'User_' num2str(u) '_' num2str(r)];
            skeletonFile = [srcFold filesep num2str(s) filesep 'User_' num2str(u) '_' num2str(r) filesep 'skeleton.mat'];
            videoFolder = [saveNewCroppedImagesTo filesep num2str(s) filesep 'User_' num2str(u) '_' num2str(r)];
            labelVec_vid = [];
            if ~cropFolderExist
                pv_createVideos = [pv_createVideos;s u r 1 {'~cropFolderExist'}];
                disp(['skipping because cropFolder doesnt exist : ', mat2str([s u r 1])])
                continue
            end                       
            switch dominantHandSearchModel
                case'labels'
                    [someMissing, ~, labelCells] = checkLabelFiles(labelFolder, 0, false);
                    if someMissing
                        pv_createVideos = [pv_createVideos;s u r 2  {'~labels missing'}];
                        disp(['skipping because some labels Missing : ', mat2str([s u r 2])])
                        continue
                    end
                    %left and right has the labels
                    [dominantHandID, dominantHandLabels] = getDominantHandFromLabels(labelCells);
                    if dominantHandID==0
                        pv_createVideos = [pv_createVideos;s u r 3 {'~dominantHandID unknown'}];
                        disp(['skipping because dominantHandID unknown : ', mat2str([s u r 3])])
                        continue
                    end
                    %I know the dominant hand and can export this video
                    v = v + 1;
                    exportVideoImagesTo = [saveNamedImagesTo filesep num2str(si,'%02d') filesep 'v' num2str(v,'%02d')];%'??/neuralNetHandVideos/sign/vX';
                    retrieveHandImagesFrom = [videoFolder filesep 'cropped' ];
                    skeletonMat = loadSkeletonFileIfExist(skeletonFile);

                    [coppiedCnt, skippedCnt, errCnt, knownKHSlist, labelVec_vid, surfImArrSlcted, skeletonVid] = ...
                            exportVideoFrames(  [s u r], retrieveHandImagesFrom, exportVideoImagesTo, dominantHandLabels, dominantHandID, ...
                                                exportStillHands, exportOnlyLabelledHands, clusterNames_single, knownKHSlist, ...
                                                skeletonMat, surfImArr_cell);

                    svfl_vid = [si*ones(size(labelVec_vid')) v*ones(size(labelVec_vid')) labelVec_vid'];
                    csev = csev + [coppiedCnt, skippedCnt, errCnt, 1];
                    dh(dominantHandID) = dh(dominantHandID) + 1;
                case'stillFrames'
                    error('not implemented yet');
                case'skeleton'
                    error('not implemented yet');
            end
            labelVecs_sign = [labelVecs_sign labelVec_vid];
            surfImArr_sign = [surfImArr_sign;surfImArrSlcted];
            skel_sign = [skel_sign;skeletonVid];
            svfl_sign = [svfl_sign;svfl_vid];

            %check all outputs sizes
            assert(size(labelVec_vid,2)==size(surfImArrSlcted,1), 'sizes dont match')
            assert(size(labelVec_vid,2)==size(skeletonVid,1), 'sizes dont match')
        catch err
            displayErrorMessage(err);
            pv_createVideos = [pv_createVideos;s u r 0 {err.message}];
        end
        labelVec_vid = [];
        surfImArrSlcted = [];
        skeletonVid = [];
        svfl_vid = [];
    end
    disp(['for sign(' num2str(s) ') used khs are : ']);
end

function skeletonMat = loadSkeletonFileIfExist(skeletonFile)
    skeletonMat = [];
    if exist(skeletonFile,'file')  
        cols2slct = [1 2 3 8 9 10 11];
        skeleton = load(skeletonFile);
        skeleton = skeleton.skeleton;
        frameLen = size(skeleton.AnkleLeft,1);
        featLenPerJoint = length(cols2slct);
        skeletonMat = zeros(frameLen, featLenPerJoint*16);
        for f = 1:frameLen
            skeletonMat(f,:) = getSkeletonVec(skeleton, cols2slct, f);
        end
        skeletonMat = reshape(skeletonMat, [], featLenPerJoint);
        skeletonMat = reshape(skeletonMat, [], featLenPerJoint*16);
    end
end

function skelVec = getSkeletonVec(skeleton, cols2slct, frameID)
    skelVec = [...
     skeleton.SpineBase(frameID,cols2slct);skeleton.SpineMid(frameID,cols2slct);...
     skeleton.Neck(frameID,cols2slct);skeleton.Head(frameID,cols2slct);...
     skeleton.ShoulderLeft(frameID,cols2slct);skeleton.ElbowLeft(frameID,cols2slct);skeleton.WristLeft(frameID,cols2slct);skeleton.HandLeft(frameID,cols2slct);skeleton.HandTipLeft(frameID,cols2slct);...
     skeleton.ShoulderRight(frameID,cols2slct);skeleton.ElbowRight(frameID,cols2slct);skeleton.WristRight(frameID,cols2slct);skeleton.HandRight(frameID,cols2slct);skeleton.HandTipRight(frameID,cols2slct); ...
     skeleton.HipLeft(frameID,cols2slct);skeleton.HipRight(frameID,cols2slct)...
    ];
    minBB = min(skelVec);
    maxBB = max(skelVec);
    lenBB = maxBB-minBB;
    skelVec = skelVec-minBB;
    skelVec = skelVec./lenBB;
    skelVec = 2*(0.5-skelVec);
    skelVec = skelVec(:)';
end

function [coppiedCnt, skippedCnt, errCnt, knownKHSlist, exportedLabelIDs_mapped, surfImArrSlcted, skeletonSlctd] = exportVideoFrames(surVec, retrieveHandImagesFrom, exportVideoImagesTo, dominantHandLabels, dominantHandID, exportStillHands, exportOnlyLabelledHands, clusterNames_single, knownKHSlist, skeletonMat, surfImArr_cell)
    %exportVideoImagesTo -> '??/neuralNetHandVideos/si/vx';
    %newlyCroppedHandImages -> '??/newlyCroppedHandImages/sign/User_u_r';
    
    
    %surVec has the sign, user and repeatID
    %labelsVec has the ids of frames
    %handID = L1,R2,B3
    %handStr = 'L','R','H'
    %fromFolder = 'sur_NewCroppedImageFolder' = /home/dg/DataPath/newlyCroppedHandImages/7/User_2_1/cropped
    %toFolder = 'neuralNetClusterFolder' = /home/dg/DataPath/neuralNetHandImages/singlePalmUp
    
    coppiedCnt = 0;
    errCnt = 0;
    if dominantHandID==1
        handStr = 'L';
        surfImArrSlcted = surfImArr_cell(:,1);
    elseif dominantHandID==2
        handStr = 'R';
        surfImArrSlcted = surfImArr_cell(:,2);
    else
        return
    end
    if ~exist(retrieveHandImagesFrom,'dir')
        return
    end
    for i=1:length(surfImArrSlcted)
        if ~isempty(surfImArrSlcted{i})
            surfImArrSlcted{i} = surfImArrSlcted{i}(1,:);
        end
    end
    
    fl = getFileList(retrieveHandImagesFrom,'.png');
    imCnt = length(fl)/3;
    dominantHandLabels = dominantHandLabels(1:imCnt);
    
    createFolderIfNotExist(exportVideoImagesTo, true);

    %use dominantHandLabels to get which frames to export --> labelsVec
    labelsVec = 1:length(dominantHandLabels);
    removeFrames_1 = false(1,length(labelsVec));
    removeFrames_2 = false(1,length(labelsVec));
    if exportOnlyLabelledHands
        %do nothing
        removeFrames_1 = dominantHandLabels==0;
    end
    if ~exportStillHands
        removeFrames_2 = dominantHandLabels==1;
    end
    removeFrames = removeFrames_1 | removeFrames_2;
    labelsVec(removeFrames) = [];
    skippedCnt = sum(removeFrames);
    
    
    exportedLabelIDs = dominantHandLabels(labelsVec);
    fNameBase_1 = ['s' num2str(surVec(1), '%03d') '_u' num2str(surVec(2),'%d') '_r' num2str(surVec(3),'%02d')];
    labels_Dest = [exportVideoImagesTo filesep fNameBase_1 '_labels.txt'];
    compSys = getVariableByComputerName('systemType');
    switch compSys
        case 'Windows'
            [fileID, message] = fopen(labels_Dest,'w','n','UTF-8');
        otherwise%case 'Ubuntu'
            [fileID, message] = fopen(labels_Dest,'w','n','windows-1258'); 
    end
            
    for f = 1:length(labelsVec)
        frameID = labelsVec(f);
        fNameBase = [fNameBase_1 '_f' num2str(coppiedCnt, '%02d')];
        fileName_Src = [retrieveHandImagesFrom filesep 'crop_' handStr 'H_' num2str(frameID, '%03d') '.png'];
        fileName_Dest = [exportVideoImagesTo filesep fNameBase '.png'];
        try
            im = imread(fileName_Src);
            %im = rgb2gray(im);
            imwrite(im, fileName_Dest);
            coppiedCnt = coppiedCnt + 1;
        catch err
            errCnt = errCnt + 1;
        end        
    end

    labelsExported = unique(exportedLabelIDs);
    labelsExported(labelsExported==0) = [];
    clusterNamesExported = clusterNames_single(labelsExported);
    [knownKHSlist, labelMap] = addToExportedKHSList(surVec(1), knownKHSlist, clusterNamesExported, labelsExported);
    exportedLabelIDs_mapped = reSetLabels(exportedLabelIDs, [0 labelsExported], [0 labelMap]);
    
    if fileID>0
        fprintf(fileID,'%d\r\n',exportedLabelIDs_mapped);
        fclose(fileID);
    end
    
    skeletonSlctd = skeletonMat(labelsVec,:);
    skel_Dest = [exportVideoImagesTo filesep fNameBase_1 '_skel.txt'];
    csvwrite(skel_Dest, skeletonSlctd);
    %from python : 
    %import pandas as pd
    %data = pd.read_csv(file_name, header=None)
    
    surfImArrSlcted = surfImArrSlcted(labelsVec);
	surfImArrSlcted = cell2mat(surfImArrSlcted);      
end

function [knownKHSlist, labelMap] = addToExportedKHSList(signID, knownKHSlist, clusterNamesExported, labelsExported)
    labelExportedCount = length(labelsExported);
    labelMap = labelsExported;
    findEqual = false;
    searchBothWays = true;
    %knownKHSlist --> {khsID,'khsName','containsList with signID'}
    for lID = 1:labelExportedCount
        labelNameCurrent = clusterNamesExported{lID};
        rowIDs = [];
        if ~isempty(knownKHSlist)
            rowIDs = searchCellList(knownKHSlist(:,2), labelNameCurrent, findEqual, searchBothWays);
        end
        if length(rowIDs)>=1
            for ri = 1:length(rowIDs)
                r = rowIDs(ri);
                %is it equal or some part equals?
                if strcmpi(knownKHSlist{r,2},labelNameCurrent)==1
                    labelMap(lID) = knownKHSlist{r,1};
                    knownKHSlist{r,4} = unique([knownKHSlist{r,4} signID]);
                elseif contains(knownKHSlist{r,2},labelNameCurrent)==1
                    labelMap(lID) = knownKHSlist{r,1};
                    %knownKHSlist(r,2) = labelNameCurrent;
                    knownKHSlist{r,3} = [knownKHSlist(r,3) ',[' labelNameCurrent '-' num2str(signID) ']'];
                    knownKHSlist{r,4} = unique([knownKHSlist{r,4} signID]);
                end
            end
        else
%             if isempty(knownKHSlist)
%                 knownKHSlist = {};
%             end
            r = size(knownKHSlist,1) + 1;
            knownKHSlist{r,1} = r;
            labelMap(lID) = knownKHSlist{r,1};
            knownKHSlist{r,2} = labelNameCurrent;
            knownKHSlist{r,3} = ['[' labelNameCurrent '-' num2str(signID) ']'];
            knownKHSlist{r,4} = signID;
        end
    end
end

function [dominantHandID, dominantHandLabels] = getDominantHandFromLabels(labelCells)
    labsLeft  = labelCells{1};
    labsRight = labelCells{2};
    dominantHandLabels = [];
    
    labsLeft(labsLeft==0) = [];
    labsLeft(labsLeft==1) = [];
    cntUniq_left = length(unique(labsLeft));
    cnt_left = length(labsLeft);
    
    labsRight(labsRight==0) = [];
    labsRight(labsRight==1) = [];
    cntUniq_right = length(unique(labsRight));
    cnt_right = length(labsRight);
    
    if (cntUniq_left*cnt_left>=cntUniq_right*cnt_right)
        dominantHandID = 1;
        dominantHandLabels = labelCells{1};
    elseif (cntUniq_left*cnt_left<=cntUniq_right*cnt_right)
        dominantHandID = 2;
        dominantHandLabels = labelCells{2};
    else
        dominantHandID = 0;
    end
end

function save_append_file(file_name, mat2sa, enforce_rewrite)
    if ~exist('enforce_rewrite','var')
        enforce_rewrite = false;
    end
    if strcmpi(file_name, '')
        disp('save nothing..')
    end
    if contains(file_name, ".mat")
        if exist(file_name,'file') && ~enforce_rewrite
            a = load(file_name);
            disp(['append into ' file_name ' with ' mat2str(size(a.mat2sa)) ', additional ' mat2str(size(mat2sa))])
            mat2sa = [a.mat2sa;mat2sa];
        else
            disp(['save ' file_name ' -> ' mat2str(size(mat2sa))])
        end
        save(file_name, 'mat2sa');
    else
        if exist(file_name,'file') && ~enforce_rewrite
            fid = fopen(file_name,'at');
            disp(['append into ' file_name ' -> ' num2str(1+count(mat2sa,'\n'))])
            mat2sa = ['\n' mat2sa];
        else
            fid = fopen(file_name,'wt');
            disp(['save ' file_name ' -> ' num2str(1+count(mat2sa,'\n'))])
        end
        fprintf(fid,mat2sa);
        fclose(fid);       
    end
end

function [pv_copyImages, numberOfFrames_cell_comb] = exportByKHS(signIDList, srcFold, saveClusterImagesTo, single0_both1, saveNewCroppedImagesTo, saveNamedImagesTo, userList, useClusterNameAsFolder, exportBP_ofBothHands, combineSingleBoth)
    pv_copyImages = [];
    [singleSignSummary, bothSignSummary] = summarizeMultipleGestures(signIDList, struct('srcFold', srcFold ,'initialFigureID', 1, 'figureSaveFolder', saveClusterImagesTo));
    numberOfFrames_cell_comb = cell(0,4);

    for s0_b1 = single0_both1
        list_dict = '';
        skel_all = [];
        surfImArr_all = [];
        listOfFiles = '';
        if s0_b1==0
            if combineSingleBoth
                toCopyFold_sb = [saveNamedImagesTo filesep 'imgs'];
            else
                toCopyFold_sb = [saveNamedImagesTo filesep 'single'];
            end
            %remove singleStill and bothStill from summary lists
            labelNamesSummary = singleSignSummary.labelNamesSummary;
            [labelNamesSummary, rowID] = removeStill(labelNamesSummary, 'singleStill');
            if ~isempty(rowID)
                signNameAssignments = singleSignSummary.signNameAssignments;
                signNameAssignments(rowID,:) = [];
            end
        else    
            if combineSingleBoth
                toCopyFold_sb = [saveNamedImagesTo filesep 'imgs'];
            else
                toCopyFold_sb = [saveNamedImagesTo filesep 'both'];
            end
            %remove singleStill and bothStill from summary lists
            labelNamesSummary = bothSignSummary.labelNamesSummary;
            [labelNamesSummary, rowID] = removeStill(labelNamesSummary, 'bothStill');
            if ~isempty(rowID)
                signNameAssignments = bothSignSummary.signNameAssignments;
                signNameAssignments(rowID,:) = [];
            end
        end
        
        listOfFiles_fileName = [toCopyFold_sb filesep 'listOfFiles.txt'];
        list_dict_fileName = [toCopyFold_sb filesep 'list_dict.txt'];
        skel_fileName = [toCopyFold_sb filesep 'skel.mat'];
        sn_fileName = [toCopyFold_sb filesep 'sn.mat'];

        if exist(listOfFiles_fileName,'file')
            if (combineSingleBoth && s0_b1 == single0_both1(1)) || length(single0_both1)==1 || (~combineSingleBoth && length(single0_both1)>1)
                disp(['deleting file <' listOfFiles_fileName '>'])
                delete(listOfFiles_fileName);
                disp(['deleted file <' listOfFiles_fileName '>'])
                disp(['deleting file <' list_dict_fileName '>'])
                delete(list_dict_fileName);
                disp(['deleted file <' list_dict_fileName '>'])
                disp(['deleting file <' skel_fileName '>'])
                delete(skel_fileName);
                disp(['deleted file <' skel_fileName '>'])
                disp(['deleting file <' sn_fileName '>'])
                delete(sn_fileName);
                disp(['deleted file <' sn_fileName '>'])
            end
        end
        
        labelNameCount = size(labelNamesSummary,1);
        try %#ok<TRYNC>
            rmdir(toCopyFold_sb);
        end
        numberOfFrames_cell = cell(labelNameCount,4);
        for labelID = 1:labelNameCount
            list_dict_label = '';
            skel_label = [];
            surfImArr_label = [];

            clusterNameCurrent = labelNamesSummary{labelID,2};
            if combineSingleBoth && contains(clusterNameCurrent, 'Cift', 'IgnoreCase', false)
                disp(['using folder <' strrep(clusterNameCurrent, 'Cift', '') '>, due to *Cift* in ' clusterNameCurrent])
            end
            skipThisLabel = ~exportBP_ofBothHands && s0_b1==1 && contains(clusterNameCurrent, 'BP', 'IgnoreCase', false);
            if skipThisLabel
                disp(['skipping <' clusterNameCurrent '>, due to *BP* '])
                continue
            end
            if strcmpi(clusterNameCurrent,'meShow2') || strcmpi(clusterNameCurrent,'niceShow2')
                disp(['skipping <' clusterNameCurrent '>, due to *Show2* '])
                continue
            end
            clusterNameCurrent_foldstr = clusterNameCurrent;
            if useClusterNameAsFolder
                if combineSingleBoth && contains(clusterNameCurrent, 'Cift', 'IgnoreCase', false)
                    clusterNameCurrent_foldstr = strrep(clusterNameCurrent, 'Cift', '');
                end
                neuralNetClusterFolder = [toCopyFold_sb filesep clusterNameCurrent_foldstr];
                list_dict_label_fileName = [neuralNetClusterFolder filesep 'list_dict_' clusterNameCurrent '.txt'];
                skel_label_fileName = [neuralNetClusterFolder filesep 'skel_' clusterNameCurrent '.mat'];
                sn_label_fileName = [neuralNetClusterFolder filesep 'sn_' clusterNameCurrent '.mat'];
            else
                neuralNetClusterFolder = toCopyFold_sb;
                list_dict_label_fileName = '';
                skel_label_fileName = '';
                sn_label_fileName = '';
            end
            if ~exist(neuralNetClusterFolder,'dir') && ~skipThisLabel
                mkdir(neuralNetClusterFolder);
            end
            numberOfFrames_cell{labelID,1} = num2str(labelID);
            numberOfFrames_cell{labelID,2} = clusterNameCurrent;
            numberOfFrames_cell{labelID,3} = 0;
            numberOfFrames_cell{labelID,4} = 0; 
            
            valid = false;
            if useClusterNameAsFolder
                [valid, dict_vars] = validate_hand_imgs_fold( toCopyFold_sb , clusterNameCurrent_foldstr, clusterNameCurrent);
            end
            if valid
                skel_label = dict_vars.skel_label;
                surfImArr_label = dict_vars.surfImArr_label;
                list_dict_label = ['\n' strrep(dict_vars.list_dict_label, newline, '\n')];
                numberOfFrames_cell{labelID,3} = length(skel_label);
                numberOfFrames_cell{labelID,4} = 0;
                
                listOfFiles_cur = getFileList(neuralNetClusterFolder, '.png', '', false);
                listOfFiles_cur = strjoin(listOfFiles_cur, [' ** ' clusterNameCurrent ' ** ' num2str(labelID) '\n']);
                listOfFiles_cur = [listOfFiles_cur ' ** ' clusterNameCurrent ' ** ' num2str(labelID)];
                listOfFiles_cur = strrep(listOfFiles_cur, [clusterNameCurrent_foldstr '_'], ['FOLDROOT' filesep clusterNameCurrent_foldstr filesep clusterNameCurrent_foldstr '_']);
                listOfFiles_cur = strrep(listOfFiles_cur, filesep, [filesep filesep]);
                listOfFiles_cur = strrep(listOfFiles_cur, newline, '\n');
                if isempty(listOfFiles)
                    listOfFiles = listOfFiles_cur;
                else
                    listOfFiles = [listOfFiles '\n' listOfFiles_cur];
                end
                %txtToAppend = ['FOLDROOT' filesep filesep clusterName filesep filesep fNameBase ' ** ' clusterNameCurrent ' ** ' num2str(labelID)];
            end            
            if ~valid
                if exist(skel_label_fileName, 'file')
                    disp(['deleting file <' skel_label_fileName '>'])
                    delete(skel_label_fileName);
                    disp(['deleted file <' skel_label_fileName '>'])
                end
                if exist(sn_label_fileName, 'file')
                    disp(['deleting file <' sn_label_fileName '>'])
                    delete(sn_label_fileName);
                    disp(['deleted file <' sn_label_fileName '>'])
                end
                if exist(list_dict_label_fileName, 'file')
                    disp(['deleting file <' list_dict_label_fileName '>'])
                    delete(list_dict_label_fileName);
                    disp(['deleted file <' list_dict_label_fileName '>'])
                end
                dispstat(['clusterName(' clusterNameCurrent ')'],'init');
                for s = signIDList
                    %does this sign have this clusters elements?
                    clustersASPInfoStruct = getClusterInfoFromASPTxt(srcFold, s);
                    if s0_b1==0
                        [labelsKnown, clusterNamesOfSign] = getDetailedLabelsFromClusterAspStruct(clustersASPInfoStruct, 'single');
                    else
                        [labelsKnown, clusterNamesOfSign] = getDetailedLabelsFromClusterAspStruct(clustersASPInfoStruct, 'both');
                    end
                    
                    clusterRow = [];
                    for i=1:length(clusterNamesOfSign)
                        if strcmpi(clusterNamesOfSign{i},clusterNameCurrent)
                            clusterRow=i;
                            break
                        end
                    end
                    if isempty(clusterRow)
                        continue
                    end

                    %it means this sign has samples of this cluster
                    [surList, listElemntCnt] = createSURList(srcFold, 1, struct('signIDList',s,'userList',userList,'maxRptID',20));
                    for l = 1:listElemntCnt
                        copiedFrameCount = [0 0 0];
                        problematicFrameCount = [0 0 0];
                        try
                            labelsLH = [];labelsRH = [];labelsBH = [];
                            u = surList(l,2);
                            r = surList(l,3);
                            userFolderStr = [num2str(s) filesep 'User_' num2str(u) '_' num2str(r)];
                            sur_NewCroppedImageFolder = [saveNewCroppedImagesTo filesep userFolderStr filesep 'cropped'];
                            labelSummary = getlabelSummary(srcFold, [s u r], 1, false);
                            if s0_b1==0
                                labelsLH = find(labelSummary.labelCells{1}==clusterRow);
                                labelsRH = find(labelSummary.labelCells{2}==clusterRow);
                                labelsBH = [];
                            else
                                labelsBH = find(labelSummary.labelCells{3}==clusterRow);
                                labelsLH = [];
                                labelsRH = [];
                            end
                            %now for each true in {labelsLH,labelsRH,labelsBH} do
                            [copiedFrameCount(1), problematicFrameCount(1), listOfFiles, list_dict_label, skeletonSlctd_L, surfImArrSlcted_L] = copyHandFrames([s u r], labelsLH, 'L', 1, clusterNameCurrent, sur_NewCroppedImageFolder, neuralNetClusterFolder, listOfFiles, list_dict_label, labelID, srcFold);
                            [copiedFrameCount(2), problematicFrameCount(2), listOfFiles, list_dict_label, skeletonSlctd_R, surfImArrSlcted_R] = copyHandFrames([s u r], labelsRH, 'R', 2, clusterNameCurrent, sur_NewCroppedImageFolder, neuralNetClusterFolder, listOfFiles, list_dict_label, labelID, srcFold);
                            [copiedFrameCount(3), problematicFrameCount(3), listOfFiles, list_dict_label, skeletonSlctd_B, surfImArrSlcted_B] = copyHandFrames([s u r], labelsBH, 'B', 3, clusterNameCurrent, sur_NewCroppedImageFolder, neuralNetClusterFolder, listOfFiles, list_dict_label, labelID, srcFold);

                            numberOfFrames_cell{labelID,3} = numberOfFrames_cell{labelID,3} + sum(copiedFrameCount);
                            numberOfFrames_cell{labelID,4} = numberOfFrames_cell{labelID,4} + sum(problematicFrameCount);

                            skel_label = [skel_label;skeletonSlctd_L;skeletonSlctd_R;skeletonSlctd_B];
                            surfImArr_label = [surfImArr_label;surfImArrSlcted_L;surfImArrSlcted_R;surfImArrSlcted_B];
                        catch err
                            dispstat(sprintf(newline),'keepprev');
                            disp(err.message)
                            pv_copyImages = [pv_copyImages;s0_b1 labelID labelNameCount s u r ];
                        end
                    end
                    dispstat(sprintf('clusterName(%s),sign(%s),copiedFrameCount(%s)',clusterNameCurrent,num2str(s),num2str(numberOfFrames_cell{labelID,3})));
                end
                %skel_label
                save_append_file(skel_label_fileName, skel_label);         
                %surfImArr_label
                save_append_file(sn_label_fileName, surfImArr_label);
                %if combineSingleBoth && contains(clusterNameCurrent, 'Cift', 'IgnoreCase', false) && useClusterNameAsFolder
                %    list_dict_label = strrep(list_dict_label, clusterNameCurrent, strrep(clusterNameCurrent, 'Cift', ''));
                %end
                save_append_file(list_dict_label_fileName, list_dict_label(3:end));

                skel_row_cnt = size(skel_label,1);
                surfImArr_row_cnt = size(surfImArr_label,1);
                list_dict_cnt = count(list_dict_label,clusterNameCurrent);
                dispstat(sprintf(newline),'keepprev');
                validate_hand_imgs_fold( toCopyFold_sb , clusterNameCurrent_foldstr, clusterNameCurrent);
            end
            if ~isempty(numberOfFrames_cell{labelID,1})
                numberOfFrames_cell_comb(size(numberOfFrames_cell_comb,1)+1,:)= numberOfFrames_cell(labelID,:);
                numberOfFrames_cell_comb{end,1} = size(numberOfFrames_cell_comb,1);
                label_corrected = numberOfFrames_cell_comb{end,1};
                if s0_b1==1
                    for z = 1:size(numberOfFrames_cell_comb,1)-1
                        if contains(numberOfFrames_cell_comb{end,2}, numberOfFrames_cell_comb{z,2}) && contains(clusterNameCurrent, 'Cift', 'IgnoreCase', false)
                            numberOfFrames_cell_comb{end,1} = numberOfFrames_cell_comb{z,1};
                            label_corrected = numberOfFrames_cell_comb{end,1};
                        end
                    end
                end
                if label_corrected ~= labelID
                    % * 3 * frustratedCift * /labelID
                    % * 9 * frustratedCift * /numberOfFrames_cell_comb{z,1}
                    old_string = [' * ' num2str(labelID) ' * ' clusterNameCurrent ' * '];
                    new_string = [' * ' num2str(label_corrected) ' * ' clusterNameCurrent ' * '];
                    disp_w_style(strtrim(['changing <' strtrim(old_string) '> to <' strtrim(new_string) '> in list_dict_label']))
                    list_dict_label = strrep(list_dict_label,old_string,new_string);
                    save_append_file(list_dict_label_fileName, list_dict_label(3:end), true);

                    % ** frustratedCift ** 9/labelID
                    % ** frustratedCift ** 11/numberOfFrames_cell_comb{z,1}
                    old_string = [' ** ' clusterNameCurrent ' ** ' num2str(labelID)];
                    new_string = [' ** ' clusterNameCurrent ' ** ' num2str(label_corrected)];
                    disp_w_style(strtrim(['changing <' strtrim(old_string) '> to <' strtrim(new_string) '> in listOfFiles']))
                    listOfFiles = strrep(listOfFiles,old_string,new_string);                    
                end
            else
                disp_w_style('cant be here','_orange')
            end
            dispstat(sprintf('clusterName(%s),copiedFrameCount(%s),errorFrameCount(%s)\n',clusterNameCurrent,num2str(numberOfFrames_cell{labelID,3}),num2str(numberOfFrames_cell{labelID,4})),'keepthis');
            if valid 
                disp('*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*')
            else
                disp('*******************************')
            end
            
            list_dict = [list_dict list_dict_label]; %#ok<AGROW> 
            skel_all = [skel_all;skel_label];
            surfImArr_all = [surfImArr_all;surfImArr_label];
        end
        numberOfFrames_cell
        save_append_file(listOfFiles_fileName, listOfFiles);
        save_append_file(list_dict_fileName, list_dict(3:end));
        save_append_file(skel_fileName, skel_all);
        save_append_file(sn_fileName, surfImArr_all);

        totalFramesCount = sum(cell2mat(numberOfFrames_cell(:,3)))  
    end
    numberOfFrames_cell_comb
    totalFramesCount = sum(cell2mat(numberOfFrames_cell_comb(:,3)))
end

function [copiedFrameCount, problematicFrameCount, listOfFiles, list_dict, skeletonSlctd, surfImArrSlcted] = copyHandFrames(surVec, labelsVec, handStr, handIdx, clusterName, fromFolder, toFolder, listOfFiles, list_dict, labelID, srcFold)
    %surVec has the sign, user and repeatID
    %labelsVec has the ids of frames
    %handID = L1,R2,B3
    %handStr = 'L','R','H'
    %fromFolder = 'sur_NewCroppedImageFolder' = /home/dg/DataPath/newlyCroppedHandImages/7/User_2_1/cropped
    %toFolder = 'neuralNetClusterFolder' = /home/dg/DataPath/neuralNetHandImages/singlePalmUp
    copiedFrameCount = 0;
    problematicFrameCount = 0;
    if ~exist(fromFolder,'dir')
        return
    end
    if isempty(labelsVec)
        skeletonSlctd=[];
        surfImArrSlcted=[];
        return
    end
    % 1. load surfNorm for the surVec
    [surfImArr_cell, ~] = getSurfNormFeatsofHands(surVec, struct('srcFold',srcFold,'skipStillFrames',true,'skipBothHands',false,'scales2Use',1.0,'makeEmptyFrames',false));
    % 2. load skel for surVec
    skel_mat = load_skel_from_sur(srcFold, surVec);
    labelsVec_copied = labelsVec;
    for f = 1:length(labelsVec)
        frameID = labelsVec(f);
        fNameBase = [clusterName '_' num2str(surVec(1), '%03d') num2str(surVec(2),'%d') num2str(surVec(3),'%02d') '_' handStr '_' num2str(copiedFrameCount, '%02d') '_' num2str(frameID, '%03d')];
        fileName_Src = [fromFolder filesep 'crop_' handStr 'H_' num2str(frameID, '%03d') '.png'];
        fileName_Dest = [toFolder filesep fNameBase '.png'];
        txtToAppend = ['FOLDROOT' filesep filesep clusterName filesep filesep fNameBase ' ** ' clusterName ' ** ' num2str(labelID)];
        %signIdx, vidIdx, frIdx, labelIdx, --, this can only be exported in video mode
        %sign, user, rep, fr, labelId, --, this can be exported from here
        list_dict_append = [num2str(surVec(1),'%d') ' * ' num2str(surVec(2),'%d') ' * ' num2str(surVec(3),'%d')  ' * ' num2str(frameID,'%d') ' * '  num2str(labelID) ' * ' clusterName ' * ' handStr];
        try
            if ~exist(fileName_Dest,'file')
                copyfile(fileName_Src, fileName_Dest, 'f');
            end
            %disp(['copied(' fileName ')']);
            copiedFrameCount = copiedFrameCount + 1;
            listOfFiles = [listOfFiles '\n' txtToAppend]; %#ok<AGROW> 
            list_dict = [list_dict '\n' list_dict_append]; %#ok<AGROW> 
        catch err
            dispstat(sprintf(newline),'keepprev');
            labelsVec_copied(labelsVec_copied==frameID) = [];
            problematicFrameCount = problematicFrameCount + 1;
            disp(['err:', err.message])
        end        
    end
    skeletonSlctd = skel_mat(labelsVec_copied,:);
    surfImArr_cell = surfImArr_cell(labelsVec_copied,handIdx);
    surfImArrSlcted = cell2mat(cellfun(@(x) x(1,:), surfImArr_cell,'UniformOutput', false));
end

function [labelNamesSummary, rowToRemove] = removeStill(labelNamesSummary, khsName)%'singleStill'
    rowToRemove = find(not(cellfun('isempty',strfind(labelNamesSummary(:,2),khsName))));
    if ~isempty(rowToRemove)
        labelNamesSummary(rowToRemove,:) = [];
    end
end

function [saveNewCroppedImagesTo, saveNamedImagesTo, saveClusterImagesTo] = assign_saveNewCroppedImagesTo(saveNewCroppedImagesTo, saveNamedImagesTo, saveClusterImagesTo, resizeImVec, numOfSigns)
    if isempty(saveNewCroppedImagesTo) || strcmpi('',saveNewCroppedImagesTo)
        compName = getComputerName;
        if length(unique(resizeImVec))==1
            resizeStr = ['rs' num2str(resizeImVec(1))];
        else
            resizeStr = ['rs' num2str(resizeImVec(1)) '_' num2str(resizeImVec(2))];
        end
        
        nosStr = ['nos' num2str(numOfSigns,'%02d')];
        if strcmpi(saveNewCroppedImagesTo, '')
            switch compName
                case 'doga-MSISSD'
                    saveNewCroppedImagesTo = ['/mnt/USB_HDD_1TB/newlyCroppedHandImages_' resizeStr];
                case 'WsUbuntu05'
                    saveNewCroppedImagesTo = ['/home/dg/DataPath/newlyCroppedHandImages_' resizeStr];
                case 'doga-msi-ubu'
                    saveNewCroppedImagesTo = ['/home/doga/DataFolder/newlyCroppedHandImages_' resizeStr];
                otherwise
                    saveNewCroppedImagesTo = ['D:\newlyCroppedHandImages_' resizeStr];
            end
        end    
        if strcmpi(saveNamedImagesTo, '')
            switch compName
                case 'doga-MSISSD'
                    saveNamedImagesTo = ['/mnt/USB_HDD_1TB/neuralNetHandImages_' nosStr '_' resizeStr];
                case 'WsUbuntu05'
                    saveNamedImagesTo = ['/home/dg/DataPath/neuralNetHandImages_' nosStr '_' resizeStr];
                case 'doga-msi-ubu'
                    saveNamedImagesTo = ['/home/doga/DataFolder/neuralNetHandImages_' nosStr '_' resizeStr];
                otherwise
                    saveNamedImagesTo = ['D:\neuralNetHandImages_' nosStr '_' resizeStr];
            end
        end
        if strcmpi(saveClusterImagesTo, '')
            saveClusterImagesTo = [saveNamedImagesTo filesep 'clusterImgs'];
        end
    end
end

function framesOpts = assign_framesOpts(framesOpts)
    if isempty(framesOpts)
        framesOpts = struct('bigVideoShow',false,'bigVideoSaveScale',0,'show',false,'save',true);    
    end
end


