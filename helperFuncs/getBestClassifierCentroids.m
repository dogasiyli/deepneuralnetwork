function [out_medoids, out_cL, in_cL, in_acc, newAcc] = getBestClassifierCentroids(D, in_medoids, gtLabels)
    %now using D and clusterLabels_Mapped I have a confusion matrix and accuracy
    %I want to increase my accuracy
    %hence I will remove the sample that creates the most in-accuracy
    %add a new sample from the highest error of confusion matrix
    
    in_medoids = sort(in_medoids);
    [medoidTable, in_acc, in_cL, in_cL_mapped2gt] = getMedoidTable(D, in_medoids, gtLabels);
    
    [medoidTable, uselesMedoidIDs, mIdCouple] = fwf_findUselessMedoidVec(medoidTable, D, in_cL_mapped2gt, gtLabels);
    
    uselessMedoidCount = min(3,length(uselesMedoidIDs));
    medoidChangeResult = zeros(uselessMedoidCount,7);
    for i = 1:uselessMedoidCount
        um = uselesMedoidIDs(i);
        medoidTableRowBool = find(medoidTable(:,1)==um);
        [accTemp, medoidTemp, newGTCountTemp] = fwf_findBetterMedoid(um, D, in_medoids, gtLabels);
        medoidChangeResult(i,:) = [medoidTable(medoidTableRowBool,[1 3 4]),medoidTemp, newGTCountTemp, 1-accTemp, um]; %#ok<FNDSB>
    end
    medoidChangeResult = sortrows(medoidChangeResult, [-5 6 3 2]);
    %disptable(medoidChangeResult,'mID|cnt|true|newMedoid|gtCnt|err|uselessMedoidID',[],'%3.4f');
    
    newAcc = 1-medoidChangeResult(1,6);
    newMedoid = medoidChangeResult(1,4);
    newMedoidIDs_wo = sort(in_medoids(in_medoids~=medoidChangeResult(1,1)));
%     if find(mIdCouple==medoidChangeResult(1,7))
%         disp('One of the closest same class member medids is removed');
%     end
    
    if newAcc > in_acc
        out_medoids = sort([newMedoidIDs_wo newMedoid]);
        %[~, out_cL] = calculateEnergyFromDistMatWithMedoids(D, out_medoids);
        [~, out_acc, out_cL] = getMedoidTable(D, out_medoids, gtLabels);
    elseif newAcc == in_acc && rand > 0.5
        out_medoids = sort([newMedoidIDs_wo newMedoid]);
        %[~, out_cL] = calculateEnergyFromDistMatWithMedoids(D, out_medoids);
        [~, out_acc, out_cL] = getMedoidTable(D, out_medoids, gtLabels);
    else
        newAcc = in_acc;
        out_medoids = in_medoids;
        out_cL = in_cL;
    end  
end

%     allGT = unique(gtLabels);
%     mapGT = unique(in_cL_mapped2gt);
    
function [medoidTable, uselesMedoidIDs, mIdCouple] = fwf_findUselessMedoidVec(medoidTable, D, in_cL_mapped2gt, gtLabels)
    %1. mID, mappedLabelID, assignedSampleCount, correctSampleCount, falseSampleCount, falseSampleRatiomedoidIDx
    %   pick the worst from medoid table
    uselesMedoidIDs = medoidTable(medoidTable(:,4)<5,1)';
    if isempty(uselesMedoidIDs)
        medoidTable = sortrows(medoidTable, [-6 4]);
    %    disptable(medoidTable,'mID|mapLb|cnt|true|false|err|idx',[],'%3.4f');
        uselesMedoidIDs = medoidTable(1,1);
    end
    
    %2. check distances of medoid samples
    %   if the closest 2 samples have same ground truth label
    %   and
    %   there is a missing sample from any class in mapped
    mIds = sort(medoidTable(:,1));
    partOfD = D(mIds,mIds);
    [minVal, minIdx] = min(partOfD(:));
    [r,c] = ind2sub([length(mIds) length(mIds)], minIdx);
    mIdCouple = sort([mIds(r) mIds(c)]);
    mIdDist = D(mIdCouple(1),mIdCouple(2));
    if minVal==mIdDist %otherwise sth must be worng
        gtLabelsCouple = gtLabels(mIdCouple);
        if length(unique(gtLabelsCouple))==1
            uselesMedoidIDs = [uselesMedoidIDs mIdCouple];
            uselesMedoidIDs = unique(uselesMedoidIDs);
        else
            mIdCouple = [];
        end
    end
end

function [newAcc, newMedoid, newGTCount] = fwf_findBetterMedoid(theMostUselessMedoid_ID, D, medoidIDs, gtLabels)
    %after removing this I want to put in a new sample that would gather as
    %much sample from its own class as possible
    %so I can check for all samples which are not in the medoids
    %use a new that would give me the highest accuracy

    newMedoidCanBeList = 1:length(gtLabels);
    newMedoidCanBeList(medoidIDs) = [];
    newMedoidIDs_wo = medoidIDs(medoidIDs~=theMostUselessMedoid_ID);

    medoidListCount = length(newMedoidCanBeList);
    resultList = zeros(medoidListCount,3);
    %confMatTemp = cell(1,medoidListCount);
    for i = 1:medoidListCount
        mNew = newMedoidCanBeList(i);
        [~, cL_temp] = calculateEnergyFromDistMatWithMedoids(D, [newMedoidIDs_wo mNew]);
        try
            [~, ~, accTemp, outStruct] = groupClusterLabelsIntoGroundTruth(cL_temp, gtLabels, struct('printResults', false));
            mappedGTcount = length(unique(outStruct.clusLabels_Mapped));
        catch
            accTemp = -1;
            mappedGTcount = 0;
        end
        resultList(i,:) = [mNew accTemp mappedGTcount];
    end

    [resultList, idx] = sortrows(resultList, [-3 -2]);
    newMedoid = resultList(1,1);
    newAcc = resultList(1,2);
    newGTCount = resultList(1,3);
end