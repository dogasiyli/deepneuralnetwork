function [blobDef, MBlocks_draw] = getBlobDef( MDist, MBlocks )
%getBlobDef use the distance matrix MDist and the block matrix which gives
%us the so called still and moving blocks to get blobDefs

    if size(MBlocks,1)>size(MDist,1) && size(MBlocks,2)>size(MDist,2)
        allBlocksStruct = getBlockDistMat(MDist, MBlocks(1:end-1,1:end-1));
    else
        allBlocksStruct = getBlockDistMat(MDist, MBlocks);
    end
    
    blockMat_All = allBlocksStruct.meanDistBlockMat;%V
    blockTypes_All = allBlocksStruct.blockTypes;%T
    DTW_All = dtwCL(blockMat_All', false);
    block_Way = DTW_All.wayBack(:,[2 1]);%W
    newWayBack = fineTuneDTWMap(blockMat_All, blockTypes_All, block_Way);
    
    blockCnt = size(newWayBack,1);
    %MBlocks_draw = max(MDist(:))*ones(size(MBlocks));
    blobDef = zeros(blockCnt,7);%[1,2,3]-best match,distance; [4,5,6,7]-the area defined frR-toR frC-toC
    for i=1:blockCnt
        curBlockCell = allBlocksStruct.blockCells{newWayBack(i,1),newWayBack(i,2)};
        cbA = curBlockCell.blockArea;
        blobArea = MDist(cbA(1):cbA(2),cbA(3):cbA(4));
        %MBlocks_draw(cbA(1):cbA(2),cbA(3):cbA(4)) = curBlockCell.meanDistMatch;
        minValMatch_pos_val = getMinBestOfBox(blobArea, cbA(3:4), cbA(1:2));
        blobDef(i,:) = [minValMatch_pos_val cbA];
    end
    MBlocks_draw = getBlockDraw_From_blobDef(blobDef, MDist);
end

