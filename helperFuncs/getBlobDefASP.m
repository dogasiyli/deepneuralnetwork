function [blobDef, MBlocks_draw] = getBlobDefASP( MDist, MBlocks, labelsX, labelsY)
%getBlobDef use the distance matrix MDist and the block matrix which gives
%us the so called still and moving blocks to get blobDefs

    if size(MBlocks,1)>size(MDist,1) && size(MBlocks,2)>size(MDist,2)
        allBlocksStruct = getBlockDistMat(MDist, MBlocks(1:end-1,1:end-1));
    else
        allBlocksStruct = getBlockDistMat(MDist, MBlocks);
    end
    
    blockTypes_All = allBlocksStruct.blockTypes;%T
    block_Way_Labels = [];
    uniqLabelsAll = unique([labelsX labelsY]);
    uniqLabelsAll(uniqLabelsAll==0) = [];
    xi=1;xLastAdded = NaN;
    yj=1;yjMin = 1;
    addedInLastRow = false;
    while xi <= size(blockTypes_All,1)
        addedInPrevRow = addedInLastRow;
        addedInLastRow = false;
        xLastAdded = NaN;
        while yj <= size(blockTypes_All,2) && xi <= size(blockTypes_All,1)
            curBlockType = blockTypes_All(xi,yj);
            if curBlockType~=3
                yj = yj + 1;
                continue
            end
            %in this block do we have a match for labels?
            ba = allBlocksStruct.blockCells{xi, yj}.blockArea;
            labels_X_block = labelsX(ba(1):ba(2));
            labels_Y_block = labelsY(ba(3):ba(4));
            labX = round(mean(labels_X_block()));
            labY = round(mean(labels_Y_block()));
            if (labX == labY) && (labX~=0)
                block_Way_Labels = [block_Way_Labels;[xi yj labX]];
                if isnan(xLastAdded)
                elseif xLastAdded~=labX
                    yjMin = min(yj, size(blockTypes_All,2));
                else
                end
                xi = xi + 1;
                xLastAdded = labX;
                addedInLastRow = true;
                continue;
            end
            yj = yj + 1;
        end
        if (addedInPrevRow && ~addedInLastRow)
            yjMin = min(yjMin+1, size(blockTypes_All,2));
        end
        if ~addedInLastRow
            xi = xi+1;
        end
        yj = yjMin;
    end
 
    blockCnt = size(block_Way_Labels,1);
    %MBlocks_draw = max(MDist(:))*ones(size(MBlocks));
    blobDef = zeros(blockCnt,8);%[1,2,3]-best match,distance; [4,5,6,7]-the area defined frR-toR frC-toC, [8] label
    for i=1:blockCnt
        curBlockCell = allBlocksStruct.blockCells{block_Way_Labels(i,1),block_Way_Labels(i,2)};
        cbA = curBlockCell.blockArea;
        blobArea = MDist(cbA(1):cbA(2),cbA(3):cbA(4));
        %MBlocks_draw(cbA(1):cbA(2),cbA(3):cbA(4)) = curBlockCell.meanDistMatch;
        minValMatch_pos_val = getMinBestOfBox(blobArea, cbA(3:4), cbA(1:2));
        blobDef(i,:) = [minValMatch_pos_val cbA block_Way_Labels(i,end)];
    end
    MBlocks_draw = getBlockDraw_From_blobDef(blobDef(:,1:end-1), MDist);
end

