function [ blobDef, MBlocks_draw] = getBlobDef_OnlyStill( MDist, MBlocks )
%getBlobDef use the distance matrix MDist and the block matrix which gives
%us the so called still and moving blocks to get blobDefs
%   Detailed explanation goes here
    error('dont use this function.. it is erroneous');
    [blockDef, blockMat, stillBlockCnt] = getBlockDistMat(MDist, MBlocks);
    
    %first remove everything that are not still
    blockDef_stillInds = find(blockDef(:,6)==3);
    blockMat_Still = blockMat(blockDef_stillInds);
    blockMat_Still = reshape(blockMat_Still,[],stillBlockCnt(2));
    blockDef = blockDef(blockDef_stillInds,:);
    
    [blockMat_Combined, blockDef_Combined ] = getGreedyDTW( blockMat_Still, blockDef);
    
    
    
    [vecIDs, distanceMin] = dtwCL(blockMat_Combined', false);
    selectedBlocks = vecIDs.wayBack-1;
    
    selectedInds = sub2ind(size(blockMat_Combined'),selectedBlocks(:,1),selectedBlocks(:,2));
    blockDef_selected = blockDef_Combined(selectedInds,:);
    
    MBlocks_draw = max(MDist(:))*ones(size(MBlocks));
    
    blobCnt = size(blockDef_selected,1);
    blobDef = zeros(blobCnt,7);%[1,2,3]-best match,distance; [4,5,6,7]-the area defined frR-toR frC-toC
    for b = 1:blobCnt
        blobArea = MDist(blockDef_selected(b,1):blockDef_selected(b,2),blockDef_selected(b,3):blockDef_selected(b,4));
        MBlocks_draw(blockDef_selected(b,1):blockDef_selected(b,2),blockDef_selected(b,3):blockDef_selected(b,4)) = MDist(blockDef_selected(b,1):blockDef_selected(b,2),blockDef_selected(b,3):blockDef_selected(b,4));
        minValMatch_pos_val = getMinBestOfBox( blobArea, blockDef_selected(b,3:4), blockDef_selected(b,1:2));
        blobDef(b,:) = [minValMatch_pos_val blockDef_selected(b,1:4)];
    end
end
