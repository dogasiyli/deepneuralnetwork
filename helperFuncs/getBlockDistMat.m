function [allBlocksStruct, stillBlocksStruct] = getBlockDistMat( MDist, MBlocks )
%getBlockDistMat Summary of this function goes here
%   Detailed explanation goes here
    
    assert(size(MDist,1)==size(MBlocks,1) && size(MDist,2)==size(MBlocks,2),'sizes must be same');
    rMax = size(MDist,1);
    cMax = size(MDist,2);
    stillBlockCnt = [0 0];
    rFr = 1;
    cFr = 1;
    rTo = rFr - 1 + find(MBlocks(2:end,1)~=MBlocks(1,1),1);
    cTo = cFr - 1 + find(MBlocks(1,2:end)~=MBlocks(1,1),1);
    blockID = 1;
    blockDef_Vec = zeros(1,9);%[rFr rTo cFr cTo pixCnt blockValue minBlockVal meanBlockVal maxBlockVal]
    %                          [ 1   2   3   4     5        6           7          8             9    ]
    curBlockVal = MBlocks(rFr,cFr);
    %now span MBlocks for each block
    rID = 0;
    cID = 0;
    while ~isempty(rTo)
        rTo = rFr - 1 + rTo;
        rID = rID + 1;
        cID = 0;
        while ~isempty(cTo)
            cTo = cFr - 1 + cTo;
            cID = cID + 1;
            %[rFr:rTo,cFr:cTo]==blockID
            
            curBlock = MDist(rFr:rTo,cFr:cTo);
            dtw_curBlock = dtwCL(curBlock, false);
            
            %Find block values and assign
            minValBlock = min(curBlock(:));
            meanValBlock = mean([dtw_curBlock.Col_Mean dtw_curBlock.Row_Mean]);
            maxValBlock = max(curBlock(:));
            blockDef_Vec(blockID,:) = [rFr rTo cFr cTo ((cTo-cFr+1)+(rTo-rFr+1)) curBlockVal minValBlock meanValBlock maxValBlock];
            if curBlockVal==3
                if rID<=2
                    stillBlockCnt(2) = stillBlockCnt(2) + 1;
                end
                if cID<=2
                    stillBlockCnt(1) = stillBlockCnt(1) + 1;
                end
            end
            
            blockID = blockID + 1;
            MBlocks(rFr:rTo,cFr:cTo) = inf;
            
            %find next cTo
            cFr = min(cTo+1,cMax);
            curBlockVal = MBlocks(rFr,cFr);
            if cFr>=cMax
                cTo = [];
            else
                cTo = find(MBlocks(rFr,cFr+1:end)~=curBlockVal,1);
                if isempty(cTo) && cFr<=cMax-1
                    cTo = cMax-(cFr-1);
                end                
            end
        end
        
        %we need to go next row
        cFr = 1;
        rFr = min(rTo+1,rMax);
        curBlockVal = MBlocks(rFr,cFr);
        cTo = find(MBlocks(rFr,cFr+1:end)~=curBlockVal,1);
        curBlockVal = MBlocks(rFr,cFr);
        if rFr>=rMax
            rTo = [];
        else
            rTo = find(MBlocks(rFr+1:end,1)~=curBlockVal,1);
            if isempty(rTo) && rFr<=rMax-1
                rTo = rMax-(rFr-1);
            end                
        end
    end
    
    %now generate the matrix that can be walked by another function
    %a matrix of cell to be walked through by the user to pick or skip the
    %blocks
    allBlockCount = [rID,cID];
    meanDistBlockMat_ALL = zeros(allBlockCount);
    stillBlockSelect = false(allBlockCount);
    blockCells = cell(allBlockCount);
    for rowCell = 1 : allBlockCount(1)
        for colCell = 1 : allBlockCount(2)
            blockID = sub2ind([cID,rID], colCell, rowCell);%the abov code was run as col first, row second fashion. but Matlab uses the other way around
            blockCells{rowCell, colCell} = struct;
            blockCells{rowCell, colCell}.blockArea = blockDef_Vec(blockID,[1 2 3 4]);%[rFr rTo cFr cTo pixCnt blockValue minBlockVal meanBlockVal maxBlockVal]
            blockCells{rowCell, colCell}.imageSelectionCount = blockDef_Vec(blockID,5);
            blockCells{rowCell, colCell}.blockType = blockDef_Vec(blockID,6);
            blockCells{rowCell, colCell}.minDistMatch = blockDef_Vec(blockID,7);
            blockCells{rowCell, colCell}.meanDistMatch = blockDef_Vec(blockID,8);
            blockCells{rowCell, colCell}.maxDistMatch = blockDef_Vec(blockID,9);
            
            meanDistBlockMat_ALL(rowCell,colCell) = blockDef_Vec(blockID,8);
            if blockDef_Vec(blockID,6)==3
                stillBlockSelect(rowCell,colCell) = 1;
            end
        end
    end  
    allBlocksStruct = struct;
    allBlocksStruct.blockCounts = size(blockCells);
    allBlocksStruct.blockCells = blockCells;
    allBlocksStruct.meanDistBlockMat = cell2mat(cellfun( @(sas) sas.meanDistMatch, blockCells, 'uni', false ));
    allBlocksStruct.blockTypes = cell2mat(cellfun( @(sas) sas.blockType, blockCells, 'uni', false ));
    
    stillBlocksStruct = struct;
    stillBlocksStruct.blockCounts = stillBlockCnt;
    stillBlocksStruct.blockCells = reshape(blockCells(stillBlockSelect),stillBlockCnt);
    stillBlocksStruct.meanDistBlockMat = cell2mat(cellfun( @(sas) sas.meanDistMatch, stillBlocksStruct.blockCells, 'uni', false ));
    stillBlocksStruct.blockTypes = cell2mat(cellfun( @(sas) sas.blockType, stillBlocksStruct.blockCells, 'uni', false ));
end

% rMax = min(size(MDist,1),size(MBlocks,1));
% cMax = min(size(MDist,2),size(MBlocks,2));
% if rMax<size(MDist,1)
%     MDist = MDist(1:rMax,:);
% elseif rMax<size(MBlocks,1)
%     MBlocks = MBlocks(1:rMax,:);        
% end
% if cMax<size(MDist,2)
%     MDist = MDist(:,1:cMax);
% elseif cMax<size(MBlocks,2)
%     MBlocks = MBlocks(:,1:cMax);
% end
