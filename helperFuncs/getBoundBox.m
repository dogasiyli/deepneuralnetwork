function [boundBox, fitsIn] = getBoundBox(plateSize, mealSize, centerPoint)
%getBoundBox gives the bounding box to put "mealSize" matrix into
%"plateSize" matrix at "centerPoint" 

    if ~exist('centerPoint','var')
        centerPoint = round(plateSize./2);
    end

% boundBox = [frRow toRow frCol toCol]
    frRow = max(1, 1+floor(centerPoint(1) - mealSize(1)/2));
    toRow = frRow + mealSize(1) - 1;

    frCol = max(1, 1+floor(centerPoint(2) - mealSize(2)/2));
    toCol = frCol + mealSize(2) - 1;


    fitsIn = (plateSize(1)>=toRow) & (plateSize(2)>=toCol);

    boundBox = [frRow toRow frCol toCol];
end