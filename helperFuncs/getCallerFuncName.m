function [ callerFuncNameString ] = getCallerFuncName( funcID )
    st = dbstack;
    if (~exist('funcID','var') || isempty(funcID))
        funcID = min(size(st,1),3);
    end
    callerFuncNameString = [st(funcID, 1).file '->' st(funcID, 1).name '' ];
    callerFuncNameString = strrep(callerFuncNameString, '_', '-');
end

