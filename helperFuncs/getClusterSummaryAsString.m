function [clusterCellString, labelCount] = getClusterSummaryAsString(labels)
    uniqLabs = unique(labels);
    labelCount = length(uniqLabs);
    clusterCells = cell(labelCount,1);
    clusterCellString = cell(labelCount,2);
    for i=1:labelCount
        curLabel = uniqLabs(i);
        idx_cur = find(labels==curLabel);
        clusterCells{i} = idx_cur;
        clusterCellString{i,2} = getVecAsSequentialIDSummaryString(idx_cur);
        clusterCellString{i,1} = num2str(curLabel,'%03d');
        %now idx_cur has all the ids,
        %I need to group them as a string identifier
    end
end