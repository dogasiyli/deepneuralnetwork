function [handsToLabelStruct, selectedImCntMat]= getClusterableFrameIDs(signSourceFolder, signID, userID, signRepeatID, labelingStrategyStr)
    signSourceFolder_X = [signSourceFolder filesep num2str(signID) filesep 'User_' num2str(userID) '_' num2str(signRepeatID)];
       
    fileName_handUsagemat = [signSourceFolder_X filesep 'handUsage.mat'];
    fileName_skeletonMat = [signSourceFolder_X filesep 'skeleton.mat'];
    fileName_cropArea = [signSourceFolder_X filesep 'cropArea.mat'];
    if 0==exist(fileName_handUsagemat,'file') && exist(fileName_skeletonMat,'file')>0
        load(fileName_skeletonMat,'skeleton');
        optParamStruct_cropVidFrames = struct('enforceReCreateNew', false);
        cropArea = getGenerateCropArea(fileName_cropArea, skeleton, optParamStruct_cropVidFrames);%load(fileName_cropArea,'cropArea');
        handDisplacementMat = getHandDisplacementVec(skeleton, cropArea);
        save(fileName_handUsagemat, 'handDisplacementMat');
    elseif exist(fileName_handUsagemat,'file')
        load(fileName_handUsagemat, 'handDisplacementMat');
    else
        error(['Neither <' fileName_handUsagemat '> exists nor can it be created' ]);
    end
    
    if strcmpi(labelingStrategyStr,'fromAssignedLabels')
        makeEmptyFrames = false;
        [~, handLabelsStruct] = getHandDisplacementFromASPLabels(signSourceFolder, signID, userID, signRepeatID, handDisplacementMat, makeEmptyFrames);
        labelsVec_LH = handLabelsStruct.LH;
        labelsVec_RH = handLabelsStruct.RH;
        labelsVec_BH = handLabelsStruct.BH;
    elseif strcmpi(labelingStrategyStr,'assignedLabelsRemove1')
        makeEmptyFrames = false;
        [~, handLabelsStruct] = getHandDisplacementFromASPLabels(signSourceFolder, signID, userID, signRepeatID, handDisplacementMat, makeEmptyFrames);
        labelsVec_LH = remove1FromVec(handLabelsStruct.LH);
        labelsVec_RH = remove1FromVec(handLabelsStruct.RH);
        labelsVec_BH = remove1FromVec(handLabelsStruct.BH);
    elseif strcmpi(labelingStrategyStr,'assignedLabelsExcept1')
        makeEmptyFrames = false;
        [~, handLabelsStruct] = getHandDisplacementFromASPLabels(signSourceFolder, signID, userID, signRepeatID, handDisplacementMat, makeEmptyFrames);
        labelsVec_LH = except1FromVec(handLabelsStruct.LH);
        labelsVec_RH = except1FromVec(handLabelsStruct.RH);
        labelsVec_BH = except1FromVec(handLabelsStruct.BH);
    elseif strcmpi(labelingStrategyStr,'fromSkeletonOnly')
        labelsVec_LH = getLabelVec(handDisplacementMat.LH);
        labelsVec_RH = getLabelVec(handDisplacementMat.RH);
        labelsVec_BH = getLabelVec(handDisplacementMat.BH);        
        fixLHfromBH = find(labelsVec_LH<labelsVec_BH);%both hands seemed to be stable but left hand seems to be moving
        fixRHfromBH = find(labelsVec_RH<labelsVec_BH);%both hands seemed to be stable but left hand seems to be moving
        if ~isempty(fixLHfromBH)
            labelsVec_LH(fixLHfromBH) = 1;
            disp(['fixed ' num2str(length(fixLHfromBH)) ' frames of LH']);
        end
        if ~isempty(fixRHfromBH)
            labelsVec_RH(fixRHfromBH) = 1;
            disp(['fixed ' num2str(length(fixRHfromBH)) ' frames of RH']);
        end
    elseif strcmpi(labelingStrategyStr,'fromSkeletonStableAll')
        labelsVec_LH = getLabelVecStableAll(handDisplacementMat.LH);
        labelsVec_RH = getLabelVecStableAll(handDisplacementMat.RH);
        labelsVec_BH = getLabelVecStableAll(handDisplacementMat.BH);        
        fixLHfromBH = find(labelsVec_LH<labelsVec_BH);%both hands seemed to be stable but left hand seems to be moving
        fixRHfromBH = find(labelsVec_RH<labelsVec_BH);%both hands seemed to be stable but left hand seems to be moving
        if ~isempty(fixLHfromBH)
            labelsVec_LH(fixLHfromBH) = 1;
            disp(['fixed ' num2str(length(fixLHfromBH)) ' frames of LH']);
        end
        if ~isempty(fixRHfromBH)
            labelsVec_RH(fixRHfromBH) = 1;
            disp(['fixed ' num2str(length(fixRHfromBH)) ' frames of RH']);
        end
    elseif strcmpi(labelingStrategyStr,'all')
        labelsVec_LH = getLabelVecStableAll(handDisplacementMat.LH);
        labelsVec_RH = getLabelVecStableAll(handDisplacementMat.RH);
        labelsVec_BH = getLabelVecStableAll(handDisplacementMat.BH);  
        labelsVec_LH = ones(size(labelsVec_LH));
        labelsVec_RH = ones(size(labelsVec_RH));
        labelsVec_BH = ones(size(labelsVec_BH));
    end
    
    selectedImCntMat = [sum([(labelsVec_LH~=0)' (labelsVec_RH~=0)' (labelsVec_BH~=0)']) 3*length(labelsVec_LH)];
    selectedImCntMat = [sum(selectedImCntMat(1:3))/selectedImCntMat(4) selectedImCntMat(1:3) sum(selectedImCntMat(1:3)) selectedImCntMat(4)];
    
    disp(['According to handDisplacement matrix in <' signSourceFolder_X '>']);
    disptable(selectedImCntMat,'perc|LH|RH|BH|Slctd|Total');
    
    handsToLabelStruct.LH = labelsVec_LH;
    handsToLabelStruct.RH = labelsVec_RH;
    handsToLabelStruct.BH = labelsVec_BH;
    handsToLabelStruct.handsAreTogether = labelsVec_BH~=0;
end

function labVec = remove1FromVec(labVec)
    labVec(labVec==1) = 0;
end

function labVec = except1FromVec(labVec)
    labVec(labVec==1) = -1;
    labVec(labVec<=0) = labVec(labVec<=0) + 1;
end

function lv = getLabelVec(dV)
    %find the first zero after first non zero in dV
    %find the last zero before last non zeros start in dV 
    firstZero_01 = find(dV~=0, 1, 'first');
    secondZero_02 = find(dV(firstZero_01:end)==0, 1, 'first');
    initID = secondZero_02+firstZero_01-2;
    
    firstZero_01 = find(dV(end:-1:1)~=0, 1, 'first');
    secondZero_02 = find(dV(end-firstZero_01:-1:1)==0, 1, 'first');
    finalID = length(dV)-secondZero_02-firstZero_01+1;
    
    lv = zeros(size(dV));
    lv(initID:finalID) = dV(initID:finalID)==0;
end

function lv = getLabelVecStableAll(dV)
    lv = double(dV==0);
%     %find the first zero after first non zero in dV
%     %find the last zero before last non zeros start in dV 
%     firstZero_01 = find(dV~=0, 1, 'first');
%     secondZero_02 = find(dV(firstZero_01:end)==0, 1, 'first');
%     initID = secondZero_02+firstZero_01-2;
%     
%     firstZero_01 = find(dV(end:-1:1)~=0, 1, 'first');
%     secondZero_02 = find(dV(end-firstZero_01:-1:1)==0, 1, 'first');
%     finalID = length(dV)-secondZero_02-firstZero_01+1;
%     
%     lv = zeros(size(dV));
%     lv(initID:finalID) = dV(initID:finalID)==0;
end