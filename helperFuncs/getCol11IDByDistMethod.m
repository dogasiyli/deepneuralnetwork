function knownDistances_col11 = getCol11IDByDistMethod( featureChar2, distMethod )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    knownDistances_col11 = -1;
    switch distMethod
        case 'c3'
            if strcmpi(featureChar2,'sn')
                knownDistances_col11 = 1;
            else
                error('c3 can only be used with surfaceNormal features')
            end
        case 'c2'
            if strcmpi(featureChar2,'sn')
                knownDistances_col11 = 2;
            else
                error('c2 can only be used with surfaceNormal features')
            end
        case 'e3'
            if strcmpi(featureChar2,'sn')
                knownDistances_col11 = 3;
            elseif strcmpi(featureChar2,'hf')
                knownDistances_col11 = 4;
            else
                error('e3 can only be used with surfaceNormal or hog features')
            end            
        otherwise
            error('no id assigned for this type of distance method');
    end
end

