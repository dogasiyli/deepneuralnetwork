function compName = getComputerName
    [~, compName] = system('hostname');
    compName = compName(1:end-1);
    switch compName
        case 'Bogazici_DogaHP'
            %HP Laptop
        case {'DESKTOP-2AQGRCR','Win7_WS_64_UL'}
            %WorkPC  
        case {'DESKTOP-GCOE5AL','DGMSISSD','q-Bilgisayar','dgssdwork','IB7702060904'}
            %dgssdmsi  
        case {'dogamsilaptop','doga-msi-ubu'}
            %dogamsilaptop
        case {'doga-MSISSD'}
            %school-msi-ubuntu 
        case 'WsUbuntu05'
            %work station 05 at DHO
        case 'LUMBAR'
            %work station 05 at DHO
        otherwise
            warning(['Computer with name <' compName '> is not known']);
    end       
end