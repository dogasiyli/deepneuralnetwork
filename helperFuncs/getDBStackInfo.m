function stackInfo = getDBStackInfo(stackInds)
% MFILELINENR returns the current linenumber
    if ~exist('stackInds','var') || isempty(stackInds)
        stackInds = [2 2];
    end
    Stack  = dbstack;
    stackInfo = struct;
    stackInfo.lineNr = Stack(stackInds(1)).line;   % the line number of the calling <stackInds-1>th function in stack
    stackInfo.fileName = Stack(stackInds(2)).name;   % the line number of the calling <stackInds-1>th function in stack
end