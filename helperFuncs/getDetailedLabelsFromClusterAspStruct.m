function [singLabs, clusterNames_single] = getDetailedLabelsFromClusterAspStruct(clustersASPInfoStruct, singBothStr)

    eval(['clustersASPInfoStruct = clustersASPInfoStruct.' singBothStr ';']);

    singLabs = [];
    clusterNames_single = [];
    numOfRows = size(clustersASPInfoStruct,2);
    removeRows = [];
    for i=1:numOfRows
        %dLab_k = detailedLabels(im_k,:);%detailedLabels [signID userID repID lh1_rh2_bh3 imageIDInVideo]
        
        cName = clustersASPInfoStruct(1, i).clusterName;
        if strcmpi(cName,'garbage') || strcmpi(cName,'unknownGest')
            removeRows = [removeRows;i];
        end
        
        signID = str2double(clustersASPInfoStruct(1, i).representativeFrameSign);
        userID = str2double(clustersASPInfoStruct(1, i).representativeFrameUser);
        repID = str2double(clustersASPInfoStruct(1, i).representativeFrameRepeat);
        switch clustersASPInfoStruct(1, i).representativeFrameHand
            case 'LH'
                lh1_rh2_bh3 = 1;
            case 'RH'
                lh1_rh2_bh3 = 2;
            case 'BH'
                lh1_rh2_bh3 = 3;
        end
        imageIDInVideo = str2double(clustersASPInfoStruct(1, i).representativeFrameFrameID);
        dLab_k = [signID userID repID lh1_rh2_bh3 imageIDInVideo];
        singLabs = [singLabs;dLab_k];
        clusterNames_single = [clusterNames_single;{clustersASPInfoStruct(1, i).clusterName}];
    end
    
    singLabs(removeRows,:) = [];
    clusterNames_single(removeRows,:) = [];
end