function [vecIDs, distanceMin ] = getGreedyDTW( blockMat_Still, blockDef)
%getGreedyDTW find the shortest path on blockMat_Still such that every row
%and col will be selected at the end, also dynamic time warping conditions
%would be satisfied check dtwCL function for dtw conditions

    [rc,cc] = size(blockMat_Still);
    M = blockMat_Still;
    blockIDMat = zeros(rc,cc);
    %vecIDs = struct;
    %vecIDs.Row = zeros(rc,1);
    %vecIDs.Col = zeros(cc,1);
    bestRowIDsforEachColSample = inf(1,cc);%best matched row ids to each col
    bestColIDsforEachRowSample = inf(rc,1);%best matched col ids to each row
    notBestSelections = [];
    selectedCellCnt = 0;
%1. 
    while sum(M(:)~=inf)>0
        [minVal,minInds] = min(M(:));
        if selectedCellCnt==0
            rowSelected=1;
            colSelected=1;
        elseif selectedCellCnt==1
            rowSelected=size(blockMat_Still,2);
            colSelected=size(blockMat_Still,1);
        else
            [rowSelected,colSelected] = ind2sub(size(M),minInds);
        end
        %now can we select [rm,cm]?
        selectable = false;
        if bestRowIDsforEachColSample(colSelected)==inf
            bestRowIDsforEachColSample(colSelected)=rowSelected;
            selectable = true;
            M(rowSelected,colSelected) = inf;
        end
        if bestColIDsforEachRowSample(rowSelected)==inf
            bestColIDsforEachRowSample(rowSelected)=colSelected;
            selectable = true;
            M(rowSelected,colSelected) = inf;
        end
        if selectable == false
            notBestSelections = [notBestSelections;[rowSelected,colSelected]];
            M(rowSelected,colSelected) = inf;
        end        
        if bestRowIDsforEachColSample(colSelected)~=inf
            %no <ri,cj> after this row and before this col can be selected
            for ri=rowSelected+1:rc
                for cj=1:colSelected-1
                    if M(ri,cj) ~= inf
                       M(ri,cj) = inf;
                    end
                end
            end
        end
        if bestColIDsforEachRowSample(rowSelected)~=inf
            %no <ri,cj> after this row and before this col can be selected
            for cj=colSelected+1:cc
                for ri=1:rowSelected-1
                   if M(ri,cj) ~= inf
                       M(ri,cj) = inf;
                   end
                end
            end
        end
        selectedCellCnt = selectedCellCnt + 1;
        blockIDMat = assignBlockID(blockIDMat, rowSelected, colSelected);
    end
    
    wayBack = find(blockIDMat>0);
    [wbc,wbr] = ind2sub(size(blockIDMat),wayBack);
    wayBack = [wbc,wbr];

    vecIDs = struct;
    vecIDs.Row = bestRowIDsforEachColSample;
    vecIDs.Col = bestColIDsforEachRowSample;
    vecIDs.wayBack = wayBack;
    distanceMin = sum(blockMat_Still(blockIDMat(:)>0));
   
   
% idsAssigned_all = unique(blockIDMat(blockIDMat>0));
% blockMat_Combined = blockMat_Still;
% blockDef_Combined = zeros(length(idsAssigned_all),size(blockDef,2));
% for blockID=idsAssigned_all
%     indsOfBlock = find(blockIDMat(:)==blockID);
%     [rowsOfBlock, colsOfBlock] = ind2sub(size(blockIDMat),indsOfBlock);
%     blockDef_Combined(blockID,:) = [min(blockDef(indsOfBlock,1)) max(blockDef(indsOfBlock,2)) min(blockDef(indsOfBlock,3)) max(blockDef(indsOfBlock,4)) sum(blockDef(indsOfBlock,5)) 3 min(blockDef(indsOfBlock,7)) max(blockDef(indsOfBlock,8))];
%     blockMat_Combined(rowsOfBlock,colsOfBlock) = min(min(blockMat_Combined(rowsOfBlock,colsOfBlock)));
% end
end

function blockIDMat = assignBlockID(blockIDMat, r, c)
    %1. if all zre zeros assign 1 to the new comer
    if sum(blockIDMat(:))==0
        blockIDMat(r, c) = 1;
        return
    end
    %2. if a row neighbour or col neighbour has a blockID this would
    %retrieve it
    neighbourIDs = NaN(3,3);
    rowUpNeighbour = r-1>0 && blockIDMat(r-1, c)>0;
    if rowUpNeighbour
        neighbourIDs(1,2) = blockIDMat(r-1, c);
    end
    rowDownNeighbour = r+1<size(blockIDMat,1) && blockIDMat(r+1, c)>0;
    if rowDownNeighbour
        neighbourIDs(3,2) = blockIDMat(r+1, c);
    end
    colLeftNeighbour = c-1>0 && blockIDMat(r, c-1)>0;
    if colLeftNeighbour
        neighbourIDs(2,1) = blockIDMat(r, c-1);
    end
    colRightNeighbour = c+1<size(blockIDMat,2) && blockIDMat(r, c+1)>0;
    if colRightNeighbour
        neighbourIDs(2,3) = blockIDMat(r, c+1);
    end
    idsAssigned_neighbours = unique(neighbourIDs(~isnan(neighbourIDs)));
    idsAssigned_all = unique(blockIDMat(blockIDMat>0));
    if ~isempty(idsAssigned_neighbours)
        %assign the minimum ID to all neighbours
        minNeighbourID = min(idsAssigned_neighbours);
        blockIDMat(r, c) = minNeighbourID;
        originalLabels = unique(blockIDMat(:));
        newLabels = originalLabels;
        newLabels(idsAssigned_neighbours+1) = minNeighbourID;
        blockIDMat = reSetLabels( blockIDMat, originalLabels, newLabels);
        originalLabels = sort(unique(blockIDMat(:)));
        newLabels = 0:length(originalLabels)-1;
        blockIDMat = reSetLabels( blockIDMat, originalLabels, newLabels);
    else
        blockIDMat(r, c) = max(idsAssigned_all(:))+1;
    end
end
