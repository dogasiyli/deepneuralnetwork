function newClusterID = getHandBasedClusterID(clustersStruct, clusterID)
    clusterTypes = clustersStruct.definition.clusterTypes;
    typeOfCluster2BFound = clusterTypes{clusterID,1};
    newClusterID = 0;
    for i=1:clusterID
        if strcmpi(clusterTypes{i,1},typeOfCluster2BFound)
            newClusterID = newClusterID + 1;
        end
    end
end

