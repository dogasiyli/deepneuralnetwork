function handDisplacementMat = getHandDisplacementVec(skeleton, cropArea, figureID)
%getCropArea According to skeleton information get the area information to
%be cropped
%   Kinect gives us left hand as right and right hand as left,
%   we can scale our calculations depending on the distance of Neck and
%   Head positions given 1 single frame
    
    hand_right_row_X = skeleton.HandLeft(:,8);
    hand_right_col_Y = skeleton.HandLeft(:,9);
    hand_left_row_X = skeleton.HandRight(:,8);
    hand_left_col_Y = skeleton.HandRight(:,9);
    
    %calculate displacements as mahalonobis distances
    dispSum_hrX = displacementSum(hand_right_row_X);
    dispSum_hlX = displacementSum(hand_left_row_X);
    dispSum_hrY = displacementSum(hand_right_col_Y);
    dispSum_hlY = displacementSum(hand_left_col_Y);
    
    dispSum_LH = dispSum_hlX + dispSum_hlY;
    dispSum_RH = dispSum_hrX + dispSum_hrY;
    dispSum_LH(dispSum_LH<mean(dispSum_LH)) = 0;
    dispSum_RH(dispSum_RH<mean(dispSum_RH)) = 0;
    dispSum_BH = dispSum_LH + dispSum_RH;
    
    if exist('figureID','var') && ~isempty(figureID) && figureID>0
        figure(2);clf;
        plot(dispSum_LH,'blue');hold on;
        plot(dispSum_RH,'red');
        plot(dispSum_BH,'black');
    else
        figureID = -1;
    end
    dispSum_BH(dispSum_BH<mean(dispSum_BH)) = 0;
    dispSum_BH((double(dispSum_RH>0) + double(dispSum_LH>0)) < 2 ) = 0;
    if figureID>0
        plot(dispSum_BH,'cyan');
    end
    
    handDisplacementMat = struct;
    handDisplacementMat.LH = dispSum_LH;
    handDisplacementMat.RH = dispSum_RH;
    handDisplacementMat.BH = dispSum_BH;
    
    if exist('cropArea','var')
        frCnt = length(dispSum_LH);
        handsAreTogether = false(1,frCnt);
        for i=1:frCnt
            %disp([num2str(i) ' of ' num2str(frCnt) ' completed..'])     
            intersectPercent = cropbBoxCollide(cropArea.RH_sml(i,:), cropArea.LH_sml(i,:));
            handsAreTogether(i) = intersectPercent>0;
        end
        handDisplacementMat.handsAreTogether = handsAreTogether;
    end
end

function dispSum_135 = displacementSum(posVecXY)
    posVecXY_frame01 = abs(posVecXY(2:end)-posVecXY(1:end-1))';
    posVecXY_frame03 = abs(posVecXY(3:end)-posVecXY(1:end-2))';
    posVecXY_frame05 = abs(posVecXY(5:end)-posVecXY(1:end-4))';
    dispSum_135 = zeros(1,length(posVecXY));
    dispSum_135(2:end) = posVecXY_frame01;
    dispSum_135(2:end-1) = dispSum_135(2:end-1) + posVecXY_frame03;
    dispSum_135(3:end-2) = dispSum_135(3:end-2) + posVecXY_frame05;
end
