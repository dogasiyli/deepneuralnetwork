function retStr = getHowOlderAsString(fileInfo_01, fileInfo_02)
    difDateNum = abs(fileInfo_01.datenum - fileInfo_02.datenum);
    [sY, sM, sD, sH, sMN, sS] = datevec(difDateNum); 
    retStr = 'file is ';
    if (sY>0)
        retStr = [retStr num2str(sY,'%d') ' year'];
        if (sY>1)
            retStr = [retStr 's, '];
        else
            retStr = [retStr ', '];
        end
    end
    if (sM>0)
        retStr = [retStr num2str(sM,'%d') ' month'];
        if (sM>1)
            retStr = [retStr 's, '];
        else
            retStr = [retStr ', '];
        end
    end
    if (sD>0)
        retStr = [retStr num2str(sD,'%d') ' day'];
        if (sD>1)
            retStr = [retStr 's, '];
        else
            retStr = [retStr ', '];
        end
    end
    if (sH>0)
        retStr = [retStr num2str(sH,'%d') ' hour'];
        if (sH>1)
            retStr = [retStr 's, '];
        else
            retStr = [retStr ', '];
        end
    end
    if (sMN>0)
        retStr = [retStr num2str(sMN,'%d') ' minute'];
        if (sMN>1)
            retStr = [retStr 's, '];
        else
            retStr = [retStr ', '];
        end
    end
    if (sS>0)
        retStr = [retStr num2str(sS,'%d') ' second'];
        if (sS>1)
            retStr = [retStr 's, '];
        else
            retStr = [retStr ', '];
        end
    end
    retStr = [retStr ' older'];
end