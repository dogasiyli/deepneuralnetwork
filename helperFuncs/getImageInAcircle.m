function imOut = getImageInAcircle(im, figID)
    circlePalette = [];
    load('whiteCircle600.mat');%loads circlePalette
    imSize= size(im);
    if (length(imSize)==3)
        %rgb
        circlePalette = cat(3, circlePalette, circlePalette, circlePalette);
        circlePalette = imresize(circlePalette, [imSize(1) imSize(2)]);
    else
        circlePalette = imresize(circlePalette, [imSize(1) imSize(2)]);
    end
    
    if exist('figID','var') && figID>0
        figure(figID);clf;hold on;
        subplot(1,3,1); imshow(im);
    end
    im = putImg_IntoBox(imresize(im,0.8), [imSize(1) imSize(2)], true, struct('thicknessParam',0.3,'colorParam',[1 1 1]));
    imOut = im.*cast(circlePalette,class(im));
    if exist('figID','var') && figID>0
        subplot(1,3,2); imshow(im);
        subplot(1,3,3); imshow(imOut);
    end
end

