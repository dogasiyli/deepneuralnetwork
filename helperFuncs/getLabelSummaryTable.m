function [listSummaryTotal, listSummarySpecial, listByUser, listByRepitition] = getLabelSummaryTable(labelSummary)

    surVec = labelSummary.surVec;
    labelCells = labelSummary.labelCells;
    
    uniqueLabels_s = unique([labelCells{:,1:2}]);
    uniqueLabels_b = unique([labelCells{:,3}]);
    uniqueUsers = unique(surVec(:,2));
    
    cnt_Labels_s = length(uniqueLabels_s);
    cnt_Labels_b = length(uniqueLabels_b);
    cnt_Labels_colCnt = cnt_Labels_s*2+cnt_Labels_b;
    cnt_Users = length(uniqueUsers);
    
    vidCnt = size(surVec,1);
        
    listByRepitition = zeros(vidCnt,3+cnt_Labels_colCnt);
    listByRepitition(:,1:3) = surVec;
    
    %cols - signID,userID,<labels>
    listByUser = zeros(cnt_Users,2+cnt_Labels_colCnt);
    listByUser(:,1) = surVec(1,1);
    listByUser(:,2) = uniqueUsers;
    
    listSummarySpecial = zeros(1+cnt_Users,2+cnt_Labels_colCnt);
    for handID = 1:3
        for i=1:vidCnt
            for j = 1:cnt_Labels_s
                labelID = uniqueLabels_s(j);
                labelCnt2Add = sum(labelCells{i,handID}==labelID);
                if handID<3
                    listByRepitition(i,3 + j + (handID-1)*cnt_Labels_s) = labelCnt2Add;
                else
                    listByRepitition(i,3 + j + 2*cnt_Labels_s) = labelCnt2Add;
                end
            end
            curUserID = surVec(i,2);
            curUserIndis = find(uniqueUsers==curUserID);
            if handID<3
                fr = 4 + (handID-1)*cnt_Labels_s;
                to = fr + cnt_Labels_s - 1;
                listByUser(curUserIndis,(fr:to)-1) = listByUser(curUserIndis,(fr:to)-1) + listByRepitition(i,fr:to);
            else
                fr = 4 + 2*cnt_Labels_s;
                to = fr + cnt_Labels_b - 1;
                listByUser(curUserIndis,(fr:to)-1) = listByUser(curUserIndis,(fr:to)-1) + listByRepitition(i,fr:to);
            end
            clear fr to
        end
    end
    listSummarySpecial(1:end-1,:) = listByUser;

    listSummarySpecial(end,1) = sum(listByUser(1,1));
    listSummarySpecial(end,3:end) = sum(listByUser(:,3:end));
    
    %cols - singleHand, bothHands
    %rows - labels
    listSummaryTotal = zeros(cnt_Labels_s,4);
    sumLeft = listSummarySpecial(end,3:3+cnt_Labels_s-1);
    sumRight = listSummarySpecial(end,3+cnt_Labels_s:3+2*cnt_Labels_s-1);
    sumBoth = listSummarySpecial(end,3+2*cnt_Labels_s:end);
    
    listSummaryTotal(1:cnt_Labels_s,1) = reshape(sumLeft+sumRight,[],1);
    listSummaryTotal(1:cnt_Labels_s,2) = reshape(sumLeft,[],1);
    listSummaryTotal(1:cnt_Labels_s,3) = reshape(sumRight,[],1);
    listSummaryTotal(1:cnt_Labels_b,4) = reshape(sumBoth,[],1);
    
    if cnt_Labels_s>cnt_Labels_b
        disptable(listSummaryTotal,'singleHand|rightHand|leftHand|bothHands',uniqueLabels_s);
    else
        disptable(listSummaryTotal,'singleHand|rightHand|leftHand|bothHands',uniqueLabels_b);
    end
end

