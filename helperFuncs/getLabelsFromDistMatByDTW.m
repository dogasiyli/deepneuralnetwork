function [ theLabels, theDistances, removedLabelCounts, meanRowAccepted] = getLabelsFromDistMatByDTW( distMat, detailedLabels_ColCentroids, dtwFlow)

    frameCnt = size(distMat,2);
    labelsUniq = unique(detailedLabels_ColCentroids(:,6));
    labelCnt = length(labelsUniq);
    
    distMatNew = NaN(labelCnt,frameCnt);
    
    for i=1:labelCnt
        curLabelRows = find(detailedLabels_ColCentroids(:,6)==i);
        distMatRow = min(distMat(curLabelRows,:));
        distMatNew(i,:) = distMatRow;
    end
    
    if ~exist('dtwFlow','var')
        dtwFlow = unique(detailedLabels_ColCentroids(:,end));
        dtwFlow(:,2) = 1/length(dtwFlow);
    end
       
    resultTable = NaN(5,5+labelCnt);
    if isempty(dtwFlow)
        L_M01 = zeros(frameCnt,1);
        D_M01 = inf(frameCnt,1);
        rLC_M01 = inf(labelCnt,1);
        resultTable(1,:) = inf(1,5+labelCnt);
    elseif sum(dtwFlow([1 end],1))==2 && size(dtwFlow,1)==1
        L_M01 = ones(frameCnt,1);
        D_M01 = distMatNew(1,:);
        rLC_M01 = zeros(labelCnt,1);
        resultTable(1,:) = [1 0 1 mean(D_M01) 1 reshape(rLC_M01,1,[])];
    elseif sum(dtwFlow([1 end],1))==2
        %dtwFlow starts and ends with still frames in it
        %M01 - use percentages of movement
        %M02 - only use the order
        [L_M01, D_M01, rLC_M01, dM_M01] =  getLabels_M01_usePercent_1X1(distMatNew, dtwFlow, frameCnt, labelCnt);
        resultTable(1,:) = [1 1 1 mean(D_M01(L_M01>1)) length(unique(rLC_M01)) reshape(rLC_M01,1,[])];
        [L_M02, D_M02, rLC_M02, dM_M02] =  getLabels_M02_noPercent_X(distMatNew, dtwFlow(dtwFlow(:,1)~=1,:), frameCnt, labelCnt);
        resultTable(2,:) = [1 0 1 mean(D_M02(L_M02>1)) length(unique(rLC_M02)) reshape(rLC_M02,1,[])];
    elseif sum(dtwFlow(:,1)==1)==0 && sum(dtwFlow(:,2))>=(1-eps)
        %dtwFlow doesnt have 1 in it
        %M03 - use percentages of labels except still frames
        [L_M03, D_M03, rLC_M03, dM_M03] =  getLabels_M03_usePercent_X(distMatNew, dtwFlow, frameCnt, labelCnt);
        resultTable(3,:) = [2 1 1 mean(D_M03(L_M03>1)) length(unique(rLC_M03)) reshape(rLC_M03,1,[])];
    elseif length(dtwFlow(:,1))==length(unique(dtwFlow(:,1))) && dtwFlow(1,end)==0
        [L_M04, D_M04, rLC_M04, dM_M04] =  getLabels_M03_usePercent_X(distMatNew, dtwFlow(2:end,:), frameCnt, labelCnt);
        resultTable(4,:) = [3 1 0 mean(D_M04(L_M04>1)) length(unique(rLC_M04)) reshape(rLC_M04,1,[])];
        [L_M05, D_M05, rLC_M05, dM_M05] =  getLabels_M02_noPercent_X(distMatNew, dtwFlow, frameCnt, labelCnt);
        resultTable(5,:) = [3 0 0 mean(D_M05(L_M05>1)) length(unique(rLC_M05)) reshape(rLC_M05,1,[])];
    else
        error('unexpected flow');
    end
    
    [resultTable, idx] = sortrows(resultTable, [4 -5]);
    resultTable(isnan(resultTable(:,1)),:) = [];
    col_strings = ['col|usePercent|real1_LR0|meanDist|labelCnt|' joinStrings(strread(num2str(1:labelCnt),'%s')','|' )];
    disptable(resultTable,col_strings);
    switch idx(1)
        case 1
            theLabels = L_M01;theDistances = D_M01;removedLabelCounts=rLC_M01;
        case 2
            theLabels = L_M02;theDistances = D_M02;removedLabelCounts=rLC_M02;
        case 3
            theLabels = L_M03;theDistances = D_M03;removedLabelCounts=rLC_M03;
        case 4
            theLabels = L_M04;theDistances = D_M04;removedLabelCounts=rLC_M04;
        case 5
            theLabels = L_M05;theDistances = D_M05;removedLabelCounts=rLC_M05;
        otherwise
    end
    meanRowAccepted = resultTable(1,1:5);
end

function [theLabels, theDistances, removedLabelCounts, distMat_M01] =  getLabels_M01_usePercent_1X1(distMatNew, dtwFlow, frameCnt, labelCnt)
    orderBasedOnPercentages = getOrderBasedOnPercent(dtwFlow, frameCnt);
    
    distMat_M01 = distMatNew(orderBasedOnPercentages,:);
    
    vecIDs = dtwCL(distMat_M01);%[vecIDs, distanceMin, D] 
    
    theLabels = orderBasedOnPercentages(vecIDs.Col);
    theDistances = distMat_M01(sub2ind(size(distMat_M01),vecIDs.Col',1:frameCnt));
    [theLabels, removedLabelCounts] =  removeLabelsBasedOnDistHistogram(theLabels, theDistances, labelCnt);
end

function [theLabels, theDistances, removedLabelCounts, distMat_M02] =  getLabels_M02_noPercent_X(distMatNew, dtwFlow, frameCnt, labelCnt)
    [theLabels, theDistances, removedLabelCounts, distMat_M02] = getLabels(distMatNew, dtwFlow(:,1)', frameCnt, labelCnt);
end

function [theLabels, theDistances, removedLabelCounts, distMat_M03] =  getLabels_M03_usePercent_X(distMatNew, dtwFlow, frameCnt, labelCnt)
    assert(sum(dtwFlow(:,1)==1)==0,'no label can be 1');
    
    [otherCols,stillCols] = getStillOtherCols(distMatNew);
    
    orderBasedOnPercentages = getOrderBasedOnPercent(dtwFlow, length(otherCols));
    
    distMat_M03 = distMatNew(orderBasedOnPercentages,otherCols);
    
    vecIDs = dtwCL(distMat_M03);%[vecIDs, distanceMin, D] 
    
    
    theLabels = NaN(frameCnt,1);
    theLabels(stillCols) = 1;
    theLabels(otherCols) = orderBasedOnPercentages(vecIDs.Col);
    assert(sum(isnan(theLabels))==0,'no label can be NaN');
    

    theDistances = NaN(1,frameCnt);
    theDistances(stillCols) = distMatNew(1,stillCols);
    theDistances(otherCols) = distMat_M03(sub2ind(size(distMat_M03),vecIDs.Col',1:length(vecIDs.Col)));
    
    [theLabels, removedLabelCounts] =  removeLabelsBasedOnDistHistogram(theLabels, theDistances, labelCnt);
end

function [theLabels, removedLabelCounts] =  removeLabelsBasedOnDistHistogram(theLabels, theDistances, labelCnt)
    %for each label, create histogram of distances for 5 groups,
    %use treshold as the fifth center 
    removedLabelCounts = zeros(labelCnt,1);
    for i=1:labelCnt
        slctdCols = find(theLabels==i);
        curLabelDists = theDistances(slctdCols);
        [cnts, centers] = hist(curLabelDists,10);
        countTresh = cumsum(cnts)./sum(cnts);
        removeFromTreshID = find(countTresh>0.85,1);
        
        removeLabels = curLabelDists>=centers(removeFromTreshID);
        removeCols = slctdCols(removeLabels);
        removedLabelCounts(i) = length(removeCols);
        theLabels(removeCols) = 0;
    end
end
function orderBasedOnPercentages = getOrderBasedOnPercent(dtwFlow, frameCnt)
    frameCnt_Vec = round(dtwFlow(:,2)*frameCnt);
    frameCnt_Vec(end) = frameCnt_Vec(end) + (frameCnt-sum(frameCnt_Vec));%remove extra labels from the last label
    assert(frameCnt==sum(frameCnt_Vec),'this must hold');

    orderBasedOnPercentages = NaN(frameCnt,1);
    i=1;
    fr=1;
    while fr<frameCnt
        to = fr + frameCnt_Vec(i)-1;
        orderBasedOnPercentages(fr:to) = dtwFlow(i,1);
        i=i+1;
        fr = to+1;
    end
end

function [theLabels, theDistances, removedLabelCounts, distMatNew] = getLabels(distMatNew, dtwFlow, frameCnt, labelCnt)
    %assert(length(dtwFlow(:,1))==length(unique(dtwFlow(:,1))),'no label can be duplicated');
    if (dtwFlow(1)~=1)%first label is not given as 1
        dtwFlow = [1 dtwFlow];
    end
    assert(sum(dtwFlow(2:end)==1)==0,'no other label can be 1');
    distMatNew = distMatNew(dtwFlow,:);
    
    [otherCols,stillCols] = getStillOtherCols(distMatNew);
    if ~isempty(otherCols)
        vecIDs = dtwCL(distMatNew(2:end,otherCols));%[vecIDs, distanceMin, D]

        theLabels = zeros(frameCnt,1);
        theLabels(stillCols) = 1;
        theLabels(otherCols) = vecIDs.Col + 1; %if 

        theDistances = distMatNew(sub2ind(size(distMatNew),theLabels',1:frameCnt));

        theLabels(otherCols) = dtwFlow(vecIDs.Col + 1); 
    else
        theLabels = ones(1,size(distMatNew,2));
        theDistances = distMatNew(1,:);
    end
    

    [theLabels, removedLabelCounts] =  removeLabelsBasedOnDistHistogram(theLabels, theDistances, labelCnt);
    
    %theLabels = reSetLabels(theLabels,0:labelCnt, [0 dtwFlow]);
end

function [otherCols,stillCols] = getStillOtherCols(distMatNew)
    [~, distMinIDs] = min(distMatNew);
    
    %remove the distMinIDs==1 cols from distMatNew
    %also remove the first row
    otherCols = find(distMinIDs~=1);
    otherCols = min(otherCols):max(otherCols);
    if ~isempty(otherCols)
        stillCols = [1:min(otherCols)-1 1+max(otherCols):size(distMatNew,2)];
    else
        stillCols = 1:size(distMatNew,2);
    end
end