function [mnDS, mxDS] = getMatrixValueBounds( X, printOutBool )
%getMatrixValueBounds get min and max of a 2D matrix
%   Display the values if needed

    [vals_sorted, inds_sorted] = sort(X(:));
    [row_in,col_jn] = ind2sub(size(X),inds_sorted(1));
    mnDS = struct;
    mnDS.d = vals_sorted(1);
    mnDS.rowID = row_in;
    mnDS.colID = col_jn;
    
    if nargout==2
        [row_ix,col_jx] = ind2sub(size(X),inds_sorted(end));
        mxDS = struct;
        mxDS.d = vals_sorted(end);
        mxDS.rowID = row_ix;
        mxDS.colID = col_jx;
    end
                
    if exist('printOutBool','var') && printOutBool
        disp(['Min distance(' num2str(mnDS.d) ') is between row(' num2str(row_in) ')-col(' num2str(col_jn) ').']);
        if nargout==2
            disp(['Max distance(' num2str(mxDS.d) ') is between row(' num2str(row_ix) ')-col(' num2str(col_jx) ').']);
        end
    end
end

