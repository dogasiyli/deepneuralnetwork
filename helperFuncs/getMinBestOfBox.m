function [minValMatch_pos_val, minBestSum_pos_val] = getMinBestOfBox( blobArea, xcolStEn, yrowStEn)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

     %min value match
     blobAreaVec = blobArea(:);
     [baV, baI] = sort(blobAreaVec);
     [baI_yRow, baI_xCol] = ind2sub(size(blobArea), baI(1));
     baIv = blobArea(baI_yRow, baI_xCol);

     minValMatch_pos_val = [baI_yRow + yrowStEn(1) - 1, baI_xCol + xcolStEn(1) - 1 , baIv];
     assert(minValMatch_pos_val(1)<=yrowStEn(2),'this can not be bigger')
     assert(minValMatch_pos_val(2)<=xcolStEn(2),'this can not be bigger')

     %best sum match
     if nargout>1
%          %xSum is a n row vec
%          xSum = sum(blobArea,2);
%          [~, baI_yRow] = min(xSum);         
%          %ySum is a n col vec
%          ySum = sum(blobArea);
%          [~, baI_xCol] = min(ySum);
%          baIv = blobArea(baI_yRow, baI_xCol);
         error('not implemented yet');
         minBestSum_pos_val = [baI_yRow + xcolStEn(1) - 1 , baI_xCol + yrowStEn(1) - 1, baIv];
     end
end

