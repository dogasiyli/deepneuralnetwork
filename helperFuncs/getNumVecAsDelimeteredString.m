function retStr = getNumVecAsDelimeteredString(numVec,delimStr,checkSequence)
    if exist('checkSequence','var') && checkSequence
        retStr = getVecAsSequentialIDSummaryString(sort(numVec));
        retStr = strrep(retStr,',',delimStr);
    else
        retStr = strrep(mat2str(numVec),' ',delimStr);
        retStr = strrep(strrep(retStr,']',''),'[','');
    end
end