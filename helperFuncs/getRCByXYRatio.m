function [ rowCnt, colCnt, ebc] = getRCByXYRatio( N, rcRatio )
%getRCByXYRatio If there are N samples to view, what should the matrix row
%col counts be? If the ratio is 1 to 1 then 
%  rowCnt = ceil(sqrt(imCnt));
%  colCnt = ceil(imCnt/rowCnt);
% but what if it isnt
%   suppose r/c = k.. then we can say that r = c*k. and we know that
%   c^2 * k >=N.. hence c>=sqrt(N/k)..
    if rcRatio>=1
        rRatio = rcRatio/(rcRatio+1);
    else
        rRatio = rcRatio;
    end
    rowCnt = ceil(sqrt(N));
    colCnt = ceil(N/rowCnt);
    K = rowCnt+colCnt;
    rowCnt = ceil(K*rRatio);
    colCnt = ceil(N/rowCnt);
    ebc = rowCnt*colCnt - N;
    while (ebc>=colCnt)
        rowCnt = rowCnt - 1;
        colCnt = ceil(N/rowCnt);
        ebc = rowCnt*colCnt - N;
    end
end

