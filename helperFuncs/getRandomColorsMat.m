function randomColors = getRandomColorsMat(colorCnt)
    randomColors = zeros(colorCnt,3);
    randomColors(1:9,:) = [0 0 1;0 1 0;0 1 1;1 0 0;1 0 1;1 1 0;1 1 1;0.5 0.5 0.5;0 0 0];
    if colorCnt>9
        randomColors(10:colorCnt,:) = rand(colorCnt-9,3);
    else
        randomColors(colorCnt+1:end,:) = [];
    end
end