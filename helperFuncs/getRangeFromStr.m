function [ X, Y ] = getRangeFromStr( str, fr, to )%['2-5',8,14]['9-12',8,14]
%getRangeFromStr read a string as X-Y and return fr-to
%   if str is in form of X-Y
%   X-Y can be in absolute range
%   X-Y can be in relative range [5-8]
    [X,Y] = getXY(str);
    %now check if X is in range fr-to
    if isnan(X)
        X=fr;
        if Y>=fr && Y<=to
            %getRangeFromStr( '-12', 8, 14 )
        else
            Y = Y-1+fr;%getRangeFromStr( '-3', 8, 14 )
        end
    elseif X>=fr && X<=to
        if Y==inf
            Y=to;%getRangeFromStr( '9-', 8, 14 )
        elseif Y>to
            Y=to;%getRangeFromStr( '9-20', 8, 14 )
        end
    else
        %X and Y are relative [2-5]
        X = X-1+fr;
        if Y==inf
            Y=to;%getRangeFromStr( '9', 8, 14 )
        else
            Y = Y-1+fr;
        end        
    end
end

function [X,Y] = getXY(str)
    X = 1;
    Y = inf;
    try
            strArr = split(str,'-');
            X = str2double(strArr(1,:));
            if size(strArr,1)==2
                Y = str2double(strArr(2,:));
            end
    catch
            strArr = strsplit(str,'-');
            X = str2double(strArr{1});
            if length(strArr)==2
                Y = str2double(strArr{2});
            end
    end
    if isnan(Y)
        Y = inf;%getRangeFromStr( '9-', 8, 14 )
    end
end