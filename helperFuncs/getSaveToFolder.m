function saveToFolder = getSaveToFolder(optParamStruct, structFieldName, srcFold)
    saveToFolder = getStructField(optParamStruct, structFieldName, '');
    if strcmp(saveToFolder,'srcFold')
        saveToFolder = srcFold;
    end
end

