function [ khsImage ] = getSignKHSImage(signID, khsIdentifier, single0_double1, srcFold)
    if ~exist('srcFold','var')
        srcFold = getStructField(optParamStruct, 'srcFold', getVariableByComputerName('srcFold'));    
    end
    khsImage = [];
    signFolder = [srcFold filesep num2str(signID)];
    if ~exist(signFolder, 'dir')
        return
    end
    
    [clusterNames, clusterInfoAll] = getClusterNamesAssignedFromTxt(srcFold, struct('signIDList',signID,'single0_double1',single0_double1));
    
    rowID = -1;
    if ischar(khsIdentifier)
        rowID = find(strcmp(clusterNames,khsIdentifier));
    elseif isnumeric(khsIdentifier)
        rowID = khsIdentifier;
    end
    
    if isempty(rowID) || rowID<1 || rowID>size(clusterInfoAll,1)
        warning('rowID is ambiguos')
        rowID
        return
    end
    
    infoSplit = strsplit(clusterInfoAll{rowID},'+');
    detailedLabels = [signID str2double(infoSplit{4}) str2double(infoSplit{5}) find(strcmp({'LH';'RH';'BH'},infoSplit{6})==1) str2double(infoSplit{7})];
    khsImage = retrieveImageFromDetailedLabels(srcFold, 1, detailedLabels);
end

