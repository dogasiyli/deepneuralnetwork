function str = getTimeAsString(secondsDouble)
%getTimeAsString get from seconds as double to readbale string
%   seconds is not always the most meaningful way of expressing time
    if secondsDouble<60
        %x seconds
        str = [mat2str(secondsDouble,5) ' seconds'];
    elseif secondsDouble<60*60
        %x minutes, y seconds
        x = floor(secondsDouble/60);
        y = secondsDouble-x*60;
        str = [num2str(x) ' minutes,' num2str(ceil(y)) ' seconds'];
    else%if secondsDouble<60*60*24
        %x hours, y minutes, z seconds
        x = floor(secondsDouble/3600);
        secondsDouble = secondsDouble-x*3600;
        y = floor(secondsDouble/60);
        z = secondsDouble-y*60;    
        str = [num2str(x) ' hours,' num2str(y) ' minutes,' num2str(ceil(z)) ' seconds'];
    end
end