function [G, uniqueMappingLabels, uniqueGTLabels, gtLabelsGrouping] = getUniqueMappingLabels(gtLabels, gtLabelsGrouping)
    uniqueGTLabels = unique(gtLabels);
    if exist('gtLabelsGrouping','var') && ~isempty(gtLabelsGrouping)
        %what are the final mapped groundtruth labels
        G = length(unique(gtLabelsGrouping));
    else
        %if no map is passed we will basically map to 1:G
        G = length(uniqueGTLabels);
        gtLabelsGrouping = 1:G;
    end
    uniqueMappingLabels = [uniqueGTLabels(:) gtLabelsGrouping(:)];
end