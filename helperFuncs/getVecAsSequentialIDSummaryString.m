function clusterCellString = getVecAsSequentialIDSummaryString(idx_cur)
    numOfSamples = length(idx_cur);
    if numOfSamples==1
        clusterCellString = num2str(idx_cur);
    else
        idxStr = '';
        idxBeg = idx_cur(1);
        idxEnd = idx_cur(1);
        for curInd = 2:numOfSamples
            idxDif = idx_cur(curInd)-idx_cur(curInd-1);
            if idxDif>1
                idxStr = addRangeToStr(idxStr, idxBeg, idxEnd);
                idxBeg = idx_cur(curInd);
                idxEnd = idx_cur(curInd);
            else
                %keep on
                idxEnd = idx_cur(curInd);
            end
            if  curInd==numOfSamples
                idxStr = addRangeToStr(idxStr, idxBeg, idxEnd);
            end
        end
        clusterCellString = idxStr(1:end-1);
    end
end

function idxStr = addRangeToStr(idxStr, idxBeg, idxEnd)
    %add the previous groupimg as string
    if idxBeg==idxEnd
        %like adding '1,'
        idxStr = [idxStr num2str(idxBeg) ',']; %#ok<*AGROW>
    elseif idxBeg==idxEnd-1
       %like adding '3-4,'
        idxStr = [idxStr num2str(idxBeg) ',' num2str(idxEnd) ','];
    else
       %like adding '3-4,'
        idxStr = [idxStr num2str(idxBeg) '-' num2str(idxEnd) ','];
    end
end
