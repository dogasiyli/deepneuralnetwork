function c_map_vec = get_color_map( color_map_str )
    switch color_map_str
        case 'confusion_mat_01'
            r_map = [linspace(1,0.85,3) linspace(0.85,0.60,3)];
            g_map = [linspace(1,1,3) linspace(1,0.85,3)];
            b_map = [linspace(1,0.6,3) linspace(0.6,1,3)];
            
            %worst = 1 1 1
            %mid color 0.85, 1.0, 0.6
            %best = 0.60, 0.85, 1.0
            c_map_vec = [r_map' g_map' b_map']; 
        otherwise
            c_map_vec = winter;
    end
end

