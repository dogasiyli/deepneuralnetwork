function [uniqNames, groupCells] = groupCellNames(singLabs_Names)
    sortByRepeatCount = true;
    [uniqNames, ia, ic] = unique(singLabs_Names);
    nameCount = length(uniqNames);
    groupCells = cell(nameCount,1);
    for i=1:nameCount
        groupCells{i} = reshape(sort(find(ic==i)),1,[]);
    end
    
    
    if sortByRepeatCount
        [repCntVec, idx] = sort(-cellfun(@length,groupCells));
        uniqNames = uniqNames(idx);
        groupCells = groupCells(idx);
    end
end