function [confMatBest, uniqueMappingLabels, confuseAcc, outStruct, h1, h2] = groupClusterLabelsIntoGroundTruth(clusterLabels, gtLabels, OPS)
%groupClusterLabelsIntoGroundTruth When we have some unsupervised
%clustering result and we know the ground truth labels of the samples we
%can find a mapping that assigns clusterLabels to such ground truth labels
%such that we maximize the supervised classification accuracy
%   clusterLabels - [1..N], where clusterLabels(i) - [1-K]
    uniqueClusterLabels = unique(clusterLabels);
    K = length(uniqueClusterLabels);
    
    originalConfusionMatrix = computeConfusionMatrix(gtLabels, clusterLabels);
    
%   gtLabels - [1..N], where gtLabels(i) can be for example 2 4 5
%   gtLabelsGrouping can group gtLabels into sth else, if this is empty
%   then gtLabels will be mapped to 1:G where G is the number of ground
%   truth label count

    %struct('gtLabelsGrouping', [],'printResults', true,'figID', -1);
    if ~exist('OPS', 'var') || ~isstruct(OPS)
        OPS = [];
    end
    gtLabelsGrouping = getStructField(OPS, 'gtLabelsGrouping', []);
    printResults     = getStructField(OPS, 'printResults', true);
    figID            = getStructField(OPS, 'figID', -1);

    if ~exist('gtLabelsGrouping','var') 
       gtLabelsGrouping = [];
    end   
    if ~exist('printResults','var') 
       printResults = true;
    end   
    
    [G, uniqueMappingLabels] = getUniqueMappingLabels(gtLabels, gtLabelsGrouping);%, uniqueGTLabels, gtLabelsGrouping
    
    gtLabels_mapped = reSetLabels( gtLabels, uniqueMappingLabels(:,1), uniqueMappingLabels(:,2) );
    
    %first find the confusion matrix and detailed information
    [relationShipTable, newGroupCell, newKlusterCell, extraOutputStruct] = createRelationShipTable(gtLabels_mapped, clusterLabels, uniqueMappingLabels);
    
    %here we know that we have new number of clusters
    newClusterCount = size(newGroupCell,1);
    assert(newClusterCount==size(newKlusterCell,2),'this equation must hold');
    
    %get the uniqeMappingVec and reset gtLabels_mapped with newGroupCell
    originalGroupLabels = NaN(1,G);
    newGroupLabels = NaN(1,G);
    originalKlusterLabels = NaN(1,K);
    newKlusterLabels = NaN(1,K);
    for k = 1:newClusterCount
        %the group labels that would fall into newID = k
        %are in newGroupCell{k}
        grpLabOld = newGroupCell{k};
        grpLabNew = k*ones(length(grpLabOld),1);
        %following line is to see that everything fits well
        originalGroupLabels(grpLabOld) = grpLabOld;
        %now create our new mapping for best accuracy of old group labels
        %and new assignedClusterGroups
        newGroupLabels(grpLabOld) = grpLabNew;
        
        
        %the old cluster labels that would fall into newID = k
        %are in newGroupCell{k}
        clsLabOld = newKlusterCell{k};
        clsLabNew = k*ones(length(clsLabOld),1);
        %following line is to see that everything fits well
        originalKlusterLabels(clsLabOld) = clsLabOld;
        %now create our new mapping for best accuracy of old cluster labels
        %and new assignedClusterGroups
        newKlusterLabels(clsLabOld) = clsLabNew;
    end
    
    uniqueMappingLabels = [uniqueMappingLabels newGroupLabels(uniqueMappingLabels(:,2))'];
    
    gtLabels_ReMapped = reSetLabels(gtLabels_mapped, originalGroupLabels, newGroupLabels);
    clusLabels_Mapped = reSetLabels(clusterLabels, originalKlusterLabels, newKlusterLabels);
    
    clusterLabelsGroupingVec =  newGroupLabels;
    [AR1,RI1,MI1,HI1]=RandIndex(clusterLabels,gtLabels_mapped);
    [AR2,RI2,MI2,HI2]=RandIndex(clusLabels_Mapped,gtLabels_ReMapped);

    confMatBest = computeConfusionMatrix(clusLabels_Mapped,gtLabels_ReMapped);        

    confuseAcc = confMatBest.Accuracy;
    confMatBest = confMatBest.Matrix;
    
    [~,grpSrtIdx] = sort(cellfun(@min,newKlusterCell));
    newGroupCellSorted = newGroupCell;%(grpSrtIdx,:);
    for g = 1:size(newGroupCellSorted,1)
        gIDs = newGroupCellSorted{g};
        Lia = ismember(uniqueMappingLabels(:,2),gIDs);
        gIDsNew = find(Lia);
        newGroupCellSorted{g} = gIDsNew;
    end
    newGroupCellStr = cellfun(@mat2str, newGroupCellSorted, 'UniformOutput', false);
    newKlusterCellStr = cellfun(@mat2str, newKlusterCell(grpSrtIdx), 'UniformOutput', false);
    
    figureTitleStr = ['gt_to_clusters(' mat2str(clusterLabelsGroupingVec) '), acc(' num2str(confuseAcc) ')'];
    if printResults
        disp('best confusion matrix');
        disptable(confMatBest);
        disp(figureTitleStr);
    end
    if nargout>4
        tickLabelCellStruct = struct;
        tickLabelCellStruct.y = newGroupCellStr;
        tickLabelCellStruct.x = newKlusterCellStr;
        labelStrStruct = struct;
        labelStrStruct.x = 'Grouped Unsupervised Cluster Groups';
        labelStrStruct.y = 'Known Label Groups';

        h1 = drawConfusionDetailed(confMatBest, struct('figureID',figID,'figureTitle',figureTitleStr,'tickLabelCellStruct',tickLabelCellStruct,'labelStrStruct',labelStrStruct));

        if nargout>5
            labelStrStruct.x = 'Original Clusters';
            labelStrStruct.y = 'Original Labels';
            h2 = drawConfusionDetailed(originalConfusionMatrix.Matrix, struct('figureID',figID+1,'figureTitle','original confusion matrix','labelStrStruct',labelStrStruct));
        else
            h2 = [];
        end
    else
        h1 = [];
        h2 = [];
    end
    
    outStruct = struct;
	outStruct.clusterLabelsGroupingVec = clusterLabelsGroupingVec;
	outStruct.newKlusterCell = newKlusterCell;
	outStruct.clusLabels_Mapped = clusLabels_Mapped;
    outStruct.gtLabels_ReMapped = gtLabels_ReMapped;
    outStruct.relationShipTable = relationShipTable;
end

function [relationShipTable, newGroupCell, newKlusterCell, extraOutputStruct] = createRelationShipTable(gtLabels_mapped, clusterLabels, uniqueMappingLabels)
    G = length(unique(gtLabels_mapped));
    K = length(unique(clusterLabels));
    confMat_G_rows = computeConfusionMatrix(gtLabels_mapped, clusterLabels);
    confMat_K_rows = computeConfusionMatrix(clusterLabels, gtLabels_mapped);
    %[G,K] = size(confMat_G_rows.Matrix);
    %in confMat_G_rows
    %(cols)K number of unsupervised clusters to be mapped into
    %(rows)G number of supervised gived labels (Ground truth)
    % 1. if confMat_G_rows.Percent(i,j) has some value over than 0.5 than G(i) should be related and mapped to that K(j)
    % 2. if confMat_K_rows.Percent(i,j) has some value over than 0.5 than K(i) should be related and include G(j)
    %so this is a many-to-many relationship
    relationShipTable = cell(G,K);
    summaryString = '';
    for g = 1:G
        %find the labels of the group
        relatedLabels = find(uniqueMappingLabels(:,2)==G);
        relatedLabels = uniqueMappingLabels(relatedLabels); %#ok<FNDSB>
        labelstr = [mat2str(relatedLabels) ' grouped into G(' num2str(g) ')'];
        for k = 1:K
            relationShipTable{g,k} = [0 0];
            if confMat_G_rows.Percent(g,k)>0.5
                relationShipTable{g,k}(1) = confMat_G_rows.Percent(g,k);
                summaryStringAdd = ['<' num2str(100*confMat_G_rows.Percent(g,k)) '> percent of the given label(' labelstr ') is in K(' num2str(k) '). Hence if K(' num2str(k) ') has G(' num2str(g) ') it is considered true. ' char(10)];
                summaryString = [summaryString summaryStringAdd]; %#ok<*AGROW>
            end
            if confMat_K_rows.Percent(k,g)>0.5
                relationShipTable{g,k}(2) = confMat_K_rows.Percent(k,g);
                summaryStringAdd = ['<' num2str(100*confMat_K_rows.Percent(k,g)) '> percent of K(' num2str(k) ') includes the given label(' labelstr '). Hence if G(' num2str(g) ') is in K(' num2str(k) ') it is considered true.' char(10)];
                summaryString = [summaryString summaryStringAdd]; %#ok<*AGROW>
            end
        end
    end
    
    [newGroupCell, newKlusterCell, extraOutputStruct] = relationShip2NewClusterGroupLabels(relationShipTable, confMat_G_rows.Matrix(1:G,1:K));
end

