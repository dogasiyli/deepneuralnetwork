function indsVec = groupSortDistMat( distMat, repCnt )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    N = size(distMat,1);%there are N number of images to be compared
    indsVec = 1:N;
    
    figure(5);clf;titlestr = 'title init';
    for j = 1:repCnt
        for i = 1:N
            pcolor(distMat);colorbar;
            title(titlestr);
            drawnow;
            [distMat, indsVec, im_i, im_j, d] = getNeighoursTogether(distMat, indsVec, i);
            titlestr = ['rep(' num2str(j) '),iter(' num2str(i) '),d(' num2str(d) '),images(' num2str(im_i) '-' num2str(im_j) ')'];
        end
    end
end

function [distMat, indsVec, im_i, im_j, d] = getNeighoursTogether(distMat, indsVec, i)
    [distVals_sorted, inds_sorted] = sort(distMat(:));
    [im_i_h,im_j_h] = ind2sub(size(distMat),inds_sorted(i));
    im_i = indsVec(im_i_h);
    im_j = indsVec(im_j_h);
    d = distVals_sorted(i);
    
    %now do such a swap that im_i_h and im_j_h gets together near the one
    %with small inds
    %the one with bigger indice needs to come near the one with small
    %indice by sending the si-1 or si+1 away
    
    
    disp(['swapping inds(' num2str(im_i_h) '-' num2str(im_j_h) '),images(' num2str(im_i) '-' num2str(im_j) ')']);
    [distMat, indsVec] = swapIndsDistMat(distMat, im_i_h, im_j_h, indsVec);
end




