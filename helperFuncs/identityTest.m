function identityTest(I_test, tresholdVals, displayOpts)
    if ~exist('displayOpts','var') || isempty(displayOpts)
        displayOpts = setDisplayOpts('none');
    end
    %% 1. Test 1 - diagonal must all equal to 1
    diagTest = diag(I_test);
    t = sum(abs(diagTest-1)>tresholdVals);
    if t==0 && displayOpts.calculation
        disp('Diagonal all equal to 1. Succeeded.');
    end
    if t~=0 && displayOpts.warningInfo
        disp([displayOpts.ies{4} 'WARNING : Diagonal between (' num2str(min(diagTest(:)),'%4.2f') ',' num2str(max(diagTest(:)),'%4.2f') '). Check your implementation.']);
    end

    %% 2. Test 2 - elements not in diagonal must be less than some treshold
    I_test(eye(length(I_test))==1) = 0;
    minTest = min(I_test(:));
    maxTest = max(I_test(:));
    testCorrect = ((minTest>-tresholdVals)&&(maxTest<tresholdVals));
    if  testCorrect && displayOpts.calculation
        disp(['Max of non diag element is between (-10^' num2str(floor(real(log10(minTest)))) ',+10^' num2str(floor(real(log10(maxTest)))) '). Succeeded.']);
    end
    if ~testCorrect && displayOpts.warningInfo
        disp([displayOpts.ies{4} 'WARNING :All elements not lower than some treshold. Check your implementation.']);
    end
end

