function [imPalette, paletteRCSize, collageObject] = imCollageFuncs( operationStr, inputVarStruct )
%imCollageFuncs Creates an image palette, and adds images to the specified
%imageIDs as a matrix box.
    collageObject = struct;
    switch operationStr
        case 'createPalette'
            %imCollageFuncs( 'createPalette', struct('imCnt',imCnt,'channelCnt',1,'imSizeMax',dpthImBiggestSize));
            collageObject.imCnt = inputVarStruct.imCnt;
            collageObject.channelCnt = inputVarStruct.channelCnt;
            if isfield(inputVarStruct,'rcRatio')
                collageObject.rcRatio = inputVarStruct.rcRatio;
            else
                collageObject.rcRatio = 1;
            end
            if isfield(inputVarStruct,'imSizeMax')
                collageObject.imSizeMax = inputVarStruct.imSizeMax;
                [imPalette, paletteRCSize] = paletteCreate_FromSize(collageObject);
            elseif isfield(inputVarStruct,'blockCounts')
                collageObject.blockCounts = inputVarStruct.blockCounts;
                [imPalette, paletteRCSize] = paletteCreate_FromBlockCnt(collageObject);
            else
                error(['No operation with name(' operationStr ') defined']);
            end
             collageObject.imPalette = imPalette;           
             collageObject.paletteRCSize = paletteRCSize;           
        case 'insertIntoPalette'
            %imPalette = imCollageFuncs('insertIntoPalette', struct('imageID',imageID,'paletteRCSize',paletteRCSize,'imPalette',imPalette,'imageToAdd',imageToAdd,'boxSize',boxSize))
            imageID = inputVarStruct.imageID;
            paletteRCSize = inputVarStruct.paletteRCSize;
            imPalette = inputVarStruct.imPalette;
            imageToAdd = inputVarStruct.imageToAdd;
            boxSize = inputVarStruct.boxSize;
            if isfield(inputVarStruct,'surroundWithColorStruct')
                imPalette = insertIntoPalette(imageID, paletteRCSize, imPalette, imageToAdd, boxSize, inputVarStruct.surroundWithColorStruct);
            else
                imPalette = insertIntoPalette(imageID, paletteRCSize, imPalette, imageToAdd, boxSize);
            end
        case 'createCircularPalette'
            %imCollageFuncs( 'createPalette', struct('imCnt',imCnt,'channelCnt',1,'imSizeMax',dpthImBiggestSize));
            collageObject.imCnt = inputVarStruct.imCnt;%how many images will be put in the circle
            collageObject.paletteSize = inputVarStruct.paletteSize;%the size of palette
            collageObject.channelCnt = inputVarStruct.channelCnt;%rgb or gray scale
            collageObject.bckColor = inputVarStruct.bckColor;%[R G B] as background color
            
            [imPalette, paletteRCSize] = paletteCreate_Circular(collageObject);
    end
end

function paletteImg = insertIntoPalette(indsImg, paletteRCSize, paletteImg, imageToAdd, eachBlockSize, surroundWithColorStruct)
    [rpi, cpi] = ind2sub(paletteRCSize, indsImg);
    pfr = 1+(rpi-1)*eachBlockSize(1);%palette from row
    ptr = rpi*eachBlockSize(1);%palette to row
    pfc = 1+(cpi-1)*eachBlockSize(2);%palette from col
    ptc = cpi*eachBlockSize(2);%palette to col
    if exist('surroundWithColorStruct','var') && isstruct(surroundWithColorStruct)
        imgRet = putImg_IntoBox(imageToAdd, eachBlockSize, true, surroundWithColorStruct);    
    else
        imgRet = putImg_IntoBox(imageToAdd, eachBlockSize, true);    
    end
    paletteImg(pfr:ptr,pfc:ptc,:) = imgRet;
end

function [imPalette, paletteRCSize] = paletteCreate_FromSize(collageObject)
    [rowCnt, colCnt, ebc] = getRCByXYRatio(collageObject.imCnt,collageObject.rcRatio);
    imPalette = zeros(rowCnt*collageObject.imSizeMax(1),colCnt*collageObject.imSizeMax(2),collageObject.channelCnt);
    paletteRCSize = [rowCnt,colCnt];
end

function [imPalette, paletteRCSize] = paletteCreate_FromBlockCnt(collageObject)
    %collageObject.imCnt, collageObject.blockCounts, collageObject.channelCnt, collageObject.rcRatio
    [rowCnt, colCnt, ebc] = getRCByXYRatio(collageObject.imCnt,collageObject.rcRatio);
    imPalette = zeros(rowCnt*collageObject.blockCounts(1),colCnt*collageObject.blockCounts(2),collageObject.channelCnt);
    paletteRCSize = [rowCnt,colCnt];
end

% function [paletteRCSize, dpthImPalette, surfRGBImPalette] = paletteCreate(imCnt, dpthImBiggestSize, blockCounts)
%     rowCnt = ceil(sqrt(imCnt));
%     colCnt = ceil(imCnt/rowCnt);
%     
%     dpthImPalette = zeros(rowCnt*dpthImBiggestSize(1),colCnt*dpthImBiggestSize(2));
%     surfRGBImPalette = zeros(rowCnt*blockCounts(1),colCnt*blockCounts(2),3);
%     paletteRCSize = [rowCnt,colCnt];
% end
