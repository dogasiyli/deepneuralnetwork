function initialSaveTheBatchesToActivatedDataFolder(dataStruct, displayOpts, saveOpts)
    countBatch = dataStruct.countBatch.train; 
    curLayerID = 0;
    folderName = [saveOpts.MainFolder saveOpts.SubFolders.ActivatedData];
    if ~exist(folderName,'dir')
        mkdir(folderName);
    end   
    for batchID=1:countBatch
        fileNameEnd = ['batch(' num2str(batchID,'%02d') ' of ' num2str(countBatch,'%02d') ').mat'];
        saveFileNameInit = ['layer(' num2str(curLayerID,'%02d') ')_'];
        saveFileName = [folderName saveFileNameInit fileNameEnd];
        if ~exist(saveFileName,'file')
            %load the data
            dataStruct = dataStruct.updateDataStruct(dataStruct, {'batchID_current', batchID});
            if displayOpts.fileOperations
                disp([displayOpts.ies{4} 'Loading ' dataStruct.fileName_current ''])
            end
            load(dataStruct.fileName_current,'data','labels');
            X = reshape(data,dataStruct.countFeat,[]);
            %save the data
            loadAndSaveForInitialLearning(curLayerID, batchID, countBatch, displayOpts, saveOpts, X, labels);
        end
    end
end