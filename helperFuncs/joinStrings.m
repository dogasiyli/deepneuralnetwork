function outStr = joinStrings( s, delimeter )
    if ~exist('delimeter','var')
        delimeter = '_';
    end
    %e.g. :
    %s={'sample','abc','1234','12'};
    %joinStrings( s ) :
    %   'sample_abc_1234_12'
    
    %joinStrings( s,- ) :
    %   'sample-abc-1234-12'
    outStr = [sprintf(['%s' delimeter],s{1:end-1}),s{end}];
end

