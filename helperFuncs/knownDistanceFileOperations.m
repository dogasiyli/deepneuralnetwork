function knownDistances = knownDistanceFileOperations( operationString, inputStruct )
%knownDistanceFileOperations applies some pre-defined operations on knownDistances
%   loadFile - loads the knownDistances matrix or struct from file
%   quantizeIfNecessary - loads the knownDistances either from file or uses the 'knownDistances' field passed. 
%                         quantizes it if it is not yet quantized. 
%                         saves the quantized structure to file if 'fileName' is passed as a structure field in inputStruct
%   tidyDistanceStruct - loads the knownDistances either from file or uses the 'knownDistances' field passed. 
%                        sorts it and checks whether it has the same distances
%                        removes them if they dont match - leaves only 1 if they all match
%
%   tidyFolderAll - gets into the folder and combines everything it sees
%                   under the equivelant knownDistances_<featName>_<cropType>_<distType>_kX.mat
%
%   e.q. : knownDistanceFileOperations( 'tidyDistanceStruct', struct('fileName','/media/dg/SSD_Data/distMats/knownDistances_sn_mc_c3_wsu.mat'));
%          knownDistanceFileOperations('tidyFolderAll', struct('sourceFolder','/media/dg/SSD_Data/distMats'));
    knownDistances = getKnownDistanceFromStruct(inputStruct, operationString);
    switch operationString
        case 'loadFile'
            %it should already be loaded
        case 'quantizeIfNecessary'
            if ~isstruct(inputStruct)
                error('inputStruct must be a struct with either <fileName> or <knownDistances> as its field');
            end
            if isstruct(knownDistances) && isfield(knownDistances,'frameInfo') && isfield(knownDistances,'additionalCols')
                %knownDistances already converted to squeezed version
                %
            else
                %knownDistances should be converted to squeezed version
                if ~isempty(knownDistances)
                    [~,knownDistances] = distMatQuantize_hospiSign(knownDistances, []);
                    updateFileIfFileNameGiven(inputStruct, knownDistances, operationString);
                end
            end            
        case 'tidyDistanceStruct'
            knownDistances = tidyKnownDistanceStruct(knownDistances);
            updateFileIfFileNameGiven(inputStruct, knownDistances, operationString);
        case 'mergeAndTidy'
            % f = '/media/doga/Data/FromUbuntu/Doga/distMats/knownDistances_';
            % f2 = 'hf_mc_e3_';
            % knownDistanceFileOperations( 'mergeAndTidy', struct('fileName',[f f2 'mlu.mat'],'fileNameAppend',[f f2 'bpu.mat'],'fileNameResult',[f f2 'mrg.mat']));
            % knownDistanceFileOperations( 'mergeAndTidy', struct('fileName',[f f2 'mrg.mat'],'fileNameAppend',[f f2 'wsu.mat'],'fileNameResult',[f f2 'mrg2.mat']));            
            
            knownDistances_append = loadFromFile_01(inputStruct.fileNameAppend);
            %now append knownDistances_append after knownDistances
            %then tidy knownDistances
            %save the tidied into fileNameResult
            cntCurrent = size(knownDistances.frameInfo,1);
            cntAppend = size(knownDistances_append.frameInfo,1);
            knownDistances.frameInfo = [knownDistances.frameInfo;knownDistances_append.frameInfo];
            knownDistances.additionalCols = [knownDistances.additionalCols;knownDistances_append.additionalCols];
            knownDistances = tidyKnownDistanceStruct(knownDistances);
            cntFinal = size(knownDistances.frameInfo,1);
            disp(['combining into known distances file (' inputStruct.fileNameResult ') - operationString(' operationString ')']);
            disp(['oldCnt(' num2str(cntCurrent) ')+appendCnt(' num2str(cntAppend) ')=ResultCnt(' num2str(cntFinal) ')']);
            save(inputStruct.fileNameResult, 'knownDistances');
        case 'tidyFolderAll'
            featureNames = {'hf','sn'};
            cropTypes = {'mc','sc'};
            distTypes = {'c3','c2','e3'};
            srcFolder = knownDistances;
            knownDistances = [];
            for f = 1:2
                for c = 1:2
                    for d = 1:3
                        fn = featureNames{f};
                        ct = cropTypes{c};
                        dt = distTypes{d};
                        fileNameInit = ['knownDistances_' fn '_' ct '_' dt '_'];
                        disp(['Searching for ' fileNameInit '?']);
                        
                        %first remove any autoTidy mats
                        [fileNames, numOfFiles] = getFileList(srcFolder, '.mat', [fileNameInit 'k'], false);
                        for i=1:numOfFiles
                            disp(['removing ' fileNames{i}]);
                            delete([srcFolder filesep fileNames{i}]);
                        end
                        
                        [fileNames, numOfFiles] = getFileList(srcFolder, '.mat', fileNameInit, false);
                        k = 0;
                        finalResultFulFileName = [fileNameInit 'k' num2str(k)];
                        
                        if numOfFiles==0
                            disp('no files like this');
                            continue
                        end
                        inputStruct = struct;
                        inputStruct.fileName = [srcFolder filesep fileNames{1}];
                        inputStruct.fileNameResult = [srcFolder filesep finalResultFulFileName '.mat'];      
                        if numOfFiles==1
                            disp(['Moving <' fileNames{1} '> to <' finalResultFulFileName '>']);
                            movefile(inputStruct.fileName,inputStruct.fileNameResult);
                        end
                        for fApp = 2:numOfFiles
                            inputStruct.fileNameAppend = [srcFolder filesep fileNames{fApp}];%[1 2, k0 3, k1 4]
                            inputStruct.fileNameResult = [srcFolder filesep finalResultFulFileName '.mat'];
                            
                            disp(['combining <' inputStruct.fileName '>+<' inputStruct.fileNameAppend '>=<' inputStruct.fileNameResult '>']);
                            knownDistanceFileOperations('mergeAndTidy', inputStruct);
                            
                            %k=0-fApp=2,k=1-fApp=3,k=2-fApp=4,...,k=numOfFiles-3-fApp=numOfFiles-1
                            if k<=numOfFiles-3
                                k = k + 1;
                                finalResultFulFileName = [fileNameInit 'k' num2str(k)];
                                inputStruct.fileName = inputStruct.fileNameResult;
                            end
                        end
                        
                        finalResultFulFileName = [srcFolder filesep fileNameInit 'combinationResult.mat'];
                        disp(['Moving <' finalResultFulFileName '> to <combinationResult.mat>']);
                        movefile(inputStruct.fileNameResult,finalResultFulFileName);
                    end
                end
            end
        case 'tidyFolderRenameDelete'
            %after 'tidyFolderAll'
            %go and rename 'knownDistances_???_combinationResult' to a
            %proper name for the computer
            %if there is only 1 'knownDistances_???' then rename that to
            %'knownDistances_???_combinationResult' first
            featureNames = {'hf','sn'};
            cropTypes = {'mc','sc'};
            distTypes = {'c3','c2','e3'};
            srcFolder = knownDistances;
            knownDistances = [];
            compAdd = getVariableByComputerName('compNameAddStr');
            compAdd = compAdd(2:end);
            for f = 1:2
                for c = 1:2
                    for d = 1:3
                        fn = featureNames{f};
                        ct = cropTypes{c};
                        dt = distTypes{d};
                        fileNameInit = ['knownDistances_' fn '_' ct '_' dt '_'];
                        disp(['Searching for ' fileNameInit '?']);
                        
                        %first remove any autoTidy mats
                        [fileNames, numOfFiles] = getFileList(srcFolder, '.mat', [fileNameInit 'k'], false);
                        for i=1:numOfFiles
                            disp(['removing ' fileNames{i}]);
                            delete([srcFolder filesep fileNames{i}]);
                        end
                        
                        %then get all the mat files starting with 'knownDistances_???'
                        [fileNames, numOfFiles] = getFileList(srcFolder, '.mat', fileNameInit, false);
                        if numOfFiles==0
                            disp('no files like this');
                            continue
                        end
                        fName = [srcFolder filesep fileNames{1}];      
                        finalResultFulFileName = [srcFolder filesep fileNameInit 'combinationResult.mat'];
                        if numOfFiles==1
                            disp(['Moving <' fileNames{1} '> to <combinationResult.mat>']);
                            movefile(fName,finalResultFulFileName);
                        end
                        for fApp = 1:numOfFiles
                            if (~strcmpi(fileNames{fApp},[fileNameInit 'combinationResult.mat']))
                                fName = [srcFolder filesep fileNames{fApp}];                           
                                disp(['removing <' fName '>']);
                                try
                                    delete(fName);
                                    disp(['removed <' fName '>']);
                                catch
                                    disp(['something went wrong tryyin to remove <' fName '>']);
                                end
                            end
                        end
                        finalResultCompFileName = [srcFolder filesep fileNameInit compAdd '.mat'];
                        disp(['Moving <' finalResultFulFileName '> to <' compAdd '.mat>']);
                        movefile(finalResultFulFileName,finalResultCompFileName);
                    end
                end
            end            
        otherwise
            error(['you requested an unimplemented operation(' operationString ')']);
    end
end

function knownDistances = tidyKnownDistanceStruct(knownDistances)
    epsHere = 9.999e-10;
    [knownDistances.frameInfo, idx] = sort(knownDistances.frameInfo);

    [~, ia, ic] = unique(knownDistances.frameInfo);
    idxToRemove = [];
    duplicateCheckMat = [];


    sumDif = sum(abs((1:length(idx)) - (reshape(idx,1,[]))));
    sum(sum(abs(reshape(idx,1,[])-1:length(idx))));
    if sumDif<eps && length(ia)==length(ic) && max(ic)==length(ia)
        disp('knownDistances is already sorted and tidy');
        return
    end
    knownDistances.additionalCols = knownDistances.additionalCols(idx,:);

    %c has the unique elements in knownDistances.frameInfo
    if (length(ia)==length(ic))
        %there is no problem
        assert(max(ic)==length(ia),'this must hold');
    else
        %there is at least 1 duplicate
        problematicLabels = 1;
        sampleCntToRemove = 1;
        while ~isempty(problematicLabels) && sampleCntToRemove>0
            histOfLabels = hist(ic,1:length(ia));
            problematicLabels = find(histOfLabels>1);
            if ~isempty(problematicLabels)
                compListIdx_01 = ia(problematicLabels);
                compListIdx_02 = compListIdx_01+1;
                sampleCntToCheck = length(compListIdx_01);
                %so every entry of compListIdx_01(i) must match compListIdx_02(i)
                listToSum = sum(abs(knownDistances.additionalCols(compListIdx_01,:) - knownDistances.additionalCols(compListIdx_02,:)),2);
                %for all the results == 0 we can remove 
                sampleIDsToRemove = listToSum<epsHere;
                sampleCntToRemove = sum(sampleIDsToRemove==1);
                if sampleCntToCheck==sampleCntToRemove
                    disp(['All of the ' num2str(sampleCntToCheck) ' samples have at least 1 duplicate and all found to be same. Hence they will be removed as duplicate']);
                elseif sampleCntToRemove>0
                    disp([num2str(sampleCntToCheck) ' of samples have at least 1 duplicate.' num2str(sampleCntToRemove) ' of them is found to be accurate and they will be removed as duplicate']);
                end
                if sampleCntToRemove>0
                    knownDistances.frameInfo(compListIdx_02(sampleIDsToRemove==1),:) = [];
                    knownDistances.additionalCols(compListIdx_02(sampleIDsToRemove==1),:) = [];
                    [~, ia, ic] = unique(knownDistances.frameInfo);
                end
                clear compListIdx_01 compListIdx_02 listToSum
                if sampleCntToRemove==0
                    histOfLabels = hist(ic,1:length(ia));
                    problematicLabels = find(histOfLabels>1);
                    disp([num2str(sampleCntToCheck) ' of samples have non-matching duplicates']);
                    clear sampleCntToRemove
                    break
                end
            end
        end
        intCols = [1 3 4];
        distCol = 2;
        stdDist = std(knownDistances.additionalCols(:,distCol));
        for i=1:length(problematicLabels)
            currentLabel = problematicLabels(i);
            problematicElements = find(ic==currentLabel);
            duplicateElementCount = length(problematicElements);
            duplicateElementMatrix = zeros(duplicateElementCount, size(knownDistances.additionalCols,2));
            for j=1:duplicateElementCount
                duplicateElementMatrix(j,:) = knownDistances.additionalCols(problematicElements(j),:);
            end
            %now if all elements in this duplicateElementMatrix is
            %same leave only 1 of the entries and remove others, if
            %any information is different delete all
            sumDif_int = sum(sum(abs(bsxfun(@minus, duplicateElementMatrix(:,intCols), duplicateElementMatrix(1,intCols)))));
            sumDif_dbl = sum(sum(abs(bsxfun(@minus, duplicateElementMatrix(:,distCol), duplicateElementMatrix(1,distCol)))));
            if sumDif_int==0 && sumDif_dbl<epsHere
                %remain single
                idxToRemove = [idxToRemove;reshape(problematicElements(2:end),[],1)]; %#ok<AGROW>
                duplicateCheckMat = [duplicateCheckMat;duplicateElementCount 2]; %#ok<AGROW>
            elseif sumDif_int==0 && sumDif_dbl<stdDist*0.1
                %remove all except first
                idxToRemove = [idxToRemove;reshape(problematicElements(2:end),[],1)]; %#ok<AGROW>
                duplicateCheckMat = [duplicateCheckMat;duplicateElementCount 1]; %#ok<AGROW>
            else
                %remove all
                idxToRemove = [idxToRemove;reshape(problematicElements,[],1)];%#ok<AGROW>
                duplicateCheckMat = [duplicateCheckMat;duplicateElementCount 0]; %#ok<AGROW>
            end
        end
        if ~isempty(duplicateCheckMat)
            if size(duplicateCheckMat,1)<20
                disptable(duplicateCheckMat, 'foundDuplicateEntry|remainEntry');
            else
                remAllCnt = sum(duplicateCheckMat(:,2)==0);
                remApxCnt = sum(duplicateCheckMat(:,2)==1);
                remSngCnt = sum(duplicateCheckMat(:,2)==2);
                disp(['RemoveAllSampleCnt(' num2str(remAllCnt) '),RemainApproximateCnt(' num2str(remApxCnt) '),RemainSingleCnt(' num2str(remSngCnt) ')']);
            end
            knownDistances.frameInfo(idxToRemove) = [];
            knownDistances.additionalCols(idxToRemove,:) = [];
        end
    end
end

function knownDistances = getKnownDistanceFromStruct(inputStruct, operationString)
    if isfield(inputStruct,'knownDistances')
        knownDistances = inputStruct.knownDistances;
    elseif isfield(inputStruct,'fileName')
        knownDistances = loadFromFile_01(inputStruct.fileName);
    elseif strcmpi(operationString,'tidyFolderAll') || strcmpi(operationString,'tidyFolderRenameDelete')
        knownDistances = inputStruct.sourceFolder;
    else
        error(['inputStruct must be a struct with either <fileName> or <knownDistances> as its field - operationString(' operationString ')']);
    end
end
function updateFileIfFileNameGiven(inputStruct, knownDistances, operationString) %#ok<INUSL>
    if isfield(inputStruct,'fileName')
        disp(['Updating known distances file to squeezed version (' inputStruct.fileName ') - operationString(' operationString ')']);
        save(inputStruct.fileName, 'knownDistances');
    end
end

function knownDistances = loadFromFile_01(knownDistancesFileName)
    try
        load(knownDistancesFileName, 'knownDistances');
    catch
        disp('knownDistances could not be loaded from knownDistancesFile');
        knownDistances = [];
    end
end