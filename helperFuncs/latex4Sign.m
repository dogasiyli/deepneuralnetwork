function latex4Sign(signID, optParamStruct)
    if ~exist('optParamStruct','var'), optParamStruct=[]; end
    srcFold = getStructField(optParamStruct, 'srcFold', getVariableByComputerName('srcFold'));
    
    try
        fileName = [srcFold filesep num2str(signID) filesep 'labelSummary.mat'];
        if exist(fileName, 'file')
            load(fileName, 'signID', 'surList', 'labelSummary', 'labelSwitches_uniq', 'listSummaryTotal', 'listSummarySpecial', 'listByUser', 'listByRepitition');
        else
            codeToRun = ['analyzeLabelsOfSign(' num2str(signID) ', struct(''srcFold'', ''' srcFold '''));'];
            disp([fileName ' doesnt exist. running <' newlinefunc() codeToRun newlinefunc()]);
            eval(codeToRun);
            return
        end
    catch err
        disp(['error occured : ' err.message])
        return
    end
    
    
    printSequenceSummaries(surList, labelSwitches_uniq);
    disp('----LabelCountSummary----');
    listSummaryTotal = getLabelSummaryTable(labelSummary);
    
    subSectionBeginStr = getSubSectionBegin_sign(signID , srcFold);
    khsInfo_str = keyHandShapesString(signID, srcFold);
    img_text = summaryImageLatex(signID);
    orderOOSLinkStr = getOOSLinkStr(signID);
    
    s = warning;
    warning('off');
    
    sectionBeginStr_oos = getSectionBegin_oos(signID);
    oos_text = later_oos(signID, labelSwitches_uniq);
    cnt_text = latexCntString(signID, listSummaryTotal, srcFold);
    [cyc_text, cycleMat] = findCyclicMotion(signID, labelSwitches_uniq);
    [ikhs_text, interchangeMat] = findInterchangableKHS(signID, labelSwitches_uniq);
    [floatingKHS_text, floatingMat] = findFloatingKHS(signID, labelSwitches_uniq);
    
    %if key-hand-shapes \textit{f} is removed from right hand labels, 
    %27 of the videos will have the exact same sequence of key-hand-shapes. 
    %We call key-hand-shapes \textit{f} \hyperlink{residual-frames}{residual-frame} 
    %hence can be disregarded when any study on sequence matching is done on Hospi-Sign Database.
    
    diary_start([srcFold filesep num2str(signID) filesep 'latexSubsection_' num2str(signID,'%03d') '.txt']);
    displayOnScreen(subSectionBeginStr);
    displayOnScreen(khsInfo_str);
    displayOnScreen(img_text);
    displayOnScreen(orderOOSLinkStr);
    diary_end();
    
    disp('XOXOXOXOXOXOXOXOXOXOXOX')
    
    diary_start([srcFold filesep num2str(signID) filesep 'latexOOSsection_' num2str(signID,'%03d') '.txt']);
    fprintf(sectionBeginStr_oos);
    disp(['Latex OrderOfSign Summary for sign(' num2str(signID) ')']);
    displayOnScreen(oos_text);
    disp(['Latex FrameCounts Summary for sign(' num2str(signID) ')']);
    displayOnScreen(cnt_text);
    %cycle information
    disp('-----------------------')
    disp(['Cyclic-motion information summary for sign(' num2str(signID) ')']);
    displayOnScreen(cyc_text);
    %interchangable key hand shape information
    disp('-----------------------')
    disp(['interchangable key hand shape information summary for sign(' num2str(signID) ')']);
    displayOnScreen(ikhs_text);
    %floating key hand shape information
    disp('-----------------------')
    disp(['floating key hand shape information summary for sign(' num2str(signID) ')']);
    displayOnScreen(floatingKHS_text);
    diary_end();
    warning(s);
end

function displayOnScreen(str)
    a = newlinefunc;
    if ~isempty(a)
        strList = strrep(str,a,'xlaklak');
        strList = strsplit(strList,'xlaklak');
        for i=1:length(strList)
            disp(strList{i});
        end
    else
        disp(str);
    end
end
function diary_start(diaryFileName)
    diary(diaryFileName);
    fprintf(newlinefunc());
    disp('xoxoxoxoxoxoxoxoxoxox');
    disp(datestr(now))
end
function diary_end()
    disp(newlinefunc());
    disp('xoxoxoxoxoxoxoxoxoxox');
    diary('off');
end

%sign explanation subsection
function subSectionBeginStr = getSubSectionBegin_sign(signID , srcFold)
    load([srcFold filesep 'BosphorusSignLabels.mat']);
    signMean = BosphorusSignLabels(signID,:); %#ok<NODEF>
    subSectionBeginStr = ['        \subsection{Sign' num2str(signID,'%03d') '-' signMean{2} '-' signMean{1} '}\label{subsection:sign' num2str(signID,'%03d') '}' newlinefunc()];
end
function khsInfo_str = keyHandShapesString(signID, srcFold)
    clusterNames_single = getClusterNamesAssignedFromTxt(srcFold, struct('signIDList',signID,'single0_double1',0));
    %clusterNames_both = getClusterNamesAssignedFromTxt(srcFold, struct('signIDList',signID,'single0_double1',1));
    sc = length(clusterNames_single);
    if sc<=3
        khsInfo_str = ['            This sign has only ' num2str(sc,'%d') ' key-hand-shapes ('];
    elseif sc==4
        khsInfo_str = '            This sign has few key-hand-shapes (';
    elseif sc<=7
        khsInfo_str = '            This sign has average number of key-hand shapes (';
    elseif sc<=8
        khsInfo_str = '            This sign has more than average number of key-hand shapes (';
    else
        khsInfo_str = '            This is one of the complicated signs that have many key-hand-shapes (';
    end
    
    for i = 2:sc
        khsInfo_str = [khsInfo_str '\hyperref[keyhandshape:' clusterNames_single{i} ']{' clusterNames_single{i} '}' ]; %#ok<*AGROW>
        if i<sc
            khsInfo_str = [khsInfo_str ', '];
        else
            khsInfo_str = [khsInfo_str ').'];
        end
    end
end
function imageString = summaryImageLatex(signID)
    imageString = ['              \begin{figure}[H]' newlinefunc() ...
                   '                \centering' newlinefunc() ...
                   '                \includegraphics[scale=0.5]{s' num2str(signID,'%03d') 'summaryLH.png}' newlinefunc() ...
                   '                \caption{sign' num2str(signID,'%03d') ', both left hand and right hand summary}' newlinefunc() ...
                   '                \label{fig:s' num2str(signID,'%03d') '_summaryLH}' newlinefunc() ...
                   '              \end{figure}' newlinefunc()];
end
function orderOOSLinkStr = getOOSLinkStr(signID)
    orderOOSLinkStr = ['              The order of key-hand-frames for this sign is shown in \reftable{Table:OrderOfSign' num2str(signID,'%03d') '}.' newlinefunc() ];
end

%Order of sign
function section_oos_BeginStr = getSectionBegin_oos(signID)
    section_oos_BeginStr = ['\newpage' newlinefunc() ...
                       '\section{Order of Sign-' num2str(signID,'%03d') '}\label{OOS:Sign' num2str(signID,'%03d') '}' newlinefunc() ...
                       '    Sign ' num2str(signID) ' is elaborated in \refsubsec{subsection:sign' num2str(signID,'%03d') '}.' newlinefunc()];
end
function oos_text = later_oos(signID, labelSwitches_uniq)
    topBase = ['    \begin{table}[!htbp]' newlinefunc() ...
               '       \centering' newlinefunc() ...
               '       \begin{tabular}{|c|c|c|c|c|c|} \hline' newlinefunc() ...
               '          \multicolumn{2}{|c|}{\textbf{Left Hand}} & \multicolumn{2}{c|}{\textbf{Right Hand}} & \multicolumn{2}{c|}{\textbf{Both Hands}} \\ \hline' newlinefunc() ...
               '          \textbf{sequence}   & \textbf{videoCnt}  & \textbf{sequence}   & \textbf{videoCnt}  & \textbf{sequence}   & \textbf{videoCnt}  \\ \hline' newlinefunc()];
    eachRow = ['          LHSSTR & LHCNT & RHSSTR & RHCNT & BHSSTR & BHCNT \\ \hline' newlinefunc()];
    botBase = ['       \end{tabular}' newlinefunc() ...
               '       \caption{Order of Sign-XXX_SIGNID_XXX}' newlinefunc() ...
               '       \label{Table:OrderOfSignXXX_SIGNID_XXX}' newlinefunc() ...
               '    \end{table}' newlinefunc()];

    oos_text = strrep(topBase, 'XXX_SIGNID_XXX', ['sign(' num2str(signID) ')']);
    
    switchCnts = [length(labelSwitches_uniq{1,1}) length(labelSwitches_uniq{2,1}) length(labelSwitches_uniq{3,1})];
    maxSwitcCnt = max(switchCnts);
    hc = 'LRB';
    sortInfos = cell(3,3);
    for h = 1:3
        sortInfos{h,1} = labelSwitches_uniq{h,4};
        [sortInfos{h,2}, sortInfos{h,3}] = sort(sortInfos{h,1},'descend');
    end
    
    for r = 1:maxSwitcCnt
        curRow = eachRow;
        for h = 1:3
            if (switchCnts(h)>=r)
                str_i = sortInfos{h,3}(r);
                labelSwitchSequence = labelSwitches_uniq{h,1}{1,str_i};
                curRow = strrep(curRow,[hc(h) 'HSSTR'], labelSwitchSequence);
                curRow = strrep(curRow,[hc(h) 'HCNT'],num2str(sortInfos{h,1}(str_i)));
            else
                curRow = strrep(curRow,[hc(h) 'HSSTR'],'-');
                curRow = strrep(curRow,[hc(h) 'HCNT'],'-');
            end
        end
        oos_text = [oos_text curRow];
    end 
    oos_text = [oos_text strrep(botBase, 'XXX_SIGNID_XXX', num2str(signID,'%03d'))];
end
function tableString = latexCntString(signID, listSummaryTotal, srcFold)

    topBase = ['    \begin{table}[htbp]' newlinefunc() ...
               '       \centering' newlinefunc() ...
               '       \begin{tabular}{|c|c|c|c|c|c|} \hline' newlinefunc() ...
               '          \textbf{XXX_SIGNID_XXX} & \multicolumn{3}{c|}{\textbf{SingleHand}} & \multicolumn{2}{c|}{\textbf{BothHand}} \\ \hline' newlinefunc() ...
               '          \textbf{labelID} & \textbf{keyHandShape} & \textbf{leftCnt} & \textbf{rightCnt} & \textbf{keyHandShape} & \textbf{bothCnt} \\ \hline' newlinefunc()];
    eachRow_base = ['          \textbf{COL01} & COL02 & COL03 & COL04 & COL05 & COL06 \\ \hline' newlinefunc()];
    bottomBase = ['       \end{tabular}' newlinefunc() ...
                  '       \caption{Number Of Key-Hand-Shapes-XXX_SIGNID_XXX}' newlinefunc() ...
                  '       \label{Table:NumOfKHSSignXXX_SIGNID_XXX}' newlinefunc() ...
                  '    \end{table}'];
        
        

    clusterNames_single = getClusterNamesAssignedFromTxt(srcFold, struct('signIDList',signID,'single0_double1',0));
    clusterNames_both = getClusterNamesAssignedFromTxt(srcFold, struct('signIDList',signID,'single0_double1',1));

    tableString = strrep(topBase, 'XXX_SIGNID_XXX', ['sign(' num2str(signID) ')']);
    
    numOfRows = size(listSummaryTotal,1);
    sc = length(clusterNames_single);
    bc = length(clusterNames_both);
    for r=0:numOfRows-1
        rowToAdd = eachRow_base;
        if r==0
            rowToAdd = strrep(rowToAdd,'COL01','-');%a,b,c,d...
            rowToAdd = strrep(rowToAdd,'COL02','noLabel');%labelNameSH
            rowToAdd = strrep(rowToAdd,'COL03',num2str(listSummaryTotal(1,2)));%leftHandCnt
            rowToAdd = strrep(rowToAdd,'COL04',num2str(listSummaryTotal(1,3)));%rightHandCnt
            rowToAdd = strrep(rowToAdd,'COL05','noLabel');%labelNameBH
            rowToAdd = strrep(rowToAdd,'COL06',num2str(listSummaryTotal(1,4)));%bothHandsCnt
        else
            rowToAdd = strrep(rowToAdd,'COL01',char('a'+r-1));%a,b,c,d...
            if sc>=r
                labelName = ['\hyperref[keyhandshape:' clusterNames_single{r} ']{' clusterNames_single{r} '}'];
                rowToAdd = strrep(rowToAdd,'COL02',labelName);%labelNameSH
                rowToAdd = strrep(rowToAdd,'COL03',num2str(listSummaryTotal(r+1,2)));%leftHandCnt
                rowToAdd = strrep(rowToAdd,'COL04',num2str(listSummaryTotal(r+1,3)));%rightHandCnt
            else
                rowToAdd = strrep(rowToAdd,'COL02','-');%labelNameSH
                rowToAdd = strrep(rowToAdd,'COL03','-');%leftHandCnt
                rowToAdd = strrep(rowToAdd,'COL04','-');%rightHandCnt
            end
            if bc>=r
                rowToAdd = strrep(rowToAdd,'COL05',clusterNames_both{r});%labelNameBH           
                rowToAdd = strrep(rowToAdd,'COL06',num2str(listSummaryTotal(r+1,4)));%bothHandsCnt
            else
                rowToAdd = strrep(rowToAdd,'COL05','-');%labelNameBH           
                rowToAdd = strrep(rowToAdd,'COL06','-');%bothHandsCnt
            end
        end
        tableString = [tableString rowToAdd];%#ok<AGROW>
    end
    tableString = [tableString strrep(bottomBase, 'XXX_SIGNID_XXX', num2str(signID,'%03d'))];

end