function [W_3D, W, infoGain, remainingFeatIDs, accPerMethod, bestMethodID_not1, methodIDPref, confMatAll] = ldaDG(trainData, trainLabels, testData, testLabels)
    % load('PR_HW1_Data_All.mat');
    % a = PR_HW1_Data_All;
    % clear PR_HW1_Data_All
    % % training_C1=a(1:30,1:4)';training_C2=a(51:80,1:4)';training_C3=a(101:130,1:4)';
    % % trainData = [training_C1;training_C2;training_C3];
    % trainData = [a(1:30,1:4);a(51:80,1:4);a(101:130,1:4)];
    % trainLabels = [ones(30,1);2*ones(30,1);3*ones(30,1)];
    % % test_C1=a(31:50,1:4)';test_C2=a(81:100,1:4)';test_C3=a(131:150,1:4)';
    % testData = [a(31:50,1:4);a(81:100,1:4);a(131:150,1:4)];
    % testLabels = [ones(20,1);2*ones(20,1);3*ones(20,1)];
    % clear a;
    
    orig_state = warning;
    warning('off','all');
     
    %here will be used 4 different methods
    methodCount = 4;
    classCount = length(unique(trainLabels));
    priorProbs = hist(trainLabels,1:classCount)./length(trainLabels);%(1/classCount)*ones(1,classCount)
    if length(priorProbs)~=classCount
        priorProbs = (1/classCount)*ones(1,classCount);
    end
     
    confMatAll_Test = cell(methodCount,1);
    confMatAll_Train = cell(methodCount,1);
    accTrain = zeros(methodCount,1);
    accTest = zeros(methodCount,1);
 
 
    %% Get mean vectors and covariance vectors
    irrelevantFeatCount = 1;
    [numOfSamples, featCount] = size(trainData);
    remainingFeatIDs = 1:featCount;
    while irrelevantFeatCount>0
        meanAll = mean(trainData)';
        meanCells = cell(classCount,1);
        covarianceCells = cell(classCount,1);
        Sw = zeros(size(trainData,2));
        Sb = zeros(size(trainData,2));
        for c = 1:classCount
            meanCells{c,1} = mean(trainData(trainLabels==c,:))'; %4x1
            covarianceCells{c,1} = cov(trainData(trainLabels==c,:)); %4x4

            Sw_c = covarianceCells{c,1}*(sum(trainLabels==c)-1);
            Sb_c = meanCells{c,1}-meanAll;
            Sb_c = sum(trainLabels==c)*(Sb_c*Sb_c');

            Sb = Sb + Sb_c;
            Sw = Sw + Sw_c;
        end
        [irrelevantFeatCount, relevantFeats_cur] = findNoInfoFeats( Sw, Sb);
        if irrelevantFeatCount>0
            irrelevFeats_cur = 1:length(remainingFeatIDs);
            irrelevFeats_cur(relevantFeats_cur) = [];        
            remainingFeatIDs(irrelevFeats_cur) = [];
            trainData = trainData(:,relevantFeats_cur);
        end
    end
    
     
    %A = inv(Sw)*Sb;
    A = inv(Sw).*Sb;
    %[eigVecs,eigVals] = eig(A);
    [W,S,~] = svd(A);
    W_lat = diag(S)./ sum(diag(S));
    infoGain = cumsum(W_lat');
    clear A eigVecs eigVals S Sw Sb
    
    W_3D = zeros(length(remainingFeatIDs)+1,classCount,3);
    if nargout<5
        methodIDPref = [];
        accPerMethod = [];
        confMatAll = [];
        return
    end
     
    %% Case 1
    % Get decision boundaries :
    W_cells = cell(classCount,1);
    w_cells = cell(classCount,1);
    w0_cells = cell(classCount,1);
    for c = 1:classCount
        W_cells{c,1} = -.5*(inv(covarianceCells{c,1})); %d by d
        w_cells{c,1} = covarianceCells{c,1}\meanCells{c,1}; %d by 1-inv(covarianceCells{c,1})*meanCells{c,1}
        w0_cells{c,1} = -.5*meanCells{c,1}'*w_cells{c,1}-.5*log(det(covarianceCells{c,1})); %1x1
    end
    %Test each tempTestArray obj with dec boundaries
    confMatAll_Train{1,1} = compConfMatSpecial(trainData,trainLabels,W_cells,w_cells,w0_cells,priorProbs);
    if exist('testData','var')
        confMatAll_Test{1,1} = compConfMatSpecial(testData,testLabels,W_cells,w_cells,w0_cells,priorProbs);
    end
    %% Case 2
    % cov_Case2 = (1/3)*cov1 + (1/3)*cov2 + (1/3)*cov3;
    % invcov = inv(cov_Case2);
    % Wzero = zeros(4,4);
    % w1=invcov*m1;w2=invcov*m2;w3=invcov*m3;
    % w10=-.5*m1'*invcov*m1;w20=-.5*m2'*invcov*m2;w30=-.5*m3'*invcov*m3;
 
    %cov_Case2 = mean(reshape(cell2mat(covarianceCells), [4, 4, 3]), 3)
    %cov_Case2 = sum(bsxfun(@times,reshape(cell2mat(covarianceCells), [4, 4, 3]),reshape(priorProbs,[1 1 3])),3);
    cov_Case2 = zeros(length(covarianceCells{1,1}));
    for c = 1:classCount
        cov_Case2 = cov_Case2 + covarianceCells{c,1}; %4x4
    end
    cov_Case2 = cov_Case2./classCount;
    invcov = inv(cov_Case2)';
    for c = 1:classCount
        W_cells{c,1} = zeros(size(cov_Case2)); %4x4
        w_cells{c,1} = invcov*meanCells{c,1}; %4x1
        w0_cells{c,1} = -.5*meanCells{c,1}'*invcov*meanCells{c,1}; %1x1
    end
    %Test each tempTestArray obj with dec boundaries
    [confMatAll_Train{2,1}, W_3D(:,:,1)] = compConfMatSpecial(trainData,trainLabels,[],w_cells,w0_cells,priorProbs);
    if exist('testData','var')
        %confMatAll_Test{2,1} = compConfMatSpecial(testData,testLabels,[],w_cells,w0_cells,priorProbs);
        confMatAll_Test{2,1} = compConfMatSpecial2(testData,testLabels,squeeze(W_3D(:,:,1)));
    end
 
    %% Case 3
    cov_Case3 = cov_Case2 .* eye(size(cov_Case2));
    %invcov = inv(cov_Case3);
    for c = 1:classCount
        %W_cells{c,1} = zeros(4,4); %4x4
        w_cells{c,1} = cov_Case3\meanCells{c,1}; %4x1
        w0_cells{c,1} = -.5*meanCells{c,1}'*(cov_Case3\meanCells{c,1}); %1x1
    end
    %Test each tempTestArray obj with dec boundaries
    [confMatAll_Train{3,1}, W_3D(:,:,2)] = compConfMatSpecial(trainData,trainLabels,[],w_cells,w0_cells,priorProbs);
    if exist('testData','var')
        %confMatAll_Test{3,1} = compConfMatSpecial(testData,testLabels,[],w_cells,w0_cells,priorProbs);
        confMatAll_Test{3,1} = compConfMatSpecial2(testData,testLabels,squeeze(W_3D(:,:,2)));
    end
    %% Case 4
    for c = 1:classCount
        %W_cells{c,1} = zeros(4,4); %4x4
        w_cells{c,1} = meanCells{c,1}; %4x1
        w0_cells{c,1} = -.5*meanCells{c,1}'*meanCells{c,1}; %1x1
    end
    %Test each tempTestArray obj with dec boundaries
    [confMatAll_Train{4,1}, W_3D(:,:,3)] = compConfMatSpecial(trainData,trainLabels,[],w_cells,w0_cells,priorProbs);
    if exist('testData','var')
        %confMatAll_Test{4,1} = compConfMatSpecial(testData,testLabels,[],w_cells,w0_cells,priorProbs);
        confMatAll_Test{4,1} = compConfMatSpecial2(testData,testLabels,squeeze(W_3D(:,:,3)));
    end
    for m=1:methodCount
        if exist('testData','var')
            accTest(m) = sum(diag(confMatAll_Test{m,1}))/sum(confMatAll_Test{m,1}(:));
        end
        accTrain(m) = sum(diag(confMatAll_Train{m,1}))/sum(confMatAll_Train{m,1}(:));
    end

    if exist('testData','var')
        [accPerMethod, methodIDPref] = sort(accTest);
        bestMethodID_not1 = findBestMethodID_Not1(accTest);
        confMatAll = struct;
        confMatAll.Test = confMatAll_Test;
        confMatAll.Train = confMatAll_Train;
    else
        [accPerMethod, methodIDPref] = sort(accTrain);
        bestMethodID_not1 = findBestMethodID_Not1(accTrain);
        confMatAll = confMatAll_Train;
    end 

    warning(orig_state);
end

function [bestMethodID_not1] = findBestMethodID_Not1(accT)
    accT = accT(2:end);
    bestAccuracy = max(accT);
    bestMethodID_not1 = find(accT==bestAccuracy,1,'last');
end

function [ConfusionMatrix, W] = compConfMatSpecial(data,labels_given,W_cells,w_cells,w0_cells,priorProbs)
    classCount = length(priorProbs);
    [n,d] = size(data);
    minimizeSampleCount = false;
    testWithForLoop = false;
    
    if minimizeSampleCount && n>10000
        n = 10000;
        slctID = randi(n,1,n);
        data = data(slctID,:);
        labels_given = labels_given(slctID);
        clear slctID
    end
    W = [];
    if ~isempty(W_cells)
        G = zeros(n,classCount);
        if testWithForLoop
            G2 = zeros(n,classCount);
            tic;
            for i=1:n
                for c = 1:classCount
                    G2(i,c)= classifySpecial(data(i,:)',W_cells{c,1},w_cells{c,1},w0_cells{c,1},priorProbs(c));
                end
            end
            t1 = toc;
            tic;
        end
        for c = 1:classCount
            X = data*W_cells{c,1};
            X = sum(X.*data,2);
            X = X + data*w_cells{c,1} + w0_cells{c,1} + log(priorProbs(c));
            G(:,c)= X;
        end
        if testWithForLoop
            t2 = toc;
            assert(sum(G(:)-G2(:))==0);
            disp(['t1(' num2str(t1) '), t2(' num2str(t2) ')']);
        end
    else
        W = zeros(d+1,classCount);
        for c = 1:classCount
            % Constant
            W(1:d,c) = w_cells{c,1};
            % Linear
            W(d+1,c) = w0_cells{c,1} + log(priorProbs(c));
        end
        X = [data,ones(n,1)];
        G = X*W;
    end
    [~, labels_predicted] = max(G,[],2);
    ConfusionMatrix = confusionmat(labels_given,labels_predicted);
end

function [ConfusionMatrix, W] = compConfMatSpecial2(data,labels_given,W)
    n = size(data,1);
    X = [data,ones(n,1)];
    G = X*W;
    [~, labels_predicted] = max(G,[],2);
    ConfusionMatrix = confusionmat(labels_given,labels_predicted);
end

function retVal=classifySpecial(arTest,W,w,w0,prior)
    retVal=arTest'*W*arTest + w'*arTest + w0 + log(prior);
end