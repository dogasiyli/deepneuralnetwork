function loopOverProjectFolders
    global dropboxPath;
    netFolderMain = dropboxPath;
    subFolderNames = getFolderList(netFolderMain, true, true);
    sfnInitID = length(netFolderMain)+1;
    folderCnt = length(subFolderNames);
    compNameTagInits = 'SDWL';


    figureName_trainAcc = 'trainAccForDisp';
    h_trainAcc = figure('Name',figureName_trainAcc,'NumberTitle','off');clf;hold on;

    for c = compNameTagInits
        for i=1:folderCnt
            curSF = subFolderNames{i,1}(sfnInitID:end);
            if ~isempty(curSF) && strcmp(curSF(1),c)
                curProjCode = curSF(1:3);
                disp(['Cur project code is <' curProjCode '>']);
                % Get a list of all files and folders in this folder.
                allFiles = dir(subFolderNames{i,1});
                % Get a logical vector that tells which is a file.
                dirFlags = [allFiles.isdir];
                % Extract only those that are directories.
                fileListStruct = allFiles(dirFlags==0);
                % Extract only names of those directories.
                fileList = {fileListStruct.name}';

                %get the GOMC file
                try
                    gomcInd = find(not(cellfun('isempty', strfind(fileList, '_gomc_'))));
                    if gomcInd>0
                        gomcFileName = fileList{gomcInd,1};
                    else
                        gomcFileName = '';
                    end
                    if strcmp(gomcFileName,'')
                        disp(['no <gomc> file under proj<' curProjCode '>']);
                    else
                        %now we found the _gomc_ file
                        load([subFolderNames{i,1} '\' gomcFileName],'outputMatrixStructCNN');
                    end
                    if ~exist('outputMatrixStructCNN','var')
                        disp(['no <outputMatrixStructCNN> variable under proj<' curProjCode '>']);
                    end
                catch
                    continue;
                end
                trBatchCnt = outputMatrixStructCNN.accPerBatch.batchCntTr;
                apbMat = outputMatrixStructCNN.accPerBatch.Mat;
                importantColumns = [outputMatrixStructCNN.accPerBatch.batchColumnsTr(end-1:end);outputMatrixStructCNN.accPerBatch.batchColumnsVa(end-1:end);outputMatrixStructCNN.accPerBatch.batchColumnsTe(end-1:end)];
                epochCompleted = outputMatrixStructCNN.iterationInfo.epochID_main;
                epochPlanned = outputMatrixStructCNN.iterationInfo.epochID_sub;

                minFuncMat = outputMatrixStructCNN.minFuncLog.resultMatrix;
                minFuncMat(minFuncMat(:,2)==0,:) = [];

                iterCntPerBatch = max(minFuncMat(:,6));
                trainAccForDisp = minFuncMat(minFuncMat(:,6)~=0,[2 4 11]);


                figure(h_trainAcc);
                %get all the epoch changes and the y values there
                [x_epochChng, x_batchChng, y_epochChng, y_batchChng, accMaxCur] = getImportantCols(trainAccForDisp);
                plot(trainAccForDisp(:,3));
                %plot([x_epochChng;x_epochChng],[zeros(size(y_epochChng));y_epochChng],'k');
                %plot([x_batchChng;x_batchChng],[zeros(size(y_batchChng));y_batchChng],'r');
            end
        end
    end
end


function [x_epochChng, x_batchChng, y_epochChng, y_batchChng, accMaxCur] = getImportantCols(trainAccForDisp)
    x_epochChng = find(trainAccForDisp(2:end,1)-trainAccForDisp(1:end-1,1)~=0)';
    x_batchChng = find(trainAccForDisp(2:end,2)-trainAccForDisp(1:end-1,2)~=0)';
    y_epochChng = trainAccForDisp(x_epochChng,3)';
    y_batchChng = trainAccForDisp(x_batchChng,3)';
    accMaxCur = max(trainAccForDisp(:,3));
end