compName = getComputerName;
switch compName
    case 'doga-MSISSD'
        saveToFoldRoot = '/mnt/USB_HDD_1TB/alptekine';
    otherwise
        saveToFoldRoot = 'D:\alptekine';
end


absenceOfBH = 'singleToBothVec';%'copyDiverseHand';
optParamStruct_editLabelsWithFolders_forAlptekin = struct('srcFold',saveToFoldRoot, 'saveToFolder', saveToFoldRoot,'absenceOfBH', absenceOfBH);
signIDList_notReady = [290 296 396 403];
signIDList_3 = [7 13 49 ];
signIDList_4_11 = [69 74 85 86 87 88 89 96];
signIDList_12_20 = [112 125 221 222 247 269 273 277 278];
signIDList_21_30 = [310 335 339 353 404 424 428 521 533];
signIDList = [ 593];%290 396 403 536 586
userList = 2:7;

optParamStruct_copyForWEB = struct('toWebFolder', saveToFoldRoot, ...
                                   'copyTextMeaningFileOption', true,...
                                   'copyAutoClusterOption', false, ...
                                   'copyLabelFilesOption', true, ...
                                   'copyVidSummaryOption', false, ...
                                   'copyCroppedZipOption', false, ...
                                   'copyLabelSummaryOption', false);
pvLoop = [];
pvCopy = [];
for s = signIDList
    if s==290
        optParamStruct_editLabelsWithFolders_forAlptekin.sbVec = [1 2 3 4 4 4 5 6 7];
    elseif s==403
        optParamStruct_editLabelsWithFolders_forAlptekin.sbVec = 1:12;
    else
        optParamStruct_editLabelsWithFolders_forAlptekin.sbVec = [];
    end
    try
        surSlctListStruct_loopOnHospiSign = struct('createMode', 1, 'signIDList',s,'userList',userList,'maxRptID',10);
        pv01 = loopOnHospiSign('copyForWEB', surSlctListStruct_loopOnHospiSign, optParamStruct_copyForWEB);
        pv02 = editLabelsWithFolders(s, 1, optParamStruct_editLabelsWithFolders_forAlptekin);
    catch
        pvLoop = [pvLoop;[s 0 0]];
        pvCopy = [pvCopy;[s 0 0]];
    end
    pvLoop = [pvLoop;pv01];
    pvCopy = [pvCopy;pv02];
end
pvLoop
pvCopy