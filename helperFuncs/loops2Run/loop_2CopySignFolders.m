signID = 49;
userList = 2:7;
optParamStruct_editLabelsWithFolders = struct('saveToFolder', 'D:\editLabelsFrames');
copyCroppedZipOption = false;
copyLabelSummaryOption = false;
optParamStruct_copyForWEB = struct('enforceReCreateNew',true, 'copyAutoClusterOption', false, 'copyLabelFilesOption', true,'copyVidSummaryOption', false,'copyCroppedZipOption', copyCroppedZipOption, 'copyLabelSummaryOption', copyLabelSummaryOption);
optParamStruct_analyzeLabelsOfSign = struct('srcFold','D:\toWEB','saveScreenOutputFolder','srcFold','removePreviousSignSummaryFile',false);

%% STEP-1 : now create the folders to check
pv01 = editLabelsWithFolders(signID, 1, optParamStruct_editLabelsWithFolders);
%% STEP-2 : now reset labels according to new labels in the folders
pv02 = editLabelsWithFolders(signID, 2, optParamStruct_editLabelsWithFolders);
%% STEP-3 : now update the labels with backup folder
moveFiles(signID,2:7, struct('LabelFiles','remainLatest'));
%% STEP-4 : prepare a folder to copy to lumbar
pv04 = loopOnHospiSign('copyForWEB',struct('createMode', 1, 'signIDList',signID,'userList',userList,'maxRptID',10),optParamStruct_copyForWEB);
%% STEP-5 : now run an analysis on the labelling
[ labelSwitches_uniq, labelSwitches_alll ] = analyzeLabelsOfSign(signID, optParamStruct_analyzeLabelsOfSign);
%if labels are ok goto STEP-6 else go to step-3 again

%% STEP-6 : now prepare a zip folder for Gizem containing:
optParamStruct_editLabelsWithFolders_forGizem = struct('saveToFolder', 'D:\gizeme')
pv05 = editLabelsWithFolders(signID, 1, optParamStruct_editLabelsWithFolders_forGizem)




surSlctListStruct_resetLabels = struct('createMode', 1, 'signIDList',586,'userList',2:7,'maxRptID',10);
optParamStruct_resetLabels_single = struct('enforceReCreateNew',true, 'oldLabelsVec', 1:8, 'newLabelsVec', [1 2 3 4 5 5 6 7], 'single0_double1', 0, 'copyToFolder', 'D:\toWEB');
pv_resetLabels = loopOnHospiSign('resetLabels', surSlctListStruct_resetLabels, optParamStruct_resetLabels_single)