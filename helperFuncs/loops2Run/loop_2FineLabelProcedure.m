function loop_2FineLabelProcedure(optParamStruct)
    % signsCompleted_20181217 - [13 49 69 74 85 86 88 273]
    % signsCompleted_20181218 - [7 87 89 96]
    % signIDList_notReady = [290 296 396 403];
    % signIDList = [112 125 221 222 247 269 277 278 310 335 339 353 404 424 428 521 533];
    
    %optParamsStruct_l2flp2 = struct('editFolderRoot', '/home/doga/Desktop', 'signIDList', 403, 'copyFrames1_editLabels2', 2);

    srcFold = getStructField(optParamStruct, 'srcFold', getVariableByComputerName('srcFold'));
    signIDList = getStructField(optParamStruct, 'signIDList', []);
    editFolderRoot = getStructField(optParamStruct, 'editFolderRoot', 'D:\gizeme');%'D:\alptekine'
    copyFrames1_editLabels2 = getStructField(optParamStruct, 'copyFrames1_editLabels2', 2);
    optParamStruct_editLabelsWithFolders = struct('srcFold',srcFold, 'saveToFolder', editFolderRoot, 'updateLabelsAt', 'srcFold');
    
    pvEdit = [];
    for s = signIDList
        try
            pv03 = editLabelsWithFolders(s, copyFrames1_editLabels2, optParamStruct_editLabelsWithFolders);
        catch
            pvEdit = [pvEdit;[s 0 0]]; %#ok<AGROW>
        end
        pvEdit = [pvEdit;pv03]; %#ok<AGROW>
    end
    if ~isempty(pvEdit)
        disp(['pvEdit = ' mat2str(pvEdit)])
    end
end