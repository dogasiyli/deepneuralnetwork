nos = 41;
compName = getComputerName;
switch compName
    case  'doga-MSISSD'
        base_DataFolder = '/mnt/USB_HDD_1TB/';
    case  'doga-msi-ubu'
        base_DataFolder = '/home/doga/DataFolder/';
    otherwise
        exit(-78)
end
fold_base = strrep([base_DataFolder filesep 'neuralNetHandImages_nosXX_rs224'],'XX',num2str(nos));
fold = [fold_base filesep 'imgs'];
data_identifier_list = {'hog','skeleton','sn','hgsk','hgsn','snsk','hgsnsk'};
pca_cnt_list_1 = [128 256 512];
pca_cnt_list_2 = [32 64 96];
user_list = [2 3 4 5 6 7];
k_fold_cnt = 5;
rand_seed = -1;

result_cells = {'data_identifier','pca','u2','u3','u4','u5','u6','u7'};
rcci = 2;
for di = 1:length(data_identifier_list)
    data_dentifier = data_identifier_list{di};
    if strcmpi(data_dentifier,'skeleton')
        pca_cnt_list = pca_cnt_list_2;
    else
        pca_cnt_list = pca_cnt_list_1;
    end
    for pca_count = pca_cnt_list
        for ui = 1:length(user_list)
            test_user = user_list(ui);
            acc_best = run_xv_fold_exp(fold, data_dentifier, pca_count, test_user, k_fold_cnt, rand_seed, struct('enforce_recreate',false,'reDrawFig',false));
            enforce_recreate = acc_best<30;
            if enforce_recreate
                acc_best = run_xv_fold_exp(fold, data_dentifier, pca_count, test_user, k_fold_cnt, rand_seed, struct('enforce_recreate',true,'reDrawFig',true));
            else
                acc_best = run_xv_fold_exp(fold, data_dentifier, pca_count, test_user, k_fold_cnt, rand_seed, struct('enforce_recreate',false,'reDrawFig',true));
            end
            result_cells(rcci,[1 2 ui+2]) = {data_dentifier,pca_count,acc_best}; 
        end
        rcci = rcci+1;
    end
end
rcci = rcci-1;
save_result_csv_fold = strrep(fold, [filesep 'imgs'], [filesep 'elm_results']);
if  k_fold_cnt==5 && rand_seed<0
    fnstr = 'su';
else
    fnstr = ['rs' num2str(rand_seed)];
end
result_csv_file_name = ['resultTable_' fnstr '.csv'];
result_csv_file_name = [save_result_csv_fold filesep result_csv_file_name];
T = cell2table(result_cells(2:end,:),'VariableNames',result_cells(1,:));
writetable(T,result_csv_file_name);