function [D, dWas] = makeDistMatFull(X)
    [d,n] = size(X);
    if d==n
        [D, dWas] = fixDistanceMatrix(X);
    else%d~=n
        %X is not a distance matrix
        dWas = ['d(' num2str(d) ') by (' num2str(n) ')n matrix of samples'];
        D = getDistanceMatrixFrom_d_n_matrix(X);
    end
end