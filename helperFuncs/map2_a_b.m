function [x_out, summaryStruct] = map2_a_b( x_in, mapmin, mapmax )
%map2_a_b map x_in values to [mapmin mapmax]
%   Detailed explanation goes here
    x_out = x_in;
    minFound = min(x_out(:));
    x_out = x_out - minFound; % map minimum value of x_in to 0 (thereby changing the maximum)
    
    maxFound = max(x_out(:));
    x_out = x_out ./ maxFound; % scale between 0 and 1
    
    mapDif = mapmax-mapmin;
    x_out = x_out * mapDif;
    
    x_out = x_out + mapmin;
    
    if nargout>1
        summaryStruct = struct;
        summaryStruct.minFound = minFound;
        summaryStruct.maxFound = maxFound;
        summaryStruct.mapDif = mapDif;
    end
end

