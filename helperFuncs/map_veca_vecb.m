function vec_mapped = map_veca_vecb( vec, old, new)
    [found, idx] = ismember(vec, old);
    vec_mapped = vec;
    vec_mapped(found) = new(idx(found));
end

