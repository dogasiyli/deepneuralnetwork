function maximizeFigureWindow( figHandle )
%https://www.mathworks.com/matlabcentral/fileexchange/31793-minimize-maximize-figure-window
    warning('off','MATLAB:HandleGraphics:ObsoletedProperty:JavaFrame'); % disable warning message
    jFrame = get(figHandle,'JavaFrame');pause(0.3);
    set(jFrame,'Maximized',true);
    warning('on','MATLAB:HandleGraphics:ObsoletedProperty:JavaFrame'); % disable warning message
end

