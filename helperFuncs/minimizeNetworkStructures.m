function inOutSizes = minimizeNetworkStructures( networkStructFolder, displayInfoLevel, removeOld )

    %displayInfoLevel = 2;
    fieldTreshold = 1;

    listing = dir(networkStructFolder);
    listing([listing.isdir]) = [];
    %a file is valid as long as it has some cnnnet in it
    numberOfFiles_All = size(listing,1);
    inOutSizes = zeros(numberOfFiles_All, 2);
    for f = 1:size(listing,1)%ignoring the first 2 as . and ..
        curFileName = listing(f,1).name;
        if ~strcmpi(curFileName(end-3:end),'.mat')
            disp(['skipping - ' curFileName]);
            continue
        end
        disp([num2str(f) '******Loading cnnnet from (' curFileName ')']);
        fileNameFull_Old =[networkStructFolder curFileName];
        [cnnnetHuge, initFileMode, allVariables] = loadCnnetFromFile(fileNameFull_Old);
        if isempty(cnnnetHuge)
            disp(['skipping - ' curFileName]);
            continue
        end        
        if ~isempty(allVariables)
            for i=1:length(allVariables.FileWhosOut)
                eval([allVariables.FileWhosOut(i,1).name ' = allVariables.Variables.' allVariables.FileWhosOut(i,1).name  ';']);
                if displayInfoLevel>0
                    disp([allVariables.FileWhosOut(i,1).name '-  assigned.'])
                end
            end
        end    
        
        [cnnnet, inSizeMB, outSizeMB ] = getCNNETreadyForNewData( cnnnetHuge, fieldTreshold, [], displayInfoLevel);

        nameString = allVariables.NameString;
        %nameString = strrep(nameString, initFileMode, 'cnnnet');
        eval([initFileMode ' = cnnnet;']);
        
        inOutSizes(f,:) = [inSizeMB, outSizeMB];
        if removeOld
            eval(['save(fileNameFull_Old,' nameString ');']);
        else
            fileNameFull_New = [networkStructFolder 'min_' curFileName];
            eval(['save(fileNameFull_New,' nameString ');']);         
        end
    end
end

