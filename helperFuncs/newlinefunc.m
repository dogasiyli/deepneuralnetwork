function retval = newlinefunc()
    try
        retval = newline;
    catch
        if ismac
            % Code to run on Mac platform
            retval = '\r';
        elseif isunix
            % Code to run on Linux platform
            retval = '\n';
        elseif ispc
            % Code to run on Windows platform
            retval = '\r\n';
        else
            retval = '\n';
        end        
    end  
end