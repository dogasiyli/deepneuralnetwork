function [ digit_mat ] = getDigitImage( digit2get, rowSize, colSize)%, colorStyle
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

%     if ~exist('colorStyle','var') || isempty(colorStyle)
%         colorStyle = 'BW';
%     end
    if ~exist('colSize','var') 
        colSize = [];
    end
    if ~exist('rowSize','var') 
        rowSize = [];
    end
    
    global dnnCodePath;
    srcFoldName = [dnnCodePath 'helperFuncs' filesep 'numberImages' filesep];

    %first check if the file exist
    fileName_Number_png = [srcFoldName 'digit' num2str(digit2get,'%d') '.png'];
    fileName_Number_mat = [srcFoldName 'digit' num2str(digit2get,'%d') '.mat'];
    if exist(fileName_Number_mat, 'file')
        load(fileName_Number_mat, 'digit_mat');
    else
        fileName_palette = [srcFoldName 'numberPalette.jpg'];
        imPalette = imread(fileName_palette);
        if digit2get==0
            digit2get=10;
        end
        colID = mod(digit2get, 4);
        if colID==0
            colID = 4;
        end
        rowID = ceil((digit2get-colID)/4)+1;
        frR = (rowID-1)*400 + 1;
        toR = frR + 399;
        frC = (colID-1)*400 + 1;
        toC = frC + 399;
        digit = imPalette(frR:toR,frC:toC,:);
        %only the black pixels should stay, all other would be set to white
        digitSum = sum(digit,3);
        blackPixSlct = digitSum<100;
        digit = double(blackPixSlct)*255;
        colSum = sum(double(blackPixSlct));
        rowSum = sum(double(blackPixSlct),2);
        
        frC = find(colSum>0,1,'first');
        toC = find(colSum(frC:end)==0,1,'first')+frC-2;
        frR = find(rowSum>0,1,'first');
        toR = find(rowSum(frR:end)==0,1,'first')+frR-2;
        
        digit_mat = digit(frR:toR,frC:toC);
        
        save(fileName_Number_mat, 'digit_mat');
        imwrite(digit_mat, fileName_Number_png);
    end
    newSize = size(digit_mat);
    if ~isempty(colSize)
        newSize(2) = colSize;
    end
    if ~isempty(rowSize)
        newSize(1) = rowSize;
    end
    if ~isempty(colSize) || ~isempty(rowSize)
        digit_mat = imresize(digit_mat, newSize);
    end
end

