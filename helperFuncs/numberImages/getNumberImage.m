function [ numIm ] = getNumberImage( num2get, rowSize, colSize, paletteSize)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

    if ~exist('colSize','var') 
        colSize = [];
    end
    if ~exist('rowSize','var') || isempty(rowSize)
        rowSize = 215;
    end
    %digit rowSize colSize
    digitRCsiz = [...
      0     215     169;...
      1     212     119;...
      2     212     173;...
      3     215     172;...
      4     211     181;...
      5     211     172;...
      6     215     171;...
      7     208     168;...
      8     216     171;...
      9     215     171];
  %min      208     119
  %max      215     181
    
    numIm = [];
    while num2get>0
        currentDigit = mod(num2get, 10);
        currentDigit_im = getDigitImage(currentDigit, rowSize);
        if isempty(numIm)
            numIm = currentDigit_im;
        else
            numIm = [currentDigit_im numIm]; %#ok<AGROW>
        end
        num2get = num2get-currentDigit;
        num2get = round(num2get/10);
    end
    
    newSize = size(numIm);
    if ~isempty(colSize)
        newSize(2) = colSize;
    end
    if ~isempty(rowSize)
        newSize(1) = rowSize;
    end
    if ~isempty(colSize) || ~isempty(rowSize)
        numIm = imresize(numIm, newSize);
    end
    
    if exist('paletteSize','var')
        if length(paletteSize)==1
            paletteSize = [paletteSize paletteSize];
        end
        if paletteSize(1)>=newSize(1) && paletteSize(1)>=newSize(1) 
            numIm_palette = zeros(paletteSize);
            %now put numIm into numIm_palette
            colDiff = paletteSize(2)-newSize(2);
            colInit = max(1,floor(colDiff/2));
            colEnd = colInit + newSize(2) - 1;

            rowDiff = paletteSize(1)-newSize(1);
            rowInit = max(1,floor(rowDiff/2));
            rowEnd = rowInit + newSize(1) - 1;


            numIm_palette(rowInit:rowEnd,colInit:colEnd) = numIm;
            numIm = numIm_palette;
        else
            numIm = imresize(numIm, paletteSize);
        end
    end
end

