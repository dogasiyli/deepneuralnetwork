function [ distribMatNew, batchMoveSummary_Rfr_Cto ] = planClustersIntoBatches( X, Y_lab, Y_pred, Y_clus, distribMatNew )
    
    categoryVec = unique([Y_lab;Y_pred]);
    categoryCount = length(categoryVec);
    clusterCount = length(unique(Y_clus));
    Y_batch = distribMatNew(:,3);
    batchCount = length(unique(Y_batch));


    [confMat_Overall, precisionMat, recallMat] = performPrecisionRecallAnalysis(Y_lab, Y_pred, Y_clus);
    
    
    kl_conf_init_id_cell = cell(clusterCount, categoryCount^2);
    kl_conf_init_count_mat = zeros(clusterCount, categoryCount^2);
    kl_batch_init_id_cell = cell(clusterCount, batchCount);
    kl_batch_init_count_mat = zeros(clusterCount, batchCount);
    kl_batch_final_id_cell = cell(clusterCount, batchCount);
    kl_batch_final_count_mat = zeros(clusterCount, batchCount);
    
    batch_conf_id_orig_cell = cell(batchCount, categoryCount^2);%Batch_Conf_Number Of Samples_ORIGinal_MATrix
    batch_conf_nos_orig_mat = zeros(batchCount, categoryCount^2);%Batch_Conf_Number Of Samples_ORIGinal_MATrix
    %batch_conf_nos_left_mat = zeros(batchCount, categoryCount^2);%Batch_Conf_Number Of Samples_ORIGinal_MATrix
    batch_conf_id_final_cell = cell(batchCount, categoryCount^2);%Batch_Conf_FINAL_ID_CELL
    batch_conf_nos_final_mat = zeros(batchCount, categoryCount^2);%Batch_Conf_Number Of Samples_ORIGinal_MATrix
    batchMoveSummary_Rfr_Cto = zeros(batchCount,batchCount);
    
    for c = 1:clusterCount
        for b = 1:batchCount
            slct = Y_clus==c & distribMatNew(:,3)==b;
            inds = reshape(find(slct==1),1,[]);
            kl_batch_init_id_cell{c,b} = inds;
            kl_batch_init_count_mat(c,b) = length(inds);
        end
        for irg = 1:categoryCount
            for jcp = 1:categoryCount
                indConf =  sub2ind_conf(categoryCount, irg, jcp);
                slct = Y_clus==c & distribMatNew(:,5)==irg & distribMatNew(:,6)==jcp;
                inds = reshape(find(slct==1),1,[]);
                kl_conf_init_id_cell{c,indConf} = inds;
                kl_conf_init_count_mat(c,indConf) = length(inds);
            end
        end
    end
    for b = 1:batchCount
        for irg = 1:categoryCount
            for jcp = 1:categoryCount
                indConf =  sub2ind_conf(categoryCount, irg, jcp);
                slct = distribMatNew(:,3)==b & Y_lab==irg & Y_pred==jcp;
                inds = find(slct==1);
                batch_conf_id_orig_cell{b,indConf} = inds;
                batch_conf_nos_orig_mat(b,indConf) = length(inds);
            end
        end
    end
    
    batch_conf_nos_left_mat = calcExpectedBatchConfNos(confMat_Overall, batch_conf_nos_orig_mat);
    %batch_conf_nos_left_mat = batch_conf_nos_orig_mat;
    for c = 1:clusterCount
        %pick the samples of that cluster
        for irg = 1:categoryCount
            for jcp = 1:categoryCount
                indConf = sub2ind_conf(categoryCount, irg, jcp);
                sampleIDs = kl_conf_init_id_cell{c,indConf};
                for s = sampleIDs
                    [~, b] = max(batch_conf_nos_left_mat(:,indConf));
                    
                    kl_batch_final_id_cell{c,b} = [kl_batch_final_id_cell{c,b} s];
                    batch_conf_id_final_cell{b,indConf} = [batch_conf_id_final_cell{b,indConf} s];
                    
                    kl_batch_final_count_mat(c,b) = kl_batch_final_count_mat(c,b) + 1;
                    batch_conf_nos_left_mat(b,indConf) = batch_conf_nos_left_mat(b,indConf) - 1;
                    batch_conf_nos_final_mat(b,indConf) = batch_conf_nos_final_mat(b,indConf) + 1;
                    
                    %find the sampleID of current batch
                    samplesOfCurBatch = find(distribMatNew(:,1)==b);
                    if isempty(samplesOfCurBatch)
                        %this is the first sample to be put into this batch
                        newIndBatch=1;
                    else
                        %there are some other samples already in the batch
                        sampleIDsGivenAlready = sort(distribMatNew(samplesOfCurBatch,2));
                        assignableIDs = 1:length(sampleIDsGivenAlready)+1;
                        sampleIDsGivenAlready(sampleIDsGivenAlready>max(assignableIDs)) = [];
                        assignableIDs(sampleIDsGivenAlready) = [];
                        newIndBatch = min(assignableIDs);
                    end
                    
                    assert(distribMatNew(s,1)==0 && distribMatNew(s,2)==0, 'this sample is already assigned');
                    oldBatchID = distribMatNew(s,3);
                    wasInSameBatch = oldBatchID==b;
                    if wasInSameBatch
                        oldIndBatch = distribMatNew(s,4);
                        allreadyAssignedRow = find(distribMatNew(:,1)==b & distribMatNew(:,2)==oldIndBatch);
                        distribMatNew(s,[1 2]) = [b oldIndBatch];
                        if ~isempty(allreadyAssignedRow)
                            distribMatNew(allreadyAssignedRow,[1 2]) = [b newIndBatch];
                        end
                    else
                        distribMatNew(s,[1 2]) = [b newIndBatch];
                    end
                    batchMoveSummary_Rfr_Cto(oldBatchID, b) = batchMoveSummary_Rfr_Cto(oldBatchID, b) + 1;
                end
            end
        end    
    end
    disp('Old confusion mat_2_vec per batches')
    disptable(batch_conf_nos_orig_mat)
    
    disp('New confusion mat_2_vec per batches')
    disptable(batch_conf_nos_final_mat)
end

function batch_conf_nos_exp_mat = calcExpectedBatchConfNos(confMat_Overall, batch_conf_nos_orig_mat)
    [batchCount, colCount] = size(batch_conf_nos_orig_mat);
    batch_conf_nos_exp_mat = zeros(size(batch_conf_nos_orig_mat));
    sumAllVec = reshape(confMat_Overall,1,[]);
    
    floorVec = floor((1/batchCount)*sumAllVec);
    batch_conf_nos_exp_mat =  repmat(floorVec,batchCount,1);
    
    additionVec = sumAllVec - sum(batch_conf_nos_exp_mat);
    for i=1:colCount
        addVecCol = randperm(batchCount);
        numOfBatchesToIncrease = additionVec(i);
        addVecCol = addVecCol(1:numOfBatchesToIncrease);
        batch_conf_nos_exp_mat(addVecCol,i) = batch_conf_nos_exp_mat(addVecCol,i) + 1;
    end
    
    %the number of samples in batches should not change
    batchSampleCounts_Old = sum(batch_conf_nos_orig_mat,2);
    batchSampleCounts_New = sum(batch_conf_nos_exp_mat,2);
    batchSampleCounts_Dif = batchSampleCounts_New-batchSampleCounts_Old;
    sampleToMove_fr = find(batchSampleCounts_Dif>0,1);
    while ~isempty(sampleToMove_fr)
        sampleToMove_to = find(batchSampleCounts_Dif<0,1);
        %now move fr->to
        colToMove = randi(colCount, 1);
        batch_conf_nos_exp_mat(sampleToMove_fr,colToMove) = batch_conf_nos_exp_mat(sampleToMove_fr,colToMove) - 1;
        batch_conf_nos_exp_mat(sampleToMove_to,colToMove) = batch_conf_nos_exp_mat(sampleToMove_to,colToMove) + 1;
        batchSampleCounts_Dif(sampleToMove_fr) = batchSampleCounts_Dif(sampleToMove_fr) - 1 ;
        batchSampleCounts_Dif(sampleToMove_to) = batchSampleCounts_Dif(sampleToMove_to) + 1 ;
        sampleToMove_fr = find(batchSampleCounts_Dif>0,1);
    end
    
    batchSampleCounts_New = sum(batch_conf_nos_exp_mat,2);
    assert(sum(batchSampleCounts_New-batchSampleCounts_Old)==0,'these must be equal');

    
    sumAllVecNew = sum(batch_conf_nos_exp_mat);
    assert(sum(sumAllVecNew-sumAllVec)==0,'these must be equal');
end

function indConf = sub2ind_conf(categoryCount, irg, jcp)
    %irg = i, row, given
    %jcp = j, col, predicted
    indConf = sub2ind([categoryCount,categoryCount], irg, jcp);
end

function [confMat_Overall, precisionMat, recallMat] = performPrecisionRecallAnalysis(Y_lab, Y_pred, Y_clus)
    categoryVec = unique([Y_lab;Y_pred]);
    classCount = length(categoryVec);
    clusterCount = length(unique(Y_clus));
    
    confMat_Overall = confusionmat(Y_lab, Y_pred);
    
    precisionMat = zeros(clusterCount+1,classCount+1);
    recallMat = zeros(clusterCount+1,classCount+1);
    
    [precision_All, recall_All] = getClassStats_Prec(confMat_Overall, categoryVec);
    precisionMat(end,:) = [precision_All mean(precision_All)];
    recallMat(end,:) = [recall_All mean(recall_All)];
    
    for c = 1:clusterCount
        slct = Y_clus==c;
        [acc_c, confMat_c, ~] = calculateConfusion(Y_pred(slct), Y_lab(slct), false, classCount);
        [precision_c, recall_c] = getClassStats_Prec(confMat_c, categoryVec);
        precisionMat(c,:) = [precision_c acc_c];
        recallMat(c,:) = [recall_c acc_c];  
    end
end

function [precisionClass, recallClass] = getClassStats_Prec(confMat, categoryVec)
    confMatStats = calcConfusionStatistics(confMat,categoryVec);
    precisionClass = [confMatStats.Precision];
    precisionClass(isnan(precisionClass)) = 0;
    recallClass = [confMatStats.Sensitivity];
    recallClass(isnan(recallClass)) = 0;
end

