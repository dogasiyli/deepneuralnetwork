function [sampleIndsVec, initIndicesOfBlocks, cB] = plotDistanceMatrixAsData(D, d, labels, figVec, sampleIndsVec, printLevel)
    if ~exist('sampleIndsVec','var')
        sampleIndsVec = [];
    end
    if ~exist('printLevel','var')
        printLevel = -1;
    end
    [X, dataMode, sampleIndsVec, initIndicesOfBlocks] = generateDataFromDistMat(D, d, labels, sampleIndsVec);
    cB = getCenterBlocks(D, initIndicesOfBlocks);
    switch dataMode
        case 'samples'
            if (length(figVec)>=1 && figVec(1)>0)
                plotClass(X, labels, figVec);
            end
        case 'uppertriangle'
            minX = min(X(:));
            X(1==tril(ones(size(X)))) = inf;
            [distVals_sorted, inds_sorted] = sort(X(:));
            [im_i,im_j] = ind2sub(size(X),inds_sorted(1));
            im_i = sampleIndsVec(im_i);
            im_j = sampleIndsVec(im_j);
            stringToDisp = ['Min distance(' num2str(distVals_sorted(1)) ') is between ' num2str(im_i) ' and ' num2str(im_j) '.'];
            try
                if (isempty(figVec))
                    return
                elseif (length(figVec)==1)
                    figure(figVec);clf;
                elseif (length(figVec)==3)
                    h = subplot(figVec(1),figVec(2),figVec(3));cla(h);
                elseif (length(figVec)==4)
                    figure(figVec(1));
                    h = subplot(figVec(2),figVec(3),figVec(4));cla(h);
                end
                X(1==tril(ones(size(X)))) = minX;
                imagesc(X);colorbar;
                title(stringToDisp);              
            catch
                if printLevel>2
                    disp(stringToDisp);
                end
            end
    end
end

function cB = getCenterBlocks(D, initIndicesOfBlocks)
    k = length(initIndicesOfBlocks);
    cB = zeros(k,3);
    for i=1:k
        im_i_k = initIndicesOfBlocks(i);
        D(im_i_k,im_i_k) = inf;
        [mn_v,mn_i] = min(D(im_i_k,:));
        cB(i,:) = [sort([im_i_k mn_i]) mn_v];
    end
    cB = sortrows(cB, 3);
end