function h = plotFilterStatsTrackAnalyse(filterBehaviourCodeVec, M_f_b_bt, filterBehaviourCurMat, figID, colorOfFilters, updatedSubTitle)
    %filterBehaviours = {'alwaysIncreasing';'seldomDecrease';'peaky';'constantDrop';'vanishing'};
    f_filterID = filterBehaviourCodeVec(1);
    f_filterBehaviourCode = filterBehaviourCodeVec(2);
    f_firstDrop = filterBehaviourCodeVec(3);
    f_constantDrop = filterBehaviourCodeVec(4);
    f_belowZero = filterBehaviourCodeVec(5);
        
    m_filterBehaviourCode = filterBehaviourCurMat(2);
    m_firstDrop = filterBehaviourCurMat(3);
    m_constantDrop = filterBehaviourCurMat(4);
    m_belowZero = filterBehaviourCurMat(5);
    
    [M_b_bt, maxCol] = getImportantParts(M_f_b_bt);
    x_perturb = 0.6*((f_filterID-1)/49)-0.3;
    x_perturb_init = -0.4 + 0.09*((f_filterID-1)/49)-0.045;
    x_perturb_end = +0.4 + 0.09*((f_filterID-1)/49)-0.045;

    h=figure(figID);hold on;
    RF = 3; CF = 2;
    %draw M_f_b_bt
    switch f_filterBehaviourCode
        case 1
            %'alwaysIncreasing';
            subplot(RF,CF,1);hold on;
            plot(M_b_bt(:,1),maxCol,'Color',colorOfFilters(f_filterID,:));
            xlabel('epochID');
            ylabel('maxValue');
        case {2,3}
            %'seldomDecrease';-2
            %'peaky';-3
            subplot(RF,CF,f_filterBehaviourCode);hold on;
            plot(M_b_bt(:,1),maxCol,'Color',colorOfFilters(f_filterID,:));
            if ~isnan(m_firstDrop)
                %just a red star to current first drop position
                X = M_b_bt(m_firstDrop,1)+x_perturb;
                Y = maxCol(m_firstDrop);
                plot(X,Y,'r*');
            end
            if ~isnan(f_firstDrop)
                %a line on the X==f_firstDrop line
                X = ones(1,2)*(M_b_bt(f_firstDrop,1)+x_perturb);
                Y = [0 , maxCol(f_firstDrop)];
                plot(X,Y,'Color',colorOfFilters(f_filterID,:));
            end
            xlabel('epochID-firstDrop of filter projected on epochID');
            ylabel({'maxValue';'firstDrop of lines are red stars'});
        case 4
            %'constantDrop';
            subplot(RF,CF,4);hold on;
            plot(M_b_bt(:,1),maxCol,'Color',colorOfFilters(f_filterID,:));
            if ~isnan(m_constantDrop)
                %just a red star to current first drop position
                X = M_b_bt(m_constantDrop,1)+x_perturb;
                Y = maxCol(m_constantDrop);
                plot(X,Y,'ro','MarkerSize',20);
                
                current_y_plotlimits = ylim;
                %plot the line for beginning of x_perturb area
                X = ones(1,2)*(M_b_bt(m_constantDrop,1)+x_perturb_init);
                plot(X,current_y_plotlimits,'Color',colorOfFilters(f_filterID,:));
                
                %plot the line for end of x_perturb area
                X = ones(1,2)*(M_b_bt(m_constantDrop,1)+x_perturb_end);
                plot(X,current_y_plotlimits,'Color',colorOfFilters(f_filterID,:));
            end
            if ~isnan(f_constantDrop)
                %a line on the X==f_firstDrop line
                X = ones(1,2)*(M_b_bt(f_constantDrop,1)+x_perturb);
                Y = [0 , maxCol(f_constantDrop)];
                plot(X,Y,'Color',colorOfFilters(f_filterID,:));
            end
            xlabel('epochID-constantDrop start point of filter projected on epochID');
            ylabel({'maxValue';'constantDrop start point of lines are red circles'});
        case 5
            %'vanishing';
            subplot(RF,CF,5:6);hold on;
            plot(M_b_bt(:,1),maxCol,'Color',colorOfFilters(f_filterID,:));
            if ~isnan(m_belowZero)
                %just a red star to current first drop position
                X = M_b_bt(m_belowZero,1)+x_perturb;
                Y = maxCol(m_belowZero);
                plot(X,Y,'ro','MarkerSize',20);
                
                current_y_plotlimits = ylim;
                %plot the line for beginning of x_perturb area
                X = ones(1,2)*(M_b_bt(m_belowZero,1)+x_perturb_init);
                plot(X,current_y_plotlimits,'Color',colorOfFilters(f_filterID,:));
                
                %plot the line for end of x_perturb area
                X = ones(1,2)*(M_b_bt(m_belowZero,1)+x_perturb_end);
                plot(X,current_y_plotlimits,'Color',colorOfFilters(f_filterID,:));
            end
            if ~isnan(f_belowZero)
                %a line on the X==f_firstDrop line
                X = ones(1,2)*(M_b_bt(f_belowZero,1)+x_perturb);
                Y = [min(M_b_bt(:,6)) , maxCol(f_belowZero)];
                plot(X,Y,'Color',colorOfFilters(f_filterID,:));
            end
            xlabel('epochID-belowZero start point of filter projected on epochID');
            ylabel({'maxValue';'belowZero start point of lines are red circles'});
    end
    title(updatedSubTitle);        
end

function [M_b_bt, maxCol, movingDif] = getImportantParts(M_b_bt)
    %sort according to epochID(col=1)
    M_b_bt = sortrows(M_b_bt,1);
    %remove additional 1's at the beginning
    initRow = find(M_b_bt(:,1)==1,1,'last');
    M_b_bt = M_b_bt(initRow:end,:);
    
    maxCol = M_b_bt(:,8);
    movingDif = [0;maxCol(2:end)-maxCol(1:end-1)];
end