function plot_2Class_NCluster(X, labelsGiven, labelsPred, labelCrossMat)
    categoryCountNew = max(labelCrossMat(:,2));
    newCategoryColors = rand(categoryCountNew,3);
    for newCategoryID = 1:categoryCountNew
        cP = labelCrossMat(newCategoryID,1);
        cG = labelCrossMat(newCategoryID,3);
        slct = labelsGiven==cG & labelsPred==cP;
        sampleCnt = sum(slct==1);
        
        originalPredictionTF = cP==cG;
        sourceLabel = labelCrossMat(newCategoryID,end);
        if sampleCnt>0
            %[sourceLabel-1-RedComponent=1]-[sourceLabel-2-GreenComponent1]
            %newCategoryColors(newCategoryID,sourceLabel) = 1;
            MarkerFaceColor_c = newCategoryColors(newCategoryID,:);
            %[sourceLabel-1-o]-[sourceLabel-2-^]
            %[cG==cP-green]-[cG~=cP-red]
            if     originalPredictionTF && sourceLabel == 1
                %Predicted true, original class=1, cluster=newCategoryID
                scatterModeStr = 'o';%same=C1
                MarkerEdgeColor_c = [0 1 0];
                lw = 2;
            elseif originalPredictionTF && sourceLabel == 2
                %Predicted true, original class=2, cluster=newCategoryID
                scatterModeStr = '^';%dif=C2
                MarkerEdgeColor_c = [0 1 0];
                lw = 2;
            elseif ~originalPredictionTF && sourceLabel == 1
                %Predicted false, original class=1, cluster=newCategoryID
                scatterModeStr = 'o';%same=C1
                MarkerEdgeColor_c = [1 0 0];
                lw = 2;
            elseif ~originalPredictionTF && sourceLabel == 2
                %Predicted false, original class=2, cluster=newCategoryID
                scatterModeStr = '^';%dif=C2
                MarkerEdgeColor_c = [1 0 0];
                lw = 2;
            end
            scatter(X(1,slct),X(2,slct),scatterModeStr,'MarkerFaceColor',MarkerFaceColor_c,'MarkerEdgeColor',MarkerEdgeColor_c,'LineWidth',lw);
        end
    end
end