function  printCellMat( cellMat )
    try
        [r,c] = size(cellMat);
        maxStrSizePerCol = zeros(1,c);
        for i_r = 1:r
            for j_c = 1:c
                try
                    curLen = getStrSize(cellMat{i_r,j_c});
                    maxStrSizePerCol(1,j_c) = max([maxStrSizePerCol(1,j_c) curLen]);
                catch
                    %do nothing
                end
            end
        end
 
        for i_r = 1:r
            for j_c = 1:c
                curCell = cellMat{i_r,j_c};
                dispStrIdentifier = ['%-' num2str(3+maxStrSizePerCol(1,j_c)) 's'];
                try
                    if ischar(curCell)
                        strToPrint = curCell;
                    elseif isstring(curCell)
                        strToPrint = curCell;
                    elseif isinteger(curCell)
                        strToPrint = num2str(curCell,'%0d');
                    elseif isfloat(curCell)
                        strToPrint = num2str(curCell,'%4.2f');
                    else
                        strToPrint = '-';
                    end
                    
                catch err
                    strToPrint = 'x';
                    %do nothing
                end
                strToPrint = sprintf(dispStrIdentifier,strToPrint);
                strToPrint = strrep(strToPrint,' ','.');
                fprintf(dispStrIdentifier,strToPrint);
            end
            disp(' ');
        end  
    catch err
        disp(['Unexpected error : ' err.message]);
    end    
end

function lenOfObj = getStrSize(obj)
    lenOfObj = 0;
    if ischar(obj)
        lenOfObj = length(obj);
    elseif isstring(obj)
        lenOfObj = strlen(obj);
    elseif isnumeric(obj)
        lenOfObj = length(obj);
    end
end

