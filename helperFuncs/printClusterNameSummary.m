function printClusterNameSummary(srcFold, signID)
    try
        clusterNames_single = getClusterNamesAssignedFromTxt(srcFold, struct('signIDList',signID,'single0_double1',0));
        clusterNames_both = getClusterNamesAssignedFromTxt(srcFold, struct('signIDList',signID,'single0_double1',1));
        
        %first column is letters a to ?
        maxLabelCnt = 1 + max([length(clusterNames_single) length(clusterNames_both)]);
        
        cellMatToPrint = cell(maxLabelCnt,3);
        
        cellMatToPrint{1,1} = 'labelID';
        cellMatToPrint{1,2} = 'singleHand';
        cellMatToPrint{1,3} = 'bothHand';
        
        for i=1:length(clusterNames_single)
            cellMatToPrint{i+1,1} = char(i + 'a' - 1);
            cellMatToPrint{i+1,2} = clusterNames_single{i};
        end
        for i=1:length(clusterNames_both)
            cellMatToPrint{i+1,1} = char(i + 'a' - 1);
            cellMatToPrint{i+1,3} = clusterNames_both{i};
        end
    catch err
        disp(['Unexpected error : ' err.message]);
    end  
    printCellMat( cellMatToPrint );
end

