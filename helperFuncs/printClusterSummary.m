function [ clusterCellString ] = printClusterSummary( labelsFinal )
%printClusterSummary According to the labels print a cluster summary of
%which indices fall into whic label
    [clusterCellString, labelCount] = getClusterSummaryAsString(labelsFinal);
    printFormattedClusterSummary( clusterCellString, labelCount);
end

function printFormattedClusterSummary( clusterCellString, labelCount)
    maxCharCnt = zeros(1,3);
    for i=1:labelCount
        maxCharCnt(2) = max(maxCharCnt(2), length(clusterCellString{i,1}));
        maxCharCnt(3) = max(maxCharCnt(3), length(clusterCellString{i,2}));
    end
    %disptable(clusterCellString, 'labelID|memberIdx');
    titleCells = {'s.nu', 'labelID', 'memberIdx'};
    maxCharCnt(1) = max(maxCharCnt(1), length(titleCells{1,1}));
    maxCharCnt(2) = max(maxCharCnt(2), length(titleCells{1,2}));
    maxCharCnt(3) = max(maxCharCnt(3), length(titleCells{1,3}));
    formatSpecStr = ['%-' num2str(maxCharCnt(1)+1) 's %-' num2str(maxCharCnt(2)+1) 's %-' num2str(maxCharCnt(3)+1) 's \n'];
    fprintf(formatSpecStr, titleCells{1}, titleCells{2}, titleCells{3});
    for i=1:labelCount
        fprintf(formatSpecStr, num2str(i), clusterCellString{i,1}, clusterCellString{i,2});
    end
end