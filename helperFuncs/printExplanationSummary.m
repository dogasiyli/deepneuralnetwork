function printExplanationSummary(movedFiles)
    %6 columns - take last 3 columns
    uniqResults = movedFiles(:,4:6);
    uniqResults = uniqResults(:);
    uniqResultsIDs = reshape(unique(uniqResults),1,[]);
    for i=uniqResultsIDs
        numOfOccurances = sum(uniqResults==i);
        switch i
            case -100
                disp(['-100:restored(' num2str(numOfOccurances) ')'])
            case 0
                disp(['0-skipped(' num2str(numOfOccurances) ')'])
            case 48
                disp(['48-newerBackup(' num2str(numOfOccurances) ')'])
            case 49
                disp(['49-bckpRestored(' num2str(numOfOccurances) ')'])
            case 50
                disp(['50-sameSrcBckp(' num2str(numOfOccurances) ')'])
            case 51
                disp(['51-srcBackedUp(' num2str(numOfOccurances) ')'])
            case 52
                disp(['52-newerSource(' num2str(numOfOccurances) ')'])
            case 100
                disp(['100-backedup(' num2str(numOfOccurances) ')'])
            otherwise
                disp(['unknownResult(' num2str(i) ')'])
        end
    end
end