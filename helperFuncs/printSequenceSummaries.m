function printSequenceSummaries( surList, labelSwitches_uniq)
    for h=1:3
        disp('********');
        switch h
            case 1
                disp('Left hand summary..')
            case 2
                disp('Rigth hand summary..')
            case 3
                disp('Both hand summary..')
        end   
        summarizeLabelSequenceGroups(surList, labelSwitches_uniq(h,:), h);
    end
end

