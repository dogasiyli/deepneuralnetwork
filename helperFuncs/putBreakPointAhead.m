function putBreakPointAhead( lineCount, stop_clear_str, stackInds)
    if ~exist('stop_clear_str','var') || isempty(stop_clear_str)
        stop_clear_str = 'dbstop';
    end
    if ~exist('stackInds','var') || isempty(stackInds)
        stackInds = [3 3];
    end
    stackInfo = getDBStackInfo(stackInds);
    eval([stop_clear_str ' in ' stackInfo.fileName ' at ' num2str(stackInfo.lineNr + lineCount) ';']);
end

