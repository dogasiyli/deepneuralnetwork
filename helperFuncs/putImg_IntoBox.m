function imgRet = putImg_IntoBox(img, boxSize, uint2RGB01, surroundWithColorStruct)
    %get the original size of the image
    if exist('surroundWithColorStruct','var') && ~isempty(surroundWithColorStruct) && isstruct(surroundWithColorStruct)
        %the box color and percentage of the thickness is in the struct
        thicknessParam = surroundWithColorStruct.thicknessParam;
        backgroundColor = surroundWithColorStruct.colorParam;
    else
        thicknessParam = 0;
        backgroundColor = [0 0 0];
    end    
    [rowCnt, colCnt, chnCnt] = size(img);
    %get the box size to put this image into
    rowBox = boxSize(1);
    colBox = boxSize(2);
    
    rowDif = rowBox - rowCnt;
    colDif = colBox - colCnt;
    %if the image can not fit into the box then resize it to fit in
    if min(rowBox-rowCnt,colBox-colCnt)<0
        imgRet = resizeImg_IntoBox(img, boxSize, true);
        if thicknessParam==0
            return
        else
            %I need to create a box with backgroundColor
            %scale the return image and put it in
            imgRet2 = putImg_IntoBox(imresize(imgRet,1-thicknessParam), boxSize, uint2RGB01, surroundWithColorStruct);
            imgRet = imgRet2;
            return;
        end
    end
    
    imgRet = zeros([rowBox colBox chnCnt]);
    rFr = floor(rowDif/2)+1;
    rTo = rFr + size(img,1) - 1; 
    cFr = floor(colDif/2)+1;
    cTo = cFr + size(img,2) - 1;
    if chnCnt>1
        imgRet(rFr:rTo,cFr:cTo,:) = img;
    else
        imgRet(rFr:rTo,cFr:cTo) = img;
    end
    %make01RGB
    if max(imgRet(:))>1 && uint2RGB01
        if max(imgRet(:))>200
           imgRet(:) = imgRet(:)./255;
        end
    end
    if uint2RGB01
        imgRet = map2_a_b(imgRet, 0, 1);
    end
    if chnCnt>1
        for c=1:chnCnt
            imgRet(1:rFr,:,c) = backgroundColor(c);
            imgRet(rTo+1:end,:,c) = backgroundColor(c);
            imgRet(:,1:cFr,c) = backgroundColor(c);
            imgRet(:,cTo+1:end,c) = backgroundColor(c);
        end
    else
        imgRet(1:rFr,:) = backgroundColor(1);
        imgRet(rTo+1:end,:) = backgroundColor(1);
        imgRet(:,1:cFr) = backgroundColor(1);
        imgRet(:,cTo+1:end) = backgroundColor(1);
    end
end