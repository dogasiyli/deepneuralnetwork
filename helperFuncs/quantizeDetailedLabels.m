function listItems = quantizeDetailedLabels(detailedLabels)
    listItems = detailedLabels(:,2)*10^8 + detailedLabels(:,3)*10^7 + detailedLabels(:,4)*10^6 + detailedLabels(:,1)*10^3 + detailedLabels(:,5);
end

