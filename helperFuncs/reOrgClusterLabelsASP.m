function [clustersStructReOrg, convTable] = reOrgClusterLabelsASP(signSourceFolder, signID, clustersStruct, figIDBegin)
%reOrgClusterLabelsASP when matlab tries to cluster frames it can change
%the labels given - this will reorganize the given labels to the text file
%associated

%1. load the text file into cell
    clustersASPInfoStruct = getClusterInfoFromASPTxt(signSourceFolder, signID);

%2. clusterID = getHandBasedClusterID(clustersStruct, clusterID);
%first check bothHands Labels
%then check singleHand Labels

    bothHandsIDs = find(not(cellfun('isempty', (strfind(clustersStruct.definition.clusterTypes, 'Both')))));
    singleHandsIDs = find(not(cellfun('isempty', (strfind(clustersStruct.definition.clusterTypes, 'Single')))));
    
    convTable_both = getConverSionTable(clustersStruct, lower({clustersASPInfoStruct.both.clusterName}), bothHandsIDs);
    convTable_single = getConverSionTable(clustersStruct, lower({clustersASPInfoStruct.single.clusterName}), singleHandsIDs);
    
    convTable = [convTable_both;convTable_single];
    convTable = sortrows(convTable, 1);
    
    oldLabels = convTable(:,1)';
    newLabels = convTable(:,4)';
    
    clustersStructReOrg = clustersStruct;
    clustersStructReOrg.dataset.assignments(:,2) = reSetLabels(clustersStructReOrg.dataset.assignments(:,2), oldLabels, newLabels); 
    clustersStructReOrg = sortClusters(clustersStructReOrg);
    for i=1:length(oldLabels)
        row_i = oldLabels(i);
        row_j = newLabels(i);
        clustersStructReOrg.definition.clusterNames{row_j,:} = clustersStruct.definition.clusterNames{row_i,:};
        clustersStructReOrg.definition.clusterImage{row_j,:} = clustersStruct.definition.clusterImage{row_i,:};
        clustersStructReOrg.definition.subClusterImages(row_j,:) = clustersStruct.definition.subClusterImages(row_i,:);
        clustersStructReOrg.definition.maxDistList(row_j,:) = clustersStruct.definition.maxDistList(row_i,:);
        clustersStructReOrg.definition.uniqueSignIDs(row_j,:) = clustersStruct.definition.uniqueSignIDs(row_i,:);
    end
    clustersStructReOrg.model.clusterDefinitionMat(:,1) = reSetLabels(clustersStructReOrg.model.clusterDefinitionMat(:,1), oldLabels, newLabels);     
    clustersStructReOrg.model.clusterDefinitionMat = sortrows(clustersStructReOrg.model.clusterDefinitionMat,[1 2]);
    
    displayClusterStructInfo(clustersStruct, [], figIDBegin);
    displayClusterStructInfo(clustersStructReOrg, [], figIDBegin+1);
end

function convTable = getConverSionTable(clustersStruct, aspClustNamesLower, sbHandsIDs)
    clusterCount_Single = length(sbHandsIDs);
    convTable = NaN(clusterCount_Single, 4);
    oldVec_Single = 1:clusterCount_Single;
    for i = oldVec_Single
        oldClustVec_i = sbHandsIDs(i);
        convTable(i,[1 2]) = [oldClustVec_i i];
        clustNameAt_old_i = clustersStruct.definition.clusterNames{oldClustVec_i,1};
        whereInTextFile = find(not(cellfun('isempty', (strfind(aspClustNamesLower, clustNameAt_old_i)))), 1);
        assert(~isempty(whereInTextFile),'couldnt find the cluster');
        newClustVec_i = sbHandsIDs(whereInTextFile);
        convTable(i,[3 4]) = [whereInTextFile newClustVec_i];
    end
end

