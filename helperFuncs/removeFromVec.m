function v = removeFromVec(v, idsToRemove)
    %toRem = find(ismember(v,idsToRemove));
    v(ismember(v,idsToRemove)) = [];
end