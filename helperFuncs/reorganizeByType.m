function [dS_New, cnnnet_new] = reorganizeByType(dS, cnnnet, displayOpts, saveOpts, dataTypeStr, finalGroupType, finalTidyType)
    global cifar10Folder;
    dS = dS.updateDataStruct(dS, {'type', dataTypeStr});
    [cnnnet_new, ~, ~, pred_labels_01] = deepNet_Test( dS, cnnnet, displayOpts, saveOpts, dataTypeStr, '', 1);
    batchCountOld = eval(['dS.countBatch.' dataTypeStr ';']);
    
    [distribMatNew, ~] = planDataShuffle(pred_labels_01, 'adaboost');
    [distribMatNew, ~] = distribMatNewClean( distribMatNew );
    %
    moveSummaryMat = shuffleData(dS, distribMatNew); %#ok<NASGU>
    shuffleResultAcc = analyzeAccAfterShuffle( distribMatNew );
    disp(['---Shuffle result accuracies(' dataTypeStr ') : ']);
    disptable(shuffleResultAcc);
    
    dS_New = dS;
    if ~strcmp(finalGroupType,'all')
        %we will definitely remove the center true group
        
        %Now there are N1 batches that have false samples in it
        %Remaining batches are filled with truly recognized batches
        %we will grab the first N1 and the last N1 and then uniformly sample
        %them to create new batches

        batchIDs_wfs = find(shuffleResultAcc(1:end-1,end)<1);%With False Samples
        batchIDs_remove = 1:batchCountOld;
        if strcmp(finalGroupType,'moreTrueSamples')
            batchIDs_fnf = shuffleResultAcc((end-length(batchIDs_wfs)):(end-1),1);%Final No False
            batchIDs_remove([batchIDs_wfs;batchIDs_fnf]) = [];
            batchIDs_fnf_new = batchIDs_fnf - min(batchIDs_fnf) + max(batchIDs_wfs) + 1;
        elseif strcmp(finalGroupType,'onlyFalse')
            batchIDs_fnf = [];
            batchIDs_remove(batchIDs_wfs) = [];
        end

        disp(['---BattchIDs with false samples (' mat2str(batchIDs_wfs) ') : ']);
        disp(['---BattchIDs to remove (' mat2str(batchIDs_remove) ') : ']);
        %now remove batchID files in "batchIDs_remove"
        %rename the batchIDs_ata to batchIDs_ata_new one by one
        %change some dS_New
        dS_New = dS;
        disp(['+++Remove batchIDs -> ' mat2str(batchIDs_remove) ' : ']);
        for b = batchIDs_remove
            if ~isfield(dS_New, 'isDefault')
                dS_New = dS_New.updateDataStruct( dS_New, {'batchID_current', b});
                %if displayOpts.fileOperations
                    disp([displayOpts.ies{4} 'Removing ' dS_New.fileName_current ''])
                %end
                delete(dS_New.fileName_current);
            end
        end
        if strcmp(finalGroupType,'moreTrueSamples') && sum(abs(batchIDs_fnf-batchIDs_fnf_new))>0
            disp(['+++Rename batchIDs from ' mat2str(batchIDs_fnf) ' -> to ' mat2str(batchIDs_fnf_new) ]);
            for bi = 1:length(batchIDs_wfs)
                if ~isfield(dS_New, 'isDefault')
                    b_id_old = batchIDs_fnf(bi);
                    b_id_new = batchIDs_fnf_new(bi);
                    dS_New = dS_New.updateDataStruct( dS_New, {'batchID_current', b_id_old});
                    bfn_old = dS_New.fileName_current;
                    dS_New = dS_New.updateDataStruct( dS_New, {'batchID_current', b_id_new});
                    bfn_new = dS_New.fileName_current;
                    if ~strcmp(bfn_old,bfn_new)
                        disp([displayOpts.ies{4} 'Changing <' bfn_old '> to <' bfn_new '>'])
                        movefile(bfn_old,bfn_new);
                    else
                        disp([displayOpts.ies{4} 'Remains same <' bfn_old '>'])
                    end
                end
            end
        end
        selected_batch_IDs = sort([batchIDs_wfs;batchIDs_fnf])';
        batchCountNew = length(selected_batch_IDs);
        disp('---Reorganize dS_New.batchInfo');
        dataTypeStr_Be = dataTypeStr;
        dataTypeStr_Be(1)=upper(dataTypeStr_Be(1));
        eval(['dS_New.countBatch.' dataTypeStr ' = batchCountNew;']);%dS_New.countBatch.train = length(selected_batch_IDs);
        eval(['dS_New.batchInfo.batchSizesMB_' dataTypeStr_Be ' = dS_New.batchInfo.batchSizesMB_' dataTypeStr_Be '(selected_batch_IDs);']);%dS_New.batchInfo.batchSizesMB_Train = dS_New.batchInfo.batchSizesMB_Train(selected_batch_IDs);
        eval(['dS_New.batchInfo.image_Count_Per_Batch_' dataTypeStr_Be ' = dS_New.batchInfo.image_Count_Per_Batch_' dataTypeStr_Be '(selected_batch_IDs,:);']);%dS_New.batchInfo.image_Count_Per_Batch_Train = dS_New.batchInfo.image_Count_Per_Batch_Train(selected_batch_IDs,:);
        eval(['dS_New.batchInfo.image_Count_Matrix_' dataTypeStr_Be ' = sum(dS_New.batchInfo.image_Count_Per_Batch_' dataTypeStr_Be ');']);%dS_New.batchInfo.image_Count_Matrix_Train = sum(dS_New.batchInfo.image_Count_Per_Batch_Train);
        eval(['dS_New.batchInfo.batch_Count_' dataTypeStr_Be ' = batchCountNew;']);%dS_New.batchInfo.batch_Count_Train = batchCountNew;
        if isfield(dS_New.batchInfo,'batchDistributions')
            dS_New.batchInfo.batchDistributions_old = dS_New.batchInfo.batchDistributions;
            dS_New.batchInfo = rmfield(dS_New.batchInfo,'batchDistributions');
        end
        dS_New.batchCnt_current = length(selected_batch_IDs);

        disp('+++Rename created filenames for new batchCount');    
        for bi = 1:batchCountNew
            if ~isfield(dS_New, 'isDefault')
                dS_New = dS_New.updateDataStruct( dS_New, {'batchID_current', bi});
                bfn_new = dS_New.fileName_current;
                dS = dS.updateDataStruct( dS, {'batchID_current', bi});
                bfn_old = dS.fileName_current;
                if ~strcmp(bfn_old,bfn_new)
                    disp([displayOpts.ies{4} 'Changing <' bfn_old '> to <' bfn_new '>'])
                    movefile(bfn_old,bfn_new);
                else
                    disp([displayOpts.ies{4} 'Remains same <' bfn_old '>'])
                end
            end
        end

        batchInfo = dS_New.batchInfo; %#ok<NASGU>
        save([cifar10Folder 'batchInfo_CIFAR10.mat'],'batchInfo');

    end
    [cnnnet_new, ~, ~, pred_labels_new] = deepNet_Test( dS_New, cnnnet_new, displayOpts, saveOpts, dataTypeStr, '',2);
    if strcmp(finalTidyType,'none')
        return
    elseif strcmp(finalTidyType,'single') && batchCountNew>1
        dataAll = [];
        labelsAll = [];
        dataCntVec = zeros(batchCountNew,1);
        for bi = 1:batchCountNew
            dS_New = dS_New.updateDataStruct( dS_New, {'batchID_current', bi});
            bfn_new = dS_New.fileName_current;
            load(bfn_new,'data','labels');
            if bi==1
                dataSize = size(data);
            end
            dataCntVec(bi) = size(data,4);
            data = reshape(data,[],dataCntVec(bi));
            %if displayOpts.fileOperations
                disp([displayOpts.ies{4} 'Adding <' bfn_new '> to the dataAll'])
                disp([displayOpts.ies{4} 'Removing ' dS_New.fileName_current ''])
            %end
            delete(dS_New.fileName_current);
            dataAll = [dataAll , data];
            labelsAll = [labelsAll , labels];
        end
        data = reshape(dataAll,[dataSize(1:3) sum(dataCntVec)]);
        labels = labelsAll;
        clear dataAll labelsAll
        
        batchCountNew = 1; %#ok<NASGU>
        eval(['dS_New.countBatch.' dataTypeStr ' = batchCountNew;']);
        eval(['dS_New.batchInfo.batchSizesMB_' dataTypeStr_Be ' = sum(dS_New.batchInfo.batchSizesMB_' dataTypeStr_Be ');']);%dS_New.batchInfo.batchSizesMB_Train = sum(dS_New.batchInfo.batchSizesMB_Train);
        eval(['dS_New.batchInfo.image_Count_Per_Batch_' dataTypeStr_Be ' = sum(dS_New.batchInfo.image_Count_Per_Batch_' dataTypeStr_Be ');']);%dS_New.batchInfo.image_Count_Per_Batch_Train = sum(dS_New.batchInfo.image_Count_Per_Batch_Train);
        eval(['dS_New.batchInfo.image_Count_Matrix_' dataTypeStr_Be ' = sum(dS_New.batchInfo.image_Count_Per_Batch_' dataTypeStr_Be ');']);%dS_New.batchInfo.image_Count_Matrix_Train = sum(dS_New.batchInfo.image_Count_Per_Batch_Train);
        eval(['dS_New.batchInfo.batch_Count_' dataTypeStr_Be ' = batchCountNew;']);%dS_New.batchInfo.batch_Count_Train = batchCountNew;
        dS_New.batchCnt_current = 1;
        
        batchInfo = dS_New.batchInfo; %#ok<NASGU>
        save([cifar10Folder 'batchInfo_CIFAR10.mat'],'batchInfo');
        
        %save the first file with new name
        dS_New = dS_New.updateDataStruct( dS_New, {'batchID_current', 1});
        bfn_new = dS_New.fileName_current;
        %if displayOpts.fileOperations
            disp([displayOpts.ies{4} 'Saving ' dS_New.fileName_current ''])
        %end
        save(bfn_new,'data','labels','-v7.3');
    elseif strcmp(finalTidyType,'uniform')
        distribMatNew_02 = planDataShuffle(pred_labels_new, 'uniform');
        distribMatNew_02 = distribMatNewClean(distribMatNew_02);
        moveSummaryMat_new = shuffleData(dS_New, distribMatNew_02); %#ok<NASGU>
        shuffleResultAcc_new = analyzeAccAfterShuffle(distribMatNew_02);
        disp('Shuffle result accuracies : ');
        disptable(shuffleResultAcc_new);
    end
    deepNet_Test( dS_New, cnnnet_new, displayOpts, saveOpts, dataTypeStr, '',3);
end