function imgRet = resizeImg_IntoBox(img, boxSize, keepAspectRatio)

    [rowCnt, colCnt, chnCnt] = size(img);
    rowBox = boxSize(1);
    colBox = boxSize(2);
    rowDif = -1;colDif=-1;

    imgRet = zeros([rowBox colBox chnCnt]);
    %resize the image to put into the box
    %keep the aspect ratio?
    if keepAspectRatio
        scaleRatio = min(rowBox/rowCnt,colBox/colCnt);
        while (rowDif<0 || colDif<0)
            imgNew = imresize(img, scaleRatio);
            rowDif = rowBox - size(imgNew,1);
            colDif = colBox - size(imgNew,2);
            if (rowDif<0 || colDif<0)
                scaleRatio = scaleRatio*0.99;
            end
        end
        rFr = floor(rowDif/2)+1;
        rTo = rFr + size(imgNew,1) - 1; 
        cFr = floor(colDif/2)+1;
        cTo = cFr + size(imgNew,2) - 1;
        if chnCnt>1
            imgRet(rFr:rTo,cFr:cTo,:) = imgNew;
        else
            imgRet(rFr:rTo,cFr:cTo) = imgNew;
        end
    else
        imgRet = imresize(img, boxSize);
    end
    if max(imgRet(:))>1
        if max(imgRet(:))>200
           imgRet(:) = imgRet(:)./max(imgRet(:));
        end
    end
    imgRet = map2_a_b(imgRet, 0, 1);
end

function fitTypeID = box_a_fitin_b(rowCnt,colCnt,rowBox,colBox)
    if (rowCnt>rowBox) && (colCnt>colBox)
        %image is bigger than box in both row and col direction
        fitTypeID = 'a_Bigger_b';
    elseif (rowCnt>rowBox) && (colCnt<=colBox)
        %image is bigger than box in row direction
        fitTypeID = 'aRow_Bigger_b';
    elseif (rowCnt<=rowBox) && (colCnt>colBox)
        %image is bigger than box in col direction
        fitTypeID = 'aCol_Bigger_b';
    elseif (rowCnt<=rowBox) && (colCnt<=colBox)
        %image can fit into the box
        fitTypeID = 'a_Fits_b';
    end
end