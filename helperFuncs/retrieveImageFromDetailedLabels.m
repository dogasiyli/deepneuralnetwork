function im = retrieveImageFromDetailedLabels(srcFold, im_k, detailedLabels)
    lh1_rh2_bh3_str = {'LH','RH','BH'};    
    dLab_k = detailedLabels(im_k,:);%detailedLabels [signID userID repID lh1_rh2_bh3 imageIDInVideo]
    usrFold_k = ['User_' num2str(dLab_k(2)) '_' num2str(dLab_k(3))];
    frameID = dLab_k(5);
    fName_k = [lh1_rh2_bh3_str{dLab_k(4)} '_' num2str(frameID,'%03d') ];
    try
        croppedFolderName = [srcFold filesep num2str(dLab_k(1)) filesep usrFold_k filesep 'cropped'];
        if ~exist(croppedFolderName,'dir')
            if exist([croppedFolderName '.zip'],'file')
                rootFolder = [srcFold filesep num2str(dLab_k(1)) filesep usrFold_k];
                frameCnt = unzipProperly(rootFolder, 'cropped');
            else
                error(['Folder(' croppedFolderName ') doesnt exist neither the zip file']);
            end
        else
            [~, frameCnt] = getFileList( [croppedFolderName filesep], '.png'); 
        end
        frameCnt = round(frameCnt/3);
        if frameID==frameCnt+1
            fName_k_new = strrep(fName_k, num2str(frameID), num2str(frameID-1));
            im = imread([croppedFolderName filesep 'crop_' fName_k_new '.png']); 
            disp(['(' fName_k_new ') loaded instead of (' fName_k ') from ' usrFold_k]);
        else
            im = imread([croppedFolderName filesep 'crop_' fName_k '.png']);
        end        
    catch
        disp(['error in file ' usrFold_k filesep 'cropped' filesep 'crop_' fName_k '.png']);
        im = [];
    end
end