function saved = saveBackupIfExist(fileName, dispInfo)
    saved = 0;
    if ~exist('dispInfo','var')
        dispInfo = false;
    end
    fInfo = dir(fileName);
    try
        folderName = fInfo.folder;
    catch
        folderName = strrep(fileName, [filesep fInfo.name], '');
    end
    if ~isempty(fInfo)
        try
            fNameVec_01 = split(fInfo.name,'.');
        catch
            fNameVec_01 = strsplit(fInfo.name,'.');
        end
        fType = fNameVec_01{end};     
        [bY, bM, bD, bH, bMN, bS] = datevec(fInfo.datenum); 
        backInt = bS + bMN*100 + bH*10000 + bD*1000000 + bM*100000000 + bY*10000000000;
        bckName = strrep(fInfo.name, ['.' fType], ['_' num2str(backInt,'%d') '.' fType]);
        if dispInfo, disp(['previous file datetime= ' fInfo.date]); end
        copyfile(fileName,[folderName filesep bckName],'f');
        if dispInfo, disp(['backup(' fileName ') completed.']); end
        saved = 1;
    elseif dispInfo
        disp([fileName 'not exists.']);
    end
end

