function [ rowIDs ] = searchCellList(cellList, strToSearch, searchMethod, searchBothWays)
%UNTITLED searches the cellList which is a cell array of strings
%   Return the row or col ID of the cell that the string is equal to
%   e.g. cellList = {'car01','car02','table'}
%   rowIDs = searchCellList(cellList, 'car', 0) --> 1,2
%   rowIDs = searchCellList(cellList, 'car', 1) --> empty
%   rowIDs = searchCellList(cellList, 'car01', 0) --> 1
%   rowIDs = searchCellList(cellList, 'car02', 1) --> 2
%   rowIDs = searchCellList(cellList, 'tab', 1) --> empty
%   rowIDs = searchCellList(cellList, 'tab', 0) --> 3
%   rowIDs = searchCellList(cellList, 'table', 1) --> 3
%   rowIDs = searchCellList(cellList, 'table', 0) --> 3
%   rowIDs = searchCellList(cellList, 'frame', 1) --> empty
%   rowIDs = searchCellList(cellList, 'frame', 0) --> empty
    if ~exist('searchBothWays', 'var') || isempty(searchBothWays)
        searchBothWays = false;
    end
    if isempty(cellList)
        rowIDs = [];
    end
    if strcmpi(searchMethod,'findEqual')
        rowIDs = find(strcmpi(cellList,strToSearch)); 
    else%searchMethod=='contains'
        rowIDs = [];
        if searchBothWays
            cellCount = length(cellList);
            for ci = 1:cellCount
                cellStr = cellList{ci};
                foundStr = contains(cellStr,strToSearch) || contains(strToSearch, cellStr);
                if foundStr
                    rowIDs = [rowIDs;ci]; %#ok<AGROW>
                end
            end
        else
            rowIDs = find((contains(cellList(:),strToSearch))); 
        end
    end
end

