function clusterResultFolder = setClusterResultFolder(srcFold, distMatInfoStruct, otherVariablesStruct, OPS)
    if ~exist('otherVariablesStruct','var')
        otherVariablesStruct = [];
    end
    if ~exist('OPS','var')
        OPS = [];
    end
    clearFolderFirst     = getStructField(OPS, 'clearFolderFirst', true);
    createDirIfNotExist  = getStructField(OPS, 'createDirIfNotExist', true);

    clusterResultFolder = [srcFold filesep 'clusterResults'];
    if ~isempty(distMatInfoStruct) && isstruct(distMatInfoStruct)
        if isfield(distMatInfoStruct,'featureName')
            clusterResultFolder = [clusterResultFolder '_' distMatInfoStruct.featureName];
        end
        if isfield(distMatInfoStruct,'cropMethod')
            clusterResultFolder = [clusterResultFolder '_' distMatInfoStruct.cropMethod];
        end
        if isfield(distMatInfoStruct,'distanceMethod')
            clusterResultFolder = [clusterResultFolder '_' distMatInfoStruct.distanceMethod];
        end
    end
    if exist('otherVariablesStruct','var') && ~isempty(otherVariablesStruct) && isstruct(otherVariablesStruct)
        if isfield(otherVariablesStruct,'modeStr')
            clusterResultFolder = [clusterResultFolder '_m' otherVariablesStruct.modeStr];
        end
        if isfield(otherVariablesStruct,'signIDList')
            clusterResultFolder = [clusterResultFolder '_s' getNumVecAsDelimeteredString(otherVariablesStruct.signIDList,'_',true)];
        end
        if isfield(otherVariablesStruct,'userList')
            clusterResultFolder = [clusterResultFolder '_u' getNumVecAsDelimeteredString(otherVariablesStruct.userList,'_',true)];
        end
    end
    if clearFolderFirst && exist(clusterResultFolder,'dir')
        rmdir(clusterResultFolder, 's');
    end
    if createDirIfNotExist && ~exist(clusterResultFolder,'dir')
        mkdir(clusterResultFolder);
    end    
end