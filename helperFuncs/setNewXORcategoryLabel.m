function [labelsNew, idxNew, labelCrossMat] = setNewXORcategoryLabel( labelsGiven, labelsPred, labelCrossMatIn, elementCountTreshold)

    countSamples = length(labelsGiven);
    categoryCountOld = max(labelsGiven);
    
    confMat = confusionmat(labelsGiven,labelsPred);
    
    categoryCountNew = sum(confMat(:)>=elementCountTreshold);
    if ~isempty(labelCrossMatIn)
        historyColCnt = size(labelCrossMatIn,2);
        labelCrossMat = zeros(categoryCountNew,2 + historyColCnt);
    else
        labelCrossMat = zeros(categoryCountNew,3);
    end
    disp(['New cluster count found is = ' num2str(categoryCountNew) '. clusterElementCnt :'])
    disp(mat2str(sort(confMat(confMat>=elementCountTreshold))));
    
    to = 0;
    newCategoryID = 0;
    labelsNew = zeros(countSamples,1);
    idxNew = zeros(countSamples,1);
    %newCategoryColors = rand(categoryCountNew,3);
    for cG = 1:categoryCountOld
        for cP = 1:categoryCountOld
            slct = labelsGiven==cG & labelsPred==cP;
            sampleCnt = sum(slct==1);
            assert(sampleCnt==confMat(cG,cP),'These values must be equal')
            if sampleCnt>=elementCountTreshold
                fr = to+1;
                to = fr+sampleCnt-1;
                newCategoryID = newCategoryID + 1;
                if ~isempty(labelCrossMatIn)
                    %find the relevant row
                    r = find(labelCrossMatIn(:,1)==cG);
                    assert(length(r)==1,'this can not be more than 1');
                    labelCrossMat_Add = [newCategoryID labelCrossMatIn(r,:)];
                else
                    labelCrossMat_Add = [newCategoryID cG];
                end
                labelCrossMat(newCategoryID,:) = [cP labelCrossMat_Add];
                labelsNew(fr:to) = newCategoryID;
                idxNew(fr:to) = find(slct==1);
            end
        end
    end
    zeroRows = find(sum(labelCrossMat,2)==0, 1);
    if ~isempty(zeroRows)
        warning('sth shitty happening');
        labelCrossMat(sum(labelCrossMat,2)~=0,:)
    end
end

