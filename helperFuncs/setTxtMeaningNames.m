function setTxtMeaningNames(srcFolder, write0_disp1)
    %setTxtMeaningNames('C:\FromUbuntu\HospiSign\HospiMOV', 1)
    %srcFolder = 'C:\FromUbuntu\HospiSign\HospiMOV';%'D:\FromUbuntu\Doga';
    if ~exist('write0_disp1','var') || isempty(write0_disp1)
        write0_disp1 = 1;
    end
    singnsMatFileName = [srcFolder filesep 'BosphorusSignLabels.mat'];
    load(singnsMatFileName);
    subFolderNames = getFolderList(srcFolder, true, true);
    for i=1:size(subFolderNames,1)
        signID = strsplit(subFolderNames{i,1}, filesep);
        signID = str2num(signID{1,end});
        if ~isempty(signID)
            fileToWrite = [srcFolder filesep num2str(signID) filesep 'textMeaning_' num2str(signID,'%03d') '.txt'];
            meaningStr = [num2str(signID,'%03d') '_' BosphorusSignLabels{signID,2}];
            if write0_disp1==0
                fid=fopen(fileToWrite,'w');
                fprintf(fid, '%s', meaningStr);
                fclose(fid);
            elseif write0_disp1==1
                disp(['SignID ' num2str(signID) '--- ' meaningStr]);
            end
        end
    end
end

