function setXYTicks(h, X_original, X_map, angleDegrees, xyTickStr)
    if ~exist('xyTickStr','var')
        xyTickStr = 'XTick';
    end
    % X_original = 45:90:450;
    % X_map = 1:10;
    %xticks(X_original);
    xTickLabelsCells = strread(num2str(X_map),'%s')';
    %xticklabels(xTickLabelsCells);
    try
        xtickangle(angleDegrees);
    catch
    end
    set(h, xyTickStr, X_original);
    set(h, [xyTickStr 'Label'], xTickLabelsCells);
end