function clustersStruct = sortClusters(clustersStruct)
    [clustersStruct.dataset.assignments, idx] = sortrows(clustersStruct.dataset.assignments,[2 3 4]);
    clustersStruct.dataset.assignments(:,1) = (1:size(clustersStruct.dataset.assignments,1))';
    clustersStruct.dataset.hogFeats = clustersStruct.dataset.hogFeats(idx,:);
    clustersStruct.dataset.imagesFineCropped = clustersStruct.dataset.imagesFineCropped(idx,:);
    clustersStruct.dataset.labels = clustersStruct.dataset.labels(idx,:);  
end