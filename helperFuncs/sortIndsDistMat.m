function [D_new, indsVec] = sortIndsDistMat(D, idx, indsVec)
    %distMat has to be such that
    %where i<j
    %d(i,j) = distMat(i,j);
    %0 = d(j,i);
    [d1,d2] = size(D);
    if d1~=d2
        error(['D(' num2tr(d1) ' by ' num2str(d2) ') is not a distance matrix']);
    else
        d = d1;
        clear d1 d2
    end
    D_new = zeros(d,d);
    for i=1:d
        D_new = setNewDistVec(D, D_new, i, idx);
    end

    if nargin>2 && exist('indsVec','var')
        indsVec = indsVec(idx);
    else
        indsVec = [];
    end
end

function D_new = setNewDistVec(D_old, D_new, i_old, idx_new)
    i_new = idx_new(i_old);
    dVec_old = [D_old(1:i_new,i_new)' D_old(i_new,i_new+1:end)];%get all vector
    dVec_new = dVec_old(idx_new);  
    D_new(1:i_old,i_old) = dVec_new(1:i_old)';
    D_new(i_old,i_old+1:end) = dVec_new(i_old+1:end);
    assert(D_new(i_old,i_old)==0,'this must hold');
end
