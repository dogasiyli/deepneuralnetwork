function distMatUniq = sortRepeatedRowsOfDistMat(distMat, labelsOfRows)
    [labelsSortedCenters, labelIdx] = sort(labelsOfRows);
    distMatSorted = distMat(labelIdx,:);
    distMatUniq = zeros(max(unique(labelsSortedCenters)),size(distMat,2));
    for i=unique(labelsSortedCenters)'
        if sum(labelsSortedCenters==i)>1
            distMatUniq(i,:) = min(distMatSorted(labelsSortedCenters==i,:));
        else
            distMatUniq(i,:) = distMatSorted(labelsSortedCenters==i,:);
        end
    end
end

