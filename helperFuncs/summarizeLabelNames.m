function [khsSummary_single, khsSummary_double] = summarizeLabelNames(optParamStruct)
    if ~exist('optParamStruct','var')
        optParamStruct = [];
    end
    
    srcFold = getStructField(optParamStruct, 'srcFold', getVariableByComputerName('srcFold'));
    signIDList = getStructField(optParamStruct, 'signIDList', [7,13,49,69,74,85,86,87,88,89,96,112,125,221,222,247,269,273,277,278,290,296,310,335,339,353,396,403,404,424,428,450,521,533,535,536,537,583,586,593,636]);
    saveImagesToFolder = getStructField(optParamStruct, 'saveImagesToFolder', '');
    enforceNewSummary = getStructField(optParamStruct, 'enforceNewSummary', false);
    
    
    khsSummary_single_fileName = [srcFold filesep 'khsSummary_single.mat'];
    khsSummary_double_fileName = [srcFold filesep 'khsSummary_double.mat'];
    
    %if enforced or any not exist :
    if enforceNewSummary==true || ~exist(khsSummary_single_fileName,'file') || ~exist(khsSummary_double_fileName,'file')
        allLabelNames = cell(0,4);
        allLabelImages = cell(0,1);
        khsSummary = cell(0,5);
        errorSigns = cell(0,2);

        allLabelNames_i = 0;
        allLabelImages_i = 0;
        for signID = signIDList
            signFolder = [srcFold filesep num2str(signID)];
            if ~exist(signFolder, 'dir')
                continue
            end

            [clusterNames_single] = getClusterNamesAssignedFromTxt(srcFold, struct('signIDList',signID,'single0_double1',0));
            [clusterNames_both] = getClusterNamesAssignedFromTxt(srcFold, struct('signIDList',signID,'single0_double1',1));

            %now that sign ID folder exists.
            try
                fileName = [srcFold filesep num2str(signID) filesep 'labelSummary.mat'];
                if ~exist(fileName, 'file') || enforceNewSummary
                    analyzeLabelsOfSign(signID,struct('srcFold',srcFold));
                end
                load(fileName, 'signID', 'surList', 'labelSummary', 'labelSwitches_uniq', 'listSummaryTotal', 'listSummarySpecial', 'listByUser', 'listByRepitition');
            catch err
                erSgnCnt = size(errorSigns,1);
                errorSigns{erSgnCnt+1,1} = signID;
                errorSigns{erSgnCnt+1,2} = err.message;
                continue
            end    

            %now we have almost all the info we need for the current sign

            for i=1:length(clusterNames_single)
                allLabelNames_i = allLabelNames_i + 1;
                allLabelNames{allLabelNames_i,1} = signID;
                allLabelNames{allLabelNames_i,2} = 0;
                allLabelNames{allLabelNames_i,3} = i;
                allLabelNames{allLabelNames_i,4} = clusterNames_single{i};

                allLabelImages_i = allLabelImages_i + 1;
                allLabelImages{allLabelImages_i,1} = getSignKHSImage(signID, clusterNames_single{i}, 0, srcFold);
            end
            for i=1:length(clusterNames_both)
                allLabelNames_i = allLabelNames_i + 1;
                allLabelNames{allLabelNames_i,1} = signID;
                allLabelNames{allLabelNames_i,2} = 1;
                allLabelNames{allLabelNames_i,3} = i;
                allLabelNames{allLabelNames_i,4} = clusterNames_both{i};

                allLabelImages_i = allLabelImages_i + 1;
                allLabelImages{allLabelImages_i,1} = getSignKHSImage(signID, clusterNames_both{i}, 1, srcFold);
            end
        end
        %now what are the unique key hand shapes?
        [khsSummary_single] = getSortedLabelNames(allLabelNames, allLabelImages, 0, saveImagesToFolder);
        [khsSummary_double] = getSortedLabelNames(allLabelNames, allLabelImages, 1, saveImagesToFolder);
        save(khsSummary_single_fileName,'khsSummary_single');
        save(khsSummary_double_fileName,'khsSummary_double');
    else
        load(khsSummary_single_fileName);
        load(khsSummary_double_fileName);
    end
    
    %load BosphorusSignLabels
    BosphorusSignLabels = [];
    load([srcFold filesep 'BosphorusSignLabels.mat']);
    
    khsCnt_single = size(khsSummary_single,1);
    for i=1:khsCnt_single
        khsName = khsSummary_single{i,1};
        khsSignIDs = khsSummary_single{i,3};
        %khsImage = khsSummary_single{i,4};
        %khsAllImages = khsSummary_single{i,5};
        
        initialInfo = ['        \subsection{' khsName '}\label{keyhandshape:' khsName '}' newlinefunc ...;
                       '           This key-hand-shape seen is \reffig{fig:khs_' khsName '} used in SIGNLISTSTRING'];
        SIGNLISTSTRING = ''; %#ok<NASGU>
        sAdditionalChar = 's are';
        if length(khsSignIDs)==1
            SIGNLISTSTRING = getSignString(BosphorusSignLabels, khsSignIDs(1));
            SIGNLISTSTRING = [SIGNLISTSTRING '.' newlinefunc ' ' newlinefunc]; %#ok<*AGROW>
            sAdditionalChar = ' is';
        elseif length(khsSignIDs)==2
            SIGNLISTSTRING = [getSignString(BosphorusSignLabels, khsSignIDs(1)) ' and ' getSignString(BosphorusSignLabels, khsSignIDs(2)) '.'  newlinefunc ' ' newlinefunc];
        else
            SIGNLISTSTRING = [' following signs :' newlinefunc];
            SIGNLISTSTRING = [SIGNLISTSTRING '           \begin{itemize}' newlinefunc];
            for j=1:length(khsSignIDs)
                if j<length(khsSignIDs)-1%1 in 3
                    SIGNLISTSTRING = [SIGNLISTSTRING '                 \item ' getSignString(BosphorusSignLabels, khsSignIDs(j)) ',' newlinefunc];
                elseif j==length(khsSignIDs)-1%2 in 3
                    SIGNLISTSTRING = [SIGNLISTSTRING '                 \item ' getSignString(BosphorusSignLabels, khsSignIDs(j)) ' and' newlinefunc];                    
                elseif j==length(khsSignIDs)%3 in 3
                    SIGNLISTSTRING = [SIGNLISTSTRING '                 \item ' getSignString(BosphorusSignLabels, khsSignIDs(j)) newlinefunc];   
                end
            end
            SIGNLISTSTRING = [SIGNLISTSTRING '           \end{itemize}' newlinefunc];            
        end
        initialInfo = strrep(initialInfo, 'SIGNLISTSTRING', SIGNLISTSTRING);
        
        labelDistributionInfo = ['           The label distribution over signs and the predicted meaning of key-hand-shape for sign' sAdditionalChar  ...
                                 ' shown in the \reftable{Table:distOfKHS' khsName '}.' newlinefunc];
        
        tableBeg = ['            \begin{table}[!htbp]'   newlinefunc() ...
                    '               \centering'  newlinefunc() ...
                    '               \begin{tabular}{|c|c|c|c|c|}'  newlinefunc() ...
                    '               \hline'  newlinefunc() ...
                    '                    \textbf{signID} & \textbf{LH} & \textbf{RH} & \textbf{Sum} & \textbf{Predicted Meaning} \\ \hline'  newlinefunc()];
        eachRow =   ['                    signID & leftHandCnt & rightHandCnt & sumHandCnt & MeanPred \\ \hline' newlinefunc()];
        tableEnd = ['               \end{tabular}' newlinefunc() ...
                    '               \caption{Key-Hand-Shape(' khsName ') Distribution Over Signs}' newlinefunc() ...
                    '               \label{Table:distOfKHS' khsName '}' newlinefunc() ...
                    '            \end{table}' newlinefunc()];
        tableOfKHS = createTableOfLabelDistribution(srcFold, khsName, khsSignIDs, 0);
        rowCntOfTable = size(tableOfKHS,1);
        rowsOfTable = '';
        for r=1:rowCntOfTable
            row2add = strrep(eachRow, 'signID', getNumberOrSomeChar(tableOfKHS(r,1), '-'));
            row2add = strrep(row2add, 'leftHandCnt', getNumberOrSomeChar(tableOfKHS(r,2), '-'));
            row2add = strrep(row2add, 'rightHandCnt', getNumberOrSomeChar(tableOfKHS(r,3), '-'));
            row2add = strrep(row2add, 'sumHandCnt', getNumberOrSomeChar(tableOfKHS(r,4), '-'));
            row2add = strrep(row2add, 'MeanPred', '-');
            rowsOfTable = [rowsOfTable row2add];
        end
        if r>1
            row2add = strrep(eachRow, 'signID', 'Total');
            row2add = strrep(row2add, 'leftHandCnt',  getNumberOrSomeChar(sum(tableOfKHS(:,2)),'-'));
            row2add = strrep(row2add, 'rightHandCnt', getNumberOrSomeChar(sum(tableOfKHS(:,3)),'-'));
            row2add = strrep(row2add, 'sumHandCnt',   getNumberOrSomeChar(sum(tableOfKHS(:,4)),'-'));
            row2add = strrep(row2add, 'MeanPred', '-');
            rowsOfTable = [rowsOfTable row2add];
        end
        
        tableString = [tableBeg rowsOfTable tableEnd];
        
        
        imageString = ['            \begin{figure}[H]' newlinefunc() ...
                       '                \centering' newlinefunc() ...
                       '                \includegraphics[scale=0.5]{' khsName '.png}' newlinefunc() ...
                       '                \caption{Key-Hand-Shape(' khsName ') Representative Frames of All Signs}' newlinefunc() ...
                       '                \label{fig:khs_' khsName '}' newlinefunc() ...
                       '            \end{figure}' newlinefunc()];
                   
        wholeStringOfKHS = [initialInfo labelDistributionInfo tableString imageString];
        
        diary_start(srcFold, khsName);
        displayOnScreen(wholeStringOfKHS);
        diary_end();
    end
    
end

function displayOnScreen(str)
    a = newlinefunc;
    if ~isempty(a)
        strList = strrep(str,a,'xlaklak');
        strList = strsplit(strList,'xlaklak');
        for i=1:length(strList)
            disp(strList{i});
        end
    else
        disp(str);
    end
end
function diary_start(srcFold, khsName)
    diaryFolderName = [srcFold filesep 'keyHandShapeLatex' ];
    diaryFileName = [diaryFolderName filesep 'latexKHSsection_' khsName '.txt'];
    if ~exist(diaryFolderName,'dir')
        mkdir(diaryFolderName);
    end
    if exist(diaryFileName,'file')
        delete(diaryFileName);
    end
    diary(diaryFileName);
    disp(' ');
    disp('xoxoxoxoxoxoxoxoxoxox');
    disp(datestr(now))
end
function diary_end()
    disp(' ');
    disp('xoxoxoxoxoxoxoxoxoxox');
    diary('off');
end

function str = getNumberOrSomeChar(number, theChar)
    if number==0
        str = theChar;
    else
        str = num2str(number);
    end
end

function [tableOfKHS, problemSign] = createTableOfLabelDistribution(srcFold, khsName, khsSignIDs, single0_double1)
    problemSign = [];
    tableOfKHS = [];
    colsToGet = [2 3 1];
    if single0_double1==1, colsToGet = 4; end
    for s=1:length(khsSignIDs)
        signID = khsSignIDs(s);
        labelSummary_file = [srcFold filesep num2str(signID) filesep 'labelSummary.mat'];
        if exist(labelSummary_file,'file')
            load(labelSummary_file,'listSummaryTotal');
        else
            problemSign = [problemSign, signID];
        end
        try
            %use listSummaryTotal and clusterNames
            clusterNames = getClusterNamesAssignedFromTxt(srcFold, struct('signIDList',signID,'single0_double1',single0_double1));
            khsIDInSign = find(strcmp(clusterNames,khsName));
            rowToGet = listSummaryTotal(khsIDInSign+1,colsToGet);
            tableOfKHS = [tableOfKHS;signID rowToGet];
        catch
            problemSign = [problemSign;signID];
        end
    end
end

function retStr = getSignString(BosphorusSignLabels, signID)
    signTr = BosphorusSignLabels{signID,1};
    signEn = BosphorusSignLabels{signID,2};
    hyperrefTxt = ['\hyperref[subsection:sign' num2str(signID,'%03d') ']'];
    signStr = ['sign' num2str(signID,'%03d') '-' signTr '-' signEn];
    retStr = [hyperrefTxt '{' signStr '}'];
end

function [khsSummary] = getSortedLabelNames(allLabelNames, allLabelImages, single0_double1, saveToFolder)
    rowsGrabbed = cell2mat(allLabelNames(:,2))==single0_double1;
    M = allLabelNames(rowsGrabbed,:);
    im = allLabelImages(rowsGrabbed);
    [uniqKeyHandShapeNames, ia, ic] = unique(M(:,4));
    %ia -> same count(61) with uniqKeyHandShapeNames, values are rowIDs of M
    %ic -> same count(169) with M, values are rowIDs of uniqKeyHandShapeNames
    %sort from most used to least used
    hist_ic = hist(ic,1:length(ia));
    [hist_ic_sorted, ic_sortedIDs] = sort(hist_ic,'descend');
    
    keyHandShapeSortedRowIDs = ia(ic_sortedIDs);
    uniqKeyHandShapeNames = uniqKeyHandShapeNames(ic_sortedIDs);
    %uniqKeyHandShapeNames = M(keyHandShapeSortedRowIDs,4);
    
    numberOfKHS = length(uniqKeyHandShapeNames);
    
    khsSummary = cell(numberOfKHS,5);
    
    h = figure(1);
    for i=1:numberOfKHS
        khsSummary{i,1} = uniqKeyHandShapeNames{i};
        khsSummary{i,2} = i;
        
        rowsFound = find(strcmp(M(:,4),khsSummary{i,1}));
        uniqueSigns = M(rowsFound,1);
        khsSummary{i,3} = cell2mat(uniqueSigns)';
        
        khsSummary{i,4} = im(keyHandShapeSortedRowIDs(i));
        
        surroundWithColorStruct = struct('thicknessParam',0.1,'colorParam',[1 1 1]);
        imSizeMax = [200 200];
        boxSize = imSizeMax;
        
        rcRatio = 1/3;
        imCnt = length(khsSummary{i,3});
        [imPalette, paletteRCSize] = imCollageFuncs('createPalette', struct('imCnt',imCnt,'channelCnt',3,'rcRatio',rcRatio,'imSizeMax',imSizeMax));
        for j=1:imCnt
            imAddCur = im(rowsFound(j));
            imAddCur = imresize(imAddCur{1},imSizeMax*0.9);
            [imPalette, paletteRCSize] = imCollageFuncs('insertIntoPalette', struct('imageID',j,'paletteRCSize',paletteRCSize,'imPalette',imPalette,'imageToAdd',imAddCur,'boxSize', boxSize,'surroundWithColorStruct',surroundWithColorStruct));
        end
        whiteImage = (max(imAddCur(:))-50)*ones(200,200,3,'like',imAddCur);whiteImage(1) = 0;
        for j=imCnt+1:prod(paletteRCSize)
            [imPalette, paletteRCSize] = imCollageFuncs('insertIntoPalette', struct('imageID',j,'paletteRCSize',paletteRCSize,'imPalette',imPalette,'imageToAdd',whiteImage,'boxSize', boxSize,'surroundWithColorStruct',surroundWithColorStruct));
        end
        
        khsSummary{i,5} = imPalette;
        sc = warning;
        warning('off');
        clf;imshow(imPalette);
        %titlestr = ['Key-Hand-Shape : ' khsSummary{i,1} ];
        %title(titlestr);
        if (imCnt>1)
            xlabel(['SingIDs = ' mat2str(khsSummary{i,3})])
        else
            xlabel(['SingID ' num2str(khsSummary{i,3})])
        end
        warning(sc);
        
        strBegin = '';
        if single0_double1==1
            strBegin = 'd_';            
        end
        if isdir(saveToFolder)
            saveas(h,[saveToFolder filesep strBegin khsSummary{i,1} '.png']);
        end
    end
end


















