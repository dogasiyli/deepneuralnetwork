function [possibleSequenceFlows, labelSummary, problematicVids] = summarizeLabels(signID, optParamStruct)
%[possibleSequenceFlows, labelSummary, problematicVids]= summarizeLabels(404, struct('userList', 2:5, 'maxRptCnt', 20));
%% 0. Define the variables
    userList = getStructField(optParamStruct, 'userList', [2 3 4 5]);
    maxRptCnt = getStructField(optParamStruct, 'maxRptCnt', 1);
    srcFold = getStructField(optParamStruct, 'srcFold', getVariableByComputerName('srcFold'));
    problematicVids = [];
%% 
    [surList, listElemntCnt] = createSURList(srcFold, 2, struct('signIDList',signID,'userID',userList,'maxRptCnt',maxRptCnt));
    labelSummary = getlabelSummary(srcFold, surList, listElemntCnt);
    possibleSequenceFlows = struct;
    possibleSequenceFlows.LH = getLabelTransitionMat(labelSummary.labelCells(:,1));
    possibleSequenceFlows.RH = getLabelTransitionMat(labelSummary.labelCells(:,2));
    possibleSequenceFlows.BH = getLabelTransitionMat(labelSummary.labelCells(:,3));
end

function [possibleSequenceFlows, uniqueLabelIDs] = getLabelTransitionMat(labelCells)
    allLabels = horzcat(1, labelCells{:});
    uniqueLabelIDs = unique(allLabels);
    uniqueLabelIDs(uniqueLabelIDs==0) = [];%remove 0's as they are not labels after all
    
    vidCnt = size(labelCells,1);
    possibleSequenceFlows = cell(vidCnt,4);
    for i=1:vidCnt
        [transitionMatForThevideo, firstLastSeenTable, firstLastSeenTable_lr] = getTransitionFromAVideo(labelCells{i}, uniqueLabelIDs);
        possibleSequenceFlows{i,1} = firstLastSeenTable(:,[1 end]);
        possibleSequenceFlows{i,2} = [firstLastSeenTable(firstLastSeenTable(:,1)~=1,1), firstLastSeenTable(firstLastSeenTable(:,1)~=1,end)./sum(firstLastSeenTable(firstLastSeenTable(:,1)~=1,end))];
        %possibleSequenceFlows{i,2}(firstLastSeenTable(:,1)==1,:) = [];
        possibleSequenceFlows{i,3} = firstLastSeenTable_lr(:,[1 end]);
        possibleSequenceFlows{i,4} = firstLastSeenTable;
        possibleSequenceFlows{i,5} = firstLastSeenTable_lr;
        possibleSequenceFlows{i,6} = transitionMatForThevideo;
    end
end

function [transitionMatForThevideo, firstLastSeenTable, firstLastSeenTable_lr] = getTransitionFromAVideo(labels, knownUniqueLabels)
    labelsBckp = labels;
    frameCount = length(labelsBckp);
    if nargin<2
        knownUniqueLabels = unique(labels);
    end
    labels(labels==0) = [];
    knownUniqueLabels(knownUniqueLabels==0) = [];
    labelCnt = length(knownUniqueLabels);
    
    transitionMatForThevideo = NaN(labelCnt);
    for fr=1:labelCnt
        for to=fr+1:labelCnt
            frTrue = labels(1:end-1)==knownUniqueLabels(fr);
            toTrue = labels(2:end)==knownUniqueLabels(to);
            transCells = find(frTrue & toTrue);
            transitionMatForThevideo(fr,to) = length(transCells);
        end
    end
    
    cur_i = 1;
    labelSeenSequence = [];
    nextDifLabelID = 1;
    while (~isempty(nextDifLabelID) && cur_i<length(labels))
        curLabel = labels(cur_i);
        nextDifLabelID = find(labels(cur_i+1:end)~=curLabel,1,'first');
        labelSeenSequence = [labelSeenSequence,[curLabel;cur_i]]; %#ok<AGROW>
        cur_i = cur_i + nextDifLabelID;
    end  
    
    %here we want the labels to still have the 0's inside..
    firstLastSeenTable_lr = get_FirstLastSeenTable_lr(labelsBckp, knownUniqueLabels);
    firstLastSeenTable = get_FirstLastSeenTable(labelSeenSequence, frameCount);  
end

function firstLastSeenTable =get_FirstLastSeenTable(labelSeenSequence, frameCount)
    firstLastSeenTable = [labelSeenSequence',[labelSeenSequence(2,2:end)-1 frameCount]'];
    firstLastSeenTable = [firstLastSeenTable,firstLastSeenTable(:,3)-firstLastSeenTable(:,2)+1];
    firstLastSeenTable = [firstLastSeenTable,firstLastSeenTable(:,4)./frameCount];
    
    %normalize the begin and end sequence lengths
    stillFramesRows = (firstLastSeenTable(:,1)==1);
    firstLastSeenTable(stillFramesRows,end) = mean(firstLastSeenTable(stillFramesRows,end));
end

function firstLastSeenTable_lr = get_FirstLastSeenTable_lr(labels, knownUniqueLabels)
    labelCnt = length(knownUniqueLabels);
    firstLastSeenTable_lr = NaN(labelCnt,4);
    
    %is this video start with 1 and end with 1 as expected
    startAndEnd_01 = sum(knownUniqueLabels==1)>0;
    if startAndEnd_01
        relatedRow = find(knownUniqueLabels==1);
        %first_beg_01 = find(labels(1:end)==1,1,'first');
        first_end_01 = find(labels(1:end)~=1,1,'first')-1;
        %last_beg_01 = length(labels) - find(labels(end:-1:1)==1,1,'first');
        last_end_01 = length(labels) - find(labels(end:-1:1)~=1,1,'first') + 2;       
        if isempty(last_end_01) && isempty(first_end_01) && labelCnt==1 && knownUniqueLabels==1
            firstLastSeenTable_lr(relatedRow,1) = 1;
            firstLastSeenTable_lr(relatedRow,2) = 1;
            firstLastSeenTable_lr(relatedRow,3) = length(labels);
            firstLastSeenTable_lr(relatedRow,4) = length(labels);            
        elseif isempty(last_end_01) && isempty(first_end_01)
            if sum(labels)==length(labels)
                firstLastSeenTable_lr(relatedRow,1) = 1;
                firstLastSeenTable_lr(relatedRow,2) = 1;
                firstLastSeenTable_lr(relatedRow,3) = length(labels);
                firstLastSeenTable_lr(relatedRow,4) = length(labels);                            
            else
                error('wtf');
            end
        else
            firstLastSeenTable_lr(relatedRow,1) = 1;
            firstLastSeenTable_lr(relatedRow,2) = first_end_01;
            firstLastSeenTable_lr(relatedRow,3) = last_end_01;
            firstLastSeenTable_lr(relatedRow,4) = first_end_01 + (length(labels)-last_end_01);            
        end
    end
    
    %for id==1 we will use a different methodology
    for i=1:labelCnt
        curLabel = knownUniqueLabels(i);
        if curLabel==1
            continue
        end
        first_beg = find(labels(1:end)==curLabel,1,'first');
        last_end = length(labels) - find(labels(end:-1:1)==curLabel,1,'first') + 1; 
        firstLastSeenTable_lr(i,:) = [curLabel first_beg last_end last_end-first_beg+1];
    end
    
    %normalize the begin and end sequence lengths
    nonStillFramesRows = (firstLastSeenTable_lr(:,1)~=1);
    firstLastSeenTable_lr(nonStillFramesRows,5) = firstLastSeenTable_lr(nonStillFramesRows,end)./sum(firstLastSeenTable_lr(nonStillFramesRows,end));
end
