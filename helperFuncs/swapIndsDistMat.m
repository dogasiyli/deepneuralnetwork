function [distMat, indsVec] = swapIndsDistMat(distMat, i, j, indsVec)
    %distMat has to be such that
    %where i<j
    %d(i,j) = distMat(i,j);
    %0 = d(j,i);
    if i==j
        return
    end
    if (i>j)
        t=i;
        i=j;
        j=t;
        clear t;
    end
    %%i<j for sure
    
    d = distMat(i,j);
    %small-i
    dVec_i = [distMat(1:i,i)' distMat(i,i+1:end)];%get all vector
    dVec_i([i j]) = [];%remove 0 and j’th
    dVec_i = [dVec_i(1:i-1) d dVec_i(i:end)];%insert d to i’th
    dVec_i = [dVec_i(1:j-1) 0 dVec_i(j:end)];%insert 0 to j’th
    
    %big-j
    dVec_j = [distMat(1:j,j)' distMat(j,j+1:end)];%get all vector
    dVec_j([i j]) = [];%remove 0 and i’th
    dVec_j = [dVec_j(1:i-1) 0 dVec_j(i:end)];%insert 0 to i’th
    dVec_j = [dVec_j(1:j-1) d dVec_j(j:end)];%insert d to j’th
    
       
    distMat(1:i,i) = dVec_j(1:i)';
    distMat(i,i+1:end) = dVec_j(i+1:end);
    assert(distMat(i,i)==0,'this must hold');

    distMat(1:j,j) = dVec_i(1:j)';
    distMat(j,j+1:end) = dVec_i(j+1:end);
    assert(distMat(j,j)==0,'this must hold');
    
    if nargin>3 && exist('indsVec','var')
        t = indsVec(i);
        indsVec(i) = indsVec(j);
        indsVec(j) = t;
    else
        indsVec = [];
    end
end
