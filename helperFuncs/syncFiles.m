function syncFiles(fromFolder, toFolder, signIDList, userIDList, selectionsStruct)

    %go through folders in fromFolder
    %if the same folder also exists in toFolder
    %do the action in selectionsStruct.FileID
    %
    for signID = signIDList
        for userID = userIDList
            for repID = 1:20
                userFolderStr = [num2str(signID) filesep 'User_' num2str(userID) '_' num2str(repID) filesep];
                str_fr = [fromFolder filesep userFolderStr];
                str_to = [toFolder filesep userFolderStr];
                if ~exist(str_fr,'dir') || ~exist(str_to,'dir')
                    continue
                end
                %now that both folders exist
                %selectionsStruct = struct;
                if isfield(selectionsStruct,'depthMat')%selectionsStruct.depthMat = 'copyIfNotExist';
                    applyActionFile([str_fr 'depth.mat'], [str_to 'depth.mat'], selectionsStruct.depthMat);
                end
                if isfield(selectionsStruct,'skeletonMat')%selectionsStruct.skeletonMat = 'copyIfNotExist';
                    applyActionFile([str_fr 'skeleton.mat'], [str_to 'skeleton.mat'], selectionsStruct.skeletonMat);
                end
                if isfield(selectionsStruct,'hogCropMat')%selectionsStruct.hogCropMat = 'copyIfNotExist';
                    applyActionFile([str_fr 'hog_crop.mat'], [str_to 'hog_crop.mat'], selectionsStruct.hogCropMat);
                end
                if isfield(selectionsStruct,'surfNormCropMat')%selectionsStruct.surfNormCropMat = 'copyIfNotExist';
                    applyActionFile([str_fr 'surfNorm_crop.mat'], [str_to 'surfNorm_crop.mat'], selectionsStruct.surfNormCropMat);
                end
                if isfield(selectionsStruct,'surfNormMat')%selectionsStruct.surfNormMat = 'copyIfNotExist';
                    applyActionFile([str_fr 'surfNorm.mat'], [str_to 'surfNorm.mat'], selectionsStruct.surfNormMat);
                end
                if isfield(selectionsStruct,'surformZip')%selectionsStruct.surformZip = 'copyIfNotExist';
                    applyActionFile([str_fr 'surfNorm.zip'], [str_to 'surfNorm.zip'], selectionsStruct.surformZip);
                end
                if isfield(selectionsStruct,'colorMp4')%selectionsStruct.colorMp4 = 'copyIfNotExist';
                    applyActionFile([str_fr 'color.mp4'], [str_to 'color.mp4'], selectionsStruct.colorMp4);
                end
            end
        end
    end
end

function applyActionFolder()
end

function applyActionFile(fileNameSrc, fileNameDst, actionStr)
    try
        src_exist = exist(fileNameSrc,'file');
        dst_exist = exist(fileNameDst,'file');
        srcAtt = dir(fileNameSrc);
        dstAtt = dir(fileNameDst);
        switch (actionStr)
            case 'copyOverwrite'
                if ~src_exist
                    disp(['file not exist [' fileNameSrc ']']);
                    return
                end
                disp(['Copy started for ' num2str(srcAtt.bytes) ' bytes']);
                copyfile(fileNameSrc, fileNameDst, 'f');
                disp(['copied [' fileNameSrc ']-->[ ' fileNameDst ']']);
            case 'copyIfBiggerOrNewer'
                if ~src_exist
                    disp(['file not exist [' fileNameSrc ']']);
                    return
                end
                if (srcAtt.bytes>dstAtt.bytes) || (srcAtt.datenum>dstAtt.datenum)
                    disp(['Copy started for ' num2str(srcAtt.bytes) ' bytes']);
                    copyfile(fileNameSrc, fileNameDst, 'f');
                    msg = ['copied [' fileNameSrc ']-->[ ' fileNameDst ']'];
                    if (srcAtt.bytes>dstAtt.bytes)
                        msg = [msg '-bigger(' num2str(srcAtt.bytes) '>' num2str(dstAtt.bytes) ')'];
                    end
                    if (srcAtt.datenum>dstAtt.datenum)
                        msg = [msg '-newer(' srcAtt.date '>' dstAtt.date ')'];
                    end
                else
                    msg = ['skipping similar files [' fileNameSrc '] and[ ' fileNameDst ']'];
                end
                disp(msg);
            case 'remainBiggerOrNewer'
                if ~src_exist && ~dst_exist
                    disp(['neither file not exist [' fileNameSrc ',' fileNameDst ']']);
                    return
                elseif  (~dst_exist && src_exist)
                    s2d = true;
                    d2s = false; 
                elseif  (dst_exist && ~src_exist)
                    s2d = false;
                    d2s = true;
                elseif  (dst_exist && src_exist)
                    s2d = false;
                    d2s = false;                    
                end
                try
                     if s2d || ((~isempty(srcAtt) && ~isempty(dstAtt))  && ((srcAtt.bytes>dstAtt.bytes) || (srcAtt.datenum>dstAtt.datenum)))
                        disp(['Copy started for ' num2str(srcAtt.bytes) ' bytes']);
                        copyfile(fileNameSrc, fileNameDst, 'f');
                        msg1 = 'copied';
                        msg2 = ['[' fileNameSrc ']-->[ ' fileNameDst ']'];
                        if (~dst_exist && src_exist)
                            msg1 = [msg1 ',dst not exist'];
                        else
                            if (srcAtt.bytes>dstAtt.bytes)
                                msg1 = [msg1 ',bigger'];
                                msg2 = [msg2 '[' num2str(srcAtt.bytes) '>' num2str(dstAtt.bytes) ']'];
                            end
                            if (srcAtt.datenum>dstAtt.datenum)
                                msg1 = [msg1 ',newer'];
                                msg2 = [msg2 '[' srcAtt.date '>' dstAtt.date ']'];
                            end
                        end                            
                    elseif d2s || ((~isempty(srcAtt) && ~isempty(dstAtt))  && ((srcAtt.bytes<dstAtt.bytes) || (srcAtt.datenum<dstAtt.datenum)))
                        t = fileNameSrc;
                        fileNameSrc = fileNameDst;
                        fileNameDst = t;
                        disp(['Copy started for ' num2str(dstAtt.bytes) ' bytes']);
                        copyfile(fileNameSrc, fileNameDst, 'f');
                        msg1 = 'copied';
                        msg2 = ['[' fileNameSrc ']-->[ ' fileNameDst ']'];
                        if (dst_exist && ~src_exist)
                            msg1 = [msg1 ',src not exist'];
                        else
                            if (srcAtt.bytes>dstAtt.bytes)
                                msg1 = [msg1 ',bigger'];
                                msg2 = [msg2 '[' num2str(srcAtt.bytes) '>' num2str(dstAtt.bytes) ']'];
                            end
                            if (srcAtt.datenum>dstAtt.datenum)
                                msg1 = [msg1 ',newer'];
                                msg2 = [msg2 '[' srcAtt.date '>' dstAtt.date ']'];
                            end
                        end
                    else
                        msg1 = 'skipping similar files';                        
                        msg2 = ['[' fileNameSrc '] and [ ' fileNameDst ']'];                        
                    end
                catch err
                    msg1 = ['error(' err.message ') occured while copying'];
                    msg2 = ['[' fileNameSrc ']-->[ ' fileNameDst ']'];
                end
                disp([msg1 '-' msg2]);
            case 'copyIfNotExist'
                if ~src_exist
                    disp(['file not exist [' fileNameSrc ']']);
                    return
                end
                if dst_exist
                    disp(['file already exist [' fileNameDst ']']);
                    return
                end
                disp(['Copy started for ' num2str(srcAtt.bytes) ' bytes']);
                copyfile(fileNameSrc, fileNameDst, 'f');
                disp(['copied [' fileNameSrc ']-->[ ' fileNameDst ']']);
            case 'removeFromSrc'
                if ~src_exist
                    disp(['file not exist [' fileNameSrc ']']);
                    return
                end
                delete(fileNameSrc);
                disp(['removed [' fileNameSrc ']']);
            case 'removeFromTo'
                if ~dst_exist
                    disp(['file not exist [' fileNameDst ']']);
                    return
                end
                delete(fileNameDst);
                disp(['removed [' fileNameDst ']']);
        end
    catch
        disp(['Error(applyActionFile(' fileNameSrc ', ' fileNameDst ', ' actionStr '))']);
    end
end


%example run:
% selectionsStruct = struct;
% selectionsStruct.hogCropMat = 'remainBiggerOrNewer';
% selectionsStruct.surfNormCropMat = 'remainBiggerOrNewer';
% selectionsStruct.surfNormMat = 'remainBiggerOrNewer';
% selectionsStruct.surformZip = 'remainBiggerOrNewer';
% disp('between bogaziciKasa-windows and msiLaptop-windows-networkShare');
% syncFiles('\\DOGAMSILAPTOP\DgMsiLapD\FromUbuntu\Doga', 'C:\FromUbuntu\HospiSign\HospiMOV', [7 13 49 69 74 85 86 87 88 586 593 636], 2:7, selectionsStruct);
% syncFiles('\\DOGAMSILAPTOP\DgMsiLapD\FromUbuntu\Doga', 'C:\FromUbuntu\HospiSign\HospiMOV', 7:800, 2:7, selectionsStruct);
