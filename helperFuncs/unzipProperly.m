function frameCnt = unzipProperly(rootFolder, zipFileName)
%unzipProperly the file <zipFileName>.zip can sometimes include a <zipFileName>
%folder itself. we want the <zipFileName> folder right in rootFolder

    unzippedFolderLevelProper = [rootFolder  filesep zipFileName filesep];
    unzippedFolderLevelInner = [rootFolder  filesep zipFileName filesep zipFileName filesep];
    fullZipName = [rootFolder filesep zipFileName '.zip'];
    disp(['Trying to unzip (' fullZipName ')..']);
    unzip(fullZipName, rootFolder);    
        
    %check if the cropped folder has cropped folder or the images
    if exist(unzippedFolderLevelInner,'dir')
        disp('unzipped but to an extra folder hence we will correct this');
        [fileList, frameCnt] = getFileList( unzippedFolderLevelInner, '.png');
        delete(fullZipName);
        zip(fullZipName, fileList, unzippedFolderLevelInner);
        rmdir(unzippedFolderLevelProper,'s');
        mkdir(unzippedFolderLevelProper);
        unzip(fullZipName, unzippedFolderLevelProper);
    else
        [~, frameCnt] = getFileList(unzippedFolderLevelProper, '.png');
        disp('unzipped sucesfully');
    end    
end

