function dS = updateDataStruct( dS, varargin)
%updateDataStruct Updates the dataStruct
%   The file names needs an update when some of the variables inside the
%   struct changes
% countBatch
% type -> can change to train,valid or test
% data_act -> can change to data or act
% batchID_current -> can change from 1 to dS.countBatch.<type>
%
% values that needs to be automatically updated :
% fileName_current = dS.getFileName_Current(dS);
% countSamples -> needs to load the labels first, then get its length
% loadFolder: 'D:\Datasets\' - or it can become the temporary save location for activation files

    %P1D0 -> Passed assed parameter = 1, Default value used = 0
    [ loadFolder,  type,  data_act,  batchID_current, P1D0] = parseArgs(...
    {'loadFolder' 'type' 'data_act' 'batchID_current' },...
    {     []        []       []            []         },...
    varargin{:});

    if (~isempty(loadFolder) && P1D0.loadFolder==1)
        if ~strcmp(loadFolder(end),'\')
            loadFolder = [loadFolder '\'];
        end
        dS.loadFolder = loadFolder;
    end
    if (~isempty(type) && P1D0.type==1)
        dS.type = type;
        eval(['dS.batchCnt_current = dS.countBatch.' type ';']);
    end
    if (~isempty(data_act) && P1D0.data_act==1)
        dS.data_act = data_act;
    end
    if (~isempty(batchID_current) && P1D0.batchID_current==1)
        dS.batchID_current = batchID_current;
    end
    
    dS.fileName_current = dS.getFileName_Current(dS);
end