function listStrVar = vec2strList(listVec, singleLetter, sepChar)
    try
        listStrVar = replace(replace(replace(mat2str(listVec),'[',''),']',''),' ',sepChar); 
    catch
        listStrVar = strrep(strrep(strrep(mat2str(listVec),'[',''),']',''),' ',sepChar); 
    end
    listStrVar = [singleLetter listStrVar];
end