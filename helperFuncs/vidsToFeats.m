function vidsToFeats(retrieveHandImagesFrom, toFolder)
    if ~exist('toFolder','var') || isempty(toFolder)
        toFolder = retrieveHandImagesFrom;
    end
    signFolds = getFolderList(retrieveHandImagesFrom, false, false);
    signFolds = sortFolderNames(signFolds, '');
    foldCnt = length(signFolds);
    vidFeatAll = [];
    for sID=1:foldCnt
        %different signs
        videoFolds = getFolderList([retrieveHandImagesFrom filesep signFolds{sID}], false, false);
        videoFolds = sortFolderNames(videoFolds, 'v');
        vidCnt = length(videoFolds);
        vidFeat = [];
        for i=1:vidCnt
            %different videos
            [fileNames, imCnt] = getFileList([retrieveHandImagesFrom filesep signFolds{sID} filesep videoFolds{i}], '.png');
            fileNames = sortrows(fileNames);
            for imID = 1:imCnt
                a = imread([retrieveHandImagesFrom filesep signFolds{sID} filesep videoFolds{i} filesep fileNames{imID}]);
                f = extFt(a);
                vidFeat = [vidFeat;f];
%                 if imID>15
%                     break
%                 end
            end
%             if i>5
%                 break
%             end
        end
        %save vidFeat to somewhere
        vidFeatFileName = [toFolder filesep 'videoFeats_s' num2str(sID,'%03d') '.mat'];
        save(vidFeatFileName,'vidFeat','-v7.3');  
        vidFeatAll = [vidFeatAll;vidFeat];
%         if sID>6
%             break
%         end
    end
    [numOfSamples, featSize] = size(vidFeatAll);
    pcaFileName = [toFolder filesep 'pcaExt.mat'];
    [coeff,score,latent] = pca(vidFeatAll);
    save(pcaFileName,'coeff','score','latent','numOfSamples','featSize','-v7.3');  
end

function f = extFt(a)
    f = TPLBP(a);
    f = f(:);
    f = f';
end

function saveMatToFile(matFileName, M)
    fid = fopen(matFileName, 'w');
    if fid==-1, error('Cannot open file: %s', matFileName); end
    [rowCnt, colCnt]  = size(M);
    for rid = 1:rowCnt
        for cid = 1:colCnt
            fprintf(fid, '%f ',  M(rid, cid));
        end 
        fprintf(fid, '\n');
    end
    fclose(fid);
end

function foldNamesSorted = sortFolderNames(foldNames, removeChar)
    foldCnt = size(foldNames,1);
    foldIntIDs = zeros(foldCnt,1);
    for i=1:foldCnt
        foldIntIDs(i) = str2double(strrep(foldNames{i},removeChar,''));
    end
    [sortedIDs, idx] = sort(foldIntIDs);
    foldNamesSorted = foldNames(idx);
end