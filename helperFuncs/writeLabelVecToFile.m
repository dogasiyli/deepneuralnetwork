function writeLabelVecToFile(theFoldName, vecTowrite, lh1_rh2_bh3, overwriteLabelFiles)
    labelFolder = theFoldName;
    if ~contains(labelFolder,[filesep 'labelFiles'])
        labelFolder = [theFoldName filesep 'labelFiles'];
    end
    if ~exist(labelFolder,'dir')
        mkdir(labelFolder);
    end
    if sum(vecTowrite)>0
        switch lh1_rh2_bh3
            case 1
                fileNameToWrite = [labelFolder filesep 'labelsCS_LH.txt'];
            case 2
                fileNameToWrite = [labelFolder filesep 'labelsCS_RH.txt'];
            case 3
                fileNameToWrite = [labelFolder filesep 'labelsCS_BH.txt'];
            otherwise
                error('1-2-3 accepted');
        end
        fileAlreadyExist = exist(fileNameToWrite,'file');
        
        if  fileAlreadyExist && ~overwriteLabelFiles
            disp(['file(' fileNameToWrite ') exist hence it is not overwritten.']);
            return
        end
        
        compSys = getVariableByComputerName('systemType');
        switch compSys
            case 'Windows'
                [fileID, message] = fopen(fileNameToWrite,'w','n','UTF-8');
            otherwise%case 'Ubuntu'
                [fileID, message] = fopen(fileNameToWrite,'w','n','windows-1258'); 
        end
        if fileID>0
            fprintf(fileID,'%d\r\n',vecTowrite);
            fclose(fileID);
        else
            disp(['file could not be opened..msg(' message ')']);
            dbstop
        end
        if fileAlreadyExist
            disp(['file(' fileNameToWrite ') is overwritten.']);
        else
            disp(['file(' fileNameToWrite ') is written.']);
        end
    end 
end
