function [ croppedIm ] = zealousCrop( imOrReadPath, OPS )
    if isnumeric(imOrReadPath)
        a = imOrReadPath;
    elseif ischar(imOrReadPath)
        a = imread(imOrReadPath);
    else
        error(['unknown class(' class(imOrReadPath) ') for imOrReadPath']);
    end
    
    
    if ~exist('OPS', 'var') || ~isstruct(OPS)
        OPS = [];
    end
    figID = getStructField(OPS, 'figID', []);
    imWritePath = getStructField(OPS, 'imWritePath', '');
    overWrite = getStructField(OPS, 'overWrite', false);
    channelSize = size(a,3);

    if numel(size(a))==3
        a_summed = sum(a,3);
    elseif numel(size(a))==2
        a_summed = a;
    end
    
    [rowCnt, colCnt] = size(a_summed);
    rowSum = sum(a_summed);
    whitePixRow = rowCnt*255*channelSize; 
    whiteRows = rowSum==whitePixRow;
    colSum = sum(a_summed,2);
    whitePixCol = colCnt*255*channelSize; 
    whiteCols = colSum==whitePixCol;
    
    croppedIm = a;
    croppedIm(whiteCols,:,:) = [];
    croppedIm(:,whiteRows,:) = [];
    
    if ~isempty(figID)
        figure(figID);
        clf;
        imshow(croppedIm);
    end
    
    if ~strcmpi(imWritePath,'')
        try
            imwrite(corppedIm, imWritePath);
        catch err
            disp(['err(' err.message ') occured while writing to <' imWritePath '>.']);
        end
    end
    if overWrite
        if ischar(imOrReadPath) && exist(imOrReadPath,'file')==2
            try
                imwrite(croppedIm, imOrReadPath);
            catch err
                disp(['err(' err.message ') occured while writing to <' imWritePath '>.']);
            end
        else
            warning(['overWrite set to true but imOrReadPath(class = ' class(imOrReadPath) ') is not a path']);
        end
    end
    
end

