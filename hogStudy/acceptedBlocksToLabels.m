function acceptedBlocksToLabels(srcFolder, destFolder, acceptedBlocksAll, newLabels, handsStruct, medoidIDsOdCluster)
%acceptedBlocksToLabels using the output of
%setLabelsFromClusterAnalysisResults this function creates the folders when
%necessary and creates the labelFiles to ve used in web application

	rowCnt = size(acceptedBlocksAll,1);
    emptyBeg = '  ';
    %col defs - [  1   2    3       4            5          6        7    ]
    %            sign user rep lh1_rh2_bh3   fromFrame  toFrame  clusterID
    
    surDef = acceptedBlocksAll(:,1)*10000 + acceptedBlocksAll(:,2)*100 + acceptedBlocksAll(:,3);
    uniqSignID = unique(acceptedBlocksAll(:,1));
    uniqLRBH = unique(acceptedBlocksAll(:,4));
    [uniqSurDef, ~, ic] = unique(surDef);
    
    differentvideoCount = length(uniqSurDef);
    for dv = 1:differentvideoCount
        surCur = uniqSurDef(dv);
        relatedRowIDs = find(ic==dv);
        
        s = floor(surCur/10000);
        u = floor((surCur-s*10000)/100);
        r = ((surCur-s*10000)-u*100);
        disp(['for sign(' num2str(s) '),user(' num2str(u) '),repeatition(' num2str(r) ')']);
        disp([emptyBeg 'there are ' num2str(length(relatedRowIDs)) ' parts']);
        acceptedBlocks_sur = [acceptedBlocksAll(relatedRowIDs,:), reshape(newLabels(acceptedBlocksAll(relatedRowIDs,end)),[],1)];
        disptable(sortrows(acceptedBlocks_sur,[4 5]),'sign|user|rep|lrb|frFr|toFr|unsuperVisedClusterID|mappedClusterIDs');
        
        uniqClustersOfVideo = unique(acceptedBlocks_sur(:,7));
        disp([emptyBeg 'unique clusterIDs assigned are ' mat2str(uniqClustersOfVideo)]);
        disp([emptyBeg 'mapped unique clusterIDs assigned are ' mat2str(unique(newLabels(uniqClustersOfVideo)))]);
        
        imgCnt = getSrcFoldAndImgCnt(s,u,r,srcFolder);
        theFoldName = checkAndCreateDestFolder(s,u,r,destFolder,'    ');
        fwf_writeLabelVecToFile(theFoldName, imgCnt, acceptedBlocks_sur, 1,'      ');
        fwf_writeLabelVecToFile(theFoldName, imgCnt, acceptedBlocks_sur, 2,'      ');
        fwf_writeLabelVecToFile(theFoldName, imgCnt, acceptedBlocks_sur, 3,'      ');
    end
    
    
    %this function also needs to create the names of the clusters
    %meaning - checking the newLabels and the medoid information
    %it needs to create : (clustersCS_Single.txt and clustersCS_Both.txt)
    % 1+tekElSabit+7+2+1+RH+5
    % 2+acil01+7+2+1+LH+32
    % 3+acil02+7+2+1+RH+30
    % 4+tekElSoru+7+2+1+LH+46
    % 
    % 
    % 
    % 
    % 
    % 
    % 
    % 
    if length(uniqSignID)>1 || nargin<=4
        return
    end
    clusterFilesFolderName = checkAndCreateClusterTxtFolder(uniqSignID,destFolder,'      ');
    if length(uniqLRBH)==2
        %singleHand
        fileNameClusterTxt = [clusterFilesFolderName filesep 'clustersCS_Single.txt'];
    elseif length(uniqLRBH)==1 && uniqLRBH==3
        %both hands
        fileNameClusterTxt = [clusterFilesFolderName filesep 'clustersCS_Both.txt'];
    end
    
    fileID = fopen(fileNameClusterTxt,'w');
    newUniqLabels = unique(newLabels);
    clusterNameCell = cell(12,1);
    for k=newUniqLabels
        figure(88);clf;
        %how many original labels does it have
        assignedOldLabelIDs = find(newLabels==k);
        for i = 1:length(assignedOldLabelIDs)
            subplot(1,length(assignedOldLabelIDs),i);
            detLab = handsStruct.detailedLabels(medoidIDsOdCluster(assignedOldLabelIDs(i)),:);
            medoidIm = retrieveImageFromDetailedLabels(srcFolder, 1, detLab);
            switch(detLab(4))
                case 1
                    handStr = 'LH';
                case 2
                    handStr = 'RH';
                case 3
                    handStr = 'BH';
                otherwise
                    error('either 1-2 or 3');
            end
            imStr = [num2str(k) '-s(' num2str(detLab(1)) '),u(' num2str(detLab(2)) '),r(' num2str(detLab(3)) '),' handStr ',f(' num2str(detLab(5)) ')'];
            imshow(medoidIm);
            title(imStr);
        end
        str = input(['Please write cluster name for k(' num2str(i) ') which matches the found clusters' mat2str(assignedOldLabelIDs) '.'],'s');
        clusterNameCell{k} = [num2str(k) '+' str '+' num2str(detLab(1)) '+' num2str(detLab(2)) '+' num2str(detLab(3)) '+' handStr '+' num2str(detLab(5))];
        fprintf(fileID,'%s\n',clusterNameCell{k});
    end
    for k=1+length(newUniqLabels):12
        fprintf(fileID,'%s\n','');
    end
    fclose(fileID);
end

function fwf_writeLabelVecToFile(theFoldName, imgCnt, acceptedBlocksOfVideo, lh1_rh2_bh3, emptyBeg)
    slctdHandRows = find(acceptedBlocksOfVideo(:,4)==lh1_rh2_bh3);
    if ~isempty(slctdHandRows)
        switch lh1_rh2_bh3
            case 1
                fileNameToWrite = [theFoldName filesep 'labelsCS_LH.txt'];
            case 2
                fileNameToWrite = [theFoldName filesep 'labelsCS_RH.txt'];
            case 3
                fileNameToWrite = [theFoldName filesep 'labelsCS_BH.txt'];
            otherwise
                error('1-2-3 accepted');
        end
        vecTowrite = zeros(imgCnt,1);
        
        disp([emptyBeg 'Write into file(' fileNameToWrite ')']);
        disptable(sortrows(acceptedBlocksOfVideo(slctdHandRows,:),5),'sign|user|rep|lrb|frFr|toFr|unsuperVisedClusterID|mappedClusterIDs');      
        for i=1:length(slctdHandRows)
            r = slctdHandRows(i);
            curRow = acceptedBlocksOfVideo(r,:);
            vecTowrite(curRow(5):curRow(6)) = curRow(8);
        end
        
        fileID = fopen(fileNameToWrite,'w','n','windows-1258');
        fprintf(fileID,'%d\r\n',vecTowrite);
        fclose(fileID);
    end 
end

function [imgCnt, theFoldName] = getSrcFoldAndImgCnt(s,u,r,srcFolder)
    imgCnt = 0;
    theFoldName = srcFolder;
    %into sign folder
    theFoldName = [theFoldName filesep num2str(s) ]; %#ok<*AGROW>
    if ~exist(theFoldName, 'dir')
        disp(['folder(' theFoldName ') doesnt exist'])
        theFoldName = '';
        return
    end
    %into user-repeat folder
    theFoldName = [theFoldName filesep 'User_' num2str(u) '_' num2str(r)];
    if ~exist(theFoldName, 'dir')
        disp(['folder(' theFoldName ') doesnt exist'])
        theFoldName = '';
        return
    end
    [~, imgCnt] = getFileList([theFoldName filesep 'cropped'], '.png', '', false);
    imgCnt = floor(imgCnt/3);
end

function theFoldName = checkAndCreateDestFolder(s,u,r,destFolder,emptyBeg)
    theFoldName = destFolder;
    %into sign folder
    theFoldNameNew = [theFoldName filesep num2str(s) ]; %#ok<*AGROW>
    if ~exist(theFoldNameNew, 'dir')
        disp([emptyBeg 'creating folder(' num2str(s) ') under (' theFoldName ')']);
        mkdir(theFoldNameNew);
    end
    theFoldName = theFoldNameNew;
    %into user-repeat folder
    theFoldNameNew = [theFoldNameNew filesep 'User_' num2str(u) '_' num2str(r)];
    if ~exist(theFoldNameNew, 'dir')
        disp([emptyBeg 'creating folder(User_' num2str(u) '_' num2str(r) ') under (' theFoldName ')']);
        mkdir(theFoldNameNew);        
    end
    theFoldName = theFoldNameNew;
    
    %labelFiles
    theFoldNameNew = [theFoldName filesep 'labelFiles'];
    if ~exist(theFoldNameNew, 'dir')
        disp([emptyBeg 'creating folder(labelFiles) under (' theFoldName ')']);
        mkdir(theFoldNameNew);        
    end
    theFoldName = theFoldNameNew;
end

function theFoldName = checkAndCreateClusterTxtFolder(s,destFolder,emptyBeg)
    theFoldName = destFolder;
    %into sign folder
    theFoldNameNew = [theFoldName filesep num2str(s) ]; %#ok<*AGROW>
    if ~exist(theFoldNameNew, 'dir')
        disp([emptyBeg 'creating folder(' num2str(s) ') under (' theFoldName ')']);
        mkdir(theFoldNameNew);
    end
    theFoldName = theFoldNameNew;
end

