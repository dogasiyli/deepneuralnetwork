function clustersStruct = addOrCreateCluster(clustersStruct, clusterProposalFresh , clusterDataSetAdditionStruct)
    numOfSamples = size(clusterDataSetAdditionStruct.labels,1);
    featsToCluster = clusterDataSetAdditionStruct.hogFeats;
    
    %0. load the clusters struct if it only has a fileName attached
    [clustersStruct, clusterFileNameStr] = loadClusterStruct(clustersStruct);
    if isempty(clustersStruct) && ~exist(clusterFileNameStr,'file')
        clustersStruct = initalizeClusterStruct(clusterFileNameStr);
    end
    assert(isfield(clustersStruct,'dataset'),'<dataset> must be a field of clustersStruct');
    assert(isfield(clustersStruct,'definition'),'<definition> must be a field of clustersStruct');
    assert(isfield(clustersStruct,'model'),'<definition> must be a field of clustersStruct');
    
    %1. load the cluster dataset which has
    %.labels
    %.hogFeats
    %.imagesFineCropped
    %dataset = clustersStruct.dataset;
    
    [knownClusterID, newClusterID] = checkClusterName(clustersStruct.definition.clusterNames, clusterProposalFresh.clusterName);
    if newClusterID>0
        clusterID = newClusterID;
    elseif knownClusterID>0
        clusterID = knownClusterID;
    end
    
    %2. append the new <clusterDataSetAdditionStruct> into the
    %clusterDataset and get the indices that these are appended into.
    %the samples would already be in labels - so we shall not be adding
    %them again. they will be removed before trying to add the new samples
    %into the clusters they belong to
    [clustersStruct, theAssignedIndices, addSampleBool, removeSampleIndsToChangeClusterID] = appendDataNewDataToDataSet(clustersStruct, clusterDataSetAdditionStruct, clusterID);
    
    sampleCount2BeAdded = sum(addSampleBool==1);
    if sampleCount2BeAdded==0
        disp([num2str(0) ' new samples to add different clusters and subclusters']);
        return;
    end
    
    %3. add each new sample to a subcluster of a cluster
    
    %4. if this is not a known cluster and a brand new one,
    %   run divideSamplesInto2ClusterByDistance to initialize the first
    %   subcluster of this cluster
    
    if newClusterID>0
        [resultStruct] = divideSamplesInto2ClusterByDistance(featsToCluster);
        %subCluster_01 = resultStruct.groupsStructArr(1);
        %subCluster_02 = resultStruct.groupsStructArr(2);
        
        allowedDist = min([mean(resultStruct.maxDistVec([2 3])) clusterProposalFresh.maxMatchValue]);
        
        centerSampleID_01 = theAssignedIndices(resultStruct.groupsStructArr(1).centerSampleID);
        centerSampleID_02 = theAssignedIndices(resultStruct.groupsStructArr(2).centerSampleID);
        
        clustersStruct = createNewCluster(clustersStruct, newClusterID, 1, clusterProposalFresh, centerSampleID_01, allowedDist);
        clustersStruct = createNewCluster(clustersStruct, newClusterID, 2, clusterProposalFresh, centerSampleID_02, allowedDist);
        addSampleBool([resultStruct.groupsStructArr.centerSampleID]) = false;%already added
        
    elseif knownClusterID>0
        allowedDist = clusterProposalFresh.maxMatchValue;
    end
    
    changesInClusters = [];
    for  i = 1:numOfSamples
        if addSampleBool(i)
            sampleId = theAssignedIndices(i);
            hogFeat_asHoGVec  = clustersStruct.dataset.hogFeats(sampleId,:);
            clusterSuitabilityVec = findBestClusterIDVec(clustersStruct, hogFeat_asHoGVec, clusterID);
            [clustersStruct, resultSummary] = addSampleToCluster(clustersStruct, clusterSuitabilityVec, sampleId, allowedDist);
            
            curLen = size(changesInClusters,2);
            if curLen<resultSummary(2)
                changesInClusters = [changesInClusters zeros(1,resultSummary(2)-curLen)];
                clear curLen
            end
            try
                changesInClusters(resultSummary(2)) = changesInClusters(resultSummary(2)) + 1; %#ok<*AGROW>
            catch
                warning(['changesInClusters(' num2str(resultSummary(2)) ') - could not be incremented']);
            end
        end
    end
    if ~isempty(removeSampleIndsToChangeClusterID)
        clustersStruct.dataset.assignments(removeSampleIndsToChangeClusterID,:) = [];
        clustersStruct.dataset.hogFeats(removeSampleIndsToChangeClusterID,:) = [];
        clustersStruct.dataset.imagesFineCropped(removeSampleIndsToChangeClusterID,:) = [];
        clustersStruct.dataset.labels(removeSampleIndsToChangeClusterID,:) = [];
    end
    changedSubClusterIDs = find(changesInClusters>0);
    numberOfSubGroupChanged = length(changedSubClusterIDs);
    if changedSubClusterIDs>0
        subClusterChangesSummaryTable = [clusterID*ones(numberOfSubGroupChanged,1),reshape(changedSubClusterIDs,numberOfSubGroupChanged,1), reshape(changesInClusters(changedSubClusterIDs),numberOfSubGroupChanged,1)];
        disptable(subClusterChangesSummaryTable,'clusterID|subClusterID|sampleCountAdded'); 
    end
    
    clustersStruct = sortClusters(clustersStruct);
    
    disp([num2str(sampleCount2BeAdded) ' number of samples added to different clusters and subclusters']);
    if sampleCount2BeAdded>0
        save(clustersStruct.clusterFileNameStr, 'clustersStruct');
    end
end

function [knownID, newID] = checkClusterName(knownNamesCellArray, newlyAssignedClusterName)
    if isempty(knownNamesCellArray)
        knownID = -1;
        newID = 1;
        return
    end
    
    %idx = find(strcmp([knownNamesCellArray{:}], newlyAssignedClusterName));
    idx = [];
    for i = 1:length(knownNamesCellArray)
        if strcmp([knownNamesCellArray{i,1}], newlyAssignedClusterName)
            idx = i;
            break;
        end
    end

    if isempty(idx)
        knownID = -1;
        newID = length(knownNamesCellArray)+1;
    else
        knownID = idx;
        newID = -1;
    end
end

function clustersStruct = initalizeClusterStruct(clusterFileNameStr)
    %this is the first time this function is being called
    %create the file by saving it to the disk
    %warn the user about this file creation
    %assign empty vectors to all place..
    %but add <dataset> as is in <clusterDataSetAdditionStruct>
    clustersStruct = struct;

    clustersStruct.clusterFileNameStr = clusterFileNameStr;

    clustersStruct.dataset = struct;
    clustersStruct.dataset.labels = [];%clusterDataSetAdditionStruct.labels;
    clustersStruct.dataset.hogFeats = [];%clusterDataSetAdditionStruct.hogFeats;
    clustersStruct.dataset.imagesFineCropped = [];%clusterDataSetAdditionStruct.imagesFineCropped;
    clustersStruct.dataset.assignments = [];%[1_datasetID, clusterID, subClusterID, distToCenterSample]

    clustersStruct.definition = struct;
    clustersStruct.definition.clusterNames = [];%{clusterProposalFresh.clusterName};%will inrease the cell length when added
    clustersStruct.definition.clusterTypes = [];%{clusterProposalFresh.clusterType};%will inrease the cell length when added
    clustersStruct.definition.clusterImage = [];%clusterProposalFresh.clusterImage;%initially set the cluster image from this, later check the <clusterProposalFresh.minMatchValue>
    clustersStruct.definition.subClusterImages = [];%will be taken from the center images of subclusters <clusterDataSetAdditionStruct.imagesFineCropped(i)>
    clustersStruct.definition.maxDistList = [];%[clusterID clusterSubID maxDistOfPairs]
    clustersStruct.definition.uniqueSignIDs = [];%{clusterProposalFresh.differentSignIDs};

    clustersStruct.model = struct;
    clustersStruct.model.clusterDefinitionMat = [];
    clustersStruct.model.centerVecMat= [];%each row is the assigned HoG vector of the subcluster
end