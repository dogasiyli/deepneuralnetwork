function [clustersStruct, resultSummary] = addSampleToCluster(clustersStruct, clusterSuitabilityVec, sampleId, allowedDist)
    %dataset, definition, model
    %hogFeat_asHoGVec  = clustersStruct.dataset.hogFeats(sampleId,:);
    %[clusterID, subClusterID, allowedDist, sampleDistanceToCenterVec, lastUsedSubClusterIDOfCluster]
    if isnan(clusterSuitabilityVec(5))
        %putBreakPointAhead(2, 'dbstop', [3 4]);
        %we can simply add this sample to -clusterID, subClusterID- found
        clustersStruct.dataset.assignments = [clustersStruct.dataset.assignments;sampleId clusterSuitabilityVec([1 2 4])];%[1_datasetID, clusterID, subClusterID, distToCenterSample]
        signID = clustersStruct.dataset.labels(sampleId,1);
        newClusterID = clusterSuitabilityVec(1);
        subClusterID = clusterSuitabilityVec(2);
        clustersStruct.definition.uniqueSignIDs{newClusterID, subClusterID} = unique([clustersStruct.definition.uniqueSignIDs{newClusterID, subClusterID} signID]);
        resultSummary = [newClusterID subClusterID 0];%3rd col is if new subcluster
        %putBreakPointAhead(-5, 'dbclear', [3 4]);
    else
        %putBreakPointAhead(2, 'dbstop', [3 4]);
        %we need to create a new subClusterID in clusterID
        newClusterID = clusterSuitabilityVec(1);
        subClusterID = clusterSuitabilityVec(5);
        clusterProposalFresh = struct;
        clusterProposalFresh.differentSignIDs = clustersStruct.dataset.labels(sampleId,1);
        centerSampleID = sampleId;
        %putBreakPointAhead(-5, 'dbclear', [3 4]);
        allowedDist = min(max(allowedDist,30),50);
        clustersStruct = createNewCluster(clustersStruct, newClusterID, subClusterID, clusterProposalFresh, centerSampleID, allowedDist);
        resultSummary = [newClusterID subClusterID 1];%3rd col is if new subcluster
    end
end