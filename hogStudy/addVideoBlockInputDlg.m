function [clusterID, frameArea] = addVideoBlockInputDlg(signSourceFolder, surVec, knownClusterNameOrID, clusterType, clusterDataSetAdditionStruct, clusterInitStr, frameArea, signIDs)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
clusterProposalStruct
%% 1. showing current clusters
    clustersStruct = [signSourceFolder filesep 'clusters_' num2str(surVec(1),'%03d') '.mat'];
    clustersStruct = loadClusterStruct(clustersStruct);
    
    curClusterNamesCell = clustersStruct.definition.clusterNames;
    [clusterID, clusterName] = findClusterNameAndIDs(knownClusterNameOrID, curClusterNamesCell);

%% 2. filling <clusterProposalFresh>
    %create as if this is a new cluster center
    clusterProposalFresh = struct;
    clusterProposalFresh.clusterName = clusterName;
    clusterProposalFresh.clusterType = clusterType;%single or both
    %clusterProposalFresh.clusterImage = [clusterProposalStruct.bestCropStruct.imCropped{1},clusterProposalStruct.bestCropStruct.imCropped{2}];
    %clusterProposalFresh.minMatchValue = minHOGFeatDist;
    %clusterProposalFresh.maxMatchValue = maxHOGFeatDist;
    clusterProposalFresh.differentSignIDs = surVec(1);
    
    
    clusterDataSetAdditionStruct
    
    knownClusterNameDefault = '';
    if ~isempty(clustersStruct) && isstruct(clustersStruct)
        curClusterNamesCell = clustersStruct.definition.clusterNames;
        definedClusterCount = size(curClusterNamesCell,1);
        clusterCountToPropose = min(3,definedClusterCount);
        
        %what we have in clusterProposalStruct??
        %1. clusterProposalStruct.bestCropStruct.hogVec - 900x1
        %2. clusterProposalStruct.bestCropStruct.imCropped - 1x2 cell - 200x200x3 uint8
        %3. clusterProposalStruct.bestCropStruct.imFineCropped - 1x2 cell - D by D uint8
        
        %we can get hog features of imCropped and pass as the most
        %important hog fetaures to guess cluster - can be more accurate
        %or the already selected hogVec as 2 hog - fast
        
        hogFeat_X = clusterProposalStruct.bestCropStruct.hogVec(:,1)';
        [clusterProbsVec_X, bestMatchFineCrop]= getSimilarityPerCluster(clustersStruct, hogFeat_X, clusterCountToPropose);
        bestMatchXFineCrop = bestMatchFineCrop{1,1};
        
        hogFeat_Y = clusterProposalStruct.bestCropStruct.hogVec(:,2)';
        [clusterProbsVec_Y, bestMatchFineCrop]= getSimilarityPerCluster(clustersStruct, hogFeat_Y, clusterCountToPropose);
        bestMatchYFineCrop = bestMatchFineCrop{1,1};
        
        %if both X and Y are assigned to same cluster as the first guesses
        %write the cluster name as the knownClusterName
        if clusterProbsVec_X(1,1)==clusterProbsVec_Y(1,1) && max([clusterProbsVec_X(:,end);clusterProbsVec_Y(:,end)])>0.8
            knownClusterNameDefault = clustersStruct.definition.clusterNames{clusterProbsVec_X(1,1)};
        end

        plotClusterProposes(clusterProbsVec_X, clusterCountToPropose, curClusterNamesCell(clusterProbsVec_X(1:clusterCountToPropose,1)), 61, 1, 'vidX', bestMatchXFineCrop);
        plotClusterProposes(clusterProbsVec_Y, clusterCountToPropose, curClusterNamesCell(clusterProbsVec_Y(1:clusterCountToPropose,1)), 61, 2, 'vidY', bestMatchYFineCrop);
        
        %show the first 3 possible clusters to be selected,

        %here we need to display:
        %
        clusterIDsToView = unique([clusterProbsVec_X(1:clusterCountToPropose,1) clusterProbsVec_Y(1:clusterCountToPropose,1)]);
        displayClusterStructInfo(clustersStruct, clusterIDsToView, 62);
        
        drawnow;
    end
    curClusterCnt = length(curClusterNamesCell);
    switch clusterInitStr
        case {'L','R'}%single hand cluster
            clusterType = 'Single';
        case 'B'%both hands cluster
            clusterType = 'Both';
    end
    
%% 2. dialog box
%HALF COMPLETED
    prompt = {'Enter either Known Cluster name or ID:','Enter new Cluster Name:','X-Accept area', 'BestCenterImageID'};
    dlg_title = 'AddTo-Create Cluster ??';
    num_lines = 1;
    xFrCnt = frameArea(2)-frameArea(1)+1;
    xAccepted_def = ['1-' num2str(xFrCnt)];
    bestCenterImageID = round(xFrCnt/2);
    defaultans = {knownClusterNameDefault, [clusterType num2str(curClusterCnt+1,'%03d')],xAccepted_def,bestCenterImageID};
    answer = inputdlg(prompt,dlg_title,num_lines,defaultans);  
        
    %according to the answer
    %1-this operation will be discarded
    %2-or this will be added to an existing cluster
    %3-or a new cluster will be created or

%% 3. result of dialog box and setClusterID, setClusterName, isNewCluster
%ALMOST COMPLETED
    [setClusterID, knownClusterNameOrID, frameArea, pixLim, bestCenterImageID] = clusterDialogInterpretXY(curClusterNamesCell, frameArea, answer);
    if isempty(answer)
        frameArea = [];
        clusterProposalFresh = [];
        clusterDataSetAdditionStruct = [];
        return;
    end
%% 4. retrieving important info from <clusterProposalStruct, >
%COMPLETED
    %select the hog feats from the approved area
    nExpected = frameArea(2)-frameArea(1)+1;
    if selectedColCntIsOne%size(hogFeatsSelected,1) ~= nExpected
        hogFeatsSelected = cell2mat(cellfun( @(sas) sas.hogVec, clusterProposalStruct.bestCropCell(selectedHOGFeatCells), 'uni', false ));
        if size(hogFeatsSelected,2) ~= 900 && size(hogFeatsSelected,1) ~= 900
            hogFeatsSelected = cell2mat(cellfun( @(sas) sas.hogVec', clusterProposalStruct.bestCropCell(selectedHOGFeatCells), 'uni', false ));
        end
    end
    %sas.hogVec = D by N = 900 x 2
    if size(hogFeatsSelected,2) == nExpected && size(hogFeatsSelected,1) == 900
        hogFeatsSelected = hogFeatsSelected';%N by D
    end
    assert(size(hogFeatsSelected,1) == nExpected && size(hogFeatsSelected,2) == 900,'sth wrong');

    imagesSelected_X = cell2mat(cellfun( @(sas) reshape(imresize(sas.imFineCropped{1,1},[200 200]),[1,120000]), clusterProposalStruct.bestCropCell(selectedHOGFeatCells), 'uni', false ));
    if size(imagesSelected_X,1) ~= 120000 && size(imagesSelected_X,2) ~= 120000
        imagesSelected_X = cell2mat(cellfun( @(sas) reshape(imresize(sas.imFineCropped{1,1},[200 200]),[120000,1]), clusterProposalStruct.bestCropCell(selectedHOGFeatCells), 'uni', false ));
    end
    %120K by N
    if size(imagesSelected_X,2) == nExpected/2 && size(imagesSelected_X,1) == 120000
        imagesSelected_X = imagesSelected_X';%N by 120K
    end
    assert(size(imagesSelected_X,1) == nExpected/2 && size(imagesSelected_X,2) == 120000,'sth wrong');
    
    %needs to be nExpected by 4
    %shouldnt be nExpected/2 by 8
    % fineCropInfoSelectedFrames = zeros(nExpected,4);
    % j = 1;
    % for ri = 1 : size(selectedHOGFeatCells,1)
    %     for cj = 1 : size(selectedHOGFeatCells,2)
    %         if selectedHOGFeatCells(ri,cj)
    %             fineCropInfoSelectedFrames(j:j+1,:) = clusterProposalStruct.bestCropCell(ri,cj).fineCropInfo;
    %             j = j+2;
    %         end
    %     end
    % end
    fineCropInfoSelectedFrames = cell2mat(cellfun( @(sas) sas.fineCropInfo, clusterProposalStruct.bestCropCell(selectedHOGFeatCells), 'uni', false ));
    if size(fineCropInfoSelectedFrames,1) ~= nExpected || size(fineCropInfoSelectedFrames,2) ~= 4
        fineCropInfoSelectedFrames = cell2mat(cellfun( @(sas) sas.fineCropInfo', clusterProposalStruct.bestCropCell(selectedHOGFeatCells), 'uni', false ));
    end
    if size(fineCropInfoSelectedFrames,2) == nExpected && size(fineCropInfoSelectedFrames,1) == 4
        %putBreakPointAhead(1, 'dbstop', [3 3]);
        fineCropInfoSelectedFrames = fineCropInfoSelectedFrames';%N by 4
        %putBreakPointAhead(-1, 'dbclear', [3 3]);
    end
    assert(size(fineCropInfoSelectedFrames,1) == nExpected && size(fineCropInfoSelectedFrames,2) == 4,'sth wrong');
    
    
%% 6. filling <clusterDataSetAdditionStruct>
    selectedCoupleCount = sum(selectedHOGFeatCells(:)==1);
    %N by 120K
    imagesFineCropped = [imagesSelected_X];
    hogFeatShuffleVec = [(1:2:(2*selectedCoupleCount - 1))';(2:2:2*selectedCoupleCount)'];
    hogFeatsSelected = hogFeatsSelected(hogFeatShuffleVec,:);%hogFeatsSelected were going like : X row 1, Y row 1, X row 2, Y row 2
    fineCropInfoSelectedFrames = fineCropInfoSelectedFrames(hogFeatShuffleVec,:);
    %hogFeats are assigned to which samples of X and Y ??
    [frameIDs_X,frameIDs_Y] = find(selectedHOGFeatCells==1);
    frameIDs_X = frameIDs_X + frameArea(1) - 1;
    frameIDs_Y = frameIDs_Y + frameArea(3) - 1;
    %imagesFineCropped and hogFeats are going like:
    %X row 1:N, Y row 1:N
    %labels = zeros(2*selectedCoupleCount,9);
    %sign user rep frame fineCropArea(5-6-7-8) lh1_rh2_bh3
    col01_signID = [signIDs(1)*ones(selectedCoupleCount,1);signIDs(2)*ones(selectedCoupleCount,1)];
    userIDs = clusterProposalStruct.vidXYInf(:,2);
    col02_userID = [userIDs(1)*ones(selectedCoupleCount,1);userIDs(2)*ones(selectedCoupleCount,1)];
    repIDs = clusterProposalStruct.vidXYInf(:,3);
    col03_repID = [repIDs(1)*ones(selectedCoupleCount,1);repIDs(2)*ones(selectedCoupleCount,1)];
    col04_frameID = [reshape(frameIDs_X,[],1);reshape(frameIDs_Y,[],1)];
    col05_08_fineCropReal = fineCropInfoSelectedFrames;
    switch clusterInitStr
        case 'LL'
            col09_lh1_rh2_bh3 = [1*ones(selectedCoupleCount,1);1*ones(selectedCoupleCount,1)];
        case 'RR'
            col09_lh1_rh2_bh3 = [2*ones(selectedCoupleCount,1);2*ones(selectedCoupleCount,1)];
        case 'RL'
            col09_lh1_rh2_bh3 = [2*ones(selectedCoupleCount,1);1*ones(selectedCoupleCount,1)];
        case 'LR'
            col09_lh1_rh2_bh3 = [1*ones(selectedCoupleCount,1);2*ones(selectedCoupleCount,1)];
        case 'BB'%both hands cluster
            col09_lh1_rh2_bh3 = [3*ones(selectedCoupleCount,1);3*ones(selectedCoupleCount,1)];
    end
    %I need the unique [signID userID repID frameID fineCropReal(1:4)],
    %hence we need to select those rows from the whole
    %<labels, hogFeatsSelected, imagesFineCropped>
    labels = [col01_signID,col02_userID,col03_repID,col04_frameID,col05_08_fineCropReal,col09_lh1_rh2_bh3];
    [~, uniqInds] = unique(labels(:,1:8),'rows');
    disp(['N(' num2str(size(labels,1)) ') cross-couple frames is down to M(' num2str(length(uniqInds)) ') frames after unique frame-crop selection operation']);
    
    clusterDataSetAdditionStruct = struct;
    clusterDataSetAdditionStruct.labels = labels(uniqInds,:);
    clusterDataSetAdditionStruct.hogFeats = hogFeatsSelected(uniqInds,:);
    clusterDataSetAdditionStruct.imagesFineCropped = imagesFineCropped(uniqInds,:);
end