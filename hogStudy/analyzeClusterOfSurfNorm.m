function [ distMat, medoidHist_K, clusterLabelsCell, handsStruct, confAccBestVec] = analyzeClusterOfSurfNorm(srcFold, surSlctListStruct, labelIDs, tryMax, kMax, gtLabelsGrouping)

    if ~exist('gtLabelsGrouping','var')
        gtLabelsGrouping = [];
    end
    
    single0_double1 = surSlctListStruct.single0_double1;    
    signID = surSlctListStruct.signIDList; 
    userList = surSlctListStruct.userList; 
    repeatIDVec = surSlctListStruct.maxRptID;  
    
    distMatInfoStruct = struct;
    distMatInfoStruct.featureName = 'sn';%surface normal
    distMatInfoStruct.cropMethod = 'mc';%multi crop
    distMatInfoStruct.distanceMethod = 'c3';%cosine distance of xyz vectors
    
    fileSaveName = createFileSaveName_DistMat(srcFold, surSlctListStruct, distMatInfoStruct);

    [handsStruct,problematicVids] = createDataSetFromLabels(srcFold, signID, userList, repeatIDVec, single0_double1, labelIDs);
    OPS_createDistMat_surfNormCells = struct('enforceRecreate',false,'useMethodID',1,'distMethod',distMatInfoStruct.distanceMethod);
    [distMat, cropIDs, mnDS, mxDS] = createDistMat_surfNormCells('sn', handsStruct, fileSaveName.distanceMatrixFile, OPS_createDistMat_surfNormCells); %#ok<ASGLU>

    N = size(distMat,1);%there are N number of images to be compared
    
    [distVals_sorted, inds_sorted] = sort(distMat(:));
    [im_i,im_j] = ind2sub(size(distMat),inds_sorted(1));

    figure(1);clf;pcolor(distMat);colorbar;
    title(['Min distance(' num2str(distVals_sorted(1)) ') is between ' num2str(im_i) ' and ' num2str(im_j) '.']);
    

    lh1_rh2_bh3_str = {'LH','RH','BH'};
    %lets draw first 8 and last 8 couples
    figure(2);clf;
    for i=1:4
        d = distVals_sorted(i);
        [im_i,im_j] = ind2sub(size(distMat),inds_sorted(i));
        
        dLab_i = handsStruct.detailedLabels(im_i,:);%detailedLabels [signID userID repID lh1_rh2_bh3 imageIDInVideo]
        dLab_j = handsStruct.detailedLabels(im_j,:);
        
        usrFold_i = ['User_' num2str(dLab_i(2)) '_' num2str(dLab_i(3))];
        usrFold_j = ['User_' num2str(dLab_j(2)) '_' num2str(dLab_j(3))];
        
        fName_i = [lh1_rh2_bh3_str{dLab_i(4)} '_' num2str(dLab_i(end-1),'%03d') ];
        fName_j = [lh1_rh2_bh3_str{dLab_j(4)} '_' num2str(dLab_j(end-1),'%03d') ];
        
        im_i_rgb = imread([srcFold filesep num2str(dLab_i(1)) filesep usrFold_i filesep 'cropped' filesep 'crop_' fName_i '.png']);
        im_j_rgb = imread([srcFold filesep num2str(dLab_j(1)) filesep usrFold_j filesep 'cropped' filesep 'crop_' fName_j '.png']);
        
        subplot(4,2,(i-1)*2+1);%1 3 5 7
        image(im_i_rgb);title(['im(' strrep(usrFold_i,'_','-') '_{' strrep(fName_i,'_','-') '}),min(' num2str(i) ',1),d(' num2str(d) ')']);
        subplot(4,2,i*2);%2 4 6 8
        image(im_j_rgb);title(['im(' strrep(usrFold_j,'_','-') '_{' strrep(fName_j,'_','-') '}),min(' num2str(i) ',2),d(' num2str(d) ')']);
    end
    
    % what if I try to form a group of samples to say - 
    % hey these guys are too much alike and can form a group
    % I can start by maintaining a group list - adding each too close
    % samples to each other 
    
    medoidHist_K_cell = zeros(kMax,kMax+1,tryMax);
    knownClusterCentersDist = zeros(kMax, N);
    clusterLabelsCell = cell(1,kMax);
    medoidCouplesCell = cell(1,kMax);
    medoidHist_Best = zeros(kMax,kMax+1);
    sampleIndsVecCell = cell(1,kMax);
    medoidHist_improve = [];
    wouldveQuitted = zeros(1,tryMax);
    for t=1:tryMax
        medoidHistCell = cell(1,kMax);
        medoidHist_K = zeros(kMax,kMax+1);
        for k=2:kMax
            if t>1
                ifMode = -1;
                [accDistVals, initialMedoids] = sort(knownClusterCentersDist(k,:));
                initialMedoids(accDistVals>=0) = [];
                if length(initialMedoids)==k
                	initialMedoids = randperm(N);
                	initialMedoids = initialMedoids(1:k);
                    ifMode = 1;
                elseif length(initialMedoids)>=k
                	initialMedoids = initialMedoids(1:k);
                    ifMode = 2;
                else
                    initialMedoidsAdd = 1:N;
                    initialMedoidsAdd(initialMedoids) = [];
                    initialMedoidsAdd = initialMedoidsAdd(randperm(length(initialMedoidsAdd)));
                    initialMedoids = sort([initialMedoids initialMedoidsAdd(1 : k-length(initialMedoids))]);
                    ifMode = 3;
                end
            else
                initialMedoids = [];
            end
            if ~isempty(initialMedoids) && length(initialMedoids)~=k
                initialMedoidsUse = initialMedoids;
            else
                initialMedoidsUse = initialMedoids;
            end
            [clusterLabels_cur, medoidHistCell{1,k}, sampleIndsVec_cur, medoidCouples_cur] = kmedoidsDG(distMat, struct('iterMax',50, 'initialMedoids', initialMedoidsUse,'clusterCount', k, 'figID', -1));
            bestMedoidRow = sortrows(medoidHistCell{1,k}, size(medoidHistCell{1,k},2));
            bestMedoidRow = bestMedoidRow(1,:);

            curMedoids = bestMedoidRow(1:end-1);
            curEnergy = bestMedoidRow(end);
            medoidHist_K(k, kMax-length(curMedoids)+1 : kMax+1) = [sort(curMedoids) curEnergy];
            
            curMedoids(curMedoids<1) = [];
            knownClusterCentersDist(k,curMedoids) = min([knownClusterCentersDist(k,curMedoids) ; ones(size(curMedoids))*curEnergy]);
            
            if medoidHist_Best(k,end)>medoidHist_K(k,end)
                medoidHist_Best(k,:) = medoidHist_K(k,:);
                clusterLabelsCell{1,k} = clusterLabels_cur;
                sampleIndsVecCell{1,k} = sampleIndsVec_cur;
                medoidCouplesCell{1,k} = medoidCouples_cur;
                medoidHist_improve = [medoidHist_improve;t k medoidHist_Best(k,:)]; %#ok<AGROW>
                disptable([t k medoidHist_Best(k,:)]);
            end
        end
        %disptable(medoidHist_K)
        medoidHist_K_cell(:,:,t) = medoidHist_K;
        if t>1
            if sum(sum(abs(medoidHist_K_cell(:,:,t)-medoidHist_K_cell(:,:,t-1))))==0
                knownClusterCentersDist = zeros(kMax, N);
                wouldveQuitted(t) = 1;
                %break
            end
        end
    end
    disptable(medoidHist_improve);
    disptable(medoidHist_Best);
    r_t =  getAllPossibleDivisors(length(wouldveQuitted));
    wouldveQuitted = reshape(wouldveQuitted,r_t(end-1),[]);
    disp(mat2str(find(wouldveQuitted==1)))
    
    %each row if showing a clusters samples
    %col-i is the ith cluster center
    rC = kMax-1;
    cC = kMax;
    confAccBestVec = zeros(1,kMax);
    hSummary = figure(3);clf;hold on;
    for k=2:kMax %rows
        clusterLabels_cur = clusterLabelsCell{1,k};
        sampleIndsVec_cur = sampleIndsVecCell{1,k};
        medoidCouples_cur = medoidCouplesCell{1,k};              
        medoidIDsOdCluster = medoidHist_Best(k, kMax-k+1 : kMax);
        medoidIDsOdCluster(medoidIDsOdCluster<0) = [];
        clusterLabels_med1 = clusterLabels_cur(medoidIDsOdCluster);%-this is the correct way of checking which cluster the sample is assigned to
        disp(['cluster summary for k=' num2str(k) ' is :']);
        printClusterSummary(clusterLabels_cur);
        disp('Part of distance matrix showing cluster medoids distances:');
        disptable(distMat(medoidIDsOdCluster,medoidIDsOdCluster));
        %clusterLabels_med2 = clusterLabels_cur(sampleIndsVec_cur(medoidIDsOdCluster));
        imageCell = cell(1,k);
        [confMatBest, uniqueMappingLabels, confAccBestVec(k), outStruct] = groupClusterLabelsIntoGroundTruth(clusterLabelsCell{1,k}, handsStruct.detailedLabels(:,6), struct('gtLabelsGrouping', gtLabelsGrouping,'printResults', true,'figID', 1));    
        for im = 1:length(medoidIDsOdCluster) %cols
            %im_k = sampleIndsVec_cur(medoidIDsOdCluster(im));
            im_k = medoidIDsOdCluster(im);
            dLab_k = handsStruct.detailedLabels(im_k,:);%detailedLabels [signID userID repID lh1_rh2_bh3 imageIDInVideo]
            usrFold_k = ['User_' num2str(dLab_k(2)) '_' num2str(dLab_k(3))];
            fName_k = [lh1_rh2_bh3_str{dLab_k(4)} '_' num2str(dLab_k(end-1),'%03d') ];
            imageCell{im} = imread([srcFold filesep num2str(dLab_k(1)) filesep usrFold_k filesep 'cropped' filesep 'crop_' fName_k '.png']);
            figure(3);hold on;
            subplot(rC, cC, sub2ind([cC,rC],im,k-1));%row:k-1, col-im
            %cl_str = ['<' num2str(clusterLabels_med1(im)) ',' num2str(clusterLabels_med2(im)) '>'];
            usr_str = ['u(' num2str(dLab_k(2)) '-' num2str(dLab_k(3))];
            gv_str = [',g(' num2str(outStruct.clusterLabelsGroupingVec(im)) ')'];
            if im==1
                accStr = [',acc(' mat2str(confAccBestVec(k), 5) ')'];
            else
                accStr = '';
            end
            image(imageCell{im});title([usr_str '_{' strrep(fName_k,'H_','-') '}),k(' num2str(im) '-' num2str(k) ')' gv_str accStr]);
        end 
    end
    figureSaveName = createFileSaveName_surfNormDistMat(srcFold, handsStruct, signID, labelIDs, single0_double1);
    figureSaveName = strrep(figureSaveName, '.mat', '.png');
    saveas(hSummary, figureSaveName);
    
    %also I need to see the switching within videos
end

