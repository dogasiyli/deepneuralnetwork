function [distMat, clusterLabelsCell, handsStruct, confAccBest, clusterResultCells] = analyzeDistMat(surSlctListStruct, distMatInfoStruct, optParamStruct)
    %srcFold = '/media/dg/SSD_Data';
    %labellingStrategy ='fromAssignedLabels','assignedLabelsRemove1','assignedLabelsExcept1','fromSkeletonOnly','fromSkeletonStableAll';
    %surSlctListStruct = struct('labelingStrategyStr',labellingStrategy,'single0_double1', 0, 'signIDList',521,'userList',2:5,'maxRptID',1);
    %distMatInfoStruct = struct('featureName','hf','cropMethod','mc','distanceMethod','e3');
    %clusteringParams = struct('tryMax',2000,'kMax',10,'gtLabelsGrouping',[],'modeStr','kMedoids','groupClusterLabels',false);

    %tryMax = clusteringParams.tryMax;
    %kMax = clusteringParams.kMax;
    %gtLabelsGrouping = clusteringParams.gtLabelsGrouping;
    %single0_double1 = surSlctListStruct.single0_double1;    
    %signID = surSlctListStruct.signIDList; 
    %userList = surSlctListStruct.userList; 
    %repeatIDVec = surSlctListStruct.maxRptID;  
    %labelingStrategyStr = surSlctListStruct.labelingStrategyStr;
    %featName = distMatInfoStruct.featureName;%'sn';%surface normal
    %cropMethod = distMatInfoStruct.cropMethod;%'mc';%multi crop
    %distMethod = distMatInfoStruct.distanceMethod;%'c3';%cosine distance of xyz vectors
    
    if ~exist('optParamStruct', 'var') || ~isstruct(optParamStruct)
        optParamStruct = [];
    end
    srcFold                = getStructField(optParamStruct, 'srcFold', getVariableByComputerName('srcFold'));
    clusteringParams       = getStructField(optParamStruct, 'clusteringParams', struct('tryMax',5,'kMax',10,'gtLabelsGrouping',[],'modeStr','kMedoids','groupClusterLabels',false));
    hogVersion       	   = getStructField(optParamStruct, 'hogVersion', 1);
    enforceRecreateDistMat = getStructField(optParamStruct, 'enforceRecreateDistMat', false);
    enforceRecreateCluster = getStructField(optParamStruct, 'enforceRecreateCluster', false);
    checkForBetterCluster  = getStructField(optParamStruct, 'checkForBetterCluster', false);
    skipClustering         = getStructField(optParamStruct, 'skipClustering', false);

    askApprovalForDifferent_kVec = getStructField(optParamStruct, 'askApprovalForDifferent_kVec', false);
    deleteDataFile               = getStructField(optParamStruct, 'deleteDataFile', true);
    clusterResultFolder          = getStructField(optParamStruct, 'clusterResultFolder', []);
    saveAllClusterResultImages   = getStructField(optParamStruct, 'saveAllClusterResultImages', true);
    if isempty(clusterResultFolder)
        clusterResultFolder = setClusterResultFolder(srcFold, distMatInfoStruct);
    end
    diaryFullFileName      = getStructField(optParamStruct, 'diaryFullFileName', [clusterResultFolder filesep 'matlabScreenDiary.txt']);
    finalizeDiary          = getStructField(optParamStruct, 'finalizeDiary', true);
    deleteDiaryFirst       = getStructField(optParamStruct, 'deleteDiaryFirst', true);
    ignoreLabels           = getStructField(optParamStruct, 'ignoreLabels', false);
    
    if ~isfield(clusteringParams,'modeStr')
        clusteringParams.modeStr = 'kMedoids';%'spectralClustering';%
    end
    if ~isfield(clusteringParams,'groupClusterLabels')
        %decides wheter to group G's and K's into a unified "K"
        clusteringParams.groupClusterLabels = false;
    end
    
    if deleteDiaryFirst && exist(diaryFullFileName,'file')==2
        delete(diaryFullFileName);
    end
    diary(diaryFullFileName);
    clc;
    displayStructureSummary('surSlctListStruct',surSlctListStruct,0);
    displayStructureSummary('distMatInfoStruct',distMatInfoStruct,0);
    displayStructureSummary('clusteringParams',clusteringParams,0);

    [distMat, handsStruct, fileSaveName] =  createSpecificDistMat(srcFold, surSlctListStruct, distMatInfoStruct, struct('hogVersion',hogVersion,'enforceRecreate',enforceRecreateDistMat));
    if ~ignoreLabels && length(unique(handsStruct.detailedLabels(:,end)))>1
        clusteringParams.gtLabels = handsStruct.detailedLabels(:,end);
        clusteringParams.tryMax = getStructField(clusteringParams, 'tryMax', 5);
    end
    if ignoreLabels
        handsStruct.detailedLabels(:,end) = 1;
    end

    %displayDistMat_pcolor(distMat, 1);
    displayMinDistantElement([], distMat, 4, handsStruct, srcFold);
    
    if enforceRecreateCluster
        [medoidHist_Best, clusterLabelsCell] = runCluster(distMat, clusteringParams);
        save(fileSaveName.clusterResult,'medoidHist_Best','clusterLabelsCell');
    elseif ~skipClustering
        if exist(fileSaveName.clusterResult,'file')
            load(fileSaveName.clusterResult,'medoidHist_Best','clusterLabelsCell');
            
            kVec_cur = clusteringParams.kVec;
            kVec_old = reshape(find(medoidHist_Best(:,1)~=inf),1,[]); %#ok<NODEF>
            kVecChanged = (length(kVec_cur)~=length(kVec_old)) || (sum(abs(kVec_cur-kVec_old))>0);
            
            %keep the loaded results anyway
            medoidHist_Best_prev = medoidHist_Best;
            clusterLabelsCell_prev = clusterLabelsCell; %#ok<NODEF>
            
            if ~kVecChanged  && ~checkForBetterCluster
                %do nothing
            else
                %delete(fileSaveName.clusterResult);
                runCluster_completed = false;
                if kVecChanged  && ~checkForBetterCluster
                    %only kVecChanged
                    if askApprovalForDifferent_kVec
                        disp(['clusteringParams.kVec passed is ' mat2str(clusteringParams.kVec) ', but kVec_of_medoidHist_Best is ' mat2str(kVec_old)]);
                        str = input('would you like to run with new clusterParamskVec(new) or use the previous(old) : ','s');
                    else
                        str = 'new';
                    end
                    if strcmp(str,'new')
                        [medoidHist_Best, clusterLabelsCell] = runCluster(distMat, clusteringParams);
                        runCluster_completed = true;
                    else %if strcmp(str,'old')
                        disp(['kVec is assigned to ' mat2str(kVec_old)]);
                        clusteringParams.kVec = kVec_old;
                    end 
                elseif checkForBetterCluster %regardless of kVecChange
                    %only checkForBetterCluster
                    [medoidHist_Best, clusterLabelsCell] = runCluster(distMat, clusteringParams);
                    runCluster_completed = true;
                end
                
                if runCluster_completed
                    [medoidHist_Combined, clusterLabelsCells_Combined, kVec_combined] = combine_kMedoidResults(medoidHist_Best, clusterLabelsCell, medoidHist_Best_prev, clusterLabelsCell_prev);
                    clusteringParams.kVec = kVec_combined;
                    medoidHist_Best = medoidHist_Combined;
                    clusterLabelsCell = clusterLabelsCells_Combined;
                end
                tableToDisplayAndSave = [reshape(clusteringParams.kVec,[],1) reshape(100*(1-medoidHist_Best(clusteringParams.kVec,end)),[],1)];
                disptable(tableToDisplayAndSave,'clusterCount|accuracy');
                %whatever happened up, save the new results
                save(fileSaveName.clusterResult,'medoidHist_Best','clusterLabelsCell');
            end
        else
            [medoidHist_Best, clusterLabelsCell] = runCluster(distMat, clusteringParams);
            save(fileSaveName.clusterResult,'medoidHist_Best','clusterLabelsCell');
        end 
    else
        load(fileSaveName.clusterResult,'medoidHist_Best','clusterLabelsCell');
        clusteringParams.kVec = find(medoidHist_Best(:,end)~=inf)';
    end

    [handsStruct.detailedLabels, signCount, labelsCount] = combineDetailedLabelsPerSign(handsStruct.detailedLabels);
    disp(['There are ' num2str(signCount) ' signs and ' num2str(labelsCount) ' labels in total to be clustered']);

    OPS_dispClustResults                     = struct;
    OPS_dispClustResults.figID               = 3;
    OPS_dispClustResults.clusteringParams    = clusteringParams;
    OPS_dispClustResults.distMatInfoStruct   = distMatInfoStruct;
    OPS_dispClustResults.clusterResultFolder = clusterResultFolder;
    OPS_dispClustResults.displayLevel        = 1;
    OPS_dispClustResults.saveAllClusterResultImages = saveAllClusterResultImages;
    
    [~, confAccBest, clusterResultCells] = displayClusterResults(srcFold, distMat, handsStruct, medoidHist_Best, clusterLabelsCell, OPS_dispClustResults);
    n = length(distMat);
    falseSampleCounts = (100-confAccBest(:,3))*n/100;
    disptable([confAccBest falseSampleCounts], 'originalClusterCount|reducedKlusterCount|confusionAccuracy|kSpecificity|combinedPrecision|falseSampleCounts',[],'%4.2f',1);    
    
    if deleteDataFile && exist(fileSaveName.dataFile,'file')
        disp(['deleting (' fileSaveName.dataFile ') which has (handsStruct, problematicVids, surSlctListStruct, distMatInfoStruct)']);
        delete(fileSaveName.dataFile)
        disp(['deleted (' fileSaveName.dataFile ')']);
    end    
    
    if finalizeDiary
        diary('off');
    end
    %figureSaveName = createFileSaveName_surfNormDistMat(srcFold, handsStruct, surSlctListStruct.signIDList, 1, surSlctListStruct.single0_double1);
    %figureSaveName = strrep(figureSaveName, '.mat', '.png');
    %saveas(hSummary, figureSaveName);
    %also I need to see the switching within videos
end

function [medoidHist_Best, clusterLabelsCell] = runCluster(distMat, clusteringParams)
%modeStr = 'kMedoids';%'spectralClustering';%
    switch clusteringParams.modeStr
        case 'kMedoids'
            % what if I try to form a group of samples to say - 
            % hey these guys are too much alike and can form a group
            % I can start by maintaining a group list - adding each too close
            % samples to each other 
            [medoidHist_Best, clusterLabelsCell] = run_kmedoidsDG_Loop(distMat, clusteringParams);
            %medoidHist_Best = [kMax,kMax+1] matrix, first kMax square has the k centroids found as best, last column has the total energy/distance according to this clustering
            %clusterLabelsCell = [1, kMax] cell. each cell has [1, N] labels of assigned clusters [1 1 1 1 1 1 2 2 2 2 2 2 3 3 3 3 3 ......... k k k k k k k ]
            %sampleIndsVecCell = [1, kMax] cell. each cell has [1, N] sortedIDs. %sampleIndsVecCell{i',j'} has sampleIDs according to the matching clusterLabelsCell{i',j'} [53 55 54 165 166 50 52 164 49 178 51 177 167]
            %medoidCouplesCell = [1, kMax] cell. each cell has [k, 3] matrix.
            %example = [52 53 0.506;214 216 1.169] - 
            %          cluster1 medoid is 52 or 53 (medoidHist_Best has this info), 
            %          the closest sample is the other one. 
            %          rows sorted according to col3, and each row has lowerID image on 1st col and the higher in 2nd col       
        case 'spectralClustering'
            iterateFor1MedoidCalc = false;
            [medoidHist_Best, clusterLabelsCell] = run_spectralCluster(distMat, clusteringParams, iterateFor1MedoidCalc);
    end
end

function [medoidHist, clusterLabelsCell] = run_spectralCluster(distMat, clusteringParams, iterateFor1MedoidCalc)
    if isfield(clusteringParams,'kMax')
        kMax = clusteringParams.kMax;
        kVec = 1:clusteringParams.kMax;
    elseif isfield(clusteringParams,'kVec')
        kVec = clusteringParams.kVec;
        kMax = max(kVec);
    else
        error('wtf');
    end

    N = size(distMat,1);%there are N number of images to be compared
    clusterLabelsCell = cell(1,kMax);
    medoidHist = inf(kMax,kMax+1);
    sampleIndsVecCell = cell(1,kMax);
    distMatSpectral = changeDistMatForSpectralClustering(distMat, [-400 +400]);
    for k=kVec
        clusterLabels_cur = spectralCluster_gz(struct('distMatrix',distMatSpectral),k);
        clusterLabels_cur = reshape(clusterLabels_cur,1,[]);
        [curMedoids, clusterLabels_corrected, uniqClustIDs] = getMedoidsFromLabels(distMat, clusterLabels_cur);
        [sampleIndsVec_cur, initIndicesOfBlocks, medoidCouples]  = plotDistanceMatrixAsData(distMat, 2, clusterLabels_cur, []);
        [curEnergy, clusterLabels_ForMedoids] = calculateEnergyFromDistMatWithMedoids(distMat, curMedoids);
        medoidHist(k, kMax-length(curMedoids)+1 : kMax+1) = [sort(curMedoids) curEnergy];

        assert(sum(curMedoids<1)==0,'all medoidIDs must be bigger than zero - hence defined');
        if iterateFor1MedoidCalc
            clusterLabelsCell{1,k} = clusterLabels_ForMedoids;
        else
            clusterLabelsCell{1,k} = clusterLabels_cur;
        end
        uniqLabelIDs = unique(clusterLabelsCell{1,k});
        assert(length(uniqLabelIDs)==k,'this must hold')
        sampleIndsVecCell{1,k} = sampleIndsVec_cur;
    end
    disptable(medoidHist);
end

function displayDistMat_pcolor(distMat, figID)
    X = distMat;
    minX = min(X(:));
    X(1==tril(ones(size(X)))) = inf;
    [distVals_sorted, inds_sorted] = sort(X(:));
    [im_i,im_j] = ind2sub(size(X),inds_sorted(1));
    titleStr = ['Min distance(' num2str(distVals_sorted(1)) ') is between ' num2str(im_i) ' and ' num2str(im_j) '.'];
    if ~isempty(figID) && figID>0
        X(1==tril(ones(size(X)))) = minX;
        figure(figID);clf;pcolor(X);colorbar;
        title(titleStr);
    else
        disp(titleStr);
    end
end

function displayMinDistantElement(figID, distMat, numOfElements, handsStruct, srcFold)
    if isempty(figID) || figID<=0 || isempty(numOfElements) || numOfElements<=0
        return
    end
    
    lh1_rh2_bh3_str = {'LH','RH','BH'};
    handsStruct_detailedLabels_colCnt = size(handsStruct.detailedLabels,2);
    
    [distVals_sorted, inds_sorted] = sort(distMat(:));
    %lets draw first 8 and last 8 couples
    figure(figID);clf;
    for i=1:numOfElements
        d = distVals_sorted(i);
        [im_i,im_j] = ind2sub(size(distMat),inds_sorted(i));
        
        dLab_i = handsStruct.detailedLabels(im_i,:);%detailedLabels [signID userID repID lh1_rh2_bh3 imageIDInVideo]
        dLab_j = handsStruct.detailedLabels(im_j,:);
        
        usrFold_i = ['User_' num2str(dLab_i(2)) '_' num2str(dLab_i(3))];
        usrFold_j = ['User_' num2str(dLab_j(2)) '_' num2str(dLab_j(3))];
        
        if handsStruct_detailedLabels_colCnt==5
            fName_i = [lh1_rh2_bh3_str{dLab_i(4)} '_' num2str(dLab_i(end),'%03d') ];
            fName_j = [lh1_rh2_bh3_str{dLab_j(4)} '_' num2str(dLab_j(end),'%03d') ];
        elseif handsStruct_detailedLabels_colCnt==6
            fName_i = [lh1_rh2_bh3_str{dLab_i(4)} '_' num2str(dLab_i(end-1),'%03d') ];
            fName_j = [lh1_rh2_bh3_str{dLab_j(4)} '_' num2str(dLab_j(end-1),'%03d') ];
        end        
        im_i_rgb = imread([srcFold filesep num2str(dLab_i(1)) filesep usrFold_i filesep 'cropped' filesep 'crop_' fName_i '.png']);
        im_j_rgb = imread([srcFold filesep num2str(dLab_j(1)) filesep usrFold_j filesep 'cropped' filesep 'crop_' fName_j '.png']);
        
        subplot(4,2,(i-1)*2+1);%1 3 5 7
        image(im_i_rgb);title(['im(' strrep(usrFold_i,'_','-') '_{' strrep(fName_i,'_','-') '}),min(' num2str(i) ',1),d(' num2str(d) ')']);
        subplot(4,2,i*2);%2 4 6 8
        image(im_j_rgb);title(['im(' strrep(usrFold_j,'_','-') '_{' strrep(fName_j,'_','-') '}),min(' num2str(i) ',2),d(' num2str(d) ')']);
    end
end
