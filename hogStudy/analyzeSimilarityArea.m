%[minDistLR, clusterStruct] = analyzeSimilarityArea([bigDataPath '/Doga'], [112 112], [2 5], [2 1], [84 63])
function [clusterProposalStruct, box_summary_X_struct, box_summary_Y_struct] = analyzeSimilarityArea(sourceFolder, signIDs, userIDs, signRepeatIDs, imIDXY, minDistBlock, bestImCrop_LL_RR_B_LR_RL, frameArea, displayImages, lrb_X3Y3XY5, titleAddStr, clusterNameInit)
    userFolderName_X = [sourceFolder filesep num2str(signIDs(1)) filesep 'User_' num2str(userIDs(1)) '_' num2str(signRepeatIDs(1))];
    userFolderName_Y = [sourceFolder filesep num2str(signIDs(2)) filesep 'User_' num2str(userIDs(2)) '_' num2str(signRepeatIDs(2))];
    
    hogCropFeatsNameFull_X = [userFolderName_X filesep 'hog_crop.mat'];
    hogCropFeatsNameFull_Y = [userFolderName_Y filesep 'hog_crop.mat'];
    
    if exist(hogCropFeatsNameFull_X,'file')
        load(hogCropFeatsNameFull_X, 'hogImArr_cell', 'imcropVec_cell');
    else
        getHOGFeatsofHands(sourceFolder, signIDs(1), userIDs(1), signRepeatIDs(1));
        load(hogCropFeatsNameFull_X, 'hogImArr_cell', 'imcropVec_cell');
    end
    
    hogImArr_cell_X = hogImArr_cell;
    imcropVec_cell_X = imcropVec_cell;
    if exist(hogCropFeatsNameFull_Y,'file')
        load(hogCropFeatsNameFull_Y, 'hogImArr_cell', 'imcropVec_cell');
    else
        getHOGFeatsofHands(sourceFolder, signIDs(2), userIDs(2), signRepeatIDs(2));
        load(hogCropFeatsNameFull_Y, 'hogImArr_cell', 'imcropVec_cell');
    end
    hogImArr_cell_Y = hogImArr_cell;
    imcropVec_cell_Y = imcropVec_cell;
    clear hogImArr_cell imcropVec
    
    boxCount = 5;

    %make the cluster propositions ready
    %only 5 of the frame crops will be added as 
    if signIDs(1) ~= signIDs(2)
        clusterName = ['s' num2str(signIDs(1)) '-u' num2str(userIDs(1)*10 + signRepeatIDs(1)) '_vs_s' num2str(signIDs(2))  '-u' num2str(userIDs(2)*10 + signRepeatIDs(2))];        
    else%signIDs(1)==signIDs(2)
        clusterName = ['s' num2str(signIDs(1)) '_u' num2str(userIDs(1)*10 + signRepeatIDs(1)) '_vs_u' num2str(userIDs(2)*10 + signRepeatIDs(2))];
    end
    
    
    %1. calculate the DTW path in distMat
    [vecIDs, distanceMin] = dtwCL(minDistBlock, false);
        
    %2. calculate the best similar couple 
    %   imIDXY(1) of videoX and
    %   imIDXY(2) of videoY
    vidXYInf = [signIDs(1) userIDs(1) signRepeatIDs(1) imIDXY(1);signIDs(2) userIDs(2) signRepeatIDs(2) imIDXY(2)];
    bestCropStruct = getBestCrops(sourceFolder, hogImArr_cell_X, imcropVec_cell_X, hogImArr_cell_Y, imcropVec_cell_Y, lrb_X3Y3XY5, vidXYInf, boxCount);
    minDistLR = bestCropStruct.minDist;
    bestMatchOrigDist = bestCropStruct.originalCropDist;
    imCropped_X = bestCropStruct.imCropped{1};
    imCropped_Y = bestCropStruct.imCropped{2};
    
    %3. create the videoPatchSummary with fineTunedBoxes
    %   using bestImCrop_LRB,
    D_size = [size(imcropVec_cell_X{1,1},1),size(imcropVec_cell_Y{1,1},1)];
    
    %for the imIDXY(1) th frame of videoX and imIDXY(2) th frame of videoY
    %the best match crops indice is bestImCrop_LRB_best
    bestImCrop_LRB_best = bestImCrop_LL_RR_B_LR_RL(imIDXY(1),imIDXY(2),lrb_X3Y3XY5(3));
    %we can get the crop indices by turning bestImCrop_LRB_best to row and col values where
    %row represents the best crop area of videoX's frame
    %col represents the best crop area of videoY's frame
    [X_imCropInds_Best, Y_imCropInds_Best] = ind2sub(D_size,bestImCrop_LRB_best);
    %then we need to get the actual area information from imcropVec_cells
    %where each box represent [frRowY toRpwY frColX toColX]
    x_box_best = imcropVec_cell_X{ imIDXY(1),lrb_X3Y3XY5(1)}(X_imCropInds_Best,:);
    y_box_best = imcropVec_cell_Y{ imIDXY(2),lrb_X3Y3XY5(2)}(Y_imCropInds_Best,:);
    
    allImCrop_LRB_X = bestImCrop_LL_RR_B_LR_RL(sub2ind(size(bestImCrop_LL_RR_B_LR_RL),(frameArea(1):frameArea(2))',frameArea(3) + vecIDs.Row - 1, lrb_X3Y3XY5(1)*ones(frameArea(2)-frameArea(1)+1,1)));
    [X_imCropInds_AllX, Y_imCropInds_AllX] = ind2sub(D_size,allImCrop_LRB_X);
    x_box_allX = imcropVec_cell_X{ imIDXY(1),lrb_X3Y3XY5(1)}(X_imCropInds_AllX,:);
    y_box_allX = imcropVec_cell_Y{ imIDXY(2),lrb_X3Y3XY5(2)}(Y_imCropInds_AllX,:);
    
    allImCrop_LRB_Y = bestImCrop_LL_RR_B_LR_RL(sub2ind(size(bestImCrop_LL_RR_B_LR_RL),frameArea(1) + vecIDs.Col - 1,(frameArea(3):frameArea(4))', lrb_X3Y3XY5(2)*ones(frameArea(4)-frameArea(3)+1,1)));
    [X_imCropInds_AllY, Y_imCropInds_AllY] = ind2sub(D_size,allImCrop_LRB_Y);
    x_box_allY = imcropVec_cell_X{ imIDXY(1),lrb_X3Y3XY5(1)}(X_imCropInds_AllY,:);
    y_box_allY = imcropVec_cell_Y{ imIDXY(2),lrb_X3Y3XY5(2)}(Y_imCropInds_AllY,:);
    
    
    %F_1_1 - X image with focused area boxed with yellow
    %we crop the boxes imCropped_X([frRowY toRpwY frColX toColX])
   %im_1_1_XBest = drawRectOnImg(imCropped_X, x_box_best([1 3]), [x_box_best(2)-x_box_best(1)+1 x_box_best(4)-x_box_best(3)+1], [1 0 1], 10);
    im_1_1_XBest = drawRectOnImg(imCropped_X, x_box_best([3 1]), [x_box_best(4)-x_box_best(3)+1 x_box_best(2)-x_box_best(1)+1], [1 0 1], 10);
    
    %F_1_2 - Y image with focused area boxed with yellow
   %im_1_2_YBest = drawRectOnImg(imCropped_Y, y_box_best([1 3]), [y_box_best(2)-y_box_best(1)+1 y_box_best(4)-y_box_best(3)+1], [1 0 1], 10);
    im_1_2_YBest = drawRectOnImg(imCropped_Y, y_box_best([3 1]), [y_box_best(4)-y_box_best(3)+1 y_box_best(2)-y_box_best(1)+1], [1 0 1], 10);
    
    %F_2_(1:2) - AllX
    LRBstr = {'RL','RR','BB','LL','LR'};  
    lh_rh_bh_str = cell(1,2);
    lh_rh_bh_str{1} = [LRBstr{1,lrb_X3Y3XY5(3)}(1) 'H'];
    lh_rh_bh_str{2} = [LRBstr{1,lrb_X3Y3XY5(3)}(2) 'H'];
    
    drawMode = 2;
    
    switch drawMode
        case 1
            %pixelLims = zeros(0,4);%[min(xp) max(xp) min(yp) max(yp)]
            box_summary_X_struct = createSummaryImage(frameArea([1 2 3 4]), userFolderName_X, x_box_allX, userFolderName_Y, y_box_allX, frameArea(3) + vecIDs.Row - 1, lh_rh_bh_str);
            box_summary_Y_struct = createSummaryImage(frameArea([3 4 1 2]), userFolderName_Y, y_box_allY, userFolderName_X, x_box_allY, frameArea(1) + vecIDs.Col - 1, fliplr(lh_rh_bh_str));
            bestCropCell = [];
        case 2
            %create a summary as a big box containing every information
            [bestCropCell, box_summary_X_struct, box_summary_Y_struct] = createBoxSummary(sourceFolder, frameArea, vecIDs, minDistBlock, vidXYInf, hogImArr_cell_X, imcropVec_cell_X, hogImArr_cell_Y, imcropVec_cell_Y, lrb_X3Y3XY5, lh_rh_bh_str);
    end

    if displayImages
        figure(82);clf;
        switch drawMode
            case 1
                subplot(4,2,[1 2]);
                bestCropX = imCropped_X(x_box_best(1):x_box_best(2),x_box_best(3):x_box_best(4),:);
                bestCropX = imresize(bestCropX,[size(im_1_1_XBest,1) size(im_1_1_XBest,2)]);
                bestCropY = imCropped_Y(y_box_best(1):y_box_best(2),y_box_best(3):y_box_best(4),:);
                bestCropY = imresize(bestCropY,[size(im_1_2_YBest,1) size(im_1_2_YBest,2)]);
                imshow([im_1_1_XBest,bestCropX,bestCropY,im_1_2_YBest]);
                titleStr01 = ['sign_{' num2str(signIDs(1)) '} - ' titleAddStr{:}];
                titleStr02 = ['vidX(u_{' num2str(userIDs(1)) '}r_{' num2str(signRepeatIDs(1)) '}fr_{' num2str(imIDXY(1)) '}) vs vidY(u_{' num2str(userIDs(2)) '}r_{' num2str(signRepeatIDs(2)) '}fr_{' num2str(imIDXY(2)) '})'];
                titleStr03 = ['origDist(' num2str(bestMatchOrigDist) ')>minDist(' num2str(minDistLR) ')'];
                title({titleStr01,titleStr02,titleStr03});

                h=subplot(4,2,[3 4]);
                image(box_summary_X_struct.image);
                xtc = box_summary_X_struct.xTicksCellX;
                setXYTicks(h, xtc{1}, xtc{2}, xtc{3});
                box_summary_X_struct.title = {['vidX dtw, ' num2str(frameArea(2)-frameArea(1)+1) ' frames, fr(' num2str(frameArea(1)) ')-to(' num2str(frameArea(2)) ')'],mat2str(reshape(frameArea(3)+vecIDs.Row-1,length(vecIDs.Row),[]))};
                title(box_summary_X_struct.title);

                h=subplot(4,2,[5 6]);
                image(box_summary_Y_struct.image);
                xtc = box_summary_Y_struct.xTicksCellX;
                setXYTicks(h, xtc{1}, xtc{2}, xtc{3});
                box_summary_X_struct.title = {['vidY dtw, ' num2str(frameArea(4)-frameArea(3)+1) ' frames, fr(' num2str(frameArea(3)) ')-to(' num2str(frameArea(4)) ')'],mat2str(reshape(frameArea(1)+vecIDs.Col-1,length(vecIDs.Col),[]))};
                title(box_summary_X_struct.title);

                frameJump = 2*round(log((frameArea(2)-frameArea(1))));

                subplot(4,2,7);
                imagesc(minDistBlock);colorbar;
                title('distMat');
                setXYTicks(gca, 1:frameJump:(frameArea(2)-frameArea(1)+1), frameArea(1):frameJump:frameArea(2), 45, 'Ytick');
                setXYTicks(gca, 1:(frameArea(4)-frameArea(3)+1), frameArea(3):frameArea(4), 90, 'XTick');

                subplot(4,2,8);
                dtwMapFig = zeros(size(minDistBlock));%2*max(minDistBlock(:))*ones
                selectedInds = vecIDs.wayBack;
                selectedInds = sub2ind(size(dtwMapFig),selectedInds(:,1),selectedInds(:,2));
                dtwMapFig(selectedInds) = minDistBlock(selectedInds);
                imagesc(dtwMapFig);colorbar;
                title('dtwPath2');
                setXYTicks(gca, 1:frameJump:(frameArea(2)-frameArea(1)+1), frameArea(1):frameJump:frameArea(2), 45, 'Ytick');
                setXYTicks(gca, 1:(frameArea(4)-frameArea(3)+1), frameArea(3):frameArea(4), 90, 'XTick');
            case 2
                numOfFramesToDisplay = (frameArea(2)-frameArea(1)+1)*(frameArea(4)-frameArea(3)+1);
                if numOfFramesToDisplay<20
                    if frameArea(2)-frameArea(1) > frameArea(4)-frameArea(3)
                        %X has more frames than Y
                        spv = [2 2;3 3;4 4];
                    else
                        %Y has more frames than X
                        spv = [3 2;3 4;5 6];
                    end
                elseif numOfFramesToDisplay<100
                    if frameArea(2)-frameArea(1) > frameArea(4)-frameArea(3)
                        %X has more frames than Y
                        spv = [1 2;1 1;2 2];
                    else
                        %Y has more frames than X
                        spv = [2 1;1 1;2 2];
                    end
                else
                    spv = [1 1;1 1;1 1];
                end
                
                if numOfFramesToDisplay<20
                    subplot(spv(1,1),spv(1,2),[1 2]);
                    bestCropX = imCropped_X(x_box_best(1):x_box_best(2),x_box_best(3):x_box_best(4),:);
                    bestCropX = imresize(bestCropX,[size(im_1_1_XBest,1) size(im_1_1_XBest,2)]);
                    bestCropY = imCropped_Y(y_box_best(1):y_box_best(2),y_box_best(3):y_box_best(4),:);
                    bestCropY = imresize(bestCropY,[size(im_1_2_YBest,1) size(im_1_2_YBest,2)]);
                    imshow([im_1_1_XBest,bestCropX,bestCropY,im_1_2_YBest]);
                    titleStr01 = ['sign_{' num2str(signIDs(1)) '} - ' titleAddStr{:}];
                    titleStr02 = ['vidX(u_{' num2str(userIDs(1)) '}r_{' num2str(signRepeatIDs(1)) '}fr_{' num2str(imIDXY(1)) '}) vs vidY(u_{' num2str(userIDs(2)) '}r_{' num2str(signRepeatIDs(2)) '}fr_{' num2str(imIDXY(2)) '})'];
                    titleStr03 = ['origDist(' num2str(bestMatchOrigDist) ')>minDist(' num2str(minDistLR) ')'];
                    title({titleStr01,titleStr02,titleStr03});
                end
                
                xtc = box_summary_X_struct.xTicksCell;
                ytc = box_summary_Y_struct.xTicksCell;
                if numOfFramesToDisplay<100
                    %it showed the stuff above
                    h=subplot(spv(1,1),spv(1,2),spv(2,1):spv(2,2));
                    titleStr = ['vidX dtw, ' num2str(frameArea(2)-frameArea(1)+1) ' frames, fr(' num2str(frameArea(1)) ')-to(' num2str(frameArea(2)) ')'];
                    box_summary_X_struct = showImageFunc(h, box_summary_X_struct, xtc, ytc, titleStr);

                    h=subplot(spv(1,1),spv(1,2),spv(3,1):spv(3,2));
                    titleStr = ['vidY dtw, ' num2str(frameArea(4)-frameArea(3)+1) ' frames, fr(' num2str(frameArea(3)) ')-to(' num2str(frameArea(4)) ')'];
                    box_summary_Y_struct = showImageFunc(h, box_summary_Y_struct, xtc, ytc, titleStr);
                else
                    %it will show only the combined figure with the title
                    %of 4 rows
                    h=subplot(spv(1,1),spv(1,2),spv(2,1):spv(2,2));
                    titleStr01 = ['sign_{' num2str(signIDs(1)) '} - ' titleAddStr{:}];
                    titleStr02 = ['vidX(u_{' num2str(userIDs(1)) '}r_{' num2str(signRepeatIDs(1)) '}fr_{' num2str(imIDXY(1)) '}) vs vidY(u_{' num2str(userIDs(2)) '}r_{' num2str(signRepeatIDs(2)) '}fr_{' num2str(imIDXY(2)) '})'];
                    titleStr03 = ['origDist(' num2str(bestMatchOrigDist) ')>minDist(' num2str(minDistLR) ')'];
                    titleStr04 = ['vidX dtw, ' num2str(frameArea(2)-frameArea(1)+1) ' frames, fr(' num2str(frameArea(1)) ')-to(' num2str(frameArea(2)) ')'];
                    titleStr05 = ['vidY dtw, ' num2str(frameArea(4)-frameArea(3)+1) ' frames, fr(' num2str(frameArea(3)) ')-to(' num2str(frameArea(4)) ')'];
                    titleStr = {titleStr01,titleStr02,titleStr03,[titleStr04 '**' titleStr05]};
                    showImageFunc(h, box_summary_X_struct, xtc, ytc, titleStr);
                    box_summary_X_struct.title = ['vidX dtw, ' num2str(frameArea(2)-frameArea(1)+1) ' frames, fr(' num2str(frameArea(1)) ')-to(' num2str(frameArea(2)) ')'];
                    box_summary_Y_struct.title = ['vidY dtw, ' num2str(frameArea(4)-frameArea(3)+1) ' frames, fr(' num2str(frameArea(3)) ')-to(' num2str(frameArea(4)) ')'];
                end
        end
    end
        
    %for X_IM_RH, Y_IM_RH
    clusterProposalStruct = struct;
    clusterProposalStruct.clusterName = [clusterNameInit clusterName];
    clusterProposalStruct.minMatchValue = bestCropStruct.minDist;
    clusterProposalStruct.maxMatchValue = max(minDistBlock(:));
    clusterProposalStruct.bestCropStruct = bestCropStruct;
    clusterProposalStruct.vidXYInf = [signIDs(1) userIDs(1) signRepeatIDs(1) frameArea(1:2) imIDXY(1);signIDs(2) userIDs(2) signRepeatIDs(2) frameArea(3:4) imIDXY(2)];
    clusterProposalStruct.bestCropCell = bestCropCell;
    clusterProposalStruct.minDistBlock = minDistBlock;
end

function box_summary_struct = showImageFunc(h, box_summary_struct, xtc, ytc, titleStr)
    image(box_summary_struct.image);
    setXYTicks(h, xtc{1}, xtc{2}, xtc{3}, 'Ytick');
    setXYTicks(h, ytc{1}, ytc{2}, ytc{3}, 'Xtick');
    box_summary_struct.title = titleStr;
    title(box_summary_struct.title);
end

function [bestCropCell, box_summary_X_struct, box_summary_Y_struct] = createBoxSummary(sourceFolder, frameArea, vecIDs, minDistBlock, vidXYInf, hogImArr_cell_X, imcropVec_cell_X, hogImArr_cell_Y, imcropVec_cell_Y, lrb_X3Y3XY5, lh_rh_bh_str)
    newImSiz = 80;
    imSepSiz = 10;
    imTotSiz = 100;
    
    xFr = frameArea(1);
    xTo = frameArea(2);
    xIM_cnt = xTo-xFr+1;
    
    yFr = frameArea(3);
    yTo = frameArea(4);
    yIM_cnt = yTo-yFr+1;
    
    box_summary_X = zeros([imTotSiz*xIM_cnt,imTotSiz*yIM_cnt,3], 'uint8');
    color_a_b = map2_a_b(minDistBlock,255,0);
    for ryi_X = 1:xIM_cnt
        for cxj_Y = 1:yIM_cnt
            frX = ryi_X + xFr - 1;
            frY = cxj_Y + yFr - 1;
            cellIDOnWayBackPath = find(vecIDs.wayBack(:,1)==frX & vecIDs.wayBack(:,2)==frY); %#ok<EFIND>
            if isempty(cellIDOnWayBackPath)
                %if all has the values directly then it would be from
                %black(most distant)(0 0 0) to white(closest)(1 1 1)
                col = round([color_a_b(ryi_X,cxj_Y) color_a_b(ryi_X,cxj_Y) color_a_b(ryi_X,cxj_Y)]);
            else
                %but if this is a selected frame then it would go from green(0 1 0) to
                %cyan(0 1 1)
                col = round([0 255 color_a_b(ryi_X,cxj_Y)]);
            end
            baBig = [1+(ryi_X-1)*imTotSiz, ryi_X*imTotSiz, 1+(cxj_Y-1)*imTotSiz, cxj_Y*imTotSiz];%boxAreaBig
            baInn = baBig + [imSepSiz -imSepSiz imSepSiz -imSepSiz];%boxAreaInn
            box_summary_X(baBig(1):baBig(2),baBig(3):baBig(4),1) = col(1);
            box_summary_X(baBig(1):baBig(2),baBig(3):baBig(4),2) = col(2);
            box_summary_X(baBig(1):baBig(2),baBig(3):baBig(4),3) = col(3);
        end
    end
    box_summary_Y = box_summary_X;
    %%%%%%%%%%%%%%%%%%%%%%%%
    %    Y_1 Y_2 Y_3 ... yTo
    %X_1
    %X_2
    %X_3
    %..
    %xTo
    %%%%%%%%%%%%%%%%%%%%%%%%  
    totFrameCnt = xIM_cnt*yIM_cnt;
    bestCropCell = cell(xIM_cnt,yIM_cnt);
    for ryi_X = 1:xIM_cnt
        for cxj_Y = 1:yIM_cnt
            frX = ryi_X + xFr - 1;
            frY = cxj_Y + yFr - 1;
            %videoX(ryi_X) vs videoY(cxj_Y)
            %vidXYInf = [signIDs(1) userIDs(1) signRepeatIDs(1) imIDXY(1);signIDs(2) userIDs(2) signRepeatIDs(2) imIDXY(2)];
            vidXYInf(1,4) = frX;
            vidXYInf(2,4) = frY;
            bestCropStruct = getBestCrops(sourceFolder, hogImArr_cell_X, imcropVec_cell_X, hogImArr_cell_Y, imcropVec_cell_Y, lrb_X3Y3XY5, vidXYInf, 1);
            
            imCroppedX = bestCropStruct.imFineCropped{1};
            imCroppedX = imresize(imCroppedX, [newImSiz newImSiz]);
            imCroppedY = bestCropStruct.imFineCropped{2};
            imCroppedY = imresize(imCroppedY, [newImSiz newImSiz]);
            
            %now define the color of the background of the surrounding box
            %of this image??
            %if this is selected - green component will be 1.0 else it will
            %be same as the other value calculated
            baBig = [1+(ryi_X-1)*imTotSiz, ryi_X*imTotSiz, 1+(cxj_Y-1)*imTotSiz, cxj_Y*imTotSiz];%boxAreaBig
            baInn = baBig + [imSepSiz -imSepSiz imSepSiz -imSepSiz];%boxAreaInn
            if totFrameCnt<100
                box_summary_X(baInn(1):baInn(2),baInn(3):baInn(4),:) = imCroppedX;           
                box_summary_Y(baInn(1):baInn(2),baInn(3):baInn(4),:) = imCroppedY;
            else
                if mod((ryi_X+cxj_Y),2)==1 %[1,2/2,1] are its own others are the other
                    box_summary_X(baInn(1):baInn(2),baInn(3):baInn(4),:) = imCroppedX;           
                    box_summary_Y(baInn(1):baInn(2),baInn(3):baInn(4),:) = imCroppedY;                    
                else
                    box_summary_X(baInn(1):baInn(2),baInn(3):baInn(4),:) = imCroppedY;           
                    box_summary_Y(baInn(1):baInn(2),baInn(3):baInn(4),:) = imCroppedX;
                end                    
            end
            
            bestCropCell{ryi_X,cxj_Y} = bestCropStruct;
        end
    end
    
    %setXYTicks(h, 45:90:90*frCnt, 1:frCnt, 45);
    xTicksCell = cell(3,1);
    xTicksCell{1} = round(imTotSiz/2):imTotSiz:imTotSiz*xIM_cnt;
    xTicksCell{2} = 1:xIM_cnt;
    xTicksCell{3} = 45;%degrees when writing
    %setXYTicks(h, xTicksCell{1}, xTicksCell{2}, xTicksCell{3});
    
    yTicksCell = cell(3,1);
    yTicksCell{1} = round(imTotSiz/2):imTotSiz:imTotSiz*yIM_cnt;
    yTicksCell{2} = 1:yIM_cnt;
    yTicksCell{3} = 45;%degrees when writing
    
    minImgCnt = 5;
    minRowColSize = imTotSiz*minImgCnt;
    [rowSize,colSize,~] = size(box_summary_X);
    
    if xIM_cnt<minImgCnt %the rows are less than the area of 10 images
        yRowStart = floor((minRowColSize-rowSize)/2);
        yRowEnd = yRowStart + rowSize - 1;
        xTicksCell{1} = yRowStart + (round(imTotSiz/2):imTotSiz:imTotSiz*xIM_cnt);
    else
        yRowStart = 1;
        yRowEnd = rowSize;        
    end
    if yIM_cnt<minImgCnt %the rows are less than the area of 10 images
        xColStart = floor((minRowColSize-colSize)/2);
        xColEnd = xColStart + colSize - 1;
        yTicksCell{1} = xColStart + (round(imTotSiz/2):imTotSiz:imTotSiz*yIM_cnt);
    else
        xColStart = 1;
        xColEnd = colSize;        
    end
    
    if (yRowStart~=1 || xColStart ~= 1)
        box_summary_big_X = zeros([minRowColSize,minRowColSize,3], 'uint8');
        box_summary_big_Y = zeros([minRowColSize,minRowColSize,3], 'uint8');        
        box_summary_big_X(yRowStart:yRowEnd,xColStart:xColEnd,:) = box_summary_X;
        box_summary_big_Y(yRowStart:yRowEnd,xColStart:xColEnd,:) = box_summary_Y;
    else
        box_summary_big_X = box_summary_X;
        box_summary_big_Y = box_summary_Y;
    end
    
    
    box_summary_X_struct = struct;
    box_summary_X_struct.image = box_summary_big_X;
    box_summary_X_struct.xTicksCell = xTicksCell;  
    
    box_summary_Y_struct = struct;
    box_summary_Y_struct.image = box_summary_big_Y;
    box_summary_Y_struct.xTicksCell = yTicksCell;  
end

%box_summary = createSummaryImage(pixelLims, userFolderName_X, 'LH')
function box_summary_struct = createSummaryImage(frameArea, userFolderName_X, x_box_allX, userFolderName_Y, y_box_allX, yBestMatchFrIDs, lh_rh_bh_str)
    newImSiz = 90;
    imSepSiz = 10;
    imTotSiz = 100;
    
    xFr = frameArea(1);
    xTo = frameArea(2);
    xIM_cnt = xTo-xFr+1;
    
    box_summary = zeros([2*newImSiz+imSepSiz,imTotSiz*(xIM_cnt-1)+newImSiz,3], 'uint8');
    
    for i=1:xIM_cnt
        frX = i + xFr - 1;
        fbX = x_box_allX(i,:);%fineBoxX
        imCroppedX = imread([userFolderName_X '/cropped/crop_' lh_rh_bh_str{1} '_' num2str(frX,'%03d') '.png']);
        imCroppedX = imresize(imCroppedX,[200 200]);
        imCroppedX = imCroppedX(fbX(1):fbX(2),fbX(3):fbX(4),:);
        imCroppedX = imresize(imCroppedX, [newImSiz newImSiz]);
        
        frY = yBestMatchFrIDs(i);
        fbY = y_box_allX(i,:);%fineBoxX
        imCroppedY = imread([userFolderName_Y '/cropped/crop_' lh_rh_bh_str{2} '_' num2str(frY,'%03d') '.png']);
        imCroppedY = imresize(imCroppedY,[200 200]);
        imCroppedY = imCroppedY(fbY(1):fbY(2),fbY(3):fbY(4),:);
        imCroppedY = imresize(imCroppedY, [newImSiz newImSiz]);
        
        %bottom is the source to be matched
        box_summary(1:newImSiz, 1+(i-1)*imTotSiz:i*imTotSiz-imSepSiz,:) = imCroppedY;
        box_summary(imTotSiz:imTotSiz+newImSiz-1, 1+(i-1)*imTotSiz:i*imTotSiz-imSepSiz,:) = imCroppedX;
    end
    %setXYTicks(h, 45:90:90*frCnt, 1:frCnt, 45);
    xTicksCell = cell(3,1);
    xTicksCell{1} = round(imTotSiz/2):imTotSiz:imTotSiz*xIM_cnt;
    xTicksCell{2} = 1:xIM_cnt;
    xTicksCell{3} = 45;%degrees when writing
    %setXYTicks(h, xTicksCell{1}, xTicksCell{2}, xTicksCell{3});
    
    if xIM_cnt<10
        box_summary_big = zeros([2*newImSiz+imSepSiz,imTotSiz*(10-1)+newImSiz,3], 'uint8');
        xStart = floor((size(box_summary_big,2)-size(box_summary,2))/2);
        xEnd = xStart + size(box_summary,2) - 1;
        box_summary_big(:,xStart:xEnd,:) = box_summary;
        box_summary = box_summary_big;
        xTicksCell{1} = xStart + (round(imTotSiz/2):imTotSiz:imTotSiz*xIM_cnt);
    end
    
    box_summary_struct = struct;
    box_summary_struct.image = box_summary;
    box_summary_struct.xTicksCell = xTicksCell;
end

%updated to flip a hand according to DOMINANT HAND
function [outMatchImage] = frfr(X_IM, Y_IM, boxCount, x_boxes, y_boxes, D, distVals, box_summary_X, box_summary_Y, pixelLims, titleAddStr,  lh1_rh2_bh3)
    outMatchBoxSize = [90,90,3];
    outMatchImage = zeros([outMatchBoxSize(1)*2,outMatchBoxSize(1)*boxCount,3], 'uint8');
    
    X_IM_show = rearrangeIm(X_IM,  lh1_rh2_bh3, [90,90]);
    Y_IM_show = rearrangeIm(Y_IM,  lh1_rh2_bh3, [90,90]);
    
    figure(2);clf;
    subplot(3,3,1);
    image(X_IM_show);
    subplot(3,3,3);%[4 or 8],was-[2 or 4]
    image(Y_IM_show);

    for boxID=1:boxCount
        colSt = (boxID-1)*outMatchBoxSize(2) + 1;
        colEn = boxID*outMatchBoxSize(2);
        x_cr = X_IM(x_boxes(boxID,1):x_boxes(boxID,2),x_boxes(boxID,3):x_boxes(boxID,4),:);
        x_cr = rearrangeIm(x_cr,  lh1_rh2_bh3, [outMatchBoxSize(1) outMatchBoxSize(2)]);
        outMatchImage(1:outMatchBoxSize(1),colSt:colEn,:) = x_cr;

        y_cr = Y_IM(y_boxes(boxID,1):y_boxes(boxID,2),y_boxes(boxID,3):y_boxes(boxID,4),:);
        y_cr = rearrangeIm(y_cr,  lh1_rh2_bh3, [outMatchBoxSize(1) outMatchBoxSize(2)]);
        outMatchImage(1+outMatchBoxSize(1):end,colSt:colEn,:) = y_cr;
    end
    subplot(3,3,2);
    imshow(outMatchImage(:,1:colEn,:));
    titleStr = {['Original match(' num2str(D(1)) ') - Best matches from'],['1(' num2str(distVals(1)) ') to ' num2str(1) '(' num2str(distVals(boxCount)) ') - ' titleAddStr]};
    title(titleStr);
    
    h=subplot(3,3,4:6);
    frCnt = round(size(box_summary_X,2)/size(box_summary_X,1));
    image(box_summary_X);title(['boxSummary-X, ' num2str(frCnt) ' frames, fr(' num2str(pixelLims(1)) ')-to(' num2str(pixelLims(2)) ')']);
    setXYTicks(h, 45:90:90*frCnt, 1:frCnt, 45);

    h=subplot(3,3,7:9);
    frCnt = round(size(box_summary_Y,2)/size(box_summary_Y,1));
    image(box_summary_Y);title(['boxSummary-Y, ' num2str(frCnt) ' frames, fr(' num2str(pixelLims(3)) ')-to(' num2str(pixelLims(4)) ')']);
    setXYTicks(h,45:90:90*frCnt, 1:frCnt, 45);
end