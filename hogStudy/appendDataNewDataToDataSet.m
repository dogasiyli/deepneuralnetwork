function [clustersStruct, theAssignedIndices, addSampleBool, removedSampleInds] = appendDataNewDataToDataSet(clustersStruct, clusterDataSetAdditionStruct, clusterID)
    dataset = clustersStruct.dataset;
    initNumOfSamples = size(clustersStruct.dataset.hogFeats,1);

    numOfSamples_add = size(clusterDataSetAdditionStruct.labels,1);
    
    theAssignedIndices = zeros(1,numOfSamples_add);
    addSampleBool = false(1,numOfSamples_add);
    removedSampleInds = [];
    i = 0;
    while i<numOfSamples_add
        i = i + 1;
        curLabelVec = clusterDataSetAdditionStruct.labels(i,:);
        %does it exist in the <dataset.labels> already?
        if isempty(dataset.labels)
            rowExistsInInds = [];
        else
            rowExistsInInds = find(sum(abs(bsxfun(@minus, dataset.labels, curLabelVec)),2)==0,1);
        end
        [thisFrameWereLabelled, foundRowInds] = checkIfExistInDataset(clustersStruct.dataset, curLabelVec(1:3), curLabelVec(4), clusterID, curLabelVec(end));
        switch thisFrameWereLabelled
            case 0
                %0-not found in labels
            case 1
                %1-found as equal to suggestion
                %go on no problem
            case 2
                %2-found as NOT equal to suggestion
                %here is a problem
                foundRowInds(foundRowInds>initNumOfSamples) = [];
                removedSampleInds = unique([removedSampleInds;foundRowInds]);
                %so we will change the labels of this/these samples
                
                % removedSampleInds(i) = true;
                % numOfSamples_add = numOfSamples_add - 1;
                % 
                % addSampleBool(i) = [];
                % theAssignedIndices(i) = [];
                % clusterDataSetAdditionStruct.hogFeats(i,:) = [];
                % clusterDataSetAdditionStruct.imagesFineCropped(i,:) = [];
                % continue
        end
        if isempty(rowExistsInInds)
            numOfSamples_was = size(dataset.labels,1);
            dataset.labels = [dataset.labels;curLabelVec];
            addSampleBool(i) = true;
            theAssignedIndices(i) = numOfSamples_was+1;
        else
            addSampleBool(i) = false;
            theAssignedIndices(i) = rowExistsInInds;            
        end
    end
        
    dataset.hogFeats = [dataset.hogFeats;clusterDataSetAdditionStruct.hogFeats(addSampleBool,:)];
    dataset.imagesFineCropped = [dataset.imagesFineCropped;clusterDataSetAdditionStruct.imagesFineCropped(addSampleBool,:)];
    
    clustersStruct.dataset = dataset;
end