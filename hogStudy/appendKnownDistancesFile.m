function [knownDistances, knownDistances_append] = appendKnownDistancesFile(knownDistances, knownDistances_append, knownDistancesFileName)
    [~,knownDistances_append] = distMatQuantize_hospiSign(knownDistances_append, []);
    if isempty(knownDistances)
        knownDistances = knownDistances_append;
    else
        knownDistances.frameInfo = [knownDistances.frameInfo;knownDistances_append.frameInfo];
        knownDistances.additionalCols = [knownDistances.additionalCols;knownDistances_append.additionalCols];
    end

    knownDistances_append = [];
    disp(['updating known distances(' knownDistancesFileName ')'])
    save(knownDistancesFileName, 'knownDistances');
end