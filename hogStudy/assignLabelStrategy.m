function labelingStrategyStr = assignLabelStrategy(labelingStrategyStr, retType)
    %'fromSkeletonStableAll','fromAssignedLabels','assignedLabelsRemove1','assignedLabelsExcept1','fromSkeletonOnly'
    if ~exist('retType','var') || isempty(retType)
        retType =1;
    end
    switch labelingStrategyStr
        case {'fromSkeletonStableAll','fsa','1'}
            switch retType
                case 1
                    labelingStrategyStr = 'fromSkeletonStableAll'; 
                case 2
                    labelingStrategyStr = 'fsa'; 
                case 3
                    labelingStrategyStr = '1'; 
            end
        case {'fromAssignedLabels','fal','2'}
            switch retType
                case 1
                    labelingStrategyStr = 'fromAssignedLabels'; 
                case 2
                    labelingStrategyStr = 'fal'; 
                case 3
                    labelingStrategyStr = '2'; 
            end
        case {'assignedLabelsRemove1','ar1','3'}
            switch retType
                case 1
                    labelingStrategyStr = 'assignedLabelsRemove1'; 
                case 2
                    labelingStrategyStr = 'ar1'; 
                case 3
                    labelingStrategyStr = '3'; 
            end
        case {'assignedLabelsExcept1','ax1','4'}
            switch retType
                case 1
                    labelingStrategyStr = 'assignedLabelsExcept1'; 
                case 2
                    labelingStrategyStr = 'ax1'; 
                case 3
                    labelingStrategyStr = '4'; 
            end
        otherwise
        %case {'fromSkeletonOnly','fso','0'}
            switch retType
                case 1
                    labelingStrategyStr = 'fromSkeletonOnly'; 
                case 2
                    labelingStrategyStr = 'fso'; 
                case 3
                    labelingStrategyStr = '0'; 
            end
   end
end