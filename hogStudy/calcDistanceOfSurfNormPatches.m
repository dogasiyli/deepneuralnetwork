function d = calcDistanceOfSurfNormPatches(im01_M, im02_M, xyzCnt)
    %set zero magnitude vecs of im01 to -im02 rows
    zeroMag_01 = im01_M(:,4)==0;
    zeroMag_02 = im02_M(:,4)==0;

    %we will only consider only if both zeroMag is false
    dotProdSamples = zeroMag_01==0 & zeroMag_02==0;
    N = length(dotProdSamples);
    d = zeros(1,N);
    
    %if xyzCnt==3 then all 3 directions are taken into account
    %if xyzCnt==2 then only X and Y directions are taken into account and Z is ignored    
    d(dotProdSamples==1) = -sum(im01_M(dotProdSamples, 1:xyzCnt).*im02_M(dotProdSamples, 1:xyzCnt),2);%similarity [-1 +1] -1 is opposite, +1 is same
    %zeroMag same means d=-1
    %zeroMag different means d=+1
    d(dotProdSamples==0 & zeroMag_01==zeroMag_02) = -inf;
    d(dotProdSamples==0 & zeroMag_01~=zeroMag_02) = +1;

    %d = -sum(d,2);
    %I need 
    % -same to be -1 (decreases the distance of two patch)
    % -opposite to be +1 (increases the distance of two image patch)
    % -irrelevant to be 0 not to have any impact on the calculation
    %dVec = -d;
end
