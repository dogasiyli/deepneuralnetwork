function [minDistVal, minDistInds, patchID_im01, patchID_im02, distOfHogs] = calcDistofHogs(hog01, hog02, hogVersion, imcropVec_struct)
    if hogVersion==1
        distOfHogs = pdist2_fast( hog01', hog02');
        [distVals, inds] = sort(distOfHogs(:));
        minDistVal = distVals(1);
        minDistInds = inds(1);
        [patchID_im01, patchID_im02] = ind2sub(size(distOfHogs),minDistInds);
    elseif hogVersion==2
        stride = 1;
        
        [hog01_extrapolated, hog01_rowColMap] = extrapolateHogV2(hog01, imcropVec_struct, stride);
        [hog02_extrapolated, hog02_rowColMap] = extrapolateHogV2(hog02, imcropVec_struct, stride);
        
        [minDistVal, minDistInds, patchID_im01, patchID_im02, distOfHogs] = pdist_hogV2(hog01_extrapolated,hog02_extrapolated, hog01_rowColMap, hog02_rowColMap);
    end
end