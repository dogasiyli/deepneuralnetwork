function [histVec, normOfHist] = calcHistVecOfBlock(B, v_anglesdg, v_magnitdg)
    [histVec, ~, normOfHist]= vectorized_version(B, v_anglesdg, v_magnitdg);
    %histVec = forloop_version(B, v_anglesdg, v_magnitdg);
    
    %the below codes differ slightly from each other at the boundaries of
    %bins. 
    %the forloop version were unreasonably not taking 'pi' degrees into any
    %bins - that is fixed. I am sure a better for loop can even be coded.
    %but it works anyways.
    %for loop(0.003209 seconds), vectorized(0.000810 seconds) for B = 9,
    %K=16384, basically 257x257 image with a 128x128 cell
    
    %tic;
    %[histVec_flv, angleBins_flv] = forloop_version(B, v_anglesdg, v_magnitdg);
    %flv = toc;
    %tic;
    %[histVec_vcv, angleBins_vcv] = vectorized_version(B, v_anglesdg, v_magnitdg);
    %vcv = toc;
    %disp(['for loop(' num2str(flv,'%8.6f') '), vectorized(' num2str(vcv,'%8.6f') ')'])
    
    %disp([histVec_flv histVec_vcv]);
    %disp(['sum of diff=' num2str(sum(histVec_flv-histVec_vcv))]);
    
    %confusedDegs = [inds angleBins_flv(inds) angleBins_vcv(inds) v_anglesdg(inds)];
    
    %histVec = histVec_vcv;
end

function [histVec, angleBins, normOfHist] = vectorized_version(B, v_anglesdg, v_magnitdg)
    %shift the angles from [-pi,+pi] --> [0,2*pi]
    v_anglesdg = v_anglesdg + pi;    
    %find the radian value to for having B bins
    divRad = 2*pi / (B-1);
    %assign binID to angles according to "B"
    angleBins = floor(v_anglesdg/divRad)+1;
    %https://www.mathworks.com/matlabcentral/newsreader/view_thread/287038
    histVec = accumarray(angleBins, v_magnitdg);
    %histVec = accumarray(angleBins(v_anglesdg<2*pi), v_magnitdg);
    normOfHist = sum(v_magnitdg);%norm(histVec);
    histVec=histVec/(norm(histVec)+0.01);
    if length(histVec)<B
        nonEmptyBins = unique(angleBins);
        histVecReturn = zeros(1,B);
        histVecReturn(1:max(nonEmptyBins)) = histVec(:);
        histVec = histVecReturn';
    end
end

function [histVec, angleBins] = forloop_version(B, v_anglesdg, v_magnitdg)
    %https://www.mathworks.com/matlabcentral/fileexchange/28689-hog-descriptor-for-matlab
    bin=0;
    histVec=zeros(B,1);
    K = max(size(v_anglesdg));
    angleBins = zeros(K,1);
    for ang_lim=-pi+2*pi/B:2*pi/B:pi
        bin=bin+1;
        for k=1:K
            if v_anglesdg(k)<=ang_lim
                v_anglesdg(k)=100;
                histVec(bin)=histVec(bin)+v_magnitdg(k);
                angleBins(k) = bin;
            end
        end
    end
    histVec=histVec/(norm(histVec)+0.01);
end