function  [mnDS, distMat, mxDS] =  calcMinDist_hogFeatCells(im01_DbyK, im02_DbyK, method, distMethod)
    if method==0
        distMat = calcDistMat_0_regular(im01_DbyK, im02_DbyK, distMethod);    
    else
        error('not implemented yet')
    end
    if nargout==3
        %find min and set
        [mnDS, mxDS] = getMatrixValueBounds(distMat, false);
        %mnDS.im01 = imCell01(mnDS.rowID,:);
        %mnDS.im02 = imCell02(mnDS.colID,:);
        mnDS.cropIDs = [mnDS.rowID,mnDS.colID];
        %mxDS.im01 = imCell01(mxDS.rowID,:);
        %mxDS.im02 = imCell02(mxDS.colID,:);
        mxDS.cropIDs = [mxDS.rowID,mxDS.colID]; 
    else
        mnDS = getMatrixValueBounds(distMat, false);
        mnDS.cropIDs = [mnDS.rowID,mnDS.colID];        
    end
    if mnDS.d==0
        mnDS.d = eps;
    end
end

function distMat = calcDistMat_0_regular(im01_DbyK, im02_DbyK, distMethod)
    cropCnt01 = size(im01_DbyK,2);
    cropCnt02 = size(im02_DbyK,2);
    switch distMethod
        case {'euclidian','e3'}
            %following gives the cosine distance using x y and z
            distMat = pdist2_fast( im01_DbyK', im02_DbyK');
            assert(size(distMat,1)==cropCnt01 && size(distMat,2)==cropCnt02,'must match')
        case {'euclidian2','22'}
            error(['unimplemented distanceType(' distMethod ')']);
        otherwise
            error(['unknown distanceType(' distMethod ')']);
    end
end
