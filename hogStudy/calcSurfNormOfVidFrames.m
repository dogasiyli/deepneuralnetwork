function calcSurfNormOfVidFrames(folderName, blockCnts, framesOpts, enforceReCreateNew)
%   calcSurfNormOfVidFrames('/mnt/Data/FromUbuntu/HospiSign/HospiMOV/296/User_2_1')
%   calcSurfNormOfVidFrames('D:\FromUbuntu\Doga\112\User_2_2')
    %folderName = 'D:\FromUbuntu\Doga\112\User_2_2';
    if nargin<2 || isempty(framesOpts)
        framesOpts = struct;
        framesOpts.bigVideoShow = false;
        framesOpts.bigVideoSaveScale = 1.00;
        framesOpts.show = false;
        framesOpts.save = true;
    end
    if nargin<3 || ~exist('enforceReCreateNew','var')
        enforceReCreateNew = false;
    end
    
    surfNormFileName = [folderName filesep 'surfNorm.mat'];
    surfNormZipFileName = [folderName filesep 'surfNorm.zip'];
    
    %check the backup folder to get the backed up surfNormFileName
    backupFold = getVariableByComputerName('backupFold');
    backupFold = [backupFold filesep 'Backup_Feats'];
    foldCells = strsplit(folderName, filesep);
    userCells = strsplit(foldCells{1,end}, '_');
    rString = sprintf('%02d',str2double(userCells{1,end}));
    backupFileName = [backupFold filesep 'surfNorm_crop_s' foldCells{1,end-1} '_u' userCells{1,2} '_r' rString '.mat'];
    backupFileExist = exist(backupFileName,'file');
    
    if exist(surfNormFileName,'file') && exist(surfNormZipFileName,'file') && enforceReCreateNew==false
        disp(['skipping calcSurfNormOfVidFrames(' folderName '); because all the files to be exported are already exported']);
        return
    elseif enforceReCreateNew==false && backupFileExist
        copyfile(backupFileName,surfNormFileName,'f');
        disp(['skipping calcSurfNormOfVidFrames(' folderName '); surfNormFileName retrieved from backup']);
        return
    else
        try
            rmdir([folderName filesep 'surfNorm'],'s');
        catch
        end
    end
    videoFileNameFull = [folderName filesep 'color.mp4'];
    v = VideoReader(videoFileNameFull);
    vidFrameMat4d = read(v,[1 Inf]); %#ok<VIDREAD>
    clear v
    
    depthFileNameFull = [folderName filesep 'depth.mat'];
    load(depthFileNameFull);
    load([folderName filesep 'skeleton.mat'], 'skeleton');
    
    % cropArea = struct;% 1_from_row_y, 2_from_col_x, 3_height_row_y, 4_width_col_x
    % cropArea.{LH_big,RH_big,BH_big,FC_big,LH_sml,RH_sml,BH_sml} = zeros(frCnt,4);
    cropAreaFileName = [folderName filesep 'cropArea.mat'];
    if exist(cropAreaFileName,'file')
        disp(['Loading(' cropAreaFileName ')']);
        load(cropAreaFileName, 'cropArea');
    else
        cropArea = getCropArea(skeleton);
        disp(['Saving(' cropAreaFileName ')']);
        save(cropAreaFileName, 'cropArea');
    end
    
    saveToFolder = [folderName filesep 'surfNorm' filesep ];
    if ~exist(saveToFolder,'dir')
        mkdir(saveToFolder);
    end
    
    if framesOpts.bigVideoSaveScale>0
        try
            v = VideoWriter([[folderName filesep] 'surfNormVideo.mp4'],'MPEG-4');
        catch err
            disp(['tried MPEG-4 as profile but didnt go trough.err(' err.message ')']);
            v = VideoWriter([[folderName filesep] 'surfNormVideo.mp4']);
        end
        v.FrameRate=10;
        open(v)
    elseif framesOpts.show
        figure(2);
    end
    
    frCnt = min(size(depth,3),size(vidFrameMat4d,4));
    warning('off','images:initSize:adjustingMag');
    OPS = struct('figID', [],'rejectPatchMethod', 'notIncludeHand','normCalcMethod', 'haar-like','changeGivenValues', false,'cropAreaStrCell', {'BH_sml','RH_sml','LH_sml'});

    surfNormCell = cell(1,3);%cells - 1-LH, 2-RH, 3-BH
    surfNormCell{1,1} = zeros(blockCnts(1)*blockCnts(2),4,frCnt);
    surfNormCell{1,2} = zeros(blockCnts(1)*blockCnts(2),4,frCnt);
    surfNormCell{1,3} = zeros(blockCnts(1)*blockCnts(2),4,frCnt);
    dispstat('','init');
    for i=1:frCnt
        [faceNormCells, quiverMatCells, blockAreaSizeCells, rgbImgAsFaceNormCells, m] = getSurfNorm(folderName,i, blockCnts, OPS);
        dispstat(sprintf('%d of %d completed..\n',i,frCnt));

        %curFrameDepth = squeeze(depth(:,:,i));      
        curFrameRGB = squeeze(vidFrameMat4d(:,:,:,i));      
        torsoRGB = cropBodyPart(curFrameRGB, cropArea.TS_big(i,1:2), cropArea.TS_big(i,3:4));
        
        %flip rh to left to make thing same for single hand
        rgbImgAsFaceNormCells{2} = flipdim(rgbImgAsFaceNormCells{2},2);
        
        torsoRGB_insert = resizeImg_IntoBox(torsoRGB, [300 300], true);
        BH_RGB_insert = resizeImg_IntoBox(rgbImgAsFaceNormCells{1}, [160 200], true);
        RH_RGB_insert = resizeImg_IntoBox(rgbImgAsFaceNormCells{2}, [80 80], true);
        LH_RGB_insert = resizeImg_IntoBox(rgbImgAsFaceNormCells{3}, [80 80], true);
        
        curFrameSurfNorm = zeros(300,500,3);
        curFrameSurfNorm(1:300,1:300,:) = torsoRGB_insert;
        curFrameSurfNorm(1:160,301:500,:) = BH_RGB_insert;
        curFrameSurfNorm(221:300,301:380,:) = RH_RGB_insert;
        curFrameSurfNorm(221:300,421:500,:) = LH_RGB_insert;
        
        surfNormCell{1,1}(:,:,i) = faceNormCells{3,1};%LH
        %flip right to left
        x = reshape(faceNormCells{2,1},[blockCnts 4]);
        x2 = -flipdim(x,2);
        x3 = reshape(x2,size(faceNormCells{2,1}));
        surfNormCell{1,2}(:,:,i) = x3;%RH
        surfNormCell{1,3}(:,:,i) = faceNormCells{1,1};%BH
        
        imwrite(curFrameSurfNorm, [saveToFolder 'surfNorm_' num2str(i,'%03d') '.png']);
        
        if framesOpts.bigVideoShow
            figure(1);
            titleStr = ['FrameID(' num2str(1) ' of ' num2str(frCnt) ')'];
            %bigVideoFrame = insertText(bigVideoFrame,positionTitleInImage,titleStr,'FontSize',18,'BoxColor', 'yellow','BoxOpacity',0.75,'TextColor','black');
            image(curFrameSurfNorm);
            title(titleStr);
            drawnow;
            %F = getframe(gcf);
            %writeVideo(v,bigVideoFrame);
        end
        if framesOpts.bigVideoSaveScale>0
            bigVideoFrameRes = imresize(curFrameSurfNorm, framesOpts.bigVideoSaveScale);
            writeVideo(v,bigVideoFrameRes);
        end

    end
    if framesOpts.bigVideoSaveScale>0
        close(v);
    end
    warning('on','images:initSize:adjustingMag'); 
    dispstat(sprintf('surfNorm_crop_s(%s)_u(%s)_r(%s) completed..\n',foldCells{1,end-1},userCells{1,2},rString),'keepthis');
    
    save(surfNormFileName,'surfNormCell');
    if framesOpts.save
        %now compress the folder into zip
        zip([folderName filesep 'surfNorm.zip'],[folderName filesep 'surfNorm']);
        rmdir([folderName filesep 'surfNorm'],'s');
    end
end
