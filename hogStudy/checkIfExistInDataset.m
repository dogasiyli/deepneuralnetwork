function [datasetCheckResult, foundRowInds] = checkIfExistInDataset(datasetStruct, surVec, imageID, clusterID, jLRB)
    %0-not found in labels
    %1-found as equal to suggestion
    %2-found as NOT equal to suggestion
    foundRowInds = [];
    labelMat = datasetStruct.labels;
    if isempty(labelMat)
        datasetCheckResult = 0;%not found
        return
    end
    %jLRB - 1Left, 2Right, 3Both
    rowSlct = labelMat(:,end)==jLRB;
    labelMat = labelMat(rowSlct,:);%[col01_signID,col02_userID,col03_repID,col04_frameID,col05_08_fineCropReal,col09_lh1_rh2_bh3];
    assignments = datasetStruct.assignments(rowSlct,:);%[1_datasetID, 2_clusterID, 3_subClusterID, 4_distToCenterSample]
    
    foundRows = find(ismember(labelMat(:,[1 2 3 4]),[surVec imageID],'rows'));
    if ~isempty(foundRows)
        relatedRows = assignments(foundRows,:);
        labelMat = labelMat(foundRows,:);
        foundRowInds = find(rowSlct);
        foundRowInds = foundRowInds(foundRows);
        try
            if  ~isempty(find(relatedRows(:,2)==clusterID, 1))
                datasetCheckResult = 1;%found as equal to suggestion
            else
                datasetCheckResult = 2;%found as NOT equal to suggestion
            end
        catch
            datasetCheckResult = 2;%found as NOT equal to suggestion
        end
    else
        datasetCheckResult = 0;%not found
    end
end