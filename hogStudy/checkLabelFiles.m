function [someMissing, labelFilesExist, labelCells] = checkLabelFiles(theFoldName, single0_double1, makeEmptyFrames)
    labelFilesExist = [false false false];
    labelCells = cell(1,3);
    if ~exist('makeEmptyFrames','var') || isempty(makeEmptyFrames)
        makeEmptyFrames = true;
    end
    try
        labelFolder = [theFoldName filesep 'labelFiles' filesep ];
        labelFile_lh = exist([labelFolder 'labelsCS_LH.txt'],'file');
        labelFile_rh = exist([labelFolder 'labelsCS_RH.txt'],'file');
        labelFile_bh = exist([labelFolder 'labelsCS_BH.txt'],'file');
        labelFilesExist = [labelFile_lh labelFile_rh labelFile_bh];
        switch (single0_double1)
            case 0
                someMissing = ~(labelFile_lh && labelFile_rh);
            case 1
                someMissing = ~labelFile_bh;
            case 2
                someMissing = ~(labelFile_lh && labelFile_rh && labelFile_bh);
        end
        if nargout ==3
            if labelFile_lh
                labelCells{1} = getLabelVecFromFile([labelFolder 'labelsCS_LH.txt'], makeEmptyFrames);
            end
            if labelFile_rh
                labelCells{2} = getLabelVecFromFile([labelFolder 'labelsCS_RH.txt'], makeEmptyFrames);
            end
            if labelFile_bh
                labelCells{3} = getLabelVecFromFile([labelFolder 'labelsCS_BH.txt'], makeEmptyFrames);
            end
        end
    catch err
        disp(['err in checkLabelFiles (' err.message ')']);
    end
end