function [ distMatReturn, blobDef ] = clearBlocks( distMat_01, minBlockSize, treshValue )
%clearBlocks only the boxes with all the values>=treshValue will remain,
%others will be deleted
%   use erosion to remove the values if any block with minBlockSize by
%   minBlockSize has any values bigger than treshValue

    distMatReturn = distMat_01;
    distMatReturn(distMat_01<=treshValue) = -1;
    distMatReturn(distMat_01 >treshValue) =  0;
    distMatReturn = distMatReturn.*-1;
    
    se = strel('square',minBlockSize);
    distMatReturn = imerode(distMatReturn, se, 'same');
    
    
    connComp = bwconncomp(distMatReturn);
    numPixels = cellfun(@numel,connComp.PixelIdxList);
    [biggest,idx] = max(numPixels);
    
    numOfBolbs = length(numPixels);
    
    blobDef = zeros(numOfBolbs,11);
    for i=1:numOfBolbs
         pixIDsCurBlob = connComp.PixelIdxList{i};
         [xp, yp] = ind2sub(size(distMat_01), pixIDsCurBlob);
         xplim = [min(xp) max(xp)];
         yplim = [min(yp) max(yp)];
         
         
         blobArea = distMat_01(xplim(1):xplim(2), yplim(1):yplim(2));
         
         %min value match
         blobAreaVec = blobArea(:);
         [baV, baI] = sort(blobAreaVec);
         [baIxp, baIyp] = ind2sub(size(blobArea), baI(1));
         baIv = blobArea(baIxp, baIyp);
         
         minValMatch_pos_val = [baIxp + xplim(1) - 1 , baIyp + yplim(1) - 1, baIv];
         
         %best sum match
         %xSum is a n row vec
         xSum = sum(blobArea,2);
         [~, baIxp] = min(xSum);         
         %ySum is a n col vec
         ySum = sum(blobArea);
         [~, baIyp] = min(ySum);
         baIv = blobArea(baIxp, baIyp);
         minBestSum_pos_val = [baIxp + xplim(1) - 1 , baIyp + yplim(1) - 1, baIv];
         
         blobDef(i,1:3) = minValMatch_pos_val;
         blobDef(i,4:6) = minBestSum_pos_val;
         blobDef(i,7:11) = [xplim yplim length(pixIDsCurBlob)];
    end
    blobDef = sortrows(blobDef, +3);
end

