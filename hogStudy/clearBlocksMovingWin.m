function [ distMatReturn, blobDef ] = clearBlocksMovingWin( distMat_01, minBlockSize, treshMul, treshVals, figID)
%clearBlocks only the boxes with all the values>=treshValue will remain,
%others will be deleted
%   use erosion to remove the values if any block with minBlockSize by
%   minBlockSize has any values bigger than treshValue

    distMatReturn = distMat_01;
    pixelLims = zeros(0,4);%[min(xp) max(xp) min(yp) max(yp)]
    %[row_i, col_j, treshBox] = minElementMat( distMat_01,        2, [0.75 2]);
    %[row_i, col_j, treshBox] = minElementMat(          M, treshMul, treshVal);
    while (min(distMatReturn(:))<treshVals(2))
        [row_i, col_j, treshBox] = minElementMat(distMatReturn, treshMul, treshVals);
        if (min(treshBox(2)-treshBox(1),treshBox(4)-treshBox(3))>=minBlockSize)
            %add it as a block
            distMatReturn(treshBox(1):treshBox(2),treshBox(3):treshBox(4)) = inf;
            pixelLims(size(pixelLims,1)+1,:) = treshBox;
        else
            distMatReturn(treshBox(1):treshBox(2),treshBox(3):treshBox(4)) = max(distMatReturn(distMatReturn(:)~=inf));
        end
%         if exist('figID','var') && ~isempty(figID) && figID>0
%             plotCurSituation(figID, distMat_01, distMatReturn, size(pixelLims,1));
%             drawnow;
%         end
    end
    numOfBolbs = size(pixelLims,1);
    if numOfBolbs==0
        blobDef = [];
        return;
    end
    
    distMatReturn(distMatReturn>=inf) = -inf;
%     if exist('figID','var') && ~isempty(figID) && figID>0
%         plotCurSituation(figID, distMat_01, distMatReturn, size(pixelLims,1));
%         drawnow;
%     end
    distMatReturn(distMatReturn>=0) = inf;
%     if exist('figID','var') && ~isempty(figID) && figID>0
%         plotCurSituation(figID, distMat_01, distMatReturn, size(pixelLims,1));
%         drawnow;
%     end
    distMatReturn(distMatReturn<0) = distMat_01(distMatReturn<0);
    distMatReturn(distMatReturn>=inf) = max(distMatReturn(distMatReturn(:)~=inf));
%     if exist('figID','var') && ~isempty(figID) && figID>0
%         plotCurSituation(figID, distMat_01, distMatReturn, size(pixelLims,1));
%         drawnow;
%     end
    
    
    blobDef = zeros(numOfBolbs,11);
    for i=1:numOfBolbs
         %pixIDsCurBlob = connComp.PixelIdxList{i};
         %[xp, yp] = ind2sub(size(distMat_01), pixIDsCurBlob);
         xplim = pixelLims(i,1:2);% [min(xp) max(xp)];
         yplim = pixelLims(i,3:4);% [min(yp) max(yp)];
         pixCnt = (yplim(2)-yplim(1))*(xplim(2)-xplim(1));
         
         
         blobArea = distMat_01(xplim(1):xplim(2), yplim(1):yplim(2));
         
         [minValMatch_pos_val, minBestSum_pos_val] = getMinBestOfBox( blobArea, xplim(1:2), yplim(1:2));
         
         blobDef(i,1:3) = minValMatch_pos_val;
         blobDef(i,4:6) = minBestSum_pos_val;
         blobDef(i,7:11) = [xplim yplim pixCnt];
    end
    blobDef = sortrows(blobDef, +3);
    
    if exist('figID','var') && ~isempty(figID) && figID>0
        plotCurSituation(figID, distMat_01, distMatReturn, numOfBolbs);
    end
end

function plotCurSituation(figID, baseMat, changedMat, numOfBolbs)
    figure(figID);clf;
    subplot(2,1,1);
    imagesc(baseMat);title('Given distance matrix between 2 videos');
    colorbar;
    subplot(2,1,2);
    changedMat(changedMat==inf) = -max(changedMat(changedMat(:)~=inf));
    imagesc(changedMat);title(['Tresholded version with ' num2str(numOfBolbs) ' blocks']);
    colorbar;
end

