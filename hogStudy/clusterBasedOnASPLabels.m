function clusterBasedOnASPLabels(signSourceFolder, signIDs, userIDs, signRepeatIDs, figIDBegin)
%signSourceFolder = '/mnt/Data/FromUbuntu/HospiSign/HospiMOV';
%signSourceFolder = '/home/dg/DataPath/FromUbuntu/Doga';%work-
%signSourceFolder = '/media/doga/Data/FromUbuntu/Doga';%laptop-msi-ubuntu
%clusterBasedOnVidCouple(signSourceFolder, 112, [2 5], [3 1])

    clustersStruct = [signSourceFolder filesep 'clusters_' num2str(signIDs(1),'%03d') '.mat'];
    
    % if ~exist('treshMul', 'var') || isempty(treshMul)
    %     treshMul = 4;
    % end
    if ~exist('figIDBegin', 'var') || isempty(figIDBegin)
        figIDBegin = -1;
    end

    userID_x = userIDs(1); signRepeatID_x = signRepeatIDs(1);
    userID_y = userIDs(2); signRepeatID_y = signRepeatIDs(2);
    
    if length(signIDs)>1
        saveFileName = [signSourceFolder filesep 's' num2str(signIDs(1)) 'vs' num2str(signIDs(2)) '_u' num2str(userID_x*10 + signRepeatID_x) '_vs_u' num2str(userID_y*10 + signRepeatID_y) '.mat'];        
    elseif length(signIDs)==1
        saveFileName = [signSourceFolder filesep 's' num2str(signIDs) '_u' num2str(userID_x*10 + signRepeatID_x) '_vs_u' num2str(userID_y*10 + signRepeatID_y) '.mat'];
        signIDs = [signIDs signIDs];
    end
    
    if exist(saveFileName, 'file')
        %load([signSourceFolder filesep 's112_u22_vs_u51.mat'])
        tic;disp(['Loading ' saveFileName '(signIDsSaved, imIDEnds, minDist_XL_YL, minDist_XR_YR, minDist_B, minDist_XL_YR, minDist_XR_YL, bestImCrop_LRB)']);
        load(saveFileName, 'signIDsSaved', 'imIDEnds', 'minDist_XL_YL', 'minDist_XR_YR', 'minDist_B', 'minDist_XL_YR', 'minDist_XR_YL', 'bestImCrop_LL_RR_B_LR_RL');
        toc;disp(['Loaded ' saveFileName]);
    else
        disp([saveFileName ' - file doesnt exist run compareVidCouplePerImageHandDist.m first']);
        disp(['running compareVidCouplePerImageHandDist(' signSourceFolder ', ' mat2str(signIDs) ','  mat2str(userIDs) ','  mat2str(signRepeatIDs) ')']);
        compareVidCouplePerImageHandDist(signSourceFolder, unique(signIDs), userIDs, signRepeatIDs);
        load(saveFileName, 'signIDsSaved', 'imIDEnds', 'minDist_XL_YL', 'minDist_XR_YR', 'minDist_B', 'minDist_XL_YR', 'minDist_XR_YL', 'bestImCrop_LL_RR_B_LR_RL');
    end
    
    %I need to also load the "handUsage.mat" files
    signSourceFolder_X = [signSourceFolder filesep num2str(signIDs(1)) filesep 'User_' num2str(userID_x) '_' num2str(signRepeatID_x)];
    signSourceFolder_Y = [signSourceFolder filesep num2str(signIDs(2)) filesep 'User_' num2str(userID_y) '_' num2str(signRepeatID_y)];
    
    [ handDisplacementMat_X, handLabelsStruct_X ] = getHandDisplacementFromASPLabels(signSourceFolder, signIDs(1), userID_x, signRepeatID_x);
    [ handDisplacementMat_Y, handLabelsStruct_Y ] = getHandDisplacementFromASPLabels(signSourceFolder, signIDs(2), userID_y, signRepeatID_y);
    clustersASPInfoStruct = getClusterInfoFromASPTxt(signSourceFolder, signIDs(1));
    
    hand_blocks = createBlocksFromDisplacementMats(handDisplacementMat_X,handDisplacementMat_Y);
   
    %treshVals = [minPossible*treshMul*0.5, minPossible*treshMul];%[0.75 2]
    %minBlockSize = 5;
    
    unzip([signSourceFolder_X filesep 'cropped.zip'],signSourceFolder_X);
    unzip([signSourceFolder_Y filesep 'cropped.zip'],signSourceFolder_Y);
    
    blobDefCell = cell(5,1);
    minDistCell = {minDist_XR_YL,minDist_XR_YR,minDist_B,minDist_XL_YL,minDist_XL_YR};
    if figIDBegin>0
        %plot left hand distmat to <figIDBegin>
        plotMinDistImageSC(minDistCell{1}, [figIDBegin+0 2 5 1], 'XR-YL', signIDs, userIDs, signRepeatIDs);
        plotMinDistImageSC(minDistCell{2}, [figIDBegin+0 2 5 2], 'Right', signIDs, userIDs, signRepeatIDs);
        plotMinDistImageSC(minDistCell{3}, [figIDBegin+0 2 5 3], 'Both' , signIDs, userIDs, signRepeatIDs);
        plotMinDistImageSC(minDistCell{4}, [figIDBegin+0 2 5 4], 'Left' , signIDs, userIDs, signRepeatIDs);
        plotMinDistImageSC(minDistCell{5}, [figIDBegin+0 2 5 5], 'XL-YR' , signIDs, userIDs, signRepeatIDs);
        
        subplot(2,5, 6);imagesc(hand_blocks.XRH_YLH.XY_blocks);colorbar;
        subplot(2,5, 7);imagesc(hand_blocks.XRH_YRH.XY_blocks);colorbar;
        subplot(2,5, 8);imagesc(hand_blocks.BH.XY_blocks);colorbar;
        subplot(2,5, 9);imagesc(hand_blocks.XLH_YLH.XY_blocks);colorbar;
        subplot(2,5,10);imagesc(hand_blocks.XLH_YRH.XY_blocks);colorbar;
        
        %figure(figIDBegin+1);
        %use minDist_R and hand_blocks.RH.XY_blocks, and get blobDefs
        blocksDrawCell = cell(1,5);
        [blobDefCell{1}, blocksDrawCell{1}] = getBlobDefASP( minDistCell{1}, hand_blocks.XRH_YLH.XY_blocks, handLabelsStruct_X.RH, handLabelsStruct_Y.LH);
        [blobDefCell{2}, blocksDrawCell{2}] = getBlobDefASP( minDistCell{2}, hand_blocks.XRH_YRH.XY_blocks, handLabelsStruct_X.RH, handLabelsStruct_Y.RH);
        [blobDefCell{3}, blocksDrawCell{3}] = getBlobDefASP( minDistCell{3}, hand_blocks.BH.XY_blocks, handLabelsStruct_X.BH, handLabelsStruct_Y.BH);
        [blobDefCell{4}, blocksDrawCell{4}] = getBlobDefASP( minDistCell{4}, hand_blocks.XLH_YLH.XY_blocks, handLabelsStruct_X.LH, handLabelsStruct_Y.LH);
        [blobDefCell{5}, blocksDrawCell{5}] = getBlobDefASP( minDistCell{5}, hand_blocks.XLH_YRH.XY_blocks, handLabelsStruct_X.LH, handLabelsStruct_Y.RH);
        
        drawSelectedBlobCells(figIDBegin, blocksDrawCell, blobDefCell, minDistCell);
        
    else
        [blobDefCell{1}] = getBlobDefASP(minDistCell{1}, hand_blocks.XRH_YLH.XY_blocks);
        [blobDefCell{2}] = getBlobDefASP(minDistCell{2}, hand_blocks.XRH_YRH.XY_blocks);
        [blobDefCell{3}] = getBlobDefASP(minDistCell{3}, hand_blocks.BH.XY_blocks);
        [blobDefCell{4}] = getBlobDefASP(minDistCell{4}, hand_blocks.XLH_YLH.XY_blocks);
        [blobDefCell{5}] = getBlobDefASP(minDistCell{5}, hand_blocks.XLH_YRH.XY_blocks);
    end
    
    comparisonFigFolder = [signSourceFolder filesep num2str(signIDs(1)) filesep 'compLrn_ux(' num2str(userID_x) num2str(signRepeatID_x) ')_uy(' num2str(userID_y) num2str(signRepeatID_y) ')'];
    if ~exist(comparisonFigFolder,'dir')
        disp(['Creating dir(' comparisonFigFolder ')'])
        mkdir(comparisonFigFolder);
    end
    
    figFileNameStart = [comparisonFigFolder filesep 's' num2str(signIDs(1)) '_u' num2str(userID_x*10 + signRepeatID_x) '_vs_u' num2str(userID_y*10 + signRepeatID_y) '_'];
    h = figure(figIDBegin+0);
    saveas(h,[figFileNameStart 'blocks.png']);
    h = figure(figIDBegin+1);
    saveas(h,[figFileNameStart 'blobsSelected_Init.png']);
    
    LRBstr = {'RL','RR','BB','LL','LR'};
    rl1_rr2_bb3_ll4_lr5 = [2 1;2 2;3 3;1 1;1 2];%lh1_rh2_bh3
    for lh1_rh2_bh3=[3 4 2 1 5]                
        blockCount = size(blobDefCell{lh1_rh2_bh3},1);
        j = 0;
        while j <= blockCount-1 %for j = 1: blockCount
            j = j + 1;
            lh1_rh2_bh3_new = [rl1_rr2_bb3_ll4_lr5(lh1_rh2_bh3,:) lh1_rh2_bh3];
            clusterInitStr = LRBstr{lh1_rh2_bh3};
            clusterNameInit = [clusterInitStr 'H-Block'];
            
            x_im_ID = blobDefCell{lh1_rh2_bh3}(j,1);
            y_im_ID = blobDefCell{lh1_rh2_bh3}(j,2);
            
            setClusterID = blobDefCell{lh1_rh2_bh3}(j,end);        
            frameArea = blobDefCell{lh1_rh2_bh3}(j,4:7);
            [clusterName, answerCell] = getClusterInfo(clustersASPInfoStruct, LRBstr{lh1_rh2_bh3}, setClusterID, frameArea);
            
            minDistBlock = minDistCell{lh1_rh2_bh3}(frameArea(1):frameArea(2),frameArea(3):frameArea(4));
            
            [clusterProposalStruct, box_summary_X_struct, box_summary_Y_struct] = analyzeSimilarityArea(signSourceFolder, signIDs, [userID_x userID_y], [signRepeatID_x signRepeatID_y], [x_im_ID y_im_ID], minDistBlock, bestImCrop_LL_RR_B_LR_RL, frameArea, true, lh1_rh2_bh3_new, [LRBstr(lh1_rh2_bh3) 'H-Block'], clusterNameInit);
            %if lh1_rh2_bh3==3 && j==1 && changedRowCnt==0
            %    putBreakPointAhead( 2, 'dbstop' );%eval(['dbstop in clusterBasedOnVidCouple at ' num2str(thisLineNr + 5)]);
            %end            
            [setClusterID, frameAreaOut, clusterProposalFresh, clusterDataSetAdditionStruct] = addClusterAreaASPLabels(answerCell, clusterProposalStruct, clusterInitStr, frameArea, signIDs, clustersStruct);
            %putBreakPointAhead( -1, 'dbclear' );%eval(['dbclear in clusterBasedOnVidCouple at ' num2str(thisLineNr + 5)]);
                        
            %now check if frameArea and frameAreaOut are the same
            %if not - update blobDefCell and their drawings
            
            changedRowCnt = 0;
            if ~isempty(frameAreaOut) && sum(abs(frameArea(:)-frameAreaOut(:)))>0
                disp(['frameArea(' mat2str(frameArea) ') changed to frameArea(' mat2str(frameAreaOut) ')']);
                
                cbA = frameAreaOut;%blobDefCell{lh1_rh2_bh3}(j,4:7);
                blobArea = minDistCell{lh1_rh2_bh3}(cbA(1):cbA(2),cbA(3):cbA(4));               
                blobDefCell{lh1_rh2_bh3}(j,4:7) = frameAreaOut;
                minValMatch_pos_val = getMinBestOfBox(blobArea, cbA(3:4), cbA(1:2));
                blobDefCell{lh1_rh2_bh3}(j,1:3) = minValMatch_pos_val;
                x_im_ID = blobDefCell{lh1_rh2_bh3}(j,1);
                y_im_ID = blobDefCell{lh1_rh2_bh3}(j,2);
                clear blobArea minValMatch_pos_val;
                
                minDistBlock = minDistCell{lh1_rh2_bh3}(frameAreaOut(1):frameAreaOut(2),frameAreaOut(3):frameAreaOut(4));
                [clusterProposalStruct, box_summary_X_struct, box_summary_Y_struct] = analyzeSimilarityArea(signSourceFolder, signIDs, [userID_x userID_y], [signRepeatID_x signRepeatID_y], [x_im_ID y_im_ID], minDistBlock, bestImCrop_LL_RR_B_LR_RL, frameAreaOut, true, lh1_rh2_bh3_new, [LRBstr(lh1_rh2_bh3) 'H-Block'], clusterNameInit);
                clusterProposalFresh.clusterImage = [clusterProposalStruct.bestCropStruct.imCropped{1},clusterProposalStruct.bestCropStruct.imCropped{2}];
                changedRowCnt = 1;
            end
            
            if setClusterID>-1
                %save fig 82 as
                %[LRBstr(lh1_rh2_bh3) 'H-Block' num2str(j,'%02d') '.png']
                %into 
                figFileNameStart = [comparisonFigFolder filesep 's' num2str(signIDs(1)) '_u' num2str(userID_x*10 + signRepeatID_x) '_vs_u' num2str(userID_y*10 + signRepeatID_y) '_' LRBstr{lh1_rh2_bh3} 'H-Block' num2str(j,'%02d')];
                figFileName_01_Fig = [figFileNameStart '_' clusterProposalFresh.clusterName '_s.png'];
                figFileName_02_Xsummary = [ figFileNameStart '_XSummary_d.png'];
                figFileName_03_Ysummary = [figFileNameStart '_YSummary_d.png'];
                h = figure(82);
                saveas(h, figFileName_01_Fig);
                imwrite(box_summary_X_struct.image, figFileName_02_Xsummary);
                imwrite(box_summary_Y_struct.image, figFileName_03_Ysummary);
                
                %createOfAppendDataSet_fileName = [figFileNameStart '_' clusterProposalFresh.clusterName '.mat'];
                %save(createOfAppendDataSet_fileName, 'clusterProposalFresh','clusterProposalStruct','clusterDataSetAdditionStruct');
                
                clustersStruct = addOrCreateCluster(clustersStruct, clusterProposalFresh , clusterDataSetAdditionStruct);
                
                %check for both X and Y
                %if the samples correctly assigned
                dCR_X = NaN(1, frameAreaOut(2)-frameAreaOut(1)+1);
                for xi = frameAreaOut(1):frameAreaOut(2)
                    dCR_X(xi-frameAreaOut(1)+1) = checkIfExistInDataset(clustersStruct.dataset, [signIDs(1), userIDs(1), signRepeatIDs(1)], xi, setClusterID, lh1_rh2_bh3_new(1));
                end
                dCR_Y = NaN(1, frameAreaOut(4)-frameAreaOut(3)+1);
                for yi = frameAreaOut(3):frameAreaOut(4)
                    dCR_Y(yi-frameAreaOut(3)+1) = checkIfExistInDataset(clustersStruct.dataset, [signIDs(2), userIDs(2), signRepeatIDs(2)], yi, setClusterID, lh1_rh2_bh3_new(2));
                end
                %dCR_X and dCR_Y must be all 0 or 1
                a = [dCR_X dCR_Y];
                a(a==1) = [];
                a(a==0) = [];
                if~isempty(a)
                    warning('some frames were labelled differently');
                end
                
                %also if LL
                if lh1_rh2_bh3==4
                    [blobDefCell, changedRowCntAdd] = updateCrossFromMatch(blobDefCell, lh1_rh2_bh3, j, [4 5 6 7], 'LL');
                    changedRowCnt = changedRowCnt + changedRowCntAdd;
                end
                if lh1_rh2_bh3==2
                    [blobDefCell, changedRowCntAdd] = updateCrossFromMatch(blobDefCell, lh1_rh2_bh3, j, [6 7 4 5], 'RR');
                    changedRowCnt = changedRowCnt + changedRowCntAdd;
                end
            else
                %remove the blob selection from current tested block
                %selections
                fAr = blobDefCell{lh1_rh2_bh3}(j,4:7);%frameAreaRemoved
                disp([clusterInitStr '- old block(' num2str(j) ' of ' num2str(size(blobDefCell{lh1_rh2_bh3},1)) ') is removed X(' mat2str(fAr(1:2)) ')-Y(' mat2str(fAr(3:4)) ')']);
                blobDefCell{lh1_rh2_bh3}(j,:) = [];
                changedRowCnt = 1;
                j = j-1;
            end
            
            if changedRowCnt>0
                blocksDrawCell = drawSelectedBlobCells(figIDBegin, blocksDrawCell, blobDefCell, minDistCell);
            end
            blockCount = size(blobDefCell{lh1_rh2_bh3},1);    
% %         if addSuccess
% %             save(rhClusterFileName, 'curClusters');
% %         end
        end        
    end
    
    h = figure(figIDBegin+1);
    saveas(h,[figFileNameStart 'blobsSelected_Final.png']);
    
    croppedHandsFolderName = [signSourceFolder_X filesep 'cropped' filesep];
    rmdir(croppedHandsFolderName,'s');
    croppedHandsFolderName = [signSourceFolder_Y filesep 'cropped' filesep];
    rmdir(croppedHandsFolderName,'s');
    displayClusterStructInfo(clustersStruct, [], 63);
end

function [clusterName, answerCell] = getClusterInfo(clustersASPInfoStruct, LRB_2char, setClusterID, frameArea)
    switch LRB_2char
        case {'LL','LR','RL','RR'}
            curInfo = clustersASPInfoStruct.single(setClusterID);
            %setClusterID, setClusterName, frameArea, pixLim
        case 'BB'
            curInfo = clustersASPInfoStruct.both(setClusterID);
    end
    %curInfo.clusterID;
    %curInfo.clusterName = curLineVec(2);
    %curInfo.representativeFrameSign = curLineVec(3);
    %curInfo.representativeFrameUser = curLineVec(4);
    %curInfo.representativeFrameRepeat = curLineVec(5);
    %curInfo.representativeFrameHand = curLineVec(6);
    %curInfo.representativeFrameFrameID = curLineVec(7);
    clusterName = curInfo.clusterName;
    answerCell = cell(4,2);
    answerCell{1,1} = clusterName;
    answerCell{2,1} = clusterName;
    frameAreaRelativeX = ['1-' num2str(frameArea(2)-frameArea(1)+1)];
    frameAreaRelativeY = ['1-' num2str(frameArea(4)-frameArea(3)+1)];
    answerCell{3,1} = frameAreaRelativeX;
    answerCell{4,1} = frameAreaRelativeY;    
end

%updateCrossFromMatch(lh1_rh2_bh3, j, V1, blockCols)
%[blobDefCell, removedRowCnt] = updateCrossFromMatch(lh1_rh2_bh3=4, j, [5 1], [4 5 6 7])
function [blobDefCell, changedRowCnt] = updateCrossFromMatch(blobDefCell, lh1_rh2_bh3, j, blockCols, LRstr)
    %remove LR's col() match blocks - lh1_rh2_bh3==2
    %if any row match exactly to X_L_fr then remove it
    removedRowIDs_LR = removeRow(blobDefCell{5}, blockCols(1:2), blobDefCell{lh1_rh2_bh3}(j, blockCols(1:2)), []);
    if ~isempty(removedRowIDs_LR)
        for i=removedRowIDs_LR
            fAr = blobDefCell{5}(i,:);%frameAreaRemoved
            disp([LRstr '-From vidXLeft-vidYRight block(' num2str(i) ' of ' num2str(size(blobDefCell{5},1)) ') is removed X(' mat2str(fAr(1:2)) ')-Y(' mat2str(fAr(3:4)) ')']);
        end
        blobDefCell{5}(removedRowIDs_LR,:) = [];
    end

    %remove RL's col() match blocks
    %if any row match exactly to Y_L_fr then remove it                    
    removedRowIDs_RL = removeRow(blobDefCell{1}, blockCols(3:4), blobDefCell{lh1_rh2_bh3}(j, blockCols(3:4)), []);
    if ~isempty(removedRowIDs_RL)
        for i=removedRowIDs_RL
            fAr = blobDefCell{1}(i,:);%frameAreaRemoved
            disp([LRstr '-From vidXRight-vidYLeft block(' num2str(i) ' of ' num2str(size(blobDefCell{1},1)) ') is removed X(' mat2str(fAr(1:2)) ')-Y(' mat2str(fAr(3:4)) ')']);
        end
        blobDefCell{1}(removedRowIDs_RL,:) = [];
    end
    changedRowCnt = length(removedRowIDs_LR) + length(removedRowIDs_RL);
end

function blocksDrawCell = drawSelectedBlobCells(figIDBegin, blocksDrawCell, blobDefCell, minDistCell)
    figure(figIDBegin+1);clf;
    subFigIDVec = [1 4 5 6 3];
    subFigTitStr = {'RL','RR','BB','LL','LR'};
    for i=1:5
        blocksDrawCell{i} = getBlockDraw_From_blobDef(blobDefCell{i}(:,1:end-1), minDistCell{i});
    end
    for i=1:5
        subplot(2,3,subFigIDVec(i));
        imagesc(blocksDrawCell{i});
        title([subFigTitStr{i} '-blobCnt(' num2str(size(blobDefCell{i},1)) ')']);
    end
end

function [removedRowIDs, blocksDraw_im] = removeRow(blockXY, blockCols, frXY, blocksDraw_im)
    removedRowIDs = find(blockXY(:,blockCols(1))==frXY(1) & blockXY(:,blockCols(2))==frXY(2));
    if ~isempty(blocksDraw_im) && ~isempty(removedRowIDs)
        for r=reshape(removedRowIDs,[],length(removedRowIDs))
            blocksDraw_im(blockXY(r,4):blockXY(r,5),blockXY(r,6):blockXY(r,7)) = max(blocksDraw_im(:));
        end
    end
    if ~exist('blocksDraw_im','var')
        blocksDraw_im = [];
    end
    removedRowIDs = reshape(removedRowIDs,1,[]);
end

function plotMinDistImageSC(minDist, figID, handStr, signIDs, userIDs, signRepeatIDs)
    if length(figID)==1
        figure(figID);clf;
    else
        figure(figID(1));
        subplot(figID(2),figID(3),figID(4));
    end
    imagesc(minDist);
    colorbar;
    title({['minDist mat of ' handStr ' Hand'],['SignID(' num2str(signIDs(1)) ')-minVal(' num2str(min(minDist(:))) ')']});
    xlabel({['user ' num2str(userIDs(1)) ', repitition ' num2str(signRepeatIDs(1))],[num2str(size(minDist,1)) ' frames']});
    ylabel({['user ' num2str(userIDs(2)) ', repitition ' num2str(signRepeatIDs(2))],[num2str(size(minDist,2)) ' frames']});
end