function [setClusterID, setClusterName, frameArea, pixLim, bestCenterImageID, isNewCluster] = clusterDialogInterpretXY(curClusterNamesCell, frameArea, answer)
    curClusterCnt = length(curClusterNamesCell);
    
    setClusterID = -1;
    setClusterName = '';
    isNewCluster = false;
    
    pixLim = [];
    bestCenterImageID = [];
    
    %1-this operation will be discarded
    if isempty(answer)
        frameArea = [];
        return;
    else
        dlg01_clusterName_knw = answer{1,1};
        dlg02_clusterName_new = answer{2,1};
        dlg01_clusterName_knw = lower(strtrim(dlg01_clusterName_knw));
        dlg02_clusterName_new = lower(strtrim(dlg02_clusterName_new));
        
        if curClusterCnt>0
            [setClusterID_knw, setClusterName_knw] = findClusterNameAndIDs(dlg01_clusterName_knw, curClusterNamesCell);
            if ~isempty(setClusterID_knw)
                %putBreakPointAhead( 3, 'dbstop' );
                setClusterID = setClusterID_knw;
                setClusterName = setClusterName_knw;
                %putBreakPointAhead( 0, 'dbclear' );
            end
            if setClusterID==-1
                [setClusterID_new, setClusterName_new] = findClusterNameAndIDs(dlg02_clusterName_new, curClusterNamesCell);
                if ~isempty(setClusterID_new)
                    putBreakPointAhead( 3, 'dbstop' );
                    setClusterID = setClusterID_new;
                    setClusterName = setClusterName_new;
                    putBreakPointAhead( 0, 'dbclear' );
                end
            end
        end
        if isempty(setClusterID) || setClusterID==-1
            %setClusterName is proposed in answer{2,1}
            %if setClusterID is still <-1> then increment the last ID
            isNewCluster = true;
            setClusterID = curClusterCnt+1;
            setClusterName = strrep(strtrim(dlg02_clusterName_new),' ','_');
        end
        if isNewCluster && setClusterID~=-1 && strcmp(setClusterName,'')
            putBreakPointAhead( 1, 'dbstop' );
            if strcmp(dlg02_clusterName_new,'') && ~strcmp(dlg02_clusterName_knw,'')
                setClusterName = dlg01_clusterName_knw;
            end
            putBreakPointAhead( -3, 'dbclear' );
        end            

        %if setClusterID is still <-1> then increment the last ID
        if setClusterID==-1
            isNewCluster = true;
            setClusterID = curClusterCnt+1;
        end
        if strcmp(setClusterName,'')
            setClusterName = [clusterType num2str(setClusterID,'%03d')];
        end
        %finally set whatever came from the search and entry
        %clusterIDToBeAdded = setClusterID;
    end
    
    if length(frameArea)==4
        [frameArea, pixLim] = interpretXYarea(frameArea, {answer{3,1},answer{4,1}});
        bestCenterImageID = [];
    elseif length(frameArea)==2 && length(answer)==3
        [frameArea, pixLim] = interpretFramearea(frameArea, answer{3,1});
        bestCenterImageID = str2num(answer{4,1});
    end
end

function [frameArea, pixLim] = interpretXYarea(frameArea, answer)
    [frameAreaX, pixLimX] = interpretFramearea(frameArea(1:2), answer{1,1});
    [frameAreaY, pixLimY] = interpretFramearea(frameArea(3:4), answer{1,2});
    
    frameArea = [frameAreaX, frameAreaY];
    pixLim = [pixLimX, pixLimY];
end
