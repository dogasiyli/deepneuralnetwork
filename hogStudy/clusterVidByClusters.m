function [clusterSuitabilityMat, stripIm] = clusterVidByClusters(signSourceFolder, surVec, figIDBegin, enforceRecrate, frameSlctTresh, stripImSizeVec, predLabelType)
%signSourceFolder = '/mnt/Data/FromUbuntu/HospiSign/HospiMOV';
%signSourceFolder = '/home/dg/DataPath/FromUbuntu/Doga';%work-
%signSourceFolder = '/media/doga/Data/FromUbuntu/Doga';%laptop-msi-ubuntu
%clusterBasedOnVidCouple(signSourceFolder, 112, 2, 3)

%%1. Assign the optional parameters to their default values if necessary
    if ~exist('figIDBegin', 'var') || isempty(figIDBegin)
        figIDBegin = -1;
    end
    if ~exist('enforceRecrate', 'var') || isempty(enforceRecrate)
        enforceRecrate = false;
    end
    if ~exist('frameSlctTresh', 'var') || isempty(frameSlctTresh)
        frameSlctTresh = 1;
    end
    if ~exist('stripImSizeVec', 'var') || isempty(stripImSizeVec)
        stripImSizeVec = [90 10];
    end
    if ~exist('predLabelType', 'var') || isempty(predLabelType)
        %handCountBased is both hands are numbered from 1 to N and single
        %hands are numbered 1 to M
        %allTogether is when cluster IDs are from 1 to M+N
        predLabelType = 'handCountBased';%'allTogether'
    end
    

%%2. Load and display the cluster information known    
    clustersStruct = [signSourceFolder filesep 'clusters_' num2str(surVec(1),'%03d') '.mat'];
    load(clustersStruct);
    if figIDBegin>0
        displayClusterStructInfo(clustersStruct, [], figIDBegin);
        drawnow;
    end
    knownClusterCount = size(clustersStruct.definition.clusterNames,1);
    
%%3. Assign filenames to be used for loading necesary data
    userFolderName = [signSourceFolder filesep num2str(surVec(1)) filesep 'User_' num2str(surVec(2)) '_' num2str(surVec(3))];
    croppedPicsZipName = [userFolderName filesep 'cropped.zip'];
    hogCropFeatsNameFull = [userFolderName filesep 'hog_crop.mat'];
    fileNameVidPerPicLabels = [userFolderName filesep 'perPicLabels.mat'];
    
    if ~exist(hogCropFeatsNameFull,'file')
        getHOGFeatsofHands(signSourceFolder, surVec(1), surVec(2), surVec(3));
    end
    
%%4. Get or create labels of the video    
    if ~exist(fileNameVidPerPicLabels, 'file') || enforceRecrate
        createOrLoadLabels = 'create';
    else
        createOrLoadLabels = 'load';
    end
    hogCellCreateStruct = struct;
    hogCellCreateStruct.signSourceFolder = signSourceFolder;
    hogCellCreateStruct.surVec = surVec;
    [clusterSuitabilityMat,frameLabelMat,frameSlctTresh,frameCnt] = getOLabels(createOrLoadLabels, clustersStruct, userFolderName, frameSlctTresh, hogCellCreateStruct);%,clusterTypeMat
    displayFrameProbabilities(figIDBegin, clusterSuitabilityMat, frameLabelMat, knownClusterCount);
    
    
    unzip(croppedPicsZipName,userFolderName);
    if ~exist('imcropVec_cell','var')
        disp(['Loading <' hogCropFeatsNameFull '> - imcropVec_cell']);
        load(hogCropFeatsNameFull, 'imcropVec_cell');
        disp('Loaded');    
    end
    
    %create the movie label strip
    %for RH, LH, BH
    %the movie strip will have
    %the cropped image - top
    %the found label representative image - {cluster,subCluster}
    %label the centers as image IDs
    stripIm = createStripIm(clustersStruct, imcropVec_cell, clusterSuitabilityMat, surVec, userFolderName, figIDBegin, enforceRecrate, stripImSizeVec, frameCnt, frameSlctTresh, [1 2 3], predLabelType);
    
    %I need to also load the "handUsage.mat" files
    signSourceFolder_X = [signSourceFolder filesep num2str(surVec(1)) filesep 'User_' num2str(surVec(2)) '_' num2str(surVec(3))];
    load([userFolderName filesep 'handUsage.mat'],'handDisplacementMat');
    
%     hand_blocks = createBlocksFromDisplacementMats(frameLabelMat, handDisplacementMat);
% 
%     %TODO - we need hand blocks of a single video

    
    croppedHandsFolderName = [userFolderName filesep 'cropped' filesep];
    rmdir(croppedHandsFolderName,'s');
end

function stripIm = createStripIm(clustersStruct, imcropVec_cell, clusterSuitabilityMat, surVec, userFolderName, figIDBegin, enforceRecrate, stripImSizeVec, frameCnt, frameSlctTresh, handIDsToCreateStripIm, predLabelType)
    if ~exist('handIDsToCreateStripIm', 'var') || isempty(handIDsToCreateStripIm)
        handIDsToCreateStripIm = [1 2 3];
    end
    newImSiz = stripImSizeVec(1);
    imSepSiz = stripImSizeVec(2);
    imTotSiz = sum(stripImSizeVec);
    
    stripImSize = [newImSiz,newImSiz];
    stripIm = cell(length(handIDsToCreateStripIm),1);    
    %create the summary strip
    %for RH, LH, BH
    stripPictureSize = [4*newImSiz+3*imSepSiz,imTotSiz*(frameCnt-1)+newImSiz,3];
    for handID = handIDsToCreateStripIm%LRB-1:3
        %clusterSuitabilityMat{j,1} = .{bestCropInfo,clusterProbs, clusterProbsBestCropId}
        LRBchar = getLRBChar(handID);
        stripImFileName = [userFolderName filesep 'vidSummary' LRBchar 'H.png'];
        stripIm{handID,1} = [];
        if exist(stripImFileName,'file')
            stripIm{handID,1} = imread(stripImFileName);
        end
        loadedPictureSize = size(stripIm{handID,1});
        if ~enforceRecrate && ~isempty(stripIm{handID,1}) && (length(loadedPictureSize)==3 && sum(abs(stripPictureSize-loadedPictureSize)))
            %the image already created and now loaded
            continue
        end
        stripIm{handID,1} = zeros(stripPictureSize, 'uint8');
        for frameID=1:frameCnt
 
            bestCropInfo = clusterSuitabilityMat{handID,1}.bestCropInfo(frameID, :);

            %the crop area info
            cropCnt = bestCropInfo(1);
            clusterID = bestCropInfo(2);
            subClusterID = bestCropInfo(3);
            imBox = imcropVec_cell{frameID,handID}(cropCnt,:);

            %for this frame there are many crops
            srcFold = [userFolderName filesep 'cropped' filesep];
            srcIm = imread([srcFold 'crop_' LRBchar 'H_' num2str(frameID,'%03d') '.png']);
            srcIm_boxed = drawRectOnImg(imresize(srcIm,[200 200]), imBox([3 1]), [imBox(4)-imBox(3)+1 imBox(2)-imBox(1)+1], [1 0 1], 10);
            srcIm = imresize(srcIm,stripImSize);
            
            %
            subClusterImage = clustersStruct.definition.subClusterImages{clusterID, subClusterID};
            subClusterImage_length = length(subClusterImage);
            subClusterImage_rMulc = subClusterImage_length/3;
            rc = sqrt(subClusterImage_rMulc);
            subClusterImage =reshape(subClusterImage,[rc,rc,3]);
            if frameID<10
                rs = floor(newImSiz/3);
            elseif frameID<100
                rs = 2*floor(newImSiz/3);
            else
                rs = newImSiz;
            end
            numIm = getNumberImage(frameID, newImSiz, rs, stripImSize);
            if clusterID<10
                rs = floor(newImSiz/3);
            elseif clusterID<100
                rs = 2*floor(newImSiz/3);
            else
                rs = newImSiz;
            end
            subClusName = clustersStruct.definition.clusterNames{clusterID,1};
            datasetCheckResult = checkIfExistInDataset(clustersStruct.dataset, surVec, frameID, clusterID, handID);
            if predLabelType=='handCountBased'
                clusterID = getHandBasedClusterID(clustersStruct, clusterID);   
            end
            clusterIDIm = getNumberImage(clusterID, newImSiz, rs, stripImSize);
            acceptedToCluster = bestCropInfo(6)>=frameSlctTresh;
            
            if figIDBegin>0
                figure(figIDBegin+2);clf;
                subplot(2,2,1); imshow(srcIm);title(['srcIm - ' LRBchar ' - ' num2str(frameID)]);
                subplot(2,2,2); imshow(srcIm_boxed);title('srcIm_{boxed}');
                if acceptedToCluster
                    subplot(2,2,3); 
                else
                    subplot(2,2,4); 
                end
                imshow(subClusterImage);title({[num2str(clusterID) '-' subClusName],mat2str(bestCropInfo(4:6),4)});
                if acceptedToCluster
                    subplot(2,2,4); 
                else
                    subplot(2,2,3); 
                end
                imshow(numIm);title('imageID');
                drawnow;
            end
            
            %numIm should go to first row
            if acceptedToCluster
                stripIm{handID,1}(1:newImSiz, 1+(frameID-1)*imTotSiz:frameID*imTotSiz-imSepSiz,2) = uint16(255*numIm);%green
            else
                stripIm{handID,1}(1:newImSiz, 1+(frameID-1)*imTotSiz:frameID*imTotSiz-imSepSiz,1) = uint16(255*numIm);%red
            end
            %the video image should go second row
            stripIm{handID,1}(1*imTotSiz:1*imTotSiz+newImSiz-1, 1+(frameID-1)*imTotSiz:frameID*imTotSiz-imSepSiz,:) = imresize(srcIm_boxed,stripImSize);
            
            %the selected subclusterImage should go
            %3rd row if >=1
            %4th row else
            %GREEN -not found in labels
            %BLUE  -found as equal to suggestion
            %YELLOW-found as NOT equal to suggestion
            %RED   -not accepted as cluster
            if acceptedToCluster
                stripIm{handID,1} = insertPicturePart(stripIm{handID,1}, subClusterImage, 2, frameID, stripImSizeVec, []);
                %stripIm{j,1}(2*imTotSiz:2*imTotSiz+newImSiz-1, 1+(i-1)*imTotSiz:i*imTotSiz-imSepSiz,:) = imresize(subClusterImage,stripImSize);
                colorID = {2,3,[1 2]};%colorID = {datasetCheckResult-explanation - color} --- {0-not found-green},{1-found as equal to suggestion-blue},{2-found as NOT equal to suggestion-yellow}
                stripIm{handID,1} = insertPicturePart(stripIm{handID,1}, uint16(255*clusterIDIm), 3, frameID, stripImSizeVec, colorID{1,datasetCheckResult+1});
            else
                stripIm{handID,1} = insertPicturePart(stripIm{handID,1}, uint16(255*clusterIDIm), 2, frameID, stripImSizeVec, 1);
                stripIm{handID,1} = insertPicturePart(stripIm{handID,1}, subClusterImage, 3, frameID, stripImSizeVec, []);
            end            
        end
        imwrite(stripIm{handID,1}, stripImFileName);
    end
end

function displayFrameProbabilities(figIDBegin, clusterSuitabilityMat, frameLabelMat, knownClusterCount)
    %R B L
    %1 2 3
    %4 5 6
    spm = [3 6;1 4;2 5];    
    frameCnt = size(frameLabelMat,1);
    if figIDBegin>0
        figure(figIDBegin+1);
        clf;
        for j = 1:3%LRB
            LRBchar = getLRBChar(j);
            %R B L
            %1 2 3
            %4 5 6
            subplot(2,3,spm(j,1));
            frmLabs = frameLabelMat(:,j);
            xIDs = find(frmLabs);
            frmLabs = frmLabs(xIDs);
            plot(xIDs, frmLabs,'k*');title(['Label of ' LRBchar]);
            xlim([1 frameCnt]);
            ylim([0 knownClusterCount]);
            subplot(2,3,spm(j,2));
            bci = clusterSuitabilityMat{j,1}.bestCropInfo;
            plot(1:frameCnt, bci(:, 6));title('Prob of label');        
            xlim([1 frameCnt]);
            drawnow;
        end
    end
end

function [clusterSuitabilityMat,frameLabelMat,frameSlctTresh,frameCnt,clusterTypeMat] = getOLabels(createOrLoadLabels, clustersStruct, userFolderName, frameSlctTresh, hogCellCreateStruct)
    LRBstr = 'LRB';
    knownClusterCount = size(clustersStruct.definition.clusterNames,1);
    hogCropFeatsNameFull = [userFolderName filesep 'hog_crop.mat'];
    fileNameVidPerPicLabels = [userFolderName filesep 'perPicLabels.mat'];
    switch createOrLoadLabels
        case 'create'
            clusTypes = clustersStruct.definition.clusterTypes;
            clusterTypeMat = false(knownClusterCount,2);%col01-singleHand,col02-bothHands
            for i = 1:knownClusterCount
                clusterTypeMat(i,:) = [strcmpi('single', clusTypes{i,1}) strcmpi('both', clusTypes{i,1})];
            end
            disp(['Loading <' hogCropFeatsNameFull '> (hogImArr_cell)']);
            load(hogCropFeatsNameFull, 'hogImArr_cell');
            disp(['Loaded hogImArr_cell of size ' mat2str(size(hogImArr_cell))]);
            if size(hogImArr_cell,2)<3
                disp('hogImArr_cell doesnt have 3 cells hence needs to be recreated..');
                getHOGFeatsofHands(hogCellCreateStruct.signSourceFolder, hogCellCreateStruct.surVec(1), hogCellCreateStruct.surVec(2), hogCellCreateStruct.surVec(3), [], true);
                load(hogCropFeatsNameFull, 'hogImArr_cell');
            end

            %load all the hog feats 1 by 1,
            %run - [clusterSuitabilityVec, minDistClusterInds, clusterProbsVec] = findBestClusterIDVec(clustersStruct, hogFeat_asHoGVec, clusterID)
            %by not knowing the clusterID
            %suppose it can come from any cluster
            %then per stable blocks-or not-decide what to ask user as labels of
            %video
            frameCnt = size(hogImArr_cell,1); %#ok<USENS>          
            clusterSuitabilityMat = cell(3,1);          
            frameLabelMat = zeros(frameCnt,3);
            for j = 1:3%LRB
                clusterSuitabilityMat{j,1} = struct;
                clusterSuitabilityMat{j,1}.bestCropInfo = zeros(frameCnt,6);%1_cropCnt/2_clusterID/3_subClusterID/4_allowedDist/5_dist/6_prob
                clusterSuitabilityMat{j,1}.clusterProbs = zeros(frameCnt,knownClusterCount);
                clusterSuitabilityMat{j,1}.clusterProbsBestCropId = zeros(frameCnt,knownClusterCount);
                for i=1:frameCnt
                    LRBchar = LRBstr(j);
                    hogImArr_cell_X_lrb = hogImArr_cell{i,j};
                    %for this frame there are many crops
                    cropCnt = size(hogImArr_cell_X_lrb,2);
                    clusterProbsVecAll = zeros(knownClusterCount*cropCnt,6);
                    for k=1:cropCnt
                        hogVec_X = hogImArr_cell_X_lrb(:,k)';
                        [clusterSuitabilityVec, minDistClusterInds, clusterProbsVec] = findBestClusterIDVec(clustersStruct, hogVec_X); %#ok<ASGLU>
                        clusterProbsVecAll(1+((k-1)*knownClusterCount):k*knownClusterCount, :) = [ones(knownClusterCount,1)*k clusterProbsVec];
                    end
                    bestCropInfo = zeros(knownClusterCount, 6);
                    for k=1:knownClusterCount
                        if j<3
                            %single hand
                            bestCropAcceptable = clusterTypeMat(k,1);
                        else
                            %both hands
                            bestCropAcceptable = clusterTypeMat(k,2);
                        end

                        if bestCropAcceptable
                            clusterProbsVec_k = clusterProbsVecAll(clusterProbsVecAll(:,2)==k,:);
                            
                            clusterProbsVec_k(clusterProbsVec_k(:,6)>=frameSlctTresh,6) = frameSlctTresh;
                            clusterProbsVec_k = sortrows(clusterProbsVec_k, [-6 +5]);
                            
                            bestCropInfo_k = clusterProbsVec_k(1,:);
                            clusterSuitabilityMat{j,1}.clusterProbs(i,k) = bestCropInfo_k(6);
                            clusterSuitabilityMat{j,1}.clusterProbsBestCropId(i,k) = bestCropInfo_k(1);
                            bestCropInfo(k,:) = bestCropInfo_k;
                        end
                     end

                    bestCropInfo(bestCropInfo(:,6)>=frameSlctTresh,6) = frameSlctTresh;
                    bestCropInfo = sortrows(bestCropInfo, [-6 +5]);

                    bestCropInfo = bestCropInfo(1,:);        
                    if bestCropInfo(6)>=frameSlctTresh
                        frameLabelMat(i,j) = bestCropInfo(2);
                    end
                    clusterSuitabilityMat{j,1}.bestCropInfo(i, :) = bestCropInfo;
                end
            end
            disp(['saving(clusterTypeMat,frameCnt,clusterSuitabilityMat,frameSlctTresh,frameLabelMat) into <' fileNameVidPerPicLabels '>'])
            save(fileNameVidPerPicLabels, 'clusterTypeMat','frameCnt','clusterSuitabilityMat','frameSlctTresh','frameLabelMat');
            disp('saved')
        case 'load'
            disp(['loading(clusterTypeMat,frameCnt,clusterSuitabilityMat,frameSlctTresh,frameLabelMat) from <' fileNameVidPerPicLabels '>'])
            load(fileNameVidPerPicLabels, 'clusterTypeMat','frameCnt','clusterSuitabilityMat','frameSlctTresh','frameLabelMat');
            disp('loaded')
    end    
end
