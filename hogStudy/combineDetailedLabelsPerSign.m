function [detailedLabelsRet, signCount, labelsCount] = combineDetailedLabelsPerSign(detailedLabels)
    %detailedLabels has 6 or 5 cols
    %6th col has either my own labels like for signID=7, 1 to 4
    % or it has only 1 single label = 1
    detailedLabelsRet = detailedLabels;%detailedLabels
    [sampleCount, labelColID] = size(detailedLabelsRet);
    labelsCount = 0;
    if labelColID==5
        uniqueSignIDs = unique(detailedLabelsRet(:,1));
        signCount = length(uniqueSignIDs);
        detailedLabelsRet = [detailedLabelsRet zeros(sampleCount,1)];
        if signCount>1
            for si = 1:length(uniqueSignIDs)
                currentSignID = uniqueSignIDs(si);
                relatedRowIDs = find(detailedLabelsRet(:,1)==currentSignID);
                labelsCount = labelsCount + 1;
                detailedLabelsRet(relatedRowIDs,6) = labelsCount;
            end
        end
    elseif labelColID==6
        %uniqueLabelsAssigned = unique(detailedLabelsRet(:,6));
        %initialLabelCount = length(uniqueLabelsAssigned);
        uniqueSignIDs = unique(detailedLabelsRet(:,1));
        signCount = length(uniqueSignIDs);
        %if signCount>1
        for si = 1:length(uniqueSignIDs)
            currentSignID = uniqueSignIDs(si);
            relatedRowIDs = find(detailedLabelsRet(:,1)==currentSignID);
            uniqueLabelsCurSign = unique(detailedLabelsRet(relatedRowIDs,6));
            uniqueLabelsCurSign_Count = length(uniqueLabelsCurSign);
            newLabelIDs = (labelsCount+1):(labelsCount+uniqueLabelsCurSign_Count);
            labelsCount = labelsCount + uniqueLabelsCurSign_Count;
            newLabelsCurSign = reSetLabels(detailedLabelsRet(relatedRowIDs,6), reshape(uniqueLabelsCurSign,1,[]), reshape(newLabelIDs,1,[]));
            detailedLabelsRet(relatedRowIDs,6) = newLabelsCurSign;
        end
        %end
    else
        error('unknow type of matrix');
    end
end