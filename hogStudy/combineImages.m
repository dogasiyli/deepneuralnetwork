function combImg = combineImages(stripImCell, stripSizes)
    totRowCnt = sum(stripSizes(:,1));
    maxColSize = max(stripSizes(:,2));
    combImg = cell(1,3);
    %now combine all into 1 image
    for handID=1:3
        combImg{1,handID} = zeros([totRowCnt maxColSize 3], 'uint8');
    end    
    listElemntCnt = size(stripImCell,2);
    for l = 1:listElemntCnt
        rFr = 1+sum(stripSizes(1:l-1,1));
        rTo = sum(stripSizes(1:l,1));
        cTo = stripSizes(l,2);
        for handID=1:3
            combImg{1,handID}(rFr:rTo,1:cTo,:) = stripImCell{handID,l};
        end
    end
end