function [cropFilledBody, cropFilledLH, cropFilledRH] = compareFillMethodsDepthImage_Berk(srcFolder, imageID, cropAreaSize, figIDs, boxSur, changeGivenValues)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

%% 0. assign unpassed optional params
    if ~exist('cropAreaSize','var')
        %the area around the hand center
        cropAreaSize = 30;
    end
    if ~exist('figIDs','var')
        figIDs = 1:5;
    end
    if ~exist('boxSur','var')
        boxSur = [10 3];
    end
    if ~exist('changeGivenValues','var') || isempty(changeGivenValues)
        %gridFit also changes the given values at oints
        %if this is false thias algorithm will not change those values
        changeGivenValues = false;
    end

    
%% 1. Load necessary variables from file    
    [depthIm, skeleton] = func01_loadData(srcFolder, imageID);
    
%% 2. Deal with faulty pixels - bigger than 1900 - farther than 1.9 meters    
    [depthIm] = func02_FaultyPixelsOperations(depthIm);  
    
%% A01. Some auxiliary functions
    %[rowCount_y,colCount_x] = size(depthIm);
    %[rowIndsVectorized,colIndsVectorized]=meshgrid(1:rowCount_y,1:colCount_x);     

%% 3. Fill the 0's of full body image 
    [depthImFilled_gridFit, valsStruct_gridFit] = fill0s_gridFit(depthIm, changeGivenValues);
    [depthImFilled_ah, fillHistB] = fill0s(depthIm, boxSur(1));
    
    midTitleStr01 = 'Original full body depth image with missing values';
    draw01_compareFillBodyResults(figIDs(1), depthIm, depthImFilled_ah, boxSur(1), depthImFilled_gridFit, valsStruct_gridFit, changeGivenValues, midTitleStr01);

%% 4. Get Hand images - both filled and not filled
    [cropIm_L_gf01, cropIm_L_ah01, cropIm_L] = getHand(depthImFilled_gridFit, depthImFilled_ah, depthIm, skeleton, imageID, cropAreaSize, 'Left');
    [cropIm_R_gf01, cropIm_R_ah01, cropIm_R] = getHand(depthImFilled_gridFit, depthImFilled_ah, depthIm, skeleton, imageID, cropAreaSize, 'Right');

%% 5. fill the cropped hand images with 2 different methods
    boxSur2 = 3;
    [cropIm_L_gf02, cropIm_L_valsStruct_gridFit02] = fill0s_gridFit(cropIm_L, changeGivenValues);
    [cropIm_L_ah02, cropIm_L_imColor, cropIm_L_grpIDs, cropIm_L_grpMedian, cropIm_L_fillHist] = reAdjustDepthImHand(cropIm_L, boxSur2); %#ok<ASGLU>
    [cropIm_R_gf02, cropIm_R_valsStruct_gridFit02] = fill0s_gridFit(cropIm_R, changeGivenValues);
    [cropIm_R_ah02, cropIm_R_imColor, cropIm_R_grpIDs, cropIm_R_grpMedian, cropIm_R_fillHist] = reAdjustDepthImHand(cropIm_R, boxSur2); %#ok<ASGLU>

%% 6. compare cropped hand images with imagesc and surf
    midTitleStr02 = 'Original Left Hand depth image with missing values';
    %fig02 - left hand - hand filled comparison
    cropIm_L_valsStruct_gridFit01 = depthImTogridFitStruct(cropIm_L_gf01);
    draw01_compareFillBodyResults(figIDs(2), cropIm_L, cropIm_L_ah02, boxSur(2), cropIm_L_gf02, cropIm_L_valsStruct_gridFit02, changeGivenValues, midTitleStr02);
    %fig03 - left hand - body filled comparison
    draw01_compareFillBodyResults(figIDs(3), cropIm_L, cropIm_L_ah01, boxSur(1), cropIm_L_gf01, cropIm_L_valsStruct_gridFit01, changeGivenValues, midTitleStr02);

    midTitleStr03 = 'Original Right Hand depth image with missing values';
    %fig04 - right hand - hand filled comparison
    cropIm_R_valsStruct_gridFit01 = depthImTogridFitStruct(cropIm_R_gf01);
    draw01_compareFillBodyResults(figIDs(4), cropIm_R, cropIm_R_ah02, boxSur(2), cropIm_R_gf02, cropIm_R_valsStruct_gridFit02, changeGivenValues, midTitleStr03);
    %fig05 - right hand - body filled comparison
    draw01_compareFillBodyResults(figIDs(5), cropIm_R, cropIm_R_ah01, boxSur(1), cropIm_R_gf01, cropIm_R_valsStruct_gridFit01, changeGivenValues, midTitleStr03);
    
    cropFilledBody = depthImFilled_gridFit;
    cropFilledLH = cropIm_L_gf02;
    cropFilledRH = cropIm_R_gf02;    
end

function [depthIm, skeleton] = func01_loadData(srcFolder, imageID)
    depth = [];
    skeleton = [];    
    load([srcFolder filesep 'depth.mat']);
    load([srcFolder filesep 'skeleton.mat']);
    depthIm = depth(:,:,imageID);
end

function [depthIm] = func02_FaultyPixelsOperations(depthIm)
    depthIm(depthIm>1900) = 0;
end

function draw01_compareFillBodyResults(figID, depthIm, depthImFilled_ah, boxSur, depthImFilled_gridFit, valsStruct_gridFit, changeGivenValues, midTitleStr)
    figure(figID);clf;hold on;
    %1 3 5
    %2 4 6
    [rowCount_y,colCount_x] = size(depthIm);
    
    %1-gridfit depth image - imagesc
    subplot(3,2,1);
    imagesc(depthImFilled_gridFit);colorbar;
    title(['Filled with gridFit method. changeGivenValues(' num2str(changeGivenValues) ')']);
    %2-original depth image - surf
    subplot(3,2,2);
    colormap(hot(256));
    h = surf(valsStruct_gridFit.x,valsStruct_gridFit.y,-valsStruct_gridFit.z);
    view(-140,70);
    set(h,'LineStyle','none');

    %3-original depth image - imagesc
    subplot(3,2,3);
    imagesc(depthIm);colorbar;
    title(midTitleStr);
    %4-original depth image - surf
    subplot(3,2,4);
    colormap(hot(256));
    h = surf(1:colCount_x,1:rowCount_y,double(depthIm));
    view(-140,70);
    set(h,'LineStyle','none');

    %5-adhoc filled depth image - imagesc
    subplot(3,2,5);
    imagesc(depthImFilled_ah);colorbar;
    title(['Filled with adHoc method. boxSur(' num2str(boxSur) ')']);
    %6-adhoc filled depth image - surf
    subplot(3,2,6);
    colormap(hot(256));
    h = surf(1:colCount_x,1:rowCount_y,-double(depthImFilled_ah));
    view(-140,70);
    set(h,'LineStyle','none');
end

function [cropIm_gf, cropIm_ah, cropIm] = getHand(depthIm_gf, depthIm_ah, depthIm, skeleton, imageID, cropAreaSize, handStr) %#ok<INUSL> skeleton and imageID is in eval
    %handStr - 'Left' or 'Right'
    %handPos = skeleton.HandLeft(imageID,10:11);
    handPos = [];
    eval(['handPos = skeleton.Hand' handStr '(imageID,10:11);']);
    
    %cropAreaSize = 30;
    cropArea_Beg = round(handPos-cropAreaSize);
    cropArea_End = round(handPos+cropAreaSize);
    crpArea = [cropArea_Beg(2) cropArea_End(2) cropArea_Beg(1) cropArea_End(1)];%[ySt yEn xSt xEn]
    
    cropIm_gf = depthIm_gf(crpArea(1):crpArea(2),crpArea(3):crpArea(4));
    cropIm_ah = depthIm_ah(crpArea(1):crpArea(2),crpArea(3):crpArea(4));
    cropIm = depthIm(crpArea(1):crpArea(2),crpArea(3):crpArea(4));
end
