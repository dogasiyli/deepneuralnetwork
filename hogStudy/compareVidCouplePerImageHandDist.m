function [minDist_XL_YL, minDist_XR_YR, minDist_B, minDist_XL_YR, minDist_XR_YL] = compareVidCouplePerImageHandDist(srcFold, signIDs, userIDs, signRepeatIDs, hogVersion)
%compareVidCouplePerImageHandDist(signSourceFolder, 112, [2 5], [3 1])
    %signSourceFolder = '/mnt/Data/FromUbuntu/HospiSign/HospiMOV';
    %signSourceFolder = '/home/dg/DataPath/FromUbuntu/Doga';%work-
    %signSourceFolder = '/media/doga/Data/FromUbuntu/Doga';%laptop-msi-ubuntu
    
    userID_x = userIDs(1); signRepeatID_x = signRepeatIDs(1);
    userID_y = userIDs(2); signRepeatID_y = signRepeatIDs(2);
    
    if length(signIDs)>1
        saveFileName = [srcFold filesep 's' num2str(signIDs(1)) 'vs' num2str(signIDs(2)) '_u' num2str(userID_x*10 + signRepeatID_x) '_vs_u' num2str(userID_y*10 + signRepeatID_y) '_v(' num2str(hogVersion,'%d') ').mat'];        
    elseif length(signIDs)==1
        saveFileName = [srcFold filesep 's' num2str(signIDs) '_u' num2str(userID_x*10 + signRepeatID_x) '_vs_u' num2str(userID_y*10 + signRepeatID_y) '_v(' num2str(hogVersion,'%d') ').mat'];
        signIDs = [signIDs signIDs];
    end
    
    if exist(saveFileName, 'file')
        try
            tic;disp(['Loading ' saveFileName '(signIDsSaved,imIDEnds,minDist_XL_YL,minDist_XR_YR,minDist_B,minDist_XL_YR,minDist_XR_YL)']);
            load(saveFileName, 'signIDsSaved', 'imIDEnds', 'minDist_XL_YL', 'minDist_XR_YR', 'minDist_B', 'minDist_XL_YR', 'minDist_XR_YL');
            toc;disp(['Loaded ' saveFileName]);
            errStr = ['File(' saveFileName ') already exists with (signIDsSaved,imIDEnds,minDist_XL_YL,minDist_XR_YR,minDist_B,minDist_XL_YR,minDist_XR_YL)'];
            if ~exist('minDist_XL_YL','var') || ~exist('minDist_XL_YL','var') || ~exist('minDist_XL_YL','var') || ~exist('minDist_XL_YL','var') || ~exist('minDist_XL_YL','var')
                error(errStr);
            end
            disp(errStr);
            return;
        catch
            disp(['File(' saveFileName ') has problems with ingredients --> (signIDsSaved,imIDEnds,minDist_XL_YL,minDist_XR_YR,minDist_B,minDist_XL_YR,minDist_XR_YL)']);
        end
    end        
    tic;disp(['Loading (precalcedHOGImCropVecs) - s(' mat2str(signIDs) ')_u(' mat2str(userIDs) ')_r(' mat2str(signRepeatIDs) ')_v(' num2str(hogVersion,'%d') ')']);
    precalcedHOGImCropVecs = getPrecalcedHOGImCropVecs(srcFold,  signIDs, userIDs, signRepeatIDs, hogVersion);
    toc;disp('Loaded (precalcedHOGImCropVecs)');
    
    assert(hogVersion~=1,'hogVersion==2 not implemented yet')

    imIDEnd_x = size(precalcedHOGImCropVecs.hogImArr_cell_X,1);
    imIDEnd_y = size(precalcedHOGImCropVecs.hogImArr_cell_Y,1);
    imIDEnds = [imIDEnd_x imIDEnd_y]; %#ok<NASGU>

    minDist_XL_YL = inf(imIDEnd_x, imIDEnd_y);
    minDist_XR_YR = inf(imIDEnd_x, imIDEnd_y);
    minDist_B = inf(imIDEnd_x, imIDEnd_y);
    minDist_XL_YR = inf(imIDEnd_x, imIDEnd_y);
    minDist_XR_YL = inf(imIDEnd_x, imIDEnd_y);
    
    bestImCrop_LL_RR_B_LR_RL = inf(imIDEnd_x, imIDEnd_y, 5);

    for xi = 1:imIDEnd_x %2-2
        for yj = 1:imIDEnd_y %5-1
            %[minDistLRB, bestImCrop_LRB_cur] = drawFoundBoxes_compareFunc(signSourceFolder, signIDs, [userID_x userID_y], [signRepeatID_x signRepeatID_y], [xi yj], precalcedHOGImCropVecs);
            [minDist_LL_RR_B_LR_RL, bestImCrop_LL_RR_B_LR_RL_cur] = drawFoundBoxes_compareFunc(srcFold, signIDs, [userID_x userID_y], [signRepeatID_x signRepeatID_y], [xi yj], precalcedHOGImCropVecs);
            minDM = minDist_LL_RR_B_LR_RL;
            if (mod(xi,10)==0 && mod(yj,10)==0 )
                disp(['xi(' num2str(xi) ' of ' num2str(imIDEnd_x) '-' num2str(yj) ' of ' num2str(imIDEnd_y) ')yj�***�LLH(' num2str(minDM(1),'%4.2f') ')--RRH(' num2str(minDM(2),'%4.2f') ')--BH(' num2str(minDM(3),'%4.2f') ') *** LRH(' num2str(minDM(4),'%4.2f') ')--RLH(' num2str(minDM(5),'%4.2f') ')' ]);
            end
            minDist_XL_YL(xi, yj) = minDist_LL_RR_B_LR_RL(1);
            minDist_XR_YR(xi, yj) = minDist_LL_RR_B_LR_RL(2);
            minDist_B(xi, yj) = minDist_LL_RR_B_LR_RL(3);
            minDist_XL_YR(xi, yj) = minDist_LL_RR_B_LR_RL(4);
            minDist_XR_YL(xi, yj) = minDist_LL_RR_B_LR_RL(5);
            bestImCrop_LL_RR_B_LR_RL(xi, yj, :) = bestImCrop_LL_RR_B_LR_RL_cur;
        end
    end
    signIDsSaved = signIDs; %#ok<NASGU>
    save(saveFileName, 'signIDsSaved', 'userIDs', 'signRepeatIDs', 'imIDEnds', 'minDist_XL_YL', 'minDist_XR_YR', 'minDist_B', 'minDist_XL_YR', 'minDist_XR_YL', 'bestImCrop_LL_RR_B_LR_RL');
end