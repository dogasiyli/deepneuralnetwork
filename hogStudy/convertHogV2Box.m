function [ boxArea ] = convertHogV2Box(patchID_im01, imcropVec_struct, imageSize)
    rows = patchID_im01([1 2]);
    cols = patchID_im01([3 4]);
    hogBoxes = patchID_im01([5 6]);
    
    imRowSize = imageSize(1);
    rySt = 1+floor(linspace(0,imRowSize,hogBoxes(1)));
    imColSize = imageSize(2);
    cxSt = 1+floor(linspace(0,imColSize,hogBoxes(2)));
    
    boxArea = [rySt(rows) cxSt(cols)];
end

