function [combImg, problematicVids] = createBigVideoExp(srcFold, surSlctListStruct)
    %operationStr = {'extractHog','surfNormVidsPics','cropVidFrames'};
    %srcFold = {'/media/doga/Data/FromUbuntu/Doga', 'D:\FromUbuntu\Doga', 'C:\FromUbuntu\HospiSign\HospiMOV'}
    %surSlctListStruct_01 = struct('createMode', 1, 'signIDList',[87 593],'userList',2:7,'maxRptID',10);
    %surSlctListStruct_02 = struct('createMode', 2, 'signIDList',[7 13 49 69 74 85 86 87 88 586 593 636],'userList',3,'maxRptID',1);
    %surSlctListStruct_03 = struct('createMode',1,'signIDList',1:700,'userList',2:7,'maxRptID',10);
    %surSlctListStruct_04 = struct('createMode',2,'signIDList',[7 13 49 69 74 85 86 88 586 636],'userList',3,'maxRptID',1);
    %pV_01 = loopOnHospiSign(srcFold{1}, surSlctListStruct_01, false);
    %pV_02 = loopOnHospiSign(srcFold{2}, surSlctListStruct_02, false);
    %pV_03 = loopOnHospiSign(srcFold{3}, surSlctListStruct_04, false);
    createMode = surSlctListStruct.createMode; 
    signIDList = surSlctListStruct.signIDList; 
    userList = surSlctListStruct.userList; 
    maxRptID = surSlctListStruct.maxRptID;    
    if isempty(srcFold)
        srcFold = getVariableByComputerName('srcFold');
    end
    problematicVids = [];
    switch createMode
        case 1 
            disp('create mode(userList)');
            [surList, listElemntCnt] = createSURList(srcFold, 1, struct('signIDList',signIDList,'userList',userList,'maxRptID',maxRptID));
        case 2
            disp('create mode(maxRepCnt)');
            %signIDList, userID, maxRptCnt
            [surList, listElemntCnt] = createSURList(srcFold, 2, struct('signIDList',signIDList,'userID',userList,'maxRptCnt',maxRptID));
    end
    disptable([(1:listElemntCnt)' surList], 'rID|sign|user|repition');
    stripImCell = cell(3,listElemntCnt);
    stripSizes = zeros(listElemntCnt,2);
    labelSummary = getlabelSummary(srcFold, surList, listElemntCnt);
    
    labelCnt_single = labelSummary.labelCells{:,1:2};
    labelsSingle = unique(labelCnt_single(:));
    labelCnt_double = labelSummary.labelCells{:,3};
    labelsDouble = unique(labelCnt_double(:));
    randomColors_S = getRandomColorsMat(length(labelsSingle));
    randomColors_D = getRandomColorsMat(length(labelsDouble));
    
    stripImSizeVec = [25 5];    
    for l = 1:listElemntCnt
        s = surList(l,1);
        u = surList(l,2);
        r = surList(l,3);
        theFoldName = [srcFold filesep num2str(s) filesep 'User_' num2str(u) '_' num2str(r)];
        disp(['will try to go into folder <' theFoldName '>'])
        %try
            [stripIm, frameCnt] = createStripIm_NoPrediction(theFoldName, [], true, stripImSizeVec, [1 2 3]);
            
            for handID=1:3
                im = stripIm{handID,1};
                labelsAssigned = labelSummary.labelCells{l,handID};
                if (length(labelsAssigned)<frameCnt)
                    labelsAssigned(length(labelsAssigned):frameCnt) = labelsAssigned(length(labelsAssigned));
                elseif (length(labelsAssigned)>frameCnt)
                    labelsAssigned = labelsAssigned(1:frameCnt);
                end
                if handID<3
                    labelsStripe = createLabelStripe(15,stripImSizeVec,randomColors_S,labelsSingle(:),labelsAssigned);
                else
                    labelsStripe = createLabelStripe(15,stripImSizeVec,randomColors_D,labelsDouble(:),labelsAssigned);
                end
                
                stripImCell{handID,l} = im;
                stripImCell{handID,l}(size(im,1)+1:size(im,1)+size(labelsStripe,1),:,:) = labelsStripe;
            end            
            outVal  = ['video summary files created. summary size is ' mat2str(size(stripIm{1,1})) '\r\n'];
            %outVal  = [outVal 'singleImage resized to' mat2str(stripImSizeVec) '\r\n'];
            %outVal  = [outVal 'each summary size is ' mat2str(size(stripIm{1,1})) '\r\n'];
            disp(outVal);
            stripSizes(l,1) = size(stripImCell{1,l},1);
            stripSizes(l,2) = size(stripImCell{1,l},2);
        %catch err
        %    disp(['ERROR = ' err])
        %    problematicVids = [problematicVids;s u r]; %#ok<AGROW>
        %end
    end
    combImg = combineImages(stripImCell, stripSizes);
end


% 
% function [someMissing, labelFilesExist] = checkLabelFiles(theFoldName, single0_double1)
%     labelFilesExist = [false false false];
%     try
%         labelFolder = [theFoldName filesep 'labelFiles' filesep ];
%         labelFile_lh = exist([labelFolder 'labelsCS_LH.txt'],'file');
%         labelFile_rh = exist([labelFolder 'labelsCS_RH.txt'],'file');
%         labelFile_bh = exist([labelFolder 'labelsCS_BH.txt'],'file');
%         labelFilesExist = [labelFile_lh labelFile_rh labelFile_bh];
%         switch (single0_double1)
%             case 0
%                 someMissing = ~(labelFile_lh && labelFile_rh);
%             case 1
%                 someMissing = ~labelFile_bh;
%         end
%     catch err
%         disp(['err in checkLabelFiles (' err.message ')']);
%     end
% end