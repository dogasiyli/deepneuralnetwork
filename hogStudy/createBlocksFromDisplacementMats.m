function hand_blocks = createBlocksFromDisplacementMats(handDisplacementMat_X,handDisplacementMat_Y)
    %XLH_YLH
    XLH_YLH_block_struct = getBlocks(handDisplacementMat_X.LH, handDisplacementMat_Y.LH);
    %XRH_YRH
    XRH_YRH_block_struct = getBlocks(handDisplacementMat_X.RH, handDisplacementMat_Y.RH);

    %BH - if hands are not together suppose as if they are in motion
    mX = handDisplacementMat_X.BH;
    mX(handDisplacementMat_X.handsAreTogether==0) = max(mX(:));
    mY = handDisplacementMat_Y.BH;
    mY(handDisplacementMat_Y.handsAreTogether==0) = max(mY(:));
    BH_block_struct = getBlocks(mX, mY);
    
    %XLH_YRH
    XLH_YRH_block_struct = getBlocks(handDisplacementMat_X.LH, handDisplacementMat_Y.RH);
    %XRH_YLH
    XRH_YLH_block_struct = getBlocks(handDisplacementMat_X.RH, handDisplacementMat_Y.LH);

    hand_blocks = struct;
    hand_blocks.XLH_YLH = XLH_YLH_block_struct;
    hand_blocks.XRH_YRH = XRH_YRH_block_struct;
    hand_blocks.BH = BH_block_struct;
    hand_blocks.XLH_YRH = XLH_YRH_block_struct;
    hand_blocks.XRH_YLH = XRH_YLH_block_struct;
end

function blockStruct = getBlocks(mX, mY)
    X_blocks = getSingleBlock(mX);
    Y_blocks = getSingleBlock(mY);
    XY_blocks = zeros(size(X_blocks,1),size(Y_blocks,1));
    for xi = 1:size(X_blocks,1)
        for yj = 1:size(Y_blocks,1)
            %now check to see what to write into XY_Blocks
            XY_blocks(xi,yj) = double(X_blocks(xi,2)>0) + 2*double(Y_blocks(yj,2)>0);
            %0 - both in motion
            %1 - Y in motion
            %2 - X in motion
            %3 - both in still
        end
    end
    blockStruct = struct;
    blockStruct.X_blocks = X_blocks;
    blockStruct.Y_blocks = Y_blocks;
    blockStruct.XY_blocks = XY_blocks;
end

function M_blocks = getSingleBlock(X)
    X(X>0) = 1;
    xL = size(X,2);
    M_blocks = zeros(xL,3);
    
    boxIDInc = [0 1 1];
    if X(1)==0
        %first frame still
        ctw = 2;%col to write
    else
        %first frame in motion
        ctw = 3;%col to write
    end
    
    M_blocks(1,ctw) = boxIDInc(ctw);
    for xi=2:xL
        if X(xi) ~= X(xi-1)
            boxIDInc(ctw) = boxIDInc(ctw) + 1;
            if ctw == 2
                ctw = 3;
            else
                ctw = 2;
            end
        end
        M_blocks(xi,ctw) = boxIDInc(ctw);
    end
end