%My aim here is to concatanate the clusterable hand samples into 1
%handStruct
function [handsStruct,problematicVids] = createClusterableDataSet_hf(srcFold, signIDList, userID, maxRptCnt, single0_double1, labelingStrategyStr, hogVersion)
    %[handsStruct,problematicVids] = createClusterableDataSet_hf( 'D:\FromUbuntu\Doga', 1:700, 1:7, 1:7, 0, 0:4);
    global bigDataPath;
    if isempty(srcFold)
        srcFold = [bigDataPath 'Doga'];
    end
    problematicVids = [];
    handsStruct = [];
    [surList, listElemntCnt] = createSURList(srcFold, 2, struct('signIDList',signIDList,'userID',userID,'maxRptCnt',maxRptCnt));
    for l = 1:listElemntCnt
        s = surList(l,1);
        u = surList(l,2);
        r = surList(l,3);
        theFoldName = [srcFold filesep num2str(s) filesep 'User_' num2str(u) '_' num2str(r)];
        disp(['will try to go into folder <' theFoldName '>'])
        try
            [handsToLabelStruct, selectedImCntMat]= getClusterableFrameIDs(srcFold, s, u, r, labelingStrategyStr);
            curVideoHandStruct = createDataSetFromFolder('hf', srcFold, s, u, r, single0_double1, handsToLabelStruct);
            handsStruct = mergeData(handsStruct,curVideoHandStruct, hogVersion);
        catch err
            disp(['err(' err.message ') in s(' num2str(s) '),u(' num2str(u) '),r(' num2str(r) ')'])
            problematicVids = [problematicVids;s u r]; %#ok<AGROW>
        end
    end
    disp(['For sign ID(' vec2strList(signIDList, 's_','_') '), user(' mat2str(userID) '), maxRptCnt(' mat2str(maxRptCnt) ');']);
    
    if single0_double1==0
        lhCnt = sum(handsStruct.detailedLabels(:,4)==1);
        rhCnt = sum(handsStruct.detailedLabels(:,4)==2);
        disp(['there are ' num2str(lhCnt) ' left hand and ' num2str(rhCnt) ' right hand samples  ']);
        if (length(userID)>1)
            usersVec = handsStruct.detailedLabels(:,2);
            histUserVec = hist(usersVec,userID);
            disptable([userID;histUserVec],[],'userID|numOfFrames');
        end
    else
        if (length(userID)>1)
            usersVec = handsStruct.detailedLabels(:,2);
            histUserVec = hist(usersVec,userID);
            disptable([userID;histUserVec],[],'userID|numOfFrames');
        end
    end
end

function [handsStruct] = mergeData(handsStruct, cur_hands, hogVersion)   
    if isempty(handsStruct) && ~isstruct(handsStruct)
        handsStruct = cur_hands;
    else
        if hogVersion==1
            handsStruct.dataCells = [handsStruct.dataCells;cur_hands.dataCells];
            handsStruct.imCropCells = [handsStruct.imCropCells;cur_hands.imCropCells];
            handsStruct.detailedLabels = [handsStruct.detailedLabels;cur_hands.detailedLabels];
        else
            error('not implemented yet');
        end
    end
end

