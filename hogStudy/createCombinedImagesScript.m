function pv = createCombinedImagesScript(signIDList, outFold)
    if ~exist(outFold,'dir')
        mkdir(outFold);
    end
    pv = [];
    try
        for s = signIDList
            loopOnHospiSign('createVidSummary',struct('createMode', 1, 'signIDList',s,'userList',2:7,'maxRptID',10),struct('enforceReCreateNew',true));
            combImg = createBigVideoExp([], struct('createMode', 1, 'signIDList',s,'userList',2:7,'maxRptID',10));
            imwrite(combImg{1},[outFold filesep num2str(s,'%03d') '_LH.png']);
            imwrite(combImg{2},[outFold filesep num2str(s,'%03d') '_RH.png']);
            imwrite(combImg{3},[outFold filesep num2str(s,'%03d') '_BH.png']);
        end
    catch
        pv = [pv;s];
    end
    pv
end