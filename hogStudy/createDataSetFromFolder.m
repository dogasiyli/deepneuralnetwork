function curVideoHandStruct = createDataSetFromFolder(featName, signSourceFolder, signID, userID, signRepeatID, single0_double1, handsToLabelStruct)
    eval(['curVideoHandStruct = createDataSetFromFolder_' featName '(signSourceFolder, signID, userID, signRepeatID, single0_double1, handsToLabelStruct);']);
end

%My aim here is to check the handsToLabelStruct handLabelsStruct
function curVideoHandStruct = createDataSetFromFolder_hf(signSourceFolder, signID, userID, signRepeatID, single0_double1, handsToLabelStruct)
    surVec = [signID, userID, signRepeatID];
    userFolderName = [signSourceFolder filesep num2str(signID) filesep 'User_' num2str(userID) '_' num2str(signRepeatID)];
    try
        [hogImArr_cell, imcropVec_cell] = getHOGFeatsofHands(signID, userID, signRepeatID, struct('srcFold',signSourceFolder, 'hogVersion', 1));          
    catch err
        disp(['skipping createDataSetFromFolder(' userFolderName '); because hogImArr is not there. err(' err.message ')']);
        return
    end 
    
    singleDimSize = size(hogImArr_cell{1,1},1); %#ok<NASGU,*USENS>
    numOfFrames = size(hogImArr_cell,1);
    
    hogImArr_cell = reshape(hogImArr_cell, numOfFrames*3, 1);
    imcropVec_cell = reshape(imcropVec_cell, numOfFrames*3, 1);

    labelCounts = [length(handsToLabelStruct.LH) length(handsToLabelStruct.RH) length(handsToLabelStruct.BH)];
    labelCount = unique(labelCounts);
    if length(labelCount)==1 && labelCount>numOfFrames
        difFrameCnt = labelCount - numOfFrames;
        disp(['hogCell has ' num2str(difFrameCnt) ' more labels hence final LH labels removed(' mat2str([handsToLabelStruct.LH(end-difFrameCnt : end);handsToLabelStruct.RH(end-difFrameCnt : end);handsToLabelStruct.BH(end-difFrameCnt : end)]) '). labels shrank']);
        handsToLabelStruct.LH = handsToLabelStruct.LH(1:end-difFrameCnt);
        handsToLabelStruct.RH = handsToLabelStruct.RH(1:end-difFrameCnt);
        handsToLabelStruct.BH = handsToLabelStruct.BH(1:end-difFrameCnt);  
    end
    
    labelsAll = double([handsToLabelStruct.LH';handsToLabelStruct.RH';handsToLabelStruct.BH']);    
    if single0_double1==0
        labelsAll(2*numOfFrames+1:end) = -1;
        labelFrameIDs_LH = find(handsToLabelStruct.LH~=0);
        numOfSamples_LH = length(labelFrameIDs_LH);
        labelFrameIDs_RH = find(handsToLabelStruct.RH~=0);
        numOfSamples_RH = length(labelFrameIDs_RH);
        if (max(handsToLabelStruct.LH)>1 || max(handsToLabelStruct.RH)>1)
            detailedLabelsLH = [repmat(surVec, numOfSamples_LH, 1) 1*ones(numOfSamples_LH,1) labelFrameIDs_LH' handsToLabelStruct.LH(labelFrameIDs_LH)'];
            detailedLabelsRH = [repmat(surVec, numOfSamples_RH, 1) 2*ones(numOfSamples_RH,1) labelFrameIDs_RH' handsToLabelStruct.RH(labelFrameIDs_RH)'];
        else
            detailedLabelsLH = [repmat(surVec, numOfSamples_LH, 1) 1*ones(numOfSamples_LH,1) labelFrameIDs_LH' 1*ones(numOfSamples_LH,1)];
            detailedLabelsRH = [repmat(surVec, numOfSamples_RH, 1) 2*ones(numOfSamples_RH,1) labelFrameIDs_RH' 1*ones(numOfSamples_RH,1)];
        end
        detailedLabels = [detailedLabelsLH;detailedLabelsRH];
    else%Both hands
        labelsAll(1:2*numOfFrames) = -1;
        labelFrameIDs_BH = find(handsToLabelStruct.BH~=0);
        numOfSamples_BH = length(labelFrameIDs_BH);
        detailedLabels = [repmat(surVec, numOfSamples_BH, 1) 3*ones(numOfSamples_BH,1) labelFrameIDs_BH'  handsToLabelStruct.BH(labelFrameIDs_BH)'];
    end
    
    slct = labelsAll>0;
    %numOfSamples = sum(slct==1);
    
    hogImArr_cell(slct~=1)=[];
    imcropVec_cell(slct~=1)=[];

    curVideoHandStruct.dataCells = hogImArr_cell;
    curVideoHandStruct.imCropCells = imcropVec_cell;
    curVideoHandStruct.detailedLabels = detailedLabels;
    assert(sum((slct==1))==size(detailedLabels,1),'these must have same lengths');
end

%My aim here is to check the handsToLabelStruct handLabelsStruct
function curVideoHandStruct = createDataSetFromFolder_sn(signSourceFolder, signID, userID, signRepeatID, single0_double1, handsToLabelStruct)
    surVec = [signID, userID, signRepeatID];
    userFolderName = [signSourceFolder filesep num2str(signID) filesep 'User_' num2str(userID) '_' num2str(signRepeatID)];
    surfNormFileName = [userFolderName filesep 'surfNorm_crop.mat'];
    surfNormZipFileName = [userFolderName filesep 'surfNorm.zip'];    
    if ~exist(surfNormZipFileName,'file')
        try
            calcSurfNormOfVidFrames(userFolderName, [20 20], [], true);
        catch err
            disp(['skipping createDataSetFromFolder(' userFolderName '); because surfNorm.zip is not there.err:']);
            displayErrorMessage(err);
            return
        end        
    end
    if ~exist(surfNormFileName,'file') 
        try
            getSurfNormFeatsofHands([signID, userID, signRepeatID], struct('srcFold',signSourceFolder));   
        catch err
            disp(['skipping createDataSetFromFolder(' userFolderName '); because surfNorm_crop.mat is not there.err:']);
            displayErrorMessage(err);
            return
        end        
    end
    load(surfNormFileName,'surfImArr_cell','imcropVec_cell');    
    singleDimSize = size(surfImArr_cell{1,1},2); %#ok<NASGU,NODEF,*USENS>
    numOfFrames = size(surfImArr_cell,1);
    
    surfImArr_cell = reshape(surfImArr_cell, numOfFrames*3, 1);
    imcropVec_cell = reshape(imcropVec_cell, numOfFrames*3, 1); %#ok<NODEF>

    labelsAll = double([handsToLabelStruct.LH';handsToLabelStruct.RH';handsToLabelStruct.BH']);    
    if single0_double1==0
        labelsAll(2*numOfFrames+1:end) = -1;
        labelFrameIDs_LH = find(handsToLabelStruct.LH>0);
        numOfSamples_LH = length(labelFrameIDs_LH);
        labelFrameIDs_RH = find(handsToLabelStruct.RH>0);
        numOfSamples_RH = length(labelFrameIDs_RH);
        if (max(handsToLabelStruct.LH)>1 || max(handsToLabelStruct.RH)>1)
            detailedLabelsLH = [repmat(surVec, numOfSamples_LH, 1) 1*ones(numOfSamples_LH,1) labelFrameIDs_LH' handsToLabelStruct.LH(labelFrameIDs_LH)'];
            detailedLabelsRH = [repmat(surVec, numOfSamples_RH, 1) 2*ones(numOfSamples_RH,1) labelFrameIDs_RH' handsToLabelStruct.RH(labelFrameIDs_RH)'];
        else
            detailedLabelsLH = [repmat(surVec, numOfSamples_LH, 1) 1*ones(numOfSamples_LH,1) labelFrameIDs_LH' 1*ones(numOfSamples_LH,1)];
            detailedLabelsRH = [repmat(surVec, numOfSamples_RH, 1) 2*ones(numOfSamples_RH,1) labelFrameIDs_RH' 1*ones(numOfSamples_RH,1)];
        end
        detailedLabels = [detailedLabelsLH;detailedLabelsRH];
    else%Both hands
        labelsAll(1:2*numOfFrames) = -1;
        labelFrameIDs_BH = find(handsToLabelStruct.BH~=0);
        numOfSamples_BH = length(labelFrameIDs_BH);
        detailedLabels = [repmat(surVec, numOfSamples_BH, 1) 3*ones(numOfSamples_BH,1) labelFrameIDs_BH'  handsToLabelStruct.BH(labelFrameIDs_BH)'];
    end
    
    slct = labelsAll>0;
    numOfSamples = [sum(labelsAll(1:numOfFrames)>0) sum(labelsAll(numOfFrames+1:2*numOfFrames)>0) sum(labelsAll(2*numOfFrames+1:end)>0)];
    numOfSamples = sum(numOfSamples);
    
    surfImArr_cell(slct~=1)=[];
    imcropVec_cell(slct~=1)=[];

    curVideoHandStruct.dataCells = surfImArr_cell;
    curVideoHandStruct.imCropCells = imcropVec_cell;
    curVideoHandStruct.detailedLabels = detailedLabels;
    assert(numOfSamples==size(detailedLabels,1),'these must have same lengths');
end