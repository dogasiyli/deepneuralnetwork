function [handsStruct,problematicVids] = createDataSetFromLabels(srcFold, signIDList, userList, repeatIDVec, single0_double1, labelIDs)
    %[handsStruct,problematicVids] = createDataSetFromLabels( 'D:\FromUbuntu\Doga', 1:700, 1:7, 1:7, 0, 0:4);
    global bigDataPath;
    if isempty(srcFold)
        srcFold = [bigDataPath 'Doga'];
    end
    problematicVids = [];
    handsStruct = [];
    for s = signIDList
        for u = userList
            for k = 1:length(repeatIDVec)
                r = repeatIDVec(k);
                theFoldName = srcFold;
                %into sign folder
                theFoldName = [theFoldName filesep num2str(s) ]; %#ok<*AGROW>
                if ~exist(theFoldName, 'dir')
                    continue
                end
                %into user-repeat folder
                theFoldName = [theFoldName filesep 'User_' num2str(u) '_' num2str(r)];
                if ~exist(theFoldName, 'dir')
                    continue
                end
                disp(['will try to go into folder <' theFoldName '>'])
                try
                    for l=labelIDs
                        curVideoHandStruct = fwf_createDataSetFromFolder(srcFold, s, u, r, single0_double1, l);
                        handsStruct = mergeData(handsStruct,curVideoHandStruct);
                    end
                catch err
                    disp(['err(' err.message ') in s(' num2str(s) '),u(' num2str(u) '),r(' num2str(r) ')'])
                    problematicVids = [problematicVids;s u r];
                end
            end
        end
    end
    disp(['For sign ID(' num2str(s) '), labels(' mat2str(labelIDs) '), users(' mat2str(userList) '), repitions(' mat2str(repeatIDVec) ');']);
    
    if single0_double1==0
        lhCnt = sum(handsStruct.detailedLabels(:,4)==1);
        rhCnt = sum(handsStruct.detailedLabels(:,4)==2);
        disp(['there are ' num2str(lhCnt) ' left hand and ' num2str(rhCnt) ' right hand samples  ']);
        if (length(userList)>1)
            usersVec = handsStruct.detailedLabels(:,2);
            histUserVec = hist(usersVec,userList);
            disptable([userList;histUserVec],[],'userID|numOfFrames');
        end
    else
        if (length(userList)>1)
            usersVec = handsStruct.detailedLabels(:,2);
            histUserVec = hist(usersVec,userList);
            disptable([userList;histUserVec],[],'userID|numOfFrames');
        end
    end
end

function [handsStruct] = mergeData(handsStruct, cur_hands)   
    if isempty(handsStruct) && ~isstruct(handsStruct)
        handsStruct = cur_hands;
    else
        handsStruct.dataCells = [handsStruct.dataCells;cur_hands.dataCells];
        handsStruct.imCropCells = [handsStruct.imCropCells;cur_hands.imCropCells];
        handsStruct.detailedLabels = [handsStruct.detailedLabels;cur_hands.detailedLabels];
    end
end

function curVideoHandStruct = fwf_createDataSetFromFolder(signSourceFolder, signID, userID, signRepeatID, single0_double1, labelID)
    surVec = [signID, userID, signRepeatID];
    userFolderName = [signSourceFolder filesep num2str(signID) filesep 'User_' num2str(userID) '_' num2str(signRepeatID)];
    surfNormFileName = [userFolderName filesep 'surfNorm_crop.mat'];
    surfNormZipFileName = [userFolderName filesep 'surfNorm.zip'];
    if ~exist(surfNormFileName,'file') || ~exist(surfNormZipFileName,'file')
        try
            calcSurfNormOfVidFrames(userFolderName, [20 20], [], true);
            getSurfNormFeatsofHands([signID, userID, signRepeatID], struct('srcFold',signSourceFolder));
        catch err
            disp(['skipping createDataSetFromFolder(' userFolderName '); because surfNorm.mat is not there.err(' err.message ')']);
            return
        end        
    end
    load(surfNormFileName,'surfImArr_cell','imcropVec_cell');
    [~, handLabelsStruct] = getHandDisplacementFromASPLabels(signSourceFolder, signID, userID, signRepeatID, [], false);
    
    singleDimSize = size(surfImArr_cell{1,1},2); %#ok<NASGU,NODEF,*USENS>
    numOfFrames = size(surfImArr_cell,1);
    
    handShortCutStr = {'LH','RH','BH'};
    for i=1:3
        eval(['curHandLabels = handLabelsStruct.' handShortCutStr{i} ';']);             
        if (size(curHandLabels,2)==numOfFrames-1)
            curHandLabels = [curHandLabels curHandLabels(end)];
        elseif (size(curHandLabels,2)==numOfFrames)
            %everything is fine
        else
            error('problm with number of samples in surfNorm and labels');
        end
        eval(['handLabelsStruct.' handShortCutStr{i} ' = curHandLabels;']);             
    end
    surfImArr_cell = reshape(surfImArr_cell, numOfFrames*3, 1);
    imcropVec_cell = reshape(imcropVec_cell, numOfFrames*3, 1); %#ok<NODEF>
    labelsAll = [handLabelsStruct.LH';handLabelsStruct.RH';handLabelsStruct.BH'];    
    if single0_double1==0
        labelsAll(2*numOfFrames+1:end) = -1;
        labelFrameIDs_LH = find(labelsAll(1:numOfFrames)==labelID);
        numOfSamples_LH = length(labelFrameIDs_LH);
        labelFrameIDs_RH = find(labelsAll(1+numOfFrames : 2*numOfFrames)==labelID);
        numOfSamples_RH = length(labelFrameIDs_RH);
        detailedLabelsLH = [repmat(surVec, numOfSamples_LH, 1) 1*ones(numOfSamples_LH,1) labelFrameIDs_LH labelID*ones(numOfSamples_LH,1)];
        detailedLabelsRH = [repmat(surVec, numOfSamples_RH, 1) 2*ones(numOfSamples_RH,1) labelFrameIDs_RH labelID*ones(numOfSamples_RH,1)];
        detailedLabels = [detailedLabelsLH;detailedLabelsRH];
    else%Both hands
        labelsAll(1:2*numOfFrames) = -1;
        labelFrameIDs_BH = find(labelsAll(2*numOfFrames+1:end)==labelID);
        numOfSamples_BH = length(labelFrameIDs_BH);
        detailedLabels = [repmat(surVec, numOfSamples_BH, 1) 3*ones(numOfSamples_BH,1) labelFrameIDs_BH labelID*ones(numOfSamples_BH,1)];
    end
    
    slct = labelsAll==labelID;
    %numOfSamples = sum(slct==1);
    
    surfImArr_cell(slct~=1)=[];
    imcropVec_cell(slct~=1)=[];

    curVideoHandStruct.dataCells = surfImArr_cell;
    curVideoHandStruct.imCropCells = imcropVec_cell;
    curVideoHandStruct.detailedLabels = detailedLabels;
end