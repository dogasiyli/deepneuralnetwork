function [distMat, cropIDs, mnDS, mxDS] = createDistMat_surfNormCells(featName, handsStruct, knownDistancesFileName, fileSaveName, OPS)
%createDistMat_surfNormCells uses handstruct and creates a distMat for the
%selected stuff..    

    if ~exist('OPS','var')
        OPS = [];
    end
    switch featName
        case 'hf'
            distMethod_default = 'c3';
        case 'sn'
            distMethod_default = 'e3';
    end
    %OPS_createDistMat_surfNormCells = struct('enforceRecreate',false,'useMethodID',0,'distMethod','c2');
    enforceRecreate     = getStructField(OPS, 'enforceRecreate', false);
    useMethodID         = getStructField(OPS, 'useMethodID', 0);
    distMethod          = getStructField(OPS, 'distMethod', distMethod_default);%{'c3','c2','e3','e2'}
    saveAtEveryNewCnt   = getStructField(OPS, 'saveAtEveryNewCnt', 10000);
    percentToDispInc    = getStructField(OPS, 'percentToDispInc', 0.1);


    extractDistMat = enforceRecreate;
    knownDistances = loadKnownDistancesFromFile(knownDistancesFileName);
    
    stateStr = 'empty';
    try
        if exist(fileSaveName,'file') && ~enforceRecreate
            disp(['Loading distMat,cropIDs,mnDS,mxDS from file(' fileSaveName ')'])
            load(fileSaveName,'distMat', 'cropIDs', 'mnDS', 'mxDS');
            [distMat, stateStr] = checkDistMatState( distMat ); %#ok<NODEF>
            switch stateStr
                case {'empty', 'notSquare', 'hasNans'}
                    extractDistMat = true;
                otherwise
                    extractDistMat = false;
            end
        end
    catch err
        disp(['Couldnt load because (' err.message ')']);
        extractDistMat = true;
    end
    if ~extractDistMat
        if exist('distMat','var') && exist('cropIDs','var') && exist('mnDS','var') && exist('mxDS','var')
            disp('Loaded distMat,cropIDs,mnDS,mxDS from file')
            return
        else
            msgDisp = 'Will create distMat,cropIDs,mnDS,mxDS from handStruct';
        end
    else
        switch stateStr
            case {'empty'}
                msgDisp = 'Will create distMat,cropIDs,mnDS,mxDS from handStruct from scratch';
            case {'notSquare'}
                msgDisp = 'Will correct distMat,cropIDs,mnDS,mxDS from handStruct from non square';
            case {'hasNans'}
                msgDisp = 'Will fill NaN values in distMat,cropIDs,mnDS,mxDS from handStruct';
            case {'complete'}
                msgDisp = 'Will go over complete distMat,cropIDs,mnDS,mxDS from handStruct';
            otherwise
                msgDisp = 'Will go over complete distMat,cropIDs,mnDS,mxDS from handStruct';
        end        
    end
    disp(msgDisp);
    
    N = size(handsStruct.dataCells, 1);
    totalIdx = sum(sum(triu(ones(N),1)));
    switch stateStr
        case {'complete', 'hasNans'}
            totalIdx = sum(sum(isnan(distMat),1));
        case {'empty', 'notSquare'}
            distMat = initializeDistMat(N);
            cropIDs = initializeDistMat(N, 'full');
            mnDS = struct;%minDistStruct
            mxDS = struct;%maxDistStruct
            mnDS.d = +inf;
            mxDS.d = -inf;
        otherwise
            distMat = initializeDistMat(N);
            cropIDs = initializeDistMat(N, 'full');
            mnDS = struct;%minDistStruct
            mxDS = struct;%maxDistStruct
            mnDS.d = +inf;
            mxDS.d = -inf;
    end        
    startTime = [];
    prevTime = [];
    curIdx = 1;
    
    cellsz_all = cell2mat(cellfun(@size,handsStruct.dataCells,'uni',false));
    D_SingleImg = cellsz_all(1,2);
    boxCnt_SingleImg = D_SingleImg/4;
    cellsz_all = cellsz_all(:,1);
    cellsz_unq = unique(cellsz_all)';
    
    %we will compare all samples with each other
    %hence we can first create a matrix to see which 2 samples to compare
    %then according to list compare and write into distmat
    searchMat = getSearcMatFromDistMat(cellsz_all, distMat);
    percentToDisp = 0.0;
    
    detailedLabels = handsStruct.detailedLabels;
    acquiredFromKnownDistances = 0;
    
    knownDistances_col11 = getCol11IDByDistMethod(featName, distMethod);
    
    knownDistances_append = [];
    skipIfKnown = true;
    
    [distMat, cropIDs, foundSampleCount] = fillDistMatWithKnownInformation_HospiSign(distMat, cropIDs, searchMat, detailedLabels, knownDistances_col11, knownDistances);
    if foundSampleCount>0
        searchMat = getSearcMatFromDistMat(cellsz_all, distMat);
        totalIdx = sum(sum(isnan(distMat),1));
    end
    disp(['Updating(' fileSaveName ') with the newly acquired distance information from knownDistances'])
    save(fileSaveName,'distMat', 'cropIDs', 'mnDS', 'mxDS', 'detailedLabels');
    disp('Updated')
    
    dispstat('','init');
    
    for csz_i=cellsz_unq(end:-1:1)
        for csz_j=cellsz_unq(end:-1:1)
            %find the rows that have csz_i in col 4 and csz_j in col 5
            idxRows = find(searchMat(:,4)==csz_i &searchMat(:,5)==csz_j);
            
            %create the maps for such samples
            
            dispstat(sprintf('will process <%d> samples of %d by (%d)\n',length(idxRows),csz_i,csz_j),'keepthis');
            %disp(['will process <' num2str(length(idxRows)) '> samples of ' num2str(csz_i) ' by (' num2str(csz_j) ')'])
            if useMethodID==3
                idx01 = repmat(1:csz_i,csz_j,1);
                map01 = reshape(1:D_SingleImg*csz_i,csz_i,D_SingleImg);
                map01 = reshape(map01(idx01(:),:)', boxCnt_SingleImg, 4, csz_i*csz_j);
                map01 = reshape(permute(map01,[2,1,3]),size(map01,2),[])';

                if csz_i==csz_j
                    map02 = map01;
                else
                    idx02 = repmat(1:csz_j,csz_i,1)';
                    map02 = reshape(1:D_SingleImg*csz_j,csz_j,D_SingleImg);
                    map02 = reshape(map02(idx02(:),:)', boxCnt_SingleImg, 4, csz_i*csz_j);
                    map02 = reshape(permute(map02,[2,1,3]),size(map02,2),[])';
                end
            elseif useMethodID==1
                map02 = reshape(1:D_SingleImg*csz_j,csz_j,D_SingleImg);
                map02 = reshape(map02', boxCnt_SingleImg, 4, csz_j);
                %next line is taken from
                %https://www.mathworks.com/matlabcentral/answers/295692-concatenate-vertically-along-the-3rd-dimension-of-a-matrix
                map02 = reshape(permute(map02,[2,1,3]),4,[])';  
                
                
                %im01_M = repmat(imCell01(i,:),1,cropCnt02);
                %im01_M = reshape(im01_M, boxCnt_SingleImg, 4, cropCnt02);
                %im01_M = reshape(im01_M, boxCnt_SingleImg, 4, cropCnt02);
                %im01_M = reshape(permute(im01_M,[2,1,3]),size(im01_M,2),[])';
        
                map01 = [];%faster then below
                % map01 = 1:D_SingleImg;
                % map01 = repmat(map01,1,csz_j);
                % map01 = reshape(map01, boxCnt_SingleImg, 4, csz_j);
                % map01 = permute(map01,[2,1,3]);
                % map01 = reshape(map01,4,[])';
            else
                map01 = [];
                map02 = [];
            end
            
            for r = idxRows'
                curPercent = 100*(curIdx/totalIdx);
                if percentToDisp<=curPercent || curIdx==1
                    if acquiredFromKnownDistances > 0
                        dispstat(sprintf('%d number of distances are acquired from known distances',acquiredFromKnownDistances),'keepthis');
                        %disp([num2str(acquiredFromKnownDistances) ' number of distances are acquiredFromKnownDistances']);
                    end
                    [startTime, prevTime, ~, ~, str2disp] = dispTimerInfo(startTime, prevTime, curIdx, totalIdx-acquiredFromKnownDistances, 10);
                    dispstat(sprintf('%d of %d is being processed for N(%d).(%4.2f percent) ** %s \n',curIdx, size(searchMat,1),N, curPercent, str2disp));
                    %disp([num2str(curIdx) ' of ' num2str(size(searchMat,1)) ' is being processed for N(' num2str(N) ').(' num2str(curPercent) ' percent)'])
                    %[startTime, prevTime] = dispTimerInfo( startTime, prevTime, curIdx, totalIdx-acquiredFromKnownDistances, 1);
                    percentToDisp = percentToDisp + percentToDispInc;
                end
                curIdx = curIdx + 1;
                i = searchMat(r,2);
                j = searchMat(r,3);
                
                mnDS_knownFromMat = struct;
                mnDS_knownFromMat.d = distMat(i,j);
                mnDS_knownFromMat.rowID = cropIDs(i,j);
                mnDS_knownFromMat.colID = cropIDs(j,i);
                mnDS_knownFromMat.cropIDs = [cropIDs(i,j) cropIDs(j,i)];
                if skipIfKnown && ~isnan(mnDS_knownFromMat.d) && mnDS_knownFromMat.rowID~=0 && mnDS_knownFromMat.colID~=0
                    continue
                end
                
                %now is the distance already calculated between these two                 
                imCell01 = handsStruct.dataCells{i,1};
                imCell02 = handsStruct.dataCells{j,1};

                %detailedLabels_01 = handsStruct.detailedLabels(id_01,:);
                %detailedLabels_02 = handsStruct.detailedLabels(id_02,:);
                %calc maps for these two
                %mnDS_ = calcMinDist_surfNormImageCells(imCell01, imCell02, 3, map02, map01);
                switch featName
                    case 'sn'
                        mnDS_ = calcMinDist_surfNormImageCells(imCell01, imCell02, useMethodID, map02, map01, distMethod);
                    case 'hf'
                        if iscell(imCell01) && length(imCell01)==1
                            imCell01 = imCell01{1,1};
                        end
                        if iscell(imCell02) && length(imCell02)==1
                            imCell02 = imCell02{1,1};
                        end
                        mnDS_ = calcMinDist_hogFeatCells(imCell01, imCell02, useMethodID, distMethod);
                end
                
                knownDistances_row = [detailedLabels(i,1:5) detailedLabels(j,1:5) knownDistances_col11 mnDS_.d mnDS_.rowID mnDS_.colID];
                knownDistances_append = [knownDistances_append;knownDistances_row]; %#ok<*AGROW>
                if size(knownDistances_append,1)>saveAtEveryNewCnt
                    dispstat(sprintf('Updating(%s) with %d new calculated distances',fileSaveName, saveAtEveryNewCnt));
                    [knownDistances, knownDistances_append] = appendKnownDistancesFile(knownDistances, knownDistances_append, knownDistancesFileName);
                    save(fileSaveName,'distMat', 'cropIDs', 'mnDS', 'mxDS', 'detailedLabels');
                    dispstat(sprintf('Updated(%s) with %d new calculated distances',fileSaveName, saveAtEveryNewCnt));
                end      
                
                if (mnDS_.d<mnDS.d)
                    mnDS = mnDS_;
                    %disp(['**im(' num2str(i) ') and im(' num2str(j) ') is the best match with d(' num2str(mnDS_.d) ') until now']);
                    %displayBextCropStruct(1, imCell01, imCell02, mnDS, {['im(' mat2str(detailedLabels_i) ')'],['best similarity(' num2str(mnDS_.d) ')'],['im(' mat2str(detailedLabels_j) ')']});
                end
                if (mnDS_.d>mxDS.d)
                    mxDS = mnDS_;
                    %disp(['**im(' num2str(i) ') and im(' num2str(j) ') is the worst match with d(' num2str(mnDS_.d) ') until now']);
                    %displayBextCropStruct(2, imCell01, imCell02, mxDS, {['im(' mat2str(detailedLabels_i) ')'],['worst similarity(' num2str(mnDS_.d) ')'],['im(' mat2str(detailedLabels_j) ')']});
                end
                
                %if isnan(mnDS_knownFromMat.d)
                %    disp(['mnDS_.d(' num2str(mnDS_.d) ') vs mnDS_.d(' num2str(mnDS_knownFromMat.d) ')']);
                %end
                %if mnDS_knownFromMat.rowID==0 
                %    disp(['mnDS_.rowID(' num2str(mnDS_.rowID) ') vs mnDS_.rowID(' num2str(mnDS_knownFromMat.rowID) ')']);
                %end
                %if mnDS_knownFromMat.colID==0
                %    disp(['mnDS_.colID(' num2str(mnDS_.colID) ') vs mnDS_.colID(' num2str(mnDS_knownFromMat.colID) ')']);
                %end
                if mnDS_.d==0
                    mnDS_.d = eps;
                end
                distMat(i,j) = mnDS_.d;
                cropIDs(i,j) = mnDS_.rowID;
                cropIDs(j,i) = mnDS_.colID;                
            end
        end    
    end
    if size(knownDistances_append,1)>0
        appendKnownDistancesFile(knownDistances, knownDistances_append, knownDistancesFileName);
    end
    
    if exist('fileSaveName','var') && ischar(fileSaveName)
        try
            %fileSaveName = 'C:\FromUbuntu\HospiSign\HospiMOV\clusterSurfNorm\007_0_2.mat'
            disp(['Saving(' fileSaveName ')'])
            save(fileSaveName,'distMat', 'cropIDs', 'mnDS', 'mxDS', 'detailedLabels');
            disp('Saved')
        catch err
            disp(['Couldnt save(' fileSaveName ') err=' err.message])
        end
    end
end

function searchMat = getSearcMatFromDistMat(cellsz_all, distMat)
    idxMat = findIn2DMat(distMat,NaN);
    searchMat = [idxMat cellsz_all(idxMat(:,2)) cellsz_all(idxMat(:,3))];
    try
        searchMat = sortrows(searchMat, [4 5 2 3], {'descend','descend','ascend','ascend'});
    catch
        searchMat = sortrows(searchMat, [-4 -5 2 3]);
    end
end

function distMatNew = initializeDistMat(N, distMatType)
    if ~exist('distMatType','var') || ~ischar(distMatType)
        D_rightUpperTriangle = triu(ones(N),+1);
        distMatNew = D_rightUpperTriangle;
        distMatNew(distMatNew ==1) = NaN;
    elseif strcmpi(distMatType,'full')
        distMatNew = NaN(N,N);
        distMatNew(eye(N)==1) = 0;
    end
end