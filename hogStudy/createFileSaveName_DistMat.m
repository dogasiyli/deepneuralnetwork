function [fileSaveName] = createFileSaveName_DistMat(srcFold, surSlctListStruct, distMatInfoStruct, hogVersion)
    %1. the folderName must be - <distMats> instead of <clusterSurfNorm>
    %2. 
    %createMode = surSlctListStruct.createMode; 
    single0_double1 = surSlctListStruct.single0_double1;    
    signIDList = surSlctListStruct.signIDList; 
    userList = surSlctListStruct.userList; 
    maxRptID = surSlctListStruct.maxRptID;
    labelingStrategyStr = surSlctListStruct.labelingStrategyStr;
    
    featName = distMatInfoStruct.featureName;
    cropMethod = distMatInfoStruct.cropMethod;
    distMethod = distMatInfoStruct.distanceMethod;
    %if (featName=='sn' && cropMethod=='sc' && distMethod=='c3') 
    %end
    sdStr = 'single';
    if single0_double1==1
        sdStr = 'double';
    end
    
    fileSaveName = [featName '_' cropMethod '_'];%'sn_mc_';
    if length(userList)==1
        userIDStr = ['u' num2str(userList)];
    else
        userIDStr = vec2strList(userList, 'u_','_');
    end
    if length(signIDList)==1
        signIDStr = ['s' num2str(signIDList,'%03d')];
        repeatIDStr = ['r' num2str(maxRptID,'%02d')];
        %1 sign, 1 user, 1 repition
        if isfield(surSlctListStruct,'labelList')
            % -Y_sXXXuZrQQ_lLL
            %     + Y   - is either 'single' or 'double'
            %     + XXX - is the signd ID
            %     + Z   - is userID
            %     + Q   - is repitionID
            %     + LL  - is label list
            labelIDsStr = vec2strList(surSlctListStruct.labelList, 'l','');
        else
            % -Y_sXXXuZrQ_cf
            %     + Y   - is either 'single' or 'double'
            %     + XXX - is the signd ID
            %     + Z   - is userID
            %     + Q   - is repitionID
            %     + cf  - means Clusterable Frames
            labelIDsStr = 'cf';
        end
        fileSaveName = [fileSaveName sdStr '_' signIDStr userIDStr repeatIDStr  '_' labelIDsStr];
    elseif length(signIDList)>1
        repeatIDStr = ['rc' num2str(maxRptID)];
        signIDStr = vec2strList(signIDList, 's_','_');
        %a lot of signs,1 user, 1 repition
        % -Y_uZrcQ_slLL
        %     + Y   - is either 'single' or 'double'
        %     + Z   - is userID
        %     + Q   - is repitionCount
        %     + LL  - is sign list
        fileSaveName = [fileSaveName sdStr '_' userIDStr repeatIDStr  '_' signIDStr];
    end
    
    distMatsFoldeNameFull = [srcFold filesep 'distMats'];
    if ~exist(distMatsFoldeNameFull,'dir')
        mkdir(distMatsFoldeNameFull)
    end
    
    switch labelingStrategyStr
        case 'fromAssignedLabels'
            labelStrategyFileNameAddStr = '_fal';
        case 'fromSkeletonOnly'
            labelStrategyFileNameAddStr = '_fso'; 
        case 'fromSkeletonStableAll'
            labelStrategyFileNameAddStr = '_fsa'; 
        case 'assignedLabelsRemove1'
            labelStrategyFileNameAddStr = '_ar1';    
        case 'assignedLabelsExcept1'
            labelStrategyFileNameAddStr = '_ax1';    
    end
    
    switch hogVersion
        case 1
            hogVersionFileNameAddStr = '';
        case 2
            hogVersionFileNameAddStr = '_hv2';
    end
    
    compNameAddStr = getVariableByComputerName('compNameAddStr');
    
    distMatFileName = [distMatsFoldeNameFull filesep fileSaveName '_dist' distMethod labelStrategyFileNameAddStr hogVersionFileNameAddStr '.mat'];
    knownDistancesFileName = [distMatsFoldeNameFull filesep 'knownDistances_' featName '_' cropMethod '_' distMethod hogVersionFileNameAddStr compNameAddStr '.mat'];
    dataFileName = [distMatsFoldeNameFull filesep fileSaveName '_data' labelStrategyFileNameAddStr hogVersionFileNameAddStr '.mat'];
    visualsFileName = [distMatsFoldeNameFull filesep fileSaveName '_visuals' labelStrategyFileNameAddStr hogVersionFileNameAddStr '.mat'];
    clusterResultFileName = [distMatsFoldeNameFull filesep fileSaveName '_clusterResult' labelStrategyFileNameAddStr hogVersionFileNameAddStr '.mat'];
    %<fileSaveName>.mat will have the distance matrix created
   
    fileSaveName = struct;
    fileSaveName.distanceMatrixFile = distMatFileName;
    fileSaveName.knownDistancesFileName = knownDistancesFileName;
    fileSaveName.dataFile = dataFileName;
    fileSaveName.visualsFile = visualsFileName;
    fileSaveName.clusterResult = clusterResultFileName;
end
