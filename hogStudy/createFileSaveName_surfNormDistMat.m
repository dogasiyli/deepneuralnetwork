function fileSaveName = createFileSaveName_surfNormDistMat(srcFold, handsStruct, signIDList, labelIDs, single0_double1)
    %1. the folderName must be - <distMats> instead of <clusterSurfNorm>
    %2. 
%     createMode = surSlctListStruct.createMode; 
%     signIDList = surSlctListStruct.signIDList; 
%     userList = surSlctListStruct.userList; 
%     maxRptID = surSlctListStruct.maxRptID;
    
    if length(signIDList)==1
        %1 sign, 1 user, 1 repition
        fileSaveName = 'sn_mc_c3_sXXX_Y_uZrQ_lLL';
    elseif length(signIDList)>1
        %a lot of signs,1 user, 1 repition
        % -uZrcQ_Y_slLL
        %     + Z   - is userID
        %     + Q   - is repitionID
        %     + Y   - is either 'single' or 'double'
        %     + LL  - is sign list
        fileSaveName = 'sn_mc_c3_uZrcQ_Y_slLL';
    end
    try
        fileSaveName = [srcFold filesep 'clusterSurfNorm' filesep 's(' num2str(signIDList,'%03d') ')_' num2str(single0_double1) '_userIDsStr_labelIDsStr.mat'];

        repeatIDs = unique(handsStruct.detailedLabels(:,3))';
        userIDs = unique(handsStruct.detailedLabels(:,2))';
        if length(repeatIDs)==1 && length(userIDs)==1
            %this is from a single users single repeat video
            userIDsStr = ['ur(' num2str(userIDs) num2str(repeatIDs) ')'];        
        else
            %this is from a single users single repeat video
            try
                userIDsStr = replace(replace(replace(mat2str(userIDs),'[',''),']',''),' ',''); 
            catch
                userIDsStr = strrep(strrep(strrep(mat2str(userIDs),'[',''),']',''),' ',''); 
            end
            %this is from a single users single repeat video
            try
                repeatIDsStr = replace(replace(replace(mat2str(repeatIDs),'[',''),']',''),' ',''); 
            catch
                repeatIDsStr = strrep(strrep(strrep(mat2str(repeatIDs),'[',''),']',''),' ',''); 
            end
            userIDsStr = ['u(' userIDsStr ')_r(' repeatIDsStr ')'];        
        end

        try
            labelIDsStr = replace(replace(replace(mat2str(labelIDs),'[',''),']',''),' ',''); 
        catch
            labelIDsStr = strrep(strrep(strrep(mat2str(labelIDs),'[',''),']',''),' ',''); 
        end
        labelIDsStr = ['l(' labelIDsStr ')'];

        try
            fileSaveName = replace(fileSaveName, 'userIDsStr', userIDsStr);
            fileSaveName = replace(fileSaveName, 'labelIDsStr', labelIDsStr);
        catch
            fileSaveName = strrep(fileSaveName, 'userIDsStr', userIDsStr);
            fileSaveName = strrep(fileSaveName, 'labelIDsStr', labelIDsStr);
        end
    catch
        fileSaveName = [srcFold filesep 'clusterSurfNorm' filesep 's(' num2str(signIDList,'%03d') ')_' num2str(single0_double1) '_err(' num2str(randi(500,1)) ').mat'];        
    end
end