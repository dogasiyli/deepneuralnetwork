function [newLabels, distMat, clusterLabelsCell, handsStruct] = createHandsStructMedoidsForASign(srcFold, surSlctListStruct, distMatInfoStruct, clusteringParams, k_selected, optParamStruct)
    %srcFold = '/media/dg/SSD_Data';
    %labellingStrategy='fromSkeletonStableAll';%'fromAssignedLabels';%'assignedLabelsRemove1';%'assignedLabelsExcept1'%;'fromSkeletonOnly';%
    %surSlctListStruct = struct('labelingStrategyStr',labellingStrategy,'single0_double1', 0, 'signIDList',521,'userList',2:5,'maxRptID',1);
    %distMatInfoStruct = struct('featureName','hf','cropMethod','mc','distanceMethod','e3');
    %clusteringParams = struct('tryMax',2000,'kMax',10,'gtLabelsGrouping',[],'modeStr','kMedoids','groupClusterLabels',false);
    
    if ~exist('optParamStruct','var')
        optParamStruct = [];
    end
    gtLabelsGrouping        = getStructField(optParamStruct, 'gtLabelsGrouping', []);
    hogVersion              = getStructField(optParamStruct, 'hogVersion', 1);
    askForNewAssignments    = getStructField(optParamStruct, 'askForNewAssignments', true);
    deleteDataFile          = getStructField(optParamStruct, 'deleteDataFile', true);
    %display the known information about selections, distances and
    %clustering parameters
    displayStructureSummary('surSlctListStruct',surSlctListStruct,0);
    displayStructureSummary('distMatInfoStruct',distMatInfoStruct,0);
    displayStructureSummary('clusteringParams',clusteringParams,0);

    %load the distMat, handsStruct and fileSaveName variables
    [distMat, handsStruct, fileSaveName] =  createSpecificDistMat(srcFold, surSlctListStruct, distMatInfoStruct, struct('hogVersion',hogVersion));
    
    %load the useful clusterResults
    clusterLabelsCell = [];%will be loaded on the next line
    load(fileSaveName.clusterResult,'medoidHist_Best','clusterLabelsCell');
    
    %from now on this function has differences from <setLabelsFromClusterAnalysisResults>
    %I have selected a <k> as to create the handStruct to have the medoids
    %to assign new labels when a new instance is met..
    
    
    if isempty(k_selected)
        k_selected = 12;
        str = input('Please select k : ','s');
        try
            k_selected = str2double(str);
        catch err
            disp(err.message);
            disp('k selected as 12 by default');
        end    
    end
    
    %the idea is = 
    [confMatBest, uniqueMappingLabels, confAccBest, outStruct] = groupClusterLabelsIntoGroundTruth(clusterLabelsCell{1,k_selected}, handsStruct.detailedLabels(:,6), struct('gtLabelsGrouping', gtLabelsGrouping,'printResults', true,'figID', 1));
    newKlusterCell = outStruct.newKlusterCell;
    kReduced = length(newKlusterCell);
    
    disp(['Best confusion matrix for cluster count(' num2str(k_selected) ') is found with k=' num2str(kReduced) ' and the acc(' num2str(confAccBest,'%4.2f') ')']);
    disptable(confMatBest);
    disptable(uniqueMappingLabels, 'originalLabels|userMappedLabels|bestConfusionLabels');
    
    clusterNames = getClusterNamesAssignedFromTxt(srcFold, surSlctListStruct);
    [~, handsStructMedoids, medoidToClusterMap, hMedoids, removedMedoidIdx] = fwf_displayMedoidCenterImages(srcFold, handsStruct, medoidHist_Best, newKlusterCell, k_selected, 92, clusterNames, surSlctListStruct.labelingStrategyStr);

    %ask the assignments to be
    if askForNewAssignments
        newLabels = fwf_getNewLabelsFromUserInput(kReduced);
    else
        newLabels = 1:k_selected;
    end
    if ~isempty(removedMedoidIdx)
        newLabels(removedMedoidIdx(:,1)) = [];
    end
    medoidCount = size(handsStructMedoids.detailedLabels,1);
    listItems_all = quantizeDetailedLabels(handsStruct.detailedLabels);
    listItems_med = quantizeDetailedLabels(handsStructMedoids.detailedLabels);
    medoidIDs = ismember(listItems_all, listItems_med);
    medoidIDs = find(medoidIDs);
    minDistAllowedPerMedoid = zeros(1,medoidCount);
    maxDistAllowedPerMedoid = zeros(1,medoidCount);
    %the ids of list members where they become medoid are in Lia
    %figure(88);clf;
    rowDivCnt = 6;
    labelingStrategyStr = surSlctListStruct.labelingStrategyStr;
    featureName = distMatInfoStruct.featureName;
    singleFrameMedoids = [];
    for i=1:medoidCount
        %for each medoid I need to find the furthest element that falls
        %into its area
        %or show the user the medoid and its assignments in a sorted order
        %and ask until which sample the user wants the assignments to be
        %made..
        %then I will understand the limit distance accepted for that medoid
        %to be considered as similar..
        assignedSamples = find(clusterLabelsCell{1,k_selected}==i);
        n = length(assignedSamples);
        
        if (n==1)
            singleFrameMedoids = [singleFrameMedoids i];
            continue
        end
        
        medoidID = medoidIDs(i);
        compCouples = [ones(n,1)*medoidID reshape(assignedSamples,n,1)];
        compCouples = sort(compCouples')';
        distVec_assSamp = distMat(sub2ind(size(distMat),compCouples(:,1),compCouples(:,2)));
        
        dl_vec = handsStruct.detailedLabels(assignedSamples,:);       
        %rcRatio = 2/rowDivCnt;
        distVec_sorted = sort(distVec_assSamp);
        %[imPalette_groupImgs, distVec_sorted, paletteRCSize_groupImgs] = f2(srcFold, distVec_assSamp, dl_vec, rcRatio);
        
        %h_s = subplot(rowDivCnt,1,1:rowDivCnt-2);
        %cla(h_s);
        %imshow(imPalette_groupImgs);
        %titleStr = ['medoid(' num2str(i) ') from group (' num2str(medoidToClusterMap(i)) '-' clusterNames{medoidToClusterMap(i)} '), has ' num2str(n) ' samples assigned.rc(' mat2str(paletteRCSize_groupImgs) ')'];
        %title(titleStr);

        %h_s = subplot(rowDivCnt,1,rowDivCnt - 1);
        %cla(h_s);
        %medoidImg = retrieveImageFromDetailedLabels(srcFold, medoidID, handsStruct.detailedLabels);
        %imshow(medoidImg);
        %title(mat2str(handsStruct.detailedLabels(medoidID,:)));
        
        %h_s = subplot(rowDivCnt,1,rowDivCnt);
        %cla(h_s);
        %plot(1:n, distVec_sorted);
        
        %what are the biggest 5 increases?
        %show first 5 in plot
        %incVec = distVec_sorted(2:end)-distVec_sorted(1:end-1);
        %[incVecSrt, idxIncVex] = sort(incVec,'descend');
        %tickCnt = min(length(idxIncVex),5);
        %xtickVals = sort(idxIncVex(1:tickCnt))';
        %setXYTicks(h_s,  xtickVals, xtickVals, 90, 'XTick');
        %setXYTicks(h_s, distVec_sorted(xtickVals)', distVec_sorted(xtickVals)', 90, 'YTick');
        %titleStr = ['critical jumps at(' mat2str([xtickVals' distVec_sorted(xtickVals)],4) ')'];
        %title(titleStr);
        
        medoidLabel = medoidToClusterMap(i);
        [~, sortByDistIdx] = sort(distVec_assSamp);
        sampleLabelsForSortedDistances = dl_vec(sortByDistIdx,end);
        fScoreList = calcRunningStats(medoidLabel, sampleLabelsForSortedDistances);
        idx = find(fScoreList(2,:)>0.9,1,'last');
        if isempty(idx)
            idx = size(fScoreList,2);
        end
        
        disp(clusterNames{medoidLabel})
        [minDistAllowedPerMedoid(i), maxDistAllowedPerMedoid(i)] = fwf_getMaxDistAllowed(featureName, labelingStrategyStr, fScoreList, distVec_sorted, idx, medoidLabel);
    end
    
    if ~isempty(singleFrameMedoids)
        handsStructMedoids.dataCells(singleFrameMedoids,:) = [];
        handsStructMedoids.detailedLabels(singleFrameMedoids,:) = [];
        handsStructMedoids.imCropCells(singleFrameMedoids,:) = [];
        minDistAllowedPerMedoid(singleFrameMedoids) = [];
        maxDistAllowedPerMedoid(singleFrameMedoids) = [];
        medoidToClusterMap(singleFrameMedoids) = [];
    end
    
    handsStructMedoids.minDistAllowedPerMedoid = minDistAllowedPerMedoid;
    handsStructMedoids.maxDistAllowedPerMedoid = maxDistAllowedPerMedoid;
    handsStructMedoids.medoidStrings = clusterNames(medoidToClusterMap');
    %I need to run per some videos to label them by medoids and their
    %distance limits
    handsStructMedoids_fileName = [srcFold filesep num2str(surSlctListStruct.signIDList(1)) filesep 'handsStructMedoids_' num2str(surSlctListStruct.signIDList(1),'%03d') '_' distMatInfoStruct.featureName  '_' distMatInfoStruct.cropMethod '_' num2str(surSlctListStruct.single0_double1) '_' surSlctListStruct.labelingStrategyStr '.mat'];
    save(handsStructMedoids_fileName,'handsStructMedoids');
    handsStructMedoids_figureSaveName = [srcFold filesep num2str(surSlctListStruct.signIDList(1)) filesep 'handsStructMedoids_' num2str(surSlctListStruct.signIDList(1),'%03d') '_' distMatInfoStruct.featureName  '_' distMatInfoStruct.cropMethod '_' num2str(surSlctListStruct.single0_double1) '_' surSlctListStruct.labelingStrategyStr '.png'];
    saveas(hMedoids,handsStructMedoids_figureSaveName);

    if deleteDataFile && exist(fileSaveName.dataFile,'file')
        disp(['deleting (' fileSaveName.dataFile ') which has (handsStruct, problematicVids, surSlctListStruct, distMatInfoStruct)']);
        delete(fileSaveName.dataFile)
        disp(['deleted (' fileSaveName.dataFile ')']);
    end
end

function [minDistAllowedPerMedoid, maxDistAllowedPerMedoid] = fwf_getMaxDistAllowed(featName, labelingStrategyStr, fScoreList, distVec_sorted, idx, medoidLabel)
    switch featName
        case 'hf'
            minDistAllowedPerMedoid = min(distVec_sorted);
            if medoidLabel==1
                maxDistAllowedPerMedoid = 30;
                disp('the initial label distance is set to 30 by default');
            else%if fScoreList(1,idx) > 0.75
                if (fScoreList(2,idx)>0.99)
                    maxDistAllowedPerMedoid = max(min(distVec_sorted(idx)*4,50),40);
                else
                    maxDistAllowedPerMedoid = distVec_sorted(idx);
                    disp('the fScoreList is :');
                    disp(fScoreList);
                end
                disp(['distAllowed(' num2str(maxDistAllowedPerMedoid,'%4.2f')  '), idx(' num2str(idx) ') is selected as acc(' num2str(fScoreList(1,idx),'%4.2f') '),fscore(' num2str(fScoreList(2,idx),'%4.2f') ')']);
            %else
            %    maxDistAllowedPerMedoid(i) = fwf_getMedoidMaxDistFromUserInput(medoidID);
            end            
        case 'sn'
            maxDistAllowedPerMedoid = min(distVec_sorted(:));
            minDistAllowedPerMedoid = max(distVec_sorted(distVec_sorted~=0));
            disp(['distAllowed between (' num2str(maxDistAllowedPerMedoid,'%4.2f')  ',' num2str(minDistAllowedPerMedoid,'%4.2f')  ')']);     
        otherwise
            error(['unknown featName(' featName ')']);
    end

end

function acceptedBlocks_cht = fwf_getAcceptedBlock(srcFold, surVec, lh1_rh2_bh3, lrh_str, handsStruct, selectedRows_sur, curAssignedLabels, medoidImCell)
    selectedRows_cht = lh1_rh2_bh3==handsStruct.detailedLabels(:,4); %cht - current hand type            
    vid_cht_selectedRows = find(selectedRows_cht & selectedRows_sur);
    disp([lrh_str 'Cnt(' num2str(length(vid_cht_selectedRows)) ')']);                       
    %which klusters are found in this video?
    %slctOfIDs = find(videoLabels_lh==k);
    videoLabels_cht = curAssignedLabels(vid_cht_selectedRows);
    videoFrameIDs_cht = unique(sort(handsStruct.detailedLabels(vid_cht_selectedRows,5)));
    %I need to ceate a palette for the summary
    videoLabels_cht_all = zeros(1,max(videoFrameIDs_cht));
    videoLabels_cht_all(videoFrameIDs_cht) = videoLabels_cht;
    [cht_clusterCellString, cht_labelCount] = getClusterSummaryAsString(videoLabels_cht_all);
    %for each row, 
    %I would grab every "," delimetered group
    %and then 
    acceptedBlocks_cht = dispAcceptRejectAlgoResults(srcFold, surVec, cht_clusterCellString, cht_labelCount, lrh_str, medoidImCell);
end

function newLabels = fwf_getNewLabelsFromUserInput(k_selected)
    str = input(['Please write ' num2str(k_selected) ' new labels using comma as delimeter if you want to further group the found clusters:\r\n'],'s');
    errFound = [];
    if isempty(str)
        newLabels = 1:k_selected;
    else
        try
            newLabels = strsplit(str,',');
            newLabels = cell2mat(cellfun(@str2num,newLabels,'un',0));
        catch err
            errFound = err;
            newLabels = 1:k_selected;
        end
    end

    if length(newLabels)==k_selected
        %all elements must be between 1 and K
        problematicLabels = newLabels<1 | newLabels>k_selected;
        if sum(problematicLabels)>0
            %there are some problematic new labels
            disp(['Lables ' mat2str(newLabels(problematicLabels)) ' are problematic ']);
        else
            %everything is fine and we can group the labels
            disptable([1:k_selected;newLabels],[],'oldLabels|newLabels');
        end
    elseif isempty(newLabels)
        newLabels = 1:k_selected;
    else
        %either user didnt combine anything or there was an err
        if ~isempty(errFound) && isstruct(errFound)
            disp(['eror occured = ' errFound.message]);
        end        
    end
end

function distAllowed = fwf_getMedoidMaxDistFromUserInput(medoidID)
    str = input(['What is the appropriate max dist to allow medoid( ' num2str(medoidID) '):'],'s');
    errFound = [];
    if isempty(str)
        distAllowed = inf;
    else
        try
            distAllowed = str2double(str);
            disp(['distAllowed = ' num2str(distAllowed)]);
        catch err
            errFound = err;
            distAllowed = inf;
        end
    end
    %either user didnt combine anything or there was an err
    if ~isempty(errFound) && isstruct(errFound)
        disp(['eror occured = ' errFound.message]);
    end        
end

function [medoidImCell, handsStructMedoids, medoidToClusterMap, h, removedMedoidIdx] = fwf_displayMedoidCenterImages(srcFold, handsStruct, medoidHist_Best, newKlusterCell, k_selected, figID, clusterNames, labelingStrategyStr)
    %in figure(figID) show me all the cluster centers 
    kMax = size(medoidHist_Best,1);
    medoidIDsOdCluster = medoidHist_Best(k_selected, kMax-k_selected+1 : kMax);
    handsStructMedoids = struct;
    handsStructMedoids.dataCells = handsStruct.dataCells(medoidIDsOdCluster);
    handsStructMedoids.detailedLabels = handsStruct.detailedLabels(medoidIDsOdCluster,:);
    handsStructMedoids.imCropCells = handsStruct.imCropCells(medoidIDsOdCluster);
    randomColors = getRandomColorsMat(k_selected);
    kReduced = length(newKlusterCell);
    h = figure(figID);clf;
    maximizeFigureWindow(h);
    rowCnt = floor(sqrt(kReduced));
    colCnt = ceil(kReduced/rowCnt);
    medoidImCell = cell(1,kReduced);
    medoidToClusterMap = zeros(1,k_selected);
    removedMedoidIdx = [];
    for k=1:kReduced
        %here we need to group the medoids under newKlusterCell{k}
        curMedoidsToGroup = newKlusterCell{k};
        curMedoidAssignedLabels = handsStructMedoids.detailedLabels(curMedoidsToGroup,6);
        selectedHandShapeID = curMedoidAssignedLabels(1);
        if length(curMedoidAssignedLabels)>1
            accumulatedHandshapePowerResult = accumarray(curMedoidAssignedLabels,ones(size(curMedoidAssignedLabels))');
            [sortedAccum, idxAccum] = sort(accumulatedHandshapePowerResult,'descend');
            selectedHandShapeID = idxAccum(1);  
            if any(curMedoidAssignedLabels~=selectedHandShapeID)
                idx = curMedoidAssignedLabels~=selectedHandShapeID;
                
                medoidKlusterIDsToRemove = curMedoidsToGroup(idx);
                medoidIDsToRemove = medoidIDsOdCluster(medoidKlusterIDsToRemove);
                
                removedMedoidIdx = [removedMedoidIdx;medoidKlusterIDsToRemove medoidIDsToRemove'];
                curMedoidsToGroup(idx) = [];
                clear idx medoidKlusterIDsToRemove medoidIDsToRemove
            end
        end       
        medoidToClusterMap(curMedoidsToGroup) = selectedHandShapeID;
        subplot(rowCnt, colCnt,k);
        medoidImCell{1,k} = fwf_getImage(srcFold, handsStruct, medoidIDsOdCluster, curMedoidsToGroup, randomColors, labelingStrategyStr);
        imshow(medoidImCell{1,k});
        title({[clusterNames{selectedHandShapeID}],['centroidIDs - ' mat2str(medoidIDsOdCluster(curMedoidsToGroup))]});
    end
    if ~isempty(removedMedoidIdx)
        medoidIDsOdCluster(removedMedoidIdx(:,1)) = [];
        handsStructMedoids.dataCells(removedMedoidIdx(:,1)) = [];
        handsStructMedoids.detailedLabels(removedMedoidIdx(:,1),:) = [];
        handsStructMedoids.imCropCells(removedMedoidIdx(:,1)) = [];
        medoidToClusterMap(removedMedoidIdx(:,1)) = [];
    end
end

function im01_medoidIm = fwf_getImage(srcFold, handsStruct, medoidIDsOdCluster, curMedoidsToGroup, randomColors, labelingStrategyStr)
    surroundWithColorStruct = struct('thicknessParam',0.2,'colorParam',[0 0 0]);
    imCollage_imSizeMax = [50 50];
    clusterGroupCnt = length(curMedoidsToGroup);
    im_k = medoidIDsOdCluster(curMedoidsToGroup);
    labelID_medoidCenter = handsStruct.detailedLabels(im_k,6);
    if strcmpi(labelingStrategyStr,'assignedLabelsRemove1')
        labelID_medoidCenter = labelID_medoidCenter - 1;
    end
    if clusterGroupCnt==1
        im01_medoidIm = retrieveImageFromDetailedLabels(srcFold, im_k, handsStruct.detailedLabels);
        surroundWithColorStruct.colorParam = randomColors(labelID_medoidCenter,:);
        im01_medoidIm = putImg_IntoBox(im01_medoidIm, int32((1+surroundWithColorStruct.thicknessParam)*[size(im01_medoidIm,1) size(im01_medoidIm,2)]), true, surroundWithColorStruct);
    else
        [im01_medoidIm, paletteRCSize_medoidIm] = imCollageFuncs( 'createPalette', struct('imCnt',clusterGroupCnt,'channelCnt',3,'imSizeMax',3*imCollage_imSizeMax));
        for cToAdd = 1:clusterGroupCnt
            imageToAdd = retrieveImageFromDetailedLabels(srcFold, im_k(cToAdd), handsStruct.detailedLabels);
            labelToAdd = handsStruct.detailedLabels(im_k(cToAdd),6);
            surroundWithColorStruct.colorParam = randomColors(labelToAdd,:);
            if isempty(imageToAdd)
                continue
            end                
            im01_medoidIm = imCollageFuncs('insertIntoPalette', struct('imageID',cToAdd,'paletteRCSize',paletteRCSize_medoidIm,'imPalette',im01_medoidIm,'imageToAdd',imageToAdd,'boxSize',3*imCollage_imSizeMax, 'surroundWithColorStruct', surroundWithColorStruct));
        end                
    end
end

function [kMax, kVec] = fwf_getKMaxVec(clusteringParams)
    if isfield(clusteringParams,'kMax')
        kMax = clusteringParams.kMax;
        kVec = 1:clusteringParams.kMax;
    elseif isfield(clusteringParams,'kVec')
        kVec = clusteringParams.kVec;
        kMax = max(kVec);
    else
        error('wtf');
    end    
end

function acceptedBlocks = dispAcceptRejectAlgoResults(srcFold, surVec, clusterCellString, labelCount, lhrhstr, medoidImCell)
    switch lhrhstr
        case 'lh'
            lh1_rh2_bh3 = 1;
        case 'rh'
            lh1_rh2_bh3 = 2;
        case 'bh'
            lh1_rh2_bh3 = 3;
        otherwise
            error('it must be either 3 of them');
    end
    acceptedBlocks = zeros(labelCount,3);
    abc = 0;
    for i=1:labelCount
        curBlockDef = clusterCellString{i,2};
        curBlockDef = strsplit(curBlockDef, ',');
        curBlokPartCnt = size(curBlockDef,2);
        clusterID = str2double(clusterCellString{i,1});
        if clusterID==0
            continue;
        end
        for p=1:curBlokPartCnt
            curBlockPartStr = curBlockDef{p};
            curBlockPartStr = strsplit(curBlockPartStr, '-');
            if size(curBlockPartStr,2)==2
                %this is from i to j
                frFr = str2double(curBlockPartStr{1});
                toFr = str2double(curBlockPartStr{2});
                frCnt = toFr-frFr+1;
                disp(['for ' lhrhstr ', this is a ' num2str(frCnt) ' frame block from ' curBlockPartStr{1} ' to ' curBlockPartStr{2} ' which falls under cluster(' num2str(clusterID) ')']);
                %this part can be made more interactive!!
                
                medoidIm = medoidImCell{1,clusterID};
                imPalette_groupImgs = f(srcFold, surVec, lh1_rh2_bh3, frFr, toFr);
                figure(92);clf;
                subplot(2,1,1);
                imshow(medoidIm);title('medoid to assign');
                subplot(2,1,2);
                imshow(imPalette_groupImgs);title(['surVec' mat2str(surVec) ',frames(' num2str(frFr) '-' num2str(toFr) ')']);
                
                acc_rej_str = input('Please accept(a) or reject(anthing but a).','s');
                if strcmpi(acc_rej_str(1),'a')
                    disp('accepted by user');
                    abc = abc+1;
                    acceptedBlocks(abc,:) = [clusterID frFr toFr];
                else
                    disp('rejected by user');
                end
            elseif size(curBlockPartStr,2)==1
                %this is a single frame or first of consecutive
                %2 frames
                disp(['for ' lhrhstr ', this is a 1 or 2 frame(' curBlockPartStr{1} ') which will be discarded and not added to cluster(' num2str(clusterID) ')']);
            else
                error('what is this?');
            end
        end
        
    end
    acceptedBlocks(abc+1:end,:) = [];
end

function imPalette_groupImgs = f(srcFold, surVec, lh1_rh2_bh3, fromFrame, toFrame)
    imCollage_imSizeMax = [50 50];
    imCnt = toFrame-fromFrame+1;
    surroundWithColorStruct = struct('thicknessParam',0.2,'colorParam',[0 0 0]);
    [imPalette_groupImgs, paletteRCSize_distinctImgs] = imCollageFuncs( 'createPalette', struct('imCnt',imCnt,'channelCnt',3,'imSizeMax',imCollage_imSizeMax));
    for i = 1:imCnt
        %this can be distance info??
        distCur = 0.5;
        surroundWithColorStruct.colorParam = ones(1,3)*distCur;
        dl = [surVec lh1_rh2_bh3 (i+fromFrame-1)];
        imageToAdd = retrieveImageFromDetailedLabels(srcFold, 1, dl);
        if isempty(imageToAdd)
            continue
        end                
        imPalette_groupImgs = imCollageFuncs('insertIntoPalette', struct('imageID',i,'paletteRCSize',paletteRCSize_distinctImgs,'imPalette',imPalette_groupImgs,'imageToAdd',imageToAdd,'boxSize',3*imCollage_imSizeMax, 'surroundWithColorStruct', surroundWithColorStruct));
    end
end

function [imPalette_groupImgs, distVec_sorted, paletteRCSize_distinctImgs] = f2(srcFold, distVec, dl_vec, rcRatio)
    imCollage_imSizeMax = [50 50];
    imCnt = size(dl_vec,1);
    surroundWithColorStruct = struct('thicknessParam',0.2,'colorParam',[0 0 0]);
    [imPalette_groupImgs, paletteRCSize_distinctImgs] = imCollageFuncs( 'createPalette', struct('imCnt',imCnt,'channelCnt',3,'imSizeMax',imCollage_imSizeMax,'rcRatio',rcRatio));
    
    %sort according to distVec
    [distVec_sorted,idx] = sort(distVec);
    dl_vec = dl_vec(idx,:);
    clear idx;
    
    distVec = map2_a_b(distVec_sorted, 0.1, 0.9);
    for i = 1:imCnt
        %this can be distance info??
        distCur = 1-distVec(i);
        surroundWithColorStruct.colorParam = ones(1,3)*distCur;
        imageToAdd = retrieveImageFromDetailedLabels(srcFold, i, dl_vec);
        if isempty(imageToAdd)
            continue
        end                
        imPalette_groupImgs = imCollageFuncs('insertIntoPalette', struct('imageID',i,'paletteRCSize',paletteRCSize_distinctImgs,'imPalette',imPalette_groupImgs,'imageToAdd',imageToAdd,'boxSize',3*imCollage_imSizeMax, 'surroundWithColorStruct', surroundWithColorStruct));
    end
end