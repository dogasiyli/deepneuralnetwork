function [frameCntTable,videoCntTable, totalCnt] = createKHSCountTables(khsSummary, single0_double1, optParamStruct)
    if ~exist('optParamStruct','var')
        optParamStruct=[];
    end
    srcFold = getStructField(optParamStruct, 'srcFold', getVariableByComputerName('srcFold'));
    signIDList = getStructField(optParamStruct, 'signIDList', [7,13,49,69,74,85,86,87,88,89,96,112,125,221,222,247,269,273,277,278,290,296,310,335,339,353,396,403,404,424,428,450,521,533,535,536,537,583,586,593,636]);

    signCount = length(signIDList);
    khsCount = size(khsSummary,1);    
    frameCntTable = zeros(signCount,2+khsCount);%signID,noLabel,khsCount
    videoCntTable = zeros(signCount,2+khsCount);%signID,vidCnt,khsCnt
    colsOf_listSummaryTotal = 4;
    if single0_double1==0
        colsOf_listSummaryTotal = 1;
    end
    totalCnt = zeros(signCount,3);
    for si = 1:length(signIDList)
        s = signIDList(si);
        frameCntTable(si,1) = s;
        videoCntTable(si,1) = s;
        try
            fileName = [srcFold filesep num2str(s) filesep 'labelSummary.mat'];
            load(fileName,'labelSummary','listByRepitition','listSummaryTotal');%listSummaryTotal,labelSwitches_uniq,listByUser,listSummarySpecial
            signVideoCnt = size(listByRepitition,1);
            
            videoCntTable(si,2) = signVideoCnt;
            frameCntTable(si,2) = listSummaryTotal(1,colsOf_listSummaryTotal);
            
            %listSummaryTotal columns --> total|lh|rh|bh
            listSummaryTotal = getLabelSummaryTable(labelSummary);
            totalCnt(si,:) = [listSummaryTotal(1,colsOf_listSummaryTotal) listSummaryTotal(2,colsOf_listSummaryTotal) sum(listSummaryTotal(3:end,colsOf_listSummaryTotal))];%noLabel singleStill others
            %printClusterNameSummary(srcFold, s );
            [clusterNames, listByRepititionCols] = getClusterNames(srcFold, s, single0_double1);
            for khs_i = 1:length(clusterNames)
                khsName = clusterNames{khs_i};
                khsSummary_rowID = find(not(cellfun('isempty',strfind(khsSummary(:,1),khsName))));
                frameCntTable(si,2+khsSummary_rowID) = frameCntTable(si,2+khsSummary_rowID) + listSummaryTotal(1+khs_i,colsOf_listSummaryTotal);
                
                khsInHowManyVideos = sum(sum(listByRepitition(:,listByRepititionCols{khs_i})>0,2)>0);
                videoCntTable(si,2+khsSummary_rowID) = khsInHowManyVideos;
            end        
        catch err
            disp(['couldnt load labelSummary.mat - err(' err.message ')']);
            return
        end
    end
end

function [clusterNames, listByRepititionCols] = getClusterNames(srcFold, s, single0_double1)
    clusterNames_single = getClusterNamesAssignedFromTxt(srcFold, struct('signIDList',s,'single0_double1',0));
    clusterNames_both = getClusterNamesAssignedFromTxt(srcFold, struct('signIDList',s,'single0_double1',1));
    sCnt = length(clusterNames_single);
    bCnt = length(clusterNames_both);
    switch single0_double1
        case 0
            clusterNames = clusterNames_single;
            listByRepitition_cols_beg = 3 + 2;
            listByRepitition_cols_end = listByRepitition_cols_beg + sCnt - 1 ;
            listByRepititionCols = listByRepitition_cols_beg : listByRepitition_cols_end;
            listByRepititionCols = [listByRepititionCols;listByRepititionCols+sCnt+1];
            listByRepititionCols = mat2cell(listByRepititionCols,2,ones(1,sCnt));
        case 1
            clusterNames = clusterNames_both; 
            listByRepitition_cols_beg = 3 + 2*(sCnt+1);
            listByRepitition_cols_end = listByRepitition_cols_beg + bCnt;
            listByRepititionCols = listByRepitition_cols_beg : listByRepitition_cols_end;
            listByRepititionCols = mat2cell(listByRepititionCols,1,ones(1,bCnt)); %#ok<MMTC>
    end
    
end