function labelsStripe = createLabelStripe(rowSize,stripImSizeVec,randomColors,uniqLabelsVec,labelsVec)
    frameCnt = length(labelsVec);
    newImSiz = stripImSizeVec(1);
    imSepSiz = stripImSizeVec(2);
    imTotSiz = sum(stripImSizeVec);
    labelsStripe = zeros(rowSize,imTotSiz*(frameCnt-1)+newImSiz,3,'uint8'); 
    for c=1:length(randomColors)
        imagesOfCurrentLabel = find(labelsVec==uniqLabelsVec(c));
        for f=1:length(imagesOfCurrentLabel)
            frameID = imagesOfCurrentLabel(f);
            labelsStripe(:,1+(frameID-1)*imTotSiz:frameID*imTotSiz-imSepSiz, 1) = uint16(randomColors(c,1)*255);
            labelsStripe(:,1+(frameID-1)*imTotSiz:frameID*imTotSiz-imSepSiz, 2) = uint16(randomColors(c,2)*255);
            labelsStripe(:,1+(frameID-1)*imTotSiz:frameID*imTotSiz-imSepSiz, 3) = uint16(randomColors(c,3)*255);
        end
    end
end

