function clustersStruct = createNewCluster(clustersStruct, newClusterID, subClusterID, clusterProposalFresh, centerSampleID, allowedDist)
    %dataset, definition, model
    
    allowedDist = min(max(allowedDist,30),50);
    
    hogFeat_asCenterVec  = clustersStruct.dataset.hogFeats(centerSampleID,:);
    clustersStruct.dataset.assignments = [clustersStruct.dataset.assignments;centerSampleID newClusterID subClusterID 0];%[1_datasetID, clusterID, subClusterID, distToCenterSample]
    
    if isfield(clusterProposalFresh, 'clusterName')
        clustersStruct.definition.clusterNames{newClusterID,1} = clusterProposalFresh.clusterName;%will inrease the cell length when added
    else
        assert(~isempty(clustersStruct.definition.clusterNames{newClusterID,1}),'this has to have a name defined already');
    end
    if isfield(clusterProposalFresh, 'clusterType')
        clustersStruct.definition.clusterTypes{newClusterID,1} = clusterProposalFresh.clusterType;%will inrease the cell length when added
    else
        assert(~isempty(clustersStruct.definition.clusterTypes{newClusterID,1}),'this has to have a type defined already');
    end
    if isfield(clusterProposalFresh, 'clusterImage')
        clustersStruct.definition.clusterImage{newClusterID, 1} = clusterProposalFresh.clusterImage;%initially set the cluster image from this, later check the <clusterProposalFresh.minMatchValue>
    else
        assert(~isempty(clustersStruct.definition.clusterImage{newClusterID,1}),'this has to have a clusterImage defined already');
    end
    clustersStruct.definition.uniqueSignIDs{newClusterID, subClusterID} = clusterProposalFresh.differentSignIDs;
    clustersStruct.definition.subClusterImages{newClusterID, subClusterID} = clustersStruct.dataset.imagesFineCropped(centerSampleID,:);
    clustersStruct.definition.maxDistList{newClusterID, subClusterID} = [newClusterID subClusterID allowedDist];
    
    clustersStruct.model.clusterDefinitionMat = [clustersStruct.model.clusterDefinitionMat;newClusterID, subClusterID, allowedDist];
    [clustersStruct.model.clusterDefinitionMat, idx] = sortrows(clustersStruct.model.clusterDefinitionMat,[1 2]);
    clustersStruct.model.centerVecMat = [clustersStruct.model.centerVecMat; hogFeat_asCenterVec];%each row is the assigned HoG vector of the subcluster
    clustersStruct.model.centerVecMat = clustersStruct.model.centerVecMat(idx,:);
    disptable(clustersStruct.model.clusterDefinitionMat,'newClusterID|subClusterID|allowedDist');
end