function hogV2MapStruct = createOrLoadHogV2DifMap(sizeImg, blockSize, stride, binCount) 

    hogV2MapsFolder = [getVariableByComputerName('srcFold') filesep 'hogV2MapsFolder'];
    hogV2MapFileName = ['im_r' num2str(sizeImg(1),'%d') '_c' num2str(sizeImg(2),'%d') '_br' num2str(blockSize(1),'%d') '_bc' num2str(blockSize(2),'%d') '_st' num2str(stride,'%d') '.mat'];
    hogV2MapFileToLoad = [hogV2MapsFolder filesep hogV2MapFileName];
    if exist(hogV2MapFileToLoad,'file')
        load(hogV2MapFileToLoad,'hogV2MapStruct');
        return
    end
    
    patchSize = blockSize;
    countSamples = 1;
    inputDataChannelSize = 1;
    tutorialMode = false;
    [map_singleInput] = mapDataForConvolution( patchSize, stride, sizeImg, countSamples, inputDataChannelSize, tutorialMode);
    
    
    hogPixInputSingle = (((map_singleInput(:)-1)*binCount + 1) + (0:binCount-1))';
    hogImageVecLen = prod(blockSize)*binCount;
    hogPixInputSingle = reshape(hogPixInputSingle, hogImageVecLen,[])';
    
    eachImageBlockCnt = size(map_singleInput,2);
    rowColMap = zeros(eachImageBlockCnt,6);
    for i = 1:eachImageBlockCnt
        leftTopPixelID = map_singleInput(1,i);%first row the specified column 
        [rowBegin, colBegin] = ind2sub(sizeImg, leftTopPixelID);
        %assert(rowBegin == leftTopPixelID,'rowBegin == leftTopPixelID , must be same');
        rowEnd = rowBegin + patchSize(1) - 1;
        colEnd = colBegin + patchSize(2) - 1;
        rowColMap(i,:) = [rowBegin rowEnd colBegin colEnd sizeImg];
    end
    
    hogV2MapStruct = struct;
    hogV2MapStruct.hogPixInputSingle = hogPixInputSingle;
    hogV2MapStruct.rowColMap = rowColMap;
    if ~exist(hogV2MapsFolder,'dir')
        mkdir(hogV2MapsFolder);
    end
    save(hogV2MapFileToLoad,'hogV2MapStruct');
end
