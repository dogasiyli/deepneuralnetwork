function [stripIm, frameCnt] = createStripIm_NoPrediction(userFolderName, figIDBegin, enforceRecrate, stripImSizeVec, handIDsToCreateStripIm)
    if ~exist('handIDsToCreateStripIm', 'var') || isempty(handIDsToCreateStripIm)
        handIDsToCreateStripIm = [1 2 3];
    end
    newImSiz = stripImSizeVec(1);
    imSepSiz = stripImSizeVec(2);
    imTotSiz = sum(stripImSizeVec);
    
    stripImSize = [newImSiz,newImSiz];
    stripIm = cell(length(handIDsToCreateStripIm),1);    
    %create the summary strip
    %for RH, LH, BH
    [~, frameCnt] = getFileList( [userFolderName  filesep 'cropped' filesep], '.png');
    if frameCnt==0
        %check if croppedZip exist
        cropVidFrames(userFolderName, [], false);
        %unzip it
        croppedPicsZipName = [userFolderName filesep 'cropped.zip'];
        if exist(croppedPicsZipName,'file')
            frameCnt = unzipProperly(userFolderName,'cropped');
            disp([num2str(frameCnt) ' files extracted from ' croppedPicsZipName]);
        else
            disp([ croppedPicsZipName ' doesnt exist']);
        end   
        [~, frameCnt] = getFileList( [userFolderName  filesep 'cropped' filesep], '.png');
    end
    frameCnt = round(frameCnt/3);
    
    stripPictureSize = [2*newImSiz+1*imSepSiz,imTotSiz*(frameCnt-1)+newImSiz,3];
    for handID = handIDsToCreateStripIm%LRB-1:3
        %clusterSuitabilityMat{j,1} = .{bestCropInfo,clusterProbs, clusterProbsBestCropId}
        LRBchar = getLRBChar(handID);
        stripImFileName = [userFolderName filesep 'vidSummary' LRBchar 'H.png'];
        stripIm{handID,1} = [];
        if exist(stripImFileName,'file')
            stripIm{handID,1} = imread(stripImFileName);
        end
        loadedPictureSize = size(stripIm{handID,1});
        frameCnt_new = 1+((loadedPictureSize(2)-newImSiz)/imTotSiz);
        if (frameCnt~=frameCnt_new)
            stripIm{handID,1} = [];
        end
        if ~enforceRecrate && ~isempty(stripIm{handID,1}) && (length(loadedPictureSize)==3 && sum(abs(stripPictureSize-loadedPictureSize)))
            %the image already created and now loaded
            continue
        end
        stripIm{handID,1} = zeros(stripPictureSize, 'uint8');
        for frameID=1:frameCnt
            %get the related frame
            srcFold = [userFolderName filesep 'cropped' filesep];
            srcIm = imread([srcFold 'crop_' LRBchar 'H_' num2str(frameID,'%03d') '.png']);
            srcIm = imresize(srcIm,stripImSize);
            
            if frameID<10
                rs = floor(newImSiz/3);
            elseif frameID<100
                rs = 2*floor(newImSiz/3);
            else
                rs = newImSiz;
            end
            numIm = getNumberImage(frameID, newImSiz, rs, stripImSize);
            
            if figIDBegin>0
                figure(figIDBegin);clf;
                subplot(1,2,1); imshow(srcIm);title(['srcIm - ' LRBchar ' - ' num2str(frameID)]);
                subplot(1,2,2); 
                imshow(numIm);title('imageID');
                drawnow;
            end
            %numIm should go to first row
            stripIm{handID,1}(1:newImSiz, 1+(frameID-1)*imTotSiz:frameID*imTotSiz-imSepSiz,2) = uint16(255*numIm);%green
            %the video image should go second row
            stripIm{handID,1}(1*imTotSiz:1*imTotSiz+newImSiz-1, 1+(frameID-1)*imTotSiz:frameID*imTotSiz-imSepSiz,:) = srcIm;          
        end
        imwrite(stripIm{handID,1}, stripImFileName);
    end
end