function m = createSurfNormOfDepthImage(srcFolder, imageID, blockCnt, useGroupIDInfo, figIDs, removeFromDisplayTresh)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

%% 1. assign unpassed optional params
    if ~exist('blockCnt','var') || isempty(blockCnt)
        blockCnt = [20 20];
    elseif length(blockCnt)==1
        blockCnt = [blockCnt blockCnt];
    end
    if ~exist('useGroupIDInfo','var') || isempty(useGroupIDInfo) || length(useGroupIDInfo)<3
        %if useGroupIDInfo is not passed or
        %is empty or
        %is false
        %then do not use group info
        useGroupIDInfo = [1 0 0];
    end
    if ~exist('figIDs','var')
        figIDs = [];
    end
    if ~exist('removeFromDisplayTresh','var')
        removeFromDisplayTresh = [];
    end

    %the area around the hand center
    cropAreaSize = 30;
    
%% 2. Load necessary variables from file    
    depth = [];
    skeleton = [];    
    load([srcFolder filesep 'depth.mat']);
    load([srcFolder filesep 'skeleton.mat']);
    
    depthIm = depth(:,:,imageID);
    depthIm(depthIm>1900) = 1900;   
    [depthImFilled, fillHistB] = fill0s(depthIm, 10);
    
%% 3. visualize 5 by 3 figure scenario   %figIDs(1) 
    rc = 5;
    cc = 3;
    if ~isempty(figIDs) && length(figIDs)>=1 &&  figIDs(1)>0
        figure(figIDs(1));clf;
        % 1  2  3
        % 4  5  6
        % 7  8  9
        %10 11 12
        %13 14 15
    
        subplot(rc,cc,[1 4]);
        imagesc(depthIm);colorbar;
        title(['The whole image(' num2str(imageID) ')']);

        subplot(rc,cc,[10 13]);
        imagesc(depthImFilled);colorbar;
        title('The whole image 0s filled');
        
        figMat_01_5_3 = [[rc cc 2 5 11 14];[rc cc 3 6 12 15]];

        [cropImFilled_L, grpIDs_L] = getHandAndDisplay(depthIm, skeleton, imageID, cropAreaSize, 'Left', figMat_01_5_3(1,:));
        [cropImFilled_R, grpIDs_R] = getHandAndDisplay(depthIm, skeleton, imageID, cropAreaSize, 'Right', figMat_01_5_3(2,:));
        
        if ~isempty(figIDs) && length(figIDs)>=1 && figIDs(1)>0
            optionalParamsStruct = struct('figVec',setFigVec(figIDs, 1, rc, cc, 8, 1, 1),'removeFromDisplayTresh', removeFromDisplayTresh);
            [faceNorms_L, quiverMat_L] = exploreImportantBlocks(cropImFilled_L, grpIDs_L, blockCnt, optionalParamsStruct, useGroupIDInfo);%#ok<ASGLU,NASGU>
            optionalParamsStruct.figVec = setFigVec(figIDs, 1, rc, cc, 9, 1, 1);
            [faceNorms_R, quiverMat_R] = exploreImportantBlocks(cropImFilled_R, grpIDs_R, blockCnt, optionalParamsStruct, useGroupIDInfo);%#ok<ASGLU,NASGU>
        end
    else
        [cropImFilled_L, grpIDs_L] = getHandAndDisplay(depthIm, skeleton, imageID, cropAreaSize, 'Left');
        [cropImFilled_R, grpIDs_R] = getHandAndDisplay(depthIm, skeleton, imageID, cropAreaSize, 'Right');        
    end
    
    %blur the cropFilled images

    %hogStruct = struct('blockCounts',[10 10],'histogramBinCount',9);
    %[hogVec_L, visualHogImage_L, combinedImageGray_L, rgbIm_L] = exractHOG_dg(cropImFilled_L, hogStruct, 'blockSteps');
    %[hogVec_R, visualHogImage_R, combinedImageGray_R, rgbIm_R] = exractHOG_dg(cropImFilled_R, hogStruct, 'blockSteps');
        
%% 4. visualize 1 by 2 figure scenario  figIDs(2)
    if ~isempty(figIDs) && length(figIDs)>=2 && figIDs(2)>0
        figure(figIDs(2));clf;

        optionalParamsStruct = struct('figVec',setFigVec(figIDs, 2, 1, 2, 1, 1, 1),'removeFromDisplayTresh', removeFromDisplayTresh);
        exploreImportantBlocks(cropImFilled_L, grpIDs_L, blockCnt, optionalParamsStruct, useGroupIDInfo);        
        
        optionalParamsStruct.figVec = setFigVec(figIDs, 2, 1, 2, 2, 1, 1);
        exploreImportantBlocks(cropImFilled_R, grpIDs_R, blockCnt, optionalParamsStruct, useGroupIDInfo); 
        
        clear optionalParamsStruct;
    end
    
%% 5. visualize 5 by 4 figure scenario    
    rc = 5;
    cc = 4;
    if ~isempty(figIDs) && length(figIDs)>=3 && figIDs(3)>0
        
        cropArea = getCropArea(skeleton, imageID, struct('colDepStr', 'depth'));
        
        figure(figIDs(3));clf;
        % 1   2   3  4
        % 5   6   7  8
        % 9  10  11 12
        % 13 14  15 16
        % 17 18  19 20
    
        subplot(rc,cc,[2 3 6 7]);
        imagesc(depthImFilled);colorbar;
        title(['The whole image(' num2str(imageID) ')']);

        subplot(rc,cc,[14 15 18 19]);
        ca = cropArea(8,:);
        imagesc(depthImFilled(ca(2):ca(2)+ca(4), ca(1):ca(1)+ca(3)));colorbar;
        title('The torso');
        
        figMat_01_5_3 = [[rc cc -1 17 1 -1];[rc cc -1 20 4 -1]];

        [cropImFilled_L, grpIDs_L] = getHandAndDisplay(depthIm, skeleton, imageID, cropAreaSize, 'Left', figMat_01_5_3(1,:));
        [cropImFilled_R, grpIDs_R] = getHandAndDisplay(depthIm, skeleton, imageID, cropAreaSize, 'Right', figMat_01_5_3(2,:));
        
        optionalParamsStruct = struct('figVec',setFigVec(figIDs, 3, rc, cc,  5, 1, 1),'removeFromDisplayTresh', removeFromDisplayTresh);%right, all pathces
        exploreImportantBlocks(cropImFilled_L, grpIDs_L, blockCnt, optionalParamsStruct, [1 0 0]); 
        optionalParamsStruct.figVec = setFigVec(figIDs, 3, rc, cc,  9, 1, 1);%right, reject if includes background
        exploreImportantBlocks(cropImFilled_L, grpIDs_L, blockCnt, optionalParamsStruct, [0 1 0]); 
        optionalParamsStruct.figVec = setFigVec(figIDs, 3, rc, cc, 13, 1, 1);%right, reject if not invluce hand
        exploreImportantBlocks(cropImFilled_L, grpIDs_L, blockCnt, optionalParamsStruct, [0 0 1]);         
        
        optionalParamsStruct.figVec = setFigVec(figIDs, 3, rc, cc,  8, 1, 1);%right, all pathces
        exploreImportantBlocks(cropImFilled_R, grpIDs_R, blockCnt, optionalParamsStruct, [1 0 0]);  
        optionalParamsStruct.figVec = setFigVec(figIDs, 3, rc, cc, 12, 1, 1);%right, reject if includes background
        exploreImportantBlocks(cropImFilled_R, grpIDs_R, blockCnt, optionalParamsStruct, [0 1 0]);        
        optionalParamsStruct.figVec = setFigVec(figIDs, 3, rc, cc, 16, 1, 1);%right, reject if not invluce hand
        exploreImportantBlocks(cropImFilled_R, grpIDs_R, blockCnt, optionalParamsStruct, [0 0 1]);  
    end    
    
    try
        m = frame2im(getframe(gcf));
    catch
        m = [];
    end
end

function figVec = setFigVec(figIDs, inds, rc, cc, subPlotID, lineWidthVal, markerSizeVal)
    figVec = [];
    if ~isempty(figIDs)
        try %#ok<TRYNC> - if anything goes wrong just return empty
            figVec = [figIDs(inds) rc cc subPlotID, lineWidthVal, markerSizeVal];
        end
    end
end

%create histogram and find 3 peaks
%1-background - deepest
%2-body
%3-hand
%4-0
%5-fill 0's with the nearest higest value
function [cropImFilled, grpIDs, fillHist] = getHandAndDisplay(depthIm, skeleton, imageID, cropAreaSize, handStr, subplotVec) %#ok<INUSL> skeleton and imageID is in eval
    %handStr - 'Left' or 'Right'
    %handPos = skeleton.HandLeft(imageID,10:11);
    handPos = [];
    eval(['handPos = skeleton.Hand' handStr '(imageID,10:11);']);
    
    %cropAreaSize = 30;
    cropArea_Beg = round(handPos-cropAreaSize);
    cropArea_End = round(handPos+cropAreaSize);
    crpArea = [cropArea_Beg(2) cropArea_End(2) cropArea_Beg(1) cropArea_End(1)];%[ySt yEn xSt xEn]
    
    cropIm = depthIm(crpArea(1):crpArea(2),crpArea(3):crpArea(4));
    
    [cropImFilled, imColor, grpIDs, grpMedian, fillHist] = reAdjustDepthImHand(cropIm, 3); %#ok<ASGLU> grpMedian - can stay-no harm

    if exist('subplotVec','var') && ~isempty(subplotVec)
        
        if subplotVec(3)>0
            subplot(subplotVec(1), subplotVec(2), subplotVec(3));
            imagesc(cropIm);colorbar;
            set(gca,'XTick',[1 size(cropIm,2)]);
            set(gca,'YTick',[1 size(cropIm,1)]);
            title([handStr ' hand']);
        end
        
        if subplotVec(4)>0
            subplot(subplotVec(1), subplotVec(2), subplotVec(4));
            imagesc(cropImFilled);colorbar;
            set(gca,'XTick',[1 size(cropIm,2)]);
            set(gca,'YTick',[1 size(cropIm,1)]);
            title([handStr ' hand 0s filled']);
        end

        if subplotVec(5)>0
            subplot(subplotVec(1), subplotVec(2), subplotVec(5));
            image(imColor);
            set(gca,'XTick',[1 size(cropIm,2)]);
            set(gca,'YTick',[1 size(cropIm,1)]);
        end

        if subplotVec(6)>0
            subplot(subplotVec(1), subplotVec(2), subplotVec(6));
            hist(grpIDs(:),1:3);
            set(gca,'YTick',[]);
            set(gca,'XTickLabel',{'Hand-Red','Body-Green','Bckgrnd-Blue'});
            xtickangle(45);
        end
    end
    
    %pass cropImFilled to get a descent 3d point cloud
    %X = get3dPointsOfDepthPxels(depthIm,  crpArea);
end

% cropIm_nnmf = double(cropIm);
% %cropIm_nnmf(cropIm_nnmf==0) = NaN;
% [W0, H0] = nnmf(cropIm_nnmf, 20, 'algorithm', 'mult');
% cropIm_nnmf = W0*H0;
% 
% subplot(rc,cc,3);
% imagesc(cropIm_nnmf);colorbar;
% title('5mult');


% leftHandPos = skeleton.HandLeft(imageID,10:11);
% 
% cropArea_Beg = round(leftHandPos-cropAreaSize);
% cropArea_End = round(leftHandPos+cropAreaSize);
% 
% cropIm = depthIm(cropArea_Beg(2):cropArea_End(2),cropArea_Beg(1):cropArea_End(1));
% 
% subplot(rc,cc,2);
% imagesc(cropIm);colorbar;
% title('Left hand');
% 
% cropImFilled = fill0s(cropIm, 10);
% subplot(rc,cc,4);
% imagesc(cropImFilled);colorbar;
% title('Left hand 0s filled');
% 
% [imColor, grpIDs] = setRGBKmeans(cropImFilled);
% subplot(rc,cc,6);
% image(imColor);
% 
% subplot(rc,cc,5);
% hist(grpIDs,1:3);