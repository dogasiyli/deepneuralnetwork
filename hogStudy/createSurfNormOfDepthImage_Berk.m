function m = createSurfNormOfDepthImage_Berk(cropFilledLH, cropFilledRH, blockCnt, useGroupIDInfo, figIDs, removeFromDisplayTresh)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

%% 1. assign unpassed optional params
    if ~exist('blockCnt','var') || isempty(blockCnt)
        blockCnt = [20 20];
    elseif length(blockCnt)==1
        blockCnt = [blockCnt blockCnt];
    end
    if ~exist('useGroupIDInfo','var') || isempty(useGroupIDInfo) || length(useGroupIDInfo)<3
        %if useGroupIDInfo is not passed or
        %is empty or
        %is false
        %then do not use group info
        useGroupIDInfo = [1 0 0];
    end
    if ~exist('figIDs','var')
        figIDs = [1:5 7];
    end
    if ~exist('removeFromDisplayTresh','var')
        removeFromDisplayTresh = [];
    end
    
    %the area around the hand center
    %cropAreaSize = 30;
    %[cropFilledBody, cropFilledLH, cropFilledRH] = compareFillMethodsDepthImage_Berk(srcFolder,imageID, cropAreaSize, figIDs(1:5), [10 3], true);
    [imColor_LH, grpIDs_LH, grpMedian_LH] = setRGBKmeans(cropFilledLH);
    [imColor_RH, grpIDs_RH, grpMedian_RH] = setRGBKmeans(cropFilledRH);

    
%% 2. Load necessary variables from file    
    
%% 3. visualize 5 by 3 figure scenario   %figIDs(1) 
        
%% 4. visualize 1 by 2 figure scenario  figIDs(2)
    if ~isempty(figIDs) && length(figIDs)>=6 && figIDs(6)>0
        figure(figIDs(6));clf;
        optionalParamsStruct = struct('figVec',setFigVec(figIDs, 6, 1, 2, 1, 1, 1),'removeFromDisplayTresh', removeFromDisplayTresh);
        exploreImportantBlocks(cropFilledLH, grpIDs_LH, blockCnt, optionalParamsStruct, useGroupIDInfo);
        optionalParamsStruct.figVec = setFigVec(figIDs, 6, 1, 2, 2, 1, 1);
        exploreImportantBlocks(cropFilledRH, grpIDs_RH, blockCnt, optionalParamsStruct, useGroupIDInfo);
        clear optionalParamsStruct;
    end

    try
        m = frame2im(getframe(gcf));
    catch
        m = [];
    end
end

function figVec = setFigVec(figIDs, inds, rc, cc, subPlotID, lineWidthVal, markerSizeVal)
    figVec = [];
    if ~isempty(figIDs)
        try %#ok<TRYNC> - if anything goes wrong just return empty
            figVec = [figIDs(inds) rc cc subPlotID, lineWidthVal, markerSizeVal];
        end
    end
end