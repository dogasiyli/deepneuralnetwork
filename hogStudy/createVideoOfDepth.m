function createVideoOfDepth(srcFolder)
    load([srcFolder filesep 'depth.mat']);
    frameCount = size(depth,3);
    
    blockCnt = 20;
    useGroupIDInfo = [];
    figIDs = [-1 -1 211];
    removeFromDisplayTresh = [];
    
    v = VideoWriter([[srcFolder filesep] 'depthVideo.avi']);
    v.FrameRate=5;
    open(v);
    for i=1:frameCount
        m = createSurfNormOfDepthImage(srcFolder, i, blockCnt, useGroupIDInfo, figIDs, removeFromDisplayTresh);
        m = imresize(m, [800,1400]);
        writeVideo(v,m);
        writeToGif(srcFolder, i, m);
    end
    close(v);
end

function writeToGif(srcFolder, frameID, m)
    filename = [srcFolder filesep 'depthVideo.gif'];
    [imind,cm] = rgb2ind(m,256);
    if frameID == 1
      imwrite(imind,cm,filename,'gif', 'Loopcount',inf);
    else
      imwrite(imind,cm,filename,'gif','WriteMode','append');
    end
end