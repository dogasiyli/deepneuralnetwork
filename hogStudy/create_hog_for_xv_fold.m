function rand_xv_cell = create_hog_for_xv_fold(fold, k_fold_cnt, rand_seed, enforce_recreate)
    %fold = '/home/doga/DataFolder/neuralNetHandImages_nos11_rs224/imgs' - laptop
    if ~exist('fold','var')
        fold = '/home/doga/DataFolder/neuralNetHandImages_nos11_rs224/imgs';
    end
    if ~exist('k_fold_cnt','var')
        k_fold_cnt = 5;
    end
    if ~exist('rand_seed','var')
        rand_seed = 1;
    end
    if ~exist('enforce_recreate','var')
        enforce_recreate = false;
    end
    list_dict = get_list_dict(fold, false);
    if strcmpi(list_dict(end),'')
        list_dict(end) = [];
    end
    img_cnt = size(list_dict,2);
    
    %for rand_seed we will go over each test user
    %5 different folds
    %assign train, validation, test indices over list_dict
    test_user_list = 2:7;
    cv_list = 1:k_fold_cnt;
    row_cnt = length(test_user_list)*length(cv_list);
    rand_xv_cell = cell(row_cnt,5);%test user, cv_id, train, valid, test
    if k_fold_cnt==5 && rand_seed<0
        xv_file_name = [fold filesep 'rand_xv_cell_byuser.mat'];
    else
        xv_file_name = [fold filesep 'rand_xv_cell_rs' num2str(rand_seed) '_xv' num2str(k_fold_cnt) '.mat'];
    end
    if exist(xv_file_name, 'file')
        load(xv_file_name,'rand_xv_cell');
    else
        rand_xv_cell = create_k_fold_xv(fold, k_fold_cnt, rand_seed, enforce_recreate);
    end
    
    hogImArrFileName = [fold filesep 'hog_10_9.mat'];
    create_hog_file = ~exist(hogImArrFileName, 'file');
    if ~create_hog_file
        load(hogImArrFileName,'hogImArr');
        create_hog_file = size(hogImArr,1)~=img_cnt; %#ok<NODEF>
    end
    if create_hog_file
        hogImArr = [];
        dispstat('','init');
        fr = 1;
        khs_prev = '';
        i=1;
        while i<=img_cnt
            %hogImArr(:,i) = get_hog_im(list_dict{i});
            [khsName, khsName_Fold] = get_names_and_folds(list_dict{i});
            khs_cur = [khsName_Fold filesep khsName];
            if ~strcmpi(khs_cur, khs_prev)
                dispstat(newline,'keepprev');
                disp_w_style(['khs changed fr(' khs_prev ') to(' khs_cur ')'],[1 0 1],true)
                %the name has changed so I can check if hog file exist
                hog_file_cur = [fold filesep khs_cur '_hog_10_9.mat'];
                hog_file_pre = [fold filesep khs_prev '_hog_10_9.mat'];
                if ~exist(hog_file_pre,'file') && (i-1-fr>0)
                    %save from fr to i-1
                    hog_khs = hogImArr(fr:i-1,:);
                    save(hog_file_pre,'hog_khs');
                    disp_w_style(['hog saved for(' khs_prev '), fr(' num2str(fr) ')-to(' num2str(i-1) ')'],[0 1 1],true)
                end                
                if exist(hog_file_cur,'file')
                    %I can load this
                    load(hog_file_cur,'hog_khs');
                    %now that loaded I need to add the loaded to hogImArr
                    hogcnt = size(hog_khs,1);
                    fr = i;
                    to = i+hogcnt-1;
                    hogImArr(fr:to,:) = hog_khs;
                    %set i to the next handshapes name
                    i = to+1;
                    khs_prev = [khsName_Fold filesep khsName];
                    disp_w_style(['hog loaded for(' khs_cur '), fr(' num2str(fr) ')-to(' num2str(to) ')'],[1 1 0],true)
                    fr = i;                    
                    continue
                else
                    %nothing to load so fr should be set to i for saving
                    %further
                    fr = i;
                end
            end
            [hogIm, handShapeName] = get_hog_im(fold, list_dict{i}, false);
            dispstat([handShapeName ' -> ' num2str(i) ' of ' num2str(img_cnt)]);
            hogImArr(i,:) = hogIm';
            
            khs_prev = [khsName_Fold filesep khsName];
            i=i+1;
        end
        save([fold filesep 'hog_10_9.mat'],'hogImArr');    
    end
end

function [khsName, khsName_Fold] = get_names_and_folds(dict_item)
    khsName = split(dict_item,'*');
    khsName = strtrim(khsName{end-1});
    khsName_Fold = strrep(khsName,'Cift','');
end

function [hogIm, handShapeName] = get_hog_im(fold, list_entry, only_test)
    hogStruct =struct('blockCounts',[10 10], 'histogramBinCount', 9);
    handInfo = struct('handDominant','LH', 'handCur', 'LH');
    
    cv = strtrim(split(list_entry,'*'));
    handShapeName = cv{6};
    % im_name = 'headShow_049201_L_00_034.png';
    im_name_beg = [cv{6} '_' pad(cv{1},3,'left','0') cv{2} pad(cv{3},2,'left','0') '_' cv{7} '_'];
    im_name_end = [pad(cv{4},3,'left','0') '.png'];
    im_fold = [fold filesep strrep(cv{6},'Cift','')];
    files = getFileList(im_fold, im_name_end, im_name_beg, false);
    if length(files)~=1
        if (only_test)
            dispstat([im_name_beg ' + ' im_name_end],'keepthis');
            files
            dispstat('\n','init');
            hogIm = [];
            return
        else
            error(['files have length(' num2str(length(files)) ')']);
        end
    end
    x_In = imread([im_fold filesep files{1}]);
    handInfo.handCur = [cv{7} 'H'];
%     
%     %if length(imSizeCur)==3 && imSizeCur(3)==3
    x_Gray = rgb2gray(x_In);
%     %end
    x_Gray = flipForHOG( x_Gray, handInfo);
    hogIm = exractHOG_dg(x_Gray, hogStruct);
end