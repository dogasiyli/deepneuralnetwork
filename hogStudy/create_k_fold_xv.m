function rand_xv_cell = create_k_fold_xv(fold, k_fold_cnt, rand_seed, enforce_recreate)
    %fold = '/home/doga/DataFolder/neuralNetHandImages_nos11_rs224/imgs' - laptop
    fold_list_khs = getFolderList(fold, false, false);
    csv_file_name = [fold filesep 'cell_cnt_per_user_' num2str(length(fold_list_khs)) '.csv'];
    if ~exist(csv_file_name,'file')
        validate_hand_imgs_fold(fold);
    end
    if ~exist('enforce_recreate','var')
        enforce_recreate = false;
    end
    list_dict = get_list_dict(fold, true);
    cell_cnt_per_user = read_csv_file_xv(csv_file_name);
    
    %for rand_seed we will go over each test user
    %5 different folds
    %assign train, validation, test indices over list_dict
    test_user_list = 2:7;
    cv_list = 1:k_fold_cnt;
    row_cnt = length(test_user_list)*length(cv_list);
    rand_xv_cell = cell(row_cnt,5);%test user, cv_id, train, valid, test
    row_id = 1;
    if rand_seed>0 || k_fold_cnt~=5
        rng(rand_seed);
        perc_tr = 1-(1/k_fold_cnt);
    end
    if k_fold_cnt==5 && rand_seed<0
        xv_file_name = [fold filesep 'rand_xv_cell_byuser.mat'];
    else
        xv_file_name = [fold filesep 'rand_xv_cell_rs' num2str(rand_seed) '_xv' num2str(k_fold_cnt) '.mat'];
    end
    if exist(xv_file_name, 'file')
        load(xv_file_name,'rand_xv_cell');
    else
        dispstat('','init');
        for test_user=test_user_list
            other_users_list = test_user_list(test_user_list~=test_user);
            dispstat(['test_user = ' num2str(test_user)]);
            for cv = cv_list
                rand_xv_cell{row_id,1} = test_user; 
                rand_xv_cell{row_id,2} = cv; 
                %rand_xv_cell{row_id,3};
                ass = [true true true];
                for khs_id = 2:size(cell_cnt_per_user,1)
                    khs_name = cell_cnt_per_user{khs_id,2};
                    sample_cnts = [cell_cnt_per_user{khs_id,3:end}];
                    dispstat(['test_user(' num2str(test_user) '), cv_id(', num2str(cv) ,'), khs_id(', num2str(khs_id-1) ,'), khs_name(', khs_name ,'), sample_cnts(', num2str(sample_cnts) ,')']);
                    %assign test user samples from test_user_id
                    te_row_ids = get_row_ids(list_dict, khs_name, test_user, 1);
                    other_row_ids = get_row_ids(list_dict, khs_name, test_user, 0);
                    %assertions
                    ass(1) = cell_cnt_per_user{khs_id,test_user+1}==length(te_row_ids);  
                    if rand_seed>0
                        ass(2) = sum([cell_cnt_per_user{khs_id,other_users_list+1}])==length(other_row_ids);
                        %now divide according to "k_fold_cnt"
                        scnt =length(other_row_ids);
                        permuted_ids = randperm(scnt);
                        tr_cnt = floor(scnt*perc_tr);
                        va_cnt = scnt-tr_cnt;
                        tr_row_ids = other_row_ids(permuted_ids(1:tr_cnt));
                        va_row_ids = other_row_ids(permuted_ids(1+tr_cnt:end));
                        ass(3) = length(va_row_ids)==va_cnt;
                    elseif k_fold_cnt==5 && rand_seed<0
                        va_user_id = other_users_list(cv);
                        va_row_ids = get_row_ids(list_dict, khs_name, va_user_id, 1);
                        [tf,~] = ismember(other_row_ids,va_row_ids);
                        tr_row_ids = other_row_ids(~tf);
                        ass(3) = (length(va_row_ids)+length(tr_row_ids))==length(other_row_ids);
                    end
                    rand_xv_cell{row_id,3} = [rand_xv_cell{row_id,3} reshape(tr_row_ids,1,[])];
                    rand_xv_cell{row_id,4} = [rand_xv_cell{row_id,4} reshape(va_row_ids,1,[])];
                    rand_xv_cell{row_id,5} = [rand_xv_cell{row_id,5} reshape(te_row_ids,1,[])];
                    assert(sum(ass)==3,'sth wrong')
                end
                row_id = row_id + 1;
            end
        end
        dispstat(newline,'keepthis');
        save(xv_file_name,'rand_xv_cell');
    end
end

function row_ids = get_row_ids(list_dict, khs_name, user_id, is1_not0)
    khs_row_ids = cell2mat(cellfun(@(x) strcmp(strtrim(x),khs_name), list_dict{6},'UniformOutput', false));
    %khs_row_cnt = sum(khs_row_ids);
    if is1_not0==1
        %user_id equals
        row_ids = list_dict{2}==user_id & khs_row_ids;
    else
        %user_id NOT equals
        row_ids = list_dict{2}~=user_id & khs_row_ids;
    end
    row_ids = find(row_ids);
end