function cropFolderExist = cropVidFrames(s,u,r,optParamStruct)
%   srcFold_cropVidFrames
%
%   framesOpts_cropVidFrames = getStructField(optParamStruct, 'framesOpts', struct('bigVideoShow',false,'bigVideoSaveScale',0.25,'show',false,'save',true));
%   optParamStruct_cropVidFrames = struct('srcFold',getVariableByComputerName('srcFold'),'framesOpts',framesOpts_cropVidFrames,'enforceReCreateNew', false);
%   cropFolderExist = cropVidFrames(optParamStruct)

%   cropVidFrames('/mnt/Data/FromUbuntu/HospiSign/HospiMOV/296/User_2_1')
%   cropVidFrames('D:\FromUbuntu\Doga\112\User_2_2')
    %folderName = 'D:\FromUbuntu\Doga\112\User_2_2';
    
    
%     optParamStruct_cropVidFrames = struct('srcFold',getVariableByComputerName('srcFold'),...
%                                           'saveToFoldRoot','D:\alptekine',...
%                                           'makeZippedFolder',false,...
%                                           'removeUnzippedFolder',false,...
%                                           'mirrorOnSaveStruct',struct('mirrorLH',false,'mirrorRH',false,'mirrorBH',false),...
%                                           'multipliers',struct('bigHandFaceRatio',[1 1],'smlHandFaceRatio',[2/3,2/3]),...
%                                           'enforceReCreateNew', false);
    if ~exist('optParamStruct','var')
        optParamStruct = [];
    end
    srcFold = getStructField(optParamStruct, 'srcFold', getVariableByComputerName('srcFold'));
    framesOpts = getStructField(optParamStruct, 'framesOpts', struct('bigVideoShow',false,'bigVideoSaveScale',0.25,'show',false,'save',true));
    enforceReCreateNew = getStructField(optParamStruct, 'enforceReCreateNew', false);
    saveToFoldRoot = getStructField(optParamStruct, 'saveToFoldRoot', getVariableByComputerName('srcFold'));
    multipliers = getStructField(optParamStruct, 'multipliers', struct('bigHandFaceRatio',[1,1],'smlHandFaceRatio',[2/3,2/3]));
    makeZippedFolder = getStructField(optParamStruct, 'makeZippedFolder', true);
    removeUnzippedFolder = getStructField(optParamStruct, 'removeUnzippedFolder', true);
    resizeImVec = getStructField(optParamStruct, 'resizeImVec', [200 200]);
    mirrorOnSaveStruct = getStructField(optParamStruct, 'mirrorOnSaveStruct', struct('mirrorLH',false,'mirrorRH',false,'mirrorBH',false));
    
    
    if ~isnumeric(s)
        error(['s(' s ') should be numeric. call cropVidFrames(s,u,r)'])
    end
    
    userVideoFolder = [srcFold filesep num2str(s) filesep 'User_' num2str(u) '_' num2str(r)];
    saveToUserFolder = [saveToFoldRoot filesep num2str(s) filesep 'User_' num2str(u) '_' num2str(r)];
    
    %now I need to know if the cropped folder will be created at the source
    %place or not. because the function would behave differently 
    saveFolderIsDifferent = strcmp(srcFold,saveToFoldRoot)==0;
    
    %cropFolder must be at saveFolder place
    cropFolderExist = (exist([saveToUserFolder filesep 'cropped'],'dir')==7);
    saveToCroppedFolder = [saveToUserFolder filesep 'cropped'];
    
    
    croppedPngZipFileName = [saveToUserFolder filesep 'cropped.zip'];
    %check if <zipExist & folderNotExist & noNeedToCreateNew> -> just unzip
    %only if saveFolderIsDifferent
    if (~saveFolderIsDifferent && exist(croppedPngZipFileName,'file') && ~cropFolderExist && ~enforceReCreateNew)
        frameCnt = unzipProperly(saveToUserFolder,'cropped');
        disp([num2str(frameCnt) ' frames extracted from ' croppedPngZipFileName]);       
    end
    % <zipExist or cropFolderExist> -> just return
    if saveFolderIsDifferent==false && (exist(croppedPngZipFileName,'file') || cropFolderExist) && enforceReCreateNew==false %exist(cropAreaFileName,'file') && exist(handUsageFileName,'file') && 
        disp(['skipping cropVidFrames(' userVideoFolder '); because all the files to be exported are already exported']);
        cropFolderExist = (exist(saveToCroppedFolder,'dir')==7);
        return
    elseif exist(saveToUserFolder,'dir') && ~enforceReCreateNew
        disp(['skipping cropVidFrames(' saveToUserFolder '); because all the files to be exported are already exported']);
        cropFolderExist = (exist(saveToCroppedFolder,'dir')==7);
        return
    elseif exist(saveToUserFolder,'dir') && enforceReCreateNew
        try
            disp(['removing cropVidFrames(' userVideoFolder '); because it exists and enforceReCreateNew==ture']);
            rmdir(saveToCroppedFolder,'s');
        catch
        end
    end
 
    videoFileNameFull = [userVideoFolder filesep 'color.mp4'];
    cropAreaFileName = [userVideoFolder filesep 'cropArea.mat'];
    handUsageFileName = [userVideoFolder filesep 'handUsage.mat'];
    if ~exist(videoFileNameFull,'file')
        error(['video file (' videoFileNameFull ') must exist.'])
    end
    if ~exist(cropAreaFileName,'file')
        error(['cropArea file (' cropAreaFileName ') must exist.'])
    end
    if ~exist(handUsageFileName,'file')
        error(['handUsage file (' handUsageFileName ') must exist.'])
    end
    
    
    v = VideoReader(videoFileNameFull);
    vidFrameMat4d = read(v,[1 Inf]); %#ok<VIDREAD>
    clear v
    load([userVideoFolder filesep 'skeleton.mat'], 'skeleton');
    
    saveToFolder_cropped = [saveToCroppedFolder filesep ];
    if ~exist(saveToFolder_cropped,'dir')
        mkdir(saveToFolder_cropped);
    end
    
    if framesOpts.bigVideoSaveScale>0
        v = VideoWriter([[saveToUserFolder filesep] 'markedVideo.avi']);
        v.FrameRate=10;
        open(v)
    elseif framesOpts.show
        figure(2);
    end
    
    frCnt = size(vidFrameMat4d,4);
    warning('off','images:initSize:adjustingMag');
    %iptsetpref('ImshowBorder','tight');
    %set(gca,'LooseInset',get(gca,'TightInset'));
    
    % cropArea = struct;% 1_from_row_y, 2_from_col_x, 3_height_row_y, 4_width_col_x
    % cropArea.{LH_big,RH_big,BH_big,FC_big,LH_sml,RH_sml,BH_sml} = zeros(frCnt,4);
    
    optParamStruct_getGenerateCropArea = struct('enforceReCreateNew', enforceReCreateNew,'colDepStr', 'color', 'multipliers', multipliers);
    cropArea = getGenerateCropArea(cropAreaFileName, skeleton, optParamStruct_getGenerateCropArea);
    
    handDisplacementMat = getHandDisplacementVec(skeleton, cropArea);
    save(handUsageFileName, 'handDisplacementMat');
    %no movement would map to height-(50+10) (1020)
    %max movement would map to 10 (10)
    startColorBoxPos_Row_Y = 10;
    
    %positionTitleInImage = [10 10];
    
    rhHeightToDraw = map2_a_b( handDisplacementMat.RH, 1020, startColorBoxPos_Row_Y );    
    lhHeightToDraw = map2_a_b( handDisplacementMat.LH, 1020, startColorBoxPos_Row_Y );    
    bhWidthToDraw  = map2_a_b( handDisplacementMat.BH, 480, 1440 );    
    
    for i=1:frCnt
        %disp([num2str(i) ' of ' num2str(frCnt) ' completed..'])
        curFrame = squeeze(vidFrameMat4d(:,:,:,i));       
        %intersectPercent = cropbBoxCollide(cropArea.RH_sml(i,:), cropArea.LH_sml(i,:));
        %handsAreTogether(i) = intersectPercent>0;
        if framesOpts.bigVideoSaveScale>0 || framesOpts.bigVideoShow
            % curFrame
            bigVideoFrame = curFrame;
            %torso
            bigVideoFrame = drawRectOnImg(bigVideoFrame, cropArea.TS_big(i,1:2), cropArea.TS_big(i,3:4), [1 1 0], 20);
            %face
            bigVideoFrame = drawRectOnImg(bigVideoFrame, cropArea.FC_big(i,1:2), cropArea.FC_big(i,3:4), [1 1 1], 10);         
            %both hands big
            bigVideoFrame = drawRectOnImg(bigVideoFrame, cropArea.BH_big(i,1:2), cropArea.BH_big(i,3:4), [1 0 0], 15);
            %left hand big
            bigVideoFrame = drawRectOnImg(bigVideoFrame, cropArea.LH_big(i,1:2), cropArea.LH_big(i,3:4), [0 0 0], 10);
            %right hand big
            bigVideoFrame = drawRectOnImg(bigVideoFrame, cropArea.RH_big(i,1:2), cropArea.RH_big(i,3:4), [0 1 0], 10);
            
            %both hands small
            bigVideoFrame = drawRectOnImg(bigVideoFrame, cropArea.BH_sml(i,1:2), cropArea.BH_sml(i,3:4), [1 0 1], 15);
            %left hand small
            bigVideoFrame = drawRectOnImg(bigVideoFrame, cropArea.LH_sml(i,1:2), cropArea.LH_sml(i,3:4), [0 0 1], 10);
            %right hand small
            bigVideoFrame = drawRectOnImg(bigVideoFrame, cropArea.RH_sml(i,1:2), cropArea.RH_sml(i,3:4), [0 1 1], 10);
            
            %right hand move
            if handDisplacementMat.RH(i)>0
                bigVideoFrame = drawRectOnImg(bigVideoFrame, [10,rhHeightToDraw(i)], [50 50], [0 1 1], 20);
            else
                bigVideoFrame = drawRectOnImg(bigVideoFrame, [10,rhHeightToDraw(i)], [50 50], [0 0 0], 20);                
            end            
            %both hands move
            if handDisplacementMat.BH(i)>0
                bigVideoFrame = drawRectOnImg(bigVideoFrame, [bhWidthToDraw(i),startColorBoxPos_Row_Y], [50 50], [1 0 1], 20);
            else
                bigVideoFrame = drawRectOnImg(bigVideoFrame, [bhWidthToDraw(i),startColorBoxPos_Row_Y], [50 50], [0 0 0], 20);                
            end            
            %both hands together
            if handDisplacementMat.handsAreTogether(i)>0
                bigVideoFrame = drawRectOnImg(bigVideoFrame, [935,61+startColorBoxPos_Row_Y], [50 50], [1 1 1], 20);
            else
                bigVideoFrame = drawRectOnImg(bigVideoFrame, [935,61+startColorBoxPos_Row_Y], [50 50], [0 0 0], 20);                
            end            
            %left hand move
            if handDisplacementMat.LH(i)>0
                bigVideoFrame = drawRectOnImg(bigVideoFrame, [1860,lhHeightToDraw(i)], [50 50], [0 0 1], 20);
            else
                bigVideoFrame = drawRectOnImg(bigVideoFrame, [1860,lhHeightToDraw(i)], [50 50], [0 0 0], 20);                
            end            
        end
        if framesOpts.bigVideoShow
            figure(1);
            titleStr = ['FrameID(' num2str(1) ' of ' num2str(frCnt) '),RightHandMove(' num2str(handDisplacementMat.RH(i)>0) '),LeftHandMove(' num2str(handDisplacementMat.LH(i)>0) '),BothHandMove(' num2str(handDisplacementMat.BH(i)>0) '),BothHandTogether(' num2str(handDisplacementMat.handsAreTogether(i)>0) ')'];
            %bigVideoFrame = insertText(bigVideoFrame,positionTitleInImage,titleStr,'FontSize',18,'BoxColor', 'yellow','BoxOpacity',0.75,'TextColor','black');
            image(bigVideoFrame);
            title(titleStr);
            drawnow;
            %F = getframe(gcf);
            %writeVideo(v,bigVideoFrame);
        end
        if framesOpts.bigVideoSaveScale>0
            bigVideoFrameRes = imresize(bigVideoFrame, framesOpts.bigVideoSaveScale);
            writeVideo(v,bigVideoFrameRes);
        end
        
        showSaveCroppedIm(curFrame, saveToFolder_cropped, i, cropArea.LH_big(i,1:2), cropArea.LH_big(i,3:4), 'LH', framesOpts, '', resizeImVec, mirrorOnSaveStruct);
        showSaveCroppedIm(curFrame, saveToFolder_cropped, i, cropArea.RH_big(i,1:2), cropArea.RH_big(i,3:4), 'RH', framesOpts, '', resizeImVec, mirrorOnSaveStruct);
        showSaveCroppedIm(curFrame, saveToFolder_cropped, i, cropArea.BH_big(i,1:2), cropArea.BH_big(i,3:4), 'BH', framesOpts, '', resizeImVec, mirrorOnSaveStruct);
        %showSaveCroppedIm(curFrame, saveToFolder, i, cropArea.FC_big(i,1:2), cropArea.FC_big(i,3:4), 'FC', framesOpts);
    end
    disp([num2str(frCnt) ' of ' num2str(frCnt) ' completed..'])
    if framesOpts.bigVideoSaveScale>0
        close(v);
    end
    warning('on','images:initSize:adjustingMag');
    
    if framesOpts.save
        %now compress the folder into zip
        if (makeZippedFolder)
            zip([saveToUserFolder filesep 'cropped.zip'],saveToCroppedFolder);
        end
        if removeUnzippedFolder
            rmdir(saveToCroppedFolder,'s');
        end
    end
    cropFolderExist = (exist(saveToCroppedFolder,'dir')==7);
end

function showSaveCroppedIm(curFrame, saveToFolder, frameID, skelInds, frameRowColLen, fileNameAbbr, framesOpts, titleStr, resizeImVec, mirrorOnSaveStruct)

    if ~exist('titleAddStr','var')
        titleStr = '';
    end
    if ~exist('resizeImVec','var')
        resizeImVec = [200 200];
    end
    if ~exist('mirrorOnSaveStruct','var')
        mirrorOnSaveStruct = struct('mirrorLH',true,'mirrorRH',false,'mirrorBH',false); %#ok<NASGU>
    end
    
    cropedIm = cropBodyPart(curFrame, skelInds, frameRowColLen);
    cropedIm = imresize(cropedIm,resizeImVec);
        
    if framesOpts.save
        mirrorBeforeSave = false;
        try %#ok<TRYNC>
            eval(['mirrorBeforeSave = mirrorOnSaveStruct.mirror' fileNameAbbr ';']);
        end
        if mirrorBeforeSave
            cropedIm = fliplr(cropedIm); %#ok<UNRCH>
        end
        imwrite(cropedIm, [saveToFolder 'crop_' fileNameAbbr '_' num2str(frameID,'%03d') '.png']);
    end
    if framesOpts.show
        imshow(cropedIm);
        title(titleStr);
        drawnow; 
    end
end
