function h = displayClusterStructInfo(clustersStruct, clusterIDsToView, figID)
%displayClusterStructInfo will draw a summary of the clusters already
%assigned
    curClusterNamesCell = clustersStruct.definition.clusterNames;
    curClusterCnt = length(curClusterNamesCell);
    
    if exist('clusterIDsToView','var') && ~isempty(clusterIDsToView) && length(clusterIDsToView)<=curClusterCnt
        curClusterCnt = length(clusterIDsToView);
    else
        clusterIDsToView = 1:curClusterCnt;
    end
    
    curClusterNamesCell = clustersStruct.definition.clusterNames(clusterIDsToView);
    curClusterImages = clustersStruct.definition.clusterImage(clusterIDsToView);
    
%    if curClusterCnt<10
        %use clustersStruct.definition.clusterImage as sub figures
        [rowCnt, colCnt] = calculateProperRowColForSubPlot(curClusterCnt);
        h = figure(figID);clf;
        for i=1:curClusterCnt
            subplot(rowCnt, colCnt, i);
            image(curClusterImages{i});
            titleStr = [num2str(clusterIDsToView(i)) '-' curClusterNamesCell{i}];
            title(titleStr);
        end
%    elseif curClusterCnt<25
%    else
%    end   
end

function [rc, cc] = calculateProperRowColForSubPlot(imageCount)
    rowCnt = floor(sqrt(imageCount));
    colCnt = ceil(imageCount/rowCnt);
    rc = max(rowCnt,colCnt);
    cc = min(rowCnt,colCnt);
end

