function h = displayCurrentClusters(curClusters, figID)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    numOfKnownClusters = size(curClusters, 2);
    
    if numOfKnownClusters<10
        rc = numOfKnownClusters;
        cc = 1;
        floatDispStr = '%8.6f';
    else
        rc = ceil(sqrt(numOfKnownClusters));
        cc = ceil(numOfKnownClusters/rc);
        floatDispStr = '%5.3f';
    end
    h = figure(figID);
    for i=1:numOfKnownClusters
        curClus = curClusters{1,i};
        subplot(rc,cc,i);
        
        smallImg = curClus.bestSelectedImagesToView;
        im2disp = prepareImageToShow(smallImg);
        image(im2disp);
        
        if cc<3
            titleStr = [num2str(i) '-clusterName(' strrep(curClus.clusterName,'_',' ') ')'];
            titleStr = [titleStr ', matchVal(' num2str(curClus.minMatchValue,floatDispStr) '<' num2str(curClus.minMatchValue,floatDispStr) ')']; %#ok<*AGROW>
            titleStr = [titleStr ', uniqueClus' mat2str(curClus.differentSignIDs)];
        else
            titleStr = [num2str(i) '-cn(' strrep(curClus.clusterName,'_',' ') ')'];
            titleStr = [titleStr ', uniCluCnt' length(curClus.differentSignIDs)];
        end
        title(titleStr);
    end
end

function im2disp = prepareImageToShow(smallImg)
    
    [rowSmall,colSmall,channelSmall] = size(smallImg);
    
    colBig = 5*rowSmall;
    colDiff = colBig - colSmall;
    
    if colDiff<rowSmall
        im2disp = smallImg;
        return;
    end
    
    im2disp = zeros([rowSmall,colBig,channelSmall],'uint8');
    
    colInit = max(1,floor(colDiff/2));
    colEnd = colInit + colSmall - 1;
    
    im2disp(:,colInit:colEnd,:) = smallImg;
end