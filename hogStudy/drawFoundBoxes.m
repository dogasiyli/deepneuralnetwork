x_lh_084 = imread('/media/doga/Data/FromUbuntu/Doga/112/User_2_2/cropped/crop_LH_084.png');
y_lh_063 = imread('/media/doga/Data/FromUbuntu/Doga/112/User_5_1/cropped/crop_LH_063.png');
x_rh_044 = imread('/media/doga/Data/FromUbuntu/Doga/112/User_2_2/cropped/crop_RH_044.png');
y_rh_033 = imread('/media/doga/Data/FromUbuntu/Doga/112/User_5_1/cropped/crop_RH_033.png');

X_IM = x_lh_084;
Y_IM = y_lh_063;

scales2Use = 1:-0.05:0.70;
strideFixed = 5;

handInfo = struct;
handInfo.handDominant = 'LH';
handInfo.handCur = 'LH';

tic;
[hogImArr_Y, imcropVec_Y] = extrapolateImForHOGComp(Y_IM, scales2Use, strideFixed, [], handInfo);
toc;
[hogImArr_X, imcropVec_X] = extrapolateImForHOGComp(X_IM, scales2Use, strideFixed, [], handInfo);
toc;
D = pdist2_fast( hogImArr_X', hogImArr_Y');
toc;

[distVals, distInds] = sort(D(:));

boxCount = 5;
maxThick = 10;
minThick = 1;
maxCurve = 0.5;
minCurve = 0.0;

[rxi, cyj] = ind2sub(size(D),distInds(1:boxCount));

x_boxes = imcropVec_X(rxi,:);
y_boxes = imcropVec_Y(cyj,:);

figure(1);clf;
subplot(2,1,1);
image(X_IM);
subplot(2,1,2);
image(Y_IM);



[lineWidths, sumStrThck] = map2_a_b( 1:boxCount, minThick, maxThick );
[curvVals, sumStrCrv] = map2_a_b( 1:boxCount, minCurve, maxCurve );

outMatchBoxSize = [90,90,3];
outMatchImage = zeros([outMatchBoxSize(1)*2,outMatchBoxSize(1)*boxCount,3], 'uint8');

for boxID=1:boxCount
    boxColor = rand(1,3);
    
    curLineWidth = lineWidths(boxCount-boxID+1);
    curCurve = curvVals(boxID);
    
    figure(1);
    subplot(2,1,1);
    hold on;
    rectangle('Position', ...
              [x_boxes(boxID,3) x_boxes(boxID,1) (x_boxes(boxID,4)-x_boxes(boxID,3))  (x_boxes(boxID,2)-x_boxes(boxID,1))], ...
              'EdgeColor', boxColor ...
              ,'Curvature',curCurve ...
              ,'LineWidth',curLineWidth);
    title(['boxID(' num2str(boxID) ')']);
    subplot(2,1,2);
    hold on;
    rectangle('Position', ...
              [y_boxes(boxID,3) y_boxes(boxID,1) (y_boxes(boxID,4)-y_boxes(boxID,3))  (y_boxes(boxID,2)-y_boxes(boxID,1))], ...
              'EdgeColor', boxColor ...
              ,'Curvature',curCurve ...
              ,'LineWidth',curLineWidth);
    title(['boxID(' num2str(boxID) ')']);
    
    colSt = (boxID-1)*outMatchBoxSize(2) + 1;
    colEn = boxID*outMatchBoxSize(2);
    x_cr = X_IM(x_boxes(boxID,1):x_boxes(boxID,2),x_boxes(boxID,3):x_boxes(boxID,4),:);
    x_cr = imresize(x_cr,[outMatchBoxSize(1) outMatchBoxSize(2)]);
    outMatchImage(1:outMatchBoxSize(1),colSt:colEn,:) = x_cr;

    y_cr = Y_IM(y_boxes(boxID,1):y_boxes(boxID,2),y_boxes(boxID,3):y_boxes(boxID,4),:);
    y_cr = imresize(y_cr,[outMatchBoxSize(1) outMatchBoxSize(2)]);
    outMatchImage(1+outMatchBoxSize(1):end,colSt:colEn,:) = y_cr;
    
    figure(2);
    if boxID==1
        subplot(2,2,1);
        imshow(x_cr);
        subplot(2,2,2);
        imshow(y_cr);
    end
    subplot(2,2,3:4);
    imshow(outMatchImage(:,1:colEn,:));
    title(['Original match(' num2str(D(1)) ') - Best matches from 1(' num2str(distVals(1)) ') to ' num2str(boxID) '(' num2str(distVals(boxID)) ')']);

    drawnow;
    %pause;
end