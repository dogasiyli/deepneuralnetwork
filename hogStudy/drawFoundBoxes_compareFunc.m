function [minDist_LL_RR_B_LR_RL, bestImCrop_LL_RR_B_LR_RL] = drawFoundBoxes_compareFunc(srcFold, signIDs, userIDs, signRepeatIDs, imIDXY, hogVersion)
%[minDistLR] = drawFoundBoxes_compareFunc([bigDataPath '/Doga'], [112 112], [2 5], [2 1])
    precalcedHOGImCropVecs = getPrecalcedHOGImCropVecs(srcFold,  signIDs, userIDs, signRepeatIDs, hogVersion);

    hogImArr_cell_X_left = precalcedHOGImCropVecs.hogImArr_cell_X{imIDXY(1),1};
    hogImArr_cell_X_right = precalcedHOGImCropVecs.hogImArr_cell_X{imIDXY(1),2};
    hogImArr_cell_X_both = precalcedHOGImCropVecs.hogImArr_cell_X{imIDXY(1),3};

    hogImArr_cell_Y_left = precalcedHOGImCropVecs.hogImArr_cell_Y{imIDXY(2),1};
    hogImArr_cell_Y_right = precalcedHOGImCropVecs.hogImArr_cell_Y{imIDXY(2),2};
    hogImArr_cell_Y_both = precalcedHOGImCropVecs.hogImArr_cell_Y{imIDXY(2),3};
    

    if hogVersion==1
        [minDistVal_Xlh_Ylh, minDistInds_Xlh_Ylh] = calcDistofHogs( hogImArr_cell_X_left , hogImArr_cell_Y_left , 1);
        [minDistVal_Xrh_Yrh, minDistInds_Xrh_Yrh] = calcDistofHogs( hogImArr_cell_X_right, hogImArr_cell_Y_right, 1);
        [minDistVal_BH, minDistInds_BH] = calcDistofHogs( hogImArr_cell_X_both, hogImArr_cell_Y_both, 1);
        [minDistVal_Xlh_Yrh, minDistInds_Xlh_Yrh] = calcDistofHogs( hogImArr_cell_X_left, hogImArr_cell_Y_right, 1);
        [minDistVal_Xrh_Ylh, minDistInds_Xrh_Ylh] = calcDistofHogs( hogImArr_cell_X_right, hogImArr_cell_Y_left, 1);
    else
        error('not implemented yet')
    end
    
    minDist_LL_RR_B_LR_RL = [minDistVal_Xlh_Ylh, minDistVal_Xrh_Yrh, minDistVal_BH, minDistVal_Xlh_Yrh, minDistVal_Xrh_Ylh];
    bestImCrop_LL_RR_B_LR_RL = [minDistInds_Xlh_Ylh, minDistInds_Xrh_Yrh, minDistInds_BH, minDistInds_Xlh_Yrh, minDistInds_Xrh_Ylh];
end