function exportDataForCpp( M, varName)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    [xl, yl] = size(M);
    
    if nargin<2
        varName = 'varName';
    end
    
    disp(['float ' varName '[' num2str(xl) '][' num2str(yl) '] =']);
    disp('{');
    for xi = 1:xl
        curLine = '{';    
        for yi = 1:yl
            numberStr = strrep(num2str(M(xi,yi),'%4.2f'), ',', '.');
            curLine = [curLine numberStr];
            if yi~=yl
                curLine = [curLine 'f, '];
            else
                curLine = [curLine 'f}'];
            end
        end
        if xi~=xl
            curLine = [curLine ','];
        end
       disp(curLine);
    end
    disp('};');
end

