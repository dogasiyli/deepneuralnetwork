%Image descriptor based on Histogram of Orientated Gradients for gray-level images. This code 
%was developed for the work: O. Ludwig, D. Delgado, V. Goncalves, and U. Nunes, 'Trainable 
%Classifier-Fusion Schemes: An Application To Pedestrian Detection,' In: 12th International IEEE 
%Conference On Intelligent Transportation Systems, 2009, St. Louis, 2009. V. 1. P. 432-437. In 
%case of publication with this code, please cite the paper above.

function hogIm = exractHOG(grayIm)

    imSizeCur = size(grayIm);
    if length(imSizeCur)==3 && imSizeCur(3)==3
        grayIm = rgb2gray(grayIm);
    end

    nwin_x=3;%set here the number of HOG windows per bound box
    nwin_y=3;
    B=9;%set here the number of histogram bins
    [L,C]=size(grayIm); % L num of lines ; C num of columns
    hogIm = zeros(nwin_x*nwin_y*B,1); % column vector with zeros
    m=sqrt(L/2);
    if C==1 % if num of columns==1
        grayIm=im_recover(grayIm,m,2*m);%verify the size of image, e.g. 25x50
        L=2*m;
        C=m;
    end
    grayIm=double(grayIm);
    step_x=floor(C/(nwin_x+1));
    step_y=floor(L/(nwin_y+1));
    cont=0;
    hx = [-1,0,1];
    hy = -hx';
    grad_xr = imfilter(double(grayIm),hx);
    grad_yu = imfilter(double(grayIm),hy);
    angles=atan2(grad_yu,grad_xr);
    magnit=((grad_yu.^2)+(grad_xr.^2)).^.5;
    for n=0:nwin_y-1
        for m=0:nwin_x-1
            cont=cont+1;
            nFrTo = [n*step_y+1, (n+2)*step_y];
            mFrTo = [m*step_x+1,(m+2)*step_x];
            angles2=angles(nFrTo(1):nFrTo(2),mFrTo(1):mFrTo(2)); 
            magnit2=magnit(nFrTo(1):nFrTo(2),mFrTo(1):mFrTo(2));
            v_angles=angles2(:);    
            v_magnit=magnit2(:);
            %assembling the histogram with 9 bins (range of 20 degrees per bin)
            H2 = calcHistVecOfBlock(B, v_angles, v_magnit);
            hogIm((cont-1)*B+1:cont*B,1)=H2;
        end
    end
end

%deletedCode-01
% K=max(size(v_angles));
% bin=0;
% H2=zeros(B,1);
% for ang_lim=-pi+2*pi/B:2*pi/B:pi
%     bin=bin+1;
%     for k=1:K
%         if v_angles(k)<ang_lim
%             v_angles(k)=100;
%             H2(bin)=H2(bin)+v_magnit(k);
%         end
%     end
% end
% 
% H2=H2/(norm(H2)+0.01); 