%Image descriptor based on Histogram of Orientated Gradients for gray-level images. This code 
%was developed for the work: O. Ludwig, D. Delgado, V. Goncalves, and U. Nunes, 'Trainable 
%Classifier-Fusion Schemes: An Application To Pedestrian Detection,' In: 12th International IEEE 
%Conference On Intelligent Transportation Systems, 2009, St. Louis, 2009. V. 1. P. 432-437. In 
%case of publication with this code, please cite the paper above.

function [hogVec, visualHogImage, combinedImageGray, rgbIm] = exractHOG_dg(grayIm, hogStruct, plotType)

    if ~exist('hogStruct','var')
        hogStruct = struct('blockCounts',[3 3],'histogramBinCount',9);
    end
    
    %% 2. Verify the input is a gray scale image
    [grayIm, rgbIm] = verifyGrayImage(grayIm);
    [grayIm, imRowCnt,imColCnt] = enhanceGrayImg(grayIm);
    
    %% 3. 
    blockCnt = getStructField(hogStruct,'blockCounts',[3 3]);%set here the number of HOG windows per bound box-[nwin_x nwin_y]
    blockCnt_ColsX = blockCnt(1);
    blockCnt_RowsY = blockCnt(2);
    binCnt = getStructField(hogStruct,'histogramBinCount',9);%set here the number of histogram bins
    
    %% 5.
    hogVec = zeros(blockCnt_ColsX*blockCnt_RowsY*binCnt,1); % column vector with zeros
    [angles, magnit] = exportGradientOfImg(grayIm);
    
    %% 6.
    [blockBoundsMat, blockCnt, hogVecBounds] = getBlockBounds_forHOG(imColCnt, imRowCnt, blockCnt_ColsX, blockCnt_RowsY, binCnt);
    histNorms = zeros(1,blockCnt);
    for i = 1:blockCnt
        angles_curBlock = angles(blockBoundsMat(i,1):blockBoundsMat(i,2),blockBoundsMat(i,3):blockBoundsMat(i,4)); 
        magnit_curBlock = magnit(blockBoundsMat(i,1):blockBoundsMat(i,2),blockBoundsMat(i,3):blockBoundsMat(i,4));
        %assembling the histogram with <B> bins (range of <180/B> degrees per bin)
        [histVec, normOfHist] = calcHistVecOfBlock(binCnt, angles_curBlock(:), magnit_curBlock(:));
        hogVec(hogVecBounds(i,1):hogVecBounds(i,2)) = histVec;
        histNorms(i) = normOfHist;
    end
    histNorms = 1-(histNorms./max(histNorms));
    
    if nargout>1
        if ~exist('plotType','var')
            plotType = 'none';%'eachSteps','blockSteps','final'
        end
        [visualHogImage, combinedImageGray] = visualizeHOG(grayIm, hogVec, blockBoundsMat, hogVecBounds, plotType, histNorms);
        if ~isempty(rgbIm)
            for rgb=1:3
                curLayer = rgbIm(:,:,rgb);
                if rgb==1
                    curLayer(visualHogImage==1) = max(curLayer(:));
                else
                    curLayer(visualHogImage==1) = 0;
                end
                rgbIm(:,:,rgb) = curLayer;
            end
        end
    end
end

function [grayIm, rgbIm] = verifyGrayImage(imIn)
    imSizeCur = size(imIn);
    rgbIm = [];
    if length(imSizeCur)==3 && imSizeCur(3)==3
        grayIm = rgb2gray(imIn);
        rgbIm = imIn;
    else
        grayIm = imIn;
    end
end
function [grayIm, imRowCnt,imColCnt] = enhanceGrayImg(grayIm)
    [imRowCnt,imColCnt]=size(grayIm);
    m=sqrt(imRowCnt/2);
    if imColCnt==1 % if num of columns==1
        grayIm=im_recover(grayIm,m,2*m);%verify the size of image, e.g. 25x50
        imRowCnt=2*m;
        imColCnt=m;
    end
    grayIm=double(grayIm);
end
function [angles, magnit] = exportGradientOfImg(grayIm)
    hx = [-1,0,+1];
    hy = [+1;0;-1];%-hx';
    grad_xr = imfilter(double(grayIm),hx);
    grad_yu = imfilter(double(grayIm),hy);
    angles = atan2(grad_yu,grad_xr);
    magnit = ((grad_yu.^2)+(grad_xr.^2)).^.5;
end