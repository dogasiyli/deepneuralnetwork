function [hogImArr_cell, imcropVec_cell] = extractHog_v2(croppedHandsFolderName, imIDBegEnd, inputImSize, handInfo, blockCountsVec, hogStruct)
    cellCnt = imIDBegEnd(2)-imIDBegEnd(1)+1;
    hogImArr_cell = cell(cellCnt,3);%1-Left, 2-Right, 3-Both Hands
    reverseStr = '';
    for i = 1:cellCnt
        msg = sprintf('underFolder(%s) - frame %d of %d is being analyzed..', croppedHandsFolderName,i, cellCnt);
        fprintf([reverseStr, msg]);
        reverseStr = repmat(sprintf('\b'), 1, length(msg));
        %disp(['underFolder(' croppedHandsFolderName ') - f' num2str(i) ' of ' num2str(cellCnt) ' is being analyzed..']);
        imID = i + imIDBegEnd(1) - 1;
        im_lh = imread([croppedHandsFolderName 'crop_LH_' num2str(imID,'%03d') '.png']);
        im_rh = imread([croppedHandsFolderName 'crop_RH_' num2str(imID,'%03d') '.png']);
        im_bh = imread([croppedHandsFolderName 'crop_BH_' num2str(imID,'%03d') '.png']);
        
        im_lh = imresize(im_lh, inputImSize);
        im_rh = imresize(im_rh, inputImSize);
        im_bh = imresize(im_bh, inputImSize);
        
        handInfo.handCur = 'LH';
        hogImArr_lh = extrapolateImForHOGComp_v2(im_lh, blockCountsVec, hogStruct, handInfo);
        handInfo.handCur = 'RH';
        hogImArr_rh = extrapolateImForHOGComp_v2(im_rh, blockCountsVec, hogStruct, handInfo);
        handInfo.handCur = 'BH';
        hogImArr_bh = extrapolateImForHOGComp_v2(im_bh, blockCountsVec, hogStruct, handInfo);
        hogImArr_cell{i,1} = hogImArr_lh;
        hogImArr_cell{i,2} = hogImArr_rh;
        hogImArr_cell{i,3} = hogImArr_bh;
    end
    imcropVec_cell = struct;
    imcropVec_cell.blockCountsVec = blockCountsVec;
    imcropVec_cell.hogStruct = hogStruct;
    disp('Done');
end