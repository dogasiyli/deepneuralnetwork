function [extrapolatedHog, rowColMap] = extrapolateHogV2(hogIn, imcropVec_struct, stride)
    baseBlockCnt = imcropVec_struct.blockCountsVec(1);
    %stride = 1;
    extrapolatedHog = [];
    rowColMap = [];
    binCount = size(hogIn,2);
    blockSize = [baseBlockCnt baseBlockCnt];
    rotDegrees = imcropVec_struct.rotDegrees;
    for i=1:length(imcropVec_struct.blockCountsVec)
        for r = 1:length(rotDegrees)
            rotDeg = rotDegrees(r);        
            blockAreaCur = [imcropVec_struct.blockCountsVec(i) imcropVec_struct.blockCountsVec(i)];
            blockCountCur = prod(blockAreaCur);
            part_hog_1 = hogIn(1:blockCountCur,:);
            hogV2MapStruct = createOrLoadHogV2DifMap(blockAreaCur, blockSize, stride, binCount);

            h1 = part_hog_1';
            h1 = h1(:);
            h1 = h1(hogV2MapStruct.hogPixInputSingle);
            h1 = reshape(h1, size(hogV2MapStruct.hogPixInputSingle));

            extrapolatedHog = [extrapolatedHog; h1];
            hogIn = hogIn(blockCountCur+1:end,:);
            if nargout==1
                continue
            end
            rowColMapAdd = [hogV2MapStruct.rowColMap, rotDeg*ones(size(hogV2MapStruct.rowColMap,1),1)];
            rowColMap = [rowColMap; rowColMapAdd];
        end
    end
end