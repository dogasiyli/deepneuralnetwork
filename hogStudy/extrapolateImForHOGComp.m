function [ hogImArr, imCropVec, subImages] = extrapolateImForHOGComp( x_In, scales2Use, strideFixed, hogStruct, handInfo)
%extrapolateImForHOGComp generate a lot of sub Images for HOG comparison
%   I have an image with some size,
%   I am looking for a sub part of image that would be the actual part I am
%   seeking for.. Hence I have to look at it wit different surrounding
%   boxes and find the best match between 2 mother images
%   That would require another function to compare these sorted subImages
%   and return which 2 best matched

%   The cropping of the image would have different scales(1.0:-0.05:0.50)
    %scales2Use = 1.0 : -0.05 : 0.50 ;
    imSizeCur = size(x_In);    
    [imCntSizPerScaleVec, imCropVec] = preCalcCropAreas(imSizeCur, scales2Use, strideFixed);
    if length(imSizeCur)==3 && imSizeCur(3)==3
        x_Gray = rgb2gray(x_In);
    end
    
    x_Gray = flipForHOG( x_Gray, handInfo);
    
    imCnt = sum(imCntSizPerScaleVec(:,1));
    assert(imCnt==size(imCropVec,1),['These<imCnt(' num2str(imCnt) ')-(' num2str(size(imCropVec,1))  ')imCropVecLen> must match']);
    hogImArr = [];
    for i=1:imCnt
        x_crop = x_Gray(imCropVec(i,1):imCropVec(i,2),imCropVec(i,3):imCropVec(i,4));
        hogIm = exractHOG_dg(x_crop, hogStruct);
        if isempty(hogImArr)
            hogImArr = zeros(size(hogIm,1), imCnt);
        end
        hogImArr(:,i) = hogIm; %#ok<AGROW>
    end
    
    if nargout>2
        %create a summary that has all the cropped versions of the source
        %image
        %each line on imCntSizPerScaleVec is a subplot/subImage
        %the sqrt of number of elements will define the blocks of the image
        %to be created
        subImageCnt = size(imCntSizPerScaleVec,1);
        subImages = cell(subImageCnt, 1);
        smallestImgRCLength = 30;
        for s = 1: subImageCnt
            numOfSamples = imCntSizPerScaleVec(s,1);
            pbs_Row = 2^(s-1);%paletteBlockCnt
            pbs_Col = ceil(imCntSizPerScaleVec(s,1)/pbs_Row);
            curPalette = zeros([pbs_Row*smallestImgRCLength,pbs_Col*smallestImgRCLength,3], 'uint8');
            j=0;
            for i=1:imCnt
                x_crop = x_In(imCropVec(i,1):imCropVec(i,2),imCropVec(i,3):imCropVec(i,4),:);
                if (imCntSizPerScaleVec(s,2)==size(x_crop,1))
                    j=j+1;
                    [biRY, biCX] = ind2sub([pbs_Row,pbs_Col],j);
                    x_crop = imresize(x_crop,[smallestImgRCLength smallestImgRCLength]);
                    curPalette((biRY-1)*smallestImgRCLength+1:(biRY*smallestImgRCLength),(biCX-1)*smallestImgRCLength+1:(biCX*smallestImgRCLength),:) = x_crop;
                end
            end
            subImages{s,1} = struct;
            subImages{s,1}.image = curPalette;
            subImages{s,1}.title = ['Original ImSize(' mat2str(size(x_In)) '), CroppedBlockSize(' mat2str(imCntSizPerScaleVec(s,2:3)) '), CroppedImgCount(' num2str(numOfSamples) ')'];
        end
        
        if true
            for s = 1: subImageCnt
                figure(68+s);clf;
                imshow(subImages{s,1}.image);
                title(subImages{s,1}.title);
            end
        end
    end
end