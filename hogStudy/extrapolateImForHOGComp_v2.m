function hogImArr = extrapolateImForHOGComp_v2(x_In, rotDegrees, blockCountsVec, hogStruct, handInfo)
%extrapolateImForHOGComp_v2 generate scaled hog outputs for HOG comparison
%   I have an image with some size,
%   I am looking for a sub part of image that would be the actual part I am
%   seeking for.. Hence I have to look at it with different surrounding
%   boxes and find the best match between 2 mother images
%   That would require another function to compare these sorted subImages
%   and return which 2 best matched

    imSizeCur = size(x_In);    
    if length(imSizeCur)==3 && imSizeCur(3)==3
        x_Gray = rgb2gray(x_In);
    end
    rotCnt = length(rotDegrees);
    rotMasks = getRotationMasks(x_Gray, rotDegrees);
    
    x_Gray = flipForHOG(x_Gray, handInfo);
    hogBlockCntAll = rotCnt*sum(blockCountsVec.*blockCountsVec);
    hogImArr = inf(hogBlockCntAll,hogStruct.histogramBinCount);
    to = 0;
    for i=1:length(blockCountsVec)
        for r = 1:rotCnt
            rotDeg = rotDegrees(r);
            hogStruct.blockCounts = [blockCountsVec(i) blockCountsVec(i)];
            x_Gray_rot = imrotate(x_Gray,rotDeg,'bilinear','crop');
            x_Gray_rot(rotMasks{r,1}) = rand(rotMasks{r,2},1)*255;
            hogIm = exractHOG_dg(x_Gray_rot, hogStruct);
            hogIm = reshape(hogIm,[],hogStruct.histogramBinCount);
            fr = to+1;
            to = fr + blockCountsVec(i).^2 - 1;
            hogImArr(fr:to,:) = hogIm;
        end
    end
    assert(to==hogBlockCntAll,['These<hogBlockCntAll(' num2str(hogBlockCntAll) ')-(' num2str(to)  ')to> must match']);
    assert(inf~=max(hogImArr(:)),'hogImArr shouldnt have inf in it');
end

function rotMasks = getRotationMasks(x_Gray, rotDegrees)
    whiteImage = ones(size(x_Gray),class(x_Gray))*max(x_Gray(:));
    rotCnt = length(rotDegrees);
    rotMasks = cell(rotCnt,2);
    for r = 1:rotCnt
        rotDeg = rotDegrees(r);
        whiteImage_rot = imrotate(whiteImage,rotDeg,'bilinear','crop');
        rotMasks{r,1} = whiteImage_rot==0;
        rotMasks{r,2} = sum(whiteImage_rot(:)==0);
    end
end