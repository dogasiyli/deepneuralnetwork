function [clusterSuitabilityVec, minDistClusterInds, clusterProbsVec] = findBestClusterIDVec(clustersStruct, hogFeat_asHoGVec, clusterID)
    clusterDefinitionMat = clustersStruct.model.clusterDefinitionMat;
    centerVecMat = clustersStruct.model.centerVecMat;
    if exist('clusterID','var') && ~isempty(clusterID)
        slctClsuterIDrows = find(clusterDefinitionMat(:,1)==clusterID);
        clusterDefinitionMat = clusterDefinitionMat(slctClsuterIDrows,:);
        centerVecMat = centerVecMat(slctClsuterIDrows,:);
    end    
    
    [distMat, minDistElements] = computeDistanceMatrix(hogFeat_asHoGVec, centerVecMat, 1);
    %assign to the closest cluster found and retrieve subcluster
    %related information
    minDistClusterInds = minDistElements.colInds;%max(minDistElements.rowInds,minDistElements.colInds)
    
    if exist('clusterID','var') && ~isempty(clusterID)
        assert(clusterID == clusterDefinitionMat(minDistClusterInds,1),'Although obvious just wanna make sure this always stands!!');    
    else
        clusterID = clusterDefinitionMat(minDistClusterInds,1);
    end
    
    if floor(minDistElements.minDist)>ceil(clusterDefinitionMat(minDistClusterInds,3))
        %need to create a new subcluster
        lastUsedSubClusterIDOfCluster = 1 + max(clusterDefinitionMat(clusterDefinitionMat(:,1)==clusterID,2));        
    else
        %can go into this cluster
        lastUsedSubClusterIDOfCluster = NaN;
    end
    %[clusterID, subClusterID, allowedDist, sampleDistanceToCenterVec, lastUsedSubClusterIDOfCluster]
    clusterSuitabilityVec = [clusterDefinitionMat(minDistClusterInds,:) minDistElements.minDist lastUsedSubClusterIDOfCluster];
    
    if nargout>2
        %for each cluster find the matching probability
        if exist('slctClsuterIDrows','var')
            clusterDefinitionMat = clustersStruct.model.clusterDefinitionMat;
            centerVecMat = clustersStruct.model.centerVecMat;
            [distMat, minDistElements] = computeDistanceMatrix(hogFeat_asHoGVec, centerVecMat, 1);
        end
        clusterCount = max(clusterDefinitionMat(:,1));
        
        clusterProbsVec = zeros(clusterCount,5);%[clusterID subClusterID acceptedDist curDist acceptedDist/curDist] -> sort for col5
        for i=1:clusterCount
            rowsOfCluster_i = find(clusterDefinitionMat(:,1)==i);
            distMat_clutser_i = distMat(rowsOfCluster_i);
            if distMat_clutser_i<0
                distMat_clutser_i=0;
            end
            [distMat_clutser_i, idx_cluster_i] = sort(distMat_clutser_i,'ascend');
            clusterProbsVec(i,1:4) = [i clusterDefinitionMat(rowsOfCluster_i(idx_cluster_i(1)),2:3) distMat_clutser_i(1)];
        end
        %clusterProbsVec(clusterProbsVec(:,4)<=0,4) = clusterProbsVec(clusterProbsVec(:,4)<=0,3)/5;
        clusterProbsVec(:,5) = clusterProbsVec(:,3)./clusterProbsVec(:,4);
        clusterProbsVec(clusterProbsVec(:,5)>=1,5) = 1;
        clusterProbsVec(~isfinite(clusterProbsVec(:,5)),5) = 0;
        clusterProbsVec = sortrows(clusterProbsVec, [-5 +4]);
    end
end