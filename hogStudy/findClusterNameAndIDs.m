function [foundlusterID, foundClusterName] = findClusterNameAndIDs(clusterName_toSearch, curClusterNamesCell)
    foundlusterID = [];
    foundClusterName = '';
    curClusterCnt = length(curClusterNamesCell);
    if ~isempty(clusterName_toSearch) && curClusterCnt>0
        %try to add to a known cluster
        foundClusterName = strrep(strtrim(clusterName_toSearch),' ','_');
        try
            foundlusterID = str2num(foundClusterName);
            foundClusterName = curClusterNamesCell{foundlusterID};
        catch
            foundlusterID = [];
        end
        if isempty(foundlusterID)
            %the <knownClusterName> is not the ID given
            %so try to find it in the names of clusters
            for i=1:curClusterCnt
                curClusterName = curClusterNamesCell{i};
                if strcmp(curClusterName, foundClusterName)
                    foundlusterID = i;
                    %if we are here
                    %knownClusterName will stay as it is
                    %and whatever will be added, will be added into the
                    %<clusterIDToBeAdded> row of curClusters
                    break;
                end
            end
            %if we are here <setClusterName> must be assigned to ''
            %so that we can search for <setClusterName> down below
            if isempty(foundlusterID)
                foundClusterName = '';
            end
        end
    end
end