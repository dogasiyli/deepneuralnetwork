function nW = fineTuneDTWMap( V, T, W)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
%V - distMat_Values
%T - distMat_Type (3 is still)
%W - distMat_Way (Way that is defined) 
%nW- distMat_newWay (updated/new Way)

    [rCnt,cCnt] = size(V);
    nodeCnt = size(W,1);
    nW = W;
    for i =1:nodeCnt
        r = W(i,1);
        c = W(i,2);
        [vr, vc, inds, tArea, vArea] = getPossibleStillNeighbourBlocks(T,V,r,c,rCnt,cCnt);
        nW(i,:) = [vr vc];
    end
    nW = unique(nW,'rows');
end

function [vr, vc, inds, tArea, vArea] = getPossibleStillNeighbourBlocks(T, V, r, c, rCnt, cCnt)
    tCur = T(r,c);
    switch tCur
        case 0
            %c-1 c c+1
            %
            % 3  1  3 - r-1
            % 2 *0* 2 -  r
            % 3  1  3 - r+1
            rr = [r-1 r+1];
            rr(rr<1) = [];
            rr(rr>rCnt) = [];
            
            cc = [c-1 c+1];
            cc(cc<1) = [];
            cc(cc>cCnt) = [];
        case 1
            %3*1*3
            rr = r;
            
            cc = [c-1 c+1];
            cc(cc<1) = [];
            cc(cc>cCnt) = [];
        case 2
            % 3
            %*2*
            % 3
            rr = [r-1 r+1];
            rr(rr<1) = [];
            rr(rr>rCnt) = [];
            
            cc = c;
        case 3
            %3 1 3 1 3
            %2 0 2 0 2
            %3 1*3*1 3
            %2 0 2 0 2
            %3 1 3 1 3
            rr = r-2 : 2 : r+2 ;
            rr(rr<1) = [];
            rr(rr>rCnt) = [];
            
            cc = c-2 : 2 : c+2 ;
            cc(cc<1) = [];
            cc(cc>cCnt) = [];
    end
    %now change rr and cc vectors to real row and col ids
    curRowCnt = length(rr);
    curColCnt = length(cc);
    rowIDs = bsxfun(@times,reshape(rr,[],1),ones(1,curColCnt));
    colIDs = bsxfun(@times,reshape(cc,1,[]),ones(curRowCnt,1));
    inds = sub2ind([rCnt cCnt], rowIDs(:), colIDs(:));
    tAreaBase = T(min(rr):max(rr),min(cc):max(cc));
    vAreaBase = V(min(rr):max(rr),min(cc):max(cc));
    tArea = reshape(T(inds),curRowCnt,curColCnt);
    vArea = reshape(V(inds),curRowCnt,curColCnt);
    
    [minVal,minInds] = min(vArea(:));
    minIndsV = inds(minInds);
    [vr,vc] = ind2sub([rCnt cCnt], minIndsV);
    assert(V(vr,vc)==minVal,'this must be true');
    
end
