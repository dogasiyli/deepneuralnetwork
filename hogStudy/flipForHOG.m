function im_Gray = flipForHOG( im_Gray, handInfo)
%flipForHOG fliplr the gray im for HOG extraction according to handInfo
%   handInfo is a struct with 2 fields
%   .handDominant -> is either 'LH','RH' as char or 1,2 as numeric
%   .handCur -> current hand image is either 'LH','RH','BH' as char or 1,2,3 as numeric
%
%   we should flip the image to get the dominant HOG feature

    if ((ischar(handInfo.handDominant) && strcmpi(handInfo.handDominant,'LH') && strcmpi(handInfo.handCur,'RH'))||(isnumeric(handInfo.handDominant) && (handInfo.handDominant==1) && (handInfo.handCur==2)))
        %handDominant is left but handCur is right so fliplr
        im_Gray = fliplr(im_Gray);
    end
    if ((ischar(handInfo.handDominant) && strcmpi(handInfo.handDominant,'RH') && strcmpi(handInfo.handCur,'LH'))||(isnumeric(handInfo.handDominant) && (handInfo.handDominant==2) && (handInfo.handCur==1)))
        %handDominant is right but handCur is left so fliplr
        im_Gray = fliplr(im_Gray);
    end
end

