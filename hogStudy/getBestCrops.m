function bestCropStruct = getBestCrops(sourceFolder, hogImArr_cell_X, imcropVec_cell_X,hogImArr_cell_Y, imcropVec_cell_Y, lrb_X3Y3XY5, vidXYInf, boxCount)
%[ minDist, distMat, distValsBlocks, x_boxes, y_boxes, imCropped_X, imCropped_Y]
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

%lh1_rh2_bh3 = [X_lh1_rh2_bh3 Y_lh1_rh2_bh3 XY_rl1_rr2_bb3_ll4_lr5]

    frameID_X = vidXYInf(1,4);
    frameID_Y = vidXYInf(2,4);

    hogImArr_cell_X_lrb = hogImArr_cell_X{frameID_X,lrb_X3Y3XY5(1)};
    imcropVec_cell_X_lrb = imcropVec_cell_X{frameID_X,lrb_X3Y3XY5(1)};
    hogImArr_cell_Y_lrb = hogImArr_cell_Y{frameID_Y,lrb_X3Y3XY5(2)};
    imcropVec_cell_Y_lrb = imcropVec_cell_Y{frameID_Y,lrb_X3Y3XY5(2)};

    %distMat is expected to be 276 by 276
    %meaning 276 different crops per image of X and Y
    %rows represent X image
    %cols represent Y image
    [distMat, minDistElements] = computeDistanceMatrix(hogImArr_cell_X_lrb', hogImArr_cell_Y_lrb', boxCount);
    rxi_rh = minDistElements.rowInds;
    cyj_rh = minDistElements.colInds;
    %rxi_rh represents the IDs of X image crop area
    %cyj_rh represents the IDs of Y image crop area
    %here we take <boxCount> number of best crops
    
    %each row has 4 elements -> fromRow toRow, fromCol toCol for indices 1 to 4
    x_boxes = imcropVec_cell_X_lrb(rxi_rh,:);
    y_boxes = imcropVec_cell_Y_lrb(cyj_rh,:);
    
    %vidXYInf = [signIDs(1) userIDs(1) signRepeatIDs(1) imIDXY(1);signIDs(2) userIDs(2) signRepeatIDs(2) imIDXY(2)];
    userFolderName_X = [sourceFolder filesep num2str(vidXYInf(1,1)) filesep 'User_' num2str(vidXYInf(1,2)) '_' num2str(vidXYInf(1,3))];
    userFolderName_Y = [sourceFolder filesep num2str(vidXYInf(2,1)) filesep 'User_' num2str(vidXYInf(2,2)) '_' num2str(vidXYInf(2,3))];
    
    LRBstr = {'RL','RR','BB','LL','LR'};  
    lh_rh_bh_str = cell(1,2);
    lh_rh_bh_str{1} = [LRBstr{1,lrb_X3Y3XY5(3)}(1) 'H'];
    lh_rh_bh_str{2} = [LRBstr{1,lrb_X3Y3XY5(3)}(2) 'H'];

    imCropped_X = imread([userFolderName_X '/cropped/crop_' lh_rh_bh_str{1} '_' num2str(frameID_X,'%03d') '.png']);
    imCropped_X = imresize(imCropped_X,[200 200]);
    imCropped_Y = imread([userFolderName_Y '/cropped/crop_' lh_rh_bh_str{2} '_' num2str(frameID_Y,'%03d') '.png']);
    imCropped_Y = imresize(imCropped_Y,[200 200]);
        
    %lrb_X3Y3XY5 = [X_lh1_rh2_bh3 Y_lh1_rh2_bh3 XY_rl1_rr2_bb3_ll4_lr5]
    realFineCropInfo_X = fineCropToRealCrop(sourceFolder, vidXYInf(1,:), lrb_X3Y3XY5(1), x_boxes(1,:));
    realFineCropInfo_Y = fineCropToRealCrop(sourceFolder, vidXYInf(2,:), lrb_X3Y3XY5(2), y_boxes(1,:));
    
    hogVec_X = hogImArr_cell_X_lrb(:,rxi_rh(1));
    hogVec_Y = hogImArr_cell_Y_lrb(:,cyj_rh(1));
    
    fbX = x_boxes(1,:);
    fbY = y_boxes(1,:);
    
    bestCropStruct = struct;
    bestCropStruct.minDist = minDistElements.minDist;
    bestCropStruct.hogVec = [hogVec_X,hogVec_Y];
    bestCropStruct.imCropped = {imCropped_X, imCropped_Y};
    bestCropStruct.imFineCropped = {imCropped_X(fbX(1):fbX(2),fbX(3):fbX(4),:), imCropped_Y(fbY(1):fbY(2),fbY(3):fbY(4),:)};
    bestCropStruct.fineCropInfo = [realFineCropInfo_X;realFineCropInfo_Y];
    bestCropStruct.x_boxes = x_boxes;
    bestCropStruct.y_boxes = y_boxes;
    bestCropStruct.originalCropDist = distMat(1,1);
end

function realCropArea = fineCropToRealCrop(sourceFolder, videoInfo4Vec, lh1_rh2_bh3, fineCropArea)
    userFolderName_X = [sourceFolder filesep num2str(videoInfo4Vec(1)) filesep 'User_' num2str(videoInfo4Vec(2)) '_' num2str(videoInfo4Vec(3))];
    cropAreaFileName = [userFolderName_X filesep 'cropArea.mat'];
    load(cropAreaFileName, 'cropArea');

    frameID = videoInfo4Vec(4);
    switch lh1_rh2_bh3
        case 3
            %both hands big
            frBig = cropArea.BH_big(frameID,1:4);
        case 1
            %left hand big
            frBig = cropArea.LH_big(frameID,1:4);
        case 2
            %right hand big
            frBig = cropArea.RH_big(frameID,1:4);    
    end
    
    %now the big area is as below 
    y_row_fr = frBig(1); 
    y_row_to = frBig(1)+frBig(3)-1; 
    x_col_fr = frBig(2); 
    x_col_to = frBig(2)+frBig(4)-1;
    
    %map 1 to 200 --> the big hand area
    yRowCrop_01 = map2_a_b(1:200,y_row_fr, y_row_to);
    xColCrop_01 = map2_a_b(1:200,x_col_fr, x_col_to);
    
    %now get the so called fineCrop by means of real crop areas
    yRowCrop_02 = yRowCrop_01([fineCropArea(1),fineCropArea(2)]);
    xColCrop_02 = xColCrop_01([fineCropArea(3),fineCropArea(4)]);
    
    realCropArea = [floor(yRowCrop_02(1)) floor(xColCrop_02(1)) round(yRowCrop_02(2)-yRowCrop_02(1)) round(xColCrop_02(2)-xColCrop_02(1))];
end

