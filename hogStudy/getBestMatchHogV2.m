function [bestMatchDif, leftImID, rightImID, bestMatchID] = getBestMatchHogV2(hogIm1, hogIm2, blockArea, baseBlockCnt, stride)
    if nargin == 0
        baseBlockCnt = 3;
        binCount = 4;
        blockArea = [4 7];
        a = reshape(1:(prod(blockArea)*binCount),binCount,prod(blockArea))';
        %b = reshape(1:(binCount*baseBlockCnt^2),binCount,baseBlockCnt^2)'/100;
        hogIm1 = a;
        leftTopInds = 6;
        ar1 = ((leftTopInds : leftTopInds+baseBlockCnt-1)') + (0:(baseBlockCnt-1)).*blockArea(1);
        randMat = rand(numel(ar1),size(hogIm1,2));
        hogIm1(ar1(:),:) = randMat;
        hogIm2 = a+max(a(:));
        hogIm2(ar1(:)+3,:) = randMat;
        stride = 1;
    end

    sizeImg = blockArea;
    blockSize = [baseBlockCnt baseBlockCnt];
    binCount = size(hogIm1,2);
    hogV2MapStruct = createOrLoadHogV2DifMap(sizeImg, blockSize, stride, binCount);
    
    
    h1 = hogIm1';
    h1 = h1(:);
    h1 = h1(hogV2MapStruct.hogPixInputSingle);
    h1 = reshape(h1, size(hogV2MapStruct.hogPixInputSingle));
    
    h2 = hogIm2';
    h2 = h2(:);
    h2 = h2(hogV2MapStruct.hogPixInputSingle);
    h2 = reshape(h2, size(hogV2MapStruct.hogPixInputSingle));
    
    D = pdist2_fast(h1,h2);
    [distVals, inds] = sort(D(:));
    [leftImID, rightImID] = ind2sub(size(D),inds(1));
    bestMatchDif = distVals(1);
    bestMatchID = {inds(1), hogV2MapStruct.rowColMap(leftImID,:), hogV2MapStruct.rowColMap(rightImID,:)};
end

