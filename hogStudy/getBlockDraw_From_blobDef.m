function MBlocks_draw = getBlockDraw_From_blobDef(blobDef, MDist)
    blockCnt = size(blobDef,1);
    MBlocks_draw = max(MDist(:))*ones(size(MDist));
    for i=1:blockCnt
        cbA = blobDef(i,end-3:end);
        MBlocks_draw(cbA(1):cbA(2),cbA(3):cbA(4)) = blobDef(i,3);
    end
end