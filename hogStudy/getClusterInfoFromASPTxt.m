function clustersASPInfoStruct = getClusterInfoFromASPTxt(signSourceFolder, signID)

    signSourceFolder_X = [signSourceFolder filesep num2str(signID)];
    singleClustersFileName = [signSourceFolder_X filesep 'clustersCS_Single.txt'];
    bothClustersFileName = [signSourceFolder_X filesep 'clustersCS_Both.txt'];
    
    clustersASPInfoStruct = struct;
    clustersASPInfoStruct.single = getClusterStructFromFile(singleClustersFileName);
    clustersASPInfoStruct.both = getClusterStructFromFile(bothClustersFileName);
end

function [clusterInfoStruct] = getClusterStructFromFile(fullFileName)
    %clusterInfoStruct = struct([]);
    clusterInfoStruct = struct('clusterID',[],'clusterName',[],'representativeFrameSign',[],'representativeFrameUser',[],'representativeFrameRepeat',[],'representativeFrameHand',[],'representativeFrameFrameID',[]);
    clusterCnt = 1;
    fid = fopen(fullFileName);
    tline = fgets(fid);
    while ischar(tline)
        try
            curClus = struct;
            try
                curLineVec = split(tline,'+');
                curClus.clusterID = curLineVec{1,1};
                curClus.clusterName = curLineVec{2,1};
                curClus.representativeFrameSign = curLineVec{3,1};
                curClus.representativeFrameUser = curLineVec{4,1};
                curClus.representativeFrameRepeat = curLineVec{5,1};
                curClus.representativeFrameHand = curLineVec{6,1};
                curClus.representativeFrameFrameID = curLineVec{7,1};
            catch
                curLineVec = strsplit(tline,'+');
                curClus.clusterID = curLineVec{1};
                curClus.clusterName = curLineVec{2};
                curClus.representativeFrameSign = curLineVec{3};
                curClus.representativeFrameUser = curLineVec{4};
                curClus.representativeFrameRepeat = curLineVec{5};
                curClus.representativeFrameHand = curLineVec{6};
                curClus.representativeFrameFrameID = curLineVec{7};            
            end
            clusterInfoStruct(clusterCnt) = curClus;
            clusterCnt = clusterCnt +1;
        catch exception
            %disp(['exception = ' exception.message])
        end
        tline = fgets(fid);
    end
    fclose(fid);
end

