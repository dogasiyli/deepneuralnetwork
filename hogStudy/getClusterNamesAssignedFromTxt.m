function [clusterNames, otherInfo] = getClusterNamesAssignedFromTxt(srcFold, surSlctListStruct)
    %into sign folder
    clusterFilesFolderName = [srcFold filesep num2str(surSlctListStruct.signIDList(1)) ]; %#ok<*AGROW>
    if surSlctListStruct.single0_double1==0
        %singleHand
        fileNameClusterTxt = [clusterFilesFolderName filesep 'clustersCS_Single.txt'];
    elseif surSlctListStruct.single0_double1==1
        %both hands
        fileNameClusterTxt = [clusterFilesFolderName filesep 'clustersCS_Both.txt'];
    end
    try
        if exist(fileNameClusterTxt,'file')
            fileID = fopen(fileNameClusterTxt,'r');
            otherInfo = textscan(fileID, '%s');
            otherInfo = otherInfo{1};
            fclose(fileID);
            for i=1:length(otherInfo)
                s = strsplit(otherInfo{i,1},'+');
                if length(s)>1
                    clusterNames{i} = s{2}; 
                else
                    clusterNames = clusterNames(1:i-1);
                    break
                end
            end
        end
    catch err
        disp(['error(' err.message ')']);
        fclose(fileID);
        clusterNames = [];
        otherInfo = [];
        return
    end
end