function [accResults, recallResults, precisionResults, f1results, confMatCells, totalResult] = getDetailedResultByLabelingStrategy(labelingStrategyStr, predictedLabels, existingLabels)
    %'fromSkeletonStableAll','fromAssignedLabels','assignedLabelsRemove1','assignedLabelsExcept1','fromSkeletonOnly'
    
    videoCount = size(predictedLabels,1);
    
    confMatCells = cell(videoCount,2);
    
    gtVec = reshape(existingLabels(:,1:2),videoCount*2,1);
    gtVec = cell2mat(cat(1,gtVec(:)'));
    gtLabels_all = unique(gtVec);
    prVec = reshape(predictedLabels(:,1:2),videoCount*2,1);
    prVec = cell2mat(cat(1,prVec(:)'));
    prLabels_all = unique(prVec);
    cMat_all = confusionmat(gtVec, prVec);%row-groundTruth, cols-predictions
    confMatStats_all = calcConfusionStatistics(cMat_all);
    totalResult.cmat = cMat_all;
    totalResult.confMatStats = confMatStats_all;
    
    labels_all = unique([gtLabels_all prLabels_all]);
    labelCnt = length(labels_all);
    
    table2disp = [cMat_all zeros(length(labels_all),1) vertcat(confMatStats_all(:).Accuracy) vertcat(confMatStats_all(:).Sensitivity) vertcat(confMatStats_all(:).Specificity) vertcat(confMatStats_all(:).Precision) vertcat(confMatStats_all(:).F1_Score)]; 
        
    disptable(table2disp);

    accResults = zeros(videoCount,labelCnt+1);
    recallResults = zeros(videoCount,labelCnt);
    precisionResults = zeros(videoCount,labelCnt);
    f1results = zeros(videoCount,labelCnt);
    
    for v=1:videoCount
        for lh1_rh2_bh3 = 1:3
            prVec = predictedLabels{v,lh1_rh2_bh3};
            gtVec = existingLabels{v,lh1_rh2_bh3};
            if sum(prVec)==0 || isempty(gtVec)
                continue
            end
            gtLabels = unique(gtVec);
            switch labelingStrategyStr
                case {'fromSkeletonStableAll','fsa','1'}%'fromSkeletonStableAll'; 
                    error('not implemented yet');
                case {'fromAssignedLabels','fal','2'}%'fromAssignedLabels';
                    %prVec has all labels including zero
                    error('not implemented yet');
                case {'assignedLabelsRemove1','ar1','3'}%'assignedLabelsRemove1';   
                    if (sum(prVec==1)>0)
                        disp('this vec can not have 1 as label');
                    end
                case {'assignedLabelsExcept1','ax1','4'}%'assignedLabelsExcept1';
                    error('not implemented yet');
                otherwise%case {'fromSkeletonOnly','fso','0'}
                    error('not implemented yet');
            end
            prLabels = unique(prVec);
            cMat = confusionmat([gtVec labels_all], [prVec labels_all]);%row-groundTruth, cols-predictions
            cMat = cMat - eye(labelCnt);
            confMatStats = calcConfusionStatistics(cMat);
            if isempty(confMatCells{v,1}) && lh1_rh2_bh3<3
                confMatCells{v,1} = cMat;
            else
                confMatCells{v,1} = confMatCells{v,1} + cMat;
            end
        end
        cMat = confMatCells{v,1};
        confMatStats = calcConfusionStatistics(cMat);
        accResults(v,1:end-1) = vertcat(confMatStats(:).Accuracy)';
        accResults(v,end) = sum(diag(cMat))/sum(cMat(:));
        recallResults(v,1:end) = vertcat(confMatStats(:).Sensitivity)';
        precisionResults(v,1:end) = vertcat(confMatStats(:).Precision)';
        f1results(v,1:end) = vertcat(confMatStats(:).F1_Score)';
    end
end