function backupFolder = getExactBackupFolder(backupFoldRoot, typeRequested)
    switch typeRequested
        case 'Feats'
        case 'Labels'
        otherwise
            error(['typeRequested must be either Feats or Labels.. ' typeRequested ' not recognized ']);
    end
    backupFolder = [backupFoldRoot 'Backup_' typeRequested ];
    if ~exist(backupFolder,'dir')
        try
            mkdir(backupFolder);
        catch
            disp(['could not create <' backupFolder '> folder']);
            return
        end
    end    
end