function cropArea = getGenerateCropArea(cropAreaFileName, skeletonMat, optParamStruct)
    %optParamStruct_getGenerateCropArea = struct('enforceReCreateNew', false,'colDepStr', 'color', 'multipliers', struct('bigHandFaceRatio',[1,1],'smlHandFaceRatio',[2/3,2/3]));
    if ~exist('optParamStruct','var')
        optParamStruct = [];
    end
    colDepStr = getStructField(optParamStruct, 'colDepStr', 'color');
    enforceReCreateNew = getStructField(optParamStruct, 'enforceReCreateNew', false);
    multipliers = getStructField(optParamStruct, 'multipliers', struct('bigHandFaceRatio',[1,1],'smlHandFaceRatio',[2/3,2/3]));

    if exist(cropAreaFileName,'file') && ~enforceReCreateNew
        disp(['Loading(' cropAreaFileName ')']);
        load(cropAreaFileName, 'cropArea');
    else
        optParamStruct_cropArea = struct('colDepStr', colDepStr,'multipliers', multipliers);
        cropArea = getCropArea(skeletonMat, [], optParamStruct_cropArea);
        disp(['Saving(' cropAreaFileName ')']);
        save(cropAreaFileName, 'cropArea');
    end
end