function [hogImArr_cell, imcropVecVariable] = getHOGFeatsofHands(signID, userID, signRepeatID, optParamStruct)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

%   imIDBegEnd = [1 105];
%   sourceFolder = '/media/doga/Data/FromUbuntu/Doga';
%   signID = 112;
%   userID = 2;
%   signRepeatID = 2;
%   OPS = struct('hogVersion',2,'enforceCreateNew',false,'srcFold',getVariableByComputerName('srcFold'));
    if ~exist('optParamStruct', 'var') || ~isstruct(optParamStruct)
        optParamStruct = [];
    end
    srcFold                 = getStructField(optParamStruct, 'srcFold', getVariableByComputerName('srcFold'));
    enforceCreateNew        = getStructField(optParamStruct, 'enforceCreateNew', false);
    dominantHand            = getStructField(optParamStruct, 'dominantHand', 'LH');
    imIDBegEnd              = getStructField(optParamStruct, 'imIDBegEnd', []);
    scales2Use              = getStructField(optParamStruct, 'scales2Use', 1:-0.10:0.70);
    strideFixed             = getStructField(optParamStruct, 'strideFixed', 5);
    blockCountsVec          = getStructField(optParamStruct, 'blockCountsVec', [10 15 19 23]);
    inputImSize             = getStructField(optParamStruct, 'inputImSize', [200 200]);
    hogStruct               = getStructField(optParamStruct, 'hogStruct', struct('blockCounts',[10 10], 'histogramBinCount', 9));
    handInfo                = getStructField(optParamStruct, 'hogStruct', struct('handDominant',dominantHand));
    hogVersion              = getStructField(optParamStruct, 'hogVersion', 1);
    removeCroppedHandsFolder= getStructField(optParamStruct, 'removeCroppedHandsFolder', false);

    %set here the number of HOG windows per bound box
    %hogStruct.blockCounts = [10 10];%[nwin_x nwin_y]
    %set here the number of histogram bins
    %hogStruct.histogramBinCount = 9;

    userFolderName = [srcFold filesep num2str(signID) filesep 'User_' num2str(userID) '_' num2str(signRepeatID)];
    hogCropFeatsNameFull = getBackupFile(userFolderName, hogVersion, enforceCreateNew);%check the backup folder to get the backed up hogCropFeats
    

    [runCreateHOGFeatsofHands, hogImArr_cell, imcropVecVariable] = getHogCells(hogCropFeatsNameFull, hogVersion, enforceCreateNew);
    if ~runCreateHOGFeatsofHands
        return
    end

    [imIDBegEnd, croppedHandsFolderName] = getOrMakeCroppedImages(userFolderName, imIDBegEnd,signID, userID, signRepeatID);
    
    if hogVersion==1
        [hogImArr_cell, imcropVec_cell] = extractHog_v1(userFolderName, signID, userID, signRepeatID, imIDBegEnd, inputImSize, handInfo, scales2Use, strideFixed, hogStruct, hogImArr_cell, imcropVecVariable);
        imcropVecVariable = imcropVec_cell;
        save(hogCropFeatsNameFull, 'hogImArr_cell', 'imcropVec_cell');
    elseif hogVersion==2
        [hogImArr_cell, imcropVec_struct] = extractHog_v2(croppedHandsFolderName, imIDBegEnd, inputImSize, handInfo, blockCountsVec, hogStruct);
        imcropVecVariable = imcropVec_struct;
        save(hogCropFeatsNameFull, 'hogImArr_cell', 'imcropVec_struct');
    end
    
    if removeCroppedHandsFolder
        rmdir(croppedHandsFolderName,'s');
    end
end

function [hogImArr_cell, imcropVec_cell] = extractHog_v1(userFolderName, signID, userID, signRepeatID, imIDBegEnd, inputImSize, handInfo, scales2Use, strideFixed, hogStruct, hogImArr_cell, imcropVec_cell)
    cellCnt = imIDBegEnd(2)-imIDBegEnd(1)+1;
    skipLRH = true;
    if ~isempty(hogImArr_cell) && size(hogImArr_cell,1)==cellCnt && size(hogImArr_cell,2)==2
        %lh and rh already created
    else
        hogImArr_cell = cell(cellCnt,3);%1-Left, 2-Right, 3-Both Hands
        skipLRH = false;
    end
    if ~isempty(imcropVec_cell) && size(imcropVec_cell,1)==cellCnt && size(imcropVec_cell,2)==2
        %lh and rh already created
    else
        imcropVec_cell = cell(cellCnt,3);%1-Left, 2-Right, 3-Both Hands
        skipLRH = false;
    end
    
    croppedHandsFolderName = [userFolderName filesep 'cropped' filesep];
    for i = 1:cellCnt
        if mod(i,20)==0
            disp(['s' num2str(signID) '_u' num2str(userID) '_r' num2str(signRepeatID) '_f' num2str(i) ' of ' num2str(cellCnt) ' is being analyzed..']);
        end
        imID = i + imIDBegEnd(1) - 1;
        
        if ~skipLRH
            im_lh = imread([croppedHandsFolderName 'crop_LH_' num2str(imID,'%03d') '.png']);
            im_lh = imresize(im_lh, inputImSize);
            handInfo.handCur = 'LH';
            [hogImArr_lh, imcropVec_lh] = extrapolateImForHOGComp(im_lh, scales2Use, strideFixed, hogStruct, handInfo);
            hogImArr_cell{i,1} = hogImArr_lh;
            imcropVec_cell{i,1} = imcropVec_lh;

            im_rh = imread([croppedHandsFolderName 'crop_RH_' num2str(imID,'%03d') '.png']);
            im_rh = imresize(im_rh, inputImSize);
            handInfo.handCur = 'RH';
            [hogImArr_rh, imcropVec_rh] = extrapolateImForHOGComp(im_rh, scales2Use, strideFixed, hogStruct, handInfo);
            hogImArr_cell{i,2} = hogImArr_rh;
            imcropVec_cell{i,2} = imcropVec_rh;
        end
        
        im_bh = imread([croppedHandsFolderName 'crop_BH_' num2str(imID,'%03d') '.png']);        
        im_bh = imresize(im_bh, inputImSize);
        handInfo.handCur = 'BH';
        [hogImArr_bh, imcropVec_bh] = extrapolateImForHOGComp(im_bh, scales2Use, strideFixed, hogStruct, handInfo);
        hogImArr_cell{i,3} = hogImArr_bh;
        imcropVec_cell{i,3} = imcropVec_bh;
    end    
end

function [imIDBegEnd, croppedHandsFolderName, frameCount] = getOrMakeCroppedImages(userFolderName, imIDBegEnd, signID, userID, signRepeatID)
    croppedHandsFolderName = [userFolderName filesep 'cropped' filesep];
    if ~exist(croppedHandsFolderName,'dir')
        try
            unzip([userFolderName filesep 'cropped.zip'],userFolderName);
        catch
            cropVidFrames(signID, userID, signRepeatID);
            unzip([userFolderName filesep 'cropped.zip'],userFolderName);
        end
    end
    [~, frameCount] = getFileList(croppedHandsFolderName, '.png');
    frameCount = round(frameCount/3);
    
    if ~exist('imIDBegEnd', 'var') || isempty(imIDBegEnd)
        imIDBegEnd = [1 frameCount];
    end
    imIDBegEnd(2) = min(imIDBegEnd(2), frameCount);
end

function [runCreateHOGFeatsofHands, hogImArr_cell, imcropVecVariable] = getHogCells(hogCropFeatsNameFull, hogVersion, enforceCreateNew)
    % hogCropFeatsNameFull2 = [userFolderName filesep 'hog_crop2.mat'];
    %00-file not exist, no enforce create
    %01-file not exist, enforce create
    %10-file exist, and no enforce create
    %11-file exist, but enforce create
    hogFileExist = exist(hogCropFeatsNameFull,'file');
    stateID = double(hogFileExist)*10+double(enforceCreateNew);
    hogImArr_cell = [];
    imcropVecVariable = [];
    runCreateHOGFeatsofHands = true;
    switch stateID
        case 21
            disp(['Although <' hogCropFeatsNameFull '> exist, hogCropFeats will be created from scratch.']);
        case 20
            disp(['<' hogCropFeatsNameFull '> exists hence hogCropFeats will be loaded.']);
            try
                if hogVersion==1
                    load(hogCropFeatsNameFull, 'hogImArr_cell', 'imcropVec_cell');
                    runCreateHOGFeatsofHands = ~exist('hogImArr_cell','var') || ~exist('imcropVec_cell','var') || size(hogImArr_cell,2)<3 || size(imcropVec_cell,2)<3;
                    imcropVecVariable = imcropVec_cell;
                else
                    load(hogCropFeatsNameFull, 'hogImArr_cell', 'imcropVec_struct');
                    runCreateHOGFeatsofHands = ~exist('hogImArr_cell','var') || ~exist('imcropVec_struct','var') || size(hogImArr_cell,2)<3;
                    imcropVecVariable = imcropVec_struct;
                end
            catch
                runCreateHOGFeatsofHands = true;%could not be loaded
            end
            if runCreateHOGFeatsofHands
                if isempty(hogImArr_cell)
                    disp(['<' hogCropFeatsNameFull '> could not be loaded - hence hogCropFeats will be created from scratch.']);
                elseif size(hogImArr_cell,2) == 2
                    disp(['<' hogCropFeatsNameFull '> loaded but bothHands will be added.']);
                end
            %else
            %    return%could be loaded
            end
        case {0,1}
            disp(['<' hogCropFeatsNameFull '> doesnt exist hence hogCropFeats will be created from scratch.']); 
    end
end

function [hogCropFeatsNameFull, backupFileName] = getBackupFile(userFolderName, hogVersion, enforceCreateNew)
    
    switch hogVersion
        case 1
            hog_crop_str = 'hog_crop';
        case 2
            hog_crop_str = 'hog_crop_v2';
    end
    
    hogCropFeatsNameFull = [userFolderName filesep hog_crop_str '.mat'];

    backupFold = getVariableByComputerName('backupFold');
    backupFold = [backupFold filesep 'Backup_Feats'];
    foldCells = strsplit(userFolderName, filesep);
    userCells = strsplit(foldCells{1,end}, '_');
    try
        backupFileName = [backupFold filesep hog_crop_str '_s' pad(foldCells{1,end-1},3,'left','0') '_u' userCells{1,2} '_r' pad(userCells{1,end},2,'left','0') '.mat'];
    catch
        backupFileName = [backupFold filesep hog_crop_str '_s' num2str(str2double(foldCells{1,end-1}),'%03d') '_u' userCells{1,2} '_r' num2str(str2double(userCells{1,end}),'%02d') '.mat'];
    end
    fileExist = exist(hogCropFeatsNameFull,'file')>0;
    backupFileExist = exist(backupFileName,'file')>0;
    if (fileExist==true && enforceCreateNew) || (fileExist==false && backupFileExist==true)  
        copyfile(backupFileName,hogCropFeatsNameFull,'f');
        disp(['skipping creation of ' hog_crop_str ' from(' userFolderName '); it is retrieved from backup']);
    elseif fileExist
        disp(['skipping creation of ' hog_crop_str '. It will be loaded from(' hogCropFeatsNameFull ')']);
    end
end