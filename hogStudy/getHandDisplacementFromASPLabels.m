function [ handDisplacementStruct, handLabelsStruct ] = getHandDisplacementFromASPLabels(signSourceFolder, signID, userID, signRepeatID, handDisplacementMat, makeEmptyFrames)

    if ~exist('makeEmptyFrames','var') || isempty(makeEmptyFrames)
        makeEmptyFrames = true;
    end
    signSourceFolder_X = [signSourceFolder filesep num2str(signID) filesep 'User_' num2str(userID) '_' num2str(signRepeatID)];
    labelsFolder = [signSourceFolder_X filesep 'labelFiles' filesep];
    imCnt = size(handDisplacementMat.LH,2);
    
    [labelsVec_LH, displacementVec_LH] = getLabelVecFromFile([labelsFolder 'labelsCS_LH.txt'],makeEmptyFrames,imCnt);
    [labelsVec_RH, displacementVec_RH] = getLabelVecFromFile([labelsFolder 'labelsCS_RH.txt'],makeEmptyFrames,imCnt);
    [labelsVec_BH, displacementVec_BH] = getLabelVecFromFile([labelsFolder 'labelsCS_BH.txt'],makeEmptyFrames,imCnt);
    imCnt = length(labelsVec_LH);
    
    if ~exist('handDisplacementMat','var') || isempty(handDisplacementMat)
        handDisplacementStruct = struct;
        handDisplacementStruct.LH = zeros(1,imCnt);
        handDisplacementStruct.RH = zeros(1,imCnt);
        handDisplacementStruct.BH = zeros(1,imCnt);
        handDisplacementStruct.handsAreTogether = false(1,imCnt);
    else
        handDisplacementStruct = handDisplacementMat;
    end
    handLabelsStruct = struct;
    handLabelsStruct.LH = zeros(1,imCnt);
    handLabelsStruct.RH = zeros(1,imCnt);
    handLabelsStruct.BH = zeros(1,imCnt);
    
    handLabelsStruct.LH = labelsVec_LH;
    handLabelsStruct.RH = labelsVec_RH;
    handLabelsStruct.BH = labelsVec_BH;
    handDisplacementStruct.LH = displacementVec_LH;
    handDisplacementStruct.RH = displacementVec_RH;
    handDisplacementStruct.BH = displacementVec_BH;
    handDisplacementStruct.handsAreTogether = labelsVec_BH~=0;
end

