function LRBChar = getLRBChar(i)
    switch i
        case 1
            LRBChar = 'L';
        case 2
            LRBChar = 'R';
        case 3
            LRBChar = 'B';
    end
end