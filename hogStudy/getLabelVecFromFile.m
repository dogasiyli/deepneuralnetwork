function [labelsVec, displacementVec] = getLabelVecFromFile(fullFileName, makeEmptyFrames, imCnt)
    if ~exist('makeEmptyFrames','var') || isempty(makeEmptyFrames)
        makeEmptyFrames = true;
    end
    labelsVec = inf(1,1000);
    labelCnt = 1;
    fid = fopen(fullFileName);
    tline = fgets(fid);
    while ischar(tline)
        labelsVec(labelCnt) = str2double(tline);
        tline = fgets(fid);
        labelCnt = labelCnt +1;
    end
    fclose(fid);
    labelsVec(labelsVec==inf) = [];
    assert(labelCnt-1==length(labelsVec),'sth wrong');
    
    if exist('imCnt','var') && imCnt>labelCnt-1
        difFrameCnt = imCnt - labelCnt + 1;
        disp(['file(' fullFileName ') has ' num2str(difFrameCnt) ' less labels hence final label(' num2str(labelsVec(end)) ') is appended to assigned labels']);
        labelsVec = [labelsVec labelsVec(end)*ones(1,difFrameCnt)];
        labelCnt = imCnt;
    end
    
    if makeEmptyFrames
        for i=1:labelCnt-2
            if (labelsVec(i)~=labelsVec(i+1) && labelsVec(i)>0 && labelsVec(i+1)>0)
                disp(['frameID(' num2str(i) '-c(' num2str(labelsVec(i)) ')) is set to zero to leave an empty space between 2 different signs'])
                labelsVec(i) = 0;
            end
        end
    end
    
    displacementVec = zeros(size(labelsVec));
    displacementVec(labelsVec==0) = 100;
end