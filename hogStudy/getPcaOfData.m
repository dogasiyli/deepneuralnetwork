function getPcaOfData(X, neededDims, baseFolderToSave, baseFileNameToSave, labelVecs_all, knownKHSlist)
    X(isnan(X)) = 0;
	[coeff,score,latent,tsquared,explained,mu] = pca(X);
	infoVal = cumsum(explained)./sum(explained);
	for i = 1:length(neededDims)
        dataCurdim = score(:,1:neededDims(i));
        infoCurDim = infoVal(neededDims(i));
        try
            save([baseFolderToSave filesep baseFileNameToSave '_pca_' num2str(neededDims(i)) '.mat'],'dataCurdim', 'infoCurDim','labelVecs_all', 'knownKHSlist');
        catch
            save([baseFolderToSave filesep baseFileNameToSave '_pca_' num2str(neededDims(i)) '-v7.3.mat'],'dataCurdim', 'infoCurDim','labelVecs_all', 'knownKHSlist', '-v7.3');
        end
    end
end