function [ precalcedHOGImCropVecs ] = getPrecalcedHOGImCropVecs( srcFold,  signIDs, userIDs, signRepeatIDs, hogVersion)
    precalcedHOGImCropVecs = struct;

    OPS = struct('hogVersion',hogVersion,'enforceCreateNew',false,'srcFold',srcFold);
    
    [hogImArr_cell, imcropVecVariable] = getHOGFeatsofHands(signIDs(1), userIDs(1), signRepeatIDs(1), OPS);
    precalcedHOGImCropVecs.hogImArr_cell_X = hogImArr_cell;
    precalcedHOGImCropVecs.imcropVec_cell_X = imcropVecVariable;
    
    [hogImArr_cell, imcropVecVariable] = getHOGFeatsofHands(signIDs(2), userIDs(2), signRepeatIDs(2), OPS);
    precalcedHOGImCropVecs.hogImArr_cell_Y = hogImArr_cell;
    precalcedHOGImCropVecs.imcropVec_cell_Y = imcropVecVariable;
end