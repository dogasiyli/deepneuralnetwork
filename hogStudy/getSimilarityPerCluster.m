function [clusterProbsVec_X, bestMatchFineCrop, knownClusterNameDefault] = getSimilarityPerCluster(clustersStruct, hogFeat_X, minClusterCntPropose, knownClusterNameTresh)
%getSimilarityPerCluster compare a hog feature accross the clusters and get
%the results back

% This function gets a hog feature and compares it to the clusterStruct,
% main job is done in <findBestClusterIDVec>,
% this function just retrieves the image and name of the cluster center(s)
% found
    if ~exist('minClusterCntPropose','var')
        minClusterCntPropose=inf;
    end
    if ~exist('knownClusterNameTresh', 'var') || isempty(knownClusterNameTresh)
        knownClusterNameTresh = 0.8;
    end
    bestMatchFineCrop = [];
    knownClusterNameDefault = '';
    if ~isempty(clustersStruct) && isstruct(clustersStruct)
        curClusterNamesCell = clustersStruct.definition.clusterNames;
        definedClusterCount = size(curClusterNamesCell,1);
        clusterCountToPropose = min(minClusterCntPropose, definedClusterCount);
        bestMatchFineCrop = cell(clusterCountToPropose,1);
        
        %what we have in clusterProposalStruct??
        %1. clusterProposalStruct.bestCropStruct.hogVec - 900x2
        %2. clusterProposalStruct.bestCropStruct.imCropped - 1x2 cell - 200x200x3 uint8
        %3. clusterProposalStruct.bestCropStruct.imFineCropped - 1x2 cell - D by D uint8
        
        %we can get hog features of imCropped and pass as the most
        %important hog fetaures to guess cluster - can be more accurate
        %or the already selected hogVec as 2 hog - fast
        
        %hogFeat_X = clusterProposalStruct.bestCropStruct.hogVec(:,1)';
        [clusterSuitabilityVec_X, minDistClusterInds_X, clusterProbsVec_X] = findBestClusterIDVec(clustersStruct, hogFeat_X); %#ok<ASGLU>
        
        %if both X and Y are assigned to same cluster as the first guesses
        %write the cluster name as the knownClusterName
        if max(clusterProbsVec_X(:,end))>knownClusterNameTresh
            knownClusterNameDefault = clustersStruct.definition.clusterNames{clusterProbsVec_X(1,1)};
        end
        for j = 1:clusterCountToPropose
            bestMatchFineCrop{j} = clustersStruct.definition.subClusterImages{clusterProbsVec_X(1,1),clusterProbsVec_X(1,2)};
        end
    end
end

