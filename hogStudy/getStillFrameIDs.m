function [stillFrameIDs, labelCells] = getStillFrameIDs(handDisplacementMat, labelCells, knownFrameCount)
%getStillFrameIDs check the hand displacement vectors and given labels(if
%exist) and try to report the still frames not to deal with them to save
%time
%   Check for single hands - hand displacement and given still frame labels
%   If they agree on 0's on displacement and 1 on label then assign as
%   still frame
%   Then if both hands agree on being still then bothHands is also a still
%   frame.
    stillFrameIDs = struct;
    
    %frameCount_handDisp   = length(handDisplacementMat.LH);
    %frameCount_labelsCell = length(labelCells{1});
    %frameCounts_Equal = (frameCount_handDisp==frameCount_labelsCell);
    if exist('knownFrameCount','var')
        labelCells = trickToKnownLabelCount(labelCells, knownFrameCount);
    end
    
    stillFrameIDs.LH = fwf_getStillFrameIDsOfSingleHand(handDisplacementMat.LH, labelCells{1});
    stillFrameIDs.RH = fwf_getStillFrameIDsOfSingleHand(handDisplacementMat.RH, labelCells{2});
    stillFrameIDs_single = stillFrameIDs.LH==1 & stillFrameIDs.RH==1;
    stillFrameIDs.BH = fwf_getStillFrameIDsOfBothHands(stillFrameIDs_single, labelCells{3});
end

function labelCells = trickToKnownLabelCount(labelCells, knownFrameCount)
    for i=1:length(labelCells)
        labelVec = labelCells{i};
        if length(labelVec)<knownFrameCount
            labelVec(length(labelVec):knownFrameCount) = labelVec(end);
        elseif length(labelVec)>knownFrameCount   
            labelVec = labelVec(1:knownFrameCount);
        end
        labelCells{i} = labelVec;
    end
end

function stillFrameVec = fwf_getStillFrameIDsOfSingleHand(handDisplacementVec, labelVec)
    still_handDisp = handDisplacementVec==0;
    beginStill_to = find(1-still_handDisp,1,'first')-1;
    endStill_from = length(still_handDisp)-find(1-still_handDisp(end:-1:1),1,'first')+2;
    still_handDisp = false(size(still_handDisp));
    still_handDisp(1:beginStill_to) = true;
    still_handDisp(endStill_from:end) = true;
    if isempty(labelVec)
        stillFrameVec = still_handDisp;
    else
        still_labels   = labelVec==1;
        stillFrameVec = still_handDisp==1 & still_labels==1;
    end
end

function stillFrameVec = fwf_getStillFrameIDsOfBothHands(stillFrameIDs_single, labelVec)
    if isempty(labelVec)
        stillFrameVec = stillFrameIDs_single==1;
    else
        stillFrameVec = stillFrameIDs_single==1 & labelVec==1;
    end
end