%OPS = struct('figID', 1,'rejectPatchMethod', 'none','normCalcMethod', 'haar-like','changeGivenValues', false,'cropAreaStrCell', {'BH_sml','RH_sml','LH_sml'});
%[croppedPartsCells, valsCellsofStruct] = getSurfNorm(srcFolder, frameID, blockCnts, struct())
function [faceNormCells, quiverMatCells, blockAreaSizeCells, rgbImgAsFaceNormCells, m, quiverH_out] = getSurfNorm(srcFolder, frameID, blockCnts, optionalParamsStruct)

%% 0. assign unpassed optional params
    cropAreaStrCells = getOptionalParamsFromStruct(optionalParamsStruct, 'cropAreaStrCell', {'BH_sml','RH_sml','LH_sml'}, true);
    changeGivenValuesCells = getOptionalParamsFromStruct(optionalParamsStruct, 'changeGivenValues', false, true);%gridFit also changes the given values at points - if this is false thias algorithm will not change those values
    normCalcMethodCells = getOptionalParamsFromStruct(optionalParamsStruct, 'normCalcMethod', 'haar-like', true);
    rejectPatchMethodCells = getOptionalParamsFromStruct(optionalParamsStruct, 'rejectPatchMethod', 'none', true);
    figID = getOptionalParamsFromStruct(optionalParamsStruct, 'figID', 1, true);
    stepsOfFillPrint = getOptionalParamsFromStruct(optionalParamsStruct, 'stepsOfFillPrint', 2, true);
    drawMethod = getOptionalParamsFromStruct(optionalParamsStruct, 'drawMethod', 2, true);
    if iscell(figID)
        figID = figID{1,1};
    end
    if ~isempty(figID) && stepsOfFillPrint>0
        stepsOfFillPrint_figID = figID+1;
        figure(figID+1);clf;
    elseif stepsOfFillPrint>0
        stepsOfFillPrint_figID = 1;
        figure(1);clf;
    else
        stepsOfFillPrint_figID = -1;
    end
    if drawMethod==3
        stepsOfFillPrint_figVec = [2 2];
    else
        stepsOfFillPrint_figVec = [2 3];
    end

    differentCropBlocksCnt = length(cropAreaStrCells);
    quiverH_out = cell(differentCropBlocksCnt,1);
%% 1. Load necessary variables from file    
    [depthIm, skeleton] = func01_loadData(srcFolder, frameID);
   
%% 2. Get the necessary bounding box
    croppedPartsCells = func02_getCroppedArea(depthIm, frameID, skeleton, cropAreaStrCells);
    if stepsOfFillPrint>0 && drawMethod~=3
        subplot(stepsOfFillPrint_figVec(1),stepsOfFillPrint_figVec(2),1);
        handChars = {'B','R','L'};
        a = imread([srcFolder filesep 'cropped' filesep 'crop_' handChars{stepsOfFillPrint} 'H_' num2str(frameID,'%03d') '.png']);
        imshow(a);
        title('a) rgb image');
    end
    
%% 3. Deal with faulty pixels - bigger than 1900 - farther than 1.9 meters    
    croppedPartsCells = func03_FaultyPixelsOperations(croppedPartsCells);   
    if stepsOfFillPrint>0
        if drawMethod==3
            subplot(stepsOfFillPrint_figVec(1),stepsOfFillPrint_figVec(2),1);
            imagesc(croppedPartsCells{stepsOfFillPrint});colorbar;
            xticks([]);
            yticks([]);         
        else
            subplot(stepsOfFillPrint_figVec(1),stepsOfFillPrint_figVec(2),2);
            imagesc(croppedPartsCells{stepsOfFillPrint});colorbar;
            title('b) zero values are missing');
        end       
    end
%% 4. Fill the 0's of croppedParts
    [croppedPartsCells, valsCellsofStruct] = func04_fill0s(croppedPartsCells, changeGivenValuesCells);
    if stepsOfFillPrint>0
        if drawMethod~=3
            subplot(stepsOfFillPrint_figVec(1),stepsOfFillPrint_figVec(2),3);
            imagesc(croppedPartsCells{stepsOfFillPrint});colorbar;
            title('c) after gridFit');
        else
            subplot(stepsOfFillPrint_figVec(1),stepsOfFillPrint_figVec(2),2);
            imagesc(croppedPartsCells{stepsOfFillPrint});colorbar;
            xticks([]);
            yticks([]);         
        end
    end
%% 5.
    grpIDs_CellsofStruct = cell(differentCropBlocksCnt,1);
    imColors_CellsofStruct = cell(differentCropBlocksCnt,1);
    imVals_CellsofStruct = cell(differentCropBlocksCnt,1);
    groupBegEnds_CellsofStruct = cell(differentCropBlocksCnt,1);
    faceNormCells = cell(differentCropBlocksCnt,1);
    quiverMatCells = cell(differentCropBlocksCnt,1);
    blockAreaSizeCells = cell(differentCropBlocksCnt,1);
    rgbImgAsFaceNormCells = cell(differentCropBlocksCnt,1);
    for i=1:differentCropBlocksCnt
        [imColors_CellsofStruct{i}, grpIDs_CellsofStruct{i}, ~, imVals_CellsofStruct{i}, groupBegEnds_CellsofStruct{i}] = setRGBKmeans(croppedPartsCells{i});
        OPS = struct('pixelGroupIDs',grpIDs_CellsofStruct{i},'normCalcMethod',normCalcMethodCells,'rejectPatchMethod',rejectPatchMethodCells);
        [faceNormCells{i}, quiverMatCells{i}, blockAreaSizeCells{i}, rgbImgAsFaceNormCells{i}] = calcFaceNormsOfImage(croppedPartsCells{i}, blockCnts, OPS);        
    end
    
%% 6. draw the result
    if ~isempty(figID)
        rowCnt = differentCropBlocksCnt;
        colCnt = 4;
          
        %1- draw the given image to the left half of the plot
        initiateFigure(figID, true);
        initiateFigure([figID rowCnt colCnt], true, 1:colCnt:rowCnt*(colCnt-1));
        imagesc(depthIm);title(['The whole body image - frameID(' num2str(frameID) ')']);
        for rowID=1:differentCropBlocksCnt
            subFigID = colCnt*(rowID-1) + 2;
            initiateFigure([figID rowCnt colCnt subFigID], [false true]);
            imagesc(croppedPartsCells{rowID});title(['Cropped part(' cropAreaStrCells{rowID} ')']);
            
            subFigID = colCnt*(rowID-1) + 3;
            titleParams = struct('blockCnts',blockCnts,'blockAreaSize', blockAreaSizeCells{rowID},'rejectPatchMethod',rejectPatchMethodCells,'rejectStr',rejectPatchMethodCells);
            
            quiverH_out{rowID} = struct;
            quiverH_out{rowID}.cropImFilled = croppedPartsCells{rowID};
            quiverH_out{rowID}.quiverMat = quiverMatCells{rowID};
            visualizeNormals(croppedPartsCells{rowID}, quiverMatCells{rowID}, titleParams, struct('figVec', [figID rowCnt colCnt subFigID]));
            if stepsOfFillPrint==rowID
                if drawMethod~=3
                    subplot(stepsOfFillPrint_figVec(1),stepsOfFillPrint_figVec(2),4);
                    plot(imVals_CellsofStruct{i}); hold on;
                    ylabel('sorted depth values')
                    ylim([min(imVals_CellsofStruct{i}) max(imVals_CellsofStruct{i})]);
                    xticks([])
                    title("d) sorted depth values");
                    plot([groupBegEnds_CellsofStruct{i}(1,2) groupBegEnds_CellsofStruct{i}(1,2)], [min(imVals_CellsofStruct{i}) max(imVals_CellsofStruct{i})], 'r');
                    plot([groupBegEnds_CellsofStruct{i}(2,2) groupBegEnds_CellsofStruct{i}(2,2)], [min(imVals_CellsofStruct{i}) max(imVals_CellsofStruct{i})], 'b');
                end
                
                if drawMethod~=3
                    subplot(stepsOfFillPrint_figVec(1),stepsOfFillPrint_figVec(2),5);
                    imshow(imColors_CellsofStruct{stepsOfFillPrint});
                    title("e) background-body-hand discrimination");
                else
                    subplot(stepsOfFillPrint_figVec(1),stepsOfFillPrint_figVec(2),3);
                    imshow(imColors_CellsofStruct{stepsOfFillPrint});
                    xticks([]);
                    yticks([]);         
                end
                
                if drawMethod~=3
                    visualizeNormals(croppedPartsCells{rowID}, quiverMatCells{rowID}, "f) SNV visualization ", struct('figVec', [stepsOfFillPrint_figID stepsOfFillPrint_figVec(1:2) 6]));
                else
                    visualizeNormals(croppedPartsCells{rowID}, quiverMatCells{rowID}, "", struct('figVec', [stepsOfFillPrint_figID stepsOfFillPrint_figVec(1:2) 4],'showColorBar', false));
                    xticks([]);
                    yticks([]);                    
                end
            end      
            
            subFigID = colCnt*(rowID-1) + 4;
            initiateFigure([figID rowCnt colCnt subFigID], [false true]);
            image(rgbImgAsFaceNormCells{rowID});title('Norms as rgb');
        end
    end
    m = [];
    if nargout >= 5 && ~isempty(figID)
        try %#ok<TRYNC>
            m = frame2im(getframe(gcf));
        end
    end
end

function [depthIm, skeleton] = func01_loadData(srcFolder, imageID)
    depth = [];
    skeleton = [];    
    load([srcFolder filesep 'depth.mat']);
    load([srcFolder filesep 'skeleton.mat']);
    depthIm = depth(:,:,imageID);
end

function croppedParts = func02_getCroppedArea(depthIm, frameID, skeleton, areaStrCell)
    cropArea = getCropArea(skeleton,[],struct('colDepStr', 'depth'));
    numOfCroppedParts = length(areaStrCell);
    croppedParts = cell(numOfCroppedParts,1);
    for i=1:numOfCroppedParts
        croppedParts{i} = func02_h01_getGroppedPart(depthIm, cropArea, frameID, areaStrCell{i});
    end
end
function croppedPart = func02_h01_getGroppedPart(depthIm, cropArea, frameID, areaStr) %#ok<INUSL>
    %areaStr -->
    %TS_big : 'torso'
    %FC_big : 'face'
    %BH_big : 'Both Hands Big Area'
    %LH_big : 'Left Hand Big Area'
    %RH_big : 'Right Hand Big Area'
    %BH_sml : 'Both Hands Small Area'
    %LH_sml : 'Left Hand Small Area'
    %RH_sml : 'Right Hand Small Area'
    areaSlctd = eval(['cropArea.' areaStr '(frameID,1:4);']);
    croppedPart = cropBodyPart(depthIm, areaSlctd(1:2), areaSlctd(3:4));
end

function [depthImCells] = func03_FaultyPixelsOperations(depthImCells)
    imgCnt = length(depthImCells);
    for i=1:imgCnt
        depthImCells{i}(depthImCells{i}>1900) = 0;
    end
end

function [depthImCells, valsCellsofStruct] = func04_fill0s(depthImCells, changeGivenValuesBool)
    imgCnt = length(depthImCells);
    valsCellsofStruct = cell(imgCnt,1);
    for i=1:imgCnt
        [depthImCells{i}, valsCellsofStruct{i}] = fill0s_gridFit(depthImCells{i}, changeGivenValuesBool);
    end
end
