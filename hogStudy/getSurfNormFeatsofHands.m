%function [surfImArr_cell, imcropVec_cell] = getSurfNormFeatsofHands(sourceFolder, signID, userID, signRepeatID, imIDBegEnd, enforceCreateNew, dominantHand)
function [surfImArr_cell, imcropVec_cell] = getSurfNormFeatsofHands(surVec, OPT)
%enforceCreateNew, dominantHand)

%getSurfNormFeatsofHands for a video crop hands in a multi-focus fashion
%and create different focused surf norm results
%   Detailed explanation goes here

%     imIDBegEnd = [1 105];
%     sourceFolder = '/media/doga/Data/FromUbuntu/Doga';
%     signID = 112;
%     userID = 2;
%     signRepeatID = 2;

%[surfImArr_cell, imcropVec_cell] = getSurfNormFeatsofHands([7, 2, 1]);
%[surfImArr_cell, imcropVec_cell] = getSurfNormFeatsofHands([112, 2, 3], struct('srcFold',srcFold,'imIDBegEnd',[1 102]));
%[surfImArr_cell, imcropVec_cell] = getSurfNormFeatsofHands([112, 2, 3], struct('skipStillFrames', true,'skipBothHands', true));

    if ~exist('OPT', 'var') || ~isstruct(OPT)
        OPT = [];
    end
    sourceFolder       = getStructField(OPT, 'srcFold', getVariableByComputerName('srcFold'));
    imIDBegEnd         = getStructField(OPT, 'imIDBegEnd', []);
    enforceCreateNew   = getStructField(OPT, 'enforceCreateNew', false);
    dominantHand       = getStructField(OPT, 'dominantHand', 'LH');
    skipStillFrames    = getStructField(OPT, 'skipStillFrames', true);
    skipBothHands      = getStructField(OPT, 'skipBothHands', true);
    saveAtEach         = getStructField(OPT, 'saveAtEach', 60);
    leaveUnfilledCells = getStructField(OPT, 'leaveUnfilledCells', false);
    scales2Use         = getStructField(OPT, 'scales2Use', 1:-0.10:0.70);
    makeEmptyFrames    = getStructField(OPT, 'makeEmptyFrames', true);
    verbose_level      = getStructField(OPT, 'verbose_level', 0);%0-only if necessary,1-

    signID          = surVec(1);
    userID          = surVec(2);
    signRepeatID    = surVec(3);
    

    handInfo = struct;
    handInfo.handDominant = dominantHand;

    strideFixed = 3;
    blockCounts = [20 20];%[nwin_x nwin_y]

    
    userFolderName = [sourceFolder filesep num2str(signID) filesep 'User_' num2str(userID) '_' num2str(signRepeatID)];
    [surfNormCropFeatsNameFull, surfNormCropBackupFileName] = getBackupFile(userFolderName, enforceCreateNew, verbose_level);
    %00-file not exist, no enforce create
    %01-file not exist, enforce create
    %10-file exist, and no enforce create
    %11-file exist, but enforce create
    if exist(surfNormCropFeatsNameFull,'file') && enforceCreateNew %11-file exist, but enforce create
        disp(['Although <' surfNormCropFeatsNameFull '> exist, surfNormCropFeats will be created from scratch.']);
    elseif ~exist(surfNormCropFeatsNameFull,'file')%0?-file not exist, no need to check enforce create
        disp(['<' surfNormCropFeatsNameFull '> doesnt exist hence surfNormCropFeats will be created from scratch.']);        
    elseif exist(surfNormCropFeatsNameFull,'file') && ~enforceCreateNew %10-file exist, and no enforce create
        if verbose_level>3
            disp(['<' surfNormCropFeatsNameFull '> exists hence surfNormCropFeats will be loaded.']);
        end
        try
            load(surfNormCropFeatsNameFull, 'surfImArr_cell', 'imcropVec_cell');
            runCreatesurfNormCropFeatsofHands = ~exist('surfImArr_cell','var') || ~exist('imcropVec_cell','var') || size(surfImArr_cell,2)<3 || size(imcropVec_cell,2)<3;
        catch
            runCreatesurfNormCropFeatsofHands = true;%could not be loaded
        end
        unfilledCells = cellfun(@isempty,surfImArr_cell)==1;
        unfilledCellCount = sum(unfilledCells(:));
        allCellsAreFilled = unfilledCellCount==0;
        if runCreatesurfNormCropFeatsofHands
             disp(['<' surfNormCropFeatsNameFull '> could not be loaded - hence surfNormCropFeats will be created from scratch.']);
        elseif allCellsAreFilled
            return%could be loaded
        elseif ~allCellsAreFilled && leaveUnfilledCells
            disp(['Although <' surfNormCropFeatsNameFull '> have unfilledCells, leaveUnfilledCells is true and skipping the filling operation.']);
            return%could be loaded
        elseif verbose_level>2
            disp(['<' surfNormCropFeatsNameFull '> loaded but there are ' num2str(unfilledCellCount) ' empty cells to be completed.']);
        end
    end
    
    depth = [];
    skeleton = [];    
    load([userFolderName filesep 'depth.mat']);
    load([userFolderName filesep 'skeleton.mat']);
    
    frameCount = size(depth,3);
    
    %videoFileNameFull = [userFolderName filesep 'color.mp4'];
    %v = VideoReader(videoFileNameFull);
    %videoMat = read(v,[1 Inf]); %#ok<VIDREAD>
    %frameCount = size(videoMat,4);
    %clear v videoMat
    
    if ~exist('imIDBegEnd', 'var') || isempty(imIDBegEnd)
        imIDBegEnd = [1 frameCount];
    end
    imIDBegEnd(2) = min(imIDBegEnd(2), frameCount);
    
    cellCnt = imIDBegEnd(2)-imIDBegEnd(1)+1;
    
    if skipStillFrames
        %we will grab the labelFiles if they are there
        %if any label file is missing the cell will be empty
        load([userFolderName filesep 'handUsage.mat'], 'handDisplacementMat');
        [someMissing, labelFilesExist, labelCells] = checkLabelFiles(userFolderName, 2, makeEmptyFrames);
        [stillFrameIDs, labelCells] = getStillFrameIDs(handDisplacementMat, labelCells, cellCnt);
    else
        stillFrameIDs = struct;
        stillFrameIDs.LH = zeros(1,cellCnt);
        stillFrameIDs.RH = zeros(1,cellCnt);
        stillFrameIDs.BH = zeros(1,cellCnt);
    end
    
    if enforceCreateNew || ~exist('surfImArr_cell','var') || size(surfImArr_cell,2)<3 || size(surfImArr_cell,1)~=cellCnt
        surfImArr_cell = cell(cellCnt,3);%1-Left, 2-Right, 3-Both Hands
        imcropVec_cell = cell(cellCnt,3);%1-Left, 2-Right, 3-Both Hands
        unfilledCells = true(cellCnt,3);
    end
    
    leftCellsToFill = unfilledCells(:,1)'==true;
    rightCellsToFill= unfilledCells(:,2)'==true;
    bothCellsToFill = unfilledCells(:,3)'==true;
    
    if skipStillFrames && skipBothHands
        unfilledCellCount_LH = sum(stillFrameIDs.LH==0 & leftCellsToFill==1);
        unfilledCellCount_RH = sum(stillFrameIDs.RH==0 & rightCellsToFill==1);
        unfilledCellCount_BH = 0;
    elseif skipStillFrames 
        unfilledCellCount_LH = sum(stillFrameIDs.LH==0 & leftCellsToFill==1);
        unfilledCellCount_RH = sum(stillFrameIDs.RH==0 & rightCellsToFill==1) ;
        unfilledCellCount_BH = sum(stillFrameIDs.BH==0 & bothCellsToFill==1);
    elseif skipBothHands
        unfilledCellCount_LH = sum(leftCellsToFill==1);
        unfilledCellCount_RH = sum(rightCellsToFill==1);
        unfilledCellCount_BH = 0;
    else
        unfilledCellCount_LH = sum(leftCellsToFill==1);
        unfilledCellCount_RH = sum(rightCellsToFill==1);
        unfilledCellCount_BH = sum(bothCellsToFill==1);
    end
    unfilledCellCount = unfilledCellCount_LH + unfilledCellCount_RH + unfilledCellCount_BH;
    unfilledCellCount_init = unfilledCellCount;
    
%     hogImArr_4d = [];%1-Left, 2-Right, 3-Both Hands
%     imcropVec_4d = [];%1-Left, 2-Right, 3-Both Hands
    tic;
    tocCur = toc;
    if verbose_level>2
        disp(['getSurfNormFeatsofHands - s' num2str(signID) '_u' num2str(userID) '_r' num2str(signRepeatID) ' : ' ...
              'LH(' num2str(unfilledCellCount_LH) ' of ' num2str(sum(leftCellsToFill==1)) '),' ...
              'RH(' num2str(unfilledCellCount_RH) ' of ' num2str(sum(rightCellsToFill==1)) '),' ...
              'BH(' num2str(unfilledCellCount_BH) ' of ' num2str(sum(bothCellsToFill==1)) '),']);
    end
    if verbose_level>0
        dispstat('','init');
    end
    cellCountToFillCounter = 0;
    fileSaveCounter = 0;
    alreadySaved = 0;
    for i = 1:cellCnt
        imID = i + imIDBegEnd(1) - 1;
        
        depthIm = depth(:,:,imID);
        depthIm(depthIm>1900) = 1900;   
        cropArea = getCropArea(skeleton,imID,struct('colDepStr', 'depth'));
        
        im_lh_croppedPart = cropBodyPart(depthIm, cropArea(1,1:2), cropArea(1,3:4));%LH_big
        im_rh_croppedPart = cropBodyPart(depthIm, cropArea(2,1:2), cropArea(2,3:4));%RH_big
        im_bh_croppedPart = cropBodyPart(depthIm, cropArea(3,1:2), cropArea(3,3:4));%BH_big
        
%         handInfo.handCur = 'LH';
%         [surfNormImArr_lh, imcropVec_lh, dpthImPalette_lh, surfRGBImPalette_lh] = extrapolateImForSurfNormComp(im_lh_croppedPart, scales2Use, strideFixed, blockCounts, handInfo);
%         handInfo.handCur = 'RH';
%         [surfNormImArr_rh, imcropVec_rh, dpthImPalette_rh, surfRGBImPalette_rh] = extrapolateImForSurfNormComp(im_rh_croppedPart, scales2Use, strideFixed, blockCounts, handInfo);
%         handInfo.handCur = 'BH';
%         [surfNormImArr_bh, imcropVec_bh, dpthImPalette_bh, surfRGBImPalette_bh] = extrapolateImForSurfNormComp(im_bh_croppedPart, scales2Use, strideFixed, blockCounts, handInfo);
        tocPrev = tocCur;
        
        surfNormImArr_lh = surfImArr_cell{i,1};
        imcropVec_lh = imcropVec_cell{i,1};
        goForLeft = ~(skipStillFrames==true && stillFrameIDs.LH(i)==1);
        goForLeft = goForLeft && (isempty(surfNormImArr_lh) || isempty(imcropVec_lh));
        if goForLeft
            handInfo.handCur = 'LH';
            [surfNormImArr_lh, imcropVec_lh] = extrapolateImForSurfNormComp(im_lh_croppedPart, scales2Use, strideFixed, blockCounts, handInfo);
            surfImArr_cell{i,1} = surfNormImArr_lh;
            imcropVec_cell{i,1} = imcropVec_lh;
            cellCountToFillCounter = cellCountToFillCounter + 1;
            fileSaveCounter = fileSaveCounter + 1;
        end
        
        surfNormImArr_rh = surfImArr_cell{i,2};
        imcropVec_rh     = imcropVec_cell{i,2};
        goForRight = ~(skipStillFrames==true && stillFrameIDs.RH(i)==1);
        goForRight = goForRight && (isempty(surfNormImArr_rh) || isempty(imcropVec_rh));
        if goForRight
            handInfo.handCur = 'RH';
            [surfNormImArr_rh, imcropVec_rh] = extrapolateImForSurfNormComp(im_rh_croppedPart, scales2Use, strideFixed, blockCounts, handInfo);
            surfImArr_cell{i,2} = surfNormImArr_rh;
            imcropVec_cell{i,2} = imcropVec_rh;
            cellCountToFillCounter = cellCountToFillCounter + 1;
            fileSaveCounter = fileSaveCounter + 1;
        end
        
        surfNormImArr_bh = surfImArr_cell{i,3};
        imcropVec_bh     = imcropVec_cell{i,3};
        goForBoth = ~skipBothHands && (isempty(surfNormImArr_bh) || isempty(imcropVec_bh));
        if goForBoth
            handInfo.handCur = 'BH';
            [surfNormImArr_bh, imcropVec_bh] = extrapolateImForSurfNormComp(im_bh_croppedPart, scales2Use, strideFixed, blockCounts, handInfo);
            surfImArr_cell{i,3} = surfNormImArr_bh;
            imcropVec_cell{i,3} = imcropVec_bh;
            cellCountToFillCounter = cellCountToFillCounter + 1;
            fileSaveCounter = fileSaveCounter + 1;
        end
        tocCur = toc;
        
%         if (mod(i,10)==1 || i==cellCnt)
%             figure(99);clf;
%             subplot(3,2,1);imagesc(dpthImPalette_lh);
%             subplot(3,2,2);image(surfRGBImPalette_lh);
%             subplot(3,2,3);imagesc(dpthImPalette_rh);
%             subplot(3,2,4);image(surfRGBImPalette_rh);
%             subplot(3,2,5);imagesc(dpthImPalette_bh);
%             subplot(3,2,6);image(surfRGBImPalette_bh);
%         end        
        tocCurStr = getTimeAsString(tocCur-tocPrev);
        tocTillNowStr = getTimeAsString(tocCur);
        expSec = (unfilledCellCount-cellCountToFillCounter)*(tocCur/cellCountToFillCounter);
        if cellCountToFillCounter>0
            tocExpStr = getTimeAsString(expSec);
        else
            tocExpStr = 'unknown';
        end

        if i == cellCnt && fileSaveCounter>0 && verbose_level>0
            dispstat(sprintf('saved(%d),f(%d) of %d is analyzed --tocCur(%s),tillNow(%s),expTime(%s)\n',alreadySaved,cellCountToFillCounter,unfilledCellCount,tocCurStr,tocTillNowStr,tocExpStr),'keepthis');
        elseif (goForLeft || goForRight || goForBoth) && verbose_level>0
            dispstat(sprintf('saved(%d),f(%d) of %d is analyzed --tocCur(%s),tillNow(%s),expTime(%s)\n',alreadySaved,cellCountToFillCounter,unfilledCellCount,tocCurStr,tocTillNowStr,tocExpStr));
        end
        if (fileSaveCounter>=saveAtEach)
            alreadySaved = alreadySaved + fileSaveCounter;
            dispstat(sprintf('Saving [surfImArr_cell,imcropVec_cell] to file(%s)\n',surfNormCropFeatsNameFull));
            save(surfNormCropFeatsNameFull, 'surfImArr_cell', 'imcropVec_cell', '-v7.3');
            dispstat(sprintf('Saved %d of %d into [surfImArr_cell,imcropVec_cell] to file(%s) - counters resetted\n',alreadySaved,unfilledCellCount_init,surfNormCropFeatsNameFull));
            fileSaveCounter = 0;
            tic;
            tocCur = toc;
            unfilledCellCount = unfilledCellCount-cellCountToFillCounter;
            cellCountToFillCounter = 0;
        elseif (fileSaveCounter>0 && i==cellCnt)
            fileSaveCounter = 0;
            dispstat(sprintf('Saving completed [surfImArr_cell,imcropVec_cell] to file(%s)\n',surfNormCropFeatsNameFull),'keepthis');
            save(surfNormCropFeatsNameFull, 'surfImArr_cell', 'imcropVec_cell', '-v7.3');
        elseif (fileSaveCounter==0 && i==cellCnt) && verbose_level>0
            dispstat(sprintf('No changes to save to file(%s)\n',surfNormCropFeatsNameFull),'keepthis');
        end
    end
%     
%     disp(['Saving [surfImArr_cell,imcropVec_cell] to file(' surfNormCropFeatsNameFull ')']);
%     save(surfNormCropFeatsNameFull, 'surfImArr_cell', 'imcropVec_cell', '-v7.3');
end

function [surfNormCropFeatsNameFull, backupFileName] = getBackupFile(userFolderName, enforceCreateNew, verbose_level)
    surfNormCropFeatsNameFull = [userFolderName filesep 'surfNorm_crop.mat'];

    backupFold = getVariableByComputerName('backupFold');
    backupFold = [backupFold filesep 'Backup_Feats'];
    foldCells = strsplit(userFolderName, filesep);
    userCells = strsplit(foldCells{1,end}, '_');
    try
        backupFileName = [backupFold filesep 'surfNorm_crop_s' pad(foldCells{1,end-1},3,'left','0') '_u' userCells{1,2} '_r' pad(userCells{1,end},2,'left','0') '.mat'];
    catch
        backupFileName = [backupFold filesep 'surfNorm_crop_s' num2str(str2double(foldCells{1,end-1}),'%03d') '_u' userCells{1,2} '_r' num2str(str2double(userCells{1,end}),'%02d') '.mat'];
    end
    fileExist = exist(surfNormCropFeatsNameFull,'file')>0;
    backupFileExist = exist(backupFileName,'file')>0;
    if (fileExist==true && enforceCreateNew) || (fileExist==false && backupFileExist==true)  
        copyfile(backupFileName,surfNormCropFeatsNameFull,'f');
        disp(['skipping creation of surfNorm_crop from(' userFolderName '); it is retrieved from backup']);
    elseif fileExist && verbose_level>0
        disp(['skipping creation of surfNorm_crop. It will be loaded from(' surfNormCropFeatsNameFull ')']);
    end
end
function [surfNormImArr, imCropVec, dpthImPalette, surfRGBImPalette] = extrapolateImForSurfNormComp(x_In, scales2Use, strideFixed, blockCounts, handInfo)
%extrapolateImForHOGComp generate a lot of sub Images for HOG comparison
%   I have an image with some size,
%   I am looking for a sub part of image that would be the actual part I am
%   seeking for.. Hence I have to look at it wit different surrounding
%   boxes and find the best match between 2 mother images
%   That would require another function to compare these sorted subImages
%   and return which 2 best matched

%   The cropping of the image would have different scales(1.0:-0.05:0.50)
    %scales2Use = 1.0 : -0.05 : 0.50 ;
    imSizeCur = size(x_In);    
    [imCntSizPerScaleVec, imCropVec] = preCalcCropAreas(imSizeCur, scales2Use, strideFixed);
    if length(imSizeCur)==3 && imSizeCur(3)==3
        x_In = rgb2gray(x_In);
    end
    
    x_In = flipForHOG( x_In, handInfo);
    
    imCnt = sum(imCntSizPerScaleVec(:,1));
    assert(imCnt==size(imCropVec,1),['These<imCnt(' num2str(imCnt) ')-(' num2str(size(imCropVec,1))  ')imCropVecLen> must match']);
    surfNormImArr = [];
    
    dpthImBiggestSize = imCntSizPerScaleVec(1,2:3);
    [dpthImPalette, dpthImPaletteRCSize] = imCollageFuncs( 'createPalette', struct('imCnt',imCnt,'channelCnt',1,'imSizeMax',dpthImBiggestSize));
    [surfRGBImPalette, surfRGBImPaletteRCSize] = imCollageFuncs( 'createPalette', struct('imCnt',imCnt,'channelCnt',3,'blockCounts',blockCounts));
    %[paletteRCSize, dpthImPalette, surfRGBImPalette] = paletteCreate(imCnt, dpthImBiggestSize, blockCounts);
    
    OPS = struct('rejectPatchMethod','notIncludeHand');
    for i=1:imCnt
        x_crop = x_In(imCropVec(i,1):imCropVec(i,2),imCropVec(i,3):imCropVec(i,4));
        x_crop(x_crop>1900) = 0;
        x_crop = fill0s_gridFit(x_crop, true);
        [~, OPS.pixelGroupIDs] = setRGBKmeans(x_crop);
        [faceNormCells, quiverMatCells, blockAreaSizeCells, rgbImgAsFaceNormCells] = calcFaceNormsOfImage(x_crop, blockCounts, OPS);
        faceNormCells = faceNormCells(:);
               
        if isempty(surfNormImArr)
            surfNormImArr = zeros(imCnt, size(faceNormCells,1));
        end
        surfNormImArr(i,:) = faceNormCells'; %#ok<AGROW>
        if nargout>2
            dpthImPalette = imCollageFuncs('insertIntoPalette', struct('imageID',i,'paletteRCSize',dpthImPaletteRCSize,'imPalette',dpthImPalette,'imageToAdd',x_crop,'boxSize',dpthImBiggestSize));
            %dpthImPalette = insertIntoPalette(i, dpthImPaletteRCSize, dpthImPalette, x_crop, dpthImBiggestSize);
            surfRGBImPalette = imCollageFuncs('insertIntoPalette', struct('imageID',i,'paletteRCSize',surfRGBImPaletteRCSize,'imPalette',surfRGBImPalette,'imageToAdd',rgbImgAsFaceNormCells,'boxSize',blockCounts));
            %surfRGBImPalette = insertIntoPalette(i, surfRGBImPaletteRCSize, surfRGBImPalette, rgbImgAsFaceNormCells, blockCounts);
        end
    end
end
