function retVal = getVariableByComputerName(variableString)
%getVariableByComputerName For different computers we need to assign some
%variebles within the functions
%   This function checks the computer name and returns the desired
%   parameter
% e.g. srcFold = getVariableByComputerName('srcFold')
% e.g. backupFold = getVariableByComputerName('backupFold')
% e.g. compNameAddStr = getVariableByComputerName('compNameAddStr')
    switch variableString
        case 'srcFold'
            retVal = getSrcFoldByComputerName();
        case 'backupFold'
            retVal = getBackupFoldByComputerName();
        case 'compNameAddStr'
            retVal = getCompNameAddStr();
        case 'systemType'
            retVal = getSystemTypeFromName();
        case 'toWebFold'
            retVal = getToWebFoldByComputerName();
        case 'clusterResultsFold'
            retVal = getClusterResultsFoldByComputerName();
        case 'computerID'
            retVal = getComputerIDByComputerName();
        otherwise
            error(['variableString(' variableString ') - not a known request type']);
    end
end

function compSystemStr = getSystemTypeFromName()
    compSystemStr = 'Windows';
    compName = getComputerName;
    switch compName
        case {'LUMBAR'}
            %LUMBAR  
        case {'Win7_WS_64_UL'}
            %WorkPC  
        case {'IB7702060904'}
            %dho-08
        case {'DESKTOP-2AQGRCR'}
            %dho-dg_hv
        case {'dgssdwork'}
            %dgssdwork 
        case {'workacerssd'}
            %workacerssd  
        case {'DGMSISSD'}
            %school-msi-windows
        case {'doga-MSISSD','DESKTOP-GCOE5AL','q-Bilgisayar'}
            %school-msi-ubuntu 
            compSystemStr = 'Ubuntu';%bogazici-pilab-ubuntu
        case {'doga-msi-ubu','dogamsilaptop'}
            %dogamsilaptop  
            %laptop-msi-ubuntu 
            compSystemStr = 'Ubuntu';%msi-laptop-ubuntu
        case {'WsUbuntu05'}
            %work station 05 at DHO
            compSystemStr = 'Ubuntu';
        otherwise
            error(['Unknown computer name(' compName ')']);
    end    
end

function compNameAddStr = getCompNameAddStr()
    compNameAddStr = '_unk';
    compName = getComputerName;
    switch compName
        case {'LUMBAR'}
            %LUMBAR  
            compNameAddStr = '_lmb';
        case {'Win7_WS_64_UL'}
            %WorkPC  
        case {'IB7702060904'}
            %dho-08
            compNameAddStr = '_dh8';
        case {'DESKTOP-2AQGRCR'}
            %dho-dg_hv
        case {'dgssdwork'}
            %dgssdwork 
        case {'workacerssd'}
            %workacerssd  
        case {'doga-MSISSD','DESKTOP-GCOE5AL','DGMSISSD','q-Bilgisayar'}
            %school-msi-ubuntu 
            compNameAddStr = '_bpu';%bogazici-pilab-ubuntu
        case {'doga-msi-ubu','dogamsilaptop'}
            %dogamsilaptop  
            %laptop-msi-ubuntu 
            compNameAddStr = '_mlu';%msi-laptop-ubuntu
        case {'WsUbuntu05'}
            %work station 05 at DHO
            compNameAddStr = '_wsu';
        otherwise
            error(['Unknown computer name(' compName ')']);
    end    
end

function srcFold = getSrcFoldByComputerName()
    compName = getComputerName;
    switch compName
        %WorkPC case {'Win7_WS_64_UL'}
        %dho-dg_hvcase {'DESKTOP-2AQGRCR'}
        %dgssdwork case {'dgssdwork'}
        %workacerssd case {'workacerssd'}
        case {'LUMBAR'}
            %LUMBAR  
            srcFold = 'D:\matlabStuff\HospiMov';
        case {'IB7702060904'}
            %dho-08
            srcFold = 'D:\Doga\hospiMov';
        case {'doga-MSISSD'}
            %school-msi-ubuntu 
            srcFold = '/mnt/Data/FromUbuntu/HospiSign/HospiMOV';%bogazici-pilab-ubuntu
        case {'DESKTOP-GCOE5AL','DGMSISSD','q-Bilgisayar'}
            %school-msi-windows 
            srcFold = 'C:\FromUbuntu\HospiSign\HospiMOV';%bogazici-pilab-windows
        case {'doga-msi-ubu'}
            %laptop-msi-ubuntu 
            srcFold = '/media/doga/Data/FromUbuntu/Doga';%msi-laptop-ubuntu
        case {'dogamsilaptop'}
            %dogamsilaptop  // windows
            srcFold = 'D:\FromUbuntu\Doga';%msi-laptop-windows
        case {'WsUbuntu05'}
            %work station 05 at DHO
            srcFold = '/media/dg/SSD_Data';
        otherwise
            error(['Unknown computer name(' compName ')']);
    end
    assert(exist(srcFold,'dir')~=0,['assigned srcFold(' srcFold ') doesnt exist'])
end

function backupFold = getBackupFoldByComputerName()
% use the return value + \Backup_Feats or \BackupLabels
    compName = getComputerName;
    switch compName
        %WorkPC case {'Win7_WS_64_UL'}
        %dho-dg_hvcase {'DESKTOP-2AQGRCR'}
        %dgssdwork case {'dgssdwork'}
        %workacerssd case {'workacerssd'}
        case {'LUMBAR'}
            %LUMBAR  
            backupFold = 'Z:';
        case {'IB7702060904'}
            %dho-08
            backupFold = 'D:\HospiBackup\';        
        case {'doga-MSISSD'}
            %school-msi-ubuntu  
            %backupFold = '/media/doga/SSD200/';%bogazici-pilab-ubuntu
            backupFold = '/media/doga/New Volume/';%bogazici-pilab-ubuntu
        case {'DESKTOP-GCOE5AL','DGMSISSD','q-Bilgisayar'}
            %school-msi-windows 
            backupFold = 'M:\';%bogazici-pilab-windows
        case {'doga-msi-ubu'}
            %laptop-msi-ubuntu   
            backupFold = '/media/doga/Data/';%msi-laptop-ubuntu
        case {'dogamsilaptop'}
            %dogamsilaptop  // windows
            backupFold = 'D:\';%msi-laptop-windows
        case {'WsUbuntu05'}
            %work station 05 at DHO  
            backupFold = '/media/dg/SSD_Data/';
        otherwise
            error(['Unknown computer name(' compName ')']);
    end
    if exist(backupFold,'dir')==0
        %warning(['backupFold(' backupFold ') doesnt exist']);
    end
    %assert(exist(backupFold,'dir')~=0,['assigned backupFold(' backupFold ') doesnt exist'])
end

function toWebFold = getToWebFoldByComputerName()
% use the return value + \Backup_Feats or \BackupLabels
    compName = getComputerName;
    switch compName
        %WorkPC case {'Win7_WS_64_UL'}
        %dho-dg_hvcase {'DESKTOP-2AQGRCR'}
        %dgssdwork case {'dgssdwork'}
        %workacerssd case {'workacerssd'}
        case {'LUMBAR'}
            %LUMBAR  
            toWebFold = 'D:\matlabStuff\toWEB';
        case {'IB7702060904'}
            %dho-08
            toWebFold = 'D:\toWEB';        
        case {'doga-MSISSD'}
            %school-msi-ubuntu  
            %backupFold = '/media/doga/SSD200/';%bogazici-pilab-ubuntu
            toWebFold = '/mnt/USB_HDD_1TB/toWEB';%bogazici-pilab-ubuntu
        case {'DESKTOP-GCOE5AL','DGMSISSD','q-Bilgisayar'}
            %school-msi-windows 
            toWebFold = 'D:\toWEB';%bogazici-pilab-windows
        case {'doga-msi-ubu'}
            %laptop-msi-ubuntu   
            toWebFold = '/media/doga/Data/toWEB';%msi-laptop-ubuntu
        case {'dogamsilaptop'}
            %dogamsilaptop  // windows
            toWebFold = 'D:\toWEB';%msi-laptop-windows
        case {'WsUbuntu05'}
            %work station 05 at DHO  
            toWebFold = '/media/dg/SSD_Data/toWEB';
        otherwise
            error(['Unknown computer name(' compName ')']);
    end    
    assert(exist(toWebFold,'dir')~=0,['assigned toWebFold(' toWebFold ') doesnt exist'])
end

function clusterResultsFold = getClusterResultsFoldByComputerName()
% use the return value + \Backup_Feats or \BackupLabels
    compName = getComputerName;
    switch compName
        %WorkPC case {'Win7_WS_64_UL'}
        %dho-dg_hvcase {'DESKTOP-2AQGRCR'}
        %dgssdwork case {'dgssdwork'}
        %workacerssd case {'workacerssd'}
        case {'LUMBAR'}
            %LUMBAR  
            clusterResultsFold = 'D:\matlabStuff\clusterResults';
        case {'IB7702060904'}
            %dho-08
            clusterResultsFold = 'D:\clusterResults';        
        case {'doga-MSISSD'}
            %school-msi-ubuntu  
            %backupFold = '/media/doga/SSD200/';%bogazici-pilab-ubuntu
            clusterResultsFold = '/mnt/USB_HDD_1TB/clusterResults';%bogazici-pilab-ubuntu
        case {'DESKTOP-GCOE5AL','DGMSISSD','q-Bilgisayar'}
            %school-msi-windows 
            clusterResultsFold = 'D:\clusterResults';%bogazici-pilab-windows
        case {'doga-msi-ubu'}
            %laptop-msi-ubuntu   
            clusterResultsFold = '/media/doga/Data/clusterResults';%msi-laptop-ubuntu
        case {'dogamsilaptop'}
            %dogamsilaptop  // windows
            clusterResultsFold = 'D:\clusterResults';%msi-laptop-windows
        case {'WsUbuntu05'}
            %work station 05 at DHO  
            clusterResultsFold = '/media/dg/SSD_Data/clusterResults';
        otherwise
            error(['Unknown computer name(' compName ')']);
    end    
    assert(exist(clusterResultsFold,'dir')~=0,['assigned clusterResults(' clusterResultsFold ') doesnt exist'])
end

function computerID = getComputerIDByComputerName()
    %retVal = getVariableByComputerName('computerID')
    %msi_laptop = 1
    %msi_kasa = 2
    %dho_workStation = 3
    %dho_ib = 4
    compName = getComputerName;
    computerID = -1;
    switch compName
        %WorkPC case {'Win7_WS_64_UL'}
        %dho-dg_hvcase {'DESKTOP-2AQGRCR'}
        %dgssdwork case {'dgssdwork'}
        %workacerssd case {'workacerssd'}
        case {'LUMBAR'}
            %LUMBAR  
            computerID = 5;      
        case {'IB7702060904'}
            %dho-08
            computerID = 4;        
        case {'doga-MSISSD','DESKTOP-GCOE5AL','DGMSISSD','q-Bilgisayar'}
            %school-msi-ubuntu  %bogazici-pilab-ubuntu %school-msi-windows %bogazici-pilab-windows
            computerID = 2;
        case {'doga-msi-ubu','dogamsilaptop'}
            %laptop-msi-ubuntu   
            computerID = 1;%msi-laptop-ubuntu %dogamsilaptop-windows %msi-laptop-windows
        case {'WsUbuntu05'}
            %work station 05 at DHO  
            computerID = 3;
        otherwise
            error(['Unknown computer name(' compName ')']);
    end    
    assert(computerID>=1,['assigned computerID(' computerID ') doesnt exist'])
end