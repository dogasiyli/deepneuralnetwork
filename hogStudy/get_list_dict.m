function [list_dict, list_dict_file_name] = get_list_dict(fold, splitBool)
    list_dict_file_name = strrep([fold filesep 'list_dict.txt'],[filesep filesep],filesep);
    if splitBool
        fid = fopen(list_dict_file_name,'r');
        list_dict = textscan(fid, '%d%d%d%d%d%s%s', 'Delimiter', '*','TreatAsEmpty','~');
        fclose(fid);
    else
        str = fileread(list_dict_file_name);
        list_dict = regexp(str, '\r\n|\r|\n', 'split');
    end
end