function labelSummary = getlabelSummary(srcFold, surList, listElemntCnt, displayInfo)
    if ~exist('displayInfo','var')
        displayInfo=false;
    end
    makeEmptyFrames = false;
    labelSummary = struct;
    labelSummary.surVec = NaN(listElemntCnt,3);%s,u,r
    labelSummary.labelCells = cell(listElemntCnt,3);%lh,rh,bh
    removeLines = 1:listElemntCnt;
    for l = 1:listElemntCnt
        s = surList(l,1);
        u = surList(l,2);
        r = surList(l,3);
        theFoldName = [srcFold filesep num2str(s) filesep 'User_' num2str(u) '_' num2str(r)];
        try
            labelsFolder = [theFoldName filesep 'labelFiles' filesep];
            if exist([labelsFolder 'labelsCS_LH.txt'],'file')
                labelsVec_LH = getLabelVecFromFile([labelsFolder 'labelsCS_LH.txt'], makeEmptyFrames);
            else
                labelsVec_LH = [];
            end
            if exist([labelsFolder 'labelsCS_RH.txt'],'file')
                labelsVec_RH = getLabelVecFromFile([labelsFolder 'labelsCS_RH.txt'], makeEmptyFrames);
            else
                labelsVec_RH = [];
            end
            if exist([labelsFolder 'labelsCS_BH.txt'],'file')
                labelsVec_BH = getLabelVecFromFile([labelsFolder 'labelsCS_BH.txt'], makeEmptyFrames);
            else
                labelsVec_BH = [];
            end
            if ~isempty(labelsVec_LH) || ~isempty(labelsVec_RH) || ~isempty(labelsVec_BH)
                labelSummary.surVec(l,:) = [s u r];
                labelSummary.labelCells{l,1} = labelsVec_LH;
                labelSummary.labelCells{l,2} = labelsVec_RH;
                labelSummary.labelCells{l,3} = labelsVec_BH;
                if displayInfo
                    disp(['s(' num2str(s) '),u(' num2str(u) '),r(' num2str(r) ') lh(' num2str(length(labelsVec_LH)) '),rh(' num2str(length(labelsVec_RH)) '),bh(' num2str(length(labelsVec_BH)) ')'])
                end
            else
                removeLines(l) = NaN;
                %disp(['s(' num2str(s) '),u(' num2str(u) '),r(' num2str(r) ') lh(' num2str(length(labelsVec_LH)) '),rh(' num2str(length(labelsVec_RH)) '),bh(' num2str(length(labelsVec_BH)) ')'])
            end
        catch err %#ok<NASGU>
            problematicVids = [problematicVids;s u r]; %#ok<AGROW>
        end
    end
    labelSummary.surVec(isnan(removeLines),:) = [];
    labelSummary.labelCells(isnan(removeLines),:) = [];
end