function [ imL, imR ] = step00_seeAndLoad2Images(figID)
    load('images2.mat','imL','imR');
    if figID>0
        figure(figID);clf;hold on;
        subplot(2,1,1);imshow(imL);title('imL-200 by 200')
        subplot(2,1,2);imshow(imR);title('imR-200 by 200')
    end
end

