function [ hogImArr_cell, imcropVec_cell] = step01_createHogFeats2Images( im01, im02, hogVersion)
    hogStruct = struct('blockCounts',[10 10], 'histogramBinCount', 9);
    handInfo = struct('handDominant', 'LH', 'handCur', 'LH');
    %as in --> getHOGFeatsofHands(signID, userID, signRepeatID, optParamStruct)
    if hogVersion==1
        hogImArr_cell = cell(1,3);%1-Left, 2-Right, 3-Both Hands
        imcropVec_cell = cell(1,3);%1-Left, 2-Right, 3-Both Hands
        scales2Use = 1:-0.10:0.70;
        strideFixed = 5;
        
        handInfo.handCur = 'LH';
        [hogImArr_01, imcropVec_01] = extrapolateImForHOGComp(im01, scales2Use, strideFixed, hogStruct, handInfo);

        handInfo.handCur = 'LH';
        [hogImArr_02, imcropVec_02] = extrapolateImForHOGComp(im02, scales2Use, strideFixed, hogStruct, handInfo);
        
        hogImArr_cell{1,1} = hogImArr_01;
        hogImArr_cell{1,2} = hogImArr_02;
        imcropVec_cell{1,1} = imcropVec_01;
        imcropVec_cell{1,2} = imcropVec_02;
    else
        %as in[hogImArr_cell, imcropVec_struct] = extractHog_v2(croppedHandsFolderName, imIDBegEnd, inputImSize, handInfo, blockCountsVec, hogStruct);
        hogImArr_cell = cell(1,3);%1-Left, 2-Right, 3-Both Hands
        
        blockCountsVec = [10 11 12];
        rotDegrees = [-30 -20 -10 0 10 20 30];
        
        handInfo.handCur = 'LH';
        hogImArr_01 = extrapolateImForHOGComp_v2(im01, rotDegrees, blockCountsVec, hogStruct, handInfo);
        
        handInfo.handCur = 'LH';
        hogImArr_02 = extrapolateImForHOGComp_v2(im02, rotDegrees, blockCountsVec, hogStruct, handInfo);
        
        hogImArr_cell{1,1} = hogImArr_01;
        hogImArr_cell{1,2} = hogImArr_02;
        
        imcropVec_cell = struct;
        imcropVec_cell.rotDegrees = rotDegrees;
        imcropVec_cell.blockCountsVec = blockCountsVec;
        imcropVec_cell.hogStruct = hogStruct;
    end
end

