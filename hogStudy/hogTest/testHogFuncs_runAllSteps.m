function testHogFuncs_runAllSteps(imL,imR)
    showSourceImageFigID = -1;
    showBoxedImageFigID = 1;

    %% step00 - load the images
    if nargin == 0 || (isempty(imL) && isempty(imR))
        try
            imL = imread('/home/dg/Desktop/crop_LH_057.png');
            imR = imread('/home/dg/Desktop/crop_LH_044.png');
        catch
            [imL, imR] = step00_seeAndLoad2Images(showSourceImageFigID);
        end
    end
    %% step01 - extract hog feats of images
    [hogImArr_cell_hv01, imcropVec_cell_hv01] = step01_createHogFeats2Images( imL, imR, 1);
    [hogImArr_cell_hv02, imcropVec_cell_hv02] = step01_createHogFeats2Images( imL, imR, 2);
    
    %% step02 - calc distances according to hogs
    %version 01
    hog01 = hogImArr_cell_hv01{1,1};
    hog02 = hogImArr_cell_hv01{1,2};
    [minDistVal_01, ~, patchID_im01, patchID_im02] = calcDistofHogs(hog01, hog02, 1);
    area_im01 = imcropVec_cell_hv01{1,1}(patchID_im01,:);%rowY_beg, rowY_end, colX_beg, colX_end,
    area_im02 = imcropVec_cell_hv01{1,2}(patchID_im02,:);%rowY_beg, rowY_end, colX_beg, colX_end,
    
    imL_boxed = drawRectOnImg(imL, [area_im01(3) area_im01(1)], [area_im01(4)-area_im01(3) area_im01(2)-area_im01(1)], [1 0 1], 5);
    imR_boxed = drawRectOnImg(imR, [area_im02(3) area_im02(1)], [area_im02(4)-area_im02(3) area_im02(2)-area_im02(1)], [1 0 1], 5);
    if showBoxedImageFigID>0
        figure(showBoxedImageFigID);clf;hold on;
        subplot(2,2,1);imshow(imL);title('imL-200 by 200')
        subplot(2,2,2);imshow(imR);title('imR-200 by 200')
        subplot(2,2,3);imshow(imL_boxed);title(['Version_02 - dist(' num2str(minDistVal_01,'%02f') ')'])
        subplot(2,2,4);imshow(imR_boxed);title('imR-200 by 200')
    end
    
    hog01 = hogImArr_cell_hv02{1,1};
    hog02 = hogImArr_cell_hv02{1,2};
    imcropVec_struct = imcropVec_cell_hv02;
    [minDistVal_01, ~, patchID_im01, patchID_im02] = calcDistofHogs(hog01, hog02, 2, imcropVec_struct);
    
    area_im01_v2 = convertHogV2Box(patchID_im01, imcropVec_struct, size(imL));
    area_im02_v2 = convertHogV2Box(patchID_im02, imcropVec_struct, size(imR));
    
    imL_boxed_v2 = imrotate(imL,patchID_im01(end),'bilinear','crop');
    imL_boxed_v2 = drawRectOnImg(imL_boxed_v2, [area_im01_v2(3) area_im01_v2(1)], [area_im01_v2(4)-area_im01_v2(3) area_im01_v2(2)-area_im01_v2(1)], [1 0 1], 5);
    imR_boxed_v2 = imrotate(imR,patchID_im02(end),'bilinear','crop');
    imR_boxed_v2 = drawRectOnImg(imR_boxed_v2, [area_im02_v2(3) area_im02_v2(1)], [area_im02_v2(4)-area_im02_v2(3) area_im02_v2(2)-area_im02_v2(1)], [1 0 1], 5);
    if showBoxedImageFigID>0
        figure(showBoxedImageFigID+1);clf;hold on;
        subplot(2,2,1);imshow(imL);title('imL-200 by 200')
        subplot(2,2,2);imshow(imR);title('imR-200 by 200')
        subplot(2,2,3);imshow(imL_boxed_v2);title(['area ' mat2str(area_im01_v2) ' - dist(' num2str(minDistVal_01,'%02f') ')-rot(' num2str(patchID_im01(end),'%d') ')'])
        subplot(2,2,4);imshow(imR_boxed_v2);title(['area ' mat2str(area_im02_v2) ' - dist(' num2str(minDistVal_01,'%02f') ')-rot(' num2str(patchID_im02(end),'%d') ')'])
    end
    
end

