function [pvRet, accuracyListAll, resultSummary] = hospSign_getDetailedResults(signIDList, optParamStruct)
%hospSign_getDetailedResults Run known steps after 4th to analyze how well which delas with it right.

%e.g. hospSign_getDetailedResults(87, optParamStruct)
%e.g. hospSign_getDetailedResults(96, struct('kVec',[15 20 25],'single0_double1', 0, 'labelStrategies2Loop', 'fromAssignedLabels*assignedLabelsRemove1', 'runSteps', [4 5]));
%e.g. hospSign_getDetailedResults(96, struct('kVec',[15 20 25],'single0_double1', 1, 'labelStrategies2Loop', 'fromAssignedLabels*assignedLabelsRemove1', 'runSteps', [4 5]));

%% 0. Define the variables
    pvRet                  = [];

    if ~exist('optParamStruct', 'var') || ~isstruct(optParamStruct)
        optParamStruct = [];
    end
    srcFold             = getStructField(optParamStruct, 'srcFold', getVariableByComputerName('srcFold'));
    backupFold          = getStructField(optParamStruct, 'backupFold', getVariableByComputerName('backupFold'));
    userList            = getStructField(optParamStruct, 'userList', [2 3 4 5]);
    featName            = getStructField(optParamStruct, 'featName', 'sn');
    cropMethod          = getStructField(optParamStruct, 'cropMethod', 'mc');
    kVec                = getStructField(optParamStruct, 'kVec', []);
    single0_double1     = getStructField(optParamStruct, 'single0_double1', 0);
    runSteps            = getStructField(optParamStruct, 'runSteps', 4);
    tryMax              = getStructField(optParamStruct, 'tryMax', 5);
    hogVersion       	= getStructField(optParamStruct, 'hogVersion', 1);
    maxRptID            = getStructField(optParamStruct, 'maxRptID', 1);
    minRepCnt           = getStructField(optParamStruct, 'minRepCnt', 0);
    deleteDataFile      = getStructField(optParamStruct, 'deleteDataFile', true);
    backupFeatsWhenDone = getStructField(optParamStruct, 'backupFeatsWhenDone', false);
    checkStep01         = getStructField(optParamStruct, 'checkStep01', true);
    %labelStrategies2Loop= getStructField(optParamStruct, 'labelStrategies2Loop', 'fromSkeletonStableAll*fromAssignedLabels*assignedLabelsRemove1*assignedLabelsExcept1');
    labelStrategies2Loop= getStructField(optParamStruct, 'labelStrategies2Loop', 'assignedLabelsRemove1');
    labelStrategies2Loop= strsplit(labelStrategies2Loop,'*');
    frameLabelingStrategies= getStructField(optParamStruct, 'frameLabelingStrategies', 'labelClusterGroups');%'labelClusterGroups*singleFrame_1nn'
    frameLabelingStrategies= strsplit(frameLabelingStrategies,'*');
 
    enforceRecreateCluster_step4     = getStructField(optParamStruct, 'enforceRecreateCluster_step4', false);
    checkForBetterCluster_step4      = getStructField(optParamStruct, 'checkForBetterCluster_step4', true);
    saveAllClusterResultImages_step4 = getStructField(optParamStruct, 'saveAllClusterResultImages_step4', true);
    skipClustering_step4             = getStructField(optParamStruct, 'skipClustering_step4', false);
    updateMostReassignedClusterFrames_step4 = getStructField(optParamStruct, 'updateMostReassignedClusterFrames_step4', false);
    
    handsStructMedoids_step5 = getStructField(optParamStruct, 'handsStructMedoids_step5', 'all');%all or sign
    onlyCalcDistIfSameHandShape_step5 = getStructField(optParamStruct, 'onlyCalcDistIfSameHandShape_step5', true);
    skipCalcDistIfUnlabelled_step5    = getStructField(optParamStruct, 'skipCalcDistIfUnlabelled_step5', true);
    
    frameLabelingStrategyCount = length(frameLabelingStrategies);

    switch featName
        case {'surfNorm','sn','surfaceNormal'}
            distMatInfoStruct = struct('featureName','sn','cropMethod',cropMethod,'distanceMethod','c2');
        otherwise%case {'hogFeat','hf','hog'}
            distMatInfoStruct = struct('featureName','hf','cropMethod',cropMethod,'distanceMethod','e3'); 
    end
    
    for signID = signIDList
        try
            surSlctListStruct = struct('createMode', 2, 'signIDList', signID, 'userList', userList, 'maxRptID',maxRptID, 'single0_double1', single0_double1);

            %'cropVidFrames' then 'extractHog' or 'surfNormVidsPics'
            fwf_runStep01(checkStep01,surSlctListStruct, distMatInfoStruct, srcFold);

            %create clusteringParams and kVec
            [clusteringParams, kVec, kVec_count] = fwf_runStep02(srcFold, surSlctListStruct, kVec, tryMax, featName);
            labelStrategyCount = length(labelStrategies2Loop);

            resultSummary = struct;
            resultSummary.labelStrategies2Loop = labelStrategies2Loop;
            accuracyListAll = zeros(labelStrategyCount*kVec_count,1+6);

            baseClusterResultFolderName = setClusterResultFolder(srcFold, distMatInfoStruct, [], struct('createDirIfNotExist', false));
            if sum(runSteps==4)>0
                for lsi = 1:labelStrategyCount
                    labelStrategyCurrent = labelStrategies2Loop{lsi};
                    fileFolderEnding = fwf_getFileFolderEnding(signID, cropMethod, labelStrategyCurrent);
                    try
                        surSlctListStruct.labelingStrategyStr = labelStrategyCurrent;

                        clusterResultFolder = [baseClusterResultFolderName '_' fileFolderEnding];
                        createFolderIfNotExist(clusterResultFolder);
                        diaryFullFileName  = [clusterResultFolder filesep 'step04_' fileFolderEnding '.txt'];            

                        OPS = struct;
                        OPS.clusteringParams = clusteringParams;
                        OPS.srcFold = srcFold;
                        OPS.enforceRecreateCluster = enforceRecreateCluster_step4;
                        OPS.diaryFullFileName = diaryFullFileName;
                        OPS.clusterResultFolder = clusterResultFolder;
                        OPS.deleteDataFile = deleteDataFile;
                        OPS.checkForBetterCluster = checkForBetterCluster_step4;
                        OPS.saveAllClusterResultImages = saveAllClusterResultImages_step4;
                        OPS.finalizeDiary = false;
                        OPS.skipClustering = skipClustering_step4;
                        

                        disp(['running analyzeDistMat for labelStrategyCurrent(' num2str(lsi) '-' labelStrategyCurrent ')']);
                        [ ~, ~, ~, confAccBest, clusterResultCells] = analyzeDistMat(surSlctListStruct, distMatInfoStruct, OPS);
                        
                        [wrongPredictionMat, toChangeRows]= analyzeClusterResultCells(clusterResultCells);
                        if updateMostReassignedClusterFrames_step4
                            [reAssLimitRows, toChangeRows2] = updateMostReassignedClusterFrames(srcFold, signID, unique(wrongPredictionMat(:,1))', toChangeRows);
                        end
                        %I want the accuracy to be high as well as the cluster
                        %count to be low
                        confAccBest_new = [confAccBest ((100-confAccBest(:,end)).*confAccBest(:,1))];
                        try
                            confAccBest_new = sortrows(confAccBest_new,[6 1],'ascend','ascend');
                        catch
                            confAccBest_new = sortrows(confAccBest_new,[6 1]);
                        end     
                        disptable(confAccBest_new, 'originalClusterCount|reducedKlusterCount|confusionAccuracy|kSpecificity|combinedPrecision|accumErrByKluster',[],'%4.2f',1);    

                        eval(['resultSummary.' labelStrategyCurrent '_accuracyList = confAccBest_new;']);
                        accuracyListAll = appendAccuracyList(accuracyListAll, lsi, confAccBest_new);

                        %if (confAccBest(1,4)>=99.99 && confAccBest(1,5)>=90.00)
                            k_selected = get_kSelected(signID, featName, confAccBest_new);
                            disp(['According to labeling results, the first row of k = ' num2str(k_selected) ' is selected with a combined precision of ' num2str(confAccBest_new(1,5),'%4.2f')]);
                        %end
                        OPS_CHSMFAS.askForNewAssignments = false;
                        OPS_CHSMFAS.hogVersion = hogVersion;
                        %[newLabels, distMat, clusterLabelsCell, handsStruct] = ...
                        createHandsStructMedoidsForASign(srcFold, surSlctListStruct, distMatInfoStruct, clusteringParams, k_selected, OPS_CHSMFAS);
                        clear('OPS','OPS_CHSMFAS');
                        diary('off');
                    catch err
                        displayErrorMessage(err);
	                    diary('off');
						pvRet = [pvRet; signID -4 lsi];
                    end 
                end
                accuracyListAllResultFolder = [baseClusterResultFolderName '_' fileFolderEnding];
                createFolderIfNotExist(accuracyListAllResultFolder);
                accuracyListAllFullFileName  = [accuracyListAllResultFolder filesep 'step04_accuracyListAll.mat'];     
                accuracyListAllFullFileName2 = [srcFold filesep num2str(signID) filesep 'step04_accuracyListAll_' num2str(signID,'%03d') '_' distMatInfoStruct.featureName  '_' distMatInfoStruct.cropMethod '_' num2str(surSlctListStruct.single0_double1) '.mat'];
                accuracyListAllFullFileName_txt = [srcFold filesep num2str(signID) filesep 'step04_accuracyListAll_' num2str(signID,'%03d') '_' distMatInfoStruct.featureName  '_' distMatInfoStruct.cropMethod '_' num2str(surSlctListStruct.single0_double1) '.txt'];
                diary(accuracyListAllFullFileName_txt);
                disp(datetime('now'));
                disptable(accuracyListAll,'lsi|kCnt|redClassCnt|confAcc|kSpec|combAcc|accumErrByKluster');
                diary('off');
                save(accuracyListAllFullFileName,'accuracyListAll');
                save(accuracyListAllFullFileName2,'accuracyListAll');
                if backupFeatsWhenDone
                    moveFiles(signID, 2:5 , struct('HOGFeat','backup'), struct('vidCount',1,'srcFold', srcFold, 'backupFold', backupFold));
                end
            end
            if sum(runSteps==5)>0
                for lsi = 1:labelStrategyCount
                    try
                        labelStrategyCurrent = labelStrategies2Loop{lsi};
                        fileFolderEnding = fwf_getFileFolderEnding(signID, cropMethod, labelStrategyCurrent);
                        comparisonResultFolder = [baseClusterResultFolderName '_' fileFolderEnding];
                        createFolderIfNotExist(comparisonResultFolder); 
                        switch handsStructMedoids_step5
                            case 'all'
                                fileName_handsStructMedoids = [srcFold filesep 'handsStructMedoids_' distMatInfoStruct.featureName  '_' distMatInfoStruct.cropMethod '_' num2str(surSlctListStruct.single0_double1) '_' labelStrategyCurrent '.mat'];
                            case 'sign'
                                fileName_handsStructMedoids = [srcFold filesep num2str(signID) filesep 'handsStructMedoids_' num2str(signID,'%03d') '_' distMatInfoStruct.featureName  '_' distMatInfoStruct.cropMethod '_' num2str(surSlctListStruct.single0_double1) '_' labelStrategyCurrent '.mat'];
                        end
                        for fls_id = 1:frameLabelingStrategyCount
                            frameLabelingStrategy_current = frameLabelingStrategies{fls_id};
                            comparisonFileFullFileName  = [comparisonResultFolder filesep 'step05_' labelStrategyCurrent '_' frameLabelingStrategy_current '_compData.mat'];  
                            if exist(comparisonFileFullFileName,'file')
                                load(comparisonFileFullFileName,'surSlctListStruct','labelStrategyCurrent','pv5','predictedLabels', 'existingLabels');
                            else
                                surSlctListStruct.signIDList = signID;
                                surSlctListStruct.userList = 2:7;
                                surSlctListStruct.maxRptID = 20;
                                surSlctListStruct.minRepCnt = minRepCnt;
                                surSlctListStruct.labelingStrategyStr = labelStrategyCurrent;
                                %possibleDTWFlows = cell(2,1);possibleDTWFlows{1} = [1 2 3];possibleDTWFlows{2} = [1 3 2];possibleDTWFlows{3} = [1 3];

                                OPS = struct('srcFold',srcFold,'overwriteLabelFiles',false, 'hogVersion', hogVersion, 'returnExistingAndPredicted', true, 'frameLabelingStrategy', frameLabelingStrategy_current);
                                OPS.fileName_handsStructMedoids = fileName_handsStructMedoids;
                                OPS.onlyCalcDistIfSameHandShape = onlyCalcDistIfSameHandShape_step5;
                                OPS.skipCalcDistIfUnlabelled = skipCalcDistIfUnlabelled_step5;
                                OPS.saveAtEveryNewCnt = 15000;
                                
                                [pv5, predictedLabels, existingLabels] =  setLabelsFromGroupedMedoids(surSlctListStruct, distMatInfoStruct, OPS);
                                save(comparisonFileFullFileName,'surSlctListStruct','labelStrategyCurrent','pv5','predictedLabels', 'existingLabels');
                                pvRet = [pvRet; pv5];
                            end
                            [accResults, recallResults, precisionResults, f1results, confMatCells, totalResult] = getDetailedResultByLabelingStrategy(labelStrategyCurrent, predictedLabels, existingLabels);
                            clear('OPS');
                        end                   
                    catch err
                        displayErrorMessage(err);
                        diary('off');
						pvRet = [pvRet; signID -5 lsi];
                    end
                    clear('predictedLabels', 'existingLabels');
                end
                fwf_backupFeats(backupFeatsWhenDone, signID, featName, srcFold, backupFold);
            end
        catch err
            displayErrorMessage(err);
            diary('off');
            pvRet = [pvRet; signID 0 0];
        end
        fwf_backupFeats(backupFeatsWhenDone, signID, featName, srcFold, backupFold);
    end
end

function fwf_backupFeats(backupFeatsWhenDone, signID, featName, srcFold, backupFold)
    if backupFeatsWhenDone
        switch featName
            case 'sn'
                moveFiles(signID, [], struct('SurfNormFeat','backup'), struct('srcFold', srcFold, 'backupFold', backupFold));
            otherwise
                moveFiles(signID, [], struct('HOGFeat','backup'), struct('srcFold', srcFold, 'backupFold', backupFold));
        end
    end                
end

function fileFolderEnding = fwf_getFileFolderEnding(signID, cropMethod, labelStrategyCurrent)
    labelingStrategyStr = assignLabelStrategy(labelStrategyCurrent, 2);
    fileFolderEnding = ['s' num2str(signID,'%03d') '_' cropMethod '_' labelingStrategyStr];
end

function [clusteringParams, kVec, kVec_count] = fwf_runStep02(srcFold, surSlctListStruct, kVec, tryMax, featName)
        clusterNames = getClusterNamesAssignedFromTxt(srcFold, surSlctListStruct);
        clusterCount = length(clusterNames)-1;
        clustBintMat = get_clustBintMat(surSlctListStruct.signIDList, featName);
        clustCntMul =  clustBintMat(4);
        linspaceBinCnt =  clustBintMat(5);
        if isempty(kVec)
            kMax = min(20,clusterCount*clustCntMul);%2-4 or 3-6
            kVec = sort(unique(round(linspace(clusterCount,kMax,linspaceBinCnt))));%9:3:18;%
        end
        clusteringParams = struct('tryMax',tryMax,'kVec',kVec,'gtLabelsGrouping',[],'modeStr','kMedoids','groupClusterLabels',true);
        kVec_count = length(kVec);
end

function accuracyListAll = appendAccuracyList(accuracyListAll, lsi, confAccBest)
    rowCnt = size(confAccBest,1);
    fr = (lsi-1)*rowCnt + 1;
    to = fr + rowCnt-1;
    accuracyListAll(fr:to,1) = lsi;
    accuracyListAll(fr:to,2:end) = confAccBest;
end

function k_selected = get_kSelected(signID, featName, confAccBest_new)
    clustBintMat = get_clustBintMat(signID, featName);
    k_desired = clustBintMat(3);
    k_desiredExist = sum(ismember(confAccBest_new(:,1),k_desired))>0;
    if k_desiredExist && k_desired>0
        k_selected = k_desired;
    else
        k_selected = confAccBest_new(1,1);
    end
end

%'cropVidFrames' then 'extractHog' or 'surfNormVidsPics'
function fwf_runStep01(checkStep01, surSlctListStruct, distMatInfoStruct, srcFold)
    if ~checkStep01
        return
    end
    pv = loopOnHospiSign('cropVidFrames', surSlctListStruct, struct('srcFold',srcFold));
    assert(isempty(pv),['there are problematic videos - ' mat2str(pv)]);
    switch distMatInfoStruct.featureName
        case 'hf'
            pv = loopOnHospiSign('extractHog', surSlctListStruct, struct('srcFold',srcFold));
        case 'sn'
            pv = loopOnHospiSign('surfNormVidsPics', surSlctListStruct, struct('srcFold',srcFold));
    end
    assert(isempty(pv),['there are problematic videos - ' mat2str(pv)]);
end
