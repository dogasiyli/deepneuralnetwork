function [pv, step2Results] = hospSign_runForASign(signID, optParamStruct)
%hospSign_runForASign Run known steps of a sign recognition process.
%   We determined a step-by-step sign recognition process..
%   Here we can manage to run for a sign, the selected processes one after
%   another.

%e.g. hospSign_runForASign(87, optParamStruct)
%e.g. hospSign_runForASign(96, struct('kVec',[15 20 25],'single0_double1', 0, 'runVec', [1 1 1 0 0]));
%e.g. hospSign_runForASign(96, struct('kVec',[15 20 25],'single0_double1', 1, 'runVec', [0 0 0 1 1]));

%% 0. Define the variables
    if ~exist('optParamStruct', 'var') || ~isstruct(optParamStruct)
        optParamStruct = [];
    end
    srcFold             = getStructField(optParamStruct, 'srcFold', getVariableByComputerName('srcFold'));
    userList            = getStructField(optParamStruct, 'userList', [2 3 4 5]);
    featName            = getStructField(optParamStruct, 'featName', 'hf');
    %labelStategyStr = getStructField(optParamStruct, 'labelStategyStr', 'fromSkeletonOnly');
    kVec                = getStructField(optParamStruct, 'kVec', [10 12 15]);
    single0_double1     = getStructField(optParamStruct, 'single0_double1', 0);
    runVec              = getStructField(optParamStruct, 'runVec', [1 1 1 0 0]);
    possibleDTWFlows    = getStructField(optParamStruct, 'possibleDTWFlows', []);
    hogVersion       	= getStructField(optParamStruct, 'hogVersion', 1);
    maxRptID            = getStructField(optParamStruct, 'maxRptID', 1);
    pv                  = NaN;
    labelStrategyStep_2 = getStructField(optParamStruct, 'labelStrategyStep_2', 'fromSkeletonStableAll');%'fromSkeletonStableAll'<-this was default,'fromAssignedLabels','assignedLabelsRemove1','assignedLabelsExcept1','fromSkeletonOnly'
    checkStep4Labeling  = getStructField(optParamStruct, 'checkStep4Labeling', false);
    enforceRecreateCluster_step4 = getStructField(optParamStruct, 'enforceRecreateCluster_step4', false);

    switch featName
        case {'surfNorm','sn','surfaceNormal'}
            distMatInfoStruct = struct('featureName','sn','cropMethod','mc','distanceMethod','c2');
        otherwise%case {'hogFeat','hf','hog'}
            distMatInfoStruct = struct('featureName','hf','cropMethod','mc','distanceMethod','e3'); 
    end
    
    step2Results = [];
    
    surSlctListStruct = struct('createMode', 2, 'signIDList', signID, 'userList', userList, 'maxRptID',maxRptID, 'single0_double1', single0_double1);
    clusteringParams = struct('tryMax',3000,'kVec',kVec,'gtLabelsGrouping',[],'modeStr','kMedoids','groupClusterLabels',false);
%% 1.loopOnHospiSign
    if runVec(1)==true
        pv = loopOnHospiSign('cropVidFrames', surSlctListStruct, struct('srcFold',srcFold));
        assert(isempty(pv),['there are problematic videos - ' mat2str(pv)]);
        switch distMatInfoStruct.featureName
            case 'hf'
                pv = loopOnHospiSign('extractHog', surSlctListStruct, struct('srcFold',srcFold));
            case 'sn'
                pv = loopOnHospiSign('surfNormVidsPics', surSlctListStruct, struct('srcFold',srcFold));
        end
        assert(isempty(pv),['there are problematic videos - ' mat2str(pv)]);
    end
    
%% 2.analyzeDistMat and unsupervised cluster - before labelling anything 
% labelingStrategyStr = 'fromSkeletonStableAll'; we do not know any labels.. so we try to acquire stable images.
% also for a specific sign run kVec cluster counts 
    if runVec(2)==true
        surSlctListStruct.labelingStrategyStr = assignLabelStrategy(labelStrategyStep_2); %we do not know any labels.. so we try to acquire stable images.
        diary('off');
        OPS = struct;
        OPS.clusteringParams= clusteringParams;
        OPS.srcFold         = srcFold;
        OPS.hogVersion      = hogVersion;
        OPS.ignoreLabels    = true;
        
        [distMat, clusterLabelsCell] = analyzeDistMat(surSlctListStruct, distMatInfoStruct, OPS);
        step2Results = struct('distMat',distMat,'clusterLabelsCell',clusterLabelsCell);
        clear('OPS');
    end

%% 3.assign clusters to labels
    if runVec(3)==true
		%clusteringParams.kVec = [10 12 15];%the more cluster seems to be better
        OPS = struct;
        OPS.k_selected = [];%12;%sth from clusteringParams.kVec
        OPS.destFolder = getVariableByComputerName('clusterResultsFold');     
        OPS.hogVersion = hogVersion;

		%[acceptedBlocksAll, newLabels, distMat, clusterLabelsCell, handsStruct] = ...
		  setLabelsFromClusterAnalysisResults(srcFold, surSlctListStruct, distMatInfoStruct, clusteringParams, OPS);   
        clear('OPS');
    end

%% 4. now that fine labelling of first 4 videos are completed.. hence group medoids needed to be found
    if runVec(4)==true
        if checkStep4Labeling
            str = input('Did you fine label the first videos? : ','s');
            switch str
                case {'y','yes','YES','Y'}
                otherwise
                    error('then fine label the initial videos first!! bye bye');
            end  
        end
        k_selected = [];%12;%sth from clusteringParams.kVec
        clusteringParams.kVec = kVec;
        clusteringParams.groupClusterLabels = true;
        clusteringParams.tryMax = 500;
        surSlctListStruct.labelingStrategyStr = 'fromAssignedLabels';
        
        diary('off');
        OPS = struct;
        OPS.clusteringParams = clusteringParams;
        OPS.srcFold = srcFold;
        OPS.enforceRecreateCluster = enforceRecreateCluster_step4;
        [ ~, ~, ~, confAccBest] = analyzeDistMat(surSlctListStruct, distMatInfoStruct, OPS);
        if (confAccBest(1,4)>=99.99 && confAccBest(1,5)>=90.00)
            k_selected = confAccBest(1,1);
            disp(['According to labeling results, the first row of k = ' num2str(k_selected) ' is selected with a combined precision of ' num2str(confAccBest(1,5),'%4.2f')]);
        end
        %[newLabels, distMat, clusterLabelsCell, handsStruct] = ...
        createHandsStructMedoidsForASign(srcFold, surSlctListStruct, distMatInfoStruct, clusteringParams, k_selected, struct('hogVersion',hogVersion));
        clear('OPS');
    end
    
%% 5.
    if runVec(5)==true
        surSlctListStruct.userList = 2:7;
        surSlctListStruct.maxRptID = 50;
        surSlctListStruct.labelingStrategyStr = 'fromAssignedLabels';
        %possibleDTWFlows = cell(2,1);possibleDTWFlows{1} = [1 2 3];possibleDTWFlows{2} = [1 3 2];possibleDTWFlows{3} = [1 3];
        
        OPS = struct('srcFold',srcFold,'overwriteLabelFiles',false, 'hogVersion', hogVersion);
        pv =  setLabelsFromGroupedMedoids(surSlctListStruct, distMatInfoStruct, OPS);
        clear('OPS');
    end
end