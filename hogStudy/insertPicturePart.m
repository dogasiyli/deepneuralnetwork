%stripIm{j,1} = insertPicturePart(stripIm{j,1}, subClusterImage, 2, j, stripImSizeVec, [])
function picCur = insertPicturePart(picCur, imToInsert, rowID, colID, stripImSizeVec, colorID)
    newImSiz = stripImSizeVec(1);
    imSepSiz = stripImSizeVec(2);
    imTotSiz = sum(stripImSizeVec);
    stripImSize = [newImSiz,newImSiz];
    if ~exist('colorID','var') || isempty(colorID) || (length(colorID)<2 && colorID<-1)
        picCur(rowID*imTotSiz:rowID*imTotSiz+newImSiz-1, 1+(colID-1)*imTotSiz:colID*imTotSiz-imSepSiz,:) = imresize(imToInsert,stripImSize);
        return
    end
    for i=1:length(colorID)
        picCur(rowID*imTotSiz:rowID*imTotSiz+newImSiz-1, 1+(colID-1)*imTotSiz:colID*imTotSiz-imSepSiz,colorID(i)) = imresize(imToInsert,stripImSize);
    end
end