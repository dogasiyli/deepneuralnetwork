function [frameArea, pixLim] = interpretFramearea(frameArea, areaDefStr)
    xFrCnt = frameArea(2)-frameArea(1)+1;
    xDef = strsplit(areaDefStr,'-');
    
    xFr = str2double(xDef(1));
    xTo = str2double(xDef(2));
    frameArea(1) = frameArea(1) + xFr - 1;
    frameArea(2) = frameArea(2) - (xFrCnt-xTo);
    
    pixLim = [xFr xTo];
end