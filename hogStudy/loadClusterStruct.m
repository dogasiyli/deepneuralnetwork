function [ clustersStruct, clusterFileNameStr ] = loadClusterStruct( clustersStruct )
%loadClusterStruct checks the passed param and loads the clusterStruct
%   clustersStruct - can be the fileName of the file to be loaded
%   clustersStruct - can be already loaded and is a struct
%   clustersStruct - can be a struct with only 1 field 'saveFileNameFull'
    clusterFileNameStr = '';
    if ischar(clustersStruct) || (isstruct(clustersStruct) && ~isfield(clustersStruct,'dataset') && isfield(clustersStruct,'saveFileNameFull'))
        if ischar(clustersStruct)
            clusterFileNameStr = clustersStruct;
        else
            clusterFileNameStr = clustersStruct.saveFileNameFull;            
        end
    elseif isstruct(clustersStruct) && isfield(clustersStruct,'clusterFileNameStr')
        clusterFileNameStr = clustersStruct.clusterFileNameStr;
    end
    if ~strcmp(clusterFileNameStr,'') && exist(clusterFileNameStr,'file') && (~isstruct(clustersStruct) || isempty(clustersStruct))
        load(clusterFileNameStr, 'clustersStruct');
        clustersStruct.clusterFileNameStr = clusterFileNameStr;
        save(clusterFileNameStr, 'clustersStruct');
    elseif ~strcmp(clusterFileNameStr,'') && ~exist(clusterFileNameStr,'file')
        clustersStruct = [];
    end
end

