function knownDistances_loaded = loadKnownDistancesFromFile(knownDistancesFileName)
    knownDistances = knownDistanceFileOperations('quantizeIfNecessary', struct('fileName', knownDistancesFileName));
    try
        load(knownDistancesFileName, 'knownDistances');
    catch
        disp('knownDistances could not be loaded from knownDistancesFile');
        knownDistances = [];
    end
    if isempty(knownDistances)
        %do nothing
    elseif isstruct(knownDistances) && isfield(knownDistances,'frameInfo') && isfield(knownDistances,'additionalCols')
        %knownDistances already converted to squeezed version
    else
        %knownDistances should be converted to squeezed version
        [~,knownDistances] = distMatQuantize_hospiSign(knownDistances, []);
        disp(['Updating known distances file to squeezed version (' knownDistancesFileName ')']);
        save(knownDistancesFileName, 'knownDistances');
    end
    knownDistances_loaded = knownDistances;    
end