function [ feats, label_vec, label_names, infoPreserved ] = load_xv_fold_data( fold, data_dentifier, pca_count )
    if contains(data_dentifier, 'hog') || contains(data_dentifier, 'hg')
        hogImArrFileName = [fold filesep 'hog_10_9.mat'];
        load(hogImArrFileName,'hogImArr');
        hgMat = hogImArr; clear hogImArr;%Nx900
    end
    if contains(data_dentifier, 'sn')
        snMatFileName = [fold filesep 'sn.mat'];
        load(snMatFileName,'mat2sa');
        snMat = mat2sa; clear mat2sa;%Nx1600
    end
    if contains(data_dentifier, 'skel') || contains(data_dentifier, 'sk')
        skelMatFileName = [fold filesep 'skel.mat'];
        load(skelMatFileName,'mat2sa');
        skMat = mat2sa; clear mat2sa;%Nx112
    end
    switch data_dentifier
        case {'hg','hog'}
            feats = hgMat;
        case {'sk','skeleton'}
            feats = skMat;
        case 'sn'
            feats = snMat;
        case 'hgsk'
            feats = [hgMat skMat];
        case 'hgsn'
            feats = [hgMat snMat];
        case 'snsk'
            feats = [snMat skMat];
        case 'hgsnsk'   
            feats = [hgMat snMat skMat];
    end
    list_dict = get_list_dict(fold, true);
    label_names_all = cellfun(@(x) strtrim(strrep(x,'Cift','')),list_dict{6}, 'UniformOutput', false);
    [label_names, ~, label_vec] = unique(label_names_all);
    label_vec = reshape(label_vec,1,[]);
    feat_size_orig = size(feats,2);
    
    %[coeff,score,latent,tsquared,explained,mu] = pca(feats);
    [~,score,~,~,explained,~] = pca(feats);
    infoVal = 100*cumsum(explained)./sum(explained); 
    infoPreserved =  infoVal(pca_count);
    feats = score(:,1:pca_count);
    disp(['feat(' data_dentifier ') of dim(' num2str(feat_size_orig) ') with pca(' num2str(pca_count) ') -> infoPreserved(%' num2str(infoPreserved,'%4.2f') ')'])
end

