function problematicVids = loop2CreateComparisonMats( srcFold, signIDList, userList, maxRptID, hogVersion)
    %problematicVidsComp = loop2CreateComparisonMats( '/media/doga/Data/FromUbuntu/Doga', 1:700, 1:7, 3, 2);
    problematicVids = [];
    for s = signIDList
        for ux = userList
            for rx = 1:maxRptID
                for uy = userList
                    for ry = 1:maxRptID
                        %users should not be same and ux must be less than uy
                        if ux>=uy
                            continue;
                        end
                        signUserRepExist_x = checkFolder(srcFold, s, ux, rx);
                        if ~signUserRepExist_x
                            continue;
                        end
                        signUserRepExist_y = checkFolder(srcFold, s, uy, ry);
                        if ~signUserRepExist_y
                            continue;
                        end
                        %here signID = s, userIDs = [ux uy], repitionIDs = [rx ry] can be compared
                        
                        disp(['will try to compare <' mat2str([s ux rx uy ry]) '>'])
                        try
                            compareVidCouplePerImageHandDist(srcFold, s, [ux uy], [rx ry], hogVersion);
                        catch
                            problematicVids = [problematicVids;s ux rx uy ry];
                        end
                    end
                end
            end
        end
    end
end

function [signUserRepExist, theFoldName] = checkFolder(srcFold, signID, userID, repID)
    theFoldName = srcFold;
    signUserRepExist = false;
    %into sign folder
    theFoldName = [theFoldName filesep num2str(signID) ]; %#ok<*AGROW>
    if ~exist(theFoldName, 'dir')
        return;
    end
    %into user-repeat folder
    theFoldName = [theFoldName filesep 'User_' num2str(userID) '_' num2str(repID)];
    if ~exist(theFoldName, 'dir')
        return;
    end
    signUserRepExist = true;
end