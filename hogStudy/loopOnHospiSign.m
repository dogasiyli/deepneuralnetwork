function problematicVids = loopOnHospiSign(operationStr, surSlctListStruct, optParamStruct)
    %operationStr = {'extractHog','surfNormVidsPics','cropVidFrames'};
    %srcFold = {'/media/doga/Data/FromUbuntu/Doga', 'D:\FromUbuntu\Doga', 'C:\FromUbuntu\HospiSign\HospiMOV'}
    %surSlctListStruct_01 = struct('createMode', 1, 'signIDList',getSignListAll(),'userList',2:7,'maxRptID',10);
    %surSlctListStruct_02 = struct('createMode', 2, 'signIDList',getSignListAll(),'userList',2:5,'maxRptID',1);
    %surSlctListStruct_03 = struct('createMode',1,'signIDList',1:700,'userList',2:7,'maxRptID',10);
    %surSlctListStruct_04 = struct('createMode',2,'signIDList',[7 13 49 69 74 85 86 88 586 636],'userList',3,'maxRptID',1);
    %pV_01 = loopOnHospiSign(srcFold{1}, surSlctListStruct_01, false);
    %pV_02 = loopOnHospiSign(srcFold{2}, surSlctListStruct_02, false);
    %pV_03 = loopOnHospiSign(srcFold{3}, surSlctListStruct_04, false);
    
    
    if ~exist('surSlctListStruct','var') || isempty(surSlctListStruct)
        surSlctListStruct = struct('createMode',1,'signIDList',1:700,'userList',2:7,'maxRptID',10);
    end
    if ~exist('optParamStruct','var') || isempty(optParamStruct)
        optParamStruct = [];
    end
    createMode = surSlctListStruct.createMode; 
    signIDList = surSlctListStruct.signIDList; 
    userList = surSlctListStruct.userList; 
    maxRptID = surSlctListStruct.maxRptID;    

    srcFold = getStructField(optParamStruct, 'srcFold', getVariableByComputerName('srcFold'));
    enforceReCreateNew = getStructField(optParamStruct, 'enforceReCreateNew', false);
    scales2Use = getStructField(optParamStruct, 'scales2Use', 1:-0.10:0.70);
    makeEmptyFrames = getStructField(optParamStruct, 'makeEmptyFrames', true);
    
    
    problematicVids = [];
    switch createMode
        case 1 %ID
            disp('create mode(userList)');
            [surList, listElemntCnt] = createSURList(srcFold, 1, struct('signIDList',signIDList,'userList',userList,'maxRptID',maxRptID));
        case 2 %Count
            disp('create mode(maxRepCnt)');
            %signIDList, userID, maxRptCnt
            [surList, listElemntCnt] = createSURList(srcFold, 2, struct('signIDList',signIDList,'userID',userList,'maxRptCnt',maxRptID));
    end
    disp(['Opration (' operationStr ')']);
    disptable([(1:listElemntCnt)' surList], 'rID|sign|user|repition');
    
    perSignVec = [];
    for l = 1:listElemntCnt
        s = surList(l,1);
        u = surList(l,2);
        r = surList(l,3);
        theFoldName = [srcFold filesep num2str(s) filesep 'User_' num2str(u) '_' num2str(r)];
        disp(['will try to go into folder <' theFoldName '>'])
        try
            tic;
            switch operationStr
                case {'ExtractHog','extractHog'}
                    hogVersion = getStructField(optParamStruct, 'hogVersion', 1);
                    getHOGFeatsofHands(s, u, r, struct('srcFold',srcFold,'hogVersion', hogVersion, 'enforceCreateNew', enforceReCreateNew));
                case {'SurfNormVidsPics','surfNormVidsPics'}
                    %operationStr = 'SurfNormVidsPics';
                    %OPS_SurfNormVidsPics = struct('skipStillFrames',skipStillFrames,'skipBothHands',skipBothHands)
                    calcSurfNormOfVidFrames(theFoldName, [20 20], [], enforceReCreateNew);
                    skipStillFrames = getStructField(optParamStruct, 'skipStillFrames',true);
                    skipBothHands = getStructField(optParamStruct, 'skipBothHands',true);
                    getSurfNormFeatsofHands([s, u, r], struct('srcFold',srcFold,'skipStillFrames',skipStillFrames,'skipBothHands',skipBothHands,'scales2Use',scales2Use,'makeEmptyFrames',makeEmptyFrames));            
                case 'cropVidFrames'
                    cropVidFrames(s,u,r,struct('enforceReCreateNew', enforceReCreateNew));
                case 'unzipCroppedZip'
                    croppedPicsZipName = [theFoldName filesep 'cropped.zip'];
                    if exist(croppedPicsZipName,'file')
                        frameCnt = unzipProperly(theFoldName,'cropped');
                        disp([num2str(frameCnt) ' files extracted from ' croppedPicsZipName]);
                        removeCroppedZip = getStructField(optParamStruct, 'removeCroppedZip', false);
                        if (removeCroppedZip)
                            if (~strcmp(getVariableByComputerName('srcFold'),srcFold))
                                %we can remove cropped zip
                                delete(croppedPicsZipName);
                                disp(['removed file ' croppedPicsZipName]);
                            else
                                disp(['cropped zip not removed because it is in the source folder - ' croppedPicsZipName]);
                            end
                        end
                    else
                        disp([ croppedPicsZipName ' doesnt exist']);
                    end
                case 'createVidSummary'
                    stripImSizeVec = [25 5];
                    stripIm = createStripIm_NoPrediction(theFoldName, [], false, stripImSizeVec, [1 2 3]);
                    outVal  = 'video summary files created\r\n';
                    outVal  = [outVal 'singleImage resized to' mat2str(stripImSizeVec) '\r\n'];
                    outVal  = [outVal 'each summary size is ' mat2str(size(stripIm{1,1})) '\r\n'];
                    disp(outVal);
                case 'copyForWEB'  
                    operatePerSignStuff = sum(perSignVec==s)==0;

                    %operationStr = 'copyForWEB';
                    toWebFolderRoot = getStructField(optParamStruct, 'toWebFolder', getVariableByComputerName('toWebFold'));
                    %toWebFolderRoot = getStructField(optParamStruct, 'toWebFolder', 'D:\toWEB');
                    toWebFolderSign = [toWebFolderRoot filesep num2str(s)];%filesep num2str(s)
                    if operatePerSignStuff
                        %create the folder under
                        if ~exist(toWebFolderSign ,'dir')
                            disp(['creating folder(' num2str(s) ') under webFolder(' toWebFolderRoot ')']);
                            mkdir(toWebFolderSign);
                        end
                    end
                    toWebFolder_sur = [toWebFolderSign filesep 'User_' num2str(u) '_' num2str(r)];
                    %DOWN - here generate the necessary files
                    copyVidSummaryOption = getStructField(optParamStruct, 'copyVidSummaryOption', true);
                    if copyVidSummaryOption && ~exist([theFoldName filesep 'vidSummaryLH.png'],'file')
                        createStripIm_NoPrediction(theFoldName, [], false, [25 5], [1 2 3]);
                    end
                    copyCroppedZipOption = getStructField(optParamStruct, 'copyCroppedZipOption', true);
                    if copyCroppedZipOption && ~exist([theFoldName filesep 'cropped.zip'],'file')
                        cropVidFrames(theFoldName, [], enforceReCreateNew);
                    end
                    %UP  - here generate the necessary files
                    
                    %Sample - optParamStruct- for differet purposes
                    
                    %1-copy nothing - only create StripIm_NoPrediction and cropped.zip - if not created before
                    %optParamStruct = struct('copyAutoClusterOption', false, 'copyLabelFilesOption', false,'copyVidSummaryOption', false,'copyCroppedZipOption', false, 'copyLabelSummaryOption', false, 'copyLatexStuffOption', false, 'copyKHSnamesOption', false);
                    
                    %2- labelFiles, cropped.zip, 'summary?H.bmp'
                    %optParamStruct_copyForWEB = struct('copyTextMeaningFileOption',true,'copyAutoClusterOption', false, 'copyLabelFilesOption', true,'copyVidSummaryOption', false,'copyCroppedZipOption', true, 'copyLabelSummaryOption', true);
                    
                    %3- for running getSurfNormFeatsOfHands 
                    %defaults (false) - copyAutoClusterOption, handsStructMedoidsFiles
                    %defaults (true)  - copyLabelFilesOption, copyCroppedZipOption, copyVidSummaryOption, copyLabelSummaryOption, 
                    %                   copyLatexStuffOption, copyKHSnamesOption, copyTextMeaningFileOption
                    %                   
                    %To copy          - 
                    %Not to copy      - 
                    %otherFiles = 'color.mp4*cropArea.mat*handUsage.mat*skeleton.mat*depth.mat'
                    %optParamStruct_copyForWEB = struct('otherFiles', 'color.mp4*cropArea.mat*handUsage.mat*skeleton.mat*depth.mat');

                    copyAutoClusterOption = getStructField(optParamStruct, 'copyAutoClusterOption', false);
                    copyLabelFilesOption = getStructField(optParamStruct, 'copyLabelFilesOption', true);
                    copyLabelSummaryOption = getStructField(optParamStruct, 'copyLabelSummaryOption', true);
                    copyLatexStuffOption = getStructField(optParamStruct, 'copyLatexStuffOption', true);
                    copyKHSnamesOption = getStructField(optParamStruct, 'copyKHSnamesOption', true);
                    handsStructMedoidsFiles = getStructField(optParamStruct, 'handsStructMedoidsFiles', false);
                    copyTextMeaningFileOption = getStructField(optParamStruct, 'copyTextMeaningFileOption', true);
                    otherFiles = getStructField(optParamStruct, 'otherFiles', {});
                    %4 - defaultOps
                    %copyAutoClusterOption false
                    %copyLabelFilesOption true
                    %copyVidSummaryOption true
                    %copyCroppedZipOption true
                    %copyLabelSummaryOption true - 'summaryLH.bmp','summaryRH.bmp','summaryBH.bmp'
                    %copyLatexStuffOption true - 'signLabelsSummary_sss.txt','labelSummary.mat','latexOOSsection_sss.txt','latexSubsection_sss.txt'
                    %copyTextMeaningFileOption true
                    %copyKHSnamesOption true
                    %handsStructMedoidsFiles false
                    %optParamStruct = struct('name',val);
                    
                    %dont copy this unless given otherwise in optParamStruct
                    if copyAutoClusterOption && exist([theFoldName filesep 'autoCluster'],'dir')
                        createFolderIfNotExist(toWebFolder_sur,true);
                        fwf_copyFolder(theFoldName,toWebFolder_sur,'autoCluster','folder');
                    end

                    %copy this unless given otherwise in optParamStruct
                    if copyLabelFilesOption && exist([theFoldName filesep 'labelFiles'],'dir')
                        createFolderIfNotExist(toWebFolder_sur,true);
                        fwf_copyFolder(theFoldName,toWebFolder_sur,'labelFiles','folder');
                    end
                    
                    %copy these unless given otherwise in optParamStruct
                    if copyVidSummaryOption
                        createFolderIfNotExist(toWebFolder_sur,true);
                        for listOfFiles = {'vidSummaryLH.png','vidSummaryRH.png','vidSummaryBH.png'}
                            fwf_copyFolder(theFoldName,toWebFolder_sur,listOfFiles{1,1},'file'); 
                        end
                    end
                    
                    %copy this unless given otherwise in optParamStruct
                    if copyCroppedZipOption
                        createFolderIfNotExist(toWebFolder_sur,true);
                        fwf_copyFolder(theFoldName,toWebFolder_sur,'cropped.zip','file'); 
                    end
                    
                    %copy these unless given otherwise in optParamStruct
                    if copyLabelSummaryOption
                        createFolderIfNotExist(toWebFolder_sur,true);
                        for listOfFiles = {'summaryLH.bmp','summaryRH.bmp','summaryBH.bmp'} %'color.mp4','markedVideo.avi'
                            if exist([theFoldName filesep listOfFiles{1}],'file')
                                fwf_copyFolder(theFoldName,toWebFolder_sur,listOfFiles{1},'file'); 
                            end
                        end
                    end
                     
                    %copy unless given otherwise in optParamStruct
                    if copyLatexStuffOption && operatePerSignStuff
                        for listOfFiles = {['signLabelsSummary_' num2str(s,'%03d') '.txt'],'labelSummary.mat',['latexOOSsection_' num2str(s,'%03d') '.txt'],['latexSubsection_' num2str(s,'%03d') '.txt']} %
                            if exist([srcFold filesep num2str(s) filesep listOfFiles{1}],'file')
                                fwf_copyFolder([srcFold filesep num2str(s)],toWebFolderSign,listOfFiles{1},'file'); 
                            end
                        end
                    end
                    
                    %copy these unless given otherwise in optParamStruct
                    if copyKHSnamesOption && operatePerSignStuff
                        for listOfFiles = {'clustersCS_Both.txt','clustersCS_Single.txt'} %
                            if exist([srcFold filesep num2str(s) filesep listOfFiles{1}],'file')
                                fwf_copyFolder([srcFold filesep num2str(s)],toWebFolderSign,listOfFiles{1},'file'); 
                            end
                        end
                    end
                    
                    %copy these unless given otherwise in optParamStruct
                    if copyTextMeaningFileOption && operatePerSignStuff
                        textMeaningFile = ['textMeaning_' num2str(s,'%03d') '.txt'];
                        fwf_copyFolder([srcFold filesep num2str(s)],toWebFolderSign,textMeaningFile,'file'); 
                    end
                    
                    %copy if any 'handsStructMedoidsFile' exist 
                    if handsStructMedoidsFiles && operatePerSignStuff
                        %handsStructMedoids_007_hf_mc_0_assignedLabelsRemove1.png
                        singFolder = [srcFold filesep num2str(s)];
                        fileNameInit = ['handsStructMedoids_' num2str(s,'%03d') '_' ];
                        disp(['Searching for ' fileNameInit '?']);
                        [fileNames, numOfFiles] = getFileList(singFolder, '', fileNameInit , false);
                        for i=1:numOfFiles
                            disp(['copying ' fileNames{i}]);
                            fwf_copyFolder(singFolder, toWebFolderSign, fileNames{i},'file'); 
                        end
                        
                        %step04_accuracyListAll_007_hf_mc_0.mat
                        %step04_accuracyListAll_007_hf_mc_0.txt
                        accuracyListAllfilenameInit  = ['step04_accuracyListAll_' num2str(s,'%03d') '_' ];
                        [fileNames, numOfFiles] = getFileList(singFolder, '', accuracyListAllfilenameInit , false);
                        for i=1:numOfFiles
                            disp(['copying ' fileNames{i}]);
                            fwf_copyFolder(singFolder, toWebFolderSign, fileNames{i},'file'); 
                        end
                        clear singFolder accuracyListAllfilenameInit fileNameInit
                    end

                    %copy these unless given otherwise in optParamStruct
                    %otherFiles = 'cropArea.mat*handUsage.mat*skeleton.mat'
                    if ~isempty(otherFiles)
                        createFolderIfNotExist(toWebFolder_sur,true);
                        otherFiles = strsplit(otherFiles,'*');
                        for listOfFiles = otherFiles
                            fwf_copyFolder(theFoldName, toWebFolder_sur,listOfFiles{1},'file'); 
                        end
                    end
                    
                    if operatePerSignStuff
                        perSignVec = [perSignVec s];
                    end
                case {'resetLabels'}
                    %optParamStruct = struct('oldLabelsVec', [1 2 3 4 5 6 7], 'newLabelsVec', [1 2 3 4 5 5 6], 'single0_double1', 0, 'copyToFolder', 'D:\toWEB');
                    %when labels are to be resetted
                    oldLabelsVec = getStructField(optParamStruct, 'oldLabelsVec', []);
                    newLabelsVec = getStructField(optParamStruct, 'newLabelsVec', []);
                    single0_double1 = getStructField(optParamStruct, 'single0_double1', 0);
                    copyToFolder = getStructField(optParamStruct, 'copyToFolder', 'D:\newLabels');

                    %we need - 'oldLabels, new labels'
                    %also single or bh is needed too
                    destFileStruct_L = struct('rootFold',copyToFolder, 's', s, 'u', u, 'r', r, 'handChar', 'L');
                    destFileStruct_R = struct('rootFold',copyToFolder, 's', s, 'u', u, 'r', r, 'handChar', 'R');
                    destFileStruct_B = struct('rootFold',copyToFolder, 's', s, 'u', u, 'r', r, 'handChar', 'B');
                    
                    if (single0_double1==0)
                        lhFile = [theFoldName filesep 'labelFiles' filesep 'labelsCS_LH.txt'];
                        rhFile = [theFoldName filesep 'labelFiles' filesep 'labelsCS_RH.txt'];
                        changeLabelsInFile(lhFile, oldLabelsVec, newLabelsVec, destFileStruct_L);
                        changeLabelsInFile(rhFile, oldLabelsVec, newLabelsVec, destFileStruct_R);
                    else
                        bhFile = [theFoldName filesep 'labelFiles' filesep 'labelsCS_BH.txt'];
                        changeLabelsInFile(bhFile, oldLabelsVec, newLabelsVec, destFileStruct_B); 
                    end
                case 'all'
                    cropVidFrames(theFoldName, [], enforceReCreateNew);
                    getHOGFeatsofHands(srcFold, s, u, r, [], enforceReCreateNew);
                    calcSurfNormOfVidFrames(theFoldName, [20 20], [], enforceReCreateNew);
                    getSurfNormFeatsofHands([s, u, r], struct('srcFold',srcFold));      
            end
            toc;
        catch err
            disp('ERROR = ')
            disp(err)
            problematicVids = [problematicVids;s u r]; %#ok<AGROW>
        end
    end
end

function changeLabelsInFile(fileName, oldLabelsVec, newLabelsVec, destFileStruct)
    if (~exist(fileName,'file'))
        disp(['file(' fileName ') dosent exist. No changes made..'])
        return
    end
    
    fInfo = dir(fileName);
    
    if ~isempty(destFileStruct)
        toUserRepFolder = createFolderForOutput(destFileStruct.rootFold, destFileStruct.s, destFileStruct.u, destFileStruct.r);
        toLabelsFolder = [toUserRepFolder filesep 'labelFiles'];
        if ~exist(toLabelsFolder,'dir')
            mkdir(toLabelsFolder);
        end  
        fileNameBackup = [toLabelsFolder filesep 'labelsCS_' destFileStruct.handChar 'H_' datestr(fInfo.datenum,'yyyymmdd') '_' datestr(fInfo.datenum,'HHMM') '.txt'];
        fileNameToWrite = [toLabelsFolder filesep 'labelsCS_' destFileStruct.handChar 'H.txt'];
    else
        fileNameBackup = strrep(fileName,'.txt',['_' datestr(fInfo.datenum,'yyyymmdd') '_' datestr(fInfo.datenum,'HHMM') '.txt']);        
        fileNameToWrite = fileName;
    end
    
    
    labelsVec = getLabelVecFromFile(fileName);
    labelsVecChanged = reSetLabels( labelsVec, oldLabelsVec, newLabelsVec);
    
    try
        copyfile(fileName, fileNameBackup);
        disp(['file(' fileNameBackup ') is now a valid backup..'])
    catch
        disp(['xxxfile(' fileNameBackup ') could not get this backup..'])
    end
    
    try 
        fileID = fopen(fileNameToWrite,'w','n','windows-1253');
        fprintf(fileID,'%d\r\n',labelsVecChanged);
        fclose(fileID);
        disp(['file(' fileNameToWrite ') is updated with new labels..'])
    catch 
        disp(['xxxfile(' fileNameToWrite ') could not be updated with new labels..'])
    end
end

function toUserRepFolder = createFolderForOutput(toDestFolder, s, u, r)
    if ~exist(toDestFolder ,'dir')
        disp(['creating folder(' toDestFolder ')']);
        mkdir(toDestFolder);
    end

    toSignFolder = [toDestFolder filesep num2str(s)];%filesep num2str(s)
    if ~exist(toSignFolder ,'dir')
        disp(['creating folder(' num2str(s) ') under toDestFolder(' toDestFolder ')']);
        mkdir(toSignFolder);
    end
    
    toUserRepFolder = [toSignFolder filesep 'User_' num2str(u) '_' num2str(r)];
    if ~exist(toUserRepFolder ,'dir')
        disp(['creating folder(' 'User_' num2str(u) '_' num2str(r) ')']);
        mkdir(toUserRepFolder);
    end
end

function fwf_copyFolder(srcFold, destFold, nameToCopy, fileOrFolder)
    switch fileOrFolder
        case 'folder'
            folderToCreate = [destFold filesep nameToCopy];
            if ~exist(folderToCreate,'dir')
                mkdir(folderToCreate);
            end
            [status,msg] = copyfile([srcFold filesep nameToCopy], [destFold filesep nameToCopy],'f');
        case 'file'
            [status,msg] = copyfile([srcFold filesep nameToCopy], destFold,'f');
    end
    
    if status==0
        disp(['could not copy ' fileOrFolder ' - ' nameToCopy ' - msg(' msg ')']);   
    else
        disp([fileOrFolder ' - ' nameToCopy ' - copied into ' destFold]);   
    end
end


