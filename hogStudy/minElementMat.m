function [ row_i, col_j, treshBox ] = minElementMat( M, treshMul, treshVal)
%minElementMat finds the minimum elemnt of matrix
%   Can also return the acceptable block area depending on 
    [minVal, minInds] = min(M(:));
    [row_i, col_j] = ind2sub(size(M), minInds);
    treshBox = [];
    if nargin>1
        treshVal_current = min(minVal*treshMul, treshVal(2));
        treshVal_current = max(treshVal_current, treshVal(1));
        %now walk on + direction until min(minVal*treshMul, treshVal)
        row_i_plus = max([0,find(M(row_i:end,col_j)>treshVal_current,1)]);
        row_i_minus = max([0,find(M(row_i:-1:1,col_j)>treshVal_current,1)]);
        col_j_plus = max([0,find(M(row_i,col_j:end)>treshVal_current,1)]);
        col_j_minus = max([0,find(M(row_i,col_j:-1:1)>treshVal_current,1)]);

        row_i_beg = max(1,row_i-row_i_minus);
        row_i_end = min(row_i+row_i_plus, size(M,1));
        col_j_beg = max(1,col_j-col_j_minus);
        col_j_end = min(col_j+col_j_plus, size(M,2));
        treshBox = [row_i_beg row_i_end col_j_beg col_j_end];
    end
end

