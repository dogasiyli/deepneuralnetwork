function movedFiles = moveFiles(signIDList, userIDList, actionStruct, optParamStruct)
    %go through folders in srcFold according to <signIDList, userIDList>
    %depending on the actionStr
    %move the files to backUp or restore
    
    %movedFiles = moveFiles(signIDList, [], struct('HOGFeat','backup'), struct('srcFold', srcFold, 'backupFold', backupFold))
    
    %actionStruct e.g. = %/struct('HOGFeat','backup');/struct('SurfNormFeat','backup');/struct('LabelFiles','restore');
    %                      struct('deleteSource',true,'overWrite',false)
    %
    %optParamStruct e.g. =  struct('backupFold', 'M:\')
    
    %e.g. moveFiles([7],2:7, struct('HOGFeat','backup','SurfNormFeat','backup'), struct('srcFold', 'C:\FromUbuntu\HospiSign\HospiMOV'), struct('backupFold', 'T:\'))
    %e.g. moveFiles([7],2:7, struct('HOGFeat','restore','SurfNormFeat','restore'), struct('srcFold', 'C:\FromUbuntu\HospiSign\HospiMOV'), struct('backupFold', 'T:\'))
    %e.g. moveFiles([7],2:7, struct('LabelFiles','backup'), struct('srcFold', '/mnt/Data/FromUbuntu/HospiSign/HospiMOV'), struct('backupFold', 'T:\'))
    
    if ~exist('optParamStruct','var')
        optParamStruct = [];
    end
    if ~isfield(actionStruct,'HOGFeat')
        actionStruct.HOGFeat = '';
    end
    if ~isfield(actionStruct,'SurfNormFeat')
        actionStruct.SurfNormFeat = '';
    end
    if ~isfield(actionStruct,'LabelFiles')
        actionStruct.LabelFiles = '';
    end
    if ~isfield(actionStruct,'CroppedFiles')
        actionStruct.CroppedFiles = '';
    end
    if isempty(userIDList)
        userIDList = 2:7;
    end
    
    srcFold = getStructField(optParamStruct, 'srcFold', getVariableByComputerName('srcFold'));
    backupFoldRoot = getStructField(optParamStruct, 'backupFold', getVariableByComputerName('backupFold'));
    vidCount = getStructField(optParamStruct, 'vidCount', 20);
    repIDVec = getStructField(optParamStruct, 'repIDVec', 1:20);
       
    dstFold = backupFoldRoot;
    if ~exist(dstFold,'dir')
        try
            mkdir(dstFold);
        catch
            disp(['could not create <' dstFold '> folder']);
            return
        end
    end
    movedFiles = [];
    for signID = signIDList
        for userID = userIDList
            ri = 1;
            vidProcessed = 0;
            while ri <= max(repIDVec) && vidProcessed < vidCount
                
                repID = repIDVec(ri);
                ri = ri + 1;
                
                userFolderStr = [num2str(signID) filesep 'User_' num2str(userID) '_' num2str(repID) filesep];
                srcFoldUserRep = [srcFold filesep userFolderStr];
                if ~exist(srcFoldUserRep,'dir')
                    continue
                end
                resVec = [0 0 0];
                %the idea is to move either hogCropMats or surfNormCropMats
                %to a different area 
                if strcmp(actionStruct.HOGFeat,'backup')
                    %take the <hog_crop.mat> from <str_fr> folder
                    %rename it to <hog_crop_s007_u2_r01.mat>
                    %move it to backupFolder
                    defaultBoolsStruct = struct('deleteSource',true,'overWrite',false);
                    [overWrite, deleteSource] = getExtraOptionsFromStruct(actionStruct, defaultBoolsStruct);
                    resVec(1) = backupFile(srcFoldUserRep,dstFold,'hog_crop',signID,userID,repID, overWrite, deleteSource);
                elseif strcmp(actionStruct.HOGFeat,'restore')
                    %move it to backupFolder                    
                    %if exist - take the <hog_crop_s007_u2_r01.mat> from backupFolder
                    %rename it to <hog_crop.mat> and move to <str_fr> folder
                    defaultBoolsStruct = struct('deleteSource',false,'overWrite',false);
                    [overWrite, deleteSource] = getExtraOptionsFromStruct(actionStruct, defaultBoolsStruct);
                    resVec(1) = restoreFile(srcFoldUserRep,dstFold,'hog_crop',signID,userID,repID, overWrite, deleteSource);
                end
                if strcmp(actionStruct.SurfNormFeat,'backup')
                    %take the <surfNorm_crop.mat> from <str_fr> folder
                    %rename it to <surfNorm_crop_s007_u2_r01.mat>
                    %move it to backupFolder
                    defaultBoolsStruct = struct('deleteSource',true,'overWrite',false);
                    [overWrite, deleteSource] = getExtraOptionsFromStruct(actionStruct, defaultBoolsStruct);
                    resVec(2) = backupFile(srcFoldUserRep,dstFold,'surfNorm_crop',signID,userID,repID, overWrite, deleteSource);
                elseif strcmp(actionStruct.SurfNormFeat,'restore')
                    %move it to backupFolder                    
                    %if exist - take the <surfNorm_crop_s007_u2_r01.mat> from backupFolder
                    %rename it to <surfNorm_crop.mat> and move to <str_fr> folder
                    defaultBoolsStruct = struct('deleteSource',false,'overWrite',false);
                    [overWrite, deleteSource] = getExtraOptionsFromStruct(actionStruct, defaultBoolsStruct);
                    resVec(2) = restoreFile(srcFoldUserRep,dstFold,'surfNorm_crop',signID,userID,repID, overWrite, deleteSource);
                end
                if strcmp(actionStruct.LabelFiles,'backup')
                    %take the <labelFiles folder> from <str_fr> folder
                    %rename it to <labelFiles_s007_u2_r01>
                    %move it to backupFolder
                    [overWrite, deleteSource] = getExtraOptionsFromStruct(actionStruct, struct('deleteSource',false,'overWrite',false));
                    resVec(3) = backupLabels(srcFoldUserRep,dstFold,signID,userID,repID, overWrite, deleteSource);
                elseif strcmp(actionStruct.LabelFiles,'restore')
                    %move it to backupFolder                    
                    %if exist - take the <labelFiles_s007_u2_r01> from backupFolder
                    %rename it to <labelFilest> and move to <str_fr> folder
                    [overWrite, deleteSource] = getExtraOptionsFromStruct(actionStruct, struct('deleteSource',false,'overWrite',false));
                    resVec(3) = restoreLabels(srcFoldUserRep,dstFold,signID,userID,repID, overWrite, deleteSource);
                elseif strcmp(actionStruct.LabelFiles,'remainLatest')
                    %check both backup and source files for labels                  
                    %if one of them exist - backup or restore accordingly
                    %if non exist skip
                    %if both exist update to the latest whichever needs
                    %updating and copy the deleted file with its timeStamp
                    %to its location
                    resVec = uptoDateLabels(srcFoldUserRep,dstFold,signID,userID,repID);
                end
                if strcmp(actionStruct.CroppedFiles,'createForCopy')
                    error('not implemented yet')
                    %take the <cropped.zip> file
                    %rename it to <labelFiles_s007_u2_r01>
                    %move it to backupFolder
                    [overWrite, deleteSource] = getExtraOptionsFromStruct(actionStruct, struct('deleteSource',false,'overWrite',false));
                    resVec(3) = backupLabels(srcFoldUserRep,dstFold,signID,userID,repID, overWrite, deleteSource);
                end
                movedFiles = [movedFiles;signID userID repID resVec]; %#ok<*AGROW>
                vidProcessed = vidProcessed + 1;
            end
        end
    end
    if ~isempty(movedFiles)
        if (strcmp(actionStruct.LabelFiles,'remainLatest'))
            disptable(movedFiles,'signID|userID|repID|LH|RH|BH')
            printExplanationSummary(movedFiles);
        else
            disptable(movedFiles,'signID|userID|repID|hogAct|surfAct|labels')
        end
    end
end

function r = backupFile(srcFold,backupFoldRoot,fileRootName,signID,userID,repID, overWrite, deleteSource)
    fileToLoad = [srcFold fileRootName '.mat'];
    newFileName = [fileRootName '_s' num2str(signID,'%03d') '_u' num2str(userID) '_r' num2str(repID,'%02d')];
    
    backupFolder = getExactBackupFolder(backupFoldRoot, 'Feats');
    fileToSave = [backupFolder filesep newFileName '.mat'];
    
    r=0;
    try
        proceedToCopy = true;
        if (overWrite==false)
            if exist(fileToSave,'file')
                disp(['backup(copy) skipped because it exists and overwrite is false<' newFileName '>'])
                proceedToCopy = false;
            end
        end
        if (proceedToCopy)
            copyfile(fileToLoad,fileToSave,'f');
            disp(['backup(copy) completed<' newFileName '>'])
        end
    catch err
        disp(['xxx backup(copy) FAILED<' newFileName '>. err(' err.message ')'])
        return
    end
    if (deleteSource)
        try
            fileExists = exist(fileToLoad,'file');
            if (fileExists==2)
                fileDeleted = deleteFile_IfOtherExistsAndSame(fileToLoad, fileToSave);
                if (fileDeleted)
                    disp(['backup(deleteSource) completed<' fileRootName '>'])
                    r = -100;%deleted because same source exist
                    return
                end
            else
                disp(['Skipping deletion of backup(deleteSource) for <' fileRootName '>'])
                r = -10;%skipping deletion because source doesnt exist
                return
            end
        catch
            disp(['xxx backup(deleteSource) FAILED<' fileRootName '> - err(' err.message ')'])
            r = -1;%skipping deletion because of some error
            return
        end
    else
        disp('backup(deleteSource) skipped')
    end
    r = 1;
end

function r = deleteFile_IfOtherExistsAndSame(fileToDelete, fileToExistAndBeSame)
    r = false;
    try
        fileInfo_del = dir(fileToDelete);%toDelete
        fileInfo_bck = dir(fileToExistAndBeSame);%will stay as backup
        if (fileInfo_del.datenum==fileInfo_bck.datenum)
            delete(fileToDelete);
            r = true;
        else
            disp(['Non-similar files according to dates. toDel(' fileToDelete '), asBckup(' fileToExistAndBeSame ')'])
        end    
    catch err
        disp(['Couldnt delete (' fileToDelete ') because--> ' err.message])
    end
end

function r = backupLabels(srcFoldUserRep,backupFoldRoot,signID,userID,repID, overWrite, deleteSource)
    labelFolderNearVideos = [srcFoldUserRep 'labelFiles'];
    folderNameForBackup = ['labelFiles' '_s' num2str(signID,'%03d') '_u' num2str(userID) '_r' num2str(repID,'%02d')];
    
    backupFolder = getExactBackupFolder(backupFoldRoot, 'Labels');
    folderToSave = [backupFolder filesep folderNameForBackup];
    r=0;
    try
        if (overWrite==false)
            if exist(folderToSave,'dir')
                disp(['backup(copy) skipped because it exists and overwrite is false<' folderNameForBackup '>'])
                return
            end
        end
        copyfile(labelFolderNearVideos,folderToSave,'f');
        disp(['backup(copy) completed<' folderNameForBackup '>'])
    catch
        disp(['xxx backup(copy) FAILED<' folderNameForBackup '>'])
        return
    end
    if (deleteSource)
        try
            delete(labelFolderNearVideos);
            disp(['backup(deleteSource) completed<' strrep(folderNameForBackup,'_','-') '>'])
        catch
            disp(['xxx backup(deleteSource) FAILED<' strrep(folderNameForBackup,'_','-') '>'])
        end
    else
        disp('backup(deleteSource) skipped')
    end
    r = 1;
end

function r = restoreLabels(srcFoldUserRep,backupFoldRoot,signID,userID,repID, overWrite, deleteSource)
    labelFolderNearVideos = [srcFoldUserRep 'labelFiles'];
    backedUpFolderName = ['labelFiles' '_s' num2str(signID,'%03d') '_u' num2str(userID) '_r' num2str(repID,'%02d')];
    
    backupFolder = getExactBackupFolder(backupFoldRoot, 'Labels');
    folderToLoad = [backupFolder filesep backedUpFolderName];
    r = 0;
    try
        if (overWrite==false)
            if exist(labelFolderNearVideos,'dir')
                disp(['restore(copy) skipped because it exists and overwrite is false<' labelFolderNearVideos '>'])
                return
            end
        end
        copyfile(folderToLoad,labelFolderNearVideos,'f');
        disp(['restore(copy) completed<' strrep(backedUpFolderName,'_','-') '>'])
    catch
        disp(['xxx restore(copy) FAILED<' strrep(backedUpFolderName,'_','-') '>'])
        return
    end
    if (deleteSource)
        try
            rmdir(folderToLoad);
            disp(['restore(deleteBackup) completed<' strrep(backedUpFolderName,'_','-') '>'])
        catch
            disp(['xxx restore(deleteBackup) FAILED<' strrep(backedUpFolderName,'_','-') '>'])
        end
    else
        disp('restore(deleteBackup) skipped')
    end
    r = -1;
end

function rVec = uptoDateLabels(srcFoldUserRep,backupFoldRoot,signID,userID,repID)
    userSourceLabelFolder = [srcFoldUserRep 'labelFiles'];
    backedUpFolderName = ['labelFiles' '_s' num2str(signID,'%03d') '_u' num2str(userID) '_r' num2str(repID,'%02d')];
    
    backupFolder = getExactBackupFolder(backupFoldRoot, 'Labels');
    userBackupFolder = [backupFolder filesep backedUpFolderName];
    rVec = [0 0 0];
    
    try
        existSrc = exist(userSourceLabelFolder,'dir');
        existBck = exist(userBackupFolder,'dir');
        
        %here I need to check for left right and both label files
        if (existSrc==7 && existBck~=7)
            %only source exist and backup doesnt exist
            copyfile(userSourceLabelFolder,userBackupFolder,'f');
            disp(['backup completed<' userBackupFolder '>'])
            rVec = [100 100 100];
            return
        elseif (existSrc~=7 && existBck==7)
            %only backup exist and source doesnt exist
            copyfile(userBackupFolder,userSourceLabelFolder,'f');
            disp(['restore completed<' strrep(userSourceLabelFolder,'_','-') '>'])
            rVec = [-100 -100 -100];
            return
        elseif (existSrc~=7 && existBck~=7)
            %neither backup nor source exists
            disp(['neither source nor backup label folder for s' num2str(signID,'%03d') '_u' num2str(userID) '_r' num2str(repID,'%02d') ' exist. Skipping.'])
            rVec = [0 0 0];
            return
        elseif (existSrc==7 && existBck==7)
            %both source and backup exist
            %do nothing
        end
        %check label for left hand
        handFileName_lh_src = [userSourceLabelFolder filesep 'labelsCS_LH.txt'];
        handFileName_lh_bck = [userBackupFolder filesep 'labelsCS_LH.txt'];
        rVec(1) = uptoDateHandLabelFile(handFileName_lh_src, handFileName_lh_bck);
        
        handFileName_rh_src = [userSourceLabelFolder filesep 'labelsCS_RH.txt'];
        handFileName_rh_bck = [userBackupFolder filesep 'labelsCS_RH.txt'];
        rVec(2) = uptoDateHandLabelFile(handFileName_rh_src, handFileName_rh_bck);
        
        handFileName_bh_src = [userSourceLabelFolder filesep 'labelsCS_BH.txt'];
        handFileName_bh_bck = [userBackupFolder filesep 'labelsCS_BH.txt'];
        rVec(3) = uptoDateHandLabelFile(handFileName_bh_src, handFileName_bh_bck);
        return
    catch
        disp(['xxx uptoDateLabels FAILED<' strrep(backedUpFolderName,'_','-') '>'])
        return
    end
    rVec = [-1 -1 -1];
end

function r = uptoDateHandLabelFile(srcFileName, bckFileName)
    existSrc = exist(srcFileName,'file');
    existBck = exist(bckFileName,'file');
    r = 0;
    %here I need to check for file existances
    if (existSrc==2 && existBck~=2)
        %only source exist and backup doesnt exist
        copyfile(srcFileName,bckFileName,'f');
        disp(['source(' srcFileName ') backed up (' bckFileName ')'])
        r = 51;
        return
    elseif (existSrc~=2 && existBck==2)
        %only backup exist and source doesnt exist
        copyfile(bckFileName,srcFileName,'f');
        disp(['backup(' bckFileName ') restored (' srcFileName ')'])
        r = 49;
        return
    elseif (existSrc~=2 && existBck~=2)
        %neither backup nor source exists
        warning(['neither source(' srcFileName ') nor backup(' bckFileName ') exist. Skipping.'])
        r = 0;
        return
    elseif (existSrc==2 && existBck==2)
        %both source and backup exist
        %check which is older
        fileInfo_src = dir(srcFileName);
        fileInfo_bck = dir(bckFileName);
        if (fileInfo_src.datenum>fileInfo_bck.datenum)
            %source is older than backup
            %copy backup.txt as backup_bY_bM_bD_bH_bMN_bS.txt
            %copy src.txt as backup.txt
            [bY, bM, bD, bH, bMN, bS] = datevec(fileInfo_bck.datenum); 
            backInt = bS + bMN*100 + bH*10000 + bD*1000000 + bM*100000000 + bY*10000000000;
            bckOld = strrep(bckFileName, '.txt', ['_' num2str(backInt,'%d') '.txt']);
            retStr = getHowOlderAsString(fileInfo_src, fileInfo_bck);
            disp(['source file = ' fileInfo_src.date])
            disp(['backup file = ' fileInfo_bck.date])
            disp(retStr)
            copyfile(bckFileName,bckOld,'f');
            copyfile(srcFileName,bckFileName,'f');
            disp(['newerSource - backup(' bckFileName ') updated with source (' srcFileName ')'])
            r = 52;
        elseif (fileInfo_src.datenum<fileInfo_bck.datenum)
            %backup is older than source
            %copy src.txt as src_sY_sM_sD_sH_sMN_bS.txt
            %copy backup.txt as src.txt            
            [sY, sM, sD, sH, sMN, sS] = datevec(fileInfo_src.datenum);  
            srcInt = sS + sMN*100 + sH*10000 + sD*1000000 + sM*100000000 + sY*10000000000;
            srcOld = strrep(srcFileName, '.txt', ['_' num2str(srcInt,'%d') '.txt']);
            retStr = getHowOlderAsString(fileInfo_src, fileInfo_bck);
            disp(['backup file = ' fileInfo_bck.date])
            disp(['source file = ' fileInfo_src.date])
            disp(retStr)
            copyfile(srcFileName,srcOld,'f');
            copyfile(bckFileName,srcFileName,'f');
            disp(['newerBackup - source(' srcFileName ') updated with backup (' bckFileName ')'])
            r = 48;
        elseif (fileInfo_src.datenum==fileInfo_bck.datenum)
            %backup is same as source 
            %dont do anthing
            %disp(['same datevecnum - source(' srcFileName ') and backup (' bckFileName ')'])
            r = 50;
        end
    end    
end

function r = restoreFile(srcFold,backupFoldRoot,fileRootName,signID,userID,repID,overWrite,deleteSource)
    fileToSave = [srcFold fileRootName '.mat'];
    oldFileName = [fileRootName '_s' num2str(signID,'%03d') '_u' num2str(userID) '_r' num2str(repID,'%02d')];
    
    backupFolder = getExactBackupFolder(backupFoldRoot, 'Feats');
    fileToLoad = [backupFolder filesep oldFileName '.mat'];
    r = 0;
    try
        if (overWrite==false)
            if exist(fileToSave,'file')
                disp(['restore(copy) skipped because it exists and overwrite is false<' oldFileName '>'])
                return
            end
        end
        copyfile(fileToLoad,fileToSave,'f');
        disp(['restore(copy) completed<' oldFileName '>'])
    catch
        disp(['xxx restore(copy) FAILED<' oldFileName '>'])
        return
    end
    if (deleteSource)
        try
            delete(fileToLoad);
            disp(['restore(deleteBackup) completed<' oldFileName '>'])
        catch
            disp(['xxx restore(deleteBackup) FAILED<' oldFileName '>'])
        end
    else
        disp('restore(deleteBackup) skipped')
    end
    r = -1;
end

function [overWrite, deleteSource] = getExtraOptionsFromStruct(actionStruct, defaultBoolsStruct)
    %defaultBoolsStruct = struct('deleteSource',true,'overWrite',false);
    if isfield(actionStruct,'deleteSource')
        deleteSource = actionStruct.deleteSource;
    else
        deleteSource = defaultBoolsStruct.deleteSource;        
    end
    if isfield(actionStruct,'overWrite')
        overWrite = actionStruct.overWrite;
    else
        overWrite = defaultBoolsStruct.overWrite;        
    end
end







