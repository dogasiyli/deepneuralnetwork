function [minDistVal, minDistInds, patchID_im01, patchID_im02, distOfHogs] = pdist_hogV2(hog01_extrapolated,hog02_extrapolated, hog01_rowColMap, hog02_rowColMap)
    distOfHogs = pdist2_fast(hog01_extrapolated,hog02_extrapolated);
    [distVals, inds] = sort(distOfHogs(:));
    minDistVal = distVals(1);
    if nargout==1
        minDistInds = [];
        patchID_im01 = [];
        patchID_im02 = [];
        return
    end
    [leftImID, rightImID] = ind2sub(size(distOfHogs),inds(1));
    minDistInds = [inds(1), leftImID, rightImID];
    patchID_im01 = hog01_rowColMap(leftImID,:);
    patchID_im02 = hog02_rowColMap(rightImID,:);           
end