function [ h ] = plotClusterProposes(clusterProbsVec, clusterCountToPropose, clusterNamesCellVec, figID, subplotID, titleAddStr, bestMatchFineCrop)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

    h = figure(figID);
    if subplotID==1
        clf;
        hold on;
    end
    if exist('bestMatchFineCrop','var')
        subplot(2,2,subplotID);
        rowCnt = numel(bestMatchFineCrop);
        rowCnt = sqrt(rowCnt/3);
        bestMatchFineCrop = reshape(bestMatchFineCrop,[rowCnt rowCnt 3]);
        imshow(bestMatchFineCrop);
        subplot(2,2,subplotID+2);
    else
        subplot(1,2,subplotID);
    end
    bar(1:clusterCountToPropose,clusterProbsVec(1:clusterCountToPropose,5));
    setXYTicks(gca, 1:clusterCountToPropose, clusterProbsVec(1:clusterCountToPropose,1), 45, 'Xtick');
    ylim([0 max(1, max(clusterProbsVec(1:clusterCountToPropose,5)))]);
    yTickVec = sort(unique([min(clusterProbsVec(1:clusterCountToPropose,5)) max(clusterProbsVec(1:clusterCountToPropose,5))]));
    set(gca,'YTick',yTickVec);
    set(gca,'XTickLabel',clusterNamesCellVec);
    title(['Proposed clus for ' titleAddStr ' cntr']);
    ylabel('posterior val');
end

