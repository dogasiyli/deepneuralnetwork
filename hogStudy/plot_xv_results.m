function plot_xv_results( OPS )
    if ~exist('OPS', 'var') || ~isstruct(OPS)
        OPS = [];
    end
    fold = getStructField(OPS, 'fold', '');
    nos = getStructField(OPS, 'nos', 11);
    k_fold_cnt = getStructField(OPS, 'k_fold_cnt', 5);
    rand_seed = getStructField(OPS, 'rand_seed', -1);
    put_title = getStructField(OPS, 'put_title', false);
    set_ident_str = getStructField(OPS, 'set_ident_str', '');
    mrk_size = getStructField(OPS, 'mrk_size', 300);
    char_size = getStructField(OPS, 'char_size', 40);
    colored = getStructField(OPS, 'colored', false);
    ylim_op = getStructField(OPS, 'ylim_op', 'auto');
    
    if (strcmpi(fold,'') && (nos==11 || nos==41))
        fold = generate_fold_name(nos);
    end
    if put_title && strcmpi(set_ident_str, '') && (nos==11 || nos==41)
        set_ident_str = get_ident_str(nos);
    end
    
    set(gcf, 'renderer', 'painter');%opengl, ZBuffer
    
    save_result_csv_fold = strrep(fold, [filesep 'imgs'], [filesep 'elm_results']);
    if  k_fold_cnt==5 && rand_seed<0
        fnstr = 'su';
    else
        fnstr = ['rs' num2str(rand_seed)];
    end
    result_csv_file_name = ['resultTable_' fnstr '.csv'];
    result_csv_file_name = [save_result_csv_fold filesep result_csv_file_name];
    %T = cell2table(result_cells(2:end,:),'VariableNames',result_cells(1,:));
    T = readtable(result_csv_file_name);
    [rc,~] = size(T);
    
    xd = 2;
    xuv = xd:xd:xd*6;
    hor_dist = xd*0.15;
    side_dif = xd/4;
    best_txt_up_dif = 4;
    
    hv = [true true true true];
    h = figure(2);clf;hold on;
    %maximizeFigureWindow(h);
    
    %1. scatter each result
    %   best ist *, others diamond - red
    %   mention best acc with identifier
    for c=3:8
        tu_id = c-1;
        [user_best_acc, user_best_row] = max(eval(['T.u' num2str(tu_id)]));
        best_ident_str = [char(strrep(T{user_best_row,1},'o',''))];% '_{' num2str(T{user_best_row,2}) '}'];
        txt_to_disp = [best_ident_str '_{(' num2str(user_best_acc,'%4.2f') ')}'];
        [xt, ha_str] = get_horizontal_allignment(c,[3,8], xuv, hor_dist);
        text(xt,user_best_acc+best_txt_up_dif,txt_to_disp,'Color','black','FontSize',char_size,'HorizontalAlignment',ha_str);
        for r= 1:rc 
            acc_plot = T{r,['u' num2str(tu_id)]};
            ident_str = strrep(T{r,1},'o','');
            cbv = [contains(ident_str,'hg') contains(ident_str,'sn') contains(ident_str,'sk')];
            %if user_best_row==r
            %    scatter(tu_id,acc_plot,150,'dg','filled','HandleVisibility','off');
            %else
            line_x = [];
            if cbv(1)%hog-red
                xp = xuv(c-2)-hor_dist;
                si = get_scatter_ident(colored, 1);
                hv = plot_single_scatter(xp, acc_plot, hv, 1, si,'hog feat used',mrk_size, colored);%ps^
                line_x = [line_x xp];
            end
            if cbv(2)%
                xp = xuv(c-2);
                si = get_scatter_ident(colored, 2);
                hv = plot_single_scatter(xp, acc_plot, hv, 2, si,'sn feat used',mrk_size, colored);
                line_x = [line_x xp];
            end
            if cbv(3)
                xp = xuv(c-2)+hor_dist;
                si = get_scatter_ident(colored, 3);
                hv = plot_single_scatter(xp, acc_plot, hv, 3, si,'skeleton feat used',mrk_size, colored);
                line_x = [line_x xp];
            end
            %end
            if hv(4) && sum(cbv)==3
                hv(4) = false;
                plot([min(line_x) max(line_x)],[acc_plot acc_plot],'-k','DisplayName','combined experiment','LineWidth',1);
            else
                if colored
                    colorVec = 1-cbv;
                else
                    colorVec = [0 0 0];
                end
                plot([min(line_x) max(line_x)],[acc_plot acc_plot],'-','Color',colorVec,'HandleVisibility','off','LineWidth',1);
            end
            if cbv(1)%hog-red
                xp = xuv(c-2)-hor_dist;
                si = get_scatter_ident(colored, 1);
                hv = plot_single_scatter(xp, acc_plot, hv, 1, si,'hog feat used',mrk_size, colored);%ps^
            end
            if cbv(2)%
                xp = xuv(c-2);
                si = get_scatter_ident(colored, 2);
                hv = plot_single_scatter(xp, acc_plot, hv, 2, si,'sn feat used',mrk_size, colored);
            end
            if cbv(3)
                xp = xuv(c-2)+hor_dist;
                si = get_scatter_ident(colored, 3);
                hv = plot_single_scatter(xp, acc_plot, hv, 3, si,'skeleton feat used',mrk_size, colored);
            end            
        end
    end
    xlim([xd-side_dif  6*xd+side_dif]);
    xticks(xuv);
    xticklabels(cellstr(num2str((2:7)')));
    set(gca,'XTickLabel',cellstr(num2str((2:7)')),'fontsize',char_size)
    lgd = legend('Location','SouthEast');
    lgd.FontSize = char_size;
    %icons(8).MarkerSize = mrk_size;
    xlabel("Test User IDs", 'FontSize', char_size);
    ylabel('Accuracy', 'FontSize', char_size);
    if put_title
        title({['HospiSign ' set_ident_str ' Dataset'];'Handcrafted Feature Fusion';'User-Independent Experiments';'<(hg-hog)-(sn-surface normal)-(sk-skeleton)>_{(pca dimension)}'});
    end
    acc_mat = table2array(T(:,3:end));
    if ischar(ylim_op) && strcmpi(ylim_op,'auto')
        ylimmin = max(0,min(acc_mat(:))-25);
        ylimmax = min(100,max(acc_mat(:))+5);
    elseif isnumeric(ylim_op) && length(ylim_op)==2
        ylimmin = max([0 ylim_op(:)]);
        ylimmax = min([100 ylim_op(:)]);
    elseif isnumeric(ylim_op) && length(ylim_op)==1
        ylimmin = min(max(0,min(acc_mat(:))-25),ylim_op);
        ylimmax = max(min(100,max(acc_mat(:))+5),ylim_op);
    end
    ylim([ylimmin ylimmax])
    ytickvals = 10*(floor(ylimmin/10)):10:10*floor(ylimmax/10);
    yticks(ytickvals)
    %set(gca,'YTickLabel',cellstr(num2str((ytickvals)')),'fontsize',char_size);
    set(gca,'linewidth',2);
end

function hv = plot_single_scatter(xp, acc_plot, hv, hv_id, mrk, display_name, mrk_size, colored)
    if colored
        if hv(hv_id)
            hv(hv_id) = false;
            scatter(xp,acc_plot,mrk_size,mrk,'filled','DisplayName', display_name);
        else
            scatter(xp,acc_plot,mrk_size,mrk,'filled','HandleVisibility','off');
        end
    else
        mrk_size=mrk_size*2;
        if hv(hv_id)
            hv(hv_id) = false;
            scatter(xp,acc_plot,mrk_size,mrk,'LineWidth',3,'DisplayName', display_name);
        else
            scatter(xp,acc_plot,mrk_size,mrk,'LineWidth',3,'HandleVisibility','off');
        end        
    end
end

function [xt, ha_str] = get_horizontal_allignment(c,mm_vec,xuv,hor_dist)
    if (c==mm_vec(1))
        xt = xuv(c-2)+1.3*hor_dist;
        ha_str = 'center';
    elseif (c==mm_vec(end))
        xt = xuv(c-2)-hor_dist;
        ha_str = 'center';
    else
        xt = xuv(c-2);
        ha_str = 'center';
    end
end

function si = get_scatter_ident(colored, i)
    if i==1%hog-red
        if colored
            si = 'pr';
        else
            si = '+k';
        end
    end
    if i==2
        if colored
            si = 'sg';
        else
            si = '.k';
        end
    end
    if i==3
        if colored
            si = '^b';
        else
            si = 'xk';
        end
    end
end

function fold = generate_fold_name(nos)
    compName = getComputerName;
    switch compName
        case  'doga-MSISSD'
            base_DataFolder = '/mnt/USB_HDD_1TB/';
        case  'doga-msi-ubu'
            base_DataFolder = '/home/doga/DataFolder/';
        otherwise
            exit(-78)
    end
    fold_base = strrep([base_DataFolder filesep 'neuralNetHandImages_nosXX_rs224'],'XX',num2str(nos));
    fold = [fold_base filesep 'imgs'];
end

function set_ident_str = get_ident_str(nos)
    switch nos
        case  11
            set_ident_str = 'Development';
        case  41
            set_ident_str = 'Expanded';
    end  
end