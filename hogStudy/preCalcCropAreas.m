function [imCntSizPerScaleVec, imCropVec] = preCalcCropAreas(imSizeCur, scales2Use, strideFixed)

    scaleCnt = length(scales2Use);

    row_y_len = imSizeCur(1);
    col_x_len = imSizeCur(2);
    
    imCntSizPerScaleVec = zeros(scaleCnt,3);%scaleID, scaleSize_rowY, scaleSize_colX
    imCropVec = zeros(1,4);%rowY_beg, rowY_end, colX_beg, colX_end,
    curRowStart = 1;
    for i=1:scaleCnt
        imSizeCur_scale = floor(imSizeCur*scales2Use(i));
        row_y_cur = imSizeCur_scale(1);
        col_x_cur = imSizeCur_scale(2);
        
        %<col_x_cur> will move with <strideFixed> steps on <col_x_len>
        row_y_begs = 1:strideFixed:(row_y_len-row_y_cur+1);%row_y_begs - beginnings
        col_x_begs = 1:strideFixed:(col_x_len-col_x_cur+1);%col_x_begs - beginnings
        
        row_y_ends = row_y_begs + row_y_cur -1;
        col_x_ends = col_x_begs + col_x_cur -1;
        
        row_rep_cnt = size(row_y_ends,2);
        col_rep_cnt = size(col_x_ends,2);
        
        
        [mX,mY] = meshgrid(1:col_rep_cnt,1:row_rep_cnt);
        row_y_begs = row_y_begs(mY);
        col_x_begs = col_x_begs(mX);
        row_y_ends = row_y_ends(mY);
        col_x_ends = col_x_ends(mX);
        
        imCntSizPerScaleVec(i,1) = col_rep_cnt*row_rep_cnt;
        imCntSizPerScaleVec(i,2:3) = [row_y_cur col_x_cur];
        imCropVec(curRowStart:curRowStart+imCntSizPerScaleVec(i,1)-1,:) = [row_y_begs(:)  row_y_ends(:) col_x_begs(:) col_x_ends(:)];
        curRowStart = find(sum(imCropVec,2)>0,1,'last') +1;
    end
end