function [labelsOut, mapMat] = reArrangeLabels(labelsIn, maxLabelIn, newLabels)
    originalLabels = 0:maxLabelIn;
    if isempty(find(newLabels==0, 1))
        newLabels = [0 newLabels];
    end
    mapMat = [originalLabels;newLabels];
    labelsOut = reSetLabels(labelsIn, mapMat(1,:), mapMat(2,:));
end

