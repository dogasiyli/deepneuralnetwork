function [cell_cnt_per_user] = read_csv_file_xv(csv_file_name)
    %cell_cnt_per_user = {'fold_khs','khs_name','u2','u3','u4','u5','u6','u7'};
    %cell_cnt_per_user = csvread(csv_file_name, 2, 0);
    fileID = fopen(csv_file_name);
    C = textscan(fileID,'%s %s %s %s %s %s %s %s','Delimiter',',');
    fclose(fileID);
    cell_cnt_per_user = reshape(cat(1,C{:}),size(C{1},1),size(C,2));
    cell_cnt_per_user(2:end,3:end) = cellfun(@(x) str2double(x), cell_cnt_per_user(2:end,3:end),'UniformOutput', false);
end