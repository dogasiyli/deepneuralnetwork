function [ imOut ] = rearrangeIm( imIn,  lh1_rh2_bh3, newSizeArr)
%rearrangeIm flips the image if right, resizes to newSizeArr
%   there is a dominant hand in learning algorithm
%   we took it as left hand hence we need to fliplr the images when they
%   are extracted from right hand area

%   this function is called from within: 

    if ischar(lh1_rh2_bh3)
        switch lh1_rh2_bh3
            case 'LH'
                lh1_rh2_bh3 = 1;
            case 'RH'
                lh1_rh2_bh3 = 2;
            case 'BH'
                lh1_rh2_bh3 = 3;
        end        
    end
    switch lh1_rh2_bh3
        case 1%left hand is the DOMINANT HAND
            %left hand
        case 2%mirror right hand to the DOMINANT LEFT HAND
            %right hand
            imIn = fliplr(imIn);
        case 3
            %both hands
    end
    imOut = imresize(imIn, newSizeArr);
end

