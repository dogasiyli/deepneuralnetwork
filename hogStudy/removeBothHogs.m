function removeBothHogs(OPS)
    %removeBothHogs(struct('signID',getSignListAll(),'hogSurfBoolVec', [0 1]));
    if ~exist('OPS', 'var') || ~isstruct(OPS)
        OPS = [];
    end
    srcFold             = getStructField(OPS, 'srcFold', getVariableByComputerName('srcFold'));
    userList            = getStructField(OPS, 'userList', 2:7);
    hogSurfBoolVec      = getStructField(OPS, 'hogSurfBoolVec', [0 0]);
    signID              = getStructField(OPS, 'signID', []);

    if ~isempty (signID)
        [surList, listElemntCnt] = createSURList(srcFold, 2, struct('signIDList',signID,'userID',userList,'maxRptCnt',20));
    else
        [surList, listElemntCnt] = fwf_getFirst4Videos;
    end
    difHg = 0;
    difSn = 0;
    for i=1:listElemntCnt
        s = surList(i,1);
        u = surList(i,2);
        r = surList(i,3);
        userFold = [srcFold filesep num2str(s) filesep 'User_' num2str(u) '_' num2str(r)];
        if ~exist(userFold,'dir')
            continue
        end
         
        if hogSurfBoolVec(1)==1
            hog_crop_mat = [userFold filesep  'hog_crop.mat'];
            if ~exist(hog_crop_mat,'file')
                continue
            end
            mbIn = fwf_load_info(hog_crop_mat);
            load(hog_crop_mat,'hogImArr_cell','imcropVec_cell');
            disp('Loaded...');
            numOfFrames=size(hogImArr_cell,1);
            for j=1:numOfFrames
                hogImArr_cell{j,3} = [];
                imcropVec_cell{j,3} = [];
            end
            disp('Updating......');
            save(hog_crop_mat,'hogImArr_cell','imcropVec_cell');
            mbOut = fwf_update_info(hog_crop_mat, mbIn);
            difHg = difHg + mbIn - mbOut;
        end

        if hogSurfBoolVec(2)==1
            surf_crop_mat = [userFold filesep  'surfNorm_crop.mat'];
            if ~exist(surf_crop_mat,'file')
                continue
            end
            mbIn = fwf_load_info(surf_crop_mat);
            load(surf_crop_mat,'surfImArr_cell','imcropVec_cell');
            disp('Loaded...');
            numOfFrames=size(surfImArr_cell,1);
            for j=1:numOfFrames
                surfImArr_cell{j,3} = [];
                imcropVec_cell{j,3} = [];
            end
            disp('Updating......');
            save(surf_crop_mat,'surfImArr_cell','imcropVec_cell');
            mbOut = fwf_update_info(surf_crop_mat, mbIn);
            difSn = difSn + mbIn - mbOut;
        end
        
    end
    disp(['difHog(' num2str(difHg) '), difSn(' num2str(difSn) ')']);
end

function [listToGoTru, listElemntCnt] = fwf_getFirst4Videos()
    listToGoTru = [   ...
                           7 2 1;   7 3 1;   7 4 1;   7 5 1;...
                          13 2 1;  13 3 1;  13 4 1;  13 5 1;...
                          49 2 1;  49 3 1;  49 4 1;  49 5 1;...
                          74 2 4;  74 3 1;  74 4 4;  74 5 4;...
                          85 2 1;  85 3 1;  85 4 1;  85 5 1;...
                          86 2 1;  86 3 1;  86 4 1;  86 5 1;...
                          87 2 2;  87 3 1;  87 4 1;  87 5 1;...
                          88 2 1;  88 3 1;  88 4 1;  88 5 1;...
                          89 2 1;  89 3 1;  89 4 1;  89 5 1;...
                          96 2 1;  96 3 1;  96 4 1;  96 5 2;...
                         112 2 2; 112 3 1; 112 4 1; 112 5 1;...
                         125 2 1; 125 3 1; 125 4 1; 125 5 1;...
                         221 2 1; 221 3 1; 221 4 1; 221 5 1;...
                         222 2 1; 222 3 1; 222 4 1; 222 5 2;...
                         247 2 2; 247 3 1; 247 4 1; 247 5 1;...
                         269 2 1; 269 3 1; 269 4 1; 269 5 1;...
                         273 2 1; 273 3 1; 273 4 1; 273 5 1;...
                         277 2 2; 277 3 2; 277 4 1; 277 5 1;...
                         278 2 1; 278 3 1; 278 4 1; 278 5 1;...
                         290 2 1; 290 3 1; 290 4 1; 290 5 2;...
                         296 2 1; 296 3 2; 296 4 1; 296 5 1;...
                         310 2 1; 310 3 1; 310 4 1; 310 5 1;...
                         335 2 2; 335 3 1; 335 4 1; 335 5 1;...
                         339 2 1; 339 3 1; 339 4 1; 339 5 2;...
                         396 2 1; 396 3 1; 396 4 1; 396 5 1;...
                         424 2 2; 424 3 1; 424 4 1; 424 5 4;...
                         428 2 1; 428 3 1; 428 4 1; 428 5 1;...
                         450 2 1; 450 3 1; 450 4 1; 450 5 1;...
                         521 2 2; 521 3 1; 521 4 1; 521 5 1;...
                         533 2 1; 533 3 1; 533 4 1; 533 5 1;...
                         535 2 2; 535 3 2; 535 4 1; 535 5 4;...
                         536 2 2; 536 3 3; 536 4 1; 536 5 1;...
                         537 2 1; 537 3 1; 537 4 1; 537 5 1;...
                         583 2 1; 583 3 1; 583 4 1; 583 5 2;...
                   ];
     listElemntCnt = size(listToGoTru,1);
end

function mbIn = fwf_load_info(fileName) %surf_crop_mat
    s = dir(fileName);
    mbIn = s.bytes/(1024*1024);
    disp(['Loading -- ' fileName ', ' num2str(mbIn) 'Mb']);
end

function mbOut = fwf_update_info(fileName, mbIn) %surf_crop_mat
    s = dir(fileName);
    mbOut = s.bytes/(1024*1024);
    disp(['Updated ' fileName '- mbIn(' num2str(mbIn) ') --> mbOut(' num2str(mbOut) ') --> mbSaved(' num2str(mbIn-mbOut) ')']);
end
