function acc_best = run_xv_fold_exp(fold, data_dentifier, pca_count, test_user, k_fold_cnt, rand_seed, OPS)
    if ~exist('OPS', 'var') || ~isstruct(OPS)
        OPS = [];
    end
    reDrawFig = getStructField(OPS, 'reDrawFig', true);
    enforce_recreate = getStructField(OPS, 'enforce_recreate', false);
    figID = getStructField(OPS, 'figID', 1);
    
    if  k_fold_cnt==5 && rand_seed<0
        add_str = 'xv = seperate validation users';
        fnstr = 'su';
    else
        add_str = ['xv-' num2str(k_fold_cnt) ' fold, rand_seed(' num2str(rand_seed) ')'];
        fnstr = ['rs' num2str(rand_seed)];
    end
    savefigfold = strrep(fold, [filesep 'imgs'], [filesep 'elm_results' filesep 'figs']);
    if ~exist(savefigfold,'dir')
        mkdir(savefigfold);
    end
    savefigname = ['ft' data_dentifier '_pca' num2str(pca_count) '_tu' num2str(test_user) '_' fnstr '.png'];
    savefigname = [savefigfold filesep savefigname];
    savematsfold = strrep(fold, [filesep 'imgs'], [filesep 'elm_results' filesep 'mats']);
    if ~exist(savematsfold,'dir')
        mkdir(savematsfold);
    end
    savematname = ['ft' data_dentifier '_pca' num2str(pca_count) '_tu' num2str(test_user) '_' fnstr '.mat'];
    savematname = [savematsfold filesep savematname];
    
    run_learn = true;
    if (exist(savematname,'file') && exist(savefigname,'file') && ~enforce_recreate)
        load(savematname, 'acc_va', 'acc_te');
        mean_va = mean(acc_va);
        mean_te = mean(acc_te);
        [~,idx] = max(mean_va);
        acc_best = mean_te(idx);
        run_learn = false;
        if ~reDrawFig
            disp(['skipping - ft' data_dentifier '_pca' num2str(pca_count) '_tu' num2str(test_user) '_' fnstr]);
            return
        else
            disp(['only redraw - ft' data_dentifier '_pca' num2str(pca_count) '_tu' num2str(test_user) '_' fnstr]);
            load(savematname,'acc_tr', 'acc_va', 'acc_te', 'conf_tr', 'conf_va', 'conf_te');
        end
    end

    hid_size =2048*(1:7);%[32 64 128 256 512 1024 2048 4096 8012];%
    hid_size_cnt = length(hid_size);
    if run_learn
        [feats, label_vec, label_names, ~ ] = load_xv_fold_data(fold, data_dentifier, pca_count );
        rand_xv_cell = create_k_fold_xv(fold, k_fold_cnt, rand_seed);
        %test_user; 
        %cv_id;
        acc_tr = zeros(k_fold_cnt, hid_size_cnt);
        acc_va = zeros(k_fold_cnt, hid_size_cnt);
        acc_te = zeros(k_fold_cnt, hid_size_cnt);
        conf_tr = cell(k_fold_cnt, hid_size_cnt);
        conf_va = cell(k_fold_cnt, hid_size_cnt);
        conf_te = cell(k_fold_cnt, hid_size_cnt);
        displayOpts = setDisplayOpts('none');
        dispstat('','init');
        for xv_id=1:k_fold_cnt
            search_col_str = strrep(cellstr(num2str(cell2mat(rand_xv_cell(:,1:2)))),' ','');
            row_id = find(cell2mat(cellfun(@(x) strcmpi(x,[num2str(test_user,'%d') num2str(xv_id,'%d')]),search_col_str, 'UniformOutput', false)));
            tr_ids = rand_xv_cell{row_id, 3};
            va_ids = rand_xv_cell{row_id, 4};
            te_ids = rand_xv_cell{row_id, 5};
            X_tr = feats(tr_ids,:); la_tr = label_vec(tr_ids);   
            [X_tr, la_tr] = rem_nan(X_tr, la_tr, ['tr - ft' data_dentifier '_pca' num2str(pca_count) '_tu' num2str(test_user) '_xv' num2str(xv_id,'%d')]);
            X_va = feats(va_ids,:); la_va = label_vec(va_ids);
            [X_va, la_va] = rem_nan(X_va, la_va, ['va - ft' data_dentifier '_pca' num2str(pca_count) '_tu' num2str(test_user) '_xv' num2str(xv_id,'%d')]);
            X_te = feats(te_ids,:); la_te = label_vec(te_ids);
            [X_te, la_te] = rem_nan(X_te, la_te, ['te - ft' data_dentifier '_pca' num2str(pca_count) '_tu' num2str(test_user) '_xv' num2str(xv_id,'%d')]);
            for h = 1:hid_size_cnt
                h_cur = hid_size(h);
                dispstat(['running for test_user(' num2str(test_user) '), xv(' num2str(xv_id) '/' num2str(k_fold_cnt) '), h(' num2str(h_cur) ')' newline],'init');
                tic;                
                [acc_tr(xv_id,h), pred_tr, W_randoms_with_bias, Welm, ~, conf_tr{xv_id,h}, ~] = ELM_training(X_tr', la_tr, 'sizeHid', h_cur, 'displayOpts',displayOpts);
                [~, acc_va(xv_id,h), conf_va{xv_id,h}] = analyzeResults(X_va', la_va, W_randoms_with_bias, Welm, false);
                [~, acc_te(xv_id,h), conf_te{xv_id,h}] = analyzeResults(X_te', la_te, W_randoms_with_bias, Welm, false);
                accDisp = [acc_tr(xv_id,h) acc_va(xv_id,h) acc_te(xv_id,h)];
                te = toc;
                dispstat(['running for test_user(' num2str(test_user) '), xv(' num2str(xv_id) '/' num2str(k_fold_cnt) '), h(' num2str(h_cur) ')' mat2str(accDisp,4) ' - in ' num2str(te,'%4.2f') ' sec' newline],'keepthis');
            end
        end
    end
    h = figure(figID);clf;hold on;
    for xv_id=1:k_fold_cnt
        if xv_id==1
            scatter(1:hid_size_cnt,acc_tr(xv_id,:),50,'dk','filled','DisplayName','train-xv');
            scatter(1:hid_size_cnt,acc_va(xv_id,:),50,'dg','filled','DisplayName','valid-xv');
            scatter(1:hid_size_cnt,acc_te(xv_id,:),50,'db','filled','DisplayName','test-xv');
        else
            scatter(1:hid_size_cnt,acc_tr(xv_id,:),50,'dk','filled','HandleVisibility','off');
            scatter(1:hid_size_cnt,acc_va(xv_id,:),50,'dg','filled','HandleVisibility','off');
            scatter(1:hid_size_cnt,acc_te(xv_id,:),50,'db','filled','HandleVisibility','off');
        end
    end
    mean_tr = mean(acc_tr);
    mean_va = mean(acc_va);
    mean_te = mean(acc_te);
    plot(1:hid_size_cnt,mean_tr,'k','LineWidth',2,'DisplayName','train-mean');
    plot(1:hid_size_cnt,mean_va,'g','LineWidth',2,'DisplayName','valid-mean');
    plot(1:hid_size_cnt,mean_te,'b','LineWidth',2,'DisplayName','test-mean');
    
    [max_va,idx] = max(mean_va);
    h_best = hid_size(idx);
    max_te = mean_te(idx);
    
    scatter(idx,max_va,150,'g','filled','HandleVisibility','off');
    scatter(idx,max_te,150,'b','filled','HandleVisibility','off');
    
    txt_inc = 1;
    if max_te>max_va
        vaAllignment = 'right';
        vaTxtSize = 14;
        vrarrow = ' \rightarrow';
        vlarrow = '';
        vapos = max_va-txt_inc;
    else
        vaAllignment = 'left';
        vaTxtSize = 18;
        vrarrow = '';
        vlarrow = '\leftarrow ';
        vapos = max_va+txt_inc;
    end
    if max_va>max_te
        teAllignment = 'right';
        teTxtSize = 14;
        trarrow = ' \rightarrow';
        tlarrow = '';
        tepos = max_te-txt_inc;
    else
        teAllignment = 'left';
        teTxtSize = 18;
        trarrow = '';
        tlarrow = '\leftarrow ';
        tepos = max_te+txt_inc;
    end
    if idx==length(hid_size)
        vaAllignment = 'right';
        vrarrow = ' \rightarrow';
        vlarrow = '';
        teAllignment = 'right';
        trarrow = ' \rightarrow';
        tlarrow = '';
    elseif idx==1
        vaAllignment = 'left';
        vrarrow = '';
        vlarrow = '\leftarrow ';
        teAllignment = 'left';
        trarrow = '';
        tlarrow = '\leftarrow ';
    end
    
    if idx>=length(hid_size)/2
        hsAllignment = 'right';
        hrarrow = ' \rightarrow';
        hlarrow = '';
    else
        hsAllignment = 'left';
        hrarrow = '';
        hlarrow = '\leftarrow ';
    end
    
    
    text(idx,vapos,[vlarrow ' valid_{max}(%' num2str(max_va,'%4.2f') ')' vrarrow],'Color','green','FontSize',vaTxtSize,'HorizontalAlignment',vaAllignment);
    text(idx,tepos,[tlarrow 'test_{selected}(%' num2str(max_te,'%4.2f') ')' trarrow],'Color','blue','FontSize',teTxtSize,'HorizontalAlignment',teAllignment);
    
    hid_size_plot_y = max(max([acc_va(:)' acc_te(:)'])*(3/4),40);
    hid_size_plot_min = min([acc_va(:)' acc_te(:)']);
    plot([idx idx],[hid_size_plot_min max(max_te,max_va)],'m');
    text(idx,hid_size_plot_y,[hlarrow 'hid-size_{selected}(%' num2str(h_best) ')' hrarrow],'Color','m','FontSize',12,'HorizontalAlignment',hsAllignment);
    
    ylim([20, 100]);
    xlim([0.8 hid_size_cnt+0.2]);   
    
    titlestr = {['feats(' data_dentifier '), pca_{count}(' num2str(pca_count) '), test_{user}(' num2str(test_user) ')'];add_str};
    title(titlestr);
    xticklabels(cellstr(num2str(hid_size'))');
    xlabel('hidden layer node count');
    ylabel('accuracy');
    legend({'train-xv','valid-xv','test-xv','train-mean','valid-mean','test-mean'},'Location','northwest')
        
    saveas(h, savefigname);
    if run_learn
        save(savematname, 'acc_tr', 'acc_va', 'acc_te', 'conf_tr', 'conf_va', 'conf_te');  
    end
    
    acc_best = max_te;
end

function [X_tr, la_tr] = rem_nan(X_tr, la_tr, block_ident_str)
    valid_rows = isnan(X_tr(:,1));
    if sum(valid_rows)>0
        disp([num2str(sum(valid_rows)) ' rows removed from ' block_ident_str])
        X_tr = X_tr(~valid_rows,:); la_tr = la_tr(~valid_rows); 
    end
end
