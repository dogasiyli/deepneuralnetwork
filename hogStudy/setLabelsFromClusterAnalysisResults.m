function [acceptedBlocksAll, newLabels, distMat, clusterLabelsCell, handsStruct] = setLabelsFromClusterAnalysisResults(srcFold, surSlctListStruct, distMatInfoStruct, clusteringParams, optParamStruct)
    %srcFold = '/media/dg/SSD_Data';
    %labellingStrategy='fromSkeletonStableAll';%'fromAssignedLabels';%'assignedLabelsRemove1';%'assignedLabelsExcept1'%;'fromSkeletonOnly';%
    %surSlctListStruct = struct('labelingStrategyStr',labellingStrategy,'single0_double1', 0, 'signIDList',521,'userList',2:5,'maxRptID',1);
    %distMatInfoStruct = struct('featureName','hf','cropMethod','mc','distanceMethod','e3');
    %clusteringParams = struct('tryMax',2000,'kMax',10,'gtLabelsGrouping',[],'modeStr','kMedoids','groupClusterLabels',false);
    
    if ~exist('optParamStruct','var')
        optParamStruct = [];
    end
    destFolder = getStructField(optParamStruct, 'destFolder', srcFold);
    hogVersion = getStructField(optParamStruct, 'hogVersion', 2);
    k_selected = getStructField(optParamStruct, 'k_selected', []);

    %display the known information about selections, distances and
    %clustering parameters
    displayStructureSummary('surSlctListStruct',surSlctListStruct,0);
    displayStructureSummary('distMatInfoStruct',distMatInfoStruct,0);
    displayStructureSummary('clusteringParams',clusteringParams,0);

    %load the distMat, handsStruct and fileSaveName variables
    [distMat, handsStruct, fileSaveName] =  createSpecificDistMat(srcFold, surSlctListStruct, distMatInfoStruct, struct('hogVersion',hogVersion));
    
    handsStructSize = size(handsStruct.dataCells,1);
    distMatSize = size(distMat,1);
    disp([num2str(handsStructSize) ' samples are compared in distMat(' mat2str(size(distMat)) ')']);
    assert(distMatSize==handsStructSize,'these must be equal');
    
    %load the useful clusterResults
    clusterLabelsCell = [];%will be loaded on the next line
    load(fileSaveName.clusterResult,'medoidHist_Best','clusterLabelsCell');
    
    kVec_of_medoidHist_Best = reshape(find(medoidHist_Best(:,1)~=inf),1,[]);
    if (length(clusteringParams.kVec)~=length(kVec_of_medoidHist_Best)) || (sum(abs(clusteringParams.kVec-kVec_of_medoidHist_Best))>0)
        disp(['clusteringParams.kVec passed is ' mat2str(clusteringParams.kVec) ', but kVec_of_medoidHist_Best is ' mat2str(kVec_of_medoidHist_Best)]);
        str = input('would you like to run with new clusterParamskVec(new) or use the previous(old) : ','s');
        if strcmp(str,'new')
            delete(fileSaveName.clusterResult);
            OPS = struct;
            OPS.clusteringParams = clusteringParams;
            OPS.srcFold = srcFold;
            analyzeDistMat(surSlctListStruct, distMatInfoStruct, OPS);
            load(fileSaveName.clusterResult,'medoidHist_Best','clusterLabelsCell');
        else %if strcmp(str,'old')
            disp(['kVec is assigned to ' mat2str(kVec_of_medoidHist_Best)]);
            clusteringParams.kVec = kVec_of_medoidHist_Best;
        end
    end
    
    
    [handsStruct.detailedLabels, signCount, labelsCount] = combineDetailedLabelsPerSign(handsStruct.detailedLabels);
    disp(['There are ' num2str(signCount) ' signs and ' num2str(labelsCount) ' labels in total to be clustered']);
    
    kMax = fwf_getKMaxVec(clusteringParams);
    assert(kMax==size(medoidHist_Best,1),'these must match');
    
    if isempty(k_selected)
        k_selected = 12;
        str = input('Please select k','s');
        try
            k_selected = str2double(str);
        catch err
            disp(err.message);
            disp('k selected as 12 by default');
        end    
    end
    
    [medoidImCell, medoidIDsOdCluster] = fwf_displayMedoidCenterImages(srcFold, handsStruct, medoidHist_Best, k_selected, kMax, 71);
    
    %ask if you need to group labels first..
    newLabels = fwf_getNewLabelsFromUserInput(k_selected);
    
    newUniqLabels = unique(newLabels);
    clusterNameCell = cell(length(newUniqLabels),1);
    for i=1:length(newUniqLabels)
        %how many original labels does it have
        k=newUniqLabels(i);
        assignedOldLabelIDs = find(newLabels==k);
        str = input(['Please write cluster name for k(' num2str(i) ') which matches the found clusters' mat2str(assignedOldLabelIDs) '.'],'s');
        clusterNameCell{k} = [num2str(i) '-' str];
    end    

    %now I want to display sth new :)
    %given the Cluster count and the clusterID
    %I would like to see the video as 10 frames of c_1_8, 2 no labels.. and
    %so on
    curAssignedLabels = clusterLabelsCell{1,k_selected};
    acceptedBlocksAll = [];
    for s = surSlctListStruct.signIDList
        for u = surSlctListStruct.userList
            %what are the unique repitionID for videos here?
            selectedRows_sign = s==handsStruct.detailedLabels(:,1);
            selectedRows_user = u==handsStruct.detailedLabels(:,2);
            selectedRows = find(selectedRows_sign & selectedRows_user);
            selectedLabelsDetailed = handsStruct.detailedLabels(selectedRows,:); %#ok<FNDSB>
            uniqRepIDs = unique(selectedLabelsDetailed(:,3));
            disp(['sign(' num2str(s) '),user(' num2str(u) '),rAll(' mat2str(uniqRepIDs) ')']);
            if isempty(uniqRepIDs)
                continue
            end
            for r=uniqRepIDs(:)
                surVec = [s u r];
                selectedRows_rep = r==handsStruct.detailedLabels(:,3);
                selectedRows_sur = selectedRows_sign & selectedRows_user & selectedRows_rep;
                disp(['sign(' num2str(s) '),user(' num2str(u) '),r(' mat2str(r) ')']);
                switch surSlctListStruct.single0_double1
                    case 0
                        acceptedBlocks_lh = fwf_getAcceptedBlock(srcFold, surVec, 1, 'lh', handsStruct, selectedRows_sur, curAssignedLabels, medoidImCell, newLabels, clusterNameCell);
                        acceptedBlocks_rh = fwf_getAcceptedBlock(srcFold, surVec, 2, 'rh', handsStruct, selectedRows_sur, curAssignedLabels, medoidImCell, newLabels, clusterNameCell);
                        if ~isempty(acceptedBlocks_lh)
                            acceptedBlocks_lh = [repmat([s u r 1],size(acceptedBlocks_lh,1),1) acceptedBlocks_lh(:,[2 3 1])];
                            acceptedBlocksAll = [acceptedBlocksAll; acceptedBlocks_lh]; %#ok<*AGROW>
                        end
                        if ~isempty(acceptedBlocks_rh)
                            acceptedBlocks_rh = [repmat([s u r 2],size(acceptedBlocks_rh,1),1) acceptedBlocks_rh(:,[2 3 1])];
                            acceptedBlocksAll = [acceptedBlocksAll; acceptedBlocks_rh];
                        end       
                    case 1
                        acceptedBlocks_bh = fwf_getAcceptedBlock(srcFold, surVec, 3, 'bh', handsStruct, selectedRows_sur, curAssignedLabels, medoidImCell, newLabels, clusterNameCell);
                        if ~isempty(acceptedBlocks_bh)
                            acceptedBlocks_bh = [repmat([s u r 3],size(acceptedBlocks_bh,1),1) acceptedBlocks_bh(:,[2 3 1])];
                            acceptedBlocksAll = [acceptedBlocksAll; acceptedBlocks_bh];
                        end
                end
            end
        end
        %now for sign[s] - what are the labels like?
        acceptedBlocksAll = sortrows(acceptedBlocksAll, [7 1 2 3 4]);
    end    
    acceptedBlocksToLabels(srcFold, destFolder, acceptedBlocksAll, newLabels, handsStruct, medoidIDsOdCluster);
end

function acceptedBlocks_cht = fwf_getAcceptedBlock(srcFold, surVec, lh1_rh2_bh3, lrh_str, handsStruct, selectedRows_sur, curAssignedLabels, medoidImCell, newLabels, clusterNameCell)
    selectedRows_cht = lh1_rh2_bh3==handsStruct.detailedLabels(:,4); %cht - current hand type            
    vid_cht_selectedRows = find(selectedRows_cht & selectedRows_sur);
    disp([lrh_str 'Cnt(' num2str(length(vid_cht_selectedRows)) ')']);                       
    %which klusters are found in this video?
    %slctOfIDs = find(videoLabels_lh==k);
    videoLabels_cht = curAssignedLabels(vid_cht_selectedRows);
    videoFrameIDs_cht = unique(sort(handsStruct.detailedLabels(vid_cht_selectedRows,5)));
    %I need to ceate a palette for the summary
    videoLabels_cht_all = zeros(1,max(videoFrameIDs_cht));
    videoLabels_cht_all(videoFrameIDs_cht) = videoLabels_cht;
    [cht_clusterCellString, cht_labelCount] = getClusterSummaryAsString(videoLabels_cht_all);
    %for each row, 
    %I would grab every "," delimetered group
    %and then 
    acceptedBlocks_cht = dispAcceptRejectAlgoResults(srcFold, surVec, cht_clusterCellString, cht_labelCount, lrh_str, medoidImCell, newLabels, clusterNameCell);
end

function newLabels = fwf_getNewLabelsFromUserInput(k_selected)
    str = input(['Please write ' num2str(k_selected) ' new labels using comma as delimeter if you want to further group the found clusters:\r\n'],'s');
    errFound = [];
    try
        newLabels = strsplit(str,',');
        newLabels = cell2mat(cellfun(@str2num,newLabels,'un',0));
    catch err
        errFound = err;
        newLabels = 1:k_selected;
    end
    if length(newLabels)==k_selected
        %all elements must be between 1 and K
        problematicLabels = newLabels<1 | newLabels>k_selected;
        if sum(problematicLabels)>0
            %there are some problematic new labels
            disp(['Lables ' mat2str(newLabels(problematicLabels)) ' are problematic ']);
        else
            %everything is fine and we can group the labels
            disptable([1:k_selected;newLabels],[],'oldLabels|newLabels');
        end
    else
        %either user didnt combine anything or there was an err
        if ~isempty(errFound) && isstruct(errFound)
            disp(['eror occured = ' errFound.message]);
        end        
    end
end

function [medoidImCell, medoidIDsOdCluster] = fwf_displayMedoidCenterImages(srcFold, handsStruct, medoidHist_Best, k_selected, kMax, figID)
    %in figure(figID) show me all the cluster centers 
    medoidIDsOdCluster = medoidHist_Best(k_selected, kMax-k_selected+1 : kMax);
    figure(figID);clf;
    rowCnt = floor(sqrt(k_selected));
    colCnt = ceil(k_selected/rowCnt);
    medoidImCell = cell(1,k_selected);
    for k=1:k_selected
        subplot(rowCnt, colCnt,k);
        medoidImCell{1,k} = retrieveImageFromDetailedLabels(srcFold, medoidIDsOdCluster(k), handsStruct.detailedLabels);        
        imshow(medoidImCell{1,k});
        title(['k(' num2str(k) ' of ' num2str(k_selected) ')']);
    end
end

function [kMax, kVec] = fwf_getKMaxVec(clusteringParams)
    if isfield(clusteringParams,'kMax')
        kMax = clusteringParams.kMax;
        kVec = 1:clusteringParams.kMax;
    elseif isfield(clusteringParams,'kVec')
        kVec = clusteringParams.kVec;
        kMax = max(kVec);
    else
        error('wtf');
    end    
end

function acceptedBlocks = dispAcceptRejectAlgoResults(srcFold, surVec, clusterCellString, labelCount, lhrhstr, medoidImCell, newLabels, clusterNameCell)
    switch lhrhstr
        case 'lh'
            lh1_rh2_bh3 = 1;
        case 'rh'
            lh1_rh2_bh3 = 2;
        case 'bh'
            lh1_rh2_bh3 = 3;
        otherwise
            error('it must be either 3 of them');
    end
    acceptedBlocks = zeros(labelCount,3);
    abc = 0;
    for i=1:labelCount
        curBlockDef = clusterCellString{i,2};
        curBlockDef = strsplit(curBlockDef, ',');
        curBlokPartCnt = size(curBlockDef,2);
        clusterID = str2double(clusterCellString{i,1});
        if clusterID==0
            continue;
        end
        for p=1:curBlokPartCnt
            curBlockPartStr = curBlockDef{p};
            curBlockPartStr = strsplit(curBlockPartStr, '-');
            if size(curBlockPartStr,2)==2
                %this is from i to j
                frFr = str2double(curBlockPartStr{1});
                toFr = str2double(curBlockPartStr{2});
                frCnt = toFr-frFr+1;
                mappedCluster = newLabels(clusterID);
                %this part can be made more interactive!!
                
                medoidIm = medoidImCell{1,clusterID};
                imPalette_groupImgs = f(srcFold, surVec, lh1_rh2_bh3, frFr, toFr);
                figure(92);clf;
                subplot(2,1,1);
                imshow(medoidIm);
                try
                  disp(['cluster(' num2str(mappedCluster) '),medoid(' num2str(clusterID) ') - ' clusterNameCell{mappedCluster} ' for ' lhrhstr ', this is a ' num2str(frCnt) ' frame block from ' curBlockPartStr{1} ' to ' curBlockPartStr{2}]);
                  title([clusterNameCell{mappedCluster} ' - clusterMain(' num2str(mappedCluster) ')-medoidID to assign(' num2str(clusterID) ')']);
                catch
                   disp(['cluster(' num2str(mappedCluster) '),medoid(' num2str(clusterID) ') - for ' lhrhstr ', this is a ' num2str(frCnt) ' frame block from ' curBlockPartStr{1} ' to ' curBlockPartStr{2}]);
                   title(['clusterMain(' num2str(mappedCluster) ')-medoidID to assign(' num2str(clusterID) ')']);
                end
                subplot(2,1,2);
                imshow(imPalette_groupImgs);title(['surVec' mat2str(surVec) ',frames(' num2str(frFr) '-' num2str(toFr) ')']);
                
                acc_rej_str = input(['Please accept(a), reject(r) or write some range([1-],[' num2str(frFr) '-],[-' num2str(toFr) '],[1-' num2str(toFr-frFr+1) ']).'],'s');
                if strcmpi(acc_rej_str(1),'a')
                    disp('accepted by user');
                    abc = abc+1;
                    acceptedBlocks(abc,:) = [clusterID frFr toFr];
                elseif strcmpi(acc_rej_str(1),'r')
                    disp('rejected by user');
                else
                    try
                        [fr, to] = getRangeFromStr( acc_rej_str, frFr, toFr );
                        if fr>=frFr && to<=toFr
                            abc = abc+1;
                            acceptedBlocks(abc,:) = [clusterID fr to];     
                            disp(['accepted (' mat2str([fr to]) ') from (' mat2str([frFr toFr]) ')']);
                        else
                            disp(['rejected due to wrong range str(' acc_rej_str '),acquired(' mat2str([fr to]) '),possible(' mat2str([frFr toFr]) ')']);
                        end
                    catch err
                        disp(['rejected due to some err - ' err.message]);
                    end
                end
            elseif size(curBlockPartStr,2)==1
                %this is a single frame or first of consecutive
                %2 frames
                disp(['for ' lhrhstr ', this is a 1 or 2 frame(' curBlockPartStr{1} ') which will be discarded and not added to cluster(' num2str(clusterID) ')']);
            else
                error('what is this?');
            end
        end
        
    end
    acceptedBlocks(abc+1:end,:) = [];
end

function imPalette_groupImgs = f(srcFold, surVec, lh1_rh2_bh3, fromFrame, toFrame)
    imCollage_imSizeMax = [50 50];
    imCnt = toFrame-fromFrame+1;
    surroundWithColorStruct = struct('thicknessParam',0.2,'colorParam',[0 0 0]);
    [imPalette_groupImgs, paletteRCSize_distinctImgs] = imCollageFuncs( 'createPalette', struct('imCnt',imCnt,'channelCnt',3,'imSizeMax',imCollage_imSizeMax));
    for i = 1:imCnt
        %this can be distance info??
        distCur = 0.5;
        surroundWithColorStruct.colorParam = ones(1,3)*distCur;
        dl = [surVec lh1_rh2_bh3 (i+fromFrame-1)];
        imageToAdd = retrieveImageFromDetailedLabels(srcFold, 1, dl);
        if isempty(imageToAdd)
            continue
        end                
        imPalette_groupImgs = imCollageFuncs('insertIntoPalette', struct('imageID',i,'paletteRCSize',paletteRCSize_distinctImgs,'imPalette',imPalette_groupImgs,'imageToAdd',imageToAdd,'boxSize',3*imCollage_imSizeMax, 'surroundWithColorStruct', surroundWithColorStruct));
    end
end