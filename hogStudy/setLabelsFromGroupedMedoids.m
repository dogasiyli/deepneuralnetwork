function [problematicVids, predictedLabels, existingLabels] = setLabelsFromGroupedMedoids(surSlctListStruct, distMatInfoStruct, OPS)
    %surSlctListStruct = struct('labelingStrategyStr','fromSkeletonOnly','single0_double1', 0, 'signIDList',521,'userList',2:7,'maxRptCnt',50);
    %distMatInfoStruct = struct('featureName','hf','cropMethod','mc','distanceMethod','e3');
    %OPS = struct('srcFold',srcFold,'overwriteLabelFiles',overwriteLabelFiles);
%% 0 . Load
    signID = surSlctListStruct.signIDList;
    if (length(signID)>1)
        error('too many signIDs here')
    end
   
    if ~exist('OPS', 'var') || ~isstruct(OPS)
        OPS = [];
    end
    srcFold                     = getStructField(OPS, 'srcFold', getVariableByComputerName('srcFold'));
    overwriteLabelFiles         = getStructField(OPS, 'overwriteLabelFiles', false);
    returnExistingAndPredicted  = getStructField(OPS, 'returnExistingAndPredicted', false);
    saveAtEveryNewCnt           = getStructField(OPS, 'saveAtEveryNewCnt', 5000);
    skipIfKnown                 = getStructField(OPS, 'skipIfKnown', true);
    reExtractDistMat            = getStructField(OPS, 'reExtractDistMat', false);
    frameLabelingStrategy       = getStructField(OPS, 'frameLabelingStrategy', 'singleFrame_1nn');%'labelClusterGroups', 'singleFrame_1nn'
    %possibleDTWFlows           = getStructField(optParamStruct, 'possibleDTWFlows', summarizeLabels(signID, struct('userList', 2:5, 'maxRptCnt', 1)));
    fileName_handsStructMedoids = getStructField(OPS, 'fileName_handsStructMedoids', '');
    
    onlyCalcDistIfSameHandShape = getStructField(OPS, 'onlyCalcDistIfSameHandShape', false);
    skipCalcDistIfUnlabelled    = getStructField(OPS, 'skipCalcDistIfUnlabelled', false);
    calcIfStruct = struct('unlabelled',skipCalcDistIfUnlabelled,'sameHandShapeOnly',onlyCalcDistIfSameHandShape);
    
    maxRptCnt = getStructField(OPS, 'maxRptCnt', 50);
    maxRptCnt = getStructField(OPS, 'maxRptID', maxRptCnt);
    minRepCnt = getStructField(OPS, 'minRepCnt', 0);
    testMode  = getStructField(OPS, 'testMode', 1);
    
    featName    = distMatInfoStruct.featureName;
    cropMethod  = distMatInfoStruct.cropMethod;
    distMethod  = distMatInfoStruct.distanceMethod;%{'c3','c2','e3','e2'}

    userList = surSlctListStruct.userList;
    
    if isempty(fileName_handsStructMedoids)
        fileName_handsStructMedoids = [srcFold filesep num2str(signID) filesep 'handsStructMedoids_' featName  '_' cropMethod '_' num2str(surSlctListStruct.single0_double1) '_' surSlctListStruct.labelingStrategyStr '.mat'];
    end

    predictedLabels = [];
    existingLabels = [];
    problematicVids = [];
    if ~exist(fileName_handsStructMedoids,'file')
        disp(['You need to run (createHandsStructMedoidsForASign) function for signID(' num2str(signID) '), sd(' num2str(surSlctListStruct.single0_double1) '), and labelingStrategyStr(' surSlctListStruct.labelingStrategyStr ') first']);
        return
    end

    handsStructMedoids = [];
    load(fileName_handsStructMedoids,'handsStructMedoids');
    N_clusters = size(handsStructMedoids.detailedLabels,1);
    
%% 1.create the list to go trough

    fileSaveName = createFileSaveName_DistMat(srcFold, surSlctListStruct, distMatInfoStruct, 1);
    knownDistancesFileName = fileSaveName.knownDistancesFileName;
    knownDistances = loadKnownDistancesFromFile(knownDistancesFileName);
    %the next one should be different
    distMatsFoldeNameFull = [srcFold filesep 'distMats'];
    distMatFoldFileInit = [distMatsFoldeNameFull filesep distMatInfoStruct.featureName '_' cropMethod '_' num2str(surSlctListStruct.single0_double1) '_' num2str(signID) '_'];%'sn_mc_0_521_';
    
    [surList, listElemntCnt] = createSURList(srcFold, 2, struct('signIDList',signID,'userID',userList,'maxRptCnt',maxRptCnt,'minRepCnt',minRepCnt));
            
    predictedLabels = cell(listElemntCnt,3);
    existingLabels  = cell(listElemntCnt,3);
    for l = 1:listElemntCnt
        s = surList(l,1);
        u = surList(l,2);
        r = surList(l,3);
        theFoldName = [srcFold filesep num2str(s) filesep 'User_' num2str(u) '_' num2str(r)];
        disp(['will predict (' mat2str([s u r]) ') labels'])
        
        
        %check if labels folder is there and 
        [someMissing, labelFilesExist, existingLabels(l,:)] = checkLabelFiles(theFoldName, surSlctListStruct.single0_double1);
        if ~someMissing && ~returnExistingAndPredicted
            disp(['skipping vid(' mat2str([s u r]) ') because all are labelled'])
            continue
        end
        
        knownDistances_append = [];
        labelsNew = struct('LH',[],'RH',[],'BH',[]); %#ok<NASGU>
        try
            [handsToLabelStruct, selectedImCntMat]= getClusterableFrameIDs(srcFold, s, u, r, 'assignedLabelsExcept1');%'fromSkeletonStableAll'
            curVideoHandStruct = createDataSetFromFolder(featName, srcFold, s, u, r, surSlctListStruct.single0_double1, handsToLabelStruct);
            if strcmpi(cropMethod,'sc')
                N = size(curVideoHandStruct.dataCells,1);
                for i=1:N
                    curVideoHandStruct.dataCells{i} = {curVideoHandStruct.dataCells{i}(1,:)};
                    curVideoHandStruct.imCropCells{i} = {curVideoHandStruct.imCropCells{i}(1,:)};
                end
            end
            
            curClusterNames = getClusterNamesAssignedFromTxt(srcFold, surSlctListStruct);
            
            labelsNew = fwf_initialize_labelsNew(handsToLabelStruct, surSlctListStruct.single0_double1);
            [N_newVid, distMat, cropIDs, detailedLabels_RowFrames, distMatFileName] = fwf_initializeDistMat(u, r, surSlctListStruct, N_clusters, curVideoHandStruct, distMethod, distMatFoldFileInit);
            if sum(isnan(distMat(:)))>0 || strcmpi(frameLabelingStrategy,'singleFrame_1nn')
                 [distMat, cropIDs, labelsNew] = fwf_labelBy_SingleFrame_1nn(srcFold, ...
                                                                 distMat, cropIDs, distMatFileName, ...
                                                                 curVideoHandStruct, curClusterNames,...
                                                                 distMatInfoStruct, handsStructMedoids, ...
                                                                 fileSaveName, skipIfKnown, saveAtEveryNewCnt, knownDistances,...
                                                                 testMode, calcIfStruct, labelsNew);
            end
            switch frameLabelingStrategy
                case 'labelClusterGroups'
                    switch featName
                        case 'sn'
                            useMethodID = 1;
                        otherwise%hf
                            useMethodID = 0;
                    end
                    labelsNew = fwf_labelBy_Clustering(srcFold, theFoldName, fileSaveName, distMatFileName, distMatInfoStruct, surSlctListStruct, u, r, handsStructMedoids, curVideoHandStruct, reExtractDistMat, useMethodID);
                otherwise %case 'singleFrame_1nn'
            end

            labelledCount_LH = sum(labelsNew.LH~=0);
            labelledCount_RH = sum(labelsNew.RH~=0);
            labelledCount_BH = sum(labelsNew.BH~=0);
            disp(['labelledHandCnt = LH(' num2str(labelledCount_LH) '),RH(' num2str(labelledCount_RH) '),BH(' num2str(labelledCount_BH) ')'])
            
            labelsNew = fwf_checkLabelCounts(existingLabels(l,:), labelsNew, surSlctListStruct.single0_double1);
            
            if returnExistingAndPredicted               
                predictedLabels{l,1} = labelsNew.LH;
                predictedLabels{l,2} = labelsNew.RH;
                predictedLabels{l,3} = labelsNew.BH;
            end

            %fwf_labelByClustering(srcFold, fileSaveName, dMIS, surSlctListStruct, u, r, distMat, detailedLabels_ColCentroids, curVideoHandStruct);
            %fwf_clusterGroupedLabelsOfResults
            %fwf_writeLabelsToFile   
        catch err
            disp(['err(' err.message ') in s(' num2str(s) '),u(' num2str(u) '),r(' num2str(r) ')'])
            displayErrorMessage(err);
            problematicVids = [problematicVids;s u r];
        end
    end    
end

function labelsNew = fwf_checkLabelCounts(existingLabels, labelsNew, single0_double1)
    if single0_double1==0
        le = length(existingLabels{1});
        ln = length(labelsNew.LH);
        if le>ln
            labelsNew.LH(ln:le) = labelsNew.LH(ln);
        elseif le<ln
            labelsNew.LH = labelsNew.LH(1:le);
        end

        le = length(existingLabels{2});
        ln = length(labelsNew.RH);
        if le>ln
            labelsNew.RH(ln:le) = labelsNew.RH(ln);
        elseif le<ln
            labelsNew.RH = labelsNew.RH(1:le);
        end
    else
        le = length(existingLabels{3});
        ln = length(labelsNew.BH);
        if le>ln
            labelsNew.BH(ln:le) = labelsNew.BH(ln);
        elseif le<ln
            labelsNew.BH = labelsNew.BH(1:le);
        end
    end
end

function fwf_writeLabelsToFile %#ok<DEFNU>
%             if labelledCount_LH~=0
% %                 if ~isempty(possibleDTWFlows.LH)
% %                     labelsNew.LH = getBestDTWFlowResult(distMat(:,curVideoHandStruct.detailedLabels(:,4)==1), detailedLabels_ColCentroids, possibleDTWFlows.LH);
% %                 else
% %                     [ labelsNew.LH, theDistances, removedLabelCounts] = getLabelsFromDistMatByDTW( distMat(:,curVideoHandStruct.detailedLabels(:,4)==1), detailedLabels_ColCentroids);
% %                 end
% %                 theLabels = labelsNew.LH;
%                 writeLabelVecToFile(theFoldName, labelsNew.LH, 1, overwriteLabelFiles);
%             end
%             if labelledCount_RH~=0
% %                 if ~isempty(possibleDTWFlows.RH)
% %                     labelsNew.RH = getBestDTWFlowResult(distMat(:,curVideoHandStruct.detailedLabels(:,4)==2), detailedLabels_ColCentroids, possibleDTWFlows.RH);
% %                 else
% %                     [ labelsNew.RH, theDistances, removedLabelCounts] = getLabelsFromDistMatByDTW( distMat(:,curVideoHandStruct.detailedLabels(:,4)==2), detailedLabels_ColCentroids, dtwFlow);
% %                 end
% %                 theLabels = labelsNew.RH;
%                 writeLabelVecToFile(theFoldName, labelsNew.RH, 2, overwriteLabelFiles);
%             end
%             if labelledCount_BH~=0
% %                 if ~isempty(possibleDTWFlows.BH)
% %                     labelsNew.BH = getBestDTWFlowResult(distMat, detailedLabels_ColCentroids, possibleDTWFlows.BH);
% %                 else
% %                     [labelsNew.BH, theDistances, removedLabelCounts] = getLabelsFromDistMatByDTW(distMat, detailedLabels_ColCentroids);
% %                 end
%                 writeLabelVecToFile(theFoldName, labelsNew.BH', 3, overwriteLabelFiles);
%             end 
end
function fwf_clusterGroupedLabelsOfResults %#ok<DEFNU>
%                 if clusteringParams.groupClusterLabels
%                     [confMatBest, uniqueMappingLabels, confAccBestVec(k), clusterLabelsGroupingVec, newKlusterCell, clusterLabels_best, h1, h2] = groupClusterLabelsIntoGroundTruth(clusterLabelsCell{1,k}, handsStruct.detailedLabels(:,6), gtLabelsGrouping, 1);
%                     kReduced(k) = length(newKlusterCell);
% 
%                     disp(['Best confusion matrix for cluster count(' num2str(k) ') is found with k=' num2str(kReduced(k)) ' and the acc(' num2str(confAccBestVec(k),'%4.2f') ')']);
%                     disptable(confMatBest);
%                     disptable(uniqueMappingLabels, 'originalLabels|userMappedLabels|bestConfusionLabels');
% 
%                     clusterBestConfusionImageFileName = [clusterResultFolder filesep 'clusterResult_k' num2str(k,'%02d') '_conf_best.png'];
%                     saveas(h1, clusterBestConfusionImageFileName);
%                     clusterOriginalConfusionImageFileName = [clusterResultFolder filesep 'clusterResult_k' num2str(k,'%02d') '_conf_orig.png'];
%                     saveas(h2, clusterOriginalConfusionImageFileName);
% 
%                     %I want to have - for each cluster
%                     % 1 : medoidCluster visual
%                     % 2 : 1 sample from labels that are assigned to this cluster - Ground truth label(1-1) is assigned to US-cluster(2)
%                     % 3 : all samples that are assigned into this group
% 
%                     %As a summary of all clusters     
%                     % 1 : the confusion matrix
%                     sampleIndsVec_best = plotDistanceMatrixAsData(distMat, 2, clusterLabels_best, []);
%                     sortedClusterLabels = clusterLabels_best(sampleIndsVec_best);        
%                     for j = 1:length(newKlusterCell) %cols   
%                         [h2, imageCell{j}] = createCollageImForAKluster(k_uniqCount, j, confAccBestVec, srcFold, handsStruct, sortedClusterLabels, sampleIndsVec_best, uniqueMappingLabels, newKlusterCell, medoidIDsOdCluster, randomColors, imCollage_imSizeMax, figID);
%                         %save h2 as clusterResult_k01_02.png
%                         %under
%                         imFullName = [clusterResultFolder filesep 'clusterResult_k' num2str(k,'%02d') '_' num2str(j,'%02d') '.png'];
%                         saveas(h2, imFullName);
%                     end
%                 else
%           
%                 end
%             end    
end

function [distMat, cropIDs, labelsNew] = fwf_labelBy_SingleFrame_1nn(srcFold, distMat, cropIDs, distMatFileName, curVideoHandStruct, curClusterNames, distMatInfoStruct, handsStructMedoids, fileSaveName, skipIfKnown, saveAtEveryNewCnt, knownDistances, testMode, calcIfStruct, labelsNew)    
    %labelsNew = struct('LH',[],'RH',[],'BH',[]);
    [N_clusters, N_newVid] = size(distMat);
    featName    = distMatInfoStruct.featureName;
    cropMethod  = distMatInfoStruct.cropMethod;
    distMethod  = distMatInfoStruct.distanceMethod;%{'c3','c2','e3','e2'}
    knownDistancesFileName = fileSaveName.knownDistancesFileName;
    knownDistances_col11 = getCol11IDByDistMethod(featName, distMethod);
    knownDistances_append = [];
    
    detailedLabels_RowFrames = curVideoHandStruct.detailedLabels;
    detailedLabels_ColCentroids = handsStructMedoids.detailedLabels;
    clusterHandShapeNames = handsStructMedoids.medoidStrings;
    
    
    skipCalcDistIfUnlabelled = calcIfStruct.unlabelled;
    onlyCalcDistIfSameHandShape = calcIfStruct.sameHandShapeOnly;
    
    skippedFrameCount_skipCalcDistIfUnlabelled = 0;
    skippedFrameCount_sameHandShape = 0;
    skippedFrameCount_known = 0;
    disp(['fwf_labelBy_SingleFrame_1nn started for N_newVid(' num2str(N_newVid) '), N_clusters(' num2str(N_clusters) ')'])
    for f = 1:N_newVid %FrameToCluster
        %calculate the distance of a sample to all cluster centers
       imCell_f = curVideoHandStruct.dataCells{f,1};
       if testMode>=2
           im_f = retrieveImageFromDetailedLabels(srcFold, f, detailedLabels_RowFrames);
       end
       curFrameClusterID = detailedLabels_RowFrames(f,end);
       %if 1 it means it is not labelled, else it is labelled sth.
       if skipCalcDistIfUnlabelled && curFrameClusterID==1
           skippedFrameCount_skipCalcDistIfUnlabelled = skippedFrameCount_skipCalcDistIfUnlabelled + 1;
           continue
       end
       for c = 1:N_clusters
            if skipIfKnown && ~isnan(distMat(c,f)) && ~isnan(cropIDs(c,f)) && ~isnan(cropIDs(c+N_clusters,f))
                skippedFrameCount_known = skippedFrameCount_known +1;
                continue
            end
            mnDS_knownFromMat = struct;
            knownDistances_check = [detailedLabels_RowFrames(f,1:5) detailedLabels_ColCentroids(c,1:5) knownDistances_col11 0 0 0];
            [ ~, dMQuant ] = distMatQuantize_hospiSign( knownDistances_check, [] );
            if isempty(dMQuant.frameInfo)
                foundRow = [];
                distMat(c,f) = 0; %distMat(c,f);
                cropIDs(c,f) = 1; %cropIDs(c,f);
                cropIDs(c+N_clusters,f) = 1; %cropIDs(c+N_clusters,f);
            else
                foundRow = find(knownDistances.frameInfo==dMQuant.frameInfo);
            end
            if ~isempty(foundRow)
                foundRow = knownDistances.additionalCols(foundRow,:);
                mnDS_knownFromMat.d = foundRow(2); %distMat(c,f);
                mnDS_knownFromMat.rowID = foundRow(3); %cropIDs(c,f);
                mnDS_knownFromMat.colID = foundRow(4); %cropIDs(c+N_clusters,f);
                mnDS_knownFromMat.cropIDs = [foundRow(3) foundRow(4)];%[cropIDs(c,f) cropIDs(c+N_clusters,f)];
            else
                mnDS_knownFromMat.d = distMat(c,f);
                mnDS_knownFromMat.rowID = cropIDs(c,f);
                mnDS_knownFromMat.colID = cropIDs(c+N_clusters,f);
                mnDS_knownFromMat.cropIDs = [cropIDs(c,f) cropIDs(c+N_clusters,f)];
            end
                      
            if skipIfKnown && ~isnan(mnDS_knownFromMat.d) && mnDS_knownFromMat.rowID~=0 && mnDS_knownFromMat.colID~=0
                distMat(c,f) = mnDS_knownFromMat.d;
                cropIDs(c,f) = mnDS_knownFromMat.rowID;
                cropIDs(c+N_clusters,f) = mnDS_knownFromMat.colID;
                continue
            end
            
            sameHandShape = strcmpi(curClusterNames{curFrameClusterID},clusterHandShapeNames{c});
            if onlyCalcDistIfSameHandShape && ~sameHandShape
                skippedFrameCount_sameHandShape = skippedFrameCount_sameHandShape + 1;
                continue
            end            
            
            imCell_c = handsStructMedoids.dataCells{c,1};
            %detailedLabels_01 = handsStruct.detailedLabels(id_01,:);
            %detailedLabels_02 = handsStruct.detailedLabels(id_02,:);
            %calc maps for these two
            %mnDS_ = calcMinDist_surfNormImageCells(imCell01, imCell02, 3, map02, map01);
            if iscell(imCell_f) && length(imCell_f)==1
                imCell_f = imCell_f{1,1};
            end
            if iscell(imCell_c) && length(imCell_c)==1
                imCell_c = imCell_c{1,1};
            end
            switch featName
                case 'sn'
                    mnDS_ = calcMinDist_surfNormImageCells(imCell_f, imCell_c, 1, [], [], distMethod);
                case 'hf'
                    mnDS_ = calcMinDist_hogFeatCells(imCell_f, imCell_c, 0, distMethod);
            end
            distMat(c,f) = mnDS_.d;
            cropIDs(c,f) = mnDS_.rowID;
            cropIDs(c+N_clusters,f) = mnDS_.colID;
            if testMode>=3
                figure(213);clf;
                im_c = retrieveImageFromDetailedLabels(srcFold, c, detailedLabels_ColCentroids);
                subplot(2,1,1);imshow(im_f);title(['given frame(' mat2str(detailedLabels_RowFrames(f,:)) ')']);
                subplot(2,1,2);imshow(im_c);title(['medoid frame(' mat2str(detailedLabels_ColCentroids(c,:)) ')-d(' num2str(mnDS_.d) ')']);
            end
            knownDistances_row = [detailedLabels_RowFrames(f,1:5) detailedLabels_ColCentroids(c,1:5) knownDistances_col11 mnDS_.d mnDS_.rowID mnDS_.colID];
            if (sum(abs(knownDistances_row(1:5)-knownDistances_row(6:10)))) >0
                knownDistances_append = [knownDistances_append;knownDistances_row]; %#ok<*AGROW>
            end 
            [knownDistances, knownDistances_append] = updateKnownDistancesFile(knownDistances_append, saveAtEveryNewCnt, knownDistances, knownDistancesFileName, distMatFileName, distMat, cropIDs, detailedLabels_RowFrames, detailedLabels_ColCentroids);
       end

       [labelsNew, best_c_d, best_c_idx, allowedDist] = fwf_assignLabelByDistance(labelsNew, distMat(:,f),detailedLabels_RowFrames(f,:),detailedLabels_ColCentroids,handsStructMedoids);

       if testMode>=2
            figure(213);clf;
            im_c = retrieveImageFromDetailedLabels(srcFold, best_c_idx, detailedLabels_ColCentroids);
            subplot(2,2,1);imshow(im_f);title(['given frame(' mat2str(detailedLabels_RowFrames(f,:)) ')']);
            if allowedDist<best_c_d
                subplot(2,2,2);imshow(im_c);title(['removed' '(' mat2str(detailedLabels_ColCentroids(best_c_idx,:)) ')-d(' num2str(best_c_d) ')-allowed(' num2str(allowedDist) ')']);                        
            else
                subplot(2,2,3);imshow(im_c);title(['accepted' '(' mat2str(detailedLabels_ColCentroids(best_c_idx,:)) ')-d(' num2str(best_c_d) ')-allowed(' num2str(allowedDist) ')']);
            end
            drawnow;
       end 
    end
    updateKnownDistancesFile(knownDistances_append, 0, knownDistances, knownDistancesFileName, distMatFileName, distMat, cropIDs, detailedLabels_RowFrames, detailedLabels_ColCentroids);   
    disp(['skippedFrameCount = skipCalcDistIfUnlabelled(' num2str(skippedFrameCount_skipCalcDistIfUnlabelled) '*' num2str(N_clusters) '), sameHandShape(' num2str(skippedFrameCount_sameHandShape) '), known(' num2str(skippedFrameCount_known) ')']);
    disp('fwf_labelBy_SingleFrame_1nn ended..');   
end
%dMIS = distMatInfoStruct
%fwf_labelByClustering(srcFold, fileSaveName, distMatInfoStruct, surSlctListStruct, u, r, distMat, detailedLabels_ColCentroids, curVideoHandStruct)
function labelsNew = fwf_labelBy_Clustering(srcFold, theFoldName, fileSaveName, distMatFileName, dMIS, surSlctListStruct, u, r, handsStructMedoids, curVideoHandStruct, reExtractDistMat, useMethodID)
    %[distMat, cropIDs, mnDS, mxDS] = createDistMat_surfNormCells(dMIS.featureName, curVideoHandStruct, fileSaveName.knownDistancesFileName, distMatFileName, true, useMethodID, dMIS.distanceMethod);
    load(distMatFileName);
    detailedLabels_ColCentroids = handsStructMedoids.detailedLabels;
    switch dMIS.featureName
        case 'sn'
            distAllowedPerMedoid = handsStructMedoids.minDistAllowedPerMedoid;%according to allowed distance
        case 'hf'
            distAllowedPerMedoid = handsStructMedoids.maxDistAllowedPerMedoid;%according to allowed distance
    end    
    distMatUniq = sortRepeatedRowsOfDistMat(distMat, detailedLabels_ColCentroids(:,end));

    %useMethodID=1;
    surSlctListStruct_sub = surSlctListStruct;
    surSlctListStruct_sub.userList = u;
    surSlctListStruct_sub.minRepCnt = r;
    surSlctListStruct_sub.maxRptID = r;
    fileSaveName_sub = createFileSaveName_DistMat(srcFold, surSlctListStruct_sub, dMIS,1);
    OPS_createDistMat_surfNormCells = struct('enforceRecreate',false,'useMethodID',useMethodID,'distMethod',dMIS.distanceMethod);
    distMat_ToCluster = createDistMat_surfNormCells(dMIS.featureName, curVideoHandStruct, fileSaveName.knownDistancesFileName, fileSaveName_sub.distanceMatrixFile, OPS_createDistMat_surfNormCells);

    clusteringParams = struct('tryMax',3000,'kVec',size(distMatUniq,1)*[2 3],'gtLabelsGrouping',[],'modeStr','kMedoids','groupClusterLabels',false);
    if exist(fileSaveName_sub.clusterResult,'file')
        load(fileSaveName_sub.clusterResult,'medoidHist_Best','clusterLabelsCell');
    else
        [medoidHist_Best, clusterLabelsCell] = run_kmedoidsDG_Loop(distMat_ToCluster, clusteringParams);
        save(fileSaveName_sub.clusterResult,'medoidHist_Best','clusterLabelsCell');
    end

    autoClusterFolder = [theFoldName filesep 'autoCluster'];
    if ~exist(autoClusterFolder,'dir')
        mkdir(autoClusterFolder);
    end


    optionalParamsStruct = struct;
    optionalParamsStruct.figID = 3;
    optionalParamsStruct.clusteringParams = clusteringParams;
    optionalParamsStruct.distMatInfoStruct = dMIS;
    optionalParamsStruct.clusterResultFolder = autoClusterFolder;

    displayClusterResults(srcFold, distMat_ToCluster, curVideoHandStruct, medoidHist_Best, clusterLabelsCell, optionalParamsStruct);%[~, confAccBest] = 
    %disptable(confAccBest, 'originalClusterCount|reducedKlusterCount|confusionAccuracy|kSpecificity|combinedPrecision',[],'%4.2f',1); 
    if isfield(clusteringParams,'kMax')
        kMax = clusteringParams.kMax;
        kVec = 1:clusteringParams.kMax;
    elseif isfield(clusteringParams,'kVec')
        kVec = clusteringParams.kVec;
        kMax = max(kVec);
    else
        error('wtf');
    end 
    accuracyCalced = zeros(1,length(kVec));
    labelsNew = cell(1,length(kVec));
    for ki=1:length(kVec) %rows
        k = kVec(ki);
        %clusterLabels_cur have the assigned cluster ID's of each sample
        clusterLabels_cur = clusterLabelsCell{1,k};
        uniqLabelsAssigned = unique(clusterLabels_cur);
        k_uniqCount = length(uniqLabelsAssigned);
        medoidIDsOdCluster = medoidHist_Best(k, kMax-k+1 : kMax);
        if k_uniqCount<k
            disp(['labels are changed from ' mat2str(uniqLabelsAssigned) ' to ' mat2str(1:k_uniqCount)]);
            clusterLabels_cur = reSetLabels(clusterLabels_cur, uniqLabelsAssigned, 1:k_uniqCount);
            medoidIDsOdCluster = medoidIDsOdCluster(uniqLabelsAssigned);
        end
        histOfLabelAssignments = hist(clusterLabels_cur,1:k_uniqCount);
        %each row has the medoid and the closest sample to it        
        medoidIDsOdCluster(medoidIDsOdCluster<0) = [];
        medoidIDsOdCluster(medoidIDsOdCluster==inf) = [];        
        medoidCount = length(medoidIDsOdCluster);

        disp(['cluster summary for k=' num2str(k) ' is :']);
        printClusterSummary(clusterLabels_cur);

        disp(['There are ' num2str(medoidCount) ' medoids found']);

        disp('Part of distance matrix showing cluster medoids distances:');
        disptable(distMat_ToCluster(medoidIDsOdCluster,medoidIDsOdCluster));

        %clusterLabels_med2 = clusterLabels_cur(sampleIndsVec_cur(medoidIDsOdCluster));
        imageCell = cell(1,k_uniqCount);

        %I have k clusters
        %clusterLabels_cur are my cluster labels
        sampleIndsVec = plotDistanceMatrixAsData(distMat_ToCluster, 2, clusterLabels_cur, []);
        sortedClusterLabels = clusterLabels_cur(sampleIndsVec);
        medoidsClusterIDs = clusterLabels_cur(medoidIDsOdCluster);
        assert(length(unique(medoidsClusterIDs))==k,'this must hold');
        newKlusterCell = num2cell(medoidsClusterIDs);
        imCollage_imSizeMax = [50 50];
        [randomColors] = createInitialLabelsCollage(srcFold, curVideoHandStruct, imCollage_imSizeMax, 43);
        
        [clusterToHandShapeMap, labelsNew{ki}, distToKnownHandShape, toCheckMat] = getClusterToHandshapesMap(distMat, detailedLabels_ColCentroids, distAllowedPerMedoid, sampleIndsVec, sortedClusterLabels);
        toCheckMat = [toCheckMat curVideoHandStruct.detailedLabels(:,end)];
        
        labelsPredicted = labelsNew{ki};
        labelsPredicted(labelsPredicted==0) = 1;
        knownLabels = curVideoHandStruct.detailedLabels(:,end)';
        [accuracyCalced(ki), confMatResult{ki}] = calculateConfusion(labelsPredicted, knownLabels);
        
%         for j = 1:k_uniqCount %cols   
%             [h2, imageCell{j}] = createCollageImRandomMatch(j, srcFold, curVideoHandStruct, sortedClusterLabels, sampleIndsVec, newKlusterCell, medoidIDsOdCluster, randomColors, imCollage_imSizeMax, 48);
%             allImIDs_curCluster = sampleIndsVec(ismember(sortedClusterLabels,j));
%             [minDistToClosest, selectedClusterOfAll] = min(distMat(:,allImIDs_curCluster));
%             assignableLabelsOfAll = detailedLabels_ColCentroids(selectedClusterOfAll,end);%per closest center selection of sign
%             assignableLabelsUniq = unique(assignableLabelsOfAll);
%             assX_rem0 = zeros(1,length(allImIDs_curCluster));
%             if length(assignableLabelsUniq)==1
%                 %all of the samples are ready to be assigned to a
%                 %single label
%                 labelToAssign = assignableLabelsUniq;
%                 switch dMIS.featureName
%                     case 'sn'
%                         assignableSampleInds = minDistToClosest<handsStructMedoids.minDistAllowedPerMedoid(selectedClusterOfAll);%according to allowed distance
%                     case 'hf'
%                         assignableSampleInds = minDistToClosest<handsStructMedoids.maxDistAllowedPerMedoid(selectedClusterOfAll);%according to allowed distance
%                 end
%                 for cci = 1:length(allImIDs_curCluster)
%                     idxGeneral = allImIDs_curCluster(cci);
%                     if (assignableSampleInds(cci)==1) 
%                         labelsNew = assignLabel(labelsNew, detailedLabels_RowFrames, idxGeneral, labelToAssign);
%                         assX_rem0(1,cci) = labelToAssign;
%                     else
%                         labelsNew = assignLabel(labelsNew, detailedLabels_RowFrames, idxGeneral, 0);
%                         assX_rem0(1,cci) = 0;
%                     end
%                 end
%                 titleStr = {['k(' num2str(j) ' of ' num2str(k) ')-all frames fell under label(' num2str(labelToAssign) ')'];['first (' num2str(sum(assX_rem0>0)) ') is assigned according to distance']};
%             else
%                 %we have to verify that this is a meaningful
%                 %cluster
%                 im_clusterCenterIndis = medoidIDsOdCluster(j);
%                 distOf_clusterIndis = distMat(:,im_clusterCenterIndis);
%                 %which cluster does it belong to?
%                 [best_c_d, closestCentroidID] = min(distOf_clusterIndis);
%                 labelToAssign = detailedLabels_ColCentroids(closestCentroidID,end);
%                 allowedDist = handsStructMedoids.maxDistAllowedPerMedoid(labelToAssign);
%                 if allowedDist>=best_c_d
%                     %yes this is a meaningful cluster
%                     %if possible assign all samples to this cluster
%                     for cci = 1:length(allImIDs_curCluster)
%                         idxGeneral = allImIDs_curCluster(cci);
%                         if (allowedDist>=distMatUniq(labelToAssign,idxGeneral)) 
%                             labelsNew = assignLabel(labelsNew, detailedLabels_RowFrames, idxGeneral, labelToAssign);
%                             assX_rem0(1,cci) = labelToAssign;
%                         else
%                             labelsNew = assignLabel(labelsNew, detailedLabels_RowFrames, idxGeneral, 0);
%                             assX_rem0(1,cci) = 0;
%                         end
%                     end                            
%                     titleStr = {['k( ' num2str(j) ' of ' num2str(k) ')-frames fell under label(' num2str(labelToAssign) ' in ' mat2str(assignableLabelsUniq)];['first (' num2str(sum(assX_rem0>0)) ') is assigned according to distance']};
%                 else
%                     %no this is not a meaningful cluster
%                     %remove the labels of all samples if they were
%                     %assigned to sth.
%                     for cci = 1:length(allImIDs_curCluster)
%                         idxGeneral = allImIDs_curCluster(cci);
%                         labelsNew = assignLabel(labelsNew, detailedLabels_RowFrames, idxGeneral, 0);
%                     end                                 
%                     titleStr = ['k( ' num2str(j) ' of ' num2str(k) ')-frames were too bad for label(' num2str(labelToAssign) ' in ' mat2str(assignableLabelsUniq)];
%                 end
%             end
% 
%             figure(h2);
%             title(titleStr);
%             imFullName = [autoClusterFolder filesep 'clusterResult_k' num2str(k,'%02d') '_' num2str(j,'%02d') '.png'];
%             saveas(h2, imFullName);
%         end
    end  
    [maxAcc, idx] = max(accuracyCalced);
    labelsNew = labelsNew{idx};
end
function [N_newVid, distMat, cropIDs, detailedLabels_RowFrames, distMatFileName] = fwf_initializeDistMat(u, r, surSlctListStruct, N_clusters, curVideoHandStruct, distMethod, distMatFoldFileInit)
    %now get the distMat for handsToLabelStruct vs handsStructMedoids
    N_newVid = size(curVideoHandStruct.detailedLabels,1);           
    curVidFileNameIdentStr = [num2str(u) '_' num2str(r)];%'2_1';
    distMatFileName = [distMatFoldFileInit curVidFileNameIdentStr '_semSupDist_' ...
                    distMethod '_' ...
                    surSlctListStruct.labelingStrategyStr '_' num2str(N_clusters,'%02d')...
                    '.mat'];%'.../sn_mc_0_521_' + '2_1' + '_semSupDist_' + 'e3' + '.mat'
    distMat = NaN(N_clusters,N_newVid);
    cropIDs = NaN(2*N_clusters,N_newVid);
    if exist(distMatFileName,'file')
        load(distMatFileName);%'detailedLabels_RowFrames'
    end
    if ~exist('detailedLabels_RowFrames','var')
        distMat = NaN(N_clusters,N_newVid);
        cropIDs = NaN(2*N_clusters,N_newVid);
        detailedLabels_RowFrames = curVideoHandStruct.detailedLabels;
        return
    elseif size(detailedLabels_RowFrames,1)~=N_newVid %#ok<NODEF>
        distMat = NaN(N_clusters,N_newVid);
        cropIDs = NaN(2*N_clusters,N_newVid); 
        detailedLabels_RowFrames = curVideoHandStruct.detailedLabels;
        return
    else
        load(distMatFileName);%'distMat', 'cropIDs', 'detailedLabels_RowFrames', 'detailedLabels_ColCentroids'
        if ~exist('distMat','var') || ~exist('cropIDs','var') || ~exist('detailedLabels_ColCentroids','var')
            disp(['file(' distMatFileName ') exists but distmat needs to be recreated.'])
            distMat = NaN(N_clusters,N_newVid);
            cropIDs = NaN(2*N_clusters,N_newVid);
        end
    end    
end
%labelsNew = fwf_initialize_labelsNew(handsToLabelStruct, surSlctListStruct.single0_double1)
function labelsNew = fwf_initialize_labelsNew(handsToLabelStruct, single0_double1)
    labelsNew.LH = zeros(size(handsToLabelStruct.LH));
    labelsNew.RH = zeros(size(handsToLabelStruct.RH));
    labelsNew.BH = zeros(size(handsToLabelStruct.BH));
    if (single0_double1==0)
        labelsNew.LH(1:5) = 1;
        labelsNew.LH(end-5:end) = 1;
        labelsNew.RH(1:5) = 1;
        labelsNew.RH(end-5:end) = 1;
    else
        labelsNew.BH(1:5) = 1;
        labelsNew.BH(end-5:end) = 1;                
    end
end
%[labelsNew, strAcc, best_c_d, best_c_idx] = fwf_assignLabelByDistance(labelsNew, distMat(:,f),detailedLabels_RowFrames(f,:),detailedLabels_ColCentroids,handsStructMedoids)
function [labelsNew, best_c_d, best_c_idx, strAcc, allowedDist] = fwf_assignLabelByDistance(labelsNew, distMat_row, detailedLabels_RowFrames_row, detailedLabels_ColCentroids, handsStructMedoids)
   [best_c_d, best_c_idx] = min(distMat_row);%distMat(:,f)
   allowedDist = handsStructMedoids.maxDistAllowedPerMedoid(best_c_idx);
   strAcc = 'accepted';
   if allowedDist<best_c_d
        strAcc = 'removed';
   %end
   else
       lh1_rh2_bh3 = detailedLabels_RowFrames_row(4);%detailedLabels_RowFrames(f,4);
       imageIDInVideo = detailedLabels_RowFrames_row(5);%detailedLabels_RowFrames(f,5);
       selectedCluster = detailedLabels_ColCentroids(best_c_idx,end);
       switch lh1_rh2_bh3%[signID userID repID lh1_rh2_bh3 imageIDInVideo]
           case 1
               labelsNew.LH(imageIDInVideo) = selectedCluster;
           case 2
               labelsNew.RH(imageIDInVideo) = selectedCluster;
           case 3
               labelsNew.BH(imageIDInVideo) = selectedCluster;
       end
   end
end
function [knownDistances, knownDistances_append] = updateKnownDistancesFile(knownDistances_append, updateEveryCnt, knownDistances, knownDistancesFileName, distMatFileName, distMat, cropIDs, detailedLabels_RowFrames, detailedLabels_ColCentroids) %#ok<INUSD>
    if size(knownDistances_append,1)>updateEveryCnt
        disp(['Updating(' distMatFileName ') with ' num2str(size(knownDistances_append,1)) ' new calculated distances'])
        [knownDistances, knownDistances_append] = appendKnownDistancesFile(knownDistances, knownDistances_append, knownDistancesFileName);
        save(distMatFileName,'distMat', 'cropIDs', 'detailedLabels_RowFrames', 'detailedLabels_ColCentroids');
        disp('Updated')
    end 
end
function labelsNew = assignLabel(labelsNew, detailedLabels_RowFrames, idInWholeArray, labelToAssign)
    if (detailedLabels_RowFrames(idInWholeArray,4)==1)
        labelsNew.LH(detailedLabels_RowFrames(idInWholeArray,5)) = labelToAssign;
    elseif (detailedLabels_RowFrames(idInWholeArray,4)==2)
        labelsNew.RH(detailedLabels_RowFrames(idInWholeArray,5)) = labelToAssign;
    elseif (detailedLabels_RowFrames(idInWholeArray,4)==3)
        labelsNew.BH(detailedLabels_RowFrames(idInWholeArray,5)) = labelToAssign;
    end
end
function theLabels = getBestDTWFlowResult(distMat, detailedLabels_ColCentroids, possibleDTWFlows)
    %left hand has labels and needs to be written into txt
        possibleDTWFlows = [possibleDTWFlows(:,1);possibleDTWFlows(:,2);possibleDTWFlows(:,3)];
        flowCnt = length(possibleDTWFlows);
        theLabels = cell(flowCnt,1);
        meanRowAccepted = inf(flowCnt,8);
        for i=1:flowCnt
            [ theLabels{i}, theDistances, removedLabelCounts, meanRowAccepted(i,1:5)] = getLabelsFromDistMatByDTW( distMat, detailedLabels_ColCentroids, possibleDTWFlows{i});
            meanRowAccepted(i,6) = sum(theDistances(theLabels{i}~=0));
            meanRowAccepted(i,7) = mean(theDistances(theLabels{i}~=0));
            meanRowAccepted(i,8) = sum(removedLabelCounts);
            %disp(['for sorting of labels = ' mat2str(possibleDTWFlows{i}) ', the distMean is (' mat2str(meanRowAccepted(i,[]),3) ')']);
        end
        mulMap = [1 2 2 2.5;2 1 2 4];
        mmSlct = 1;
        meanDist1_cleverSortVals = mulMap(mmSlct,1)*map2_a_b(1-meanRowAccepted(:,4)./max(meanRowAccepted(:,4)),0,1);
        labelCnt_cleverSortVals = mulMap(mmSlct,2)*map2_a_b(meanRowAccepted(:,5)./max(meanRowAccepted(:,5)),0,1);
        meanDist2_cleverSortVals = mulMap(mmSlct,3)*map2_a_b(1-meanRowAccepted(:,6)./max(meanRowAccepted(:,6)),0,1);
        removedFrameCnt_cleverSortVals = mulMap(mmSlct,4)*map2_a_b(1-meanRowAccepted(:,8)./max(meanRowAccepted(:,8)),0,1);
        cleverSortScores = meanDist1_cleverSortVals + labelCnt_cleverSortVals + meanDist2_cleverSortVals + removedFrameCnt_cleverSortVals;
        [cleverSortScores_sorted,idx] = sort(cleverSortScores./max(cleverSortScores),'descend'); 
        
        meanRowAccepted = [meanRowAccepted(idx,:) cleverSortScores_sorted];
        disptable(meanRowAccepted,'col|usePercent|real1_LR0|meanDistNo1|labelCnt|sumW1|meanW1|removedFrameCnt|cleverSort');
        theLabels = theLabels{idx(1)};
end

