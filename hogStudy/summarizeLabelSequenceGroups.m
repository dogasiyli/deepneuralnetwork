function summaryTableCell = summarizeLabelSequenceGroups(surList, labelSwitches_of_hand, lh1_rh2_bh3)
    countOfDifferentSequences = labelSwitches_of_hand{4};
    [countOfDifferentSequences_sorted, countOfDifferentSequences_inds] = sort(countOfDifferentSequences,'descend');
    numOfDifferentSequences = length(countOfDifferentSequences);
    differentUserList = reshape(unique(surList(:,2)),1,[]);
    summaryTableCell = cell(numOfDifferentSequences,3);
    
    disp(['Number of different sequnces are - in a sorted way - ' mat2str(countOfDifferentSequences_sorted)]);   
    
    txt = sprintf('%-20s %-12s %s','sequenceString','numOfVideos','userDetails');
    disp(txt)
    
    for i=1:numOfDifferentSequences
        inds = countOfDifferentSequences_inds(i);
        stringKnown = labelSwitches_of_hand{1}{inds};
        
        %first column is for label sequence
        summaryTableCell{i,1} = stringKnown;
        %second column has how many videos have this labelling
        summaryTableCell{i,2} = countOfDifferentSequences_sorted(i);
        
        videoIDs = find(labelSwitches_of_hand{3}==inds);
        assert(length(videoIDs)==countOfDifferentSequences_sorted(i),'sth wrong');
        videoTable = surList(videoIDs,:);
        stringConcat = '';
        for j=1:length(differentUserList)
            u=differentUserList(j);
            stringSummary_user = getUserSelectionSummaryAsString(u, surList, videoTable);
            %summaryTableCell{i,2+j} = stringSummary_user;
            if ~strcmp(stringSummary_user,'')
                stringConcat = [stringConcat 'u' num2str(u) '{' stringSummary_user '}/'];
            end
        end
        summaryTableCell{i,3} = stringConcat;
        
        txt = sprintf('%-20s %-12s %s',stringKnown,num2str(countOfDifferentSequences_sorted(i)),stringConcat);
        disp(txt)
    end
end

function summaryString = getUserSelectionSummaryAsString(userID, surList, videoTable)
    list_all = surList(surList(:,2)==userID,3);
    list_slctd = videoTable(videoTable(:,2)==userID,3);
    

    videoCnt_all = length(list_all);
    videoCnt_slctd = length(list_slctd);
    videoCnt_other = videoCnt_all-videoCnt_slctd;
    
    list_other = list_all;
    for i=1:videoCnt_slctd
        list_other(list_other==list_slctd(i))=[];
    end

    if (videoCnt_slctd==videoCnt_all)
        summaryString = ['all(' num2str(videoCnt_slctd) ')'];
    elseif (videoCnt_slctd==0)
        summaryString = '';
    elseif (videoCnt_slctd==1)
        summaryString = ['j(' num2str(list_slctd) ')'];
    elseif (videoCnt_slctd==2)
        summaryString = ['j(' mat2str(list_slctd(:)) ')'];
    elseif (videoCnt_other==1)
        summaryString = ['x(' num2str(list_other) ')'];
    elseif (videoCnt_other==2)
        summaryString = ['x(' mat2str(list_other(:)) ')'];
    else
        summaryString = [num2str(videoCnt_slctd) '-(' mat2str(list_slctd(:)) ')'];
    end
    %summaryString = [num2str()];
end

