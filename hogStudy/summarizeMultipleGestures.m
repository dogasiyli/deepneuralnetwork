function [singleSignSummary, bothSignSummary] = summarizeMultipleGestures(signIDList, optParamStruct)
    %optParamStruct = struct('srcFold', getVariableByComputerName('srcFold'),'userList', 2:7,'initialFigureID', 1);
    if ~exist('optParamStruct', 'var') || ~isstruct(optParamStruct)
        optParamStruct = [];
    end
    srcFold = getStructField(optParamStruct, 'srcFold', getVariableByComputerName('srcFold'));
    userList = getStructField(optParamStruct, 'userList', 2:7);
    initialFigureID = getStructField(optParamStruct, 'initialFigureID', -1);
    rowColCnt = getStructField(optParamStruct, 'rowColCnt', 4);
    figureSaveFolder = getStructField(optParamStruct, 'figureSaveFolder', getVariableByComputerName('srcFold'));
    
    surList = createSURList(srcFold, 2, struct('signIDList',signIDList,'userID',userList,'maxRptCnt',1));
    
    uniqeSignIDs = reshape(unique(surList(:,1)),1,[]);
    
    single_representativeFrameIDs = [];
    single_labelNamesAll = [];
    both_representativeFrameIDs = [];
    both_labelNamesAll = [];
    
    for s = uniqeSignIDs
        %check if it has 
        clustersASPInfoStruct = getClusterInfoFromASPTxt(srcFold, s);
        
        [singLabs, clusterNames_single] = getDetailedLabelsFromClusterAspStruct(clustersASPInfoStruct, 'single');
        [bothLabs, clusterNames_both] = getDetailedLabelsFromClusterAspStruct(clustersASPInfoStruct, 'both');
        
        %displaySigns(srcFold, singLabs, clusterNames_single, 61);
        %displaySigns(srcFold, bothLabs, clusterNames_both, 62);
        
        single_representativeFrameIDs = [single_representativeFrameIDs;singLabs]; %#ok<*AGROW>
        both_representativeFrameIDs = [both_representativeFrameIDs;bothLabs];
        single_labelNamesAll = [single_labelNamesAll;clusterNames_single];
        both_labelNamesAll = [both_labelNamesAll;clusterNames_both];
    end
    
    %if (initialFigureID>0)
        disp('Single hand images');
        [single_labelSummaryCell, single_signNameAss, lastFigureID] = displaySignsGrouped(srcFold, figureSaveFolder, single_representativeFrameIDs, single_labelNamesAll, initialFigureID,'s', signIDList, rowColCnt);
        disp('Both hand images');
        [both_labelSummaryCell, both_signNameAss] = displaySignsGrouped(srcFold, figureSaveFolder, both_representativeFrameIDs, both_labelNamesAll, lastFigureID,'b', signIDList, rowColCnt); 
    %end
    
    singleSignSummary.labelNamesAsList = single_labelNamesAll;
    singleSignSummary.labelNamesSummary = single_labelSummaryCell;
    singleSignSummary.representativeFrameIDs = single_representativeFrameIDs;
    singleSignSummary.signNameAssignments = single_signNameAss;
    
    bothSignSummary.labelNamesAsList = both_labelNamesAll;
    bothSignSummary.labelNamesSummary = both_labelSummaryCell;
    bothSignSummary.representativeFrameIDs = both_representativeFrameIDs;
    bothSignSummary.signNameAssignments = both_signNameAss;
end

function [signSummaryCell, signNameAss, figureID] = displaySignsGrouped(srcFold, figureSaveFolder, singLabs, clusterNames_single, figureID, sdStr, signIDList, rowColCnt)
        %now that I can generate an array with information of the
        %clusterCenter
        
        multipleClusterResultFolderName = [figureSaveFolder filesep 'imageCentroidSummaries' filesep];
        if exist(multipleClusterResultFolderName,'dir')~=7
            mkdir(multipleClusterResultFolderName);
            disp(['created folder(' multipleClusterResultFolderName ') for saving imageCentroidSummaries']);
        else
            disp(['folder(' multipleClusterResultFolderName ') already exist for saving imageCentroidSummaries']);
        end
        [uniqNames, groupCells] = groupCellNames(clusterNames_single);
        nameCnt = length(uniqNames);
        onlyOnePage = nameCnt<=rowColCnt^2;
        
        uniqueSignIDs = unique(singLabs(:,1));
        disp(['There are total of ' num2str(length(uniqueSignIDs)) ' signs.']);
        disp(['Selected Centroid count is ' num2str(length(clusterNames_single)) '  centroid images.']);        
        disp(['Centroids can be further grouped into ' num2str(nameCnt) ' groups.']);
        
        [repCntVec, idx] = sort(-cellfun(@length,groupCells));
        repCntHis = hist(-repCntVec,unique(-repCntVec));

        randomColors = getRandomColorsMat(nameCnt);
        
        signNameAss = zeros(nameCnt,length(uniqueSignIDs));
        
        signSummaryCell = cell(nameCnt,4);
        
        rowCnt = rowColCnt;
        colCnt = rowColCnt;
                
        %rowCnt = ceil(sqrt(nameCnt));
        %colCnt = ceil(nameCnt/rowCnt);
        imCollage_imSizeMax = [50 50];
        figIDAdd = -1;
        for i=1:nameCnt
            numOfImages = length(groupCells{i});
            
            signIDsCur = singLabs(groupCells{i},1);
            indsFound = getInds(signIDsCur, signIDList);
            signNameAss(i,indsFound) = 1;
            
            signSummaryCell{i,1} = i;
            signSummaryCell{i,2} = uniqNames{i};
            signSummaryCell{i,3} = groupCells{i};        
            signSummaryCell{i,4} = reshape(signIDsCur,size(groupCells{i}));        
            
            surroundWithColorStruct = struct('thicknessParam',0.2,'colorParam',randomColors(i,:));
            if numOfImages==1
                theAssignedRow = groupCells{i};
                imPalette = retrieveImageFromDetailedLabels(srcFold, theAssignedRow, singLabs);
                titleStr = [num2str(i) '-' uniqNames{i} '-s[' num2str(singLabs(theAssignedRow,1)) ']'];
            else
                %multiple images..
                theAssignedRow = groupCells{i};
                [imPalette, paletteRCSize] = imCollageFuncs( 'createPalette', struct('imCnt',numOfImages,'channelCnt',3,'imSizeMax',3*imCollage_imSizeMax));
                for j = 1:numOfImages
                    %find a sample of the label - if possible the one closest
                    %to the 
                    imageToAdd = retrieveImageFromDetailedLabels(srcFold,  theAssignedRow(j), singLabs);
                    if isempty(imageToAdd)
                        continue
                    end                
                    imPalette = imCollageFuncs('insertIntoPalette', struct('imageID',j,'paletteRCSize',paletteRCSize,'imPalette',imPalette,'imageToAdd',imageToAdd,'boxSize',3*imCollage_imSizeMax, 'surroundWithColorStruct', surroundWithColorStruct));
                end
                
                if length(theAssignedRow)>6
                    titleStr = {[num2str(i) '-' uniqNames{i} ];[num2str(length(theAssignedRow)) ' of ' num2str(length(unique(singLabs(:,1))))  ' signs ']};
                else
                    titleStr = {[num2str(i) '-' uniqNames{i} ];['s' strrep(mat2str(singLabs(theAssignedRow,1)),' ','/')]};
                end
            end
            
            
            mod_i = 1+mod((i-1),rowCnt*colCnt);
            if mod_i==1
                figIDAdd = figIDAdd+1;
                if figIDAdd>0
                    h = figure(figureID + figIDAdd-1);
                    multipleClusterResultFileName = [multipleClusterResultFolderName sdStr '_' num2str(figIDAdd-1,'%02d') '.png'];
                    maximizeFigureWindow(h);
                    saveas(h,multipleClusterResultFileName);
                end
                if figureID>0
                    h = figure(figureID + figIDAdd);
                    clf;
                end
            end
            
            if figureID>0
                subplot(rowCnt,colCnt,mod_i);
                imshow(imPalette);
                title(titleStr);  
            end
        end
        
        if figureID>0
            multipleClusterResultFileName = [multipleClusterResultFolderName sdStr '_' num2str(figIDAdd,'%02d') '.png'];
            maximizeFigureWindow(h);
            saveas(h,multipleClusterResultFileName);
            disp(['++Saved ' num2str(figIDAdd) ' to "' multipleClusterResultFileName '" image for summary .']);
            figureID = figureID + figIDAdd + 1;
        end
end

function indsFound = getInds(theSmallList, theBigList)
    indsFound = NaN(length(theSmallList),1);
    for i=1:length(theSmallList)
        indsFound(i) = find(theBigList==theSmallList(i));
    end
end

function displaySigns(srcFold, singLabs, clusterNames_single, figureID)
        %now that I can generate an array with information of the
        %clusterCenter
        
        imCnt = size(singLabs,1);
        rowCnt = ceil(sqrt(imCnt));
        colCnt = ceil(imCnt/rowCnt);
        figure(figureID);
        clf;
        for i=1:size(singLabs,1)
            im01_medoidIm = retrieveImageFromDetailedLabels(srcFold, i, singLabs);
            subplot(rowCnt,colCnt,i);
            imshow(im01_medoidIm);
            title(['s(' num2str(singLabs(i,1)) ')-' clusterNames_single{i}]);
        end
end
