function [matToFill,maxOfAll] = updateStateOfFiles_hospiSign(M1, M2_ks, M3)

    global dnnCodePath;
    colNames_M2 = {'signID', 'folderSrc', 'hogSrc', 'surfSrc', 'folderBck', 'hogBck', 'surfBck'}; %#ok<NASGU>
    computerID = getVariableByComputerName('computerID');
    
    
    [rowCnt,colCnt] = size(M2_ks);
    matToFill = NaN(rowCnt,1+3*colCnt);  
    matToFill(:,1) = 1:rowCnt;  
    
    %mat files will have - "M1, M2_ks, M3" matrices
    %computerID_X as struct with "M1, M2_ks, M3"
    stateOfFilesFolderName = [dnnCodePath 'stateOfFiles'];
    createFolderIfNotExist(stateOfFilesFolderName);
    stateOfFilesMatFileName = [stateOfFilesFolderName filesep 'stateOfFiles_' num2str(computerID) '.mat'];
    save(stateOfFilesMatFileName,'M1','M2_ks','M3');
    datesCell = {'unknown','unknown','unknown'};
    
    for i=1:3
        colFr = 2 + (i-1)*colCnt;
        colTo = colFr + colCnt-1;
        stateOfFilesMatFileName = [stateOfFilesFolderName filesep 'stateOfFiles_' num2str(i) '.mat'];
        if exist(stateOfFilesMatFileName,'file')
            fileInfo_src = dir(stateOfFilesMatFileName);
            datesCell{i} = fileInfo_src.date;
            load(stateOfFilesMatFileName,'M2_ks');%loads M2_ks
            matToFill(:,colFr:colTo) = M2_ks;
        else
            matToFill(:,colFr) = M2_ks(:,1);
            matToFill(:,colFr+1:colTo) = 0;
        end        
    end
    matToFill_MatFileName = [stateOfFilesFolderName filesep 'stateOfAllComputers.mat'];
    
    maxOfAll = zeros(rowCnt,4);
    maxOfAll(:,1) = M2_ks(:,1);%signID
    maxOfAll(:,2) = max(matToFill(:,0+[3 6 10 13 17 20]),[],2);%folderCnt
    maxOfAll(:,3) = max(matToFill(:,1+[3 6 10 13 17 20]),[],2);%hog
    maxOfAll(:,4) = max(matToFill(:,2+[3 6 10 13 17 20]),[],2);%sn
    
    disptable(maxOfAll,'signID|folderCnt|hogFeat|surfNorm');
    for i=1:3
        if ~strcmpi(datesCell{i},'unknown')
            disp(['CompID_' num2str(i) ' date = ' datesCell{i}]);
        end
    end
    
    save(matToFill_MatFileName,'matToFill','maxOfAll','datesCell','colNames_M2');
end