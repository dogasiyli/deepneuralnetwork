function [valid, dict_vars, cell_cnt_per_user] = validate_hand_imgs_fold( fold , khs_group, khs_name, csv_file_name, save_into_fold)
    fold_list_khs = getFolderList(fold, false, false);
    valid = true;
    dict_vars = [];
    if nargin<3
        khs_group='';
        khs_name='';
    end
    if nargin<5
        csv_file_name='';
        save_into_fold=true;
    end
    
    cell_summary = {'fold_khs','khs_name','png_count','dict_count','skel_row_cnt','surfImArr_row_cnt'};
    cell_sum_rid = 2;
    cell_cnt_per_user = {'fold_khs','khs_name','u2','u3','u4','u5','u6','u7'};
    cell_cnt_rid = 2;
    
    disp('********')
    if ~strcmpi(khs_group,'')
        clusterRow = [];
        for i=1:length(fold_list_khs)
            if strcmpi(fold_list_khs{i},khs_group)
                clusterRow=i;
                break
            end
        end
        %clusterRow = find(not(cellfun('isempty',strfind(fold_list_khs,khs_name)))); %#ok<STRCL1>
        if ~isempty(clusterRow)
            fold_list_khs = fold_list_khs(clusterRow);
            disp(['only khs_group(' khs_group ') at inds(' num2str(clusterRow) ') will be checked']);
        else
            disp(['khs_group(' khs_group ') not found under fold(' fold ')']);
            valid = false;
        end
    end
    for i=1:length(fold_list_khs)
        cur_fold_khs = fold_list_khs{i};
        if ~strcmpi(khs_group,'') && ~strcmpi(khs_name,'') && length(fold_list_khs)==1
            list_dict_file_name_list = {['list_dict_' khs_name '.txt']};
        else
            list_dict_file_name_list = getFileList([fold filesep cur_fold_khs], '.txt', 'list_dict_', false); 
        end
        for fi_id = 1:size(list_dict_file_name_list,1)
            disp('+++++++++')
            list_dict_file_name_end = list_dict_file_name_list{fi_id};
            list_dict_file_name = [fold filesep cur_fold_khs filesep list_dict_file_name_end];
            khs_name = strrep(strrep(list_dict_file_name_end, 'list_dict_', ''),'.txt','');
            png_list_khs = getFileList([fold filesep cur_fold_khs], '.png', [khs_name '_'], false);
            png_count = size(png_list_khs,1);
            [list_dict, urf_vec, urf_bool, dict_count] = read_dict_file(list_dict_file_name);
            [surfImArr_row_cnt, skel_row_cnt, surfImArr_label, skel_label] = get_cnts(fold, cur_fold_khs, khs_name);
            
            cnt_vec = [png_count dict_count surfImArr_row_cnt skel_row_cnt];
            uniq_cnt_cnt = length(unique(cnt_vec));
            
            if length(fold_list_khs)==1
                if uniq_cnt_cnt~=1
                    valid = false;
                    dict_vars = [];
                else
                    try
                        valid = true;
                        list_dict_label = fileread(list_dict_file_name);
                        dict_vars = struct('surfImArr_label',surfImArr_label,'skel_label',skel_label,'list_dict_label',list_dict_label);
                        fprintf('clusterName(%s),counts validated(%d)\n',khs_name,skel_row_cnt);
                        disp('---------')
                        return
                    catch err
                        disp(err.message)
                        valid = false;
                    end
                end
            else
                valid = valid&true;
            end
            
            if uniq_cnt_cnt==1 && png_count>0
                disp([khs_name ' counts match(' num2str(png_count) ')'])
            else
                fprintf('XX-clusterName(%s),counts wrong - png_count(%d),skel_row_cnt(%d),surfImArr_row_cnt(%d),dict_count(%d)\n',khs_name,png_count,skel_row_cnt,surfImArr_row_cnt,dict_count);
                valid = false;
                cnt_not_found = 0;
                cnt_repeated = 0;
                if ~isempty(urf_vec)
                    for p=1:length(png_list_khs)
                        png_name = png_list_khs{p};
                        png_vec = split(png_name,'_');
                        sur_str = png_vec{2};
                        lrb_int = LRB_to_int(png_vec{3});
                        frame_id = str2double(strrep(png_vec(end),'.png',''));
                        signID = str2double(sur_str(1:3));
                        userID = str2double(sur_str(4));
                        repID = str2double(sur_str(5:6));

                        urf_str = [num2str(signID,'%03d') num2str(lrb_int) num2str(userID) num2str(repID,'%02d') num2str(frame_id,'%03d')];

                        %find [userID repID frame_id]
                        %urf_row_id = find(not(cellfun('isempty',strfind(urf_vec,urf_str))));
                        urf_row_id = find(contains(urf_vec,urf_str));
                        if length(urf_row_id)==1
                            if urf_bool(urf_row_id)==false
                                urf_bool(urf_row_id)=true;
                            else
                                disp_dict_row(list_dict, urf_row_id, '- repeated_in_dict')
                                cnt_repeated=cnt_repeated+1;
                            end
                        elseif isempty(urf_row_id)
                            disp([png_name '-only_as_png-not found in dict'])
                            cnt_not_found=cnt_not_found+1;
                        else
                            error('multiple occurances')
                        end
                    end
                    urf_row_ids = find(urf_bool==false);
                    cnt_only_in_dict = length(urf_row_ids);
                    for p=1:length(urf_row_ids)
                        disp_dict_row(list_dict, urf_row_ids(p), '-only_in_dict-no png')
                    end
                else
                    cnt_not_found = length(png_list_khs);
                    cnt_only_in_dict = 0;
                    cnt_repeated = 0;    
                end
                disp(['summary_01 - only_as_png(' num2str(cnt_not_found) '),only_in_dict(' num2str(cnt_only_in_dict) '),repeated_in_dict(' num2str(cnt_repeated) ')'])                
            end
            if png_count>0
                disp(['summary_02 - png_count(' num2str(png_count) '), dict_count(' num2str(dict_count) '),skel_row_cnt(' num2str(skel_row_cnt) '),surfImArr_row_cnt(' num2str(surfImArr_row_cnt) ')'])
                [cell_summary, cell_sum_rid] = add_2_cell_sum(cell_summary, cell_sum_rid,cur_fold_khs,khs_name,png_count,dict_count,skel_row_cnt,surfImArr_row_cnt);
            end
            [cell_cnt_per_user, cell_cnt_rid] = add_2_cell_cnt(cell_cnt_per_user, cell_cnt_rid,cur_fold_khs,khs_name,list_dict);
            disp('+++++++++')
        end
    end
    disp('********')
    
    if ~strcmpi(csv_file_name,'')
        if save_into_fold
            csv_file_name = [fold filesep csv_file_name];
        end
        T = cell2table(cell_summary(2:end,:),'VariableNames',cell_summary(1,:));
        writetable(T,csv_file_name);
    end
    
    csv_file_name = [fold filesep 'cell_cnt_per_user_' num2str(length(fold_list_khs)) '.csv'];
    T = cell2table(cell_cnt_per_user(2:end,:),'VariableNames',cell_cnt_per_user(1,:));
    writetable(T,csv_file_name);
end

function [surfImArr_row_cnt, skel_row_cnt, surfImArr_label, skel_label] = get_cnts(fold, cur_fold_khs, khs_name)
    %list_dict_file_name = [fold filesep cur_fold_khs filesep 'list_dict_' khs_name '.txt'];
    sn_khs_file_name = [fold filesep cur_fold_khs filesep 'sn_' khs_name '.mat'];
    skel_khs_file_name = [fold filesep cur_fold_khs filesep 'skel_' khs_name '.mat'];
    if exist(sn_khs_file_name,'file')
        surfImArr_label = load(sn_khs_file_name);
        surfImArr_label = surfImArr_label.mat2sa;
        surfImArr_row_cnt = size(surfImArr_label,1);
    else
        surfImArr_label = [];
        surfImArr_row_cnt = 0;
    end
    if exist(skel_khs_file_name,'file')
        skel_label = load(skel_khs_file_name);
        skel_label = skel_label.mat2sa; 
        skel_row_cnt = size(skel_label,1);
    else
        skel_label = [];
        skel_row_cnt = 0;
    end  
    %if exist(list_dict_file_name,'file')
    %    list_dict_label = fileread(list_dict_file_name);
    %    dict_count = count(list_dict_label,khs_name);
    %else
    %    dict_count = 0;
    %end  
end

function lrb_int = LRB_to_int(lrb_char)
    if lrb_char=='L'
        lrb_int=1;
        return
    elseif lrb_char=='R'
        lrb_int=2;
        return
    end
    lrb_int=3;
end

function [cell_summary, cell_sum_rid] = add_2_cell_sum(cell_summary, cell_sum_rid,cur_fold_khs,khs_name,png_count,dict_count,skel_row_cnt,surfImArr_row_cnt)
    cell_summary(cell_sum_rid,:) = {cur_fold_khs,khs_name,png_count,dict_count,skel_row_cnt,surfImArr_row_cnt};
    cell_sum_rid = cell_sum_rid+1;
end

function [cell_cnt_per_user, cell_cnt_rid] = add_2_cell_cnt(cell_cnt_per_user, cell_cnt_rid,cur_fold_khs,khs_name,list_dict)
    uc = zeros(1,6);
    if ~isempty(list_dict)
        for u=2:7
            uc(u-1) = sum(list_dict{1,2}==u);
        end
    end
    cell_cnt_per_user(cell_cnt_rid,:) = {cur_fold_khs,khs_name,uc(1),uc(2),uc(3),uc(4),uc(5),uc(6)};
    cell_cnt_rid = cell_cnt_rid+1;    
end

function [list_dict, urf_vec, urf_bool, dict_count] = read_dict_file(list_dict_file_name)
    if exist(list_dict_file_name, 'file')
        fid = fopen(list_dict_file_name,'r');
        list_dict = textscan(fid, '%d%d%d%d%d%s%s', 'Delimiter', '*','TreatAsEmpty','~');
        fclose(fid);
        
        lrb_int = zeros(size(list_dict{1,7}),'int32');
        lrb_int(not(cellfun('isempty',strfind(list_dict{1,7},'L'))))=1;
        lrb_int(not(cellfun('isempty',strfind(list_dict{1,7},'R'))))=2;
        lrb_int(not(cellfun('isempty',strfind(list_dict{1,7},'B'))))=3;
        
        signID_col  = cellfun(@(x) num2str(x,'%03d'),list_dict(1),'UniformOutput', false);
        signID_col = signID_col{1};
        lrb_int_col = cellfun(@(x) num2str(x,'%d'),{lrb_int},'UniformOutput', false);
        lrb_int_col = lrb_int_col{1};
        userID_col  = cellfun(@(x) num2str(x,'%d'),list_dict(2),'UniformOutput', false);
        userID_col = userID_col{1};
        repID_col   = cellfun(@(x) num2str(x,'%02d'),list_dict(3),'UniformOutput', false);
        repID_col = repID_col{1};
        frameID_col   = cellfun(@(x) num2str(x,'%03d'),list_dict(4),'UniformOutput', false);
        frameID_col = frameID_col{1};
        new_line_col = repmat({newline},size(list_dict{4}));
        
        urf_vec = strrep(strjoin([string(signID_col) string(lrb_int_col) string(userID_col) string(repID_col) string(frameID_col) new_line_col]'),' ','');
        urf_vec = cellstr(splitlines(urf_vec));
        urf_vec= urf_vec(1:end-1);
        
        dict_count = size(list_dict{1},1);
    else
        list_dict = '';
        urf_vec = [];
        dict_count = 0;
    end
    urf_bool = false(length(urf_vec),1);
end

function disp_dict_row(list_dict, row_id, init_str)
    if isempty(list_dict)
        return
    end
    fprintf(init_str);
    fprintf('Line(%d) : ',row_id);
    for i=1:5
        fprintf('%d * ',list_dict{i}(row_id));
    end
    fprintf('%d-%d-%d-%d-%d-%s * %s\n',list_dict{1,1}(row_id),list_dict{1,2}(row_id),list_dict{1,3}(row_id),list_dict{1,4}(row_id),list_dict{1,5}(row_id),string(list_dict{1,6}(row_id)),string(list_dict{1,7}(row_id)));  
end
