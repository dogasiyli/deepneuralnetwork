function [ numOfFrames, combinedImg ] = viewVideoAsClusters(sourceFolder, signID, userID, repetitionID)
%[ numOfFrames ] = viewVideoAsClusters('/mnt/Data/FromUbuntu/HospiSign/HospiMOV', 13, 2, 1)

%viewVideoAsClusters plot the video frames with the already defined
%clusters

%1. load the cluster matrix
%2. get only the part of cluster matrix which is related to 
%   clusterMemberMatrix - [*signID* *userID* *repetitionID* frameID]
%   into [clusterID frameID] matrix
%3. The view will be :
%   15 frames per row
%   each row will be [30*3+2*10] rows, [30*15] cols, 3 channels
%   a frame will be viewed from top to bottom as follows
%   [ 1-30] - frame itself
%   [31-60] - left hand cluster equivelant if any
%   [61-70] - left hand cluster color if any, or black
%   [71-100] - right hand cluster equivelant if any
%   [101-110] - left hand cluster color if any, or black

    userFolderName = [sourceFolder filesep num2str(signID) filesep 'User_' num2str(userID) '_' num2str(repetitionID)];
    nof = getNumOfFilesInFolder([userFolderName filesep 'cropped'], 'png');
    numOfFrames = round(nof/3);
    
    framesPerRow = 15;
    
    rowCellCnt = ceil(numOfFrames/framesPerRow);
    colCellCnt = framesPerRow;
    dispImgRCSize = 30;
    clustColorRowSize = 10;
    
    paletteCells = cell(rowCellCnt,1);
    
    [rhCurClusters, rhClusterImgs] = getClusterImages([sourceFolder filesep 'rightHandClusters.mat'], dispImgRCSize);
    [lhCurClusters, lhClusterImgs] = getClusterImages([sourceFolder filesep 'leftHandClusters.mat'], dispImgRCSize); 
    
    videoClusterAssignmentVec = zeros(numOfFrames, 3);%[frameID rhClusID lhClusID]
    videoClusterAssignmentVec(:,1) = 1:numOfFrames;
    videoClusterAssignmentVec = getClusterAssignmentVec(videoClusterAssignmentVec, 2, rhCurClusters, signID, userID, repetitionID);
    videoClusterAssignmentVec = getClusterAssignmentVec(videoClusterAssignmentVec, 1, lhCurClusters, signID, userID, repetitionID);
    
    curPalette_rh = getPaletteForCluster(videoClusterAssignmentVec, 2, rhClusterImgs, framesPerRow, dispImgRCSize, clustColorRowSize);
    curPalette_lh = getPaletteForCluster(videoClusterAssignmentVec, 1, lhClusterImgs, framesPerRow, dispImgRCSize, clustColorRowSize);
    vidFramesPalette = getPaletteForVideo(userFolderName, numOfFrames, framesPerRow, dispImgRCSize);
    
     [combinedPaletteCells, combinedImg] = combinePalettes(vidFramesPalette, curPalette_lh, curPalette_rh, numOfFrames, framesPerRow, dispImgRCSize, clustColorRowSize);
end

function [combinedPaletteCells, combinedImg] = combinePalettes(vidFramesPalette, curPalette_lh, curPalette_rh, numOfFrames, framesPerRow, dispImgRCSize, clustColorRowSize)
%create the palette cells for a cluster
    %framesPerRow = 15;dispImgRCSize = 30;
    rowCellCnt = ceil(numOfFrames/framesPerRow);
    colCellCnt = framesPerRow;
    combinedPaletteCells = cell(rowCellCnt,2);
    
    figure(1);clf;

    combinedImg = zeros([rowCellCnt*2*(dispImgRCSize+clustColorRowSize),2*colCellCnt*dispImgRCSize,3],'uint8');
    for pc = 1:rowCellCnt
        cp_lh = curPalette_lh{pc,:};
        cp_rh = curPalette_rh{pc,:};
        cp_vd_lh = vidFramesPalette{pc,1};
        cp_vd_rh = vidFramesPalette{pc,2};
        frFrame = (pc-1)*framesPerRow+1;
        
        blockColSize = 2*dispImgRCSize;
        curPalette = zeros([2*(dispImgRCSize+clustColorRowSize),2*colCellCnt*dispImgRCSize,3],'uint8');
        
        for j = 1:framesPerRow
            if frFrame+j-1>numOfFrames
                j = j-1; %#ok<FXSET>
                break;
            end
            % imID = frFrame+j-1;
            % im_lh = zeros([2*dispImgRCSize,dispImgRCSize,3],'uint8');
            % im_rh = zeros([2*dispImgRCSize,dispImgRCSize,3],'uint8');
            % im_cc = zeros([2*clustColorRowSize,2*dispImgRCSize,3],'uint8');
            curBlock = zeros([2*(dispImgRCSize+clustColorRowSize),blockColSize,3],'uint8');
            %%%%%%%%%%%%%%%%%%%%%%---%%%%%%%%%%%%%%%%%%%%%%
            %%im_vd_lh%%im_vd_rh%%---%%30 x 30 %%30 x 30 %%im_lh-im_rh
            %%%%%%%%%%%%%%%%%%%%%%---%%%%%%%%%%%%%%%%%%%%%%im_lh-im_rh
            %%im_cl_lh%%im_cl_rh%%---%%30 x 30 %%30 x 30 %%im_lh-im_rh
            %%%%%%%%%%%%%%%%%%%%%%---%%%%%%%%%%%%%%%%%%%%%%
            %%im_color%%im_color%%---%%10 x 30 %%--------%%im_cc-im_cc
            %%col_lh  %%col_rh  %%---%%--------%%10 x 30 %%im_cc-im_cc
            %%%%%%%%%%%%%%%%%%%%%%---%%%%%%%%%%%%%%%%%%%%%%
            
            colFr = (j-1)*dispImgRCSize+1;
            colTo = j*dispImgRCSize;
            
            frto = [1 dispImgRCSize dispImgRCSize+1 2*dispImgRCSize 2*dispImgRCSize+1 2*dispImgRCSize+clustColorRowSize 2*dispImgRCSize+clustColorRowSize+1 2*dispImgRCSize+2*clustColorRowSize];
            
            %single frame operations to fill
            curBlock(frto(1):frto(2),frto(1):frto(2),:) = cp_vd_lh(frto(1):frto(2),colFr:colTo,:);%add im_vd_lh
            curBlock(frto(1):frto(2),frto(3):frto(4),:) = cp_vd_rh(frto(1):frto(2),colFr:colTo,:);%add im_vd_rh
            curBlock(frto(3):frto(4),frto(1):frto(2),:) =    cp_lh(frto(1):frto(2),colFr:colTo,:);%add cp_lh
            curBlock(frto(3):frto(4),frto(3):frto(4),:) =    cp_rh(frto(1):frto(2),colFr:colTo,:);%add cp_rh

            curBlock(frto(5):frto(6),frto(1):frto(2),:) =    cp_lh(frto(3):end,colFr:colTo,:);%add cp_lh-color of cluster
            curBlock(frto(7):frto(8),frto(3):frto(4),:) =    cp_rh(frto(3):end,colFr:colTo,:);%add cp_rh-color of cluster
            
            colFr = (j-1)*2*dispImgRCSize+1;
            colTo = j*2*dispImgRCSize;

            curPalette(:,colFr:colTo,:) = curBlock;
        end
        h = subplot(rowCellCnt,1,pc);
        image(curPalette);
        
        frCnt = j;
        frFr = frFrame;
        toFr = frFrame+frCnt;
        initCol = round(blockColSize/2);
        setXYTicks(h, initCol:blockColSize:blockColSize*frCnt-1, frFr:toFr, initCol);
        set(gca,'ytick',[])
        
        drawnow;
        combinedPaletteCells{pc,1} = curPalette;
        
        rs =2*(dispImgRCSize+clustColorRowSize);
        combinedImg((pc-1)*rs+1:pc*rs,:,:) = curPalette;
    end
end

function paletteCells = getPaletteForVideo(userFolderName, numOfFrames, framesPerRow, dispImgRCSize)
%create the palette cells for a cluster
    %framesPerRow = 15;dispImgRCSize = 30;
    rowCellCnt = ceil(numOfFrames/framesPerRow);
    colCellCnt = framesPerRow;
    paletteCells = cell(rowCellCnt,2);

    for pc = 1:rowCellCnt
        curPalette_lh = zeros([dispImgRCSize,colCellCnt*dispImgRCSize,3],'uint8');
        curPalette_rh = zeros([dispImgRCSize,colCellCnt*dispImgRCSize,3],'uint8');
        frFrame = (pc-1)*framesPerRow+1;
        for j = 1:framesPerRow
            if frFrame+j-1>numOfFrames
                break;
            end
            imID = frFrame+j-1;
            im_lh = imread([userFolderName  filesep 'cropped/crop_LH_' num2str(imID,'%03d') '.png']);
            im_rh = imread([userFolderName filesep 'cropped/crop_RH_' num2str(imID,'%03d') '.png']);

            colFr = (j-1)*dispImgRCSize+1;
            colTo = j*dispImgRCSize;
            
            curPalette_lh(1:dispImgRCSize,colFr:colTo,:) = imresize(im_lh,[dispImgRCSize,dispImgRCSize]);
            curPalette_rh(1:dispImgRCSize,colFr:colTo,:) = imresize(im_rh,[dispImgRCSize,dispImgRCSize]);
        end
        paletteCells{pc,1} = curPalette_lh;
        paletteCells{pc,2} = curPalette_rh;
    end
end

function paletteCells = getPaletteForCluster(videoClusterAssignmentVec, lh1_rh2, clusterImgs, framesPerRow, dispImgRCSize, clustColorRowSize)
%create the palette cells for a cluster
    %framesPerRow = 15;dispImgRCSize = 30;
    numOfFrames = size(videoClusterAssignmentVec,1);
    rowCellCnt = ceil(numOfFrames/framesPerRow);
    colCellCnt = framesPerRow;
    paletteCells = cell(rowCellCnt,1);
    
    foundUniqClusterIDs = unique(sort(videoClusterAssignmentVec(:,lh1_rh2+1)));
    
    for pc = 1:rowCellCnt
        curPalette = zeros([dispImgRCSize+clustColorRowSize,colCellCnt*dispImgRCSize,3],'uint8');
        frFrame = (pc-1)*framesPerRow+1;
        for j = 1:framesPerRow
            if frFrame+j-1>numOfFrames
                break;
            end
            %what is the selected cluster??
            clusterID = videoClusterAssignmentVec(frFrame+j-1,lh1_rh2+1);
            if clusterID>0
                clusterColorID = find(foundUniqClusterIDs==clusterID) - 1;
                colorToUse = dec2bin(clusterColorID,3);
                colFr = (j-1)*dispImgRCSize+1;
                colTo = j*dispImgRCSize;
                colFrClus = (clusterID-1)*dispImgRCSize+1;
                colToClus = clusterID*dispImgRCSize;
                curPalette(1:dispImgRCSize,colFr:colTo,:) = clusterImgs(:,colFrClus:colToClus,:);
                
                curPalette(1+dispImgRCSize:end,colFr:colTo,1) = 255*str2double(colorToUse(1));
                curPalette(1+dispImgRCSize:end,colFr:colTo,2) = 255*str2double(colorToUse(2));
                curPalette(1+dispImgRCSize:end,colFr:colTo,3) = 255*str2double(colorToUse(3));
            end
        end
        paletteCells{pc,1} = curPalette;
    end
end

function [curClusters, clustImgs] = getClusterImages(clusterFileName, dispImgRCSize)
    if exist(clusterFileName,'file')
        curClusters = [];
        load(clusterFileName, 'curClusters');

        %now lets work on curClusters
        numOfKnownClusters = size(curClusters, 2);
        clustImgs = zeros([dispImgRCSize,numOfKnownClusters*dispImgRCSize,3], 'uint8');
        for i=1:numOfKnownClusters
            curClus = curClusters{1,i};
            smallImg = curClus.bestSelectedImagesToView;
            smallImg = smallImg(1:90,1:90,:);
            im2disp = imresize(smallImg, [dispImgRCSize dispImgRCSize]);
            clustImgs(:,((i-1)*dispImgRCSize+1):(i*dispImgRCSize),:) = im2disp;
        end
    end
end

function videoClusterAssignmentVec = getClusterAssignmentVec(videoClusterAssignmentVec, lh1_rh2, curClusters, signID, userID, repetitionID)
%2. get only the part of cluster matrix which is related to 
%   clusterMemberMatrix - [*signID* *userID* *repetitionID* frameID]
%   into [clusterID frameID] matrix

    numOfKnownClusters = size(curClusters, 2);
    for clusterID=1:numOfKnownClusters
        clusterMemberMatrix = curClusters{1,clusterID}.clusterMemberMatrix;
        clusterMemberMatrix = clusterMemberMatrix(clusterMemberMatrix(:,1)==signID,:);
        clusterMemberMatrix = clusterMemberMatrix(clusterMemberMatrix(:,2)==userID,:);
        clusterMemberMatrix = clusterMemberMatrix(clusterMemberMatrix(:,3)==repetitionID,:);
        videoClusterAssignmentVec(clusterMemberMatrix(:,4),lh1_rh2+1) = clusterID;
    end
end

function nof = getNumOfFilesInFolder(folderFull, extension)
%https://www.mathworks.com/matlabcentral/newsreader/view_thread/272630
    if (folderFull(end)==filesep)
        folderFull = folderFull(1:end-1);
    end
    if exist('extension','var') && ~isempty(extension) && ~strcmp(extension,'')
        extension = strrep(extension,'.','');%remove the dot if there is
        D = dir([folderFull, [filesep '*.' extension]]);
    else
        D = dir([folderFull, [filesep '*.']]);
    end
    nof = length(D(not([D.isdir])));
end