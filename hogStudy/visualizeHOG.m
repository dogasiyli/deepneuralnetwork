function [visualHogImage, combinedImage, h] = visualizeHOG(grayIm, hogVec, blockBoundsMat, hogVecBounds, plotType, histNorms)


    if ~exist('plotType','var')
        plotType = 'none';%'eachSteps','blockSteps','final'
    end
    
    if ~strcmp(plotType,'none')
        h = figure(23);clf;
        subplot(2,2,1);
        imagesc(grayIm);
    else
        h = [];
    end
    
    colorMul = 1;
    if max(grayIm(:)>1)
        colorMul = 255;
    end    
    
    visualHogImage = zeros(size(grayIm));
    
    blockCnt = size(blockBoundsMat,1);
    hogBinCnt = size(hogVec,1)/blockCnt;

    hogAngleCenters =-pi+2*pi/hogBinCnt : 2*pi/hogBinCnt : pi;
    unitVecs = [cos(hogAngleCenters)',sin(hogAngleCenters)'];
    combinedImage = grayIm;
    
    for i = 1:blockCnt
        curBlockBounds = blockBoundsMat(i,:);
        curHOGFeats = hogVec(hogVecBounds(i,1):hogVecBounds(i,2));
        
        centerPoint = [round(sum(curBlockBounds(1:2))/2), round(sum(curBlockBounds(3:4))/2)];
        lineLengths = [curBlockBounds(2)-curBlockBounds(1), curBlockBounds(4)-curBlockBounds(3)]/2;
        
        lineLensPerHogFeat = bsxfun(@times,lineLengths,curHOGFeats.^2);
        ptFr = centerPoint;
        for j=1:size(unitVecs,1)
            %each hog center will draw a line on the current block
            ptTo = ptFr + lineLensPerHogFeat(j,:).*unitVecs(j,:);
            visualHogImage = drawLineOnImg(visualHogImage, ptFr, ptTo, histNorms(i), 1);
            if strcmp(plotType,'eachSteps')            
                subplot(2,2,2);
                imagesc(visualHogImage);
                combinedImage(visualHogImage>0) = min(combinedImage(:));
                h01_draw_combined_image(combinedImage);
                drawnow;
            end
        end
        if strcmp(plotType,'blockSteps')            
            subplot(2,2,2);
            imagesc(visualHogImage);
            combinedImage(visualHogImage>0) = min(combinedImage(:));
            h01_draw_combined_image(combinedImage);
            drawnow;
        end
    end
    combinedImage(visualHogImage>0) = max(combinedImage(:));
    
    if ~strcmp(plotType,'none')          
        subplot(2,2,2);
        imagesc(visualHogImage);
        drawnow;
    end
    if ~strcmp(plotType,'none')
        h01_draw_combined_image(combinedImage);
    end 
end

function h01_draw_combined_image(combinedImage)
    subplot(2,2,3);
    imagesc(combinedImage);
end
