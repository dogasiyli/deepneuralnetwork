nos = 11;
list_dict = get_list_dict_by_comp(nos, true);
%columns = sign-01 / user-02 / rep-03 / frameID-04 / khsID - 05 / khsName - 06 / LRB - 07
uniq_signIDs = unique(list_dict{1});

test_signIDs = uniq_signIDs(1:4);
train_signIDs = uniq_signIDs(~ismember(uniq_signIDs,test_signIDs));

[khsIDs, khsInds] = unique(list_dict{5});
khsCnt = length(khsInds);
khsNames = cell(khsCnt,1);
khsNamesAll = strtrim(list_dict{6});
for i=1:khsCnt
    listRows = list_dict{5}==khsIDs(i);
    khsNamesRows = khsNamesAll(listRows);
    signIDs = list_dict{1}(listRows);
    signStr = mat2str(unique(signIDs));
    curNames = unique(khsNamesRows);
    str_2_write = [num2str(i,'%02d') '-' ];
    for j=1:length(curNames)
        str_2_write = [str_2_write curNames{j} ', '];
    end
    str_2_write = str_2_write(1:end-2);
    str_2_write = [str_2_write '-' signStr];
    %str_2_write = join(strtrim(curNames),',');
    khsNames(i) = {str_2_write};
end
khsNames
[khs_cells, new_labels_vec] = correct_list_dict(list_dict);