function [ khs_clean ] = combine_Cift_khs( khs_in, disp_names)
    if ~exist('disp_names','var')
        disp_names = false;
    end
    i = 1;
    N = length(khs_in);
    ids_to_remove = [];
    while i<N
        cur_khs = khs_in{i};
        j = i+1;
        while j<N+1 && sum(ids_to_remove==j)==0 && sum(ids_to_remove==i)==0
            khs_next = khs_in{j};
            if strcmpi(khs_next, [cur_khs 'Cift'])
                ids_to_remove = [ids_to_remove j];
                khs_in{i} = [cur_khs '(Cift)'];
                if disp_names
                    disp([khs_in{i} ', i(' num2str(i) '), j(' num2str(j) ')']);
                end
                j=N+1;
                break;
            end
            j = j + 1;
        end
        if j<N+1 && sum(ids_to_remove==i)==0
            if disp_names
                disp([cur_khs '-clean']);
            end
        end
        i = i + 1;
    end
    ids_clean = 1:N;
    ids_clean(ids_to_remove) = [];
    khs_clean = khs_in(ids_clean);
end