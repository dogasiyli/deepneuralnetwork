nos = 11;
compName = getComputerName;
switch compName
    case  'doga-MSISSD'
        base_DataFolder = '/mnt/USB_HDD_1TB/';
    case  'doga-msi-ubu'
        base_DataFolder = '/home/doga/DataFolder/';
    otherwise
        exit(-78)
end

hid_size =[32 64 128 256 512 1024 2048 4096 8012];
k_fold_cnt = 5;rand_seed=-1;

data_identifier_list = {'hog','skeleton','sn','hgsk','hgsn','snsk','hgsnsk'};
confMatStats = cell(1,7);
for di = 1:7
    data_dentifier = data_identifier_list{di};
    
    pca_count = 256;
    if strcmpi(data_dentifier,'skeleton')
        pca_count = 64;
    end
    test_user = 2;

    if  k_fold_cnt==5 && rand_seed<0
        add_str = 'xv = seperate validation users';
        fnstr = 'su';
    else
        add_str = ['xv-' num2str(k_fold_cnt) ' fold, rand_seed(' num2str(rand_seed) ')'];
        fnstr = ['rs' num2str(rand_seed)];
    end

    fold_base = strrep([base_DataFolder filesep 'neuralNetHandImages_nosXX_rs224'],'XX',num2str(nos));
    fold = [fold_base filesep 'imgs'];
    savematsfold = strrep(fold, [filesep 'imgs'], [filesep 'elm_results' filesep 'mats']);
    savematname = ['ft' data_dentifier '_pca' num2str(pca_count) '_tu' num2str(test_user) '_' fnstr '.mat'];
    savematname = [savematsfold filesep savematname];

    list_dict = get_list_dict(fold, true);
    label_names_all = cellfun(@(x) strtrim(strrep(x,'Cift','')),list_dict{6}, 'UniformOutput', false);
    [label_names, ~, label_vec] = unique(label_names_all);

    load(savematname,'acc_tr', 'acc_va', 'acc_te', 'conf_tr', 'conf_va', 'conf_te');
    max_va = max(acc_va);
    max_te = max(acc_te);
    [~,idx_hidSize] = max(max_va);
    idx_k = find(acc_va(:,idx_hidSize)==max(max_va));

    best_hidSize = hid_size(idx_hidSize);
    disp(['best experiment for k_fold_cnt(' num2str(k_fold_cnt) '),teuser(' num2str(test_user) ') = hid_size=' num2str(best_hidSize) ', xv_id=' num2str(idx_k)])
    acc_best_va = acc_va(idx_k,idx_hidSize);
    acc_best_te = acc_te(idx_k,idx_hidSize);
    disp(['----acc_best_va(' num2str(acc_best_va) '),acc_best_te(' num2str(acc_best_te) ')'])
    conf_best_te = conf_te{idx_k,idx_hidSize};

    confMatStats{di} = calcConfusionStatistics(conf_best_te);
    figureTitle = [data_dentifier ',k-fold-cnt(' num2str(k_fold_cnt) '),teuser(' num2str(test_user) '),hid-size=' num2str(best_hidSize) ',xv-id(' num2str(idx_k) ')'];
    drawConfMatDetailed(conf_best_te, struct('figureID',di,'categories',{label_names},'figureTitle',figureTitle));
end
    
fprintf('%-20s|','KHSName-F1');
for di = 1:7
    fprintf('%-8s|',[data_identifier_list{di}]);
end
fprintf('\n');
for i=1:length(label_names)
    fprintf('%-20s|',pad(label_names{i},20,'right','-'));
    for di = 1:7
        cms = confMatStats{di};
        %disp_struct_string = ['%-' num2str(length(data_identifier_list{di})+1) 's|'];
        fprintf('%-8s|',num2str(cms(i).F1_Score,'%4.2f'));
    end
    fprintf('\n');
end

        