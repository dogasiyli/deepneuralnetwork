function [khs_cells, new_labels_vec] = correct_list_dict(nos)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    [list_dict, list_dict_file_name] = get_list_dict_by_comp(nos, true);
    new_labels_vec = zeros(size(list_dict{5}));
    khsNamesAll = strtrim(list_dict{6});
    [khsNames, khsInds] = unique(khsNamesAll);
    khsCnt = length(khsInds);
    khs_cells = cell(khsCnt,3);
    new_khs_id = 0;
    label_ids_conversions = zeros(length(khsNames),3);
    for i=1:khsCnt
        curName = khsNames(i);
        listRows = find(cell2mat(cellfun(@(x) strcmpi(x,curName),khsNamesAll, 'UniformOutput', false)));
        khsIDsOld = list_dict{5}(listRows);
        signIDs = reshape(unique(list_dict{1}(listRows)),1,[]);
        %if the name exists without "Cift" append it to its friend without "Cift"
        found_khs_id = -1;
        if i>1 && contains(curName,"Cift")
            curNameWOcift = strrep(curName,"Cift","");
            alreadyFoundID = find(cell2mat(cellfun(@(x) strcmpi(x,curNameWOcift),khs_cells(1:new_khs_id,1), 'UniformOutput', false)));
            if length(alreadyFoundID)==1
                found_khs_id = alreadyFoundID;
            elseif length(alreadyFoundID)>1
                error("shouldnt be here");
            end
        end
        cell2 = reshape(unique(khsIDsOld),1,[]);
        if found_khs_id==-1
            new_khs_id = new_khs_id + 1;
            khs_cells{new_khs_id,1} = khsNames{i};
            khs_cells{new_khs_id,2} = cell2;
            khs_cells{new_khs_id,3} = signIDs;
            disp(['*i(' num2str(i) '->' num2str(new_khs_id) '), khsName(' khsNames{i} '), khsIDsOld(' mat2str(khs_cells{new_khs_id,2}) ')']);
            new_labels_vec(listRows) = new_khs_id;
            label_ids_conversions(i,:) = [i cell2 new_khs_id];
        else
            khs_cells(found_khs_id,1) = strcat(khs_cells{found_khs_id,1},', ',khsNames(i));
            khs_cells(found_khs_id,2) = {[khs_cells{found_khs_id,2} cell2]};
            khs_cells(found_khs_id,3) = {[khs_cells{found_khs_id,3} signIDs]};
            disp(['**ii(' num2str(i) '->' num2str(found_khs_id) '), khsNames(' khs_cells{found_khs_id,1} '), khsIDsOld(' mat2str(khs_cells{found_khs_id,2}) ')']);
            new_labels_vec(listRows) = found_khs_id;
            label_ids_conversions(i,:) = [i cell2 found_khs_id];
        end 
    end
    khs_cells(new_khs_id+1:end,:)=[];
    [list_dict, list_dict_file_name] = get_list_dict_by_comp(nos, false);
    for i=1:khsCnt
        curName = khsNames{i};
        old_string = [' * ' num2str(label_ids_conversions(i,2)) ' * ' curName ' * '];
        new_string = [' * ' num2str(label_ids_conversions(i,3)) ' * ' curName ' * '];
        disp(strtrim(['changing <' strtrim(old_string) '> to <' strtrim(new_string) '>']))
        list_dict = cellfun(@(x) strrep(x,old_string,new_string),list_dict, 'UniformOutput', false);
    end
    list_dict_file_name_new = strrep(list_dict_file_name,'list_dict.txt','list_dict_corrected.txt');
    fid = fopen(list_dict_file_name_new,'wt');
    disp(['save ' list_dict_file_name_new]);
    list_to_save = strjoin(list_dict, newline);
    fprintf(fid,list_to_save);
    fclose(fid);
end

