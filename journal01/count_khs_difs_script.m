nos = 11;
[list_dict, list_dict_file_name] = get_list_dict_by_comp(nos, true);
%columns = sign-01 / user-02 / rep-03 / frameID-04 / khsID - 05 / khsName - 06 / LRB - 07
uniq_signIDs = unique(list_dict{1});


% khs_counts_beg_others = cell(2,3);
% khs_counts_beg_others{1,1} = 'OnlyTrain';
% khs_counts_beg_others{1,2} = 'BothTrainTest';
% khs_counts_beg_others{1,3} = 'OnlyTest';
% 
% for train_sign_cnt = 1:10
%     train_signIDs = uniq_signIDs(1:train_sign_cnt);
%     test_signIDs = uniq_signIDs(~ismember(uniq_signIDs,train_signIDs));
%     %[khs_cells, new_labels_vec] = correct_list_dict(nos);
% 
%     %I would like to get indices for train and test
% 
%     %Also I would like to get 1by3 cell array
%     %-------------------------------------------------------------------
%     %|Only in Test Set-|-Both In Train and Test Set-|-Only In Train Set|
%     %-------------------------------------------------------------------
%     khs_set_cell = get_khs_set_cell(list_dict, train_signIDs, test_signIDs);
%     
%     filename = strrep(list_dict_file_name,['imgs' filesep 'list_dict.txt'],['khs_dit_ts' num2str(train_sign_cnt) '.csv']);
%     T = cell2table(khs_set_cell(2:end,:),'VariableNames',khs_set_cell(1,:));
%     writetable(T,filename)
%     
%     khs_counts_beg_others(train_sign_cnt+1,:) = khs_set_cell(2,:);
% end
% filename = strrep(list_dict_file_name,['imgs' filesep 'list_dict.txt'],'khs_counts_beg_others.csv');
% T = cell2table(khs_counts_beg_others(2:end,:),'VariableNames',khs_counts_beg_others(1,:));
% writetable(T,filename)

khs_counts_next = cell(2,nos);
khs_counts_next{1,1} = 'OnlyTrain';
khs_counts_next{1,2} = 'BothTrainTest';
khs_counts_next{1,3} = 'OnlyTest';

for train_sign_cnt = 1:nos-1
    train_signIDs = uniq_signIDs(1:train_sign_cnt);
    test_signIDs = uniq_signIDs(train_sign_cnt+1);
    %[khs_cells, new_labels_vec] = correct_list_dict(nos);

    %I would like to get indices for train and test

    %Also I would like to get 1by3 cell array
    %-------------------------------------------------------------------
    %|Only in Test Set-|-Both In Train and Test Set-|-Only In Train Set|
    %-------------------------------------------------------------------
    khs_set_cell = get_khs_set_cell(list_dict, train_signIDs, test_signIDs);
    
    filename = strrep(list_dict_file_name,['imgs' filesep 'list_dict.txt'],['khs_next_ts' num2str(train_sign_cnt) '.csv']);
    T = cell2table(khs_set_cell(2:end,:),'VariableNames',khs_set_cell(1,:));
    writetable(T,filename)
    
    khs_counts_next(train_sign_cnt+1,:) = khs_set_cell(2,:);
end
filename = strrep(list_dict_file_name,['imgs' filesep 'list_dict.txt'],'khs_counts_next.csv');
T = cell2table(khs_counts_next(2:end,:),'VariableNames',khs_counts_next(1,:));
writetable(T,filename)