function khs_set_cell = get_khs_set_cell( list_dict, train_signIDs, test_signIDs )
    khsNamesAll = strtrim(list_dict{6});
    
    [khsNames_Train_uq, ~] = unique(khsNamesAll(ismember(list_dict{1},train_signIDs)));
    [khsNames_Test_uq, ~] = unique(khsNamesAll(ismember(list_dict{1},test_signIDs)));
    khsNames_Train_clean = combine_Cift_khs(khsNames_Train_uq);
    khsNames_Test_clean = combine_Cift_khs(khsNames_Test_uq);
    
    khs_tr_in_te = ismember(khsNames_Train_clean,khsNames_Test_clean);
    khs_only_in_tr = khsNames_Train_clean(~khs_tr_in_te);
    khs_in_both = khsNames_Train_clean(khs_tr_in_te);
    khs_te_in_tr = ismember(khsNames_Test_clean,khsNames_Train_clean);
    khs_only_in_te = khsNames_Test_clean(~khs_te_in_tr);
    
    khs_set_cell = cell(2,3);
    khs_set_cell{1,1} = 'OnlyTrain';
    khs_set_cell{1,2} = 'BothTrainTest';
    khs_set_cell{1,3} = 'OnlyTest';
    khs_set_cell{2,1} = ['s(' num2str(length(train_signIDs)) ')-khs(' num2str(length(khs_only_in_tr)) ')'];
    khs_set_cell{2,2} = ['khs(' num2str(length(khs_in_both)) ')'];
    khs_set_cell{2,3} = ['s(' num2str(length(test_signIDs)) ')-khs(' num2str(length(khs_only_in_te)) ')'];
    if ~isempty(khs_only_in_tr), khs_set_cell(3,1) = join(khs_only_in_tr,', '); else khs_set_cell{3,1} = "NONE"; end
    if ~isempty(khs_in_both), khs_set_cell(3,2) = join(khs_in_both,', '); else khs_set_cell{3,2} = "NONE"; end
    if ~isempty(khs_only_in_te), khs_set_cell(3,3) = join(khs_only_in_te,', '); else khs_set_cell{3,3} = "NONE"; end
    khs_set_cell{4,1} = length(khs_only_in_tr);
    khs_set_cell{4,2} = length(khs_in_both);
    khs_set_cell{4,3} = length(khs_only_in_te);
end

