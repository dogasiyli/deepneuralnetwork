function [list_dict, list_dict_file_name] = get_list_dict_by_comp(nos, splitBool)
    compName = getComputerName;
    switch compName
        case  'doga-MSISSD'
            base_DataFolder = '/mnt/USB_HDD_1TB/';
        case  'doga-msi-ubu'
            base_DataFolder = '/home/doga/DataFolder/';
        otherwise
            exit(-78)
    end
    fold_base = strrep([base_DataFolder filesep 'neuralNetHandImages_nosXX_rs224'],'XX',num2str(nos));
    fold = [fold_base filesep 'imgs'];
    [list_dict, list_dict_file_name] = get_list_dict(fold, splitBool);
end

