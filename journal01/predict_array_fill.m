function [ retStruct ] = predict_array_fill()
    prec2approve = 0.2:0.1:0.8;
    drawConfMat = false;
    train_sign_cnt_vec = 1:40;
    
    rng(1);
    known_rand_val = 0.417;
    check_reproducibility(known_rand_val);    
    
    rowCnt = length(train_sign_cnt_vec);
    colCnt = length(prec2approve);
    M_f1 = zeros(rowCnt,colCnt);
    M_slc = zeros(rowCnt,colCnt);
    
    colNames_2 = {'trainSignCnt', 'known', 'new', 'shared', 'f1Max', 'tresh', 'acc', 'N', 'TP', 'FP', 'FN', 'TN', 'slctSamp', 'hid_size'};
    M_row = zeros(rowCnt,length(colNames_2));
    
    ha = [64 128 256 512 1024 2048 4096 8012];
    hss = round(map2_a_b(1:41,1,length(ha)));
    try_hs = 4;
    
	for i = 1:length(train_sign_cnt_vec)
        train_sign_cnt = train_sign_cnt_vec(i);
        if train_sign_cnt<11
            nos = 11;
        else
            nos = 41;
        end
        hs_first = min(hss(i),length(ha)-try_hs);
        tmp_rows = zeros(try_hs,length(colNames_2));
        for hs_i = 1:try_hs
            hid_size = ha(hs_first+hs_i-1);
            rng(1);
            z = predict_treshold(struct('nos', nos,'train_sign_cnt', train_sign_cnt, ...
                                        'prec2approve', prec2approve, ...
                                        'hid_size', hid_size, ...
                                        'drawConfMat', drawConfMat));
            foundVals = table2array(z.ret_table);
            slctSampRecVec = foundVals(:,2);
            true_sample_cnt = foundVals(:,4);
            f1Vec = foundVals(:,6);

            M_slc(i,:) = slctSampRecVec;
            M_f1(i,:) = f1Vec;

            [maxF1, mai] = max(f1Vec);
            bestTresh = prec2approve(mai);
            slctSamp = true_sample_cnt(mai);
            accSltc = foundVals(mai,3);
            TFPN = foundVals(mai,7:10);   
            tmp_rows(hs_i,:)  = [z.trainSignCnt z.knownKHSCount z.newKHSCount z.sharedKHSCount maxF1 bestTresh, accSltc, z.N, TFPN, slctSamp, hid_size];
            if z.sharedKHSCount==0
                break;
            end
        end
        trsh_hs = tmp_rows(:,[5 end]);
        disp(trsh_hs);
        [mv, mi] = max(trsh_hs(:,1));
        M_row(i,:) = tmp_rows(mi,:);
	end
    retStruct.prec2approve = {prec2approve};
    retStruct.hid_size = {hid_size};
    retStruct.train_sign_cnt_vec = {train_sign_cnt_vec};
    
    colNames_1 = compose('prec%d',round(10*prec2approve));
    
    M_f1 = (round(10000*M_f1)/100);
    M_slc = (round(10000*M_slc)/100);
    retStruct.M_acc = array2table(M_f1, 'VariableNames', colNames_1);
    retStruct.M_slc = array2table(M_slc, 'VariableNames', colNames_1);
    retStruct.M_row = array2table(M_row, 'VariableNames', colNames_2);
    
    disp(retStruct.M_acc);
    disp(retStruct.M_slc);
    disp(retStruct.M_row);
    
    y_label_str = 'train signed sentences count';
    title_str = 'Selected Sample Count from Test Samples To Annotate Automatically';
    draw_result(M_slc, 1, train_sign_cnt_vec, prec2approve, y_label_str, title_str);

    title_str = 'F1 Scores of Selected Test Samples To Annotate Automatically';
    draw_result(M_f1, 2, train_sign_cnt_vec, prec2approve, y_label_str, title_str);
    j=1;
    while j>0
        fname = ['/home/doga/Desktop/predict_array_fill' num2str(j,'%03d') '.csv'];
        if exist(fname,'file')
            j = j + 1;
            continue;
        end
        writetable(retStruct.M_row, fname);
        j=-1;
    end
end

function draw_result(M_acc, figureID, train_sign_cnt_vec, prec2approve, y_label_str, title_str)
    c_map_vec = get_color_map('confusion_mat_01');
    drawConfMatDetailed(M_acc,struct('figureID',figureID,'draw_circles', false, 'colorMapMat', c_map_vec));
    yticklabels(compose('t%d',train_sign_cnt_vec));
    xticklabels(compose('%2.1f',prec2approve));
    xlabel('precision selected');
    ylabel(y_label_str);
    title(title_str);
end
