function retStruct = predict_treshold(OPS)
    if ~exist('OPS', 'var') || ~isstruct(OPS)
        OPS = [];
    end
    nos             = getStructField(OPS, 'nos', 11);
    train_sign_cnt  = getStructField(OPS, 'train_sign_cnt', nos-1);
    train_signIDs   = getStructField(OPS, 'train_signIDs', []);
    test_signIDs    = getStructField(OPS, 'test_signIDs', []);
    pca_count       = getStructField(OPS, 'pca_count', 256);
    hid_size        = getStructField(OPS, 'hid_size', 512);
    prec2approve    = getStructField(OPS, 'prec2approve', 0.5);
    drawConfMat     = getStructField(OPS, 'drawConfMat', true);

    rng(1);
    known_rand_val = 0.417;
    check_reproducibility(known_rand_val);    

    %columns = sign-01 / user-02 / rep-03 / frameID-04 / khsID - 05 / khsName - 06 / LRB - 07
    [list_dict, list_dict_file_name] = get_list_dict_by_comp(nos, true);
    fold_name = strrep(list_dict_file_name,[filesep 'list_dict.txt'],'');
    %hog_filename = strrep(list_dict_file_name,'list_dict.txt','hog_10_9.mat');
    [train_signIDs, test_signIDs] = get_sign_ids(list_dict, train_sign_cnt, train_signIDs, test_signIDs);
    
    %-------------------------------------------------------------------
    %|Only in Test Set-|-Both In Train and Test Set-|-Only In Train Set|
    %-------------------------------------------------------------------
    khs_set_cell = get_khs_set_cell(list_dict, train_signIDs, test_signIDs);
    T = cell2table(khs_set_cell(2:end,:),'VariableNames',khs_set_cell(1,:));
    disp(T(1,:));
    
    %load(hog_filename,'hogImArr');
    [train_rows, test_rows] = get_hog_rows(list_dict, train_signIDs, test_signIDs);
    [feats, label_vec, label_names, ~ ] = load_xv_fold_data(fold_name, 'hgsk', pca_count );
    train_hog = feats(train_rows,:);
    train_labels = label_vec(train_rows);
    train_labels_uniq = unique(train_labels);
    train_label_names = label_names(train_labels_uniq);

    test_hog = feats(test_rows,:);
    test_labels = label_vec(test_rows);
    pred_uniq = unique(test_labels);
    
    %for each 
    disp(['running for train(' mat2str(train_signIDs) '), test(' mat2str(test_signIDs) ')']);
    displayOpts = setDisplayOpts('none');
    
    tic;  
    [acc_tr, pred_tr, W_randoms_with_bias, Welm, ~, conf_tr, act] = ELM_training(train_hog', train_labels, 'sizeHid', hid_size, 'displayOpts',displayOpts);
    if drawConfMat
        figure(27);hold on;
        binrng = 0:0.05:0.95; % Create Bin Ranges
        plot_act_train(act, binrng,pred_tr, train_labels);
    end
    
    [preds_test_all, ~, conf_te, nTotal_te, nChanged_te, hidAct_te, act_te] = analyzeResults(test_hog', test_labels, W_randoms_with_bias, Welm, false);
    
    te = toc;
    disp([getTimeAsString(te) ' passed for learning']);
    
    if drawConfMat
        categories = f(test_labels, preds_test_all, label_names);
        c_map_vec = get_color_map( 'confusion_mat_01' );
        drawConfMatDetailed(conf_te, struct('figureID', {28}, 'figureTitle' , 'test', 'categories', {categories}, 'remove_blanks', true, 'row_sorting', 'max', 'col_sorting', 'max','colorMapMat',c_map_vec));
    end
    
    retStruct = struct('trainSignCnt',length(train_signIDs));
    retStruct.knownKHSCount = khs_set_cell{4,1}; 
    retStruct.sharedKHSCount = khs_set_cell{4,2}; 
    retStruct.newKHSCount = khs_set_cell{4,3}; 
    retStruct.trainAccuracy = acc_tr; 
    retStruct.ELM_hid_size = hid_size; 
    retStruct.N = length(preds_test_all); 
    
    if length(prec2approve)==1 && drawConfMat
        figure(34);clf;hold on;
        figure(35);clf;hold on;
        pred_labs = label_names(pred_uniq);
        shared_cnt = 0; ps = 0;
        different_cnt = 0; pd = 0;
        for p=1:length(pred_labs)
            is_shared = sum(pred_uniq(p)==unique(train_labels))>0;
            shared_cnt = shared_cnt + is_shared;
            different_cnt = different_cnt + ~is_shared;
        end
        for p=1:length(pred_labs)
            is_shared = sum(pred_uniq(p)==unique(train_labels))>0;
            selected_samples = test_labels==pred_uniq(p);
            sub_title_str = [pred_labs{p} '(test-' num2str(sum(selected_samples)) ')'];
            if is_shared
                figure(34);
                ps = ps + 1;
                train_samples_cnt = sum(train_labels==pred_uniq(p));
                sub_title_str = [pred_labs{p} '(test-' num2str(sum(selected_samples)) '),train(' num2str(train_samples_cnt) ')'];
                correct_samples = (selected_samples & preds_test_all==pred_uniq(p));
                in_correct_samples = (selected_samples & preds_test_all~=pred_uniq(p));
                stacked_hist([shared_cnt 1 ps], act_te.predicted_un(correct_samples), act_te.predicted_un(in_correct_samples), "wrong", "correct", sub_title_str, binrng, [], 'newline');
            else
                figure(35);
                pd = pd + 1;
                plot_hist([different_cnt 1 pd], act_te.predicted_un(selected_samples), sub_title_str, binrng);
            end
        end
    
        samples_approved = act_te.predicted_un>prec2approve;
        pred_2_disp = preds_test_all(samples_approved);
        test_labels_approved = test_labels(samples_approved);
        [~, confMatApproved, ~, ~] = calculateConfusion(pred_2_disp,test_labels_approved,false);
        categories = f(test_labels_approved, pred_2_disp, label_names);
        selectedSamplePercent = sum(confMatApproved(:))/sum(conf_te(:));
        figureTitle = ['test - prec(%' num2str(100*prec2approve,'%4.2f') ') - selectedSamplePercent(' num2str(100*selectedSamplePercent,'%4.2f') ')' ];
        drawConfMatDetailed(confMatApproved, struct('figureID', {29}, 'figureTitle' , figureTitle, 'categories', {categories}, 'remove_blanks', true, 'row_sorting', 'max', 'col_sorting', 'max','colorMapMat',c_map_vec));
    end
    
    ret_arr = NaN(length(prec2approve),10);
    uq = get_uq(train_labels_uniq, pred_uniq);
    for i=1:length(prec2approve)
        prec2app_cur = prec2approve(i);
        samples_approved = act_te.predicted_un>prec2app_cur;
        pred_2_disp = preds_test_all(samples_approved);
        test_labels_approved = test_labels(samples_approved);
        [~, confMatApproved, ~, label_map] = calculateConfusion(pred_2_disp,test_labels_approved,false);
        selectedSamplePercent = sum(confMatApproved(:))/sum(conf_te(:));
        
        %in this confusion matrix some rows and cols belong to "unknown hand shapes"
        %for unknown handshape we can not assign any correct labels
        if length(prec2approve)==1
            TCur = check_table_cur(confMatApproved, label_names, label_map, pred_uniq, train_labels_uniq);
            disp(TCur);
        end
        
        N_slctd = sum(confMatApproved(:));%samples selected to be annotated (TP+FN->known to be true)
        TP = sum(diag(confMatApproved));%samples annotated correctly
        FP = N_slctd-TP;%samples annotated incorrectly
        pred_not_annotated = preds_test_all(~samples_approved);
        %if a label in <not annotated group> is in the train set labels too, I wouldve found it
        %then missing it would be a type2 error -> FN
        %samples not annotated incorrectly, normally they would be labelled bu we didnt, we missed
        [FN, TN] = get_negative_counts(pred_not_annotated, uq, label_names);

        acc_cur = (TP+TN)/sum(conf_te(:));
        precise = TP/(TP+FP);
        recall = TP/(TP+FN);
        f1 = (2*TP)/(2*TP+FP+FN);
        ret_arr(i,:) = [prec2app_cur selectedSamplePercent acc_cur precise recall f1 TP FP FN TN];
    end
    T = array2table(ret_arr,'VariableNames',{'precTresh','slctN','acc', 'precise', 'recall', 'f1', 'TP','FP','FN', 'TN'});
    retStruct.ret_table = T; 
    displayStructureSummary('retStruct',retStruct,1);
end

function [FN, TN, table_explained] = get_negative_counts(pred_not_annotated, uq, label_names)
    FN = sum(ismember(pred_not_annotated, uq.bo));
    TN = length(pred_not_annotated)-FN;%samples "not annotated" correctly, because their labels not exist
    uq_preds_cur = unique(pred_not_annotated);
    if nargout>2
        M_te = cell(length(uq_preds_cur),3);
        for pi = 1:length(uq_preds_cur)
            pcur_id = uq_preds_cur(pi);
            pcur_name = label_names{pcur_id};
            t_f_n = ~ismember(pcur_id, uq.bo);
            if t_f_n
                t_f_n = "TrueNeg";
            else
                t_f_n = "FalseNeg";            
            end
            M_te{pi,1} = pcur_name;
            M_te{pi,2} = t_f_n;
            M_te{pi,3} = num2str(sum(pred_not_annotated==pcur_id));
        end
        table_explained = cell2table(M_te, 'VariableNames', {'KHS','TN_FN','Cnt'});
        disp(table_explained);
    end
end

function uq = get_uq(train_labels_uniq, pred_uniq)
    uq_tr = ismember(train_labels_uniq, pred_uniq);
    uq_bo = train_labels_uniq(uq_tr);
    uq_tr = train_labels_uniq(~uq_tr);
    uq_te = ismember(pred_uniq, train_labels_uniq);
    uq_te = pred_uniq(~uq_te);
    uq.tr = uq_tr;
    uq.te = uq_te;
    uq.bo = uq_bo;
end

function TCur = check_table_cur(confMatApproved, label_names, label_map, pred_uniq, train_labels_uniq)
    test_rows = ismember(label_map(:,2),pred_uniq);
    train_rows = ismember(label_map(:,2),train_labels_uniq);
    both_rows = test_rows & train_rows;
    test_rows(both_rows) = false;
    train_rows(both_rows) = false;
    rcCnt = length(train_rows);
    rowcolnames = cell(rcCnt,1);
    for rci = 1:rcCnt
        tetrb_str = '';
        if test_rows(rci)
            tetrb_str = 'te';
        elseif train_rows(rci)
            tetrb_str = 'tr';
        elseif both_rows(rci)
            tetrb_str = 'bo';
        end
        rowcolnames{rci} = [tetrb_str '_' label_names{label_map(rci,2)}];
    end
    TCur = array2table(confMatApproved, 'VariableNames', rowcolnames, 'RowNames', rowcolnames);
    writetable(TCur, '/home/doga/Desktop/TCur.csv');
end

function categories = f(givenLabels, predictedLabels, label_names)
    given_categories = reshape(unique(givenLabels),1,[]);
    predictied_categories = reshape(unique(predictedLabels),1,[]);
    all_categories = unique([given_categories predictied_categories]);
    categories = strtrim(label_names(all_categories));
end

function plot_act_train(act,binrng,pred_tr, train_labels, max_lab_cnt)
    max_lab_cnt = max(hist(train_labels));
    ylim_vec = [0 max_lab_cnt];
    plot_hist([2 2 1], act.predicted_un, "predicted-unnormed", binrng, ylim_vec, false);
    plot_hist([2 2 2], act.given_un, "given-unnormed", binrng, ylim_vec, false);
    correct_samples = pred_tr==train_labels;
    stacked_hist([2 2 3], act.predicted_un(correct_samples), act.predicted_un(~correct_samples), "wrong", "correct", "predicted-unnormed", binrng, ylim_vec, 'vertical');
    stacked_hist([2 2 4], act.given_un(correct_samples), act.given_un(~correct_samples), "wrong", "correct", "given-unnormed", binrng, ylim_vec, 'vertical');
end

function plot_hist(sub_plot_vec, d1, titlestr, binrng, ylim_vec, with_data_legent)
    subplot(sub_plot_vec(1),sub_plot_vec(2),sub_plot_vec(3));
    cla;
    cnt_vals = hist(d1,binrng);
    bar(binrng, cnt_vals, 'b');
    if ~exist('with_data_legent','var')
        with_data_legent = true;
    end    
    if with_data_legent
        data_selection = 100*(cumsum(cnt_vals)./sum(cnt_vals));
        ypos = 1.1*max(cnt_vals);
        xi = 1;%find(cnt_vals(1:end-3)==0,1,'last');
        if ~isempty(xi)
            h = text(binrng(xi),ypos, '%data');
            %set(h,'Rotation',90);
        end
        for xi = 2:length(binrng)
            if (cnt_vals(xi))>0
                str = ['%' num2str(data_selection(xi),'%2.0f')];
                h = text(binrng(xi),ypos, str,'HorizontalAlignment','left');
                %set(h,'Rotation',45);
            end
        end  
        if ~exist('ylim_vec','var')
            ylim_vec = [0 max(cnt_vals)+100];
        end        
    elseif ~exist('ylim_vec','var')
        ylim_vec = [0 max(cnt_vals)];
    end

    title(titlestr);
    xlim([min(binrng) max(binrng)]);
    xticks(binrng);
    ylim(ylim_vec);
end

function stacked_hist(sub_plot_vec, d1, d2, legend_1, legend_2, title_str, binrng, ylim_vec, txt_in_mode)
    counts1 = histc(d1, binrng); % Histogram For ‘d1’
    counts2 = histc(d2, binrng); % Histogram For ‘d2’
    counts3 = counts1 + counts2; % Histogram Sum ‘d1’+‘d2’
    subplot(sub_plot_vec(1),sub_plot_vec(2),sub_plot_vec(3));
    cla;
    hold on;
    bar(binrng, counts3, 'r');
    bar(binrng, counts1, 'b');
    legend(legend_1, legend_2,'Location', 'southwest')
    title(title_str);
    xlim([min(binrng) max(binrng)]);
    if ~exist('ylim_vec','var') || isempty(ylim_vec)
        ylim_vec = [0 max(counts3)*1.5];
    end
    ylim(ylim_vec);
    xticks(binrng);
    set(gca,'XTickLabelRotation',45);
    
    acc_per_hist = counts2./counts3;
    acc_per_hist(isnan(acc_per_hist))=0;
    acc_per_hist_accum = cumsum(counts1(end:-1:1))./cumsum(counts3(end:-1:1));
    acc_per_hist_accum(isnan(acc_per_hist_accum))=0;
    acc_per_hist_accum = 100*acc_per_hist_accum(end:-1:1);
    data_selection = 100*(cumsum(counts3(end:-1:1))./sum(counts3));
    data_selection=data_selection(end:-1:1);
    xi = 2;%find(counts3(1:end-3)==0,1,'last');
    if ~isempty(xi)
        switch txt_in_mode
            case 'newline'
                ypos = 0.75*max(ylim_vec);
                text(binrng(xi),ypos, ['%acc' newline '%data'],'HorizontalAlignment','center');
            case 'vertical'
                ypos = 1.3*max(counts3);
                h = text(binrng(xi),ypos, ['%acc' ',' '%data'],'HorizontalAlignment','center');
                set(h,'Rotation',90);
        end
    end
    for xi = 2:length(binrng)
        if (counts3(xi))>0
            switch txt_in_mode
                case 'vertical'
                    str = ['%' num2str(acc_per_hist_accum(xi),'%3.0f') ',%' num2str(data_selection(xi),'%2.0f')];
                    h = text(binrng(xi),ypos, str);
                    set(h,'Rotation',90);
                case 'newline'
                    str = ['%' num2str(acc_per_hist_accum(xi),'%3.0f') newline '%' num2str(data_selection(xi),'%2.0f')];
                    text(binrng(xi),ypos, str);
            end
        end
    end
end

function [train_signIDs, test_signIDs] = get_sign_ids(list_dict, train_sign_cnt, train_signIDs, test_signIDs)
    uniq_signIDs = unique(list_dict{1});
    if isempty(train_signIDs)
        train_signIDs = uniq_signIDs(1:train_sign_cnt);
    end
    if isempty(test_signIDs)
        test_signIDs = uniq_signIDs(train_sign_cnt+1);
    end
end

function [train_rows, test_rows] = get_hog_rows(list_dict, train_signIDs, test_signIDs)
    train_rows = find(ismember(list_dict{1},train_signIDs));
    test_rows = find(ismember(list_dict{1},test_signIDs));
end