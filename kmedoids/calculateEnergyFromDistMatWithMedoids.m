function [energy, newLabel] = calculateEnergyFromDistMatWithMedoids(D, medoidIDs)
%     distanceMatrixType = 'positiveDistances';
%     if min(D(:))<0
%         distanceMatrixType = 'negativeDistances';
%     end
    if min(D(:))<0
        valMedoids = D(sub2ind(size(D),medoidIDs,medoidIDs));
        D(sub2ind(size(D),medoidIDs,medoidIDs))=-inf;
        [val, newLabel] = min(D(medoidIDs,:),[],1);                % assign labels       
        val(medoidIDs) = valMedoids;
    else
        [val, newLabel] = min(D(medoidIDs,:),[],1);                % assign labels       
    end
    energy = sum(val);
end