function [ medoidHist_Combined, clusterLabelsCells_Combined, kVec_combined] = combine_kMedoidResults( medHist_Cur, clusLabCellCur, medHist_Prv, clusLabCellPrv)

    kVec_cur = reshape(find(medHist_Cur(:,1)~=inf),1,[]);
    kVec_old = reshape(find(medHist_Prv(:,1)~=inf),1,[]);
    kVec_combined = sort(unique([kVec_cur kVec_old]));
    
    acc_cur = zeros(length(kVec_combined),1);
    acc_old = zeros(length(kVec_combined),1);
    acc_cur(ismember(kVec_combined,kVec_cur)) = reshape(100*(1-medHist_Cur(kVec_cur,end)),[],1);
    acc_old(ismember(kVec_combined,kVec_old)) = reshape(100*(1-medHist_Prv(kVec_old,end)),[],1);
    accNew = max(acc_cur,acc_old);
    
    combTableToDisp = [kVec_combined' acc_cur acc_old accNew];
    disptable(combTableToDisp,'clusterCount|accCur|accOld|accNew');
    
    kMax_cur = max(kVec_cur);
    kMax_old = max(kVec_old);
    kMax = max(kVec_combined);
    clusterLabelsCells_Combined = cell(1,kMax);
    medoidHist_Combined = inf(kMax,kMax+1);    
    
    for i=1:length(kVec_combined)
        curK = kVec_combined(i);
        if sum(kVec_old==curK)>0
            prevAcc = 1-medHist_Prv(curK,end);
        else
            prevAcc = 0;
        end
        if sum(kVec_cur==curK)>0
            curAcc = 1-medHist_Cur(curK,end);
        else
            curAcc = 0;
        end

        medoidHist_Combined(curK, :) = 0;
        if curAcc<prevAcc %use previous
            medoidHist_Combined(curK, kMax-curK+1 : end) = medHist_Prv(curK,kMax_old-curK+1 : end);
            clusterLabelsCells_Combined(curK) = clusLabCellPrv(curK);
        else%use current
            medoidHist_Combined(curK, kMax-curK+1 : end) = medHist_Cur(curK,kMax_cur-curK+1 : end);
            clusterLabelsCells_Combined(curK) = clusLabCellCur(curK);
        end
    end  
end

