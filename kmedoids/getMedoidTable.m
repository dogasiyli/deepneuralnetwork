function [medoidTable, combinedPrecision, clusterLabels, clusterLabels_Mapped, acc] = getMedoidTable(distMat, medoidIDs, gtLabels, OPS)
%getMedoidTable 
% medoidIDs           - indices of medoids from a dataset
% gtLabels            - groudTruth labels of the dataset
% clusterLabels       - cluster labels of the samples - the samples that fall near medoids by their IDs

    if ~exist('OPS','var')
        OPS=[];
    end
    sortRowsVec  = getStructField(OPS, 'sortRowsVec', [4 -5]);
    displayTable = getStructField(OPS, 'displayTable', false);

    [~, clusterLabels] = calculateEnergyFromDistMatWithMedoids(distMat, medoidIDs);
    [~, uniqueMappingLabels, acc, outStruct] = groupClusterLabelsIntoGroundTruth(clusterLabels, gtLabels, struct('printResults', false,'figID', -1));
    
    clusterLabels_Mapped = outStruct.clusLabels_Mapped;
    origClassCount = length(unique(gtLabels));
    mapKlusterCnt  = length(unique(clusterLabels_Mapped));
    kSpecificity = mapKlusterCnt/origClassCount;
    combinedPrecision = acc*kSpecificity;
    
% uniqueMappingLabels - gtLabels to clusterLabels_Mapped mapping
    %which medoid has from which class?
    medoidCount = length(medoidIDs);
    medoidTable = zeros(medoidCount,6);%medoidID, mappedLabelID, totalAssignedSampleCount, trueAssignmentCount, falseAssignmentCount, false/all
    for i = 1:medoidCount
        mID = medoidIDs(i);
        samplesOfCurrentMedoid = find(clusterLabels==i);
        mappedClusterLabelsOfThisMedoidSamples = clusterLabels_Mapped(samplesOfCurrentMedoid);
        mappedLabelID = unique(mappedClusterLabelsOfThisMedoidSamples);
        grountTruthOfThisMedoidSamples = gtLabels(samplesOfCurrentMedoid);
        grountTruthOfThisMedoidSamples = reSetLabels(grountTruthOfThisMedoidSamples, uniqueMappingLabels(:,1), uniqueMappingLabels(:,3));
        
        medoidTable(i,1) = mID;
        medoidTable(i,2) = mappedLabelID;
        medoidTable(i,3) = length(mappedClusterLabelsOfThisMedoidSamples);
        medoidTable(i,4) = sum(grountTruthOfThisMedoidSamples==mappedLabelID);
        medoidTable(i,5) = medoidTable(i,3)-medoidTable(i,4);
        medoidTable(i,6) = medoidTable(i,5)/medoidTable(i,4);
        medoidTable(i,7) = i;
    end
    
    medoidTable = sortrows(medoidTable, sortRowsVec);
    if displayTable
        disptable(medoidTable,'mID|mapLb|cnt|true|false|err|idx',[],'%3.4f');
    end
end

