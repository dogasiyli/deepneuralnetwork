function [medoidIDs, clusterLabels_corrected, uniqClustIDs] = getMedoidsFromLabels(D, clusterLabels)
    n = length(clusterLabels);
    clusterLabels_corrected = zeros(1,n);
    [uniqClustIDs,~,clusterLabels_corrected(:)] = unique(clusterLabels);   % remove empty clusters
    medoidIDs = inf(1,length(uniqClustIDs));
    for k=uniqClustIDs
        sampleIDsOf_k = find(clusterLabels==k);
        distMat_selected = D(sampleIDsOf_k,sampleIDsOf_k);
        d_sum = sum(distMat_selected);
        [minSum, minIdx] = min(d_sum);
        medoidIDs(k) = sampleIDsOf_k(minIdx);
    end
    %[~, medoidIDs2] = min(D*sparse(1:n,clusterLabels,1),[],1);  % find k medoids
    medoidIDs = unique(medoidIDs);
end