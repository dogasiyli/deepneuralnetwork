%SOURCE = https://www.mathworks.com/matlabcentral/fileexchange/28898-kmedoids
function [cL_best, cM_hist, sIV_best, medoidCouples] = kmedoidsDG(X, optionalParamsStruct)
    % [cL_final, medoidsHistory, sampleIndsVec] = kmedoids(X, struct('iterMax',50,'initialMedoids',[1 21 80],'clusterCount',5));
    % cL - clusterLabels {_final, _init, _cur, _new}
    % cC - clusterCount
    % cM - clusterMedoids {_init, _new, _hist, _try}
    % fO - figureOpts.{'rc=rowCount','draw=boolean'}
    % e  - energy {_init, _old}
    
    % Perform k-medoids clustering.
    % Input:
    %   X: d x n data matrix or a DxD distance matrix
    %   init: k number of clusters or label (1 x n vector)
    % Output:
    %   label: 1 x n cluster label
    %   index: index of medoids
    %   energy: optimization target value
    % Written by Mo Chen (sth4nth@gmail.com).
    % Updated by Recep Doğa SİYLİ (dogasiyli@gmail.com)

    if ~exist('optionalParamsStruct','var') || isempty(optionalParamsStruct)
       optionalParamsStruct = [];
    end
    iterMax = getOptionalParamsFromStruct(optionalParamsStruct, 'iterMax', 1000, true);
    figID = getOptionalParamsFromStruct(optionalParamsStruct, 'figID', -1, true);
    cC = getOptionalParamsFromStruct(optionalParamsStruct, 'clusterCount', 2, false);
    cL_init = getOptionalParamsFromStruct(optionalParamsStruct, 'initialLabels', [], true);
    cM_init = getOptionalParamsFromStruct(optionalParamsStruct, 'initialMedoids', [], true);
    gtLabels = getOptionalParamsFromStruct(optionalParamsStruct, 'gtLabels', [], true);
    printLevel = getOptionalParamsFromStruct(optionalParamsStruct, 'printLevel', -1, true);
        

    [D, cC, cL_init, cM_init, e_init, fO, n] = arrangeInitialization(X, cM_init, cL_init, cC, figID, printLevel);
  
    iterID = 0;
    cM_old = rearrangeMedoidIDs(cM_init, cC, unique(cL_init));

    if isempty(gtLabels)
        cM_hist = appendMedoidsHistory(cM_init, cC, e_init, unique(cL_init));
    end

    cL_prev = cL_init;
    cL_cur = zeros(1,n);%make empty for looping at least once
     
    cM_new = cM_init;
    e_old = e_init;
    
    while any(cL_prev ~= cL_cur) && iterID<iterMax && ~isempty(gtLabels)
        %now using D and cl_prev I have a confusion matrix and accuracy
        %I want to increase my accuracy
        %hence I will remove the sample that creates the most in-accuracy
        %add a new sample from the highest error of confusion matrix
        
        %which medoid has from which class?
        
        [cM_new, cL_cur, cL_prev, acc_Prv, acc_New] = getBestClassifierCentroids(D, cM_new, gtLabels);
        if iterID==0
            e_old = 1-acc_Prv;
            cM_hist = appendMedoidsHistory(cM_init, cC, 1-acc_Prv, unique(cL_init));
        end
        cM_hist = appendMedoidsHistory(cM_new, cC, 1-acc_New, unique(cL_cur), cM_hist);
        iterID = iterID + 1;
        cL_best = cL_cur;
        [sIV_best, ~, medoidCouples]  = plotDistanceMatrixAsData(D, 2, cL_best, []);
    end

    while any(cL_prev ~= cL_cur) && iterID<iterMax && isempty(gtLabels)
        [cM_new, cL_cur, uniqClustIDs] = getMedoidsFromLabels(D, cL_prev);
        [e_new, cL_new] = calculateEnergyFromDistMatWithMedoids(D, cM_new);
        
        cM_hist = appendMedoidsHistory(cM_new, cC, e_new, unique(cL_new), cM_hist);

        histOld = hist(cL_prev, uniqClustIDs);
        histNew = hist(cL_new, unique(cL_new));
        energyPredict = -inf;
        
        if sum(abs(cL_prev - cL_new))==0
            [sIV_prev, cM_try] = plotDistanceMatrixAsData(D, 2, cL_prev, fO.figID+1, 1:n);
            if iterID==0
                title(['Initial medoids are best medoids(' mat2str(cM_init) '),possibleMedoids(' mat2str(cM_try) '),hist(' mat2str(histOld) '),energy(' num2str(e_new) ')-distMat']);
            end
            %first try 
            [energyPredict, labelsPredict] = calculateEnergyFromDistMatWithMedoids(D, cM_try);
            if (energyPredict<e_new)
                cM_try = sort(cM_try);
                if printLevel>2
                    disp(['tried (' mat2str(cM_try) ') instead of (' mat2str(cM_new) ') and got better distance sum (' mat2str(e_new,8) ' to better ' mat2str(energyPredict,8) ')']);
                end
                cL_new = labelsPredict;
                cM_old = cM_new;
                cM_new = cM_try;
                %energyOld = energy;
                histNew = hist(cL_new, unique(cL_new));
            else
                if printLevel>2
                    disp(['tried (' mat2str(cM_try) ') instead of (' mat2str(cM_new) ') but got worse distance sum (' mat2str(e_new,8) ' to worse ' mat2str(energyPredict,8) ')']);
                end
                if fO.draw
                    figure(fO.figID);
                    subplot(fO.rc,1,fO.rc);plot(1:size(cM_hist,1), cM_hist(end:-1:1,end)', '-k');title(['energyNow(' num2str(cM_hist(1,end)) '),energyInit(' num2str(cM_hist(end,end)) ')']);
                    drawnow;
                end
                [cL_best, sIV_best, medoidCouples] = getBest_clusterLabels(cL_prev, cL_new, e_new, e_old, D, printLevel);
                [cM_best, cL_best2, uniqClustIDs] = getMedoidsFromLabels(D, cL_best);
                if printLevel>1
                    printClusterSummary( cL_best );
                    disptable(cM_hist);
                end
                return
            end
        end
        
        iterID = iterID + 1;
        if fO.draw
            plotDistanceMatrixAsData(D, 2, cL_prev, [fO.figID fO.rc 1 1]);title(['Old labels-medoids(' mat2str(cM_old) '),hist(' mat2str(histOld) '),energy(' num2str(e_old) ')-distMat']);   
            [sIV_new, cM_try] = plotDistanceMatrixAsData(D, 2, cL_new, [fO.figID fO.rc 1 2]);
            cM_try = sort(cM_try);
            if (energyPredict~=-inf)
                title(['New labels-medoids(' mat2str(cM_new) '),possibleMedoids(' mat2str(cM_try) '),hist(' mat2str(histNew) '),energy(' num2str(energyPredict) ')-distMat']);   
            else
                title(['New labels-medoids(' mat2str(cM_new) '),possibleMedoids(' mat2str(cM_try) '),hist(' mat2str(histNew) '),energy(' num2str(e_new) ')-distMat']);   
            end
            if size(X,1)<=3
                plotClass(X,cL_new,[fO.figID fO.rc 1 3]);
                title(['Iteration(' num2str(iterID) '), clusterCount(' num2str(length(uniqClustIDs)) ' of ' num2str(cC) ')']);
            end            
            subplot(fO.rc,1,fO.rc);plot(1:size(cM_hist,1), cM_hist(end:-1:1,end)', '-k');title(['energyNow(' num2str(cM_hist(1,end)) '),energyInit(' num2str(cM_hist(end,end)) ')']);
            drawnow;
        end
        if printLevel>2
            disp(['proceeding medoids (' mat2str(cM_old) ') to (' mat2str(cM_new) ') and got energy (' num2str(e_old) ' to ' num2str(e_new) ')']);        
        end
        cL_prev = cL_new;
        cM_old = cM_new;
        e_old = e_new;
        [cL_best, sIV_best, medoidCouples] = getBest_clusterLabels(cL_prev, cL_new, e_new, e_old, D, printLevel);
    end
    
    if printLevel>1
        printClusterSummary( cL_best );
        disptable(cM_hist);
    end
end

function [cL_best, sIV_best, medoidCouples] = getBest_clusterLabels(cL_prev, cL_new, e_new, e_old, D, printLevel)
    if e_new<=e_old
        cL_best = cL_new;
    else
        cL_best = cL_prev;
    end
    [sIV_best, initIndicesOfBlocks, medoidCouples]  = plotDistanceMatrixAsData(D, 2, cL_best, []); %#ok<ASGLU>
    if printLevel>2
        disptable(medoidCouples, 'medoid01|medoid02|distance');
    end
end

function [D, clusterCount, initialClusterLabels, initialMedoids, initialEnergy, figureOpts, n] = arrangeInitialization(X, initialMedoids, initialClusterLabels, clusterCount, figID, printLevel)
    n = size(X,2);
    [D, dWas] = makeDistMatFull(X);
    if printLevel>3
        disp(['Input X was - ' dWas]);
    end

    % If we know the initial medoids we will use them
    % else if we know the initial labels we will use them
    % else if we know the cluster count we will act accordingly

    if ~isempty(initialMedoids)
        [initialEnergy, initialClusterLabels] = calculateEnergyFromDistMatWithMedoids(D, initialMedoids);
        clusterCount = length(initialMedoids);%length(unique(initialClusterLabels));
    elseif ~isempty(initialClusterLabels)
        assert(numel(initialClusterLabels)==n,['labels must be of length ' num2str(n)])
        clusterCount = length(unique(initialClusterLabels));
        [initialMedoids] = getMedoidsFromLabels(D, initialClusterLabels);
        initialEnergy = calculateEnergyFromDistMatWithMedoids(D, initialMedoids);        
    else
        assert(numel(clusterCount)==1,'clusterCount must be of length 1')
        initialMedoids = randperm(n,clusterCount);
        [initialEnergy, initialClusterLabels] = calculateEnergyFromDistMatWithMedoids(D, initialMedoids);
    end
    
    figureOpts = struct;
    figureOpts.figID = figID;
    figureOpts.draw = figID>0;
    
    if figureOpts.draw
        figure(figID);clf;
    end
    
    if size(X,1)<=3
        figureOpts.rc = 4;
    else
        figureOpts.rc = 3;
    end    
end

function medoidsHistory = appendMedoidsHistory(medoidIDs, clusterCount, energyOfMedoids, uniqClustIDs, medoidsHistory)
    if nargin==4
        medoidsHistory = [rearrangeMedoidIDs(medoidIDs, clusterCount, uniqClustIDs) energyOfMedoids];
    else
        medoidsHistory = [[rearrangeMedoidIDs(medoidIDs, clusterCount, uniqClustIDs) energyOfMedoids];medoidsHistory];
    end
end

function curMedoids = rearrangeMedoidIDs(medoidIDs, cC, uniqClustIDs)
    if length(medoidIDs)<cC
        curMedoids = -ones(1,cC);
        curMedoids(uniqClustIDs) = medoidIDs;
    else
        curMedoids = medoidIDs;
    end
end

function [D, dWas] = getDistanceMatrixFrom_d_n_matrix(X)
    D = pdist2_fast(X',X');
%     X = bsxfun(@minus, X, mean(X,2));             % reduce chance of numerical problems
%     v = dot(X,X,1);
%     D = v+v'-2*(X'*X);            % Euclidean distance matrix
    [D, dWas] = fixDistanceMatrix(D);
end




