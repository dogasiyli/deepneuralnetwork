function [medoidHist_Best, clusterLabelsCell, sampleIndsVecCell, medoidCouplesCell] = run_kmedoidsDG_Loop(distMat, clusteringParams)
    tryMax = clusteringParams.tryMax;
    if isfield(clusteringParams,'kMax')
        kMax = clusteringParams.kMax;
        kVec = 1:clusteringParams.kMax;
    elseif isfield(clusteringParams,'kVec')
        kVec = clusteringParams.kVec;
        kMax = max(kVec);
    else
        error('wtf');
    end
    
    gtLabels = getOptionalParamsFromStruct(clusteringParams, 'gtLabels', [], true);
    useDispStat = isempty(gtLabels);
    
    if useDispStat
        dispstat('','init');
    end
    dispstat_t_k = [-1 -1];

    [distMat, dWas] = makeDistMatFull(distMat);
    N = size(distMat,1);%there are N number of images to be compared
    %medoidHist_K_cell = zeros(kMax,kMax+1,tryMax);
    %knownClusterCentersDist = inf(kMax, N);
    clusterLabelsCell = cell(1,kMax);
    medoidCouplesCell = cell(1,kMax);
    medoidHist_Best = inf(kMax,kMax+1);
    sampleIndsVecCell = cell(1,kMax);
    medoidHist_improve = [];
    %wouldveQuitted = zeros(1,tryMax);
    medoidHistCell = cell(tryMax,kMax);
    for t=1:tryMax
        medoidHist_K = zeros(kMax,kMax+1);
        for k = kVec
            if t>1
                %ifMode = -1;
                %[accDistVals, initialMedoids] = sort(knownClusterCentersDist(k,:));
                %initialMedoids(accDistVals==inf) = [];
                initialMedoids = [];
                if ~isempty(gtLabels)
                    initialMedoids = createInitLabelsForKnownGT(distMat,  k, gtLabels, t);
                elseif length(initialMedoids)==k
                	initialMedoids = randperm(N);
                	initialMedoids = initialMedoids(1:k);
                %    ifMode = 1;
                %elseif length(initialMedoids)>=k
                %	initialMedoids = initialMedoids(1:k);
                %    ifMode = 2;
                else
                    initialMedoidsAdd = 1:N;
                    initialMedoidsAdd(initialMedoids) = [];
                    initialMedoidsAdd = initialMedoidsAdd(randperm(length(initialMedoidsAdd)));
                    initialMedoids = sort([initialMedoids initialMedoidsAdd(1 : k-length(initialMedoids))]);
                %    ifMode = 3;
                end
            elseif ~isempty(gtLabels)
                initialMedoids = createInitLabelsForKnownGT(distMat,  k, gtLabels, t);
            else
                initialMedoids = [];
            end
            %if ~isempty(initialMedoids) && length(initialMedoids)~=k
            %    initialMedoidsUse = initialMedoids;
            %else
                initialMedoidsUse = initialMedoids;
            %end
            tryCnt = 0;
            while tryCnt>=0 && tryCnt<10
                try
                    [clusterLabels_cur, medoidHistCell{t,k}, sampleIndsVec_cur, medoidCouples_cur] = kmedoidsDG(distMat, struct('gtLabels',gtLabels,'iterMax',50, 'initialMedoids', initialMedoidsUse,'clusterCount', k, 'figID', -1));
                    tryCnt = -1;
                catch
                    disp(['Trying(' num2str(tryCnt) '...)']);
                    tryCnt = tryCnt+1;
                    if ~isempty(gtLabels)
                        initialMedoidsUse = createInitLabelsForKnownGT(distMat,  k, gtLabels, t);
                    end
                end
            end
            if tryCnt>=10
                warning('reached maximum trial count of 10 at run_kmedoidsDG_Loop line 66');
                continue
            end
            bestMedoidRow = sortrows(medoidHistCell{t,k}, size(medoidHistCell{t,k},2));
            bestMedoidRow = bestMedoidRow(1,:);

            curMedoids = bestMedoidRow(1:end-1);
            curEnergy = bestMedoidRow(end);
            medoidHist_K(k, kMax-length(curMedoids)+1 : kMax+1) = [sort(curMedoids) curEnergy];
            try
                [~, curClusLabels] = calculateEnergyFromDistMatWithMedoids(distMat, curMedoids);
            catch
                disp('*-*-*-*');
                continue
            end
            
            %curMedoids(curMedoids<1) = [];
            %knownClusterCentersDist(k,curMedoids) = min([knownClusterCentersDist(k,curMedoids) ; ones(size(curMedoids))*curEnergy]);
            
            if medoidHist_Best(k,end)>medoidHist_K(k,end)
                medoidHist_Best(k,:) = medoidHist_K(k,:);
                clusterLabelsCell{1,k} = clusterLabels_cur;
                sampleIndsVecCell{1,k} = sampleIndsVec_cur;
                medoidCouplesCell{1,k} = medoidCouples_cur;
                medoidHist_improve = [medoidHist_improve;t k medoidHist_Best(k,:)]; %#ok<AGROW>
                %if (dispstat_k~=k)
                %    dispstat(sprintf('%d %d %s \n', t, k, mat2str(medoidHist_Best(k,:),5)),'keepthis');
                if medoidHist_Best(k,end)==min(medoidHist_Best(:,end))
                    dispstat_t_k = [t k];
                    fwf_displayImprovement2(medoidHist_Best, t, tryMax, dispstat_t_k,useDispStat);
                end
                %
                %disptable([t k medoidHist_Best(k,:)]);
            end
            if mod(t,10)==0
                fwf_displayImprovement2(medoidHist_Best, t, tryMax, dispstat_t_k,useDispStat);
            end
        end
    end
    if useDispStat
        dispstat('','keepprev');
    end
    %medoidHist_improve = sortrows(medoidHist_improve, [2,-size(medoidHist_improve,2)]);
    %disptable(medoidHist_improve);
    %disptable(medoidHist_Best(kVec,:));
    tableToDisplay = [reshape(kVec,[],1) reshape(100*(1-medoidHist_Best(kVec,end)),[],1)];
    disptable(tableToDisplay,'clusterCount|accuracy');
    %r_t =  getAllPossibleDivisors(length(wouldveQuitted));
    %wouldveQuitted = reshape(wouldveQuitted,r_t(end-1),[]);
    %disp(mat2str(find(wouldveQuitted==1)))
end

function cM_init = createInitLabelsForKnownGT(X, cC, gtLabels, t)
    uniqueLabels = reshape(unique(gtLabels),1,[]);
    sampleCountPerGT = hist(gtLabels,uniqueLabels);
    kCountPerGT = max(round(cC*(sampleCountPerGT./sum(sampleCountPerGT))),1);
    dif_kCount = sum(kCountPerGT)-cC;
    while(dif_kCount<0)
        [minVal, minIDx] = min(kCountPerGT);
        kCountPerGT(minIDx) = minVal+1;
        dif_kCount = sum(kCountPerGT)-cC;
    end
    while(dif_kCount>0)
        [maxVal, maxIDx] = max(kCountPerGT);
        kCountPerGT(maxIDx) = maxVal-1;
        dif_kCount = sum(kCountPerGT)-cC;
    end
    clear('maxVal', 'maxIDx','maxVal', 'maxIDx','maxVal', 'maxIDx','dif_kCount');

    labelCount = length(uniqueLabels);
    medSet_i = 0;
    unUsedMedoidIDs = 1:length(gtLabels);
    cM_init = [];
    for cli = 1:labelCount
        cl = uniqueLabels(cli);
        cl_sample_ids = find(gtLabels==cl);
        if t==1
            if kCountPerGT(cli)==1
                %I want to find the saample that is most distant to all
                %other class samples
                partOfDistMat = X(gtLabels~=cl, cl_sample_ids);
                distSums = mean(partOfDistMat);%sums the cols and give 1 row
                [~, furthestColIdx] = max(distSums);
                medSet_i = medSet_i + 1;
                cM_init(medSet_i) = cl_sample_ids(furthestColIdx);
                unUsedMedoidIDs = removeFromVec(unUsedMedoidIDs, cM_init(medSet_i));
            else
                partOfDistMat = X(cl_sample_ids,cl_sample_ids);
                %find the two closest element in partial matrix of the same
                %class
                if any(partOfDistMat<0)
                    %this line is good if distances can be negative
                    partOfDistMat(partOfDistMat==0) = -2*max(abs(partOfDistMat));
                    [row_i, col_j] = minElementMat(-partOfDistMat);
                else
                    partOfDistMat(partOfDistMat==0) = 2*max(partOfDistMat);
                    [row_i, col_j] = minElementMat(partOfDistMat);
                end
                medSet_i = medSet_i + 1;
                cM_init(medSet_i) = cl_sample_ids(row_i);
                unUsedMedoidIDs = removeFromVec(unUsedMedoidIDs, cM_init(medSet_i));
                medSet_i = medSet_i + 1;
                cM_init(medSet_i) = cl_sample_ids(col_j); 
                unUsedMedoidIDs = removeFromVec(unUsedMedoidIDs, cM_init(medSet_i));
            end            
        else
            fr = length(cM_init)+1;
            to = fr+kCountPerGT(cli)-1;
            keepSearch = true;
            while keepSearch
                addMedoidPerm = randperm(length(cl_sample_ids));
                medoidIDsToUse = cl_sample_ids(addMedoidPerm(1:kCountPerGT(cli)));
                try
                    cM_init(fr:to) = medoidIDsToUse;
                    unUsedMedoidIDs = removeFromVec(unUsedMedoidIDs, cM_init(fr:to));
                    keepSearch = false;
                catch
                    keepSearch = true;
                end
            end
        end
    end
    remainCount = cC - medSet_i;
    if remainCount>0
        addMedoidPerm = randperm(length(unUsedMedoidIDs));
        cM_init(medSet_i+1:cC) = unUsedMedoidIDs(addMedoidPerm(1:remainCount));
    end
    cM_init = sort(cM_init);
end

function fwf_displayImprovement2(medoidHist_Best,t, tMax, dispstat_t_k,useDispStat)
    if useDispStat
        %try 170 of 3000, k(8) - 1770671.73
        dispstat(sprintf('try %d of %d, k(%d) - %s \n', t, tMax, dispstat_t_k(2), mat2str(medoidHist_Best(dispstat_t_k(2),medoidHist_Best(dispstat_t_k(2),:)~=0),5)));
    else
        disp(['try ' num2str(t) ' of ' num2str(tMax) ', k(' num2str(dispstat_t_k(2)) ') - ' num2str(100*(1-medoidHist_Best(dispstat_t_k(2),end)),'%4.2f')]);
    end
end

% function displayImprovement(medoidHist_Best,t, tMax, dispstat_t_k)
%     dispstat(sprintf('%d of %d - %d %d %s \n', t, tMax, dispstat_t_k(1), dispstat_t_k(2), mat2str(medoidHist_Best(dispstat_t_k(2),medoidHist_Best(dispstat_t_k(2),:)~=0),5)));
% end