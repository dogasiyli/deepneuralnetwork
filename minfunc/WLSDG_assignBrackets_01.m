function [ paramIT, bracketResult, retVal ] = WLSDG_assignBrackets_01( paramIT, paramFX, objFuncParamsStruct )
    retVal = 0;
    %retVal = 1 --> resultIllegal=true
    %retVal = 2 --> esc_case_01=true
    %retVal = 3 --> esc_case_02=true
    %retVal = 4 --> esc_case_03=true
    
    suf_dec = paramFX.suf_dec;
    curv_param = paramFX.curv_param;
    
    cost_init = paramIT.steps.zero.cost;
    gtd_init = paramIT.steps.zero.gtd;
    
    cost_new = paramIT.steps.curr.cost;
    g_new = paramIT.steps.curr.g;
    gtd_new = paramIT.steps.curr.gtd;
    t_cur = paramIT.steps.curr.t;
    
    cost_prev = paramIT.steps.prev.cost;
    
    bracketResult = struct;
    
    %% Bracketing Phase
    if ~isLegal(cost_new) || (objFuncParamsStruct.calculateGradient && ~isLegal(g_new))
        retVal = 1;%resultIllegal = true;
        %deleted code 1 - at the end of this file
        paramIT = illegalResultFinalArmijoBacktrack(paramIT, paramFX);
        return;
    end

    esc_case_01 = cost_new > cost_init + suf_dec*t_cur*gtd_init || (paramIT.iter_cur > 1 && cost_new >= cost_prev);        
    if objFuncParamsStruct.calculateGradient
        esc_case_02 = abs(gtd_new) <= -curv_param*gtd_init;
        esc_case_03 = gtd_new >= 0;
    end

    if esc_case_01
        %1: if cost_new is bigger than initial cost + some value OR
        %2: this is not the first iteration and cost_new is bigger than or equal to previous cost
        %then the bracket is between previous t and new t
        retVal = 2;%break;
    elseif objFuncParamsStruct.calculateGradient && esc_case_02
        %if absolute of new directional derivative is less then or equal to -(curvature parameter)*(directional derivative at starting location)
        %then condition is satisfied
        paramIT.wolfeConditionSatisfied = 1;
        retVal = 3;%break;
    elseif objFuncParamsStruct.calculateGradient && esc_case_03
        %if new directional derivative is bigger than or equal to 0
        %then the bracket is between previous t and new t
        retVal = 4;%break;
    end
    
    if objFuncParamsStruct.calculateGradient && esc_case_02
        %if absolute of new directional derivative is less then or equal to -(curvature parameter)*(directional derivative at starting location)
        %then condition is satisfied
        bracketResult.bracket = t_cur;
        bracketResult.bracketCost = cost_new;
        bracketResult.bracketGrad = g_new;
    else
        %pick the best 2 outputs from
        %paramIT.steps.{zero,curr,prev}
        %fill the following with those
        [bracket, bracketCost, bracketGrad] = WLSDG_computeBracketsFromSteps(paramIT);
        bracketResult.bracket = bracket;
        bracketResult.bracketCost = bracketCost;
        bracketResult.bracketGrad = bracketGrad;
    end
end

function paramIT = illegalResultFinalArmijoBacktrack(paramIT, paramFX)
    global minFuncStepInfo;
    t_prev = paramIT.steps.prev.t;
    t_cur = paramIT.steps.curr.t;
    funEvals = paramIT.funEvals;
    
    tolX = paramFX.tolX;
    objFuncParamsStruct = paramFX.objFuncParamsStruct;
    disp_debug = paramFX.disp_debug;
    desc_dir = paramFX.desc_dir;
    cost_init = paramFX.cost_init;
    grad_init = paramFX.grad_init;
    grad_dir_der = paramFX.grad_dir_der;
    suf_dec = paramFX.suf_dec;
    interpType = paramFX.interpType;
    doPlot = paramFX.doPlot;
    funObj = paramFX.funObj;
    saveHessianComp = paramFX.saveHessianComp;
    computeHessian = paramFX.computeHessian;
    
    x_init = paramFX.x_init;    

    if disp_debug
        fprintf('Extrapolated into illegal region, switching to Armijo line-search\n');
    end
    t_cur = (t_cur + t_prev)/2;
    % Do Armijo
    if computeHessian
        [t_cur,x_new,cost_new,grad_new,armijoFunEvals,paramIT.H] = ArmijoBacktrack(...
          x_init,t_cur,desc_dir,cost_init,cost_init,grad_init,grad_dir_der,suf_dec,max(0,min(interpType-2,2)),tolX,disp_debug,doPlot,saveHessianComp,...
          funObj,objFuncParamsStruct);
    else
        if ~isempty(minFuncStepInfo) && isstruct(minFuncStepInfo) && isfield(minFuncStepInfo,'StepType')
            minFuncStepInfo.StepType = 'ArmijoBacktrack';%'WolfLineBracket';%'WolfLineInit';%'MainIteration';%'WolfLineStep';%
            minFuncStepInfo.StepLength = t_cur;
        end
        [t_cur,x_new,cost_new,grad_new,armijoFunEvals] = ArmijoBacktrack(...
          x_init,t_cur,desc_dir,cost_init,cost_init,grad_init,grad_dir_der,suf_dec,max(0,min(interpType-2,2)),tolX,disp_debug,doPlot,saveHessianComp,...
          funObj,objFuncParamsStruct);
    end
    funEvals = funEvals + armijoFunEvals;

    paramIT.steps.curr.t = t_cur;
    paramIT.steps.curr.theta = x_new;
    paramIT.steps.curr.cost = cost_new;
    paramIT.steps.curr.g = grad_new;
    paramIT.funEvals = funEvals;
end
