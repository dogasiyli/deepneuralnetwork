function t_new = WLSDG_assignBrackets_02( paramIT, paramFX)
%Only updates paramIT.steps.curr.t by applying interpolation

    interpType = paramFX.interpType;
    disp_debug = paramFX.disp_debug;
    doPlot = paramFX.doPlot;
    objFuncParamsStruct = paramFX.objFuncParamsStruct;

    cost_prev = paramIT.steps.prev.cost;
    t_prev = paramIT.steps.prev.t;
    gtd_prev = paramIT.steps.prev.gtd;
    
    cost_curr = paramIT.steps.curr.cost;
    t_curr = paramIT.steps.curr.t;
    gtd_curr = paramIT.steps.curr.gtd;

    minStep = t_curr + 0.01*(t_curr-t_prev);
    maxStep = t_curr*10;
    if interpType == 3
        if disp_debug
            fprintf('Extending Braket\n');
        end
        t_new = maxStep;
    elseif interpType ==4
        if disp_debug
            fprintf('Cubic Extrapolation\n');
        end
        if objFuncParamsStruct.calculateGradient
            t_new = polyinterp( [t_prev  cost_prev gtd_prev; ...
                                 t_curr cost_curr  gtd_curr  ]...
                               ,doPlot, minStep, maxStep);

        else
            t_new = polyinterp( [t_prev  cost_prev 0; ...
                                 t_curr cost_curr  0]...
                               ,doPlot, minStep, maxStep);
        end
    else
        t_new = mixedExtrap( t_prev, cost_prev, gtd_prev...
                            ,t_curr,cost_curr, gtd_curr ...
                            ,minStep,maxStep,disp_debug,doPlot);
    end
end

function [t] = mixedExtrap(x0,f0,g0,x1,f1,g1,minStep,maxStep,disp_debug,doPlot)
    alpha_c = polyinterp([x0 f0 g0; x1 f1 g1],doPlot,minStep,maxStep);
    alpha_s = polyinterp([x0 f0 g0; x1 sqrt(-1) g1],doPlot,minStep,maxStep);
    if alpha_c > minStep && abs(alpha_c - x1) < abs(alpha_s - x1)
        if disp_debug
            fprintf('Cubic Extrapolation\n');
        end
        t = alpha_c;
    else
        if disp_debug
            fprintf('Secant Extrapolation\n');
        end
        t = alpha_s;
    end
end