function [ bracket, bracketCost, bracketGrad] = WLSDG_computeBracketsFromSteps( paramIT )
    if paramIT.steps.zero.t ~= paramIT.steps.prev.t
        costVals = [paramIT.steps.zero.cost paramIT.steps.prev.cost paramIT.steps.curr.cost];
        t_Vals = [paramIT.steps.zero.t paramIT.steps.prev.t paramIT.steps.curr.t];
        g_Vals = [{paramIT.steps.zero.g};{paramIT.steps.prev.g}; {paramIT.steps.curr.g}];
        
        %sort them
        [costValsSorted_1, costValIDs] = sort(costVals); 
        t_ValsSorted_1 = t_Vals(costValIDs);
        g_ValsSorted_1 = g_Vals(costValIDs);
        
        % sort according to first 2 t_Vals and pick the first 2 at the same time
        [t_ValsSorted_2, t_ValIDs] = sort(t_ValsSorted_1(1:2));   
        costValsSorted_2 = costValsSorted_1(t_ValIDs);
        g_ValsSorted_2 = g_ValsSorted_1(t_ValIDs);
        g_ValsSorted_2 = [g_ValsSorted_2(1),g_ValsSorted_2(2)];
    else
        costValsSorted_2 = [paramIT.steps.zero.cost paramIT.steps.curr.cost];
        t_ValsSorted_2 = [paramIT.steps.zero.t paramIT.steps.curr.t];
        g_ValsSorted_2 = [{paramIT.steps.zero.g}; {paramIT.steps.curr.g}];
    end

    bracket = t_ValsSorted_2;
    bracketCost = costValsSorted_2;
    
    t_dif = abs(bracket(1)-bracket(2));
    cost_dif = abs(bracketCost(1)-bracketCost(2));
    
    if t_dif<0.001 && cost_dif<0.001
        a = 1;
        if isempty(g_ValsSorted_2{1}) || isempty(g_ValsSorted_2{2})
            if isempty(g_ValsSorted_2{1})
                a = 2;
            elseif isempty(g_ValsSorted_2{2})
                a = 1;
            else
                error('WTF');
            end
        end
        bracket = bracket(a);
        bracketCost = bracketCost(a);
        bracketGrad = g_ValsSorted_2{a};
    else
        if ~isempty(g_ValsSorted_2{1}) && ~isempty(g_ValsSorted_2{2})
            bracketGrad = [g_ValsSorted_2{1},g_ValsSorted_2{2}];
        elseif ~isempty(g_ValsSorted_2{1})
            bracketGrad = [g_ValsSorted_2{1} NaN(size(g_ValsSorted_2{1}))];
        elseif ~isempty(g_ValsSorted_2{2})
            bracketGrad = [NaN(size(g_ValsSorted_2{2})) g_ValsSorted_2{2}];
        else
            bracketGrad = NaN(size(paramIT.steps.zero.g,1), size(bracket,2));     
        end
    end
    if isempty(bracketGrad) || (size(bracketGrad,2)~=size(bracket,2))
        bracketGrad = NaN(size(paramIT.steps.zero.g,1), size(bracket,2));        
    end
end

