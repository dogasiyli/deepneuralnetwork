function [t] = WLSDG_mixedInterp(bracket,bracketCost,bracketGrad,d,Tpos,oldLOval,oldLOFval,oldLOGval,disp_debug,doPlot)
    % Mixed Case %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    nonTpos = -Tpos+3;

    gtdT = bracketGrad(:,Tpos)'*d;
    gtdNonT = bracketGrad(:,nonTpos)'*d;
    oldLOgtd = oldLOGval'*d;
    if bracketCost(Tpos) > oldLOFval
        alpha_c = polyinterp([oldLOval oldLOFval oldLOgtd
            bracket(Tpos) bracketCost(Tpos) gtdT],doPlot);
        alpha_q = polyinterp([oldLOval oldLOFval oldLOgtd
            bracket(Tpos) bracketCost(Tpos) sqrt(-1)],doPlot);
        if abs(alpha_c - oldLOval) < abs(alpha_q - oldLOval)
            if disp_debug
                fprintf('Cubic Interpolation\n');
            end
            t = alpha_c;
        else
            if disp_debug
                fprintf('Mixed Quad/Cubic Interpolation\n');
            end
            t = (alpha_q + alpha_c)/2;
        end
    elseif gtdT'*oldLOgtd < 0
        alpha_c = polyinterp([oldLOval oldLOFval oldLOgtd
            bracket(Tpos) bracketCost(Tpos) gtdT],doPlot);
        alpha_s = polyinterp([oldLOval oldLOFval oldLOgtd
            bracket(Tpos) sqrt(-1) gtdT],doPlot);
        if abs(alpha_c - bracket(Tpos)) >= abs(alpha_s - bracket(Tpos))
            if disp_debug
                fprintf('Cubic Interpolation\n');
            end
            t = alpha_c;
        else
            if disp_debug
                fprintf('Quad Interpolation\n');
            end
            t = alpha_s;
        end
    elseif abs(gtdT) <= abs(oldLOgtd)
        alpha_c = polyinterp([oldLOval oldLOFval oldLOgtd
            bracket(Tpos) bracketCost(Tpos) gtdT],...
            doPlot,min(bracket),max(bracket));
        alpha_s = polyinterp([oldLOval sqrt(-1) oldLOgtd
            bracket(Tpos) bracketCost(Tpos) gtdT],...
            doPlot,min(bracket),max(bracket));
        if alpha_c > min(bracket) && alpha_c < max(bracket)
            if abs(alpha_c - bracket(Tpos)) < abs(alpha_s - bracket(Tpos))
                if disp_debug
                    fprintf('Bounded Cubic Extrapolation\n');
                end
                t = alpha_c;
            else
                if disp_debug
                    fprintf('Bounded Secant Extrapolation\n');
                end
                t = alpha_s;
            end
        else
            if disp_debug
                fprintf('Bounded Secant Extrapolation\n');
            end
            t = alpha_s;
        end

        if bracket(Tpos) > oldLOval
            t = min(bracket(Tpos) + 0.66*(bracket(nonTpos) - bracket(Tpos)),t);
        else
            t = max(bracket(Tpos) + 0.66*(bracket(nonTpos) - bracket(Tpos)),t);
        end
    else
        t = polyinterp([bracket(nonTpos) bracketCost(nonTpos) gtdNonT
            bracket(Tpos) bracketCost(Tpos) gtdT],doPlot);
    end
end