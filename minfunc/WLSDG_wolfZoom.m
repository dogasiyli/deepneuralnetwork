function paramIT =  WLSDG_wolfZoom(paramIT, paramFX)
%% STEP04 - wolfZoom
% conversion completed
% TODO : what if gradient is not being calculated??
    if ~paramIT.wolfeConditionSatisfied && size(paramIT.bracket,2)>1
        tempBool = paramFX.objFuncParamsStruct.calculateGradient;
        paramFX.objFuncParamsStruct.calculateGradient = true;
        
        wolfeConditionSatisfied = false;
        bracket = paramIT.bracket;
        bracketGrad = paramIT.bracketGrad;
        bracketCost = paramIT.bracketCost;

        LOposRemoved = paramIT.LOposRemoved;
        Tpos = paramIT.Tpos;
        insufProgress = paramIT.insufProgress;
        if isfield(paramIT, 'LOpos')
            LOpos = paramIT.LOpos;
        else
            LOpos = 1;
        end
        iter_cur = paramIT.iter_cur;
        
        grad_dir_der = paramFX.grad_dir_der;
        disp_debug = paramFX.disp_debug;
        interpType = paramFX.interpType;
        desc_dir = paramFX.desc_dir;
        doPlot = paramFX.doPlot;
        iter_max = paramFX.iter_max;
        cost_init = paramFX.cost_init;
        suf_dec = paramFX.suf_dec;
        curv_param = paramFX.curv_param;
        tolX = paramFX.tolX;
        wolfZoomCnt=0;
        while ~wolfeConditionSatisfied && iter_cur < iter_max && wolfZoomCnt<10
            % Find High and Low Points in bracket
            [f_LO, LOpos] = min(bracketCost);
            HIpos = -LOpos + 3;

            % Compute new trial value
            if interpType == 3 || ~isLegal(bracketCost) || ~isLegal(bracketGrad)
                if disp_debug
                    fprintf('Bisecting\n');
                end
                t_cur = mean(bracket);
            elseif interpType == 4
                if disp_debug
                    fprintf('Grad-Cubic Interpolation\n');
                end
                t_cur = polyinterp([bracket(1) bracketCost(1) bracketGrad(:,1)'*desc_dir
                    bracket(2) bracketCost(2) bracketGrad(:,2)'*desc_dir],doPlot);
            else
                % Mixed Case %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                if LOposRemoved == 0
                    %nonTpos is a local variable
                    nonTpos = -Tpos+3;
                    oldLOval = bracket(nonTpos);
                    oldLOFval = bracketCost(nonTpos);
                    oldLOGval = bracketGrad(:,nonTpos);
                end
                t_cur = WLSDG_mixedInterp(bracket,bracketCost,bracketGrad,desc_dir,Tpos,oldLOval,oldLOFval,oldLOGval,disp_debug,doPlot);
            end


            % Test that we are making sufficient progress
            curProgess = min(max(bracket)-t_cur,t_cur-min(bracket))/(max(bracket)-min(bracket));
            if  curProgess < 0.1
                if disp_debug
                    fprintf('Interpolation close to boundary');
                end
                if insufProgress || t_cur>=max(bracket) || t_cur <= min(bracket)
                    if disp_debug
                        fprintf(', Evaluating at 0.1 away from boundary\n');
                    end
                    if abs(t_cur-max(bracket)) < abs(t_cur-min(bracket))
                        t_cur = max(bracket)-0.1*(max(bracket)-min(bracket));
                    else
                        t_cur = min(bracket)+0.1*(max(bracket)-min(bracket));
                    end
                    insufProgress = 0;
                else
                    if disp_debug
                        fprintf('\n');
                    end
                    insufProgress = 1;
                end
            else
                insufProgress = 0;
            end
            
            %check with t_cur and t_prev
%             t_prev = paramIT.steps.prev.t;
%             t_dif = abs(t_prev-t_cur);
%             if t_dif<10e-08
%                 wolfeConditionSatisfied = 1;%so that we can go into dgZoom
%                 insufProgress=1;
%                 [bracket, bracketCost, bracketGrad] = WLSDG_computeBracketsFromSteps(paramIT);  
%                 break;
%             end
            % Evaluate new point
            wolfZoomCnt=wolfZoomCnt+1;
            paramIT = evalNewPoint(paramIT, paramFX, t_cur, ['WolfLineZoom(' num2str(wolfZoomCnt) ')']);
            cost_new = paramIT.steps.curr.cost;
            grad_new = paramIT.steps.curr.g;
            grad_dir_der_new = paramIT.steps.curr.gtd;

            if cost_new > cost_init + suf_dec*t_cur*grad_dir_der || cost_new >= f_LO
                % Armijo condition not satisfied or not lower than lowest
                % point
                bracket(HIpos) = t_cur;
                bracketCost(HIpos) = cost_new;
                bracketGrad(:,HIpos) = grad_new;
                Tpos = HIpos;
            else
                if abs(grad_dir_der_new) <= - curv_param*grad_dir_der
                    % Wolfe conditions satisfied
                   wolfeConditionSatisfied = 1;
                   paramIT.steps.optm = paramIT.steps.curr;
                elseif grad_dir_der_new*(bracket(HIpos)-bracket(LOpos)) >= 0
                    % Old HI becomes new LO
                    bracket(HIpos) = bracket(LOpos);
                    bracketCost(HIpos) = bracketCost(LOpos);
                    bracketGrad(:,HIpos) = bracketGrad(:,LOpos);
                    if interpType == 5
                        if disp_debug
                            fprintf('LO Pos is being removed!\n');
                        end
                        LOposRemoved = 1;
                        oldLOval = bracket(LOpos);
                        oldLOFval = bracketCost(LOpos);
                        oldLOGval = bracketGrad(:,LOpos);
                    end
                end
                % New point becomes new LO
                bracket(LOpos) = t_cur;
                bracketCost(LOpos) = cost_new;
                bracketGrad(:,LOpos) = grad_new;
                Tpos = LOpos;
            end

            if ~wolfeConditionSatisfied && abs((bracket(1)-bracket(2))*grad_dir_der_new) < tolX
                if disp_debug
                    fprintf('Line Search can not make further progress\n');
                end
                break;
            end
        end

        paramIT.wolfeConditionSatisfied = wolfeConditionSatisfied;
        paramIT.LOposRemoved = LOposRemoved;
        paramIT.Tpos = Tpos;
        paramIT.insufProgress = insufProgress;
        paramIT.LOpos = LOpos;
        
        paramIT.bracket = bracket;
        paramIT.bracketCost = bracketCost;
        paramIT.bracketGrad = bracketGrad;
        
        paramFX.objFuncParamsStruct.calculateGradient = tempBool;
    end
end