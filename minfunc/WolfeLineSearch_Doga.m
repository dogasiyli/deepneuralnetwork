function [t_found,cost_new,grad_new,funEvals,H] = WolfeLineSearch_Doga(paramFX, objFuncParamsStruct)
%    paramIT, paramFX)
%
% Bracketing Line Search to Satisfy Wolfe Conditions
%
% Inputs:
%   paramIT : ITeration parameters
%             t_cur : current step size
%             t_found : found step size
%   paramFX : FiXed parameters
%             x_init: starting location (oldName : "x") --> parameters to be optimized
%             t_init: initial step size (oldName : "t")
%             desc_dir: descent direction (oldName : "d")
%             cost_init: function value at starting location (oldName : "f")
%             grad_init: gradient at starting location (oldName : "g")
%             grad_dir_der: directional derivative at starting location (oldName : "gtd")
%             suf_dec: sufficient decrease parameter (oldName : "c1")
%             curv_param: curvature parameter (oldName : "c2")
%             disp_debug: display debugging information (oldName : "debug")
%             interpType: type of interpolation (oldName : "LS")
%             iter_max: maximum number of iterations
%             tolX: minimum allowable step length
%             doPlot: do a graphical display of interpolation
%             funObj: objective function
%             objFuncParamsStruct: parameters of objective function
%
% Outputs:
%   t_found: step length (oldName : "t")
%   cost_new: function value at x_init+t_found*desc_dir
%   grad_new: gradient value at x_init+t_found*desc_dir
%   funEvals: number function evaluations performed by line search
%   H: Hessian at initial guess (only computed if requested

%     error('Not completed yet');

% Doga addition

%% STEP01 - assignParams
   [paramIT, paramFX] = assignParams(paramFX, objFuncParamsStruct);
    paramFX.computeHessian = (nargout == 5);    

%% STEP02 - evaluate_init_step
%  Evaluate the Objective and Gradient at the Initial Step
    paramIT =  evaluate_init_step(paramIT, paramFX);
    checkToPause([mfilename '-afterInit'],[],'after-evaluate_init_step');

%% STEP03 - assignBrackets
%  Wolfe criteria
%  Bracket an Interval containing a point satisfying this criteria
    [paramIT, resultIllegal] = assignBrackets(paramIT, paramFX);
    if resultIllegal
        [t_found,cost_new,grad_new,funEvals,H] = assignFinalParams(paramIT, paramFX);
        return
    end

%% STEP04 - wolfZoom
% We now either have a point satisfying the criteria, or a bracket
% surrounding a point satisfying the criteria
% Refine the bracket until we find a point satisfying the criteria
    paramIT =  WLSDG_wolfZoom(paramIT, paramFX);

%% STEP05 - Finalize function
    paramIT =  finalOperations(paramIT, paramFX);
    
%% STEP06 - assignFinalParams
%  Grab return values
    checkToPause([mfilename '-beforeFinal'],[],'before-assignFinalParams');
    [t_found,cost_new,grad_new,funEvals,H] = assignFinalParams(paramIT, paramFX);
end

%% STEP01 - assignParams
% conversion completed
function [paramIT, paramFX] = assignParams(paramFX,objFuncParamsStruct)
%   paramFX : FiXed parameters
    paramFX.objFuncParamsStruct = objFuncParamsStruct;
    if paramFX.iter_max<=1
        paramFX.iter_max = 5;%minimum allowed maximum iteration count is 5
    end
    
    paramIT.steps = struct;
    paramIT.steps.zero = struct;
    paramIT.steps.prev = struct;
%   paramIT : ITeration parameters
    paramIT.steps.zero.cost = paramFX.cost_init; %(oldName : "f_prev")
    paramIT.steps.zero.g = paramFX.grad_init; %(oldName : "g_prev")
    paramIT.steps.zero.t = 0;
    paramIT.steps.zero.d = paramFX.desc_dir;
    paramIT.steps.zero.gtd = paramFX.grad_dir_der; %(oldName : "gtd_prev")
    paramIT.steps.zero.theta = paramFX.x_init;
    if isfield(objFuncParamsStruct,'useAccuracyAsCost') && objFuncParamsStruct.useAccuracyAsCost==true
        paramIT.steps.zero.acc = 100-paramFX.cost_init;
    else
        paramIT.steps.zero.acc = NaN;
    end
    paramIT.steps.optm = paramIT.steps.zero;
    
    paramIT.steps.curr.cost = NaN;
    paramIT.steps.curr.g = NaN;
    paramIT.steps.curr.t = paramFX.t_init;
    paramIT.steps.curr.d = NaN;
    paramIT.steps.curr.gtd = NaN;
    %paramIT.steps.curr.theta = paramFX.x_init + paramIT.steps.curr.t*paramFX.desc_dir;
    
    %initializations
    paramIT.funEvals = 0;
    paramIT.iter_cur = 0; %(oldName : "LSiter")
    paramIT.insufProgress = 0;
    paramIT.Tpos = 2;
    paramIT.LOposRemoved = 0;

    %assign initial parameters
    paramIT.wolfeConditionSatisfied = 0; %(oldName : "done")

    %Assign extra parameters for OPS(objFuncParamsStruct)
    %when the wolfline search is being used, the gradient calculation can
    %be time consuming. different than the source wolfline search function
    %we are trying to use 1-accuracy as the cost and trying to pursue the
    %correct 't' according to accuracy. hence in the wolfline search we try
    %to use accuracy as the cost function
    paramFX.objFuncParamsStruct.useAccuracyAsCost = getStructField(objFuncParamsStruct,'useAccuracyAsCost',false);
    paramFX.objFuncParamsStruct.calculateGradient = getStructField(objFuncParamsStruct,'calculateGradient',true);    
end

%% STEP02 - evaluate_init_step
% conversion completed
function [paramIT, paramFX] =  evaluate_init_step(paramIT, paramFX)
    global minFuncStepInfo;

    if ~isempty(minFuncStepInfo) && isstruct(minFuncStepInfo) && isfield(minFuncStepInfo,'StepType')
        minFuncStepInfo.StepType = 'WolfLineInit';%'MainIteration';%'WolfLineStep';%'ArmijoBacktrack';%
        minFuncStepInfo.StepLength = paramIT.steps.curr.t;
    end
    
    tempCalculateGradientBool = paramFX.objFuncParamsStruct.calculateGradient;
    objFuncParamsStruct.calculateGradient = true;
        paramIT = evalNewPoint(paramIT, paramFX, paramIT.steps.curr.t ,'WolfeLineSearch_Doga@evaluate_init_step');
    paramFX.objFuncParamsStruct.calculateGradient = tempCalculateGradientBool;
    clear tempCalculateGradientBool;
end

%% STEP03 - assignBrackets
function [paramIT, resultIllegal] = assignBrackets(paramIT, paramFX)
    iter_max = paramFX.iter_max;
    objFuncParamsStruct = paramFX.objFuncParamsStruct;
    resultIllegal = false;
    
    paramIT.steps.prev = paramIT.steps.zero;
    while paramIT.iter_cur < iter_max
        %Find the bracket - maybe this while loop can end here!!
        [paramIT, bracketResult, retVal] = WLSDG_assignBrackets_01( paramIT, paramFX, objFuncParamsStruct );
        switch retVal
            case 1
                resultIllegal = true;
                return
            case {2,3,4}
                bracket = bracketResult.bracket;
                bracketCost = bracketResult.bracketCost;
                bracketGrad = bracketResult.bracketGrad;
                clear bracketResult;
                break
        end
        %Update paramIT.steps.curr.t by applying interpolation
        t_new = WLSDG_assignBrackets_02(paramIT, paramFX);
        esc_case_04 = abs(paramIT.steps.curr.t-t_new)<0.0001;
        if esc_case_04
            retVal = 5;%break;
            bracket = bracketResult.bracket;
            bracketCost = bracketResult.bracketCost;
            bracketGrad = bracketResult.bracketGrad;
            clear bracketResult;
            break
        end
        %Use the new paramIT.steps.curr.t and evaluate 
        paramIT = evalNewPoint(paramIT, paramFX, t_new, 'WolfLineBracket-2');
    end
    assert(size(bracketGrad,2)==size(bracket,2),'bracketGradCalculation problematic');
    
    %it can either end due to retVal = {2,3,4} or paramIT.iter_cur == iter_max
    switch retVal
        case {2,4,5}
            %bracketResult.bracket = [t_prev t_cur];
            %bracketResult.bracketCost = [cost_prev cost_new];
            %if objFuncParamsStruct.calculateGradient
            %   bracketResult.bracketGrad = [g_prev g_new];
            %else
            %   bracketResult.bracketGrad = NaN;
            %end
            if sum(isnan(bracketGrad(:)))>0
                %if at least one of the bracketGrad is empty get in here
                tempBool = objFuncParamsStruct.calculateGradient;
                paramFX.objFuncParamsStruct.calculateGradient = true;
                for j=1:size(bracketGrad,2)
                    if sum(isnan(bracketGrad(:,j)))>0
                        paramIT = evalNewPoint(paramIT, paramFX, bracket(j),['WolfLineBracket-CalcGrad(' num2str(j) ')']);
                        bracketGrad(:,j) = paramIT.steps.curr.g;
                    end
                end
                paramFX.objFuncParamsStruct.calculateGradient = tempBool;
                clear tempBool;
                %The job is for wolfZoom to handle now
            else
                %both gradients are already calculated
                %maybe it is case 5 and paramIT.wolfeConditionSatisfied = 1;
            end
        case 3
            %bracketResult.bracket = t_cur;
            %bracketResult.bracketCost = cost_new;
            %bracketResult.bracketGrad = g_new;
            %paramIT.wolfeConditionSatisfied = 1;
            
            %The answer is found
            %it is paramIT.steps.curr.t if it has better solution
            % if paramIT.steps.curr.cost<paramIT.steps.zero.cost
            %     paramIT.steps.XXXoptm = paramIT.steps.curr;
            % else
            %     paramIT.steps.XXXoptm = paramIT.steps.zero;
            % end
        otherwise
            if paramIT.iter_cur == iter_max
                %it just ended because iteration count
                %it is paramIT.steps.curr.t if it has better solution
                % if paramIT.steps.curr.cost<paramIT.steps.zero.cost
                %     paramIT.steps.XXXoptm = paramIT.steps.curr;
                % else
                %     paramIT.steps.XXXoptm = paramIT.steps.zero;
                % end
            else
                %this should not happen!!
                error('We shall not be here');
            end
    end
    
    if paramIT.iter_cur == iter_max
        if paramFX.objFuncParamsStruct.calculateGradient==false && paramIT.steps.curr.t ~= paramIT.steps.optm.t
            paramFX.objFuncParamsStruct.calculateGradient = true;
            %I have to check bracketCost
            paramIT = evalNewPoint(paramIT, paramFX, paramIT.steps.optm.t, 'Wolf_{Acc-IterMax}-TCurAssign');
        end
        
        cost_init = paramIT.steps.zero.cost;
        g_init = paramIT.steps.zero.g;
        
        cost_new = paramIT.steps.optm.cost;
        g_new = paramIT.steps.optm.g;
        t_new = paramIT.steps.optm.t;
        
        bracket = [0 t_new];
        bracketCost = [cost_init cost_new];
        bracketGrad = [g_init g_new];
    end
    
    paramIT.bracket = bracket;
    paramIT.bracketCost = bracketCost;
    paramIT.bracketGrad = bracketGrad; 
end

%% STEP05 - Finalize function
% conversion completed
% TODO : what if gradient is not being calculated??
function paramIT =  finalOperations(paramIT, paramFX)
    if paramIT.iter_cur == paramFX.iter_max
        if paramFX.disp_debug
            fprintf('Line Search Exceeded Maximum Line Search Iterations\n');
        end
    end
    assert(isfield(paramIT.steps.optm,'cost') && isfield(paramIT.steps.optm,'g') && ~isempty(paramIT.steps.optm.g),'<cost> and <g> must be fields of paramIT.steps.optm');
    
    %Evaluate Hessian at new point
    if paramFX.computeHessian && paramIT.funEvals > 1 && paramFX.saveHessianComp
        paramIT = evalNewPoint(paramIT, paramFX, paramIT.steps.curr.t ,'EvaluateHessian@NewPoint');
    end
    
%     if paramIT.steps.optm.t==0
%         %now apply doga bracket search
%         paramIT =  dgZoom(paramIT, paramFX);
%     end
end

%% STEP06 - assignFinalParams
% conversion completed
% TODO : what if gradient is not being calculated??
function [t_found,cost_new,grad_new,funEvals,H] = assignFinalParams(paramIT, paramFX)
    if (paramFX.computeHessian)
        H = paramIT.H;
    else
        H = [];
    end
    t_found = paramIT.steps.optm.t;
    cost_new = paramIT.steps.optm.cost;
    grad_new = paramIT.steps.optm.g;
    funEvals = paramIT.funEvals;
end

% other screen notepad++ view
%   paramIT : ITeration parameters
% 
% 			  t_prev : previous step size
%             t_cur : current step size
% 			  cost_prev : previous function value (oldName : "f_prev")
% 			  cost_new : cur function value (oldName : "f_new")
%             grad_prev: previous gradient (oldName : "g_prev")
% 			  grad_new
% 			  grad_dir_der_prev : previous directional derivative
% 			  grad_dir_der_new : new directional derivative
% 
% 			  iter_cur : current iteration (oldName : "LSiter")
% 			  funEvals
% 
%             t_found : found step size
% 			  wolfeConditionSatisfied : (oldName : "done")
% 			  insufProgress
% 			  H
% 			  x_new
% 
% 			  Tpos
% 			  LOposRemoved
% 			  LOpos
% 			  bracket
% 			  bracketCost
% 			  bracketGrad
% 
%   paramFX : FiXed parameters
%             desc_dir: descent direction (oldName : "d")
%             cost_init: function value at starting location (oldName : "f")
%             grad_init: gradient at starting location (oldName : "g")
%             grad_dir_der: directional derivative at starting location (oldName : "gtd")
%             suf_dec: sufficient decrease parameter (oldName : "c1")
%             curv_param: curvature parameter (oldName : "c2")
%             disp_debug: display debugging information (oldName : "debug")
%             interpType: type of interpolation (oldName : "LS")
%             iter_max: maximum number of iterations
%             tolX: minimum allowable step length
%             doPlot: do a graphical display of interpolation
%             funObj: objective function
%             varargin: parameters of objective function
% 			  saveHessianComp
% 			  computeHessian
%             x_init: starting location (oldName : x) --> parameters to be optimized
%             t_init: initial step size (oldName : t)








%deleted code 1 :
%                 if disp_debug
%                     fprintf('Extrapolated into illegal region, Bisecting\n');
%                 end
%                 t_cur = (t_cur + t_prev)/2;
%                 if ~saveHessianComp && paramFX.computeHessian
%                     [cost_new,grad_new,H] = feval(funObj, x + t*d, paramFX.vararginStruct{:}); 
%                 else
%                     [cost_new,grad_new] = feval(funObj, x + t*d, paramFX.vararginStruct{:}); 
%                     if ~isempty(minFuncStepInfo) && isstruct(minFuncStepInfo) && isfield(minFuncStepInfo,'StepType')
%                         minFuncStepInfo.FunEvals = minFuncStepInfo.FunEvals + 1;
%                         minFuncStepInfo.FunCost = cost_new;
%                         minFuncStepInfo.StepType = 'WolfLineBracket';%'WolfLineInit';%'MainIteration';%'WolfLineStep';%'ArmijoBacktrack';%
%                         minFuncStepInfo.StepLength = t;
%                     end
%                 end
%                 funEvals = funEvals + 1;
%                 grad_dir_der_new = grad_new'*d;
%                 iter_cur = iter_cur+1;
%                 continue;