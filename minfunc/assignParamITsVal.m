function paramIT = assignParamITsVal(paramIT,structStr,costVal,g,t,d,gtd,acc,theta) %#ok<INUSD>
    if ~isfield(paramIT.steps,structStr)
        eval(['paramIT.steps.' structStr ' = struct;']);
    end
    eval(['paramIT.steps.' structStr '.cost = costVal;']);%paramIT.steps.zero.cost = [];%old usage : paramIT.f etc;
    eval(['paramIT.steps.' structStr '.g = g;']);%paramIT.steps.zero.g = [];%old usage : paramIT.g or grad_cur etc.;
    eval(['paramIT.steps.' structStr '.t = t;']);%paramIT.steps.zero.t = 1;%old usage : paramIT.t;
    eval(['paramIT.steps.' structStr '.d = d;']);%paramIT.steps.zero.d = zeros(paramIT.p,1);%old usage : paramIT.d;
    eval(['paramIT.steps.' structStr '.gtd = gtd;']);%paramIT.steps.zero.gtd = [];%old usage : paramIT.gtd;
    eval(['paramIT.steps.' structStr '.acc = acc;']);%this is newly added;
    eval(['paramIT.steps.' structStr '.theta = theta;']);%this is newly added;
end