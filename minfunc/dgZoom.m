function paramIT =  dgZoom(paramIT, paramFX)
    %this function is called when no better accuracy is gained and cost is
    %used as 1-accuracy
    
    best_t = paramIT.steps.optm.t;%supposed to be 0
    bestCost = paramIT.steps.optm.cost;

    interpolateParam = 0.5;
    paramIT.dgBracket_tCur = [0.00 0.25 0.50 0.75 1.00];
    paramIT = assignNewDgBrackets_Initial(paramIT, interpolateParam);
    
    dgConditionSatisfied = false;
    bracketCount_cur = 1;
    bracketCount_max = 8;   
    accuracyIncreased_arr = false(1,bracketCount_max);
    
    while ~dgConditionSatisfied && bracketCount_cur<=bracketCount_max
        %1. get the dgBracket_tCur and bracektCost
        dgBracket_tCur = paramIT.dgBracket_tCur;
        dgBracket_Cost = paramIT.dgBracket_Cost;
        assert(length(dgBracket_tCur)==length(dgBracket_Cost),'dgBracket_tCur and dgBracket_Cost must have same lengths')
        accuracyIncreased_epoch = false;
        
        %2.find the cost values for the bracket params
        for i=1:length(dgBracket_tCur)
            if ~isnan(dgBracket_Cost(i))
                %this value is already known
                continue;
            end
            paramIT = evalNewPoint(paramIT, paramFX, dgBracket_tCur(i), ['DogaBracket-trial('  num2str(bracketCount_cur)  '\' num2str(bracketCount_max) ')-it(' num2str(i) '\' num2str(length(dgBracket_tCur)) ')']);
            dgBracket_Cost(i) = paramIT.steps.optm.cost;
            if dgBracket_Cost(i)<bestCost
                accuracyIncreased_epoch = true;
                best_t = dgBracket_tCur(i);
                bestCost = paramIT.steps.curr.cost;
            end
        end
        
        %3.
%         if accuracyIncreased_epoch
            %we gained some accuracy at this bracketing
            %3.a. find the best 2 costs in dgBracket_tCur
            [costValsSorted, costInds] = sort(dgBracket_Cost);%[0.6 0.7 0.8 0.9 1.0],[5 2 1 3 4]] = sort([0.8 0.7 0.9 1.0 0.6])
            tIndsSelected = dgBracket_tCur(costInds(1:2));%[1.0 -0.5] = <dgBracket_tCur = [-1.0 -0.5 0.0 +0.5 +1.0]>(5,2)
            [tIndsSorted, tIndsInds] = sort(tIndsSelected);%[[-0.5 1.0],[2 1]] = sort([1.0 -0.5])
            %3.b. assign first and last for zooming into
            paramIT.dgBracket_tCur = linspace(tIndsSorted(1),tIndsSorted(2),length(dgBracket_tCur));
            paramIT.dgBracket_Cost = NaN(size(paramIT.dgBracket_Cost));
            paramIT.dgBracket_Cost(1) = dgBracket_Cost(costInds(tIndsInds(1)));
            paramIT.dgBracket_Cost(end) = dgBracket_Cost(costInds(tIndsInds(2)));
%            interpolateParam = (tIndsSorted(2)-tIndsSorted(1))/length(dgBracket_tCur);
%         else
%             %we didn't gain any accuracy at this bracketing
%             dgBracket_tCur = dgBracket_tCur(3)+([-interpolateParam^2 -interpolateParam 0 interpolateParam interpolateParam^2]);
%             dgBracket_tCur(3) = paramIT.dgBracket_tCur(3);
%             dgBracket_Cost([1 2 4 5]) = nan;
%             [tValsSorted, tInds] = sort(dgBracket_tCur);
%             paramIT.dgBracket_tCur = tValsSorted;
%             paramIT.dgBracket_Cost = dgBracket_Cost(tInds);
            if bracketCount_cur>1 && (accuracyIncreased_arr(bracketCount_cur-1) && ~accuracyIncreased_epoch)
                break;
            end
%         end
        accuracyIncreased_arr(bracketCount_cur) = accuracyIncreased_epoch;
        bracketCount_cur = bracketCount_cur +1;
    end
    
    dgConditionSatisfied =  best_t~=0;
    %now we can be here for 2 different reasons
    if dgConditionSatisfied
        %a better accuracy is found
        paramFX.objFuncParamsStruct.calculateGradient = true;
        paramIT = evalNewPoint(paramIT, paramFX, best_t,['dgZoom-Acc-TCur(' num2str(best_t,'%6.4e') ')-Assign']);
    else
        %no better accuracy can be found
        checkToPause([mfilename '-noBetterAcc'],[],'');
        paramFX.objFuncParamsStruct.useAccuracyAsCost = false;
        paramFX.objFuncParamsStruct.calculateGradient = true;
        [t_found,cost_new,grad_new,funEvals] = WolfeLineSearch_Doga(paramFX, paramFX.objFuncParamsStruct);
        paramIT.steps.optm.t = t_found;
        paramIT.steps.optm.cost = cost_new;
        paramIT.steps.optm.g = grad_new;
        paramIT.funEvals = paramIT.funEvals + funEvals;        
    end
end

% function paramIT = assignNewDgBrackets_V02(paramIT, interpolateParam)
%     paramIT.dgBracket_tCurDif = interpolateParam*paramIT.dgBracket_tCurDif;
%     paramIT.dgBracket_tCur = -2*paramIT.dgBracket_tCurDif : paramIT.dgBracket_tCurDif : 2*paramIT.dgBracket_tCurDif;
%     paramIT.dgBracket_Cost = [nan nan paramIT.cost_new nan nan];
% end

function paramIT = assignNewDgBrackets_Initial(paramIT, interpolateParam)
    paramIT.dgBracket_tCurDif = interpolateParam;
    paramIT.dgBracket_tCur = -2*paramIT.dgBracket_tCurDif : paramIT.dgBracket_tCurDif : 2*paramIT.dgBracket_tCurDif;
    paramIT.dgBracket_Cost = [nan nan paramIT.steps.optm.cost nan nan];
end