function paramIT = evalNewPoint(paramIT, paramFX, t_new, curStepType)
    global minFuncStepInfo;

    %Assign prev=cur
    paramIT.steps.prev = paramIT.steps.curr;
        
    funObj = paramFX.funObj;
    x_init = paramFX.x_init;
    desc_dir = paramFX.desc_dir;
    objFuncParamsStruct = paramFX.objFuncParamsStruct;
        
    if ~isempty(minFuncStepInfo) && isstruct(minFuncStepInfo) && isfield(minFuncStepInfo,'StepType')
        minFuncStepInfo.FunEvals = minFuncStepInfo.FunEvals + 1;
        minFuncStepInfo.StepType = curStepType;%'WolfLineBracket-2';%'ArmijoBacktrack';%'WolfLineInit';%'MainIteration';%'WolfLineStep';%
        minFuncStepInfo.StepLength = t_new;
    end
    theta_new = x_init + t_new*desc_dir;
    [cost_new, grad_new, optionalObjFuncOutputs] = feval(funObj, theta_new, objFuncParamsStruct);
    if (paramFX.saveHessianComp || paramFX.computeHessian) && isfield(optionalObjFuncOutputs,'hessianParam')
        paramIT.H = optionalObjFuncOutputs.hessianParam;
    end
    if ~isempty(grad_new)
         paramIT.steps.curr.gtd = grad_new'*desc_dir;
    end
    paramIT.funEvals = paramIT.funEvals + 1;
    paramIT.iter_cur =  paramIT.iter_cur+1;
    paramIT.steps.curr.t = t_new;
    paramIT.steps.curr.cost = cost_new;
    paramIT.steps.curr.g = grad_new;
    if isfield(optionalObjFuncOutputs,'accuracy')
        minFuncStepInfo.Accuracy = optionalObjFuncOutputs.accuracy;
    end
    if isfield(minFuncStepInfo,'Accuracy')
        paramIT.steps.curr.acc = minFuncStepInfo.Accuracy;
    end
    
    %Update optimum result if needed to
    if paramIT.steps.optm.cost>=paramIT.steps.curr.cost
        checkToPause([mfilename '-better'],[],['lower cost found-t(' num2str(t_new) ')-cost_new(' num2str(cost_new) ')']);
        paramIT.steps.curr.theta = theta_new;
        paramIT.steps.optm = paramIT.steps.curr;
    else
        checkToPause([mfilename '-nobetter'],[],'lower cost NOT found');        
    end
end