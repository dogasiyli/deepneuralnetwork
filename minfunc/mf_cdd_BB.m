function [paramIT, paramME] = mf_cdd_BB(iterationID, f, paramIT, paramME)
%assign initializations
    grad_cur = paramIT.grad_cur;
    if isfield(paramIT, 'g_old'), g_old = paramIT.g_old; end
    t = paramIT.t;
    d = paramIT.d;
    bbType = paramME.bbType;
    if isfield(paramME, 'myF_old'), myF_old = paramME.myF_old; end
    if isfield(paramME, 'v'), v = paramME.v; end
    
    if iterationID == 1
        d = -grad_cur;
    else
        y = grad_cur-g_old;
        s = t*d;
        if bbType == 0
            yy = y'*y;
            alpha = (s'*y)/(yy);
            if alpha <= 1e-10 || alpha > 1e10
                alpha = 1;
            end
        elseif bbType == 1
            sy = s'*y;
            alpha = (s'*s)/sy;
            if alpha <= 1e-10 || alpha > 1e10
                alpha = 1;
            end
        elseif bbType == 2 % Conic Interpolation ('Modified BB')
            sy = s'*y;
            ss = s'*s;
            alpha = ss/sy;
            if alpha <= 1e-10 || alpha > 1e10
                alpha = 1;
            end
            alphaConic = ss/(6*(myF_old - f) + 4*grad_cur'*s + 2*g_old'*s);
            if alphaConic > .001*alpha && alphaConic < 1000*alpha
                alpha = alphaConic;
            end
        elseif bbType == 3 % Gradient Method with retards (bb type 1, random selection of previous step)
            sy = s'*y;
            alpha = (s'*s)/sy;
            if alpha <= 1e-10 || alpha > 1e10
                alpha = 1;
            end
            v(1+mod(iterationID-2,5)) = alpha;
            alpha = v(ceil(rand*length(v)));
        end
        d = -alpha*grad_cur;
    end
    paramIT.d = d;
    paramME.g_old = paramIT.grad_cur;
    paramME.myF_old = paramIT.f;
    paramME.v = v;
end