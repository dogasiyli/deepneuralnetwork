function [paramIT, paramME] = mf_cdd_CG(iterationID, paramGN, paramME, paramIT)
%assign initializations
    d = paramIT.d;
    grad_cur = paramIT.grad_cur;
    if isfield(paramIT, 'g_old'), g_old = paramIT.g_old; end
    cgUpdate = paramME.cgUpdate;
    debug = paramGN.debug;

    if iterationID == 1
        d = -grad_cur; % Initially use steepest descent direction
    else
        gtgo = grad_cur'*g_old;
        gotgo = g_old'*g_old;

        switch (cgUpdate)
            case 0 % Fletcher-Reeves
                beta = (grad_cur'*grad_cur)/(gotgo);
            case 1% Polak-Ribiere
                beta = (grad_cur'*(grad_cur-g_old)) /(gotgo);
            case 2% Hestenes-Stiefel
                beta = (grad_cur'*(grad_cur-g_old))/((grad_cur-g_old)'*d);
            otherwise% Gilbert-Nocedal
                beta_FR = (grad_cur'*(grad_cur-g_old)) /(gotgo);
                beta_PR = (grad_cur'*grad_cur-gtgo)/(gotgo);
                beta = max(-beta_FR,min(beta_PR,beta_FR));                
        end
        
        d = -grad_cur + beta*d;

        % Restart if not a direction of sufficient descent
        if grad_cur'*d > -tolX
            if debug
                fprintf('Restarting CG\n');
            end
            beta = 0;
            d = -grad_cur;
        end
        % Old restart rule:
        %if beta < 0 || abs(gtgo)/(gotgo) >= 0.1 || grad_cur'*d >= 0
    end
%do updates    
    paramIT.d = d;
    paramIT.g_old = grad_cur;
end