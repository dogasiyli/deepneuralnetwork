function [paramIT] = mf_cdd_CSD(iterationID, paramIT, paramME)
    if mod(iterationID,paramME.cycle) == 1 % Use Steepest Descent
        alpha = 1;
        paramIT.LS_init = 2;
        paramIT.LS = 4; % Precise Line Search
    elseif mod(iterationID,paramME.cycle) == mod(1+1,paramME.cycle) % Use Previous Step
        alpha = paramIT.t;
        paramIT.LS_init = 0;
        paramIT.LS = 2; % Non-monotonic line search
    end
    paramIT.d = -alpha*paramIT.grad_cur;
end