function [paramIT, paramME] = mf_cdd_LBGFS(iterationID, paramGN, paramME, paramIT)
%assign initializations
    debug = paramGN.debug;
    useMex = paramGN.useMex;
    corrections = paramME.corrections;
    Damped = paramME.Damped;
    if iterationID==1
        grad_cur = paramIT.steps.zero.g;%the final grad that was found is the initial grad of our search
        t_cur = paramIT.steps.zero.t;
    else
        grad_cur = paramIT.steps.curr.g;%the final grad that was found is the initial grad of our search
        t_cur = paramIT.steps.curr.t;
        d_prev = paramIT.steps.prev.d;
    end
    if isfield(paramIT.steps, 'prev')
        grad_old = paramIT.steps.prev.g;
    end
    
    % Update the direction and step sizes
    if iterationID == 1
        d_cur = -grad_cur; % Initially use steepest descent direction
        Hdiag = 1;
        old_dirs = zeros(length(grad_cur),0);
        old_stps = zeros(length(d_cur),0);
        paramIT.steps.zero.d = d_cur;
        paramIT.steps.optm.d = d_cur;
    else
        %initializations for readibility
        old_dirs = paramME.old_dirs;
        old_stps = paramME.old_stps;
        Hdiag = paramME.Hdiag;
        if Damped
            [old_dirs, old_stps, Hdiag] = ...
                dampedUpdate( grad_cur-grad_old...
                             ,t_cur*d_prev...
                             ,corrections, debug...
                             ,old_dirs,old_stps,Hdiag);
        else
            [old_dirs, old_stps, Hdiag] = ...
                lbfgsUpdate(  grad_cur-grad_old...
                             ,t_cur*d_prev...
                             ,corrections...
                             ,debug...
                             ,old_dirs,old_stps,Hdiag);
        end

        if useMex
            d_cur = lbfgsC( -grad_cur...
                       ,old_dirs, old_stps, Hdiag);
        else
            d_cur = lbfgs( -grad_cur...
                      ,old_dirs, old_stps, Hdiag);
        end
    end

% update the parameters
    %curr
    paramIT.steps.curr.d = d_cur;%paramIT.d = d_cur;
    
    paramME.old_dirs = old_dirs;
    paramME.old_stps = old_stps;
    paramME.Hdiag = Hdiag;
end