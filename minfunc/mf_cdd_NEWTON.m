function [paramIT, paramME] = mf_cdd_NEWTON(iterationID, paramGN, paramME, paramIT)
    error('not corrected yet');
%paramME.H = H
%paramME.input = varargin{:};
    if paramME.cgSolve == 0
        switch (paramME.HessianModify)
            case 0
                % Attempt to perform a Cholesky factorization of the Hessian
                [R, posDef] = chol(paramME.H);
                
                % If the Cholesky factorization was successful, then the Hessian is
                % positive definite, solve the system
                if posDef == 0
                    d = -R\(R'\grad_cur);
                else
                    % otherwise, adjust the Hessian to be positive definite based on the
                    % minimum eigenvalue, and solve with QR
                    % (expensive, we don't want to do this very much)
                    if debug
                        fprintf('Adjusting Hessian\n');
                    end
                    paramME.H = paramME.H + eye(length(grad_cur)) * max(0,1e-12 - min(real(eig(paramME.H))));
                    d = -paramME.H\grad_cur;
                end
            case 1% Modified Incomplete Cholesky
                R = mcholinc(paramME.H,debug);
                d = -R\(R'\grad_cur);
            case 2% Modified Generalized Cholesky
                if paramME.useMex
                    [L, D, perm] = mcholC(paramME.H);
                else
                    [L, D, perm] = mchol(paramME.H);
                end
                d(perm) = -L' \ ((D.^-1).*(L \ grad_cur(perm)));
            case 3% Modified Spectral Decomposition
                [V,D] = eig((paramME.H+paramME.H')/2);
                D = diag(D);
                D = max(abs(D),max(max(abs(D)),1)*1e-12);
                d = -V*((V'*grad_cur)./D);               
            case 4% Modified Symmetric Indefinite Factorization
                [L,D,perm] = ldl(paramME.H,'vector');
                [blockPos, junk] = find(triu(D,1));
                for diagInd = setdiff(setdiff(1:p,blockPos),blockPos+1)
                    if D(diagInd,diagInd) < 1e-12
                        D(diagInd,diagInd) = 1e-12;
                    end
                end
                for blockInd = blockPos'
                    block = D(blockInd:blockInd+1,blockInd:blockInd+1);
                    block_a = block(1);
                    block_b = block(2);
                    block_d = block(4);
                    lambda = (block_a+block_d)/2 - sqrt(4*block_b^2 + (block_a - block_d)^2)/2;
                    D(blockInd:blockInd+1,blockInd:blockInd+1) = block+eye(2)*(lambda+1e-12);
                end
                d(perm) = -L' \ (D \ (L \ grad_cur(perm)));
            otherwise
                % Take Newton step if Hessian is pd,
                % otherwise take a step with negative curvature
                [R, posDef] = chol(paramME.H);
                if posDef == 0
                    d = -R\(R'\grad_cur);
                else
                    if debug
                        fprintf('Taking Direction of Negative Curvature\n');
                    end
                    [V,D] = eig(paramME.H);
                    u = V(:,1);
                    d = -sign(u'*grad_cur)*u;
                end
        end
    else
        % Solve with Conjugate Gradient
        cgMaxIter = p;
        cgForce = min(0.5,sqrt(norm(grad_cur)))*norm(grad_cur);
        
        % Select Preconditioner
        switch (paramME.cgSolve)
            case 1% No preconditioner
                precondFunc = [];
                precondArgs = [];
            case 2% Diagonal preconditioner
                precDiag = diag(paramME.H);
                precDiag(precDiag < 1e-12) = 1e-12 - min(precDiag);
                precondFunc = @precondDiag;
                precondArgs = {precDiag.^-1};      
            case 3% L-BFGS preconditioner
                if i == 1
                    paramME.old_dirs = zeros(length(grad_cur),0);
                    paramME.old_stps = zeros(length(grad_cur),0);
                    paramME.Hdiag = 1;
                else
                    [paramME.old_dirs,paramME.old_stps,paramME.Hdiag] = ...
                                                lbfgsUpdate( grad_cur-paramME.g_old...
                                                            ,t*d...
                                                            ,paramME.corrections...
                                                            ,debug...
                                                            ,paramME.old_dirs,paramME.old_stps,paramME.Hdiag);
                end
                paramME.g_old = grad_cur;
                if paramME.useMex
                    precondFunc = @lbfgsC;
                else
                    precondFunc = @lbfgs;
                end
                precondArgs = {paramME.old_dirs, paramME.old_stps, paramME.Hdiag};
            otherwise
                if cgSolve > 0
                    % Symmetric Successive Overelaxation Preconditioner
                    omega = cgSolve;
                    D = diag(paramME.H);
                    D(D < 1e-12) = 1e-12 - min(D);
                    precDiag = (omega/(2-omega))*D.^-1;
                    precTriu = diag(D/omega) + triu(paramME.H,1);
                    precondFunc = @precondTriuDiag;
                    precondArgs = {precTriu,precDiag.^-1};
                else
                    % Incomplete Cholesky Preconditioner
                    opts.droptol = -cgSolve;
                    opts.rdiag = 1;
                    R = cholinc(sparse(paramME.H),opts);
                    if min(diag(R)) < 1e-12
                        R = cholinc(sparse(paramME.H + eye*(1e-12 - min(diag(R)))),opts);
                    end
                    precondFunc = @precondTriu;
                    precondArgs = {R};
                end                
        end

        % Run cg with the appropriate preconditioner
        if isempty(paramME.HvFunc)
            % No user-supplied Hessian-vector function
            [d,cgIter,cgRes] = conjGrad(paramME.H...
                                        ,-grad_cur...
                                        ,cgForce,cgMaxIter...
                                        ,debug,precondFunc,precondArgs);
        else
            % Use user-supplied Hessian-vector function
            [d,cgIter,cgRes] = conjGrad( paramME.H...
                                        ,-grad_cur...
                                        ,cgForce,cgMaxIter...
                                        ,debug,precondFunc,precondArgs...
                                        ,paramME.HvFunc,{thetaOut,paramME.input{:}});
        end
        if debug
            fprintf('CG stopped after %d iterations w/ residual %.5e\n', cgIter, cgRes);
            %funEvals = funEvals + cgIter;
        end
    end
end
