function [paramIT, paramME] = mf_cdd_NEWTON0(iterationID, paramGN, paramME, paramIT)
    %initializations for readibility
    debug = paramGN.debug;
    maxFunEvals = paramGN.maxFunEvals;
    useComplex = paramGN.useComplex;

    corrections = paramME.corrections;    
    cgSolve = paramME.cgSolve;
    HvFunc = paramME.HvFunc;
    useNegCurv = paramME.useNegCurv;
    precFunc = paramME.precFunc;
    
    funEvals = paramIT.funEvals;
    p = paramIT.p;
    d = paramIT.d;
    t = paramIT.t;
    grad_cur = paramIT.grad_cur;
    g_old = paramIT.g_old;
    

    cgMaxIter = min(p,maxFunEvals-funEvals);
    cgForce = min(0.5,sqrt(norm(grad_cur)))*norm(grad_cur);
    
    % Set-up preconditioner
    precondFunc = [];
    precondArgs = [];
    if cgSolve == 1
        if isempty(precFunc) % Apply L-BFGS preconditioner
            if iterationID == 1
                old_dirs = zeros(length(grad_cur),0);
                old_stps = zeros(length(grad_cur),0);
                Hdiag = 1;
            else
                old_dirs = paramME.old_dirs;
                old_stps = paramME.old_stps;
                Hdiag = paramME.Hdiag;
                [old_dirs, old_stps, Hdiag] = lbfgsUpdate( grad_cur-g_old...
                                                          ,t*d...
                                                          ,corrections...
                                                          ,debug...
                                                          ,old_dirs, old_stps, Hdiag);
                if useMex
                    precondFunc = @lbfgsC;
                else
                    precondFunc = @lbfgs;
                end
                precondArgs = {old_dirs,old_stps,Hdiag};
            end
            paramIT.g_old = grad_cur;
        else
            % Apply user-defined preconditioner
            precondFunc = precFunc;
            precondArgs = {thetaOut,paramIT.inputs{:}};%precondArgs = {thetaOut,varargin{:}};
        end
    end

    % Solve Newton system using cg and hessian-vector products
    if isempty(HvFunc)
        % No user-supplied Hessian-vector function,
        % use automatic differentiation
        HvFun = @autoHv;
        HvArgs = {thetaOut,grad_cur,useComplex,funObj,paramIT.inputs{:}};
    else
        % Use user-supplid Hessian-vector function
        HvFun = HvFunc;
        HvArgs = {thetaOut,paramIT.inputs{:}};
    end

    if useNegCurv
        [d,cgIter,cgRes,negCurv] = conjGrad([],-grad_cur,cgForce,cgMaxIter,debug,precondFunc,precondArgs,HvFun,HvArgs);
    else
        [d,cgIter,cgRes] = conjGrad([],-grad_cur,cgForce,cgMaxIter,debug,precondFunc,precondArgs,HvFun,HvArgs);
    end

    if debug
        fprintf('newtonCG stopped on iteration %d w/ residual %.5e\n',cgIter,cgRes);
    end

    if useNegCurv
        if ~isempty(negCurv)
            %if debug
            fprintf('Using negative curvature direction\n');
            %end
            d = negCurv/norm(negCurv);
            d = d/sum(abs(grad_cur));
        end
    end
    
    paramME.old_dirs = old_dirs;
    paramME.old_stps = old_stps;
    paramME.Hdiag = Hdiag;
    paramIT.d = d;
    paramIT.funEvals = funEvals+cgIter;
end