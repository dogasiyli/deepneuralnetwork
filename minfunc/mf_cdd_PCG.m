function [paramIT, paramME] = mf_cdd_PCG(iterationID, thetaOut, paramGN, paramME, paramIT)
%assign initializations
    debug = paramGN.debug;
    tolX = paramGN.tolX;
    useMex = paramGN.useMex;
    cgUpdate = paramME.cgUpdate;
    precFunc = paramME.precFunc;
    if isfield(paramME, 's_old'), s_old = paramME.s_old; end
    grad_cur = paramIT.grad_cur;
    if isfield(paramIT, 'g_old'), g_old = paramIT.g_old; end

    % Apply preconditioner to negative gradient
    if isempty(precFunc)
        % Use L-BFGS Preconditioner
        if iterationID == 1
            old_dirs = zeros(length(grad_cur),0);
            old_stps = zeros(length(grad_cur),0);
            Hdiag = 1;
            s = -grad_cur;
        else
        %initializations for readibility
            d = paramIT.d;
            t = paramIT.t;
            corrections = paramME.corrections;
            old_dirs = paramME.old_dirs;
            old_stps = paramME.old_stps;
            Hdiag = paramME.Hdiag;            
            [old_dirs,old_stps,Hdiag] = lbfgsUpdate( grad_cur-g_old...
                                                    ,t*d...
                                                    ,corrections...
                                                    ,debug...
                                                    ,old_dirs,old_stps,Hdiag);

            if useMex
                s = lbfgsC(-grad_cur,old_dirs,old_stps,Hdiag);
            else
                s = lbfgs(-grad_cur,old_dirs,old_stps,Hdiag);
            end
        end
    else % User-supplied preconditioner
        s = precFunc(-grad_cur, thetaOut, paramGN.objFuncParamsStruct);
    end

    if iterationID == 1
        d = s;
    else
        if cgUpdate == 0% Preconditioned Fletcher-Reeves
            beta = (grad_cur'*s)/(g_old'*s_old);
        elseif cgUpdate < 3% Preconditioned Polak-Ribiere
            beta = (grad_cur'*(s-s_old))/(g_old'*s_old);
        else% Preconditioned Gilbert-Nocedal
            beta_FR = (grad_cur'*s)/(g_old'*s_old);
            beta_PR = (grad_cur'*(s-s_old))/(g_old'*s_old);
            beta = max(-beta_FR,min(beta_PR,beta_FR));
        end
        d = s + beta*d;

        if grad_cur'*d > -tolX
            if debug
                fprintf('Restarting CG\n');
            end
            beta = 0;
            d = s;
        end
    end
    paramIT.d = d;
    paramIT.g_old = grad_cur;
    paramME.Hdiag = Hdiag;
    paramME.old_stps = old_stps;
    paramME.old_dirs = old_dirs;
    paramME.s_old = s;
end