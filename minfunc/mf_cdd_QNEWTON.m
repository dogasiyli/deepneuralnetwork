function [paramIT, paramME] = mf_cdd_QNEWTON(iterationID, paramGN, paramME, paramIT)
%assign initializations
    debug = paramGN.debug;
    qnUpdate = paramME.qnUpdate;
    initialHessType = paramME.initialHessType;
    Damped = paramME.Damped;
    d = paramIT.d;
    t = paramIT.t;
    grad_cur = paramIT.grad_cur;
    g_old = paramIT.g_old;

    if iterationID == 1
        d = -grad_cur;
        assert(exist('s','var'),'s needs to be initialized - but to what?');
    else
        % Compute difference vectors
        y = grad_cur - g_old;
        s = t*d;
        if iterationID == 2
            % Make initial Hessian approximation
            if initialHessType == 0
                % Identity
                if qnUpdate <= 1
                    R = eye(length(grad_cur));
                else
                    H = eye(length(grad_cur));
                end
            else
                % Scaled Identity
                if debug
                    fprintf('Scaling Initial Hessian Approximation\n');
                end
                if qnUpdate <= 1
                    % Use Cholesky of Hessian approximation
                    R = sqrt((y'*y)/(y'*s))*eye(length(grad_cur));
                else
                    % Use Inverse of Hessian approximation
                    H = eye(length(grad_cur))*(y'*s)/(y'*y);
                end
            end
        end
        
        switch (qnUpdate)
            case 0% Use BFGS updates
                Bs = R'*(R*s);
                if Damped
                    eta = .02;
                    if y'*s < eta*s'*Bs
                        if debug
                            fprintf('Damped Update\n');
                        end
                        theta = min(max(0,((1-eta)*s'*Bs)/(s'*Bs - y'*s)),1);
                        y = theta*y + (1-theta)*Bs;
                    end
                    R = cholupdate(cholupdate(R,y/sqrt(y'*s)),Bs/sqrt(s'*Bs),'-');
                else
                    if y'*s > 1e-10
                        R = cholupdate(cholupdate(R,y/sqrt(y'*s)),Bs/sqrt(s'*Bs),'-');
                    else
                        if debug
                            fprintf('Skipping Update\n');
                        end
                    end
                end
            case 1% Perform SR1 Update if it maintains positive-definiteness
                Bs = R'*(R*s);
                ymBs = y-Bs;
                if abs(s'*ymBs) >= norm(s)*norm(ymBs)*1e-8 && (s-((R\(R'\y))))'*y > 1e-10
                    R = cholupdate(R,-ymBs/sqrt(ymBs'*s),'-');
                else
                    if debug
                        fprintf('SR1 not positive-definite, doing BFGS Update\n');
                    end
                    if Damped
                        eta = .02;
                        if y'*s < eta*s'*Bs
                            if debug
                                fprintf('Damped Update\n');
                            end
                            theta = min(max(0,((1-eta)*s'*Bs)/(s'*Bs - y'*s)),1);
                            y = theta*y + (1-theta)*Bs;
                        end
                        R = cholupdate(cholupdate(R,y/sqrt(y'*s)),Bs/sqrt(s'*Bs),'-');
                    else
                        if y'*s > 1e-10
                            R = cholupdate(cholupdate(R,y/sqrt(y'*s)),Bs/sqrt(s'*Bs),'-');
                        else
                            if debug
                                fprintf('Skipping Update\n');
                            end
                        end
                    end
                end
            case 2% Use Hoshino update
                v = sqrt(y'*H*y)*(s/(s'*y) - (H*y)/(y'*H*y));
                phi = 1/(1 + (y'*H*y)/(s'*y));
                H = H + (s*s')/(s'*y) - (H*y*y'*H)/(y'*H*y) + phi*v*v';
            case 3% Self-Scaling BFGS update
                ys = y'*s;
                Hy = H*y;
                yHy = y'*Hy;
                gamma = ys/yHy;
                v = sqrt(yHy)*(s/ys - Hy/yHy);
                H = gamma*(H - Hy*Hy'/yHy + v*v') + (s*s')/ys;
            case 4% Oren's Self-Scaling Variable Metric update
                % Oren's method
                if (s'*y)/(y'*H*y) > 1
                    phi = 1; % BFGS
                    omega = 0;
                elseif (s'*(H\s))/(s'*y) < 1
                    phi = 0; % DFP
                    omega = 1;
                else
                    phi = (s'*y)*(y'*H*y-s'*y)/((s'*(H\s))*(y'*H*y)-(s'*y)^2);
                    omega = phi;
                end

                gamma = (1-omega)*(s'*y)/(y'*H*y) + omega*(s'*(H\s))/(s'*y);
                v = sqrt(y'*H*y)*(s/(s'*y) - (H*y)/(y'*H*y));
                H = gamma*(H - (H*y*y'*H)/(y'*H*y) + phi*v*v') + (s*s')/(s'*y);                
            case 5% McCormick-Huang asymmetric update
                theta = 1;
                phi = 0;
                psi = 1;
                omega = 0;
                t1 = s*(theta*s + phi*H'*y)';
                t2 = (theta*s + phi*H'*y)'*y;
                t3 = H*y*(psi*s + omega*H'*y)';
                t4 = (psi*s + omega*H'*y)'*y;
                H = H + t1/t2 - t3/t4;                
        end

        if qnUpdate <= 1
            d = -R\(R'\grad_cur);
        else
            d = -H*grad_cur;
        end

    end
    paramIT.d = d;
    paramIT.g_old = grad_cur;
end