function [paramIT, paramME] = mf_cdd_TENSOR(iterationID, paramGN, paramME, paramIT)
    error('not corrected yet');
%paramME.input = varargin{:};
    if paramGN.numDiff
        % Compute 3rd-order Tensor Numerically
        [junk1, junk2, junk3, T] = autoTensor(thetaOut,useComplex,funObj,paramME.input{:});
    else
        % Use user-supplied 3rd-derivative Tensor
        [junk1, junk2, junk3, T] = feval(funObj, thetaOut, paramME.input{:});
    end
    options_sub.Method = 'newton';
    options_sub.Display = 'none';
    options_sub.TolX = tolX;
    options_sub.TolFun = tolFun;
    d = minFunc(@taylorModel,zeros(p,1),options_sub,f,grad_cur,H,T);

    if any(abs(d) > 1e5) || all(abs(d) < 1e-5) || grad_cur'*d > -tolX
        if debug
            fprintf('Using 2nd-Order Step\n');
        end
        [V,D] = eig((H+H')/2);
        D = diag(D);
        D = max(abs(D),max(max(abs(D)),1)*1e-12);
        d = -V*((V'*grad_cur)./D);
    else
        if debug
            fprintf('Using 3rd-Order Step\n');
        end
    end
end