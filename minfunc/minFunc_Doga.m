function [thetaOut,cost_found,exitflag,output] = minFunc_Doga(funObj, theta_init, optionalMethodParamsStruct, objFuncParamsStruct, varargin)
% minFunc(funObj,theta0,options,varargin)
% Details are at the end of this file
%% 1. Parameter-wise initializations
    global minFuncStepInfo;
    if nargin < 3
        optionalMethodParamsStruct = [];
        objFuncParamsStruct = struct;
    end
    [paramME, paramGN, paramIT, minFuncStepInfo_Created] = getMethodParams(optionalMethodParamsStruct, theta_init, objFuncParamsStruct);
    % If necessary, form numerical differentiation functions
    [funObj, paramGN, paramIT] = formNumerical(funObj, paramGN, paramIT, varargin{:});

%% 2. Evaluate Initial Point
    paramIT = evaluateInitialPoint(funObj, theta_init, paramGN, paramIT);
    % Initial output
    output_Initial(paramGN, paramIT);
    % Initial check
    [exitflag, msg, output] = initialPoint_Check(paramIT, paramGN);
    if ~isempty(exitflag), displayExitMsg(paramGN, exitflag, msg), return 
    end
    
%% 3. Loop until maxIter
% Perform up to a maximum of 'maxIter' descent steps:
    for i = 1:paramGN.maxIter
        paramIT = loopCheck_Initial(paramGN,paramIT);
        %COMPUTE DESCENT DIRECTION
        [paramIT, paramME, exitflag, msg] = computeDescentDirection(i, paramGN, paramIT, paramME);
        if ~isempty(exitflag), break, end
        %COMPUTE STEP LENGTH
        [paramIT, exitflag, msg] =  computeStepLength(i, funObj, paramGN, paramME, paramIT);
        [paramIT, exitflag, msg] = loopCheck_Final(i, paramIT, paramGN.objFuncParamsStruct, exitflag, msg);
        if ~isempty(exitflag), break, end
        checkToPause([mfilename '-iteration'],[],['minFunc_Doga(in-iteration[' num2str(i) ' of ' num2str(paramGN.maxIter) '])']);
    end
    displayExitMsg(paramGN, exitflag, msg);
    
    thetaOut = paramIT.steps.optm.theta;
    cost_found = paramIT.steps.optm.cost;
    minFuncStepInfo.Accuracy = paramIT.steps.optm.cost;
    checkToPause([mfilename '-beforeFinal'],[],'before-finalOutput');
    output = finalOutput(thetaOut, i, msg, nargout, paramGN, paramIT);
    if (minFuncStepInfo_Created)
        minFuncStepInfo = [];
    end
end

%% 1. Parameter-wise initializations
function [ paramME, paramGN, paramIT, minFuncStepInfo_Created] = getMethodParams(optionalMethodParamsStruct, theta_init, objFuncParamsStruct)
    % Get Parameters
    [verbose,verboseI,debug,doPlot,maxFunEvals,maxIter,tolFun,tolX,method,... paramGN
     corrections, ...      [PCG(4),LBFGS(5),NEWTON0(7),NEWTON(8)]
     c1, c2, ...           paramGN
     LS_init, ...          paramsIT - CSD[1]
     LS, ...               paramsIT - CSD[1]
     cgSolve,...           NEWTON0(7), NEWTON(8)
     qnUpdate, ...         QNEWTON(6)
     cgUpdate,...          CG(3),PCG(4),SCG(10)
     initialHessType,...   QNEWTON(6)
     HessianModify, ...    NEWTON(8)
     Fref, ...             paramGN
     useComplex, ...       -,NEWTON0(7)
     numDiff,...           -,NEWTON(8)
     LS_saveHessianComp,...NEWTON(8)
     DerivativeCheck,...   paramGN
     Damped, ...           LBFGS(5),QNEWTON(6)
     HvFunc, ...           NEWTON0(7)
     bbType,...            BB(2)
     cycle,...             CSD(1)
     HessianIter,...       NEWTON(8),TENSOR(9)
     outputFcn,...         paramGN
     useMex,...            paramGN - LBFGS(5), NEWTON(8)
     useNegCurv,...        NEWTON0(7)
     precFunc...           PCG(4),NEWTON0(7)
     ] = ...
     minFunc_processInputOptions(optionalMethodParamsStruct);
 
    paramIT.p = length(theta_init);
    paramIT.steps = struct;
    
    paramIT.computeHessian = method >= 8;
    paramGN.logfile = getStructField(optionalMethodParamsStruct,'logfile',[]);
        
    switch (method)
        case 0%SD = 0;
            methodString = 'Steepest_Descent';
        case 1%CSD = 1;
            methodString = 'Cyclic_Steepest_Descent';
        case 2%BB = 2;
            methodString = 'Barzilai_Borwein_Gradient';
        case 3%CG = 3;
            methodString = 'Nonlinear_Conjugate_Gradient';
        case 4%PCG = 4;
            methodString = 'Preconditioned_Nonlinear_Conjugate_Gradient';
        case 5%LBFGS = 5;
            methodString = 'Quasi_Newton_Limited_Memory_BFGS';
        case 6%QNEWTON = 6;
            methodString = 'Quasi_Newton_Hessian_Approx';
        case 7%NEWTON0 = 7;
            methodString = 'Hessian_Free_Newton';
        case 8%NEWTON = 8;
            methodString = 'Newton';
        case 9%TENSOR = 9;
            methodString = 'Tensor';
        case 10%SCG
           error('this was not a constant');
           methodString = 'Scaled_Nonlinear_Conjugate_Gradient'; %#ok<UNRCH>
        otherwise
            error('Unknown method number')
    end

    switch (methodString)
        case 'Steepest_Descent'%SD = 0;
        case 'Cyclic_Steepest_Descent'%CSD = 1;
            paramME.cycle = cycle;
        case 'Barzilai_Borwein_Gradient'%BB = 2;
            paramME.bbType = bbType;
        case 'Nonlinear_Conjugate_Gradient'%CG = 3;
            paramME.cgUpdate = cgUpdate;
        case 'Preconditioned_Nonlinear_Conjugate_Gradient'%PCG = 4;
            paramME.cgUpdate = cgUpdate;
            paramME.precFunc = precFunc;
            paramME.corrections = corrections;
        case 'Quasi_Newton_Limited_Memory_BFGS'%LBFGS = 5;
            paramME.Hdiag = 1;
            paramME.old_dirs = zeros(length(theta_init),0);
            paramME.old_stps = zeros(length(theta_init),0);            
            paramME.Damped = Damped;
            paramME.corrections = corrections;
        case 'Quasi_Newton_Hessian_Approx'%QNEWTON = 6;
            paramME.initialHessType = initialHessType;
            paramME.qnUpdate = qnUpdate;
            paramME.Damped = Damped;
        case 'Hessian_Free_Newton'%NEWTON0 = 7;
            paramME.HvFunc = HvFunc;
           %paramME.useComplex = useComplex;
            paramME.useNegCurv = useNegCurv;
            paramME.precFunc = precFunc;
            paramME.corrections = corrections;
            paramME.cgSolve = cgSolve;
        case 'Newton'%NEWTON = 8;
            paramME.corrections = corrections;
            paramME.cgSolve = cgSolve;
            paramME.HessianModify = HessianModify;
            paramME.LS_saveHessianComp = LS_saveHessianComp;
           %paramME.numDiff = numDiff;
            paramME.HessianIter = HessianIter;
        case 'Tensor'%TENSOR = 9;
            paramME.HessianIter = HessianIter;
        case 'Scaled_Nonlinear_Conjugate_Gradient'%SCG
            paramME.cgUpdate = cgUpdate; %#ok<STRNU>
            error('this was not a constant');
        otherwise
            error('Unknown method')
    end
    
    paramGN.verbose = verbose;
    paramGN.verboseI = verboseI;
    paramGN.debug = debug;
    paramGN.doPlot = doPlot;
    paramGN.maxFunEvals = maxFunEvals;
    paramGN.maxIter = maxIter;
    paramGN.tolFun = tolFun;
    paramGN.tolX = tolX;
    paramGN.method = method;
    paramGN.c1 = c1;
    paramGN.c2 = c2;
    paramGN.useMex = useMex;
    paramGN.DerivativeCheck = DerivativeCheck;
    paramGN.Fref = Fref;
    paramGN.outputFcn = outputFcn;
    paramGN.methodString = methodString;
    paramGN.useComplex = useComplex;
    paramGN.numDiff = numDiff;
    if ~isfield(objFuncParamsStruct,'calculateGradient')
        objFuncParamsStruct.calculateGradient = true;
    end
    if ~isfield(objFuncParamsStruct,'useAccuracyAsCost')
        objFuncParamsStruct.useAccuracyAsCost = false;
    end
    paramGN.objFuncParamsStruct = objFuncParamsStruct;
    
    paramIT.funEvals = 1;
    paramIT.LS = LS;
    paramIT.LS_init = LS_init;
    paramIT.resultInfo = [];%iteration, funeval, t_cur, gdt, f,acc,type

    global minFuncStepInfo;
    if isempty(minFuncStepInfo) || ~isstruct(minFuncStepInfo) || ~isfield(minFuncStepInfo,'Iteration')
        minFuncStepInfo = []; %#ok<NASGU>
        minFuncStepInfo = struct;
        minFuncStepInfo.Iteration = 1;
        minFuncStepInfo.FunEvals = 1;
        minFuncStepInfo_Created = true;
    else
        minFuncStepInfo_Created = false;
    end
    try
        if isfield(objFuncParamsStruct,'dataStruct') && isfield(objFuncParamsStruct.dataStruct,'batchID_current') 
            minFuncStepInfo.BatchID = objFuncParamsStruct.dataStruct.batchID_current;
        end
    catch
    end
end
function [funObj, paramGN, paramIT] = formNumerical(funObj, paramGN, paramIT, varargin)
    paramIT.input  = varargin;%assign
    paramIT.funEvalMultiplier = 1;
    if paramGN.numDiff && ~strcmp(paramGN.methodString,'Tensor')
        paramIT.input(3:end+2) = paramIT.input(1:end);
        paramIT.input{1} = paramGN.useComplex;
        paramIT.input{2} = funObj;
        warning('This part has to be checked before being used - paused in formNumerical in minFunc_Doga');
        disp('This part has to be checked before being used - paused in formNumerical in minFunc_Doga');
        pause;
        if ~strcmp(paramGN.methodString,'Newton')
            funObj = @autoGrad;
        else
            funObj = @autoHess;
        end
        if strcmp(paramGN.methodString,'Hessian_Free_Newton') && paramGN.useComplex == 1
            if paramGN.debug
                fprintf('Turning off the use of complex differentials\n');
            end
            paramGN.useComplex = 0;
        end
        %assign  - paramIT.funEvalMultiplier
        if paramGN.useComplex==1
            paramIT.funEvalMultiplier = paramIT.p;
            if paramGN.debug, fprintf('Using complex differentials for gradient computation\n'); end
        else%if paramGN.useComplex==0
            paramIT.funEvalMultiplier = paramIT.p+1;
            if paramGN.debug, fprintf('Using finite differences for gradient computation\n'); end
        end
    end
end
  
%% 2. Evaluate Initial Point
function paramIT = evaluateInitialPoint(funObj, theta_init, paramGN, paramIT)
    global minFuncStepInfo;
    minFuncStepInfo.StepType = 'MainIteration';
    minFuncStepInfo.StepLength = 0;
    
    objFuncParamsStruct = paramGN.objFuncParamsStruct;
    
    objFuncParamsStruct.calculateGradient = true;
    [cost_Init,grad_init,optionalObjFuncOutputs] = feval(funObj, theta_init, objFuncParamsStruct);
    if paramIT.computeHessian && isfield(optionalObjFuncOutputs,'hessianParam')
        paramIT.H = optionalObjFuncOutputs.hessianParam;
    end
    
    if paramGN.DerivativeCheck
        if paramGN.numDiff
            fprintf('Can not do derivative checking when numDiff is 1\n');
        end
        % Check provided gradient/hessian function using numerical derivatives
        fprintf('Checking Gradient:\n');
        %[f2,g2] = autoGrad(thetaOut, useComplex, funObj, paramIT.input{:});
         [ ~,g2] = autoGrad(theta_init, paramGN.useComplex, funObj, objFuncParamsStruct);

        fprintf('Max difference between user and numerical gradient: %f\n',max(abs(grad_init-g2)));
        if max(abs(grad_init-g2)) > 1e-4
            fprintf('User NumDif:\n');
            diff = abs(grad_init-g2);
            disp('Grda_Cur Grad_New Abs_Grad_Diff');
            disp(mat2str([grad_init g2 diff]));
            pause;
        end

        if paramIT.computeHessian
            fprintf('Check Hessian:\n');
            %[f2,g2,H2] = autoHess(thetaOut,useComplex,funObj,paramIT.input{:});
            [  ~, ~,H2] = autoHess(theta_init,paramGN.useComplex,funObj,paramGN.objFuncParamsStruct);
            fprintf('Max difference between user and numerical hessian: %f\n',max(abs(paramIT.H(:)-H2(:))));
            if max(abs(paramIT.H(:)-H2(:))) > 1e-4
                disp('Hessian_Cur');
                disp(mat2str(paramIT.H));
                disp('Hessian_New');
                disp(mat2str(H2));
                disp('Hessian_Diff');
                diff = abs(paramIT.H-H2);
                disp(mat2str(H2),diff);
                pause;
            end
        end
    end
    if isfield(optionalObjFuncOutputs,'accuracy')
        acc = optionalObjFuncOutputs.accuracy;
    else
        acc = [];
    end
    paramIT = assignParamITsVal(paramIT,'zero',cost_Init,grad_init, 0,zeros(paramIT.p,1),[],acc,theta_init);
    paramIT = assignParamITsVal(paramIT,'curr',[],[], 1,[],[],[],[]);
    paramIT = assignParamITsVal(paramIT,'optm',cost_Init,grad_init, 0,[],[],acc,theta_init);
end
function output_Initial(paramGN, paramIT)
%    global minFuncStepInfo;
% Output Log
    if paramGN.verboseI
        %if isfield(minFuncStepInfo,'AdditionalLogTitle'),fprintf('%s\n',minFuncStepInfo.AdditionalLogTitle); end
        fprintf('% 4s% 4s% 10s% 5s% 7s% 15s% 15s% 15s% 10s %s\n','eID','bID','Timestamp', 'iter','feval','StepLength','FunctionVal','OptCond','Accuracy','Precision-Recall');
    end

    if paramGN.logfile
        fid = fopen(paramGN.logfile, 'a');
        if (fid > 0)
            %if isfield(minFuncStepInfo,'AdditionalLogTitle'),fprintf(fid, '%s\n',minFuncStepInfo.AdditionalLogTitle);end
            fprintf(fid, '% 4s% 4s% 10s% 5s% 7s% 15s% 15s% 15s% 10s %s\n','eID','bID','Timestamp', 'iter','feval','StepLength','FunctionVal','OptCond','Accuracy','Precision-Recall');
            fclose(fid);
        end
    end

% Output Function
    if ~isempty(paramGN.outputFcn)
        theta_init = paramIT.steps.zero.theta;
        grad_init = paramIT.steps.zero.g;
        cost_init = paramIT.steps.zero.cost;
        callOutput(paramGN.outputFcn, theta_init, 'init', 0, paramIT.funEvals, cost_init, [], [], grad_init, [], sum(abs(grad_init)), paramGN.objFuncParamsStruct);
    end
end
function [exitflag, msg, output] = initialPoint_Check(paramIT, paramGN)
    [exitflag, msg] = setExitFlagAndMessage([]);
    output = []; 
    %Check optimality of initial point
    optCond = sum(abs(paramIT.steps.zero.g));
    if  optCond <= paramGN.tolFun
%         if isempty(exitflag)
%         	msg = '';
%         else
%             msg = ['1.' msg ' and 2.'];
%         end
%         [exitflag, msgNew] = setExitFlagAndMessage(1, ['Optimality Condition(' num2str(optCond,'%10.5e') ') < TolFun' num2str(paramGN.tolFun,'%10.5e')], 'initialPoint_Check', 'sum(abs(gradient_at_zero)) <= paramGN.tolFun');
%         msg = [msg msgNew];
        [exitflag, msg] = setExitFlagAndMessage(1, ['Optimality Condition(' num2str(optCond,'%10.5e') ') < TolFun' num2str(paramGN.tolFun,'%10.5e')], 'initialPoint_Check', 'sum(abs(gradient_at_zero)) <= paramGN.tolFun');
        output = struct( 'iterations',0 ...
                        ,'funcCount',1 ...
                        ,'algorithm',paramGN.methodString...
                        ,'firstorderopt',optCond ...
                        ,'message',msg...
                        ,'funevals',paramIT.funEvals...
                        ,'cost',paramIT.steps.zero.cost);
    end
end

%% 3. Loop until maxIter
function paramIT = loopCheck_Initial(paramGN,paramIT)
    global minFuncStepInfo;
    if isfield(minFuncStepInfo,'Accuracy')
        paramIT.accArr = zeros(1, 1+paramGN.maxIter);
        paramIT.accArr(1,1) = minFuncStepInfo.Accuracy;
    end
    paramIT.costArr = zeros(1, 1+paramGN.maxIter);
    if ~isfield(minFuncStepInfo,'Iteration')
        minFuncStepInfo.Iteration = 1;
    end
    minFuncStepInfo.StepType = 'MainIteration';
    minFuncStepInfo.NewIteration = true;
end
function [paramIT, paramME, exitflag, msg] = computeDescentDirection(i, paramGN, paramIT, paramME)
    [exitflag, msg] = setExitFlagAndMessage([]);
    switch paramGN.methodString
        case 'Steepest_Descent' %DONE
            [paramIT] = mf_cdd_SD(grad_cur);
        case 'Cyclic_Steepest_Descent' %DONE
            [paramIT] = mf_cdd_CSD(i, paramIT, paramME);
        case 'Barzilai_Borwein_Gradient' % Steepest Descent with Barzilai and Borwein Step Length
            [paramIT, paramME] = mf_cdd_BB(i, paramIT.steps.optm.cost, paramIT, paramME);
        case 'Nonlinear_Conjugate_Gradient'
            [paramIT, paramME] = mf_cdd_CG(i, paramGN, paramME, paramIT);
        case 'Preconditioned_Nonlinear_Conjugate_Gradient'
            [paramIT, paramME] = mf_cdd_PCG(i, paramGN, paramME, paramIT);
        case 'Quasi_Newton_Limited_Memory_BFGS' % L-BFGS
            [paramIT, paramME] = mf_cdd_LBGFS(i, paramGN, paramME, paramIT);
        case 'Quasi_Newton_Hessian_Approx'
            [paramIT, paramME] = mf_cdd_QNEWTON(i, paramGN, paramME, paramIT);
        case 'Hessian_Free_Newton'
            [paramIT, paramME] = mf_cdd_NEWTON0(i, paramGN, paramME, paramIT);
        case 'Newton'
            [paramIT, paramME] = mf_cdd_NEWTON(i, paramGN, paramME, paramIT);
        case 'Tensor' % Tensor Method
            [paramIT, paramME] = mf_cdd_TENSOR(i, paramGN, paramME, paramIT);
    end
    if ~isLegal(paramIT.steps.curr.d)
        [exitflag, msg] = setExitFlagAndMessage(3, 'Step direction is illegal!', 'computeDescentDirection', '~isLegal(paramIT.d)');
    end
end
function [paramIT, exitflag, msg] =  computeStepLength(iterationID, funObj, paramGN, paramME, paramIT)
    global minFuncStepInfo;
    if iterationID==1
        %before coming to here we only evaluated the function value at
        %stepLength=0
        cost_init = paramIT.steps.zero.cost;%the final cost that was found is the initial cost of our search
        grad_init = paramIT.steps.zero.g;%the final grad that was found is the initial grad of our search
        t_init = 1;%use 1 as the initial step length
        d_init = paramIT.steps.zero.d;
        acc_init = paramIT.steps.zero.acc;
        theta_init = paramIT.steps.zero.theta;
        outputInfo(0, cost_init, theta_init, paramGN, paramIT, minFuncStepInfo);
    else
        %we have been using this function at least once before.
        %hence we performed a line search and found curr params
        %if the curr params were better than optm params we can go on using
        %curr params, else the previous params were the best ones hence we
        %have to stop trying
        cost_init = paramIT.steps.curr.cost;%the final cost that was found is the initial cost of our search
        grad_init = paramIT.steps.curr.g;%the final grad that was found is the initial grad of our search
        t_init = paramIT.steps.curr.t;
        d_init = paramIT.steps.curr.d;
        acc_init = paramIT.steps.curr.acc;
        theta_init = paramIT.steps.curr.theta;
    end
    %theta_init is the best we could do until now with this dataset and
    %params
    
    % Directional Derivative
    [gtd_init, exitflag, msg] = checkDirectionalDerivative(grad_init, d_init, paramGN.tolX);
    if ~isempty(exitflag), return, end
    paramIT.steps.curr.gtd = gtd_init;
    if iterationID==1
        paramIT.steps.zero.gtd = gtd_init;
        %WRITE_OUTPUT_EXCEL - {0_INITIAL_POINT}
        excelInfo = prepareExcelInfo(paramIT, 0);
        %excelInfo = [minFuncStepInfo.Iteration paramIT.funEvals paramIT.steps.zero.t paramIT.steps.zero.gtd paramIT.steps.zero.cost paramIT.steps.zero.acc 0];
        paramIT.resultInfo = [paramIT.resultInfo;excelInfo];
        paramIT.steps.optm.gtd = gtd_init;
        %[exitflag, msgNew] = setExitFlagAndMessage(1, ['Optimality Condition(' num2str(optCond,'%10.5e') ') < TolFun' num2str(paramGN.tolFun,'%10.5e')], 'initialPoint_Check', 'sum(abs(gradient_at_zero)) <= paramGN.tolFun');
    elseif iterationID>1
        %if I am here it means that the new iteration is succesful hence I can
        %set the values in curr to optm
        %otherwise optm params will be either as in zero vals or the last
        %successfull curr vals
        
        %WRITE_OUTPUT_EXCEL - {1_BETTER_POINT}
        paramIT = assignParamITsVal(paramIT,'optm',paramIT.steps.curr.cost,paramIT.steps.curr.g, paramIT.steps.curr.t, paramIT.steps.curr.d, paramIT.steps.curr.gtd, paramIT.steps.curr.acc, paramIT.steps.curr.theta);    
        excelInfo = prepareExcelInfo(paramIT, 1);
        %excelInfo = [minFuncStepInfo.Iteration paramIT.funEvals paramIT.steps.optm.t paramIT.steps.optm.gtd paramIT.steps.optm.cost paramIT.steps.optm.acc 1];
        paramIT.resultInfo = [paramIT.resultInfo;excelInfo];
    end
    
    % Select Initial Guess
    %following sets (paramIT.steps.curr.t)
    paramIT = selectInitialGuess(iterationID, cost_init, grad_init, t_init, d_init, gtd_init, theta_init, funObj, paramGN, paramIT, paramME);

    % Compute reference fr if using non-monotone objective
    paramIT = compute_fr(iterationID, cost_init, paramIT, paramGN);

    paramIT.computeHessian = updateHessianParam(paramGN.methodString, paramME, iterationID);

    % Line Search
    t_init = paramIT.steps.curr.t;
    [paramIT, exitflag, msg] = performLineSearch(theta_init, cost_init, grad_init, t_init, d_init, gtd_init, paramGN, paramME, paramIT, funObj);
    if iterationID==1
        paramIT = assignParamITsVal(paramIT,'prev',cost_init,grad_init, 0, d_init, gtd_init, acc_init, theta_init);
    else
        paramIT = assignParamITsVal(paramIT,'prev',cost_init,grad_init, t_init, d_init, gtd_init, acc_init, theta_init);
    end
    
    if ~isempty(exitflag), return, end

    % Output iteration information
    outputInfo(iterationID, paramIT.steps.curr.cost, paramIT.steps.curr.theta, paramGN, paramIT,minFuncStepInfo);
    
    % Check Optimality Condition
    [exitflag, msg] = checkOptimalityCondition(paramIT.steps.curr.g, paramGN.tolFun);
    if ~isempty(exitflag), return, end
    % Check for lack of progress
    [exitflag, msg] = checkLackOfProgress(paramIT.steps.curr.t, paramIT.steps.curr.d, paramGN.tolX, paramIT.steps.curr.cost, cost_init);
    if ~isempty(exitflag), return, end
    % Check for going over iteration/evaluation limit
    [exitflag, msg] = checkLimits(paramIT.funEvals, paramIT.funEvalMultiplier, paramGN.maxFunEvals, iterationID, paramGN.maxIter);
    if ~isempty(exitflag), return, end    
end
function excelInfo = prepareExcelInfo(paramIT, insertionID)
    global minFuncStepInfo;
    if isfield(minFuncStepInfo,'Iteration') && ~isempty(minFuncStepInfo.Iteration)
        iterID = minFuncStepInfo.Iteration;
    else
        iterID = 0;
    end
    if isfield(paramIT,'funEvals') && ~isempty(paramIT.funEvals)
        funEvals = paramIT.funEvals;
    else
        funEvals = 0;
    end
    if ~isempty(paramIT.steps.optm.t)
        t = paramIT.steps.optm.t;
    else
        t = 0;
    end
    if ~isempty(paramIT.steps.optm.gtd)
        gtd = paramIT.steps.optm.gtd;
    else
        gtd = -0;
    end
    if ~isempty(paramIT.steps.optm.cost)
        cost = paramIT.steps.optm.cost;
    else
        cost = 0;
    end
    if ~isempty(paramIT.steps.optm.acc)
        acc = paramIT.steps.optm.acc;
    else
        acc = 0;
    end
    excelInfo = [iterID funEvals t gtd cost acc insertionID];
end
function [paramIT, exitflag, msg] = performLineSearch(theta_init, cost_init, grad_init, t_init, d_init, gtd_init, paramGN, paramME, paramIT, funObj)
%When we come here it is obvious that we have an initial point :
%with step_length, gradient(g vs d??), cost
%this function tries to find a better point by appliying some stepLength to
%the gradient calculated

    [exitflag, msg] = setExitFlagAndMessage([]);
    
    debug = paramGN.debug;%used
    doPlot = paramGN.doPlot;%used
    tolX = paramGN.tolX;%used
    c1 = paramGN.c1;%used
    c2 = paramGN.c2;%used
    maxFunEvals = paramGN.maxFunEvals;%used

    LS = paramIT.LS;%used
    funEvals = paramIT.funEvals;%used, assigned, updated
    fr = paramIT.fr;%used
    computeHessian = paramIT.computeHessian;%used
    
    p = paramIT.p;%used

    if LS < 3 % Use Armijo Bactracking
        % Perform Backtracking line search
        if computeHessian
            LS_saveHessianComp = isfield(paramME, 'LS_saveHessianComp') && paramME.LS_saveHessianComp;
            [t_cur,theta_cur,cost_cur,grad_cur,LSfunEvals,paramIT.H] = ...
                    ArmijoBacktrack( theta_init,t_init,d_init,cost_init...
                                    ,fr,grad_init,gtd_init...
                                    ,c1,LS,tolX,debug,doPlot,LS_saveHessianComp...
                                    ,funObj...
                                    ,paramGN.objFuncParamsStruct);
        else
            [t_cur,theta_cur,cost_cur,grad_cur,LSfunEvals] = ...
                    ArmijoBacktrack( theta_init,t_init,d_init,cost_init ...
                                    ,fr,grad_init,gtd_init ...
                                    ,c1,LS,tolX,debug,doPlot,1 ...
                                    ,funObj...
                                    ,paramGN.objFuncParamsStruct);
        end
        funEvals = funEvals + LSfunEvals;
    elseif LS < 6
        % Find Point satisfying Wolfe
        
        wolfLineParams = setWolfLineParams(theta_init,t_init,d_init,cost_init,grad_init,gtd_init,c1,c2,LS,maxFunEvals-funEvals,tolX,debug,doPlot,computeHessian~=true,funObj,paramGN.objFuncParamsStruct);
        if computeHessian
            %[t_new,f_new,g_new,LSfunEvals,H] = WolfeLineSearch(thetaOut,t,d,f,grad_cur,gtd,c1,c2,LS,maxFunEvals-funEvals,tolX,debug,doPlot,LS_saveHessianComp,funObj,paramIT.input{:});
            [t_cur,cost_cur,grad_cur,LSfunEvals,paramIT.H] = WolfeLineSearch_Doga(wolfLineParams,paramGN.objFuncParamsStruct);
        else
            %[t_new,f_new,g_new,LSfunEvals] = WolfeLineSearch(thetaOut,t,d,f,grad_cur,gtd,c1,c2,LS,maxFunEvals-funEvals,tolX,debug,doPlot,1,funObj,paramIT.input{:});
            if (paramGN.objFuncParamsStruct.useAccuracyAsCost)
                paramGN.objFuncParamsStruct.calculateGradient = false;
            end
            [t_cur,cost_cur,grad_cur,LSfunEvals] = WolfeLineSearch_Doga(wolfLineParams,paramGN.objFuncParamsStruct);
        end
        funEvals = funEvals + LSfunEvals;
        theta_cur = theta_init + t_cur*d_init;
    else
        % Use Matlab optim toolbox line search
%       [t, f_new, fPrime_new, g_new, LSexitFlag, LSiter]=...
        [t_cur, cost_cur,          ~, grad_cur,          ~, LSiter]=...
            lineSearch({'fungrad',[],funObj},theta_init,p,1,p,d_init,cost_init,gtd_init,t_init,c1,c2,-inf,maxFunEvals-funEvals,...
            tolX,[],[],[],paramGN.objFuncParamsStruct);
        funEvals = funEvals + LSiter;
        if isempty(t_cur)
            [exitflag, msg] = setExitFlagAndMessage(-2,'Matlab LineSearch failed', 'performLineSearch','isempty(stepLen_found)');
            return
        end

        if paramGN.method >= NEWTON
           %[f_new,g_new,H] = funObj(thetaOut + t_new*d,paramIT.input{:});
            [cost_cur,grad_cur] = funObj(theta_init + t_cur*d_init,paramGN.objFuncParamsStruct);
            funEvals = funEvals + 1;
        end
        theta_cur = theta_init + t_cur*d_init;
    end
    
    paramIT.funEvals = funEvals;
    
    global minFuncStepInfo;
    if isfield(minFuncStepInfo,'Accuracy')
        acc_cur = minFuncStepInfo.Accuracy;
    else
        acc_cur = 0;
    end
    d_cur = NaN;%will be calculated in computeDescentDirection
    gtd_cur = NaN;%will be calculated in computeStepLength
    paramIT = assignParamITsVal(paramIT,'curr',cost_cur,grad_cur, t_cur, d_cur,gtd_cur,acc_cur,theta_cur);
end
function [paramIT, exitflag, msg] = loopCheck_Final(iterationID, paramIT, objFuncParamsStruct, exitflag, msg)
    global minFuncStepInfo;
    if isfield(minFuncStepInfo,'Accuracy')
        paramIT.accArr(iterationID+1) = minFuncStepInfo.Accuracy;        
    end
    if paramIT.steps.optm.cost>paramIT.steps.curr.cost
        paramIT = assignParamITsVal(paramIT,'optm',paramIT.steps.curr.cost,paramIT.steps.curr.g, paramIT.steps.curr.t, paramIT.steps.curr.d, paramIT.steps.curr.gtd, paramIT.steps.curr.acc, paramIT.steps.curr.theta);
    end
    minFuncStepInfo.Iteration = minFuncStepInfo.Iteration + 1;
    
    if ~isempty(exitflag)
        return
    elseif isfield(minFuncStepInfo,'Accuracy')
        if (iterationID>1)
            if isfield(objFuncParamsStruct,'useAccuracyAsCost') && objFuncParamsStruct.useAccuracyAsCost
                accDiff = paramIT.accArr(iterationID+1)-paramIT.accArr(iterationID);     
            else
                last_10_Acc_mean = mean(paramIT.accArr(max(1,iterationID-9):iterationID+1));
                accDiff = abs(paramIT.accArr(iterationID+1)-last_10_Acc_mean);                   
            end
            minFuncStepInfo.AccuracyDif = accDiff;
            if (accDiff<0.00001)
                [exitflag, msg] = setExitFlagAndMessage(2,['Accuracy difference(' num2str(accDiff) ') below 0.00001'], 'loopCheck_Final','accDiff<0.00001');
            end
        end
    end 
end

function [paramIT] = selectInitialGuess(iterationID, cost_init, grad_init, t_init, d_init, gtd_init, theta_init, funObj,  paramGN, paramIT, paramME)
    %initializations for readibility
    if isfield(paramIT.steps, 'prev')
        gtd_prev = paramIT.steps.prev.gtd;
        cost_prev = paramIT.steps.prev.cost;
    end
    
    LS_init = paramIT.LS_init;
    funEvals = paramIT.funEvals;

    if iterationID == 1
        switch paramGN.methodString
            case {'Steepest_Descent', 'Cyclic_Steepest_Descent', 'Barzilai_Borwein_Gradient', 'Nonlinear_Conjugate_Gradient'...
                  ,'Preconditioned_Nonlinear_Conjugate_Gradient', 'Quasi_Newton_Limited_Memory_BFGS'...
                  ,'Quasi_Newton_Hessian_Approx'}
                t_cur = min(1,1/sum(abs(grad_init)));
            case{'Hessian_Free_Newton','Newton','Tensor'}
                t_cur = 1;
        end
    else
        if LS_init == 0
            % Newton step
            t_cur = 1;
        elseif LS_init == 1
            % Close to previous step length
            t_cur = t_init*min(2,(gtd_prev)/(gtd_init));
        elseif LS_init == 2
            % Quadratic Initialization based on {f,grad_cur} and previous f
            t_cur = min(1,2*(cost_init-cost_prev)/(gtd_init));
        elseif LS_init == 3
            % Double previous step length
            t_cur = min(1,t_init*2);
        elseif LS_init == 4
            % Scaled step length if possible
            if isempty(paramME.HvFunc)
                % No user-supplied Hessian-vector function,
                % use automatic differentiation
                dHd = d_init'*autoHv(d_init,theta_init,grad_init,0,funObj,paramME.input{:});
            else
                % Use user-supplid Hessian-vector function
                dHd = d_init'*paramME.HvFunc(d_init, theta_init, paramME.input{:});
            end
            funEvals = funEvals + 1;
            if dHd > 0
                t_cur = -gtd_init/(dHd);
            else
                t_cur = min(1,2*(cost_init-cost_prev)/(gtd_init));
            end
        end

        if t_cur <= 0
            t_cur = 1;
        end
    end
    
    paramIT.funEvals = funEvals;
    %the following values has to be run by funObj to be complete
    paramIT.steps.curr.t = t_cur;
end
function [paramIT] = compute_fr(iterationID, cost_init, paramIT, paramGN)
   if paramGN.Fref == 1
        paramIT.fr = cost_init;
    else
        if iterationID == 1
            paramIT.old_fvals = repmat(-inf,[paramGN.Fref 1]);
        end
        if iterationID <= paramGN.Fref
            paramIT.old_fvals(iterationID) = cost_init;
        else
            paramIT.old_fvals = [paramIT.old_fvals(2:end);cost_init];
        end
        paramIT.fr = max(paramIT.old_fvals);
   end
end
function  computeHessian = updateHessianParam(methodString, paramME, i)
    switch methodString
        case {'Steepest_Descent', 'Cyclic_Steepest_Descent', 'Barzilai_Borwein_Gradient', 'Nonlinear_Conjugate_Gradient'...
              ,'Preconditioned_Nonlinear_Conjugate_Gradient', 'Quasi_Newton_Limited_Memory_BFGS'...
              ,'Quasi_Newton_Hessian_Approx','Hessian_Free_Newton'}
            computeHessian = 0;
        case{'Newton','Tensor'}
            if paramME.HessianIter == 1
                computeHessian = 1;
            elseif i > 1 && mod(i-1,paramME.HessianIter) == 0
                computeHessian = 1;
            end
        otherwise
            error('Unknown method')
    end
end

%% 4. Control functions
function [exitflag, msg] = checkLimits(funEvals, funEvalMultiplier, maxFunEvals, i, maxIter)
    if funEvals*funEvalMultiplier > maxFunEvals
        exitflag = 0;
        msg = 'Exceeded Maximum Number of Function Evaluations';
    elseif i == maxIter
        exitflag = 0;
        msg='Exceeded Maximum Number of Iterations';
    else
        exitflag = [];
        msg = [];
    end
end
function [exitflag, msg] = checkLackOfProgress(t, d, tolX, f, f_old)    
    if sum(abs(t*d)) <= tolX
        exitflag=2;
        msg = 'Step Size below TolX';
    elseif ~isempty(f_old) && abs(f-f_old) < tolX
        exitflag=2;
        msg = 'Function Value changing by less than TolX';
    else
        exitflag = [];
        msg = [];
    end
end
function [exitflag, msg] = checkOptimalityCondition(grad_cur, tolFun)  
    optCond = sum(abs(grad_cur));
    if  optCond <= tolFun
        [exitflag, msg] = setExitFlagAndMessage(1, ['Optimality Condition(' num2str(optCond,'%10.5e') ') < TolFun' num2str(tolFun,'%10.5e')], 'checkOptimalityCondition', 'sum(abs(grad_cur)) <= tolFun');
    else
        [exitflag, msg] = setExitFlagAndMessage([]);
    end
end
function [gtd, exitflag, msg] = checkDirectionalDerivative(grad_cur,d,tolX)
    gtd = grad_cur'*d;
    % Check that progress can be made along direction
    if gtd > -tolX
        [exitflag, msg] = setExitFlagAndMessage(2, ['Directional Derivative(' num2str(gtd,'%10.5e') ') < TolX(' num2str(-tolX,'%10.5e') ')'], 'checkDirectionalDerivative', 'gtd > -tolX');
    else
        [exitflag, msg] = setExitFlagAndMessage([]);
    end
end
function outputInfo(i, f, thetaOut, paramGN, paramIT, minFuncStepInfo)
    verboseI = paramGN.verboseI;%use
    logfile = paramGN.logfile;
    outputFcn = paramGN.outputFcn;
    
    t_cur = minFuncStepInfo.StepLength;%paramIT.steps.curr.t;
    grad_cur = paramIT.steps.curr.g;
    funEvals = paramIT.funEvals;
    funEvalMultiplier = paramIT.funEvalMultiplier;
    

    c = clock; % [year month day hour minute seconds]
    txtTimeStamp = sprintf('%02d:%02d:%02d ',c(4),c(5),round(c(6)));
    try % isfield(minFuncStepInfo,'ConfMat')
        ConfMat = minFuncStepInfo.ConfMat;
        confMatStats = calcConfusionStatistics(ConfMat,1:size(ConfMat,1));
        precStr = mat2str([confMatStats.Precision],3);
        recallStr = mat2str([confMatStats.Sensitivity],3);
        confMatStatsInfo = [' Prc(' precStr ')-Rec(' recallStr ')'];
    catch %else
        confMatStatsInfo = '';
    end
    if isfield(minFuncStepInfo,'Accuracy')
        accInfo = minFuncStepInfo.Accuracy;
    else
        accInfo = 0;
    end
    if isfield(minFuncStepInfo,'EpochID')
        epochID = minFuncStepInfo.EpochID;
    else
        epochID = 0;
    end
    if isfield(minFuncStepInfo,'BatchID')
        batchID = minFuncStepInfo.BatchID;
    else
        batchID = 0;
    end
    if verboseI
        fprintf('%4d%4d% 10s%5d%7d%15.5e%15.5e%15.5e%10.4f%s\n',epochID,batchID,txtTimeStamp,i,funEvals*funEvalMultiplier,t_cur,f,sum(abs(grad_cur)),accInfo,confMatStatsInfo);
    end
    if logfile
        fid = fopen(logfile, 'a');
        if (fid > 0)
            fprintf(fid, '%4d%4d% 10s%5d%7d%15.5e%15.5e%15.5e%10.4f%s\n',epochID,batchID,txtTimeStamp,i,funEvals*funEvalMultiplier,t_cur,f,sum(abs(grad_cur)),accInfo,confMatStatsInfo);
            fclose(fid);
        end
    end
    globalOutputStruct('updateMinFuncLog',[(c(1)*10^10+c(2)*10^8+c(3)*10^6+c(4)*10^4+c(5)*10^2+floor(c(6))),i,funEvals*funEvalMultiplier,t_cur,f,sum(abs(grad_cur)),accInfo]);

    % Output Function
    if ~isempty(outputFcn)
        gtd = paramIT.steps.curr.gtd;
        d = paramIT.steps.curr.d;
        callOutput(outputFcn,thetaOut,'iter',i,funEvals,f,t_cur,gtd,grad_cur,d,sum(abs(grad_cur)),paramGN.objFuncParamsStruct);
    end
end
function output = finalOutput(thetaOut, i, msg, returnParamCount, paramGN, paramIT)
    excelInfo = prepareExcelInfo(paramIT, 2);
    %excelInfo = [minFuncStepInfo.Iteration paramIT.funEvals paramIT.steps.optm.t paramIT.steps.optm.gtd paramIT.steps.optm.cost paramIT.steps.optm.acc 2];
    resultsInfo = [paramIT.resultInfo;excelInfo];
    if paramGN.verbose
        fprintf('%s\n',msg);
    end
    if returnParamCount > 3
        output = struct( 'iterations',i...
                        ,'funcCount',paramIT.funEvals*paramIT.funEvalMultiplier...
                        ,'algorithm',paramGN.methodString...
                        ,'firstorderopt',sum(abs(paramIT.steps.optm.g))...
                        ,'message',msg...
                        ,'funevals',paramIT.funEvals...
                        ,'cost',paramIT.steps.optm.cost...
                        ,'resultsInfo',resultsInfo);
    else
        output = [];
    end
    
% Output Function
    if ~isempty(paramGN.outputFcn)
        callOutput(paramGN.outputFcn,thetaOut,'done',i,paramIT.funEvals,f,paramIT.t,paramIT.gtd,paramIT.grad_cur,paramIT.d,sum(abs(paramIT.grad_cur)),paramGN.objFuncParamsStruct);
    end
end
function displayExitMsg(paramGN, exitflag, msg)
    if paramGN.verbose && ~isempty(exitflag) &&  ~isempty(msg)
        fprintf('exit_flag : %d\n exit_msg : %s\n',exitflag,msg);
    end
end

%% 5. parameter assigning functions
function wolfLineParams = setWolfLineParams(theta_init,stepLen_init,desc_dir,cost_init,grad_init,grad_dir_der,suf_dec,curv_param,interpType,iter_max,tolX,disp_debug,doPlot,saveHessianComp,funObj,varargin)
    wolfLineParams.x_init = theta_init;
    wolfLineParams.t_init = stepLen_init;%initial step length to use
    wolfLineParams.desc_dir = desc_dir;%: descent direction (oldName : "d")
    wolfLineParams.cost_init = cost_init;
    wolfLineParams.grad_init = grad_init;
    wolfLineParams.grad_dir_der = grad_dir_der;
    wolfLineParams.suf_dec = suf_dec;
    wolfLineParams.curv_param = curv_param;
    wolfLineParams.disp_debug = disp_debug;
    wolfLineParams.interpType = interpType;
    wolfLineParams.tolX = tolX;
    wolfLineParams.doPlot = doPlot;
    wolfLineParams.funObj = funObj;
    wolfLineParams.saveHessianComp = saveHessianComp;
    wolfLineParams.iter_max = iter_max;      
    wolfLineParams.vararginStruct = varargin{:};
end
function [exitflag, msg] = setExitFlagAndMessage(exitflag, msg, mainFuncName, conditionStr)
    if nargin==1
        exitflag = [];
        msg = [];
        return
    end
    msg = ['msg(' msg '),callerFunc(' mainFuncName '),condition(' conditionStr ')'];
    switch exitflag
        case -2
            %condition(isempty(stepLen_found)),msg('Matlab LineSearch failed') in performLineSearch
        case 1
            %condition(sum(abs(gradient_at_zero)) <= paramGN.tolFun),msg('Optimality Condition below TolFun') in initialPoint_Check 
            %condition(sum(abs(grad_cur)) <= tolFun),msg('Optimality Condition below TolFun') in checkOptimalityCondition 
        case 2
            %condition(accDiff<0.00001),msg(['Accuracy difference(' num2str(accDiff) ') below 0.00001']) in loopCheck_Final
            %condition(gtd > -tolX),msg('Directional Derivative below TolX') in checkDirectionalDerivative
        case 3
            %condition(~isLegal(paramIT.d)),msg('Step direction is illegal!'') in computeDescentDirection
    end
end
% Unconstrained optimizer using a line search strategy
% Uses an interface very similar to fminunc
%   (it doesn't support all of the optimization toolbox options,
%       but supports many other options).
% It computes descent directions using one of ('Method'):
%   - 'sd': Steepest Descent
%       (no previous information used, not recommended)
%   - 'csd': Cyclic Steepest Descent
%       (uses previous step length for a fixed length cycle)
%   - 'bb': Barzilai and Borwein Gradient
%       (uses only previous step)
%   - 'cg': Non-Linear Conjugate Gradient
%       (uses only previous step and a vector beta)
%   - 'scg': Scaled Non-Linear Conjugate Gradient
%       (uses previous step and a vector beta, 
%           and Hessian-vector products to initialize line search)
%   - 'pcg': Preconditionined Non-Linear Conjugate Gradient
%       (uses only previous step and a vector beta, preconditioned version)
%   - 'lbfgs': Quasi-Newton with Limited-Memory BFGS Updating
%       (default: uses a predetermined nunber of previous steps to form a 
%           low-rank Hessian approximation)
%   - 'newton0': Hessian-Free Newton
%       (numerically computes Hessian-Vector products)
%   - 'pnewton0': Preconditioned Hessian-Free Newton 
%       (numerically computes Hessian-Vector products, preconditioned
%       version)
%   - 'qnewton': Quasi-Newton Hessian approximation
%       (uses dense Hessian approximation)
%   - 'mnewton': Newton's method with Hessian calculation after every
%   user-specified number of iterations
%       (needs user-supplied Hessian matrix)
%   - 'newton': Newton's method with Hessian calculation every iteration
%       (needs user-supplied Hessian matrix)
%   - 'tensor': Tensor
%       (needs user-supplied Hessian matrix and Tensor of 3rd partial derivatives)
%
% Several line search strategies are available for finding a step length satisfying
%   the termination criteria ('LS'):
%   - 0: Backtrack w/ Step Size Halving
%   - 1: Backtrack w/ Quadratic/Cubic Interpolation from new function values
%   - 2: Backtrack w/ Cubic Interpolation from new function + gradient
%   values (default for 'bb' and 'sd')
%   - 3: Bracketing w/ Step Size Doubling and Bisection
%   - 4: Bracketing w/ Cubic Interpolation/Extrapolation with function +
%   gradient values (default for all except 'bb' and 'sd')
%   - 5: Bracketing w/ Mixed Quadratic/Cubic Interpolation/Extrapolation
%   - 6: Use Matlab Optimization Toolbox's line search
%           (requires Matlab's linesearch.m to be added to the path)
%
%   Above, the first three find a point satisfying the Armijo conditions,
%   while the last four search for find a point satisfying the Wolfe
%   conditions.  If the objective function overflows, it is recommended
%   to use one of the first 3.
%   The first three can be used to perform a non-monotone
%   linesearch by changing the option 'Fref'.
%
% Several strategies for choosing the initial step size are avaiable ('LS_init'):
%   - 0: Always try an initial step length of 1 (default for all except 'cg' and 'sd')
%       (t = 1)
%   - 1: Use a step similar to the previous step (default for 'cg' and 'sd')
%       (t = t_old*min(2,grad_cur'd/g_old'd_old))
%   - 2: Quadratic Initialization using previous function value and new
%   function value/gradient (use this if steps tend to be very long)
%       (t = min(1,2*(f-f_old)/grad_cur))
%   - 3: The minimum between 1 and twice the previous step length
%       (t = min(1,2*t)
%   - 4: The scaled conjugate gradient step length (may accelerate
%   conjugate gradient methods, but requires a Hessian-vector product)
%       (t = grad_cur'd/d'Hd)
%
% Inputs:
%   funObj is a function handle
%   theta0 is a starting vector;
%   options is a struct containing parameters
%  (defaults are used for non-existent or blank fields)
%   all other arguments are passed to funObj
%
% Outputs:
%   thetaOut is the minimum value found
%   f is the function value at the minimum found
%   exitflag returns an exit condition
%   output returns a structure with other information
%
% Supported Input Options
%   Display - Level of display [ off | final | (iter) | full | excessive ]
%   MaxFunEvals - Maximum number of function evaluations allowed (1000)
%   MaxIter - Maximum number of iterations allowed (500)
%   TolFun - Termination tolerance on the first-order optimality (1e-5)
%   TolX - Termination tolerance on progress in terms of function/parameter changes (1e-9)
%   Method - [ sd | csd | bb | cg | scg | pcg | {lbfgs} | newton0 | pnewton0 |
%       qnewton | mnewton | newton | tensor ]
%   c1 - Sufficient Decrease for Armijo condition (1e-4)
%   c2 - Curvature Decrease for Wolfe conditions (.2 for cg methods, .9 otherwise)
%   LS_init - Line Search Initialization -see above (2 for cg/sd, 4 for scg, 0 otherwise)
%   LS - Line Search type -see above (2 for bb, 4 otherwise)
%   Fref - Setting this to a positive integer greater than 1
%       will use non-monotone Armijo objective in the line search.
%       (20 for bb, 10 for csd, 1 for all others)
%   numDiff - compute derivative numerically
%       (default: 0) (this option has a different effect for 'newton', see below)
%   useComplex - if 1, use complex differentials when computing numerical derivatives
%       to get very accurate values (default: 0)
%   DerivativeCheck - if 'on', computes derivatives numerically at initial
%       point and compares to user-supplied derivative (default: 'off')
%   outputFcn - function to run after each iteration (default: []).  It
%       should have the following interface:
%       outputFcn(thetaOut,infoStruct,state,varargin{:})
%   useMex - where applicable, use mex files to speed things up (default: 1)
%
% Method-specific input options:
%   newton:
%       HessianModify - type of Hessian modification for direct solvers to
%       use if the Hessian is not positive definite (default: 0)
%           0: Minimum Euclidean norm s.t. eigenvalues sufficiently large
%           (requires eigenvalues on iterations where matrix is not pd)
%           1: Start with (1/2)*||A||_F and increment until Cholesky succeeds
%           (an approximation to method 0, does not require eigenvalues)
%           2: Modified LDL factorization
%           (only 1 generalized Cholesky factorization done and no eigenvalues required)
%           3: Modified Spectral Decomposition
%           (requires eigenvalues)
%           4: Modified Symmetric Indefinite Factorization
%           5: Uses the eigenvector of the smallest eigenvalue as negative
%           curvature direction
%       cgSolve - use conjugate gradient instead of direct solver (default: 0)
%           0: Direct Solver
%           1: Conjugate Gradient
%           2: Conjugate Gradient with Diagonal Preconditioner
%           3: Conjugate Gradient with LBFGS Preconditioner
%           thetaOut: Conjugate Graident with Symmetric Successive Over Relaxation
%           Preconditioner with parameter thetaOut
%               (where thetaOut is a real number in the range [0,2])
%           thetaOut: Conjugate Gradient with Incomplete Cholesky Preconditioner
%           with drop tolerance -thetaOut
%               (where thetaOut is a real negative number)
%       numDiff - compute Hessian numerically
%                 (default: 0, done with complex differentials if useComplex = 1)
%       LS_saveHessiancomp - when on, only computes the Hessian at the
%       first and last iteration of the line search (default: 1)
%   mnewton:
%       HessianIter - number of iterations to use same Hessian (default: 5)
%   qnewton:
%       initialHessType - scale initial Hessian approximation (default: 1)
%       qnUpdate - type of quasi-Newton update (default: 3):
%           0: BFGS
%           1: SR1 (when it is positive-definite, otherwise BFGS)
%           2: Hoshino
%           3: Self-Scaling BFGS
%           4: Oren's Self-Scaling Variable Metric method 
%           5: McCormick-Huang asymmetric update
%       Damped - use damped BFGS update (default: 1)
%   newton0/pnewton0:
%       HvFunc - user-supplied function that returns Hessian-vector products
%           (by default, these are computed numerically using autoHv)
%           HvFunc should have the following interface: HvFunc(v,thetaOut,varargin{:})
%       useComplex - use a complex perturbation to get high accuracy
%           Hessian-vector products (default: 0)
%           (the increased accuracy can make the method much more efficient,
%               but gradient code must properly support complex inputs)
%       useNegCurv - a negative curvature direction is used as the descent
%           direction if one is encountered during the cg iterations
%           (default: 1)
%       precFunc (for pnewton0 only) - user-supplied preconditioner
%           (by default, an L-BFGS preconditioner is used)
%           precFunc should have the following interfact:
%           precFunc(v,thetaOut,varargin{:})
%   lbfgs:
%       Corr - number of corrections to store in memory (default: 100)
%           (higher numbers converge faster but use more memory)
%       Damped - use damped update (default: 0)
%   pcg:
%       cgUpdate - type of update (default: 2)
%   cg/scg/pcg:
%       cgUpdate - type of update (default for cg/scg: 2, default for pcg: 1)
%           0: Fletcher Reeves
%           1: Polak-Ribiere
%           2: Hestenes-Stiefel (not supported for pcg)
%           3: Gilbert-Nocedal
%       HvFunc (for scg only)- user-supplied function that returns Hessian-vector 
%           products
%           (by default, these are computed numerically using autoHv)
%           HvFunc should have the following interface:
%           HvFunc(v,thetaOut,varargin{:})
%       precFunc (for pcg only) - user-supplied preconditioner
%           (by default, an L-BFGS preconditioner is used)
%           precFunc should have the following interfact:
%           precFunc(v,thetaOut,varargin{:})
%   bb:
%       bbType - type of bb step (default: 1)
%           0: min_alpha ||delta_x - alpha delta_g||_2
%           1: min_alpha ||alpha delta_x - delta_g||_2
%           2: Conic BB
%           3: Gradient method with retards
%   csd:
%       cycle - length of cycle (default: 3)
%
% Supported Output Options
%   iterations - number of iterations taken
%   funcCount - number of function evaluations
%   algorithm - algorithm used
%   firstorderopt - first-order optimality
%   message - exit message
%   resultTraceStruct.funccount - function evaluations after each iteration
%   resultTraceStruct.fval - function value after each iteration
%
% Author: Mark Schmidt (2006)
% Web: http://www.cs.ubc.ca/~schmidtm
%
% Sources (in order of how much the source material contributes):
%   J. Nocedal and S.J. Wright.  1999.  "Numerical Optimization".  Springer Verlag.
%   R. Fletcher.  1987.  "Practical Methods of Optimization".  Wiley.
%   J. Demmel.  1997.  "Applied Linear Algebra.  SIAM.
%   R. Barret, M. Berry, T. Chan, J. Demmel, J. Dongarra, V. Eijkhout, R.
%   Pozo, C. Romine, and H. Van der Vost.  1994.  "Templates for the Solution of
%   Linear Systems: Building Blocks for Iterative Methods".  SIAM.
%   J. More and D. Thuente.  "Line search algorithms with guaranteed
%   sufficient decrease".  ACM Trans. Math. Softw. vol 20, 286-307, 1994.
%   M. Raydan.  "The Barzilai and Borwein gradient method for the large
%   scale unconstrained minimization problem".  SIAM J. Optim., 7, 26-33,
%   (1997).
%   "Mathematical Optimization".  The Computational Science Education
%   Project.  1995.
%   C. Kelley.  1999.  "Iterative Methods for Optimization".  Frontiers in
%   Applied Mathematics.  SIAM.