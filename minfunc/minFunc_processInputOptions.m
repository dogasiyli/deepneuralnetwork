function [verbose,verboseI,debug,doPlot,maxFunEvals,maxIter,tolFun,tolX,method,...
    corrections,c1,c2,LS_init,LS,cgSolve,qnUpdate,cgUpdate,initialHessType,...
    HessianModify,Fref,useComplex,numDiff,LS_saveHessianComp,...
    DerivativeCheck,Damped,HvFunc,bbType,cycle,...
    HessianIter,outputFcn,useMex,useNegCurv,precFunc] = ...
    minFunc_processInputOptions(optionalMethodParamsStruct)

    % Constants
    SD = 0;
    CSD = 1;
    BB = 2;
    CG = 3;
    PCG = 4;
    LBFGS = 5;
    QNEWTON = 6;
    NEWTON0 = 7;
    NEWTON = 8;
    TENSOR = 9;

    verbose = 1;
    verboseI= 1;
    debug = 0;
    doPlot = 0;
    method = LBFGS;
    cgSolve = 0;

    optionalMethodParamsStruct = toUpper(optionalMethodParamsStruct);

    if isfield(optionalMethodParamsStruct,'DISPLAY')
        switch(upper(optionalMethodParamsStruct.DISPLAY))
            case 0
                verbose = 0;
                verboseI = 0;
            case 'FINAL'
                verboseI = 0;
            case 'OFF'
                verbose = 0;
                verboseI = 0;
            case 'NONE'
                verbose = 0;
                verboseI = 0;
            case 'FULL'
                debug = 1;
            case 'EXCESSIVE'
                debug = 1;
                doPlot = 1;
        end
    end


    LS_init = 0;
    c2 = 0.9;
    LS = 4;
    Fref = 1;
    Damped = 0;
    HessianIter = 1;
    if isfield(optionalMethodParamsStruct,'METHOD')
        m = upper(optionalMethodParamsStruct.METHOD);
        switch(m)
            case 'TENSOR'
                method = TENSOR;
            case 'NEWTON'
                method = NEWTON;
            case 'MNEWTON'
                method = NEWTON;
                HessianIter = 5;
            case 'PNEWTON0'
                method = NEWTON0;
                cgSolve = 1;
            case 'NEWTON0'
                method = NEWTON0;
            case 'QNEWTON'
                method = QNEWTON;
                Damped = 1;
            case 'LBFGS'
                method = LBFGS;
            case 'BB'
                method = BB;
                LS = 2;
                Fref = 20;
            case 'PCG'
                method = PCG;
                c2 = 0.2;
                LS_init = 2;
            case 'SCG'
                method = CG;
                c2 = 0.2;
                LS_init = 4;
            case 'CG'
                method = CG;
                c2 = 0.2;
                LS_init = 2;
            case 'CSD'
                method = CSD;
                c2 = 0.2;
                Fref = 10;
                LS_init = 2;
            case 'SD'
                method = SD;
                LS_init = 2;
        end
    end

    maxFunEvals = getStructField(optionalMethodParamsStruct,'MAXFUNEVALS',5000);
    maxIter = getStructField(optionalMethodParamsStruct,'MAXITER',5000);
    tolFun = getStructField(optionalMethodParamsStruct,'TOLFUN',1e-5);
    tolX = getStructField(optionalMethodParamsStruct,'TOLX',1e-9);
    corrections = getStructField(optionalMethodParamsStruct,'CORR',100);
    c1 = getStructField(optionalMethodParamsStruct,'C1',1e-4);
    c2 = getStructField(optionalMethodParamsStruct,'C2',c2);
    LS_init = getStructField(optionalMethodParamsStruct,'LS_INIT',LS_init);
    LS = getStructField(optionalMethodParamsStruct,'LS',LS);
    cgSolve = getStructField(optionalMethodParamsStruct,'CGSOLVE',cgSolve);
    qnUpdate = getStructField(optionalMethodParamsStruct,'QNUPDATE',3);
    cgUpdate = getStructField(optionalMethodParamsStruct,'CGUPDATE',2);
    initialHessType = getStructField(optionalMethodParamsStruct,'INITIALHESSTYPE',1);
    HessianModify = getStructField(optionalMethodParamsStruct,'HESSIANMODIFY',0);
    Fref = getStructField(optionalMethodParamsStruct,'FREF',Fref);
    useComplex = getStructField(optionalMethodParamsStruct,'USECOMPLEX',0);
    numDiff = getStructField(optionalMethodParamsStruct,'NUMDIFF',0);
    LS_saveHessianComp = getStructField(optionalMethodParamsStruct,'LS_SAVEHESSIANCOMP',1);
    DerivativeCheck = getStructField(optionalMethodParamsStruct,'DERIVATIVECHECK',0);
    Damped = getStructField(optionalMethodParamsStruct,'DAMPED',Damped);
    HvFunc = getStructField(optionalMethodParamsStruct,'HVFUNC',[]);
    bbType = getStructField(optionalMethodParamsStruct,'BBTYPE',0);
    cycle = getStructField(optionalMethodParamsStruct,'CYCLE',3);
    HessianIter = getStructField(optionalMethodParamsStruct,'HESSIANITER',HessianIter);
    outputFcn = getStructField(optionalMethodParamsStruct,'OUTPUTFCN',[]);
    useMex = getStructField(optionalMethodParamsStruct,'USEMEX',1);
    useNegCurv = getStructField(optionalMethodParamsStruct,'USENEGCURV',1);
    precFunc = getStructField(optionalMethodParamsStruct,'PRECFUNC',[]);
end

function [optionsStruct] = toUpper(optionsStruct)
    if ~isempty(optionsStruct)
        fn = fieldnames(optionsStruct);
        for i = 1:length(fn)
            upperFieldName = ~isfield(optionsStruct, upper(fn{i}));
            if upperFieldName
                optionsStruct.(upper(fn{i})) = optionsStruct.(fn{i});
                optionsStruct = rmfield(optionsStruct,fn{i});
            end
            %optionsStruct = setfield(optionsStruct,upper(fn{i}),getfield(optionsStruct,fn{i}));
        end
    end
end