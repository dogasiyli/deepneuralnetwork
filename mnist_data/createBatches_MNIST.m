function [initialDataStruct, batchInfo] = createBatches_MNIST(mainFolder, bathSizeMaxMB, imageRowColSize, percentages)

%  This loads our training and test data from the MNIST database files.
    [trainData, trainLabels, testData, testLabels, sampleCounts] = script_loadMnistDatabase_Percent(100, 100);
%    [ memAvail, memMatlab ] = displayMemory;%returns in megabytes
    
    if (~exist('imageRowColSize','var') || isempty(imageRowColSize))
        imageRowColSize = 28;
    end
    if (~exist('bathSizeMaxMB','var') || isempty(bathSizeMaxMB))
        bathSizeMaxMB = 50;
    end
    if (~exist('percentages','var') || isempty(percentages))
        percentages = struct;
    end
    if (isfield(percentages,'Train') && ~isfield(percentages,'Validation'))
        percentages.Validation = 1-percentages.Train;  
    end
    if (~isfield(percentages,'Train') || ~isfield(percentages,'Validation'))
        percentages.Train = 0.85;
        percentages.Validation = 1-percentages.Train;  
    end
    if (~isfield(percentages,'Test'))
        percentages.Test = 1.0;
    end
    
    if (~strcmp(mainFolder(end),'\'))
        mainFolder = [mainFolder '\'];
    end
    if (~exist(mainFolder,'dir'))
        mkdir(mainFolder)
    end
    batchInfo = seperateMNISTData( mainFolder, trainLabels, testLabels, imageRowColSize, bathSizeMaxMB, percentages );
    trainDataUsed = false(size(trainLabels));
    testDataUsed = false(size(testLabels));
    % mainFolder =  'D:\Datasets\mnist';
    for bt = 1:3 %Batch Type - Train(1), Validation(2), Test(3)
        [btStr, batch_Count] = getVals(bt, batchInfo);
        for b=1:batch_Count
            fileNameBatch = [mainFolder 'data_batch_' btStr '_' num2str(b,'%02d') '-' num2str(batch_Count,'%02d') '.mat'];
            if exist(fileNameBatch,'file')
                disp(['***Data for (' fileNameBatch ') is being skipped']);
                continue;
            end            
            batch_image_count = getBIC(bt, batchInfo, b);
            bic=1;
            disp(['**Initializing data for (' fileNameBatch ')']);
            data = rand(imageRowColSize,imageRowColSize,1,batch_image_count);
            labels = zeros(1,batch_image_count);
            % trainData can be in 3 different forms
            % 2 dimensional - [d numberOfSamples] = size(trainData)
            % 3 dimensional - [imageDim imageDim numberOfSamples] = size(trainData);
            % 4 dimensional - [imageDim imageDim channelSize numberOfSamples] = size(trainData)
    
            %we need to create the batch file which consists of
            %trainData - 
            %trainLabels - 
            %additional information can also be added like
            %each samples information from dataInfo which is N by 10
            
            for d = 1:10
                if (bt==1)%train
                    imageCountCur = batchInfo.image_Count_Per_Batch_Train(b,d);             
                elseif (bt==2)%valid
                    imageCountCur = batchInfo.image_Count_Per_Batch_Valid(b,d);               
                else%test
                    imageCountCur = batchInfo.image_Count_Per_Batch_Test(b,d); 
                end
                disp(['Digit-' num2str(d-1) ', image count = ' num2str(imageCountCur)]);  
                
                %the samples that fall into the Batch Type and Batch ID
                slct = getSelections(batchInfo.batchDistributions, bt, d, b);

                %images_RGBD is [imageRowColSize imageRowColSize 4 imageCountCur]
                slctImageCount = sum(slct==1);
                assert(imageCountCur==slctImageCount,'these two values must be equal');
                
                if (bt<3)%train
                    samplesToBeSelected = false(size(trainLabels));
                    samplesToBeSelected(trainLabels==d) = slct;
                    assert(sum(trainDataUsed(samplesToBeSelected))==0,'some samples are being re-used')
                    trainDataUsed(samplesToBeSelected) = true;

                    images_gray_d = reshape(trainData(:,samplesToBeSelected),imageRowColSize,imageRowColSize,1,slctImageCount);
                    labels_cur = trainLabels(samplesToBeSelected);
                else
                    samplesToBeSelected = false(size(testLabels));
                    samplesToBeSelected(testLabels==d) = slct;
                    assert(sum(testDataUsed(samplesToBeSelected))==0,'some samples are being re-used')
                    testDataUsed(samplesToBeSelected) = true;

                    images_gray_d = reshape(testData(:,samplesToBeSelected),imageRowColSize,imageRowColSize,1,slctImageCount);
                    labels_cur = testLabels(samplesToBeSelected);
                end
                data(:,:,:,bic:bic+slctImageCount-1) = images_gray_d;
                labels(:,bic:bic+slctImageCount-1) = d;
                correctLabelsCount = sum(labels_cur==d);
                assert(slctImageCount==correctLabelsCount,'must be equal');
                bic = bic+slctImageCount;
            end

            disp(['Data for (' fileNameBatch ') is being saved']);
            a = whos('data');
            uncompressedSize = a.bytes/(1024*1024);
            save(fileNameBatch,'data','labels','uncompressedSize','-v7.3');
            a = dir(fileNameBatch);
            compressedSize = a.bytes/(1024*1024);
            disp(['Data of size ' num2str(uncompressedSize, '%6.3f') 'MB is compressed into ' num2str(compressedSize, '%6.3f') 'MB']);
            disp(['**Data for (' fileNameBatch ') is saved']);
            clear a compressedSize uncompressedSize
        end %b=1:batch_Count
    end
    
    initialDataStruct = createInitialDataStruct_MNIST(mainFolder, batchInfo);
end

function [btStr, batch_Count] = getVals(bt,batchInfo)
    switch bt
        case 1
            btStr = 'train';
            batch_Count = batchInfo.batch_Count_Train;
        case 2
            btStr = 'valid';
            batch_Count = batchInfo.batch_Count_Valid;
        case 3
            btStr = 'test';
            batch_Count = batchInfo.batch_Count_Test;
    end
end

function [batch_image_count] = getBIC(bt,batchInfo,b)
    switch bt
        case 1
            batch_image_count = sum(batchInfo.image_Count_Per_Batch_Train(b,:));
        case 2
            batch_image_count = sum(batchInfo.image_Count_Per_Batch_Valid(b,:));
        case 3
            batch_image_count = sum(batchInfo.image_Count_Per_Batch_Test(b,:));
    end
end

function [slct] = getSelections(batchDistributions, bt, d, b)
    switch bt
        case 1
            bid = batchDistributions(1,d).bid;
            slct = batchDistributions(1,d).TR==1 & bid==b;
        case 2
            bid = batchDistributions(1,d).bid;
            slct = batchDistributions(1,d).VA==1 & bid==b;
        case 3
            bid = batchDistributions(2,d).bid;
            slct = batchDistributions(2,d).TE==1 & bid==b;
    end  
end