function dS = createInitialDataStruct_MNIST(loadFolder, batchInfo)
%createInitialDataStruct creates the dataStruct variable 

% dataStruct  dS
%
% 1.params that do not change
% 	a. countCategory -> total number of class IDs that the whole data has.
%   b. countBatch.{train,validation,test} -> it is a struct itself too
% 	c. batchInfo -> a matrix that has the information about the data to be loaded.
% 					IS IT MEANINGFUL TO HAVE .SAVE AND .LOAD SEPERATELY - SUCH THAT 
% 					cnnnet{layer-1}.dataStruct.batchInfo.save == cnnnet{layer}.dataStruct.batchInfo.load
% 					or maybe here we will have a fileName as a function
% 					data_batch_<type>_<XX>.mat
%   d. getFileName -> an inline function to get fileName to load
% 	e. singleImageShape -> [R,C,F]
%   f. imageShapeIdentifier-> 'square' or 'rectangle' - only square will be allowed initially
% 	g. countFeat -> prod(R,C,F)
%
% 2.params that will change accordingly
% 	a. type -> train, validation, test
%   b. data_act -> 'data' or 'act'
% 	c. loadFolder -> the exact folder that the train/valid/test data/act is in.
%
% 3.params that change per batch
%   a. batchID_current
%   b. fileName_current
% 	c. countSamples -> total number of samples in current batch
% 	d. numOfBatches -> how many batches of this data type exist
% 	e. curBatchID -> initially it will be 1, then it will increment until numOfBatches
% 	f. labelFunc -> an ?inline? function that returns labes of the so called batch.


% 1.params that do not change
% 	a. countCategory -> total number of class IDs that the whole data has.
    dS.countCategory = 10;%10 digits%
%   b. countBatch.{train,validation,test} -> it is a struct itself too
    dS.countBatch.train = batchInfo.batch_Count_Train;
    dS.countBatch.valid = batchInfo.batch_Count_Valid;
    dS.countBatch.test = batchInfo.batch_Count_Test;
    dS.countBatch.act = [];%the number of batches in an activation batch
% 	c. batchInfo -> a matrix that has the information about the data to be loaded.
% 					IS IT MEANINGFUL TO HAVE .SAVE AND .LOAD SEPERATELY - SUCH THAT 
% 					cnnnet{layer-1}.dataStruct.batchInfo.save == cnnnet{layer}.dataStruct.batchInfo.load
% 					or maybe here we will have a fileName as a function
% 					data_batch_<type>_<XX>.mat
        %load('D:\Datasets\dataset5\batchInfo.mat');
    dS.batchInfo = [];%batchInfo;
        %clear batchInfo;
%   d. fileNameStruct -> an inline function to get fileName to load
    dS.fileNameStruct = '<data-act>_batch_<train-valid-test>_<batchID>-<batchCount>.mat';
%   e. getFileName -> an inline function to get fileName to load
    dS.getFileName = @(data_act_STR, type_STR, batchID_INT, batchCnt_INT) [data_act_STR '_batch_' type_STR '_' num2str(batchID_INT,'%02d') '-' num2str(batchCnt_INT,'%02d') '.mat'];
%   f. getFileName_Current -> an inline function to get the current fileName to load
    dS.getFileName_Current = @(dS_) [dS_.loadFolder dS_.data_act '_batch_' dS_.type '_' num2str(dS_.batchID_current,'%02d') '-' num2str(dS_.batchCnt_current,'%02d') '.mat'];
%      e.g. data_batch_train_01.mat
%   g. getLabelVec
    dS.getLabelVec = @(labels_) labels_(:)';
%   h. getLabelArr
    dS.getLabelArr = @(labels_) full(sparse(labels_, 1:length(labels_), 1));
% 	i. singleImageShape -> [R,C,F]
    dS.singleImageShape = [28,28,1];
%   j. imageShapeIdentifier-> 'square' or 'rectangle' - only square will be allowed initially
    dS.imageShapeIdentifier = 'square';
% 	k. countFeat -> prod(R,C,F)
    dS.countFeat = prod(dS.singleImageShape);
%   l. updateDataStruct
    dS.updateDataStruct = @(dS_, varargin_) updateDataStruct(dS_, varargin_{:});
    
% 2.params that will change accordingly
% 	a. type -> train, validation, test
    dS.type = 'train';
%   b. data_act -> 'data' or 'act'
    dS.data_act = 'data';
% 	c. loadFolder -> the exact folder that the train/valid/test data/act is in.
    if (~strcmp(loadFolder(end),'\'))
        loadFolder = [loadFolder '\'];
    end
    dS.loadFolder = loadFolder;%'D:\Datasets\mnist\';%will become the temporary folder for activations
%
% 3.params that change per batch
%   a. batchID_current -> initially it will be 1, then it will increment until numOfBatches
    dS.batchID_current = 1;
    dS.batchCnt_current = dS.countBatch.train;
%   b. fileName_current
    dS.fileName_current = dS.getFileName_Current(dS);
% 	c. countSamples -> total number of samples in current batch
        load(dS.fileName_current);
        data_size_vec  = size(data);
    dS.singleImageShape = data_size_vec(1:3);%normally already manually written above
    dS.countSamples = data_size_vec(4);
        clear data_size_vec;
    dS.countFeat = prod(dS.singleImageShape);%normally already manually written above

% % example extraction of labels
%     load(dS.fileName_current);
%     labelVec = dS.getLabelVec(samplesInfo_Gray_batch);
%     labelArr = dS.getLabelArr(samplesInfo_Gray_batch);
%
% Also I need a function named 'updateDataStruct' to update the structure
% variables

end