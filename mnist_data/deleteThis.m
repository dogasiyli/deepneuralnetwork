1.

calcSlack
[ s_out, cost_cur ] = calcSlack( a, [], 'output_softmax', cnnnet{layer,1}.actDerivative,'groundTruth', full(sparse(labels, 1:countSamples, 1)));
	Xf2 = @(a_) bsxfun(@minus,a_,max(a_,[],1));
	htx = @(X_) bsxfun(@rdivide,exp(X_),sum(exp(X_)));
	X = Xf2(a_in);%theta*data;%
	probsAll = htx(X);
	logProbsAll = log(probsAll);
	s_out = (groundTruth-probsAll);
	cost = (-1/size(groundTruth, 2))*sum(logProbsAll(:).*groundTruth(:));
	
	
[W_grad_cur, b_grad_cur, weightDecayCost_cur]  = calcDeltaWeight( s_in, a, W, weightDecayParam, 'softmax', displayOpts);
		W_grad = ((s_prev*a_next')./ m) + lambda*W;
		b_grad = [];
		weightDecayCost = (lambda/2)*sum(W(:).^2);%applyWeightDecay
		

-------------------
we know our activation at softmax layer
we want to backpropogate it - hence we need to calculate a slack variable
normally this is easy to calculate for a regression output.
but when the error function changes - i get fucked up


2.

%         if (layer==layerCount-1)
%             %the output layer is a special one
%             load([saveOpts.MainFolder 'Activation_' num2str(activationID,'%02d') '.mat'],'a');
%             %groundTruth = full(sparse(labels, 1:countSamples, 1));
%             [ s_out, cost_cur ] = calcSlack( softMaxedData, [], 'output_softmax', cnnnet{layer,1}.actDerivative,'groundTruth', full(sparse(labels, 1:countSamples, 1)));
%             activationID = activationID-1;%7->6
%         else
% 
%         end

+++++++
softmaxThetaCost = (-1/M)*sum(logProbsAll(:).*groundTruth(:));%DONE in calcSlack - cost
softmaxThetaGrad = (-1/M).*(groundTruth-probsAll)*(a3');

%Weight Decay
softmaxThetaCost = softmaxThetaCost + (lambda/2)*sum(W34(:).^2);
softmaxThetaGrad = softmaxThetaGrad + lambda*W34;

slack4 = W34'*(groundTruth-probsAll);%DONE in calcSlack - s_out
+++++++




------------
[s_out, cost_cur] = calcSlack( softMaxedData,  [],     'softmax',          [],    'W', W,'groundTruth', full(sparse(labels, 1:countSamples, 1)));
------->>>>
[ s_out, cost ]   = calcSlack(      a_in,    s_in, layerIdentifier, actDerivative, varargin)
            %a_in = softMaxedData
            [groundTruth, W] = parseArgs({'groundTruth', 'W'},{[],[]},varargin{:});
            
            A = bsxfun(@minus,a_in,max(a_in,[],1));
            A = exp(A);
            Y_1 = bsxfun(@rdivide,A,sum(A));%probsAll
            Y = log(Y_1);%logProbsAll;

            % Compute the cost and gradient for softmax regression.
            sampleCount = length(groundTruth);
            cost = (-1/sampleCount)*sum(Y(:).*groundTruth(:));            
            s_out = W'*(groundTruth-Y_1);
[s_out, cost_cur] 
<<<<-------
------------
[W_grad_cur, b_grad_cur, weightDecayCost_cur]  = calcDeltaWeight( s_in, a, W, weightDecayParam, 'softmax', displayOpts);
------->>>>
[W_grad, b_grad, weightDecayCost]  = calcDeltaWeight( s_prev, a_next, W, lambda, layerIdentifier, displayOpts ,varargin)


3.

	% 
	A = theta*data;
	A = bsxfun(@minus,A,max(A,[],1));
	A = exp(A);
	Y_1 = bsxfun(@rdivide,A,sum(A));%probsAll
	Y = log(Y_1);%logProbsAll;
	
    % Compute the cost and gradient for softmax regression.
    cost = (-1/sampleCount)*sum(Y(:).*groundTruth(:));
    thetagrad = (-1/sampleCount).*(groundTruth-Y_1)*data';
    
    % Apply "Weight Decay" to both
    cost = cost + (lambda/2)*sum(theta(:).^2);
    thetagrad = thetagrad + lambda*theta;
	
	
----
these lines are proved to be working correctly.
but what we do not have here is the slack variable
so lets check how we did use the slack variable for other backpropogation layers?
