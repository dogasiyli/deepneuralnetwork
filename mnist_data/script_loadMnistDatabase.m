%this part is put into script for increasing readibility of convolution
%code. this part can be changed to any other data loading script in the
%convolution example code

% Load MNIST database files
trainData   = loadMNISTImages('train-images-idx3-ubyte');
trainLabels = loadMNISTLabels('train-labels-idx1-ubyte');
testData   = loadMNISTImages('t10k-images-idx3-ubyte');
testLabels = loadMNISTLabels('t10k-labels-idx1-ubyte');

trainData   = trainData(:, trainLabels >= 0 & trainLabels <= 9);
trainLabels = trainLabels(trainLabels >= 0 & trainLabels <= 9)' + 1; % Shift Labels to the Range 1-5
testData   = testData(:, testLabels >= 0 & testLabels <= 9);
testLabels = testLabels(testLabels >= 0 & testLabels <= 9)' + 1; % Shift Labels to the Range 1-5

% Output Some Statistics
% fprintf('# examples in unlabeled set: %d\n', size(unlabeledData, 2));
fprintf('# examples in supervised training set: %d\n', size(trainData, 2));
fprintf('# examples in supervised testing set: %d\n\n', size(testData, 2));
clear mnistData mnistLabels labeledSet numTrain
clear trainSet testSet