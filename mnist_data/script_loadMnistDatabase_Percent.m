function [trainData, trainLabels, testData, testLabels, sampleCounts] = script_loadMnistDatabase_Percent(trainPercent, testPercent, displayDataSize, lastDigitToLearn)
    %this part is put into script for increasing readibility of convolution
    %code. this part can be changed to any other data loading script in the
    %convolution example code

    %if the percent is given as in 100 then change it to between 0 and 1
    if (trainPercent>1)
        trainPercent = trainPercent*0.01;
    end
    if (testPercent>1)
        testPercent = testPercent*0.01;
    end
    
    if (~exist('displayDataSize','var') || isempty(displayDataSize))
        displayDataSize = false;
    end
    
    if (~exist('lastDigitToLearn','var') || isempty(lastDigitToLearn) || (lastDigitToLearn<=0 || lastDigitToLearn>=10))
        lastDigitToLearn = 9;
    end
    labelsToGet = 0:lastDigitToLearn;

    % Load data
    sampleCounts = zeros(3,lastDigitToLearn+2);%rows:all-train-test||cols:all-0-1-2-..-numLabels  
    [trainData, trainLabels, sampleCounts(2,:)] = getSomePercentOfData(loadMNISTImages('train'), loadMNISTLabels('train'), labelsToGet, trainPercent);
    [testData, testLabels, sampleCounts(3,:)] = getSomePercentOfData(loadMNISTImages('test'), loadMNISTLabels('test'), labelsToGet, testPercent);
    sampleCounts(1,:) = sum(sampleCounts(2:3,:));
    trainLabels = trainLabels + 1; % Shift Labels to the Range +1
    testLabels  = testLabels + 1;  % Shift Labels to the Range +1

    % Output Some Statistics
    %fprintf('# examples in unlabeled set: %d\n', size(unlabeledData, 2));
    if (displayDataSize)
        fprintf('# examples in supervised training set: %d\n', size(trainData, 2));
        fprintf('# examples in supervised testing set: %d\n\n', size(testData, 2));
    end
    %clear mnistData mnistLabels labeledSet numTrain
    %clear trainSet testSet
end

function [dataOut, labelsOut, sampleCounts] = getSomePercentOfData(dataIn, labelsIn, labelsToGet, percent)
    assert(size(dataIn,2)==length(labelsIn),'The size of labels and input must be equal')
    sampleCounts = zeros(1,length(labelsToGet)+1); %rows:all-train-test||cols:all-0-1-2-..-numLabels  
    selectedSamples = [];
    for i = 1:length(labelsToGet)
        l = labelsToGet(i);
            samplesOf_l = find(labelsIn == l);
            numToAcquire = min(length(samplesOf_l),max(1,round(length(samplesOf_l)*percent)));%something between 1 and all
        sampleCounts(i+1) = numToAcquire;
        selectedSamples = [selectedSamples;samplesOf_l(1:numToAcquire)];
    end
    sampleCounts(1) = sum(sampleCounts(:));
    dataOut   = dataIn(:, selectedSamples);
    labelsOut = labelsIn(selectedSamples)';
end