function [ batchInfo ] = seperateMNISTData( mainFolder, trainLabels, testLabels, imageRowColSize, bathSizeMaxMB, percentages)
%seperateMNISTData This function will seperate the data into batches
% mainFolder = 'G:\Data\mnist';

    %Matrix is 1 by 10 where each each column represents a class of digit
    batchDistributions = struct([]);
    
    %I need trainData, validationData and testData
    %testData obviously will only have the testPerson's data
    %train data would have the samples
    
    %The max batch size is given in mb or count of images
    %I will know how many image can there be in a batch
    
    singleImageSize = imageRowColSize^2;%this many double values
    singleImageSize = singleImageSize*8;%bytes
    singleImageSize = singleImageSize/(1024*1024);%megabytes
    disp(['Single image size is ' num2str(singleImageSize) ' MB'])
    
    imageCountMaxPerBatch = floor(bathSizeMaxMB/singleImageSize);
    disp(['Maximum allowed image count per batch is ' num2str(imageCountMaxPerBatch)])

    image_Count_Matrix_TrainValid = hist(trainLabels,1:10);%size(trainData,2);
    image_Count_Matrix_Test_Initially = hist(testLabels,1:10);%size(testData,2);
    
    image_Count_Total_Test = floor(percentages.Test*sum(image_Count_Matrix_Test_Initially(:)));
    batch_Count_Test = ceil(image_Count_Total_Test/imageCountMaxPerBatch);
    repCount = ceil(image_Count_Total_Test/batch_Count_Test);
    bid_Test = repmat(1:batch_Count_Test,1,repCount);
    bid_Test = bid_Test(1:image_Count_Total_Test);
    

    image_Count_Total_TrainValid = sum(image_Count_Matrix_TrainValid(:));
    image_Count_Total_Train = floor(image_Count_Total_TrainValid*percentages.Train);
    image_Count_Total_Valid = min(image_Count_Total_TrainValid-image_Count_Total_Train, floor(image_Count_Total_TrainValid*percentages.Validation));
    gcdTV = gcd(image_Count_Total_Train, image_Count_Total_Valid);
    gcdT = image_Count_Total_Train/gcdTV;
    gcdV = image_Count_Total_Valid/gcdTV;
    
    batch_Count_Train = ceil((image_Count_Total_Train)/imageCountMaxPerBatch);
    batch_Count_Valid = ceil((image_Count_Total_Valid)/imageCountMaxPerBatch);
%     batch_Count_TrainValid = batch_Count_Train+batch_Count_Valid;
%     singleTrain_vec = repmat(1:batch_Count_Train,1,gcdT);%round(linspace(0.5,batch_Count_Train+0.49,gcdT));%this one takes care of the distribution along train and validation percentages%
%     singleValid_vec = round(linspace(batch_Count_Train+0.5,batch_Count_Train+batch_Count_Valid+0.49,gcdV));%this one takes care of the distribution along train and validation percentages%repmat(batch_Count_Train+1:batch_Count_TrainValid,1,gcdV);
%     singleTV_vec = [singleTrain_vec(:);singleValid_vec(:)]';%this one takes care of the distribution along train and validation percentages
    singleTV_vec = 1:(gcdT+gcdV);
    repCount = ceil(image_Count_Total_TrainValid/length(singleTV_vec));
    bid_TrainValid = repmat(singleTV_vec,1,repCount);
    %now label train and valid
    trainSamples = mod(bid_TrainValid-1,gcdT+gcdV)<gcdT;
    validSamples = ~trainSamples;
    %now put trainSamples to trainBins and validSamples to validBins
    allTrain_vec = repmat(1:batch_Count_Train,1,ceil(sum(trainSamples)/batch_Count_Train));
    allTrain_vec = allTrain_vec(1:sum(trainSamples));
    bid_TrainValid(trainSamples) = allTrain_vec;
    allValid_vec = repmat(1:batch_Count_Valid,1,ceil(sum(validSamples)/batch_Count_Valid));
    allValid_vec = allValid_vec(1:sum(validSamples));
    bid_TrainValid(validSamples) = allValid_vec+batch_Count_Train;
    bid_TrainValid = bid_TrainValid(1:image_Count_Total_TrainValid);

    disp(['Train - imageCount(' num2str(image_Count_Total_Train) '), batchCount(' num2str(batch_Count_Train) ')'])
    disp(['Valid - imageCount(' num2str(image_Count_Total_Valid) '), batchCount(' num2str(batch_Count_Valid) ')'])
    disp(['Test  - imageCount(' num2str(image_Count_Total_Test) '), batchCount(' num2str(batch_Count_Test) ')'])
    

    % mainFolder = 'G:\Data\mnist';
    
    image_Count_Matrix_Train = zeros(1,10);
    image_Count_Matrix_Valid = zeros(1,10);
    image_Count_Matrix_Test  = zeros(1,10);
    image_Count_Per_Batch_Train = zeros(batch_Count_Train,10);
    image_Count_Per_Batch_Valid = zeros(batch_Count_Valid,10);
    image_Count_Per_Batch_Test = zeros(batch_Count_Test,10);

    %first I need to fill in batchDistributions cell
    %so that I can know which sample to put into which batch data
    for d = 1:10
        for t = 1:2%t=1(train or validation),2(test)
            %these images will be distributed as
            %train
            %validation
            %test
            if (t==2)
                %test
                imageCountCur = image_Count_Matrix_Test_Initially(d);
                %train-validation-test information will all be 3=test
                tvt = 3*ones(1,imageCountCur);%all will be testData
                %Batch ID information will be set as 1-2-3-...-batch_Count_Test
                bid = bid_Test(1:imageCountCur);
                bid_Test = bid_Test(1+imageCountCur : end);

                batchDistributions(2,d).tvt = tvt;
                batchDistributions(2,d).bid = bid;
                batchDistributions(2,d).TR = false(1,imageCountCur);
                batchDistributions(2,d).VA = false(1,imageCountCur);
                batchDistributions(2,d).TE = true(1,imageCountCur);

                image_Count_Matrix_Test(d) = image_Count_Matrix_Test(d) + sum(batchDistributions(2,d).TE==1);
            else
                imageCountCur = image_Count_Matrix_TrainValid(d);
                %train or validation
                %Batch ID information will be set as 1-2-3-...-batch_Count_TrainValid
                %if "1<=bid<=batch_Count_Train" then it is train sample
                %else if "batch_Count_Train+1<=bid<=batch_Count_TrainValid" then it is validation sample
                bid = bid_TrainValid(1:imageCountCur);
                bid_TrainValid = bid_TrainValid(1+imageCountCur : end);

                %train-validation-test information will be 
                %1=train if 
                tvt = 1*ones(1,imageCountCur);%will be either train(1) or validation(2)
                tvt(bid>batch_Count_Train) = 2;
                batchDistributions(1,d).tvt = tvt;
                batchDistributions(1,d).TR = bid <= batch_Count_Train;
                batchDistributions(1,d).VA = bid >  batch_Count_Train;
                batchDistributions(1,d).TE = false(1,imageCountCur);

                bid(bid>batch_Count_Train) = bid(bid>batch_Count_Train)-batch_Count_Train;
                batchDistributions(1,d).bid = bid;

                image_Count_Matrix_Train(d) = image_Count_Matrix_Train(d) + sum(batchDistributions(1,d).TR==1);
                image_Count_Matrix_Valid(d) = image_Count_Matrix_Valid(d) + sum(batchDistributions(1,d).VA==1);
            end

        end
        for b=1:batch_Count_Train
            image_Count_Per_Batch_Train(b,d) = image_Count_Per_Batch_Train(b,d) + sum(batchDistributions(1,d).TR==1 & batchDistributions(1,d).bid==b);
        end
        for b=1:batch_Count_Valid
            image_Count_Per_Batch_Valid(b,d) = image_Count_Per_Batch_Valid(b,d) + sum(batchDistributions(1,d).VA==1 & batchDistributions(1,d).bid==b);
        end
        for b=1:batch_Count_Test
            image_Count_Per_Batch_Test(b,d) = image_Count_Per_Batch_Test(b,d) + sum(batchDistributions(2,d).TE==1 & batchDistributions(2,d).bid==b);
        end
    end%t=1:2
    
    batchInfo.batchSizesMB_Train = singleImageSize*sum(image_Count_Per_Batch_Train,2);
    batchInfo.batchSizesMB_Valid = singleImageSize*sum(image_Count_Per_Batch_Valid,2);
    batchInfo.batchSizesMB_Test = singleImageSize*sum(image_Count_Per_Batch_Test,2);
    
    batchInfo.image_Count_Per_Batch_Train = image_Count_Per_Batch_Train;
    batchInfo.image_Count_Per_Batch_Valid = image_Count_Per_Batch_Valid;
    batchInfo.image_Count_Per_Batch_Test = image_Count_Per_Batch_Test;
    
    batchInfo.image_Count_Matrix_Train = image_Count_Matrix_Train;
    batchInfo.image_Count_Matrix_Valid = image_Count_Matrix_Valid;
    batchInfo.image_Count_Matrix_Test = image_Count_Matrix_Test;
    batchInfo.batch_Count_Train = batch_Count_Train;
    batchInfo.batch_Count_Valid = batch_Count_Valid;
    batchInfo.batch_Count_Test = batch_Count_Test;
    batchInfo.batchDistributions = batchDistributions;
    
    save([mainFolder '\batchInfo.mat'],'batchInfo');
    %createBatches(mainFolder, dataInfoStruct, samplesInfo_RGBD, batchInfo, imageRowColSize);
end
