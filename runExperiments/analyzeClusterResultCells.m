function [wrongPredictionMat, toChangeRows]= analyzeClusterResultCells( clusterResultCells )
    rowCnt = size(clusterResultCells,1);
    wrongPredictionMat = zeros(0,10);
    for i=1:rowCnt
        curMat = clusterResultCells{i,5};
        klustCnt = clusterResultCells{i,1};
        elemCnt = size(curMat,1);
        wrongPredictionMat = [wrongPredictionMat;klustCnt*ones(elemCnt,1) curMat zeros(elemCnt,2)];
    end
    wrongPredictionMat(:,9) = wrongPredictionMat(:,2)*10000000 + ...
                              wrongPredictionMat(:,3)*100000 + ...
                              wrongPredictionMat(:,4)*10000 + ...
                              wrongPredictionMat(:,5)*1000 + ...
                              wrongPredictionMat(:,6)*1;
    [b, m, ic] = unique(wrongPredictionMat(:,9));
    h = accumarray(ic, 1);
    maph = h(ic); 
    wrongPredictionMat(:,10) = maph;
    toChangeRows = [wrongPredictionMat(m,[2:8 10]) h];
    wrongPredictionMat = sortrows(wrongPredictionMat,[-10 2 3 4 5 6]);
    
    toChangeRows = sortrows(toChangeRows,[-9 1:5]);
end

