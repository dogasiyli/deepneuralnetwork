function [hsmResults, pv] = combineHSMresults(OPS)
%combineHSMresults all signs need to have 
% sn - handsStructMedoids_593_sn_mc_0_assignedLabelsRemove1.mat
% and
% hf - handsStructMedoids_593_hf_mc_0_assignedLabelsRemove1.mat
% when we load the file we get
% handsStructMedoids.
% .dataCells
% .detailedLabels with 6 columns
% .imCropCells
% .minDistAllowedPerMedoid
% .maxDistAllowedPerMedoid
% 

    global dnnCodePath;
    if ~exist('OPS', 'var') || ~isstruct(OPS)
        OPS = [];
    end
    srcFold             = getStructField(OPS, 'srcFold', getVariableByComputerName('srcFold'));
    cropMethod          = getStructField(OPS, 'cropMethod', 'mc');
    signIDList          = getStructField(OPS, 'signIDList', getSignListAll());    
    toFolder            = getStructField(OPS, 'toFolder', [dnnCodePath filesep 'runExperiments']);
    
    createFolderIfNotExist(toFolder, true);
    %removePrevImages(toFolder);
    
    signCount = length(signIDList);   
    pv = [];
    
    hsmResults_fileName = [toFolder filesep 'hsmResults.mat'];
    hsmResults_txtFileName = [toFolder filesep 'hsmResults.txt'];
    hsmResults = cell(41,11);
    if exist(hsmResults_fileName,'file')
        load(hsmResults_fileName,'hsmResults');
    end
    if size(hsmResults,2)~=11
        signListAll = getSignListAll;
        for i=1:length(signListAll)
            hsmResults{i,1} = signListAll(i);
        end
        save(hsmResults_fileName,'hsmResults');
    end
    
    compNameAddStr = getVariableByComputerName('compNameAddStr');
    
    khsCols = [2 3];
    snCols = [4 5 6 7];
    hfCols = [8 9 10 11];
    
    
    fid_hsmTxt = fopen(hsmResults_txtFileName,'w+');
    
    for i=1:signCount
        signID = signIDList(i);
        signFolder = [srcFold filesep num2str(signID)];
        signRow = find(getSignListAll()==signID);
        
        handsStructMedoids_sn = getHSMF(signFolder, signID, 'sn', cropMethod);
        handsStructMedoids_hf = getHSMF(signFolder, signID, 'hf', cropMethod);
        [selectedKluster_sn, accuracyMat_sn] = getAccuracyList(signFolder, signID, 'sn', cropMethod);
        [selectedKluster_hf, accuracyMat_hf] = getAccuracyList(signFolder, signID, 'hf', cropMethod);
        
        clustersASPInfoStruct = getClusterInfoFromASPTxt(srcFold, signID);
        hsmResults{signRow,khsCols(1)} = compNameAddStr;
        khsCnt = length({clustersASPInfoStruct.single.clusterName});
        hsmResults{signRow,khsCols(2)} = clustersASPInfoStruct.single(2).clusterName;
        for j=3:khsCnt
            hsmResults{signRow,khsCols(2)} = [hsmResults{signRow,khsCols(2)} ', ' clustersASPInfoStruct.single(j).clusterName];
        end
        
        if ~isempty(selectedKluster_sn)
            hsmResults{signRow,snCols(1)} = compNameAddStr;
            hsmResults{signRow,snCols(2)} = selectedKluster_sn;
            hsmResults{signRow,snCols(3)} = accuracyMat_sn;
            if ~isempty(handsStructMedoids_sn)
                hsmResults{signRow,snCols(4)} = handsStructMedoids_sn.medoidStrings;
            else
                hsmResults{signRow,snCols(4)} = 'ERROR HERE';
            end
        elseif isempty(hsmResults{signRow,snCols(1)}) || strcmpi(hsmResults{signRow,snCols(1)},'-') 
            hsmResults{signRow,snCols(1)} = '-';
            hsmResults{signRow,snCols(2)} = [0 0 0];
            hsmResults{signRow,snCols(3)} = [0;0];
            hsmResults{signRow,snCols(4)} = '-';            
        end
        if ~isempty(selectedKluster_hf)
            hsmResults{signRow,hfCols(1)} = compNameAddStr;
            hsmResults{signRow,hfCols(2)} = selectedKluster_hf;
            hsmResults{signRow,hfCols(3)} = accuracyMat_hf;
            if ~isempty(handsStructMedoids_hf)
                hsmResults{signRow,hfCols(4)} = handsStructMedoids_hf.medoidStrings;
            else
                hsmResults{signRow,hfCols(4)} = 'ERROR HERE';
            end
        elseif isempty(hsmResults{signRow,hfCols(1)}) || strcmpi(hsmResults{signRow,hfCols(1)},'-') 
            hsmResults{signRow,hfCols(1)} = '-';
            hsmResults{signRow,hfCols(2)} = [0 0 0];
            hsmResults{signRow,hfCols(3)} = [0;0];
            hsmResults{signRow,hfCols(4)} = '-';            
        end
        
        %print 11 cells into 
        fprintf(fid_hsmTxt,'%d.SignID = %d\n', signRow, signID);
        %khsCols - 2 and 3
        fprintf(fid_hsmTxt,'  khsNames from(%s) =  %s\n', hsmResults{signRow,khsCols(1)}, hsmResults{signRow,khsCols(2)});
        
        %sn
        printSummary_sn_hf(fid_hsmTxt,hsmResults,signRow,snCols, 'surfaceNormal');
        %hf
        printSummary_sn_hf(fid_hsmTxt,hsmResults,signRow,hfCols, 'histogramFeature');

    end
    save(hsmResults_fileName,'hsmResults');
    fclose(fid_hsmTxt);
end

function printSummary_sn_hf(fid_hsmTxt,hsmResults,signRow,xCols, whichTxt)
    xVec = hsmResults{signRow,xCols(2)};%khsCnt, klusterCnt, accuracy
    xMat = hsmResults{signRow,xCols(3)};%nCols of 2 rows [clusterCnt;acc]
    xStrings = countUniqueCells(hsmResults{signRow,xCols(4)});
    if strcmpi (whichTxt,'surfaceNormal')
        fprintf(fid_hsmTxt,'  ++++\n');
    end
    fprintf(fid_hsmTxt,'  %s results from(%s) = for %d khs, %d clusters and %4.2f accuracy\n', whichTxt, hsmResults{signRow,xCols(1)}, xVec(1), xVec(2), xVec(3));
    if sum(xMat(:))~=0
        fprintf(fid_hsmTxt,'  other clusterResults are : \n');
        fprintf(fid_hsmTxt,'  ');
        fprintf(fid_hsmTxt,'  k(%s)-(%s)acc : \n',mat2str(xMat(1,:)),mat2str(xMat(2,:)));
%         for j=1:size(xMat,2) 
%             fprintf(fid_hsmTxt,'[k(%d),acc(%2.0f)]',xMat(1,j),xMat(2,j));
%         end
%        fprintf(fid_hsmTxt,'\n');
    end
    fprintf(fid_hsmTxt,'  numberOfMedoids : %s \n', xStrings);
    if strcmpi (whichTxt,'surfaceNormal')
        fprintf(fid_hsmTxt,'  ++++\n');
    else
        fprintf(fid_hsmTxt,'******\n******\n');
    end
end

function handsStructMedoids = getHSMF(signFolder, signID, featName, cropMethod)
    handsStructMedoids = [];
    handsStructMedoidsFileName = ['handsStructMedoids_' num2str(signID,'%03d') '_' featName '_' cropMethod '_0_assignedLabelsRemove1.mat'];
    fullFileNameToLoad = [signFolder filesep handsStructMedoidsFileName];
    if 2==exist(fullFileNameToLoad,'file')
        load(fullFileNameToLoad, 'handsStructMedoids');
    end
end

%step04_accuracyListAll_593_sn_mc_0
function [selectedKluster, accuracyMat] = getAccuracyList(signFolder, signID, featName, cropMethod)
    selectedKluster = [];
    accuracyMat = [];
    accuracyListFileName = ['step04_accuracyListAll_' num2str(signID,'%03d') '_' featName '_' cropMethod '_0.mat'];
    fullFileNameToLoad = [signFolder filesep accuracyListFileName];
    if 2==exist(fullFileNameToLoad,'file')
        load(fullFileNameToLoad, 'accuracyListAll');
    else
        return
    end
       
%     %only get the rows with col5 == 100
%     accuracyListAll(accuracyListAll(:,5)~=100,:) = [];
%     
%     if isempty(accuracyListAll)
%         return
%     end
    
    %I will return 2 things
    %1.
    %the selected result as
    %labelCount, klusterCountSelected, accuracy
    selectedKluster = accuracyListAll(1,[3 2 6]);
    %2.
    %accuracyMat as 2 sortedKlusterCount and accuracies
    [~,idx] = sort(accuracyListAll(:,2));
    accuracyMat = round(accuracyListAll(idx,[2 4])');
end
