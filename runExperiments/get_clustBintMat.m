function clustBintMat = get_clustBintMat(signID, hfsnStr)
    clustBintMat = ...
    [... hf1_sn2   sign  kSelected   clustCntMul   linspaceBinCnt
            1        7        5            2             4        ;...1+
            2        7        6            2             4        ;...1++
            1       13        8            2             4        ;...2++ - k8-5%100
            2       13        7            2             4        ;...2++
            1       49        4            2             4        ;...3++
            2       49        5            2             4        ;...3++
            1       69        4            2             4        ;...4++ - k5-2%100
            2       69        5            2             4        ;...4++
            1       74        4            2             4        ;...5++ - k4-3%100
            2       74        5            2             4        ;...5++
            1       85        5            2             4        ;...6++
            2       85        5            2             4        ;...6++
            1       86        7            2             4        ;...7++
            2       86        8            2             4        ;...7++
            1       87        4            2             4        ;...8++ - k4-3%100
            2       87        7            2             4        ;...8++
            1       88        3            2             4        ;...9++ - k3-2%100
            2       88        5            2             4        ;...9++
            1       89        3            2             4        ;...10++
            2       89        5            2             4        ;...10++
            1       96        7            2             4        ;...11++
            2       96       12            3             6        ;...11++
            1      112        7            2             4        ;...12++
            2      112        5            2             4        ;...12++
            1      125        5            2             4        ;...13++
            2      125        5            2             4        ;...13++
            1      221        5            2             4        ;...14++
            2      221        7            2             4        ;...14++
            1      222        4            2             4        ;...15++ - k4-2%100
            2      222        5            2             4        ;...15++
            1      247        5            2             4        ;...16++ - k5-2%100
            2      247        5            2             4        ;...16++
            1      269       10            2             4        ;...17++
            2      269       12            3             6        ;...17++
            1      273       10            3             6        ;...18++
            2      273        8            3             6        ;...18++
            1      277        7            2             4        ;...19++
            2      277        8            2             4        ;...19++
            1      278        5            2             4        ;...20++ - k5-3%100
            2      278        5            2             4        ;...20++
            1      290       15            3             6        ;...21++
            2      290       13            3             6        ;...21++
            1      296        7            2             4        ;...22++
            2      296       10            2             4        ;...22++
            1      310        4            2             4        ;...23++ - k4-3%100
            2      310        5            2             4        ;...23++
            1      335        3            2             4        ;...24++ - k4-2%100
            2      335        5            2             4        ;...24++
            1      339       15            3             6        ;...25++
            2      339       15            3             6        ;...25++
            1      353        4            2             4        ;...26++
            2      353        5            2             4        ;...26++
            1      396        9            3             6        ;...27++
            2      396        8            3             6        ;...27++
            1      403       18            3             6        ;...28++
            2      403       18            3             6        ;...28++
            1      404       10            3             6        ;...29++
            2      404       13            3             6        ;...29++
            1      424        8            2             4        ;...30++
            2      424       10            3             6        ;...30++
            1      428        5            2             4        ;...31++
            2      428        5            2             4        ;...31++
            1      450       -1            2             4        ;...32-
            2      450       -1            2             4        ;...32-
            1      521        8            2             4        ;...33++
            2      521       10            2             4        ;...33++
            1      533        5            2             4        ;...34++ - k5-2%100
            2      533        3            2             4        ;...34++
            1      535        9            3             6        ;...35++
            2      535        9            3             6        ;...35++
            1      536       10            2             4        ;...36++
            2      536       10            2             4        ;...36++
            1      537        5            2             4        ;...37++
            2      537        7            2             4        ;...37++
            1      583        5            2             4        ;...38++ - k5-2%100
            2      583        5            2             4        ;...38++ - k5-2%100
            1      586       13            3             6        ;...39++
            2      586        8            3             6        ;...39++
            1      593        5            2             4        ;...40++
            2      593        8            2             4        ;...40++
            1      636        5            2             4        ;...41++
            2      636        7            2             4         ...41++
    ];%

    clustBintMat = clustBintMat(clustBintMat(:,2)==signID,:);
    switch hfsnStr
        case 'sn'
            clustBintMat = clustBintMat(clustBintMat(:,1)==2,:);
        otherwise
            clustBintMat = clustBintMat(clustBintMat(:,1)==1,:);
    end
end