runStepId = 1;
k_selected = 5;
if runStepId == 1
    disp(['According to labeling results, the first row of k = ' num2str(k_selected) ' is selected with a combined precision of ' num2str(confAccBest_new(1,5),'%4.2f')]);
    OPS_CHSMFAS.askForNewAssignments = false;
    OPS_CHSMFAS.hogVersion = hogVersion;
    OPS_CHSMFAS.deleteDataFile = false;
    createHandsStructMedoidsForASign(srcFold, surSlctListStruct, distMatInfoStruct, clusteringParams, k_selected, OPS_CHSMFAS);
    OPS_CHSMFAS.deleteDataFile = true;
else
    clear('OPS','OPS_CHSMFAS');
    diary('off');
    accuracyListAllResultFolder = [baseClusterResultFolderName '_' fileFolderEnding];
    createFolderIfNotExist(accuracyListAllResultFolder);
    accuracyListAllFullFileName  = [accuracyListAllResultFolder filesep 'step04_accuracyListAll.mat'];     
    accuracyListAllFullFileName2 = [srcFold filesep num2str(signID) filesep 'step04_accuracyListAll_' num2str(signID,'%03d') '_' distMatInfoStruct.featureName  '_' distMatInfoStruct.cropMethod '_' num2str(surSlctListStruct.single0_double1) '.mat'];
    accuracyListAllFullFileName_txt = [srcFold filesep num2str(signID) filesep 'step04_accuracyListAll_' num2str(signID,'%03d') '_' distMatInfoStruct.featureName  '_' distMatInfoStruct.cropMethod '_' num2str(surSlctListStruct.single0_double1) '.txt'];
    diary(accuracyListAllFullFileName_txt);
    disp(datetime('now'));
    disptable(accuracyListAll,'lsi|kCnt|redClassCnt|confAcc|kSpec|combAcc|accumErrByKluster');
    diary('off');
    save(accuracyListAllFullFileName,'accuracyListAll');
    save(accuracyListAllFullFileName2,'accuracyListAll');
    if backupFeatsWhenDone
      moveFiles(signID, 2:5 , struct('HOGFeat','backup'), struct('vidCount',1,'srcFold', srcFold, 'backupFold', backupFold));
    end
end