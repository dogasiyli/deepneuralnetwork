% % signIDList = [7 13 49 69 74 85 86 87 88 89 96 ... 
% %               112 125 221 222 247 269 273 277 278 290 296 ...
% %               310 335 339 353 396 403 404 424 428 ... 450
% %               521 533 535 536 537 583 586 593 636];%
% % signIDList = [339 396 403 535 593];% 533 537 583 593
% OPS_server = struct;
% OPS_server.featName = 'sn';
% OPS_server.cropMethod = 'mc';
% OPS_server.kVec = [];
% OPS_server.runSteps = [4];
% OPS_server.tryMax = 5;
% OPS_server.deleteDataFile = false;
% OPS_server.backupFeatsWhenDone = false;
% OPS_server.checkStep01 = false;
% OPS_server.enforceRecreateCluster_step4 = true;
% OPS_server.checkForBetterCluster_step4 = true;
% OPS_server.saveAllClusterResultImages_step4 = false;
% %[pvRet_00, accuracyListAll_00, resultSummary_00] = hospSign_getDetailedResults(signIDList, OPS_server);
% 
% %1. delete all 'handsStructMedoids_0_'**.mat under srcFold
% %2. delete all 'clusterResults_hf_sc_' and 'clusterResults_hf_mc_' folders
% 
% %3. run above config
% %4. change 
% OPS_server.enforceRecreateCluster_step4 = false;
% %   run 2 times 
% % [pvRet_01, accuracyListAll_01, resultSummary_01] = hospSign_getDetailedResults(signIDList, OPS_server);
% % [pvRet_02, accuracyListAll_02, resultSummary_02] = hospSign_getDetailedResults(signIDList, OPS_server);
% 
% %5. change 
% OPS_server.saveAllClusterResultImages_step4 = true;
% [pvRet_03, accuracyListAll_03, resultSummary_03] = hospSign_getDetailedResults(signIDList, OPS_server);

% OPS_server.enforceRecreateCluster_step4 = true;
% OPS_server.checkForBetterCluster_step4 = false;
% OPS_server.skipClustering_step4 = false;


% %6. run step 5 for transferred signs
% signIDList = [222 533 536 537 583 586 636];
% OPS_server.runSteps = 5;
% OPS_server.enforceRecreateCluster_step4 = false;
% [pvRet_04, accuracyListAll_04, resultSummary_04] = hospSign_getDetailedResults(signIDList, OPS_server);


%signsWithFeats_sn = [74 85 86 87 88 89 96 269 278 450];%17 haziran done [10]
%signsWithFeats_sn = [222 273 404];%18 haziran done[3]
signsWithFeats_sn = [13 49 112 125 221 247 277];%19 haziran done go go go [7]
%signsWithFeats_sn_later = [7 69 290 296 310 335 339 353 396 403 424 428 521 533 535 536 537 583 586 593 636];%[21]

%signsWithFeats_hf = [521 533 535 536 537 583 586 636];%17 haziran done [8]
%signsWithFeats_hf = [222];%18 haziran done [1]
signsWithFeats_hf = [7 13 49 69 85 86 87 88 89 96 112 125 221 247 269 273 277 278 296 310 335 339 353 424 428];%19 haziran go go go [25]
%signsWithFeats_hf_later = [74 396 404];%backup feats to transfer - [3]
% HF -> 403 450 think about them, 593 hogFeats at laptop, 290 ??? [4]

OPS_wsu = struct;
OPS_wsu.featName = 'sn';
OPS_wsu.cropMethod = 'mc';
OPS_wsu.kVec = [];
OPS_wsu.runSteps = 5;
OPS_wsu.tryMax = 5;
OPS_wsu.deleteDataFile = false;
OPS_wsu.checkStep01 = false;
OPS_wsu.frameLabelingStrategies = 'singleFrame_1nn';
OPS_wsu.handsStructMedoids_step5 = 'all';
%
OPS_wsu.backupFeatsWhenDone = true;
OPS_wsu.onlyCalcDistIfSameHandShape_step5 = true;
OPS_wsu.skipCalcDistIfUnlabelled_step5 = true;

% [pvRet_wsu, accuracyListAll_03, resultSummary_03] = hospSign_getDetailedResults(signsWithFeats_sn, OPS_wsu);

OPS_lmb = OPS_wsu;
OPS_lmb.featName = 'hf';
OPS_lmb.backupFeatsWhenDone = true;
OPS_lmb.onlyCalcDistIfSameHandShape_step5 = true;
OPS_lmb.skipCalcDistIfUnlabelled_step5 = true;
[pvRet_lmb, accuracyListAll_03, resultSummary_03] = hospSign_getDetailedResults(signsWithFeats_hf, OPS_lmb);

