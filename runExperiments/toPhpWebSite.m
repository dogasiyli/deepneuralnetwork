function [resultTable, handsStructMedoids_all, pv] = toPhpWebSite(OPS)
%INSERT INTO `motions`(`id`, `numberof_case`, `nameof_motion`, `variation`, `rgbordeep`, `grp_element`, `picname`, `picture`, `cent_id`) VALUES ([value-1],[value-2],[value-3],[value-4],[value-5],[value-6],[value-7],[value-8],[value-9])
% 1 - id - nan
% 2 - numberof_case : signID
% 3 - nameof_motion : handShapeName
% 4 - variation : id of handShapeName variation for a sign
% 5 - rgbordeep : rgb - get rgb image, save it with thhe col-1 id as rgb_<id>_<signID>_<handShapeName>_<variationId>
% 6 - grp_element : the id if selected as unique from all handsStructMedoids
% 7 - picname : blob
% 8 - cent_id

    if ~exist('OPS', 'var') || ~isstruct(OPS)
        OPS = [];
    end
    srcFold             = getStructField(OPS, 'srcFold', getVariableByComputerName('srcFold'));
    featName            = getStructField(OPS, 'featName', 'sn');
    cropMethod          = getStructField(OPS, 'cropMethod', 'mc');
    signIDList          = getStructField(OPS, 'signIDList', getSignListAll());
    toFolder            = getStructField(OPS, 'toFolder', [srcFold filesep 'experimentResults']);
    exportImages        = getStructField(OPS, 'exportImages', false);
    single0_double1     = getStructField(OPS, 'single0_double1', 0);
    labelStrategy       = getStructField(OPS, 'labelStrategy', 'assignedLabelsRemove1');
    
    if exportImages
        summaryPngsFolder = [toFolder filesep 'summaryPngs'];
        createFolderIfNotExist(toFolder, true);
        createFolderIfNotExist(summaryPngsFolder, true);
        removePrevImages(toFolder);
        removePrevImages(summaryPngsFolder);
    end
    
    handsStructMedoids_all = [];
    %handsStructMedoids_fNameBase = ['handsStructMedoids_' featName '_' cropMethod '_' num2str(single0_double1) '_' labelStrategy '.mat'];
    signCount = length(signIDList);
    resultTable = cell(signCount,7);
    entryID = 0;
    
    pv = [];
    for i=1:signCount
        signID = signIDList(i);
        signFolder = [srcFold filesep num2str(signID)];
        handsStructMedoidsFileName = ['handsStructMedoids_' num2str(signID,'%03d') '_' featName '_' cropMethod '_0_assignedLabelsRemove1.mat'];
        
        fullFileNameToLoad = [signFolder filesep handsStructMedoidsFileName];
        if 2==exist(fullFileNameToLoad,'file')
            load(fullFileNameToLoad, 'handsStructMedoids');
        else
            continue
        end
        
        if exportImages
            summaryPngsFileName = [summaryPngsFolder filesep 's' num2str(signID) '.png'];
            summaryImageNotFound = true;
            si_u = 2;
            si_r = 1;
            summaryImage = zeros(200,200,3); 
            while si_u<8 && summaryImageNotFound
                while si_r<10 && summaryImageNotFound
                    summaryImage_fn = [signFolder filesep 'User_' num2str(si_u) '_' num2str(si_r) filesep 'summaryLH.bmp'];
                    if exist(summaryImage_fn,'file')==2
                        summaryImage = imread(summaryImage_fn);
                        summaryImageNotFound = false;
                    end
                    si_r = si_r + 1;
                end
                si_u = si_u + 1;
            end
            imwrite(summaryImage, summaryPngsFileName);
        end
        
        detLabs         = handsStructMedoids.detailedLabels;
        medoidStrings   = handsStructMedoids.medoidStrings;
        medoidCnt = length(medoidStrings);
        
        depthIm_OPS = struct('figID', 1,'rejectPatchMethod', 'notIncludeHand','normCalcMethod', 'haar-like','changeGivenValues', false,'cropAreaStrCell', {'BH_sml','RH_sml','LH_sml'});
        
        for j=1:medoidCnt
            handShapeName = medoidStrings{j};
            variationID = getVariationID(medoidStrings, j);
            
            handsStructMedoids_all = insertIntoHandStructMedoidsAll(handsStructMedoids_all, ...
                handsStructMedoids.dataCells(j,1), handsStructMedoids.detailedLabels(j,:), handsStructMedoids.imCropCells(j,1), ...
                handsStructMedoids.minDistAllowedPerMedoid(j), handsStructMedoids.maxDistAllowedPerMedoid(j), handsStructMedoids.medoidStrings(1,j));
            
            entryID = entryID + 1;
            resultTable_col6 = getVariationID(handsStructMedoids_all.medoidStrings, entryID);
            yesNoStr = isGroupElement(handShapeName, medoidStrings);

            resultTable{entryID,1} = entryID;
            resultTable{entryID,2} = signID;
            resultTable{entryID,3} = handShapeName;
            resultTable{entryID,4} = variationID;
            resultTable{entryID,5} = 'rgb'; % also decide on getting depth to this list
            resultTable{entryID,6} = resultTable_col6; %get this one from handsStructMedoids_all.medoidStrings
            resultTable{entryID,7} = ['s' num2str(signID) '_' handShapeName '_' num2str(variationID) '_rgb_' yesNoStr '.png'];
            resultTable{entryID,8} = yesNoStr;
            
            if exportImages
                try
                    userFold = [signFolder filesep 'User_' num2str(detLabs(j,2)) '_' num2str(detLabs(j,3)) ];
                    [faceNormCells, quiverMatCells, blockAreaSizeCells, rgbImgAsFaceNormCells, m, quiverH_out] = getSurfNorm(userFold, detLabs(j,5), [20 20], depthIm_OPS);
                    quiverIm = quiverH_out{4-detLabs(j,4)};
                    depthIm = rgbImgAsFaceNormCells{4-detLabs(j,4)};
                    depthIm_resized = resizeImg_IntoBox(depthIm, [200 200], true);
                catch
                    depthIm_resized = zeros(200,200,3); 
                    quiverIm = [];
                    pv = [pv;-5 detLabs(j,[1 2 3])];
                end

                %save image to folder with the resultTable{entryID,7} name
                saveToName = [toFolder filesep resultTable{entryID,7}];
                saveToName_depth = strrep(saveToName,'_rgb_','_dpt_');
                saveToName_quiver = strrep(saveToName,'_rgb_','_qui_');

                im = retrieveImageFromDetailedLabels(srcFold, 1, detLabs(j,:));
                if isempty(im)
                    im = zeros(200,200,3);
                    pv = [pv;+5 detLabs(j,[1 2 3])];
                end
                imwrite(im, saveToName);
                imwrite(depthIm_resized, saveToName_depth);

                if ~isempty(quiverIm)
                    quiverIm = visualizeNormals(quiverIm.cropImFilled, quiverIm.quiverMat, [], struct('figVec',53,'showColorBar',false));
                    saveas(quiverIm, saveToName_quiver);
                else
                    quiverIm = zeros(200,200,3);
                    imwrite(quiverIm, saveToName_quiver);
                end
            end
        end
    end
    
    %handsStructMedoids_allFileName = 'handsStructMedoids_sn_mc_0_assignedLabelsRemove1.mat';
    handsStructMedoids_allFileName = [srcFold filesep 'handsStructMedoids_' featName '_' cropMethod '_' num2str(single0_double1) '_' labelStrategy '.mat'];
    handsStructMedoids = handsStructMedoids_all;
    save(handsStructMedoids_allFileName,'handsStructMedoids');
end

function yesNoStr = isGroupElement(handShapeName, medoidStrings)
    srcVec = strcmpi(handShapeName,medoidStrings);
    variationID = sum(srcVec);
    if variationID>1
        yesNoStr = 'yes';
    else
        yesNoStr = 'no';
    end
end

function variationID = getVariationID(medoidStrings, colID)
    stringToSearch = medoidStrings{colID};
    srcVec = strcmpi(stringToSearch,medoidStrings);
    variationID = sum(srcVec(1:colID));
end

function removePrevImages(imageFolder)
    [fileNames, numOfFiles] = getFileList(imageFolder, '.png');
    for i=1:numOfFiles
        fn = [imageFolder filesep fileNames{i}];
        delete(fn);
    end
end

function handsStructMedoids = insertIntoHandStructMedoidsAll(handsStructMedoids, dataCell, detailedLabelsRow, imCropCell, minDistAllowedPerMedoid, maxDistAllowedPerMedoid, clusterNameMedoid)
    if isempty(handsStructMedoids)
        handsStructMedoids = struct;
        handsStructMedoids.dataCells = dataCell;
        handsStructMedoids.detailedLabels = detailedLabelsRow;
        handsStructMedoids.imCropCells = imCropCell;
        handsStructMedoids.minDistAllowedPerMedoid = minDistAllowedPerMedoid;
        handsStructMedoids.maxDistAllowedPerMedoid = maxDistAllowedPerMedoid;
        handsStructMedoids.medoidStrings = clusterNameMedoid;
    else
        cnt = length(handsStructMedoids.minDistAllowedPerMedoid)+1;
        handsStructMedoids.dataCells{cnt,1} = dataCell;
        handsStructMedoids.detailedLabels(cnt,:) = detailedLabelsRow;
        handsStructMedoids.imCropCells{cnt,1} = imCropCell;
        handsStructMedoids.minDistAllowedPerMedoid(1,cnt) = minDistAllowedPerMedoid;
        handsStructMedoids.maxDistAllowedPerMedoid(1,cnt) = maxDistAllowedPerMedoid;
        handsStructMedoids.medoidStrings(1,cnt) = clusterNameMedoid;
    end
end

