function [reAssLimitRows, toChangeRowsSorted] = updateMostReassignedClusterFrames(srcFold, signID, kVec, toChangeRows)
    toChangeRowsSorted = sortrows(toChangeRows,1:5);
    
    dif_cluster_experiment_count = length(kVec);
    reAssLimit = round(dif_cluster_experiment_count/2);
    
    reAssLimitRows = toChangeRows(toChangeRows(:,8)>reAssLimit,2:7);
    vidIDs = reAssLimitRows(:,1)*100 + reAssLimitRows(:,2)*10 + reAssLimitRows(:,3);
    uniqVids = unique(vidIDs);
    uniqVidCnt = length(uniqVids);
    LR_cell = {'L','R'};
    for vi = 1:uniqVidCnt
        vidIDCur = uniqVids(vi);
        l_r = mod(vidIDCur,10);
        vidIDCur = (vidIDCur-l_r)/10;
        r = mod(vidIDCur,10);
        vidIDCur = (vidIDCur-r)/10;
        u = vidIDCur;
        
        userFold = [srcFold filesep num2str(signID) filesep 'User_' num2str(u) '_' num2str(r)];
        labelFileNameFullName = [userFold filesep 'labelFiles' filesep 'labelsCS_' LR_cell{l_r} 'H.txt'];
        lv_old = getLabelVecFromFile(labelFileNameFullName,false);
        lv_new = lv_old;
        
        relatedRows = find(vidIDs==uniqVids(vi));
        disp(['s' num2str(signID) 'u' num2str(u) 'r' num2str(r) ' --> frameCnt : ' num2str(length(relatedRows))]);
        cfStr = '';
        for ri = 1:length(relatedRows)
            rowToChangeCur = reAssLimitRows(relatedRows(ri),:);
            frameID = rowToChangeCur(4);
            oldLabel_1 = rowToChangeCur(5);
            labelToSet = rowToChangeCur(6)+1;
            old_lv_old = lv_old(frameID);
            if old_lv_old-1~=oldLabel_1
                warning('we wouldnt be here- do nothing');
            else
                lv_new(frameID) = labelToSet;
                cfStr = [cfStr ',f' num2str(frameID) '(' num2str(old_lv_old) '-' num2str(labelToSet) ')' ]; %#ok<AGROW>
            end
        end
        disp(cfStr);
        writeLabelVecToFile(userFold, lv_new, l_r, true);
        %update label file
    end
end

