function runFirst(setCurrentFolderPathID, colorSchemaID)
%% runFirst This function creates global variables that assigns some folder paths to some specific variables

% These functions can then be used to read/write/addpath
% example : cd('D:\GitHub\deepneuralnetwork');runFirst('dnnCodePath');

    mainDirectory = pwd;
    addpath([mainDirectory  filesep 'helperFuncs' filesep]);
    allSubfolders = getFolderList(mainDirectory, false, true);
    cellCnt = size(allSubfolders,1);
    for i = 1:cellCnt
        disp(['Adding (' allSubfolders{i} ') and all its subfolders to working directory.']);
        addpath(genpath(allSubfolders{i}));
    end
    
    global bigDataPath;
    global minFuncPath;
    global mnistDataPath;
    global dnnCodePath;
    global fastDiskTempPath;
    global outputFolderPath;
    global dropboxPath;
    global cifar10Folder;
    
    if (nargin >=2 && exist('colorSchemaID','var') && colorSchemaID>=1 && colorSchemaID<=2)
        setColorSchema(colorSchemaID);
    end
    
    compName = getComputerName;
    switch compName
        case {'LUMBAR'}
            %WorkPC  
            bigDataPath = 'D:\matlabStuff\otherFolders\bigDataPath\';
            minFuncPath = 'D:\matlabStuff\deepneuralnetwork\minfunc\';
            mnistDataPath = 'D:\matlabStuff\deepneuralnetwork\mnist_data\';
            dnnCodePath = 'D:\matlabStuff\deepneuralnetwork\';
            fastDiskTempPath = 'D:\matlabStuff\otherFolders\FastDiskTemp\';
            outputFolderPath = 'D:\matlabStuff\otherFolders\outputFolder\';
            dropboxPath = 'D:\matlabStuff\otherFolders\dropboxFolder\';
            cifar10Folder = 'D:\matlabStuff\otherFolders\CIFAR10\';            
        case {'Win7_WS_64_UL'}
            %WorkPC  
            bigDataPath = 'D:\';
            minFuncPath = 'D:\BitBucketRepo\deepneuralnetwork\minfunc\';
            mnistDataPath = 'D:\BitBucketRepo\deepneuralnetwork\mnist_data\';
            dnnCodePath = 'D:\BitBucketRepo\deepneuralnetwork\';
            fastDiskTempPath = 'D:\FastDiskTemp\';
            outputFolderPath = 'D:\outputFolder\';
        case {'DESKTOP-GCOE5AL','DGMSISSD','q-Bilgisayar',}
            %dgssdmsi  
            bigDataPath = 'D:\Datasets\';
            minFuncPath = 'D:\GitHub\deepneuralnetwork\minFunc\';
            mnistDataPath = 'D:\GitHub\deepneuralnetwork\MNIST_data\';
            dnnCodePath = 'D:\GitHub\deepneuralnetwork\';
            fastDiskTempPath = 'D:\FastDiskTemp\';
            outputFolderPath = 'D:\outputFolder\';
            dropboxPath = 'C:\Users\dgssdmsi\OneDrive\Belgeler\';
            cifar10Folder = 'D:\Datasets\CIFAR10\';
        case {'IB7702060904'}
            %dho-08
            bigDataPath = 'D:\Data\';
            minFuncPath = 'D:\Bitbucket\deepneuralnetwork\minFunc\';
            mnistDataPath = 'D:\Bitbucket\deepneuralnetwork\mnist_data\';
            dnnCodePath = 'D:\Bitbucket\deepneuralnetwork\';
            fastDiskTempPath = 'D:\FastDiskTemp\';
            outputFolderPath = 'D:\outputFolder\';
            dropboxPath = 'D:\Bitbucket\tutorial_20150728\dropboxFolder\';
            cifar10Folder = 'D:\Data\CIFAR10\';
        case {'DESKTOP-2AQGRCR'}
            %dho-dg_hv
            bigDataPath = 'D:\Data\';
            minFuncPath = 'D:\Bitbucket\deepneuralnetwork\minFunc\';
            mnistDataPath = 'D:\Bitbucket\deepneuralnetwork\mnist_data\';
            dnnCodePath = 'D:\Bitbucket\deepneuralnetwork\';
            fastDiskTempPath = 'D:\FastDiskTemp\';
            outputFolderPath = 'D:\outputFolder\';
            dropboxPath = 'D:\OneDrive\Belgeler\';
            cifar10Folder = 'D:\Data\CIFAR10\';
        case {'dgssdwork'}
            %dgssdwork 
            bigDataPath = 'G:\Data\';
            minFuncPath = 'G:\BitbucketRepo\deepneuralnetwork\minFunc\';
            mnistDataPath = 'G:\BitbucketRepo\deepneuralnetwork\mnist_data\';
            dnnCodePath = 'G:\BitbucketRepo\deepneuralnetwork\';
            fastDiskTempPath = 'C:\FastDiskTemp\';
            outputFolderPath = 'C:\outputFolder\';
            dropboxPath = 'G:\BitbucketRepo\tutorial_20150728\dropboxFolder\';
            cifar10Folder = 'G:\Data\CIFAR10\';
        case {'dogamsilaptop'}
            %dogamsilaptop  
            bigDataPath = 'D:\Data\';
            minFuncPath = 'D:\Bitbucket\deepneuralnetwork\minFunc\';
            mnistDataPath = 'D:\Bitbucket\deepneuralnetwork\mnist_data\';
            dnnCodePath = 'D:\Bitbucket\deepneuralnetwork\';
            fastDiskTempPath = 'D:\FastDiskTemp\';
            outputFolderPath = 'D:\outputFolder\';
            dropboxPath = 'D:\Bitbucket\tutorial_20150728\dropboxFolder\';
            cifar10Folder = 'D:\Data\CIFAR10\';
        case {'workacerssd'}
            %workacerssd  
            bigDataPath = 'D:\Data\';
            minFuncPath = 'D:\BitbucketRepo\deepneuralnetwork\minfunc\';
            mnistDataPath = 'D:\Data\mnist_data\';
            dnnCodePath = 'D:\BitbucketRepo\deepneuralnetwork\';
            fastDiskTempPath = 'D:\FastDiskTemp\';
            outputFolderPath = 'D:\outputFolder\';
            dropboxPath = 'D:\BitbucketRepo\tutorial_20150728\dropboxFolder\';
            cifar10Folder = 'D:\Data\CIFAR10\';
        case {'doga-MSISSD'}
            %school-msi-ubuntu 
            gitHubPath = '/mnt/USB_HDD_1TB/GitHub';%gitHubPath = '/home/doga/GithUBuntU';
            diskPath = '/mnt/USB_HDD_1TB';
            diskPath2 = '/mnt/SSD_J';
            bigDataPath = [diskPath '/FromUbuntu/'];
            minFuncPath = [gitHubPath '/deepneuralnetwork/minfunc/'];
            mnistDataPath = [gitHubPath '/deepneuralnetwork/mnist_data'];
            dnnCodePath = [gitHubPath '/deepneuralnetwork/'];
            fastDiskTempPath = [diskPath2 '/FastDiskTemp/'];
            outputFolderPath = [diskPath '/outputFolder/'];
            dropboxPath = [diskPath '/GitHub/tutorial_20150728/dropboxFolder/'];
            cifar10Folder = [diskPath '/Datasets/CIFAR10/'];
        case {'doga-msi-ubu'}
            %school-msi-ubuntu 
            bigDataPath = '/media/doga/Data/FromUbuntu/';
            minFuncPath = '/home/doga/GithUBuntU/deepneuralnetwork/minfunc/';
            mnistDataPath = '/home/doga/GithUBuntU/deepneuralnetwork/mnist_data';
            dnnCodePath = '/home/doga/GithUBuntU/deepneuralnetwork/';
            fastDiskTempPath = '/media/doga/Data/FastDiskTemp/';
            outputFolderPath = '/media/doga/Data/outputFolder/';
            dropboxPath = '/media/doga/Data/GitHub/tutorial_20150728/dropboxFolder/';
            cifar10Folder = '/media/doga/Data/Data/CIFAR10/';
        case {'WsUbuntu05','wsubuntu'}
            %work station 05 at DHO 
            gitHubPath = '/home/wsubuntu/github';
            dataPath = '/media/wsubuntu/SSD_Data/DataPath';
            minFuncPath = [gitHubPath '/deepneuralnetwork/minfunc/'];
            mnistDataPath = [gitHubPath '/deepneuralnetwork/mnist_data'];
            dnnCodePath = [gitHubPath '/deepneuralnetwork/'];
            dropboxPath = [gitHubPath '/tutorial_20150728/dropboxFolder/'];
            bigDataPath = [dataPath '/FromUbuntu/'];
            fastDiskTempPath = [dataPath '/FastDiskTemp/'];
            outputFolderPath = [dataPath '/outputFolder/'];
            cifar10Folder = [dataPath '/Data/CIFAR10/'];            
        case {'WsUbuntu05'}
            %work station 05 at DHO 
            gitHubPath = '/home/dg/github';
            dataPath = '/home/dg/DataPath';
            minFuncPath = [gitHubPath '/deepneuralnetwork/minfunc/'];
            mnistDataPath = [gitHubPath '/deepneuralnetwork/mnist_data'];
            dnnCodePath = [gitHubPath '/deepneuralnetwork/'];
            dropboxPath = [gitHubPath '/tutorial_20150728/dropboxFolder/'];
            bigDataPath = [dataPath '/FromUbuntu/'];
            fastDiskTempPath = [dataPath '/FastDiskTemp/'];
            outputFolderPath = [dataPath '/outputFolder/'];
            cifar10Folder = [dataPath '/Data/CIFAR10/'];            
        otherwise
            error(['Unknown computer name(' compName ')']);
    end
    
    addAllPaths;

%setCurrentFolderPathID
    if (nargin>0 && exist('setCurrentFolderPathID','var') && ~isempty(setCurrentFolderPathID))
        switch setCurrentFolderPathID
            case {'bigDataPath'}
                cd(bigDataPath);
            case {'minFuncPath'}
                cd(minFuncPath);
            case {'mnistDataPath'}
                cd(mnistDataPath);
            case {'dnnCodePath'}
                cd(dnnCodePath);
            case {'fastDiskTempPath'}
                cd(fastDiskTempPath);
            case {'dropboxPath'}
                cd(dropboxPath);
            case {'cifar10Folder'}
                cd(cifar10Folder);
            otherwise
                if exist(setCurrentFolderPathID,'dir')
                    cd(setCurrentFolderPathID);
                else
                    warning(['Unknown path(' setCurrentFolderPathID '). Not changing the directory.']);
                end
        end
    end
end

function addAllPaths
    global bigDataPath;
    if exist(bigDataPath,'dir')
        addpath(bigDataPath);
        disp(['(' bigDataPath ') is added to path as bigDataPath.'])
    else
        warning([bigDataPath ' is not a valid path for now. bigDataPath is assigned empty']);
        bigDataPath = [];
    end
    
    global minFuncPath;
    if exist(minFuncPath,'dir')
        addpath(minFuncPath);
        disp(['(' minFuncPath ') is added to path as minFuncPath.'])
    else
        warning([minFuncPath ' is not a valid path for now. minFuncPath is assigned empty']);
        minFuncPath = [];
    end
    
    global mnistDataPath;
    if exist(mnistDataPath,'dir')
        addpath(mnistDataPath);
        disp(['(' mnistDataPath ') is added to path as mnistDataPath.'])
    else
        warning([mnistDataPath ' is not a valid path for now. mnistDataPath is assigned empty']);
        mnistDataPath = [];
    end
    
    global dnnCodePath;
    if exist(dnnCodePath,'dir')
        addpath(dnnCodePath);
        disp(['(' dnnCodePath ') is added to path as dnnCodePath.'])
    else
        warning([dnnCodePath ' is not a valid path for now. dnnCodePath is assigned empty']);
        dnnCodePath = [];
    end
    
    global fastDiskTempPath;
    if exist(fastDiskTempPath,'dir')
        addpath(fastDiskTempPath);
        disp(['(' fastDiskTempPath ') is added to path as fastDiskTempPath.'])
    else
        folderCreationStatus = mkdir(fastDiskTempPath);
        if (folderCreationStatus)
            addpath(fastDiskTempPath);
            disp(['(' fastDiskTempPath ') is created and added to path as fastDiskTempPath.'])
        else
            warning([fastDiskTempPath ' could not be created and fastDiskTempPath is assigned empty']);
        end
    end    
    
    global outputFolderPath;
    if exist(outputFolderPath,'dir')
        addpath(outputFolderPath);
        disp(['(' outputFolderPath ') is added to path as outputFolderPath.'])
    else
        folderCreationStatus = mkdir(outputFolderPath);
        if (folderCreationStatus)
            addpath(outputFolderPath);
            disp(['(' outputFolderPath ') is created and added to path as outputFolderPath.'])
        else
            warning([outputFolderPath ' could not be created and outputFolderPath is assigned empty']);
        end
    end

    global dropboxPath;
    if exist(dropboxPath,'dir')
        addpath(dropboxPath);
        disp(['(' dropboxPath ') is added to path as dropboxPath.'])
    else
        folderCreationStatus = mkdir(dropboxPath);
        if (folderCreationStatus)
            addpath(dropboxPath);
            disp(['(' dropboxPath ') is created and added to path as dropboxPath.'])
        else
            warning([dropboxPath ' could not be created and dropboxPath is assigned empty']);
        end
    end    
    
    global cifar10Folder;
    if exist(cifar10Folder,'dir')
        addpath(cifar10Folder);
        disp(['(' cifar10Folder ') is added to path as cifar10Folder.'])
    else
        warning([cifar10Folder ' is not a valid path for now. cifar10Folder is assigned empty']);
        cifar10Folder = [];
    end    
    
end