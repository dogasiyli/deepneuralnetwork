function newFilterImage = createFilterByDilatation(initialLayerPatterns, pt1, pt2, figureID)
%createFilterByDilatation create a new pattern by dilating one filter with
%one another
%   Detailed explanation goes here
    pt1_fig = reshape(initialLayerPatterns(:,pt1),5,5);
    pt2_fig = reshape(initialLayerPatterns(:,pt2),5,5);
    %newFilterImage = imdilate(pt1_fig, pt2_fig, 'full');
    newFilterImage = imDilateFull(pt1_fig, pt2_fig);

    if (exist('figureID','var') && figureID>0)
        figure(figureID);clf;
        colormap('Gray');

        subplot(2,2,1);
        imagesc(pt1_fig);
        title(['pt' num2str(pt1) '_{fig}']);
        subplot(2,2,3);
        imagesc(pt2_fig);
        title(['pt' num2str(pt2) '_{fig}']);

        subplot(2,2,[2 4]);
        imagesc(newFilterImage);
        title(['imdilate(pt' num2str(pt1) '_{fig}, pt' num2str(pt2) '_{fig}, "full")']);
    end
end
