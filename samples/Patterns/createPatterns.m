function initialLayerPatterns = createPatterns(figureID)
%createPatterns Creates the patterns of the initial layer
%   Using the output of this function
%   We will use imdilate function to create new patterns and images

%look at : Patterns.xlsx to see what the following is all about
initialLayerPatterns = [
0	0	1	0	0	1	0	0	0	0	1	0	0	0	1;
0	0	0	1	0	0	0	0	0	0	1	0	0	0	0;
1	0	0	1	1	0	1	0	1	1	1	0	0	0	0;
0	0	0	1	0	0	0	0	0	0	1	0	0	0	0;
0	1	0	0	0	0	0	1	0	0	1	0	0	0	1;
0	0	0	1	0	1	0	0	0	0	0	0	0	0	0;
0	0	1	0	1	0	0	0	1	1	0	0	0	1	1;
1	0	0	0	0	0	1	0	0	1	0	0	0	1	0;
0	1	0	0	1	0	0	0	1	1	0	0	0	1	1;
0	0	0	1	0	0	0	1	0	0	0	0	0	0	0;
1	0	0	1	1	1	0	0	0	1	0	1	0	1	0;
1	0	0	0	0	0	0	0	1	1	0	1	0	0	0;
1	1	1	0	0	0	1	0	0	0	0	1	0	0	1;
1	0	0	0	0	0	0	0	1	1	0	1	0	0	0;
1	0	0	1	1	0	0	1	0	1	0	1	0	1	0;
0	0	0	1	0	1	0	0	0	0	0	0	0	0	0;
0	1	0	0	1	0	0	0	1	1	0	0	0	1	1;
1	0	0	0	0	0	1	0	0	1	0	0	0	1	0;
0	0	1	0	1	0	0	0	1	1	0	0	0	1	1;
0	0	0	1	0	0	0	1	0	0	0	0	0	0	0;
0	1	0	0	0	1	0	0	0	0	0	0	1	0	1;
0	0	0	1	0	0	0	0	0	0	0	0	1	0	0;
1	0	0	1	1	0	1	0	1	1	0	0	1	0	0;
0	0	0	1	0	0	0	0	0	0	0	0	1	0	0;
0	0	1	0	0	0	0	1	0	0	0	0	1	0	1
];

if (exist('figureID','var') && figureID>0)
    figure(figureID);clf;
    rc = 3;
    cc = 5;
    colormap('Gray');
    for r=1:rc
        for c = 1:cc
            spid = (r-1)*cc + c;
            subplot(rc,cc,spid);
            imagesc(reshape(initialLayerPatterns(:,spid),5,5));
            title(['Pattern(' num2str(spid) ')']);
        end
    end
end

end

