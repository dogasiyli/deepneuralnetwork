function dilatedImage = imDilateFull(penFig, plateFig)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

%Dilate penFig with plateFig
% for each 1 in 'plateFig'
% draw penFig
% for 0 do nothing
    [plR,plC] = size(plateFig);
    [peR,peC] = size(penFig);

    outR = plR + peR-1;
    outC = plC + peC-1;
    
    dilatedImage = zeros(outR,outC);
    for r = 1:plR
        for c = 1:plC
            if (plateFig(r,c)==1)
                rArea = r:r+peR-1;
                cArea = c:c+peC-1;
                dilatedImage(rArea,cArea) = dilatedImage(rArea,cArea) + penFig;
            end
        end
    end

    dilatedImage(dilatedImage>0) = 1;
end
