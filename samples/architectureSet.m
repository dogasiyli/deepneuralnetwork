function cnnnet = architectureSet( architectureIdentifier,  varargin )
%architectureSet Initial setup for a cnnnet
%   creates a cnnet according to the identifier passed to the function
    
    %P1D0 -> Passed assed paramaeter = 1, Default value used = 0
    [ displayOpts,  priorInfoUse,  softMaxInitialIteration, P1D0] = parseArgs(...
    {'displayOpts' 'priorInfoUse' 'softMaxInitialIteration'},...
    {      -1            'none'                   10       },...
    varargin{:}); 
    if P1D0.displayOpts==0
        displayOpts = setDisplayOpts('all');
    end

    switch architectureIdentifier
        case 'CIFAR10_DEEP04'
            %filtCnt = [10 10 12 15 40 50];%S12
            %filtCnt = [4 4 5 6 15 25];%S16,L16
            filtCnt = [4 4 4 4 10 10];%L12
            varargin_1 = {  'activationType', 'relu'...
                          , 'initializeWeightMethod', 'AE_PB'...
                          , 'applyOrthonormalization', true...
                          , 'wvnMode','epoch'...
                          , 'bnMode','batch'...
                          , 'filterStatsTrackMode', 'perBatchIteration'...
                          , 'stride', [1 1]}; 
            varargin_2 = varargin_1;
            varargin_2{4} = 'Random';%'initializeWeightMethod'
            varargin_2{6} = true;%'applyOrthonormalization'
            %1,2
            cnnnet = appendNetworkLayer( [], 'convolution', displayOpts, 'countFilt', filtCnt(1), 'inputChannelSize', 3, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %3,4
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(2), 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %5,6
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(3), 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %7,8
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(4), 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %9
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [2 2], 'poolType', 'mean' );
            %10,11
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(5), 'filterSize', [3 3], varargin_2{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %12,13
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(6), 'filterSize', [5 5], varargin_2{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %14
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [6 6], 'poolType', 'mean' );
            %15
            cnnnet = appendNetworkLayer( cnnnet, 'softmax', displayOpts, 'initializeWeightMethod', 'Random', 'minFuncMaxIter', softMaxInitialIteration ,'priorInfoUse', priorInfoUse);
        case 'CIFAR10_DEEP04_PCA'
            filtCnt = [10 10 12 15 40 50];
            varargin_1 = {  'activationType', 'relu'...
                          , 'initializeWeightMethod', 'PCA'...
                          , 'applyOrthonormalization', true...
                          , 'wvnMode','epoch'...
                          , 'bnMode','batch'...
                          , 'filterStatsTrackMode', 'perBatchIteration'...
                          , 'stride', [1 1]}; 
            varargin_2 = varargin_1;
            varargin_2{4} = 'Random';%'initializeWeightMethod'
            varargin_2{6} = true;%'applyOrthonormalization'
            %1,2
            cnnnet = appendNetworkLayer( [], 'convolution', displayOpts, 'countFilt', filtCnt(1), 'inputChannelSize', 3, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %3,4
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(2), 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %5,6
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(3), 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %7,8
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(4), 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %9
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [2 2], 'poolType', 'mean' );
            %10,11
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(5), 'filterSize', [3 3], varargin_2{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %12,13
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(6), 'filterSize', [5 5], varargin_2{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %14
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [6 6], 'poolType', 'mean' );
            %15
            cnnnet = appendNetworkLayer( cnnnet, 'softmax', displayOpts, 'initializeWeightMethod', 'Random', 'minFuncMaxIter', softMaxInitialIteration ,'priorInfoUse', priorInfoUse);
        case 'CIFAR10_HV01_PCA_All'
            filtCnt = [32 64 64 64];%H01, H03->H26
            varargin_1 = {  'activationType', 'relu'...
                          , 'initializeWeightMethod', 'PCA'...
                          , 'applyOrthonormalization', true...
                          , 'wvnMode','epoch'...
                          , 'bnMode','batch'...
                          , 'filterStatsTrackMode', 'perBatchIteration'...
                          , 'stride', [1 1]}; 
            %1,2
            cnnnet = appendNetworkLayer( [], 'convolution', displayOpts, 'countFilt', filtCnt(1), 'inputChannelSize', 3, 'filterSize', [5 5], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %3,4
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(2), 'filterSize', [5 5], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %5,6
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [2 2], 'poolType', 'mean' );
            %10,11
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(3), 'filterSize', [5 5], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %12,13
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(4), 'filterSize', [5 5], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %14
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [4 4], 'poolType', 'mean' );
            %15
            cnnnet = appendNetworkLayer( cnnnet, 'softmax', displayOpts, 'initializeWeightMethod', 'Random', 'minFuncMaxIter', softMaxInitialIteration ,'priorInfoUse', priorInfoUse);
        case 'CIFAR10_DEEP04_PCA_All'
            filtCnt = [10 10 12 15 40 50];%H02,H16
            %filtCnt = [4 4 5 6 15 25];%D02,S54
            %filtCnt = [4 4 5 6 20 40];%S24,L19
            %filtCnt = [4 4 4 4 10 10];%S25->S38, L20, S42->S55
            varargin_1 = {  'activationType', 'relu'...
                          , 'initializeWeightMethod', 'PCA'...
                          , 'applyOrthonormalization', true...
                          , 'wvnMode','epoch'...'none' for L19, initial for S54, epoch for others
                          , 'bnMode','batch'...'epoch' for L19-S54-H13, batch for others
                          , 'filterStatsTrackMode', 'perBatchIteration'...
                          , 'stride', [1 1]}; 
            %1,2
            cnnnet = appendNetworkLayer( [], 'convolution', displayOpts, 'countFilt', filtCnt(1), 'inputChannelSize', 3, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %3,4
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(2), 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %5,6
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(3), 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %7,8
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(4), 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %9
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [2 2], 'poolType', 'mean' );
            %10,11
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(5), 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %12,13
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(6), 'filterSize', [5 5], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %14
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [6 6], 'poolType', 'mean' );
            %15
            cnnnet = appendNetworkLayer( cnnnet, 'softmax', displayOpts, 'initializeWeightMethod', 'Random', 'minFuncMaxIter', softMaxInitialIteration ,'priorInfoUse', priorInfoUse);
        case 'CIFAR10_DEEP02_PCA_Rand04'
            %filtCnt = [8 8 10 12 30 50];
            %filtCnt = [16 16 20 24 60 80];
            filtCnt = [4 4 4 4 10 10];%L13,L17,D09->D54
            %filtCnt = [12 12 12 12 60 100];%S22,S23
            %filtCnt = [4 4 5 6 15 25];%L14
            varargin_1 = {  'activationType', 'relu'...
                          , 'initializeWeightMethod', 'PCA'...
                          , 'applyOrthonormalization', true...
                          , 'wvnMode','epoch'...
                          , 'bnMode','batch'...
                          , 'filterStatsTrackMode', 'perBatchIteration'...
                          , 'stride', [1 1]};
            varargin_2 = varargin_1;
            varargin_2{4} = 'Random';%'initializeWeightMethod'
            %1,2
            cnnnet = appendNetworkLayer( [], 'convolution', displayOpts, 'countFilt', filtCnt(1), 'inputChannelSize', 3, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %3,4
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(2), 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %5,6
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(3), 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %7,8
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(4), 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);

            %9
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [2 2], 'poolType', 'mean' );
            
            %10,11
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(5), 'filterSize', [3 3], varargin_2{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);

            %12,13
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(6), 'filterSize', [5 5], varargin_2{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            
            %14
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [6 6], 'poolType', 'mean' );
            
            %15
            cnnnet = appendNetworkLayer( cnnnet, 'softmax', displayOpts, 'initializeWeightMethod', 'Random', 'minFuncMaxIter', softMaxInitialIteration ,'priorInfoUse', priorInfoUse);
        case 'CIFAR10_DEEP01'
            varargin_1 = {  'activationType', 'relu'...
                          , 'initializeWeightMethod', 'Autoencoder'...
                          , 'applyOrthonormalization', true...
                          , 'wvnMode','epoch'...
                          , 'bnMode','batch'...
                          , 'filterStatsTrackMode', 'perBatchIteration'...
                          , 'stride', [1 1]}; 
            varargin_2 = varargin_1;
            varargin_2{4} = 'LDA';%'initializeWeightMethod'
            %1,2
            cnnnet = appendNetworkLayer( [], 'convolution', displayOpts, 'countFilt', 8, 'inputChannelSize', 3, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %3,4
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 8, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %5,6
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 10, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %7,8
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 12, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %9
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [2 2], 'poolType', 'mean' );
            %10,11
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 30, 'filterSize', [3 3], varargin_2{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %12,13
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 80, 'filterSize', [5 5], varargin_2{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %14
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [6 6], 'poolType', 'mean' );
            %15
            cnnnet = appendNetworkLayer( cnnnet, 'softmax', displayOpts, 'initializeWeightMethod', 'Random', 'minFuncMaxIter', softMaxInitialIteration ,'priorInfoUse', priorInfoUse);
        case 'CIFAR10_DEEP01_Rand'
            varargin_1 = {  'activationType', 'relu'...
                          , 'initializeWeightMethod', 'Random'...
                          , 'applyOrthonormalization', false...
                          , 'wvnMode','epoch'...
                          , 'bnMode','batch'...
                          , 'filterStatsTrackMode', 'perBatchIteration'...
                          , 'stride', [1 1]};
            %filtCnt = [4 4 4 4 10 10];%H12->H57
            filtCnt = [10 10 12 15 40 50];%H14
            %1,2
            cnnnet = appendNetworkLayer( [], 'convolution', displayOpts, 'countFilt', filtCnt(1), 'inputChannelSize', 3, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %3,4
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(2), 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %5,6
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(3), 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %7,8
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(4), 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %9
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [2 2], 'poolType', 'mean' );
            %10,11
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(5), 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %12,13
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(6), 'filterSize', [5 5], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %14
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [6 6], 'poolType', 'mean' );
            %15
            cnnnet = appendNetworkLayer( cnnnet, 'softmax', displayOpts, 'initializeWeightMethod', 'Random', 'minFuncMaxIter', softMaxInitialIteration ,'priorInfoUse', priorInfoUse);
        case 'CIFAR10_DEEP02'
            varargin_1 = {  'activationType', 'relu'...
                          , 'initializeWeightMethod', 'Autoencoder'...
                          , 'applyOrthonormalization', true...
                          , 'wvnMode','epoch'...
                          , 'bnMode','batch'...
                          , 'filterStatsTrackMode', 'perBatchIteration'...
                          , 'stride', [1 1]};
            varargin_2 = varargin_1;
            varargin_2{4} = 'Random';%'initializeWeightMethod'
            %1,2
            cnnnet = appendNetworkLayer( [], 'convolution', displayOpts, 'countFilt', 8, 'inputChannelSize', 3, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %3,4
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 8, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %5,6
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 10, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %7,8
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 12, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);

            %9
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [2 2], 'poolType', 'mean' );
            
            %10,11
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 30, 'filterSize', [3 3], varargin_2{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);

            %12,13
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 50, 'filterSize', [5 5], varargin_2{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            
            %14
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [6 6], 'poolType', 'mean' );
            
            %15
            cnnnet = appendNetworkLayer( cnnnet, 'softmax', displayOpts, 'initializeWeightMethod', 'Random', 'minFuncMaxIter', softMaxInitialIteration ,'priorInfoUse', priorInfoUse);
        case 'CIFAR10_DEEP02_PCA_PerClass_Rand'
            varargin_1 = {  'activationType', 'relu'...
                          , 'initializeWeightMethod', 'PCA_PerClass'...
                          , 'applyOrthonormalization', true...
                          , 'wvnMode','epoch'...
                          , 'bnMode','batch'...
                          , 'filterStatsTrackMode', 'perBatchIteration'...
                          , 'stride', [1 1]};
            varargin_2 = varargin_1;
            varargin_2{4} = 'Random';%'initializeWeightMethod'
            %1,2
            cnnnet = appendNetworkLayer( [], 'convolution', displayOpts, 'countFilt', 8, 'inputChannelSize', 3, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %3,4
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 8, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %5,6
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 10, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %7,8
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 12, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %9
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [2 2], 'poolType', 'mean' );
            %10,11
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 30, 'filterSize', [3 3], varargin_2{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %12,13
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 50, 'filterSize', [5 5], varargin_2{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %14
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [6 6], 'poolType', 'mean' );
            %15
            cnnnet = appendNetworkLayer( cnnnet, 'softmax', displayOpts, 'initializeWeightMethod', 'Random', 'minFuncMaxIter', softMaxInitialIteration ,'priorInfoUse', priorInfoUse);
        case 'CIFAR10_DEEP02_PCA_Rand'
            varargin_1 = {  'activationType', 'relu'...
                          , 'initializeWeightMethod', 'PCA'...
                          , 'applyOrthonormalization', true...
                          , 'wvnMode','epoch'...
                          , 'bnMode','batch'...
                          , 'filterStatsTrackMode', 'perBatchIteration'...
                          , 'stride', [1 1]};
            varargin_2 = varargin_1;
            varargin_2{4} = 'Random';%'initializeWeightMethod'
            %1,2
            cnnnet = appendNetworkLayer( [], 'convolution', displayOpts, 'countFilt', 8, 'inputChannelSize', 3, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %3,4
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 8, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %5,6
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 10, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %7,8
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 12, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %9
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [2 2], 'poolType', 'mean' );
            %10,11
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 30, 'filterSize', [3 3], varargin_2{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %12,13
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 50, 'filterSize', [5 5], varargin_2{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %14
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [6 6], 'poolType', 'mean' );
            %15
            cnnnet = appendNetworkLayer( cnnnet, 'softmax', displayOpts, 'initializeWeightMethod', 'Random', 'minFuncMaxIter', softMaxInitialIteration ,'priorInfoUse', priorInfoUse);
        case 'CIFAR10_DEEP02_XOR_ELM_SBN'
            varargin_1 = {  'activationType', 'relu'...
                          , 'initializeWeightMethod', 'XOR_ELM'...
                          , 'applyOrthonormalization', false...
                          , 'wvnMode','none'...
                          , 'bnMode','batch'...
                          , 'filterStatsTrackMode', 'perBatchIteration'...
                          , 'stride', [1 1]};
            %1,2
            cnnnet = appendNetworkLayer( [], 'convolution', displayOpts, 'countFilt', 8, 'inputChannelSize', 3, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %3,4
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 8, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %5,6
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 10, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %7,8
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 12, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %9
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [2 2], 'poolType', 'mean' );
            %10,11
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 30, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %12,13
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 50, 'filterSize', [5 5], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %14
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [6 6], 'poolType', 'mean' );
            %15
            cnnnet = appendNetworkLayer( cnnnet, 'softmax', displayOpts, 'initializeWeightMethod', 'Random', 'minFuncMaxIter', softMaxInitialIteration ,'priorInfoUse', priorInfoUse);
        case 'CIFAR10_DEEP02_V2'
            varargin_1 = {  'activationType', 'relu'...
                          , 'initializeWeightMethod', 'Autoencoder'...
                          , 'applyOrthonormalization', true...
                          ..., 'wvnMode','initial' by default
                          ..., 'bnMode','initial' by default
                          , 'filterStatsTrackMode', 'perBatchIteration'...
                          , 'stride', [1 1]};
            varargin_2 = varargin_1;
            varargin_2{4} = 'Random';%'initializeWeightMethod'
            %1,2
            cnnnet = appendNetworkLayer( [], 'convolution', displayOpts, 'countFilt', 8, 'inputChannelSize', 3, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %3,4
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 8, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %5,6
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 10, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %7,8
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 12, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %9
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [2 2], 'poolType', 'mean' );
            %10,11
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 30, 'filterSize', [3 3], varargin_2{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %12,13
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 50, 'filterSize', [5 5], varargin_2{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %14
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [6 6], 'poolType', 'mean' );  
            %15
            cnnnet = appendNetworkLayer( cnnnet, 'softmax', displayOpts, 'initializeWeightMethod', 'Random', 'minFuncMaxIter', softMaxInitialIteration ,'priorInfoUse', priorInfoUse);
        case 'CIFAR10_DEEP02_Rand'
            varargin_1 = {  'activationType', 'relu'...
                          , 'initializeWeightMethod', 'Random'...
                          , 'applyOrthonormalization', false...
                          , 'wvnMode','epoch'...
                          , 'bnMode','batch'...
                          , 'filterStatsTrackMode', 'perBatchIteration'...
                          , 'stride', [1 1]};
            %1,2
            cnnnet = appendNetworkLayer( [], 'convolution', displayOpts, 'countFilt', 8, 'inputChannelSize', 3, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %3,4
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 8, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %5,6
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 10, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %7,8
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 12, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %9
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [2 2], 'poolType', 'mean' );
            %10,11
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 30, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %12,13
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 50, 'filterSize', [5 5], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %14
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [6 6], 'poolType', 'mean' );
            %15
            cnnnet = appendNetworkLayer( cnnnet, 'softmax', displayOpts, 'initializeWeightMethod', 'Random', 'minFuncMaxIter', softMaxInitialIteration ,'priorInfoUse', priorInfoUse);
        case 'CIFAR10_DEEP03'
            varargin_1 = {  'activationType', 'relu'...
                          , 'initializeWeightMethod', 'AE_PB'...
                          , 'applyOrthonormalization', true...
                          , 'wvnMode','epoch'...
                          , 'bnMode','batch'...
                          , 'filterStatsTrackMode', 'perBatchIteration'...
                          , 'stride', [1 1]};
            varargin_2 = varargin_1;
            varargin_2{4} = 'Random';%'initializeWeightMethod'
            %1,2
            cnnnet = appendNetworkLayer( [], 'convolution', displayOpts, 'countFilt', 12, 'inputChannelSize', 3, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %3,4
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 12, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %5,6
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 15, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %7,8
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 20, 'filterSize', [3 3], varargin_1{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %9
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [2 2], 'poolType', 'mean' );
            %10,11
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 40, 'filterSize', [3 3], varargin_2{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %12,13
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 60, 'filterSize', [5 5], varargin_2{:});
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %14
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [6 6], 'poolType', 'mean' );
            %15
            cnnnet = appendNetworkLayer( cnnnet, 'softmax', displayOpts, 'initializeWeightMethod', 'Random', 'minFuncMaxIter', softMaxInitialIteration ,'priorInfoUse', priorInfoUse);
        case 'CIFAR10_DEEP05_CONV5'
            filtCnt = [5 5 5 5 5      8 8 8];%S19
            filterSize_1 = [5 5];
            filterSize_2 = [4 4];
            varargin_1 = {  'activationType', 'relu'...
                          , 'initializeWeightMethod', 'PCA'...
                          , 'applyOrthonormalization', true...
                          , 'wvnMode','epoch'...
                          , 'bnMode','batch'...
                          , 'filterStatsTrackMode', 'perBatchIteration'...
                          , 'stride', [1 1]};
            varargin_2 = varargin_1;
            varargin_2{4} = 'Random';%'initializeWeightMethod'
            %1,2 - 32->28
            cnnnet = appendNetworkLayer( [], 'convolution', displayOpts, 'countFilt', filtCnt(1), 'inputChannelSize', 3, 'filterSize', filterSize_1, varargin_1{:});cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %3,4 - 28->24
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(2), 'filterSize', filterSize_1, varargin_1{:});cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %5,6 - 24->20
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(3), 'filterSize', filterSize_1, varargin_1{:});cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %7,8 - 20->16
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(4), 'filterSize', filterSize_1, varargin_1{:});cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %9,10 - 16->12
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(5), 'filterSize', filterSize_1, varargin_1{:});cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);

            %varargin_2
            %11,12 - 12->8
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(6), 'filterSize', filterSize_1, varargin_2{:});cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %12,13 - 8->4
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(7), 'filterSize', filterSize_1, varargin_2{:});cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            %14,15 - 4->1 (filterSize_2)
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(8), 'filterSize', filterSize_2, varargin_2{:});cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            
            %15
            cnnnet = appendNetworkLayer( cnnnet, 'softmax', displayOpts, 'initializeWeightMethod', 'Random', 'minFuncMaxIter', softMaxInitialIteration ,'priorInfoUse', priorInfoUse);
        case 'ConvELM2RandXavier_Pool_Surrey'
            cnnnet = appendNetworkLayer( [], 'convolution', displayOpts, 'countFilt', 12, 'inputChannelSize', 3, 'filterSize', [8 8], 'stride', [3 3], 'activationType', 'relu', 'initializeWeightMethod', 'ELM');%'Experimental, Xavier, Glorot_Bengio, He_Rang_Zhen
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 18, 'stride', [1 1], 'filterSize', [5 5], 'activationType', 'relu', 'initializeWeightMethod', 'Random','randInitMode','Xavier');
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [3 3], 'poolType', 'mean' );
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 80, 'filterSize', [7 7], 'activationType', 'relu', 'initializeWeightMethod', 'Random','randInitMode','Xavier');
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            cnnnet = appendNetworkLayer( cnnnet, 'softmax', displayOpts, 'initializeWeightMethod', 'Random', 'minFuncMaxIter', softMaxInitialIteration ,'priorInfoUse', priorInfoUse);
        case 'Conv2_Pool_Conv_AutoencoderSurrey'
            cnnnet = appendNetworkLayer( [], 'convolution', displayOpts, 'countFilt', 12, 'inputChannelSize', 3, 'filterSize', [8 8], 'stride', [3 3], 'activationType', 'relu', 'initializeWeightMethod', 'Autoencoder');%'Experimental, Xavier, Glorot_Bengio, He_Rang_Zhen
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 18, 'stride', [1 1], 'filterSize', [5 5], 'activationType', 'relu', 'initializeWeightMethod', 'Autoencoder');
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [3 3], 'poolType', 'mean' );
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 80, 'filterSize', [7 7], 'activationType', 'relu', 'initializeWeightMethod', 'Autoencoder');
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            cnnnet = appendNetworkLayer( cnnnet, 'softmax', displayOpts, 'initializeWeightMethod', 'Random', 'minFuncMaxIter', softMaxInitialIteration ,'priorInfoUse', priorInfoUse);
        case 'Surrey_AutoencoderOrthonormal'
            cnnnet = appendNetworkLayer( [], 'convolution', displayOpts, 'countFilt', 12, 'inputChannelSize', 3, 'filterSize', [8 8], 'stride', [3 3], 'activationType', 'relu', 'initializeWeightMethod', 'Random','applyOrthonormalization',true,'wvnMode','batch','bnMode','batch','filterStatsTrackMode','perBatchIteration');
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 18, 'stride', [1 1], 'filterSize', [5 5], 'activationType', 'relu', 'initializeWeightMethod', 'Random','applyOrthonormalization',true,'wvnMode','batch','bnMode','batch','filterStatsTrackMode','perBatchIteration');
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [3 3], 'poolType', 'mean' );
            cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', 80, 'filterSize', [7 7], 'activationType', 'relu', 'initializeWeightMethod', 'Random','applyOrthonormalization',true,'wvnMode','batch','bnMode','batch','filterStatsTrackMode','perBatchIteration');
            cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
            cnnnet = appendNetworkLayer( cnnnet, 'softmax', displayOpts, 'initializeWeightMethod', 'Random', 'minFuncMaxIter', softMaxInitialIteration ,'priorInfoUse', priorInfoUse);
        otherwise
    end
end