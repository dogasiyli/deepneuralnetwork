%% Convolution Neural Network Exercise

%  Instructions
%  ------------
% 
%  Try different networks and check if the gradient calculations are
%  correct

%%======================================================================
%% STEP 0: Initialize Parameters and Load Data
%  Here we initialize some parameters used for the exercise.
clearvars -except exampleID;%clear all;%
clc;%
    exampleID = 4;
    global fastDiskTempPath;
    useAccuracyAsCost = false;
    saveLevel = 'none';
    displayLevel = 'none';
    rng(10);
    trainPercent = 50;
    testPercent = 100;
    displayDataSize = true;
    [trainData, trainLabels, testData, testLabels, sampleCounts] = script_loadMnistDatabase_Percent(trainPercent, testPercent, displayDataSize);
    inputDimensionSize = [8 8];
    countFilt = 6;
    switch exampleID
        case 1%strideParam = 1,inputChannelSize = 1;filterDim = 9;poolDim = 10;
            architectureIdentifier = 'TestBackProp_Conv_Pool_Soft_Ex01';
        case 2%strideParam = 1,**inputChannelSize = 4**;filterDim = 9;poolDim = 10
            architectureIdentifier = 'TestBackProp_Conv_Pool_Soft_Ex02';
        case 3%**strideParam = 2**,inputChannelSize = 4;filterDim = 9;poolDim = 10;
            architectureIdentifier = 'TestBackProp_Conv_Pool_Soft_Ex03';
        case 4%strideParam = 2,**inputChannelSize = 1**;filterDim = 9;poolDim = 5;
            architectureIdentifier = 'TestBackProp_Conv_Pool_Soft_Ex04';
        %random data after this example
        case 5%
            inputChannelSize = 2;
            filterDim = [5 5];
            strideParam = [1 1];
            
            poolDim = [2 2];
            
            categoryCount  = 3;
            
            countSamples = 3;
            paramCount = prod(filterDim)*countFilt*inputChannelSize + countFilt + 24*categoryCount + categoryCount;
            architectureIdentifier = 'TestBackProp_Conv_Pool_Soft_Ex05';
        case 20%
            categoryCount  = 10;
            countSamples = 10;
            paramCount = 80;
            architectureIdentifier = 'TestBackProp_Full_Soft_Ex20';
    end
    
% Initialize Parameters
    saveMainFolder = [fastDiskTempPath 'deleteThis-exampleID(' num2str(exampleID) ')\'];
    saveOpts = setSaveOpts(saveLevel, saveMainFolder);
    displayOpts = setDisplayOpts(displayLevel);
    cnnnet = architectureSet( architectureIdentifier, 'displayOpts', displayOpts, 'useAccuracyAsCost', useAccuracyAsCost, 'softMaxInitialIteration',0);

    if exampleID<20
        inputChannelSize = cnnnet{1,1}.inputChannelSize;
        if inputChannelSize>1 && exampleID<=4
            trainData = repmat(trainData,inputChannelSize,1);
            testData = repmat(testData,inputChannelSize,1);
        end
    end
    if exampleID<=4
        db_images = trainData(:,1:5);
        db_labels = trainLabels(1:5);
        filterDim = cnnnet{1,1}.filterSize;
    end
    
    rng(10);
    sameDataSetFileName = ['D:\theControlSamples_' num2str(exampleID,'%02d') '.mat'];
    if exist(sameDataSetFileName,'file')
        load(sameDataSetFileName,'db_images','db_labels','db_theta');
    elseif exampleID>4
        if exampleID<20
            db_images = 0.5-rand(prod(inputDimensionSize)*inputChannelSize,countSamples);
            db_labels = randi(categoryCount,[1,3]);
            db_theta = 0.5-rand(paramCount,1);
            save(sameDataSetFileName,'db_images','db_labels','db_theta');
        else
            
        end
    end    
    
    %initial run the cnnet to set weight params
    OPS = struct('cnnnet',{cnnnet},'data',db_images,'labels',db_labels,'displayOpts',displayOpts,'saveOpts',saveOpts,'memoryMethod','all_in_memory', 'useAccuracyAsCost',useAccuracyAsCost,'calculateGradient',false);
    [ cnnnet, softMaxedData, accuracyTrainDeep, confTrainDeep, activationID] = ...
                                                                forwardPropogateDeepNet( OPS.data, OPS.labels, cnnnet ...
                                                                                        ,'calculateBackPropMap', true ...1
                                                                                        ,'displayOpts', OPS.displayOpts ...1
                                                                                        ,'saveOpts', OPS.saveOpts);    

%%======================================================================
%% STEP 1: Implement convNet Objective
%  Implement the function cnnCost.m.

%%======================================================================
%% STEP 2: Gradient Check
%  Use the file computeNumericalGradient.m to check the gradient
%  calculation for your cnnCost.m function.  You may need to add the
%  appropriate path or copy the file to this directory.
    numOfImagesToDebug=3;  % set this to true to check gradient
    if numOfImagesToDebug>0
        % To speed up gradient checking, we will use a reduced network and a debugging data set
        %methods
        %case 1%as it was given-strideParam = 1,inputChannelSize = 1,filterDim = 9,poolDim = 2;  
        %case 2%strideParam = 1,inputChannelSize = 3,poolDim = 2,filterDim = 9;    % Filter size for conv layer
        %case 3%strideParam = 2,inputChannelSize = 3,poolDim = 3,filterDim = 9;    % Filter size for conv layer
        if exampleID<20
            db_strideParam = cnnnet{1,1}.stride;
            db_filterDim = cnnnet{1,1}.filterSize;% Filter size for conv layer
            db_numFilters = cnnnet{1,1}.countFilt;% Number of filters for conv layer
            db_poolDim = cnnnet{3,1}.poolSize;% Pooling dimension, (should divide imageDim-filterDim+1)
        end
        if exampleID<=4
            db_images = trainData(:,1:numOfImagesToDebug);
            db_labels = trainLabels(1:numOfImagesToDebug);
        else
            %load the theta and images and labels to cross check with the other example
        end
        db_theta_now = stack_cnnnet_into_theta( cnnnet );
        if exist('db_theta','var')
            assert(length(db_theta_now)==length(db_theta),'problem');
            cnnnet = update_cnnnet_by_theta(db_theta, cnnnet, displayOpts);
            db_theta_now = stack_cnnnet_into_theta( cnnnet );
        end
        if exist(sameDataSetFileName,'file')
            load(sameDataSetFileName,'db_images','db_labels','db_theta');
            cnnnet = update_cnnnet_by_theta(db_theta, cnnnet, displayOpts);
        end
        %db_theta_now = 0.5-rand(size(db_theta_now));
        %cnnnet = update_cnnnet_by_theta(db_theta, cnnnet, displayOpts);
        
        OPS = struct('cnnnet',{cnnnet},'data',db_images,'labels',db_labels,'displayOpts',displayOpts,'saveOpts',saveOpts,'memoryMethod','all_in_memory', 'useAccuracyAsCost',useAccuracyAsCost,'calculateGradient',true);
        [cost_DG, grad_DG, optionalOutputs] = stackedAECostMulCNNNET_BP(db_theta_now, OPS);
        sameGradFileName_AndNG = ['D:\theCostAndGrad_AndNG_' num2str(exampleID,'%02d') '.mat'];
        sameGradFileName_DG = ['D:\theCostAndGrad_DG_' num2str(exampleID,'%02d') '.mat'];
        save(sameGradFileName_DG,'cost_DG','grad_DG');
        if exist(sameGradFileName_AndNG,'file')
            load(sameGradFileName_AndNG,'cost_AndNG','grad_AndNG');
        end
        
        % Check gradients
        [numGrad, problematicMatches] = computeNumGrad(@(x) stackedAECostMulCNNNET_BP(x, OPS), db_theta_now, grad_DG);

        diff = norm(numGrad-grad_DG)/norm(numGrad+grad_DG);
        % Should be small. In our implementation, these values are usually 
        % less than 1e-9.
        disp(['Diff equals to : ' num2str(diff)]); 
        % Use this to visually compare the gradients side by side
        if ~isempty(problematicMatches)
            disp(mat2str(problematicMatches));
        elseif diff > (1e-9)*size(grad_DG,1)
            allDif = abs(numGrad-grad_DG);
            [allDifSorted, idx] = sort(allDif,'descend');
            dispDifMat = [idx,allDifSorted,grad_DG(idx),numGrad(idx)];
            dispDifMat = dispDifMat(abs(allDifSorted)>0);
            dispDifMat = sortrows(dispDifMat,1);
            disp(dispDifMat);
        end
        assert(diff < (1e-9)*size(grad_DG,1),'Difference too large. Check your gradient computation again');    
    end

%%======================================================================
%% STEP 3: Learn Parameters
%  Implement minFuncSGD.m, then train the model.

minFuncOptions.epochs = 3;
minFuncOptions.minibatch = 256;
minFuncOptions.alpha = 1e-1;
minFuncOptions.momentum = .95;

OPS = struct('cnnnet',{cnnnet},'data',trainData,'labels',trainLabels,'displayOpts',displayOpts,'saveOpts',saveOpts,'memoryMethod','all_in_memory', 'useAccuracyAsCost',useAccuracyAsCost,'calculateGradient',true);
theta = stack_cnnnet_into_theta( cnnnet );
opt_theta = minFuncSGD(@(x,y) stackedAECostMulCNNNET_BP(x,y),theta,OPS,minFuncOptions);
cnnnet = update_cnnnet_by_theta(opt_theta, cnnnet, displayOpts);

%%======================================================================
%% STEP 4: Test
%  Test the performance of the trained model using the MNIST test set. Your
%  accuracy should be above 97% after 3 epochs of training

% [~,cost,preds]=cnnCost(opttheta,testImages,testLabels,numClasses,...
%                 filterDim, strideParam,numFilters,poolDim,true);
% 
% acc = (sum(preds==testLabels)/length(preds))*100;
% 
% % Accuracy should be around 97.4% after 3 epochs
% fprintf('Accuracy is %f percent\n',acc);
