function [ x, f, exitflag, optionalOutputs] = fevalCaller( funObj, x0, minFuncParamsStruct, objFuncParamsStruct)
%fevalCaller mimics minFunc_Doga.m
%   calls fevalSample

%call 
%[x, f, exitflag, optionalOutputs] = fevalCaller(@fevalSample, 987, [], struct('val1' , 1, 'val2', 2, 'useAccuracyAsCost', true, 'calculateGradient', false)); 
%optionalOutputs.accuracy
%optionalOutputs.hidLayeractivations
%optionalOutputs.hessianParam

%optionalMethodParamsStruct mimics the optional parameters for minFunc_Doga

    optionalOutputs.accuracy = [];
    optionalOutputs.hidLayeractivations = [];
    objFuncParamsStruct.optionalOutputs = optionalOutputs;
    
    %objFuncParamsStruct.useAccuracyAsCost = true;
    %objFuncParamsStruct.calculateGradient = false;
    
    [cost, grad, optionalOutputs] = feval(funObj, x0, objFuncParamsStruct);

    x = x0;
    f = cost;
    exitflag = 1;
    
end

