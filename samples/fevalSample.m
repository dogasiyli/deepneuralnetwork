function [ cost, grad, optionalOutputs] = fevalSample( coreParam, objFuncParamsStruct)
%fevalSample This function is for testing feval function that is used in
%minfunc

    %this function is called from feval function
    disp(['core param = ' mat2str(coreParam)]);
    calculatedAccuracy = 0.98;
    
    if isfield(objFuncParamsStruct,'useAccuracyAsCost') && objFuncParamsStruct.useAccuracyAsCost
        cost = 1-calculatedAccuracy;
    else
        cost = 5;
    end
    
    if isfield(objFuncParamsStruct,'calculateGradient') && objFuncParamsStruct.calculateGradient
        grad = dummGradFunc();
    else
        grad = [];
    end
    
    optionalOutputs = assignOptionalObjectiveFunctionOutputs(objFuncParamsStruct, struct('accuracy',calculatedAccuracy));
    pauseFileFullPath = 'D:\GitHub\deepneuralnetwork\samples\pauseFileNameSample.txt';
    checkToPause(mfilename);%[TF, pauseFileFullPath] = checkToPause(mfilename, pauseFileFullPath);
end

function [grad] = dummGradFunc()
    grad = 1:3:15;
end
