function laser_training_doga_ELMvsRNN_V2()
    % load laser sequence, create input and target symbols
    S = loadseq('laser.trs');
        data.trainSamples = S(:,1:8000-1);
        data.trainOutputs = S(:,2:8000);
        data.testSamples = S(:,8000-1:end-1);
        data.testOutputs = S(:,8000:end);
        clear S;
    
    [~,trainLabels] = max(data.trainOutputs);
    [~,testLabels] = max(data.testOutputs);
    
    sigm = @(z_) 1.0 ./ (1.0 + exp(-z_));
    HUC =10;%number of hidden units

    %now X has only data at time t=0
    Xtest_t0 = data.testSamples;
    Xtrain_t0 = data.trainSamples;
    
%% step-1    
    %lets see how appending data one after another helps
    maxWinSize = 10;
    accApp_Tr = zeros(maxWinSize, 1);
    accApp_Te = zeros(maxWinSize, 1);
    for appendSize = 1:maxWinSize
        disp(['Appending ' num2str(appendSize) ' winsizes one after another'])
        [Xapp, Yapp] = createAppendedData( Xtrain_t0, trainLabels ,appendSize );
        [accApp_Tr(appendSize), ~, Wr_app, Welm_app] = ELM_training(Xapp, Yapp, 'hidSize', HUC, 'displayInfo', false);
        [Xapp, Yapp] = createAppendedData( Xtest_t0, testLabels ,appendSize );       
        [~, accApp_Te(appendSize)] = analyzeResults(Xapp, Yapp, Wr_app, Welm_app, false); 
        disp('**************');
%         pause;
    end
    disptable([accApp_Tr, accApp_Te],'accTr|accTe');

%% step-2
    %now try to learn rnn with elm
    Xtr0 = data.trainSamples; Ytr0 = trainLabels;
    %1.Input data ile encoder kullanarak weightleri ogren
    %2.Ogrendigin W ile input data aktivasyonunu hesapla
    %3.i=1,X_t(i) anindaki data ile act(X_t(i-1)) birlestir -> combData
    %4.Bu data ile autoencoder kullanarak  layer weightlerini ogren
    %5.t=1
    
    Wr_at = cell(maxWinSize+1,1);
    Welm_at = cell(maxWinSize+1,1);
    Wr_ct = cell(maxWinSize+1,1);
    Welm_ct = cell(maxWinSize+1,1);
    Welm_ct2 = cell(maxWinSize+1,1);
    
    accELM = zeros(maxWinSize+1,4);%cols=actTr,combTr,actTe,comboTe
    [accELM(1,1), ~, Wr_at{1,1}, Welm_at{1,1}] = ELM_training(Xtr0, Ytr0, 'hidSize', HUC, 'displayInfo', true);
    prevAct_Tr = sigm(Welm_at{1,1}'*Xtr0(:,1:end-1));
    Xtr0 = Xtr0(:,2:end);Ytr0 = Ytr0(2:end);
    for w=1:maxWinSize
        %3.i=1,X_t(i) anindaki data ile act(X_t(i-1)) birlestir
        combData_Tr = [Xtr0;prevAct_Tr];%(HUC+d by N-w)
        disp(['Combined data ELM-w(' num2str(w) ')']);
        [accELM(w+1,2), ~, Wr_ct{w+1,1}, Welm_ct{w+1,1}] = ELM_training(combData_Tr, Ytr0, 'hidSize', HUC, 'displayInfo', true);%(HUC by HUC+d)
        W = ELM_autoencoder(prevAct_Tr(:,1:end-1),'Y',prevAct_Tr(:,2:end)', 'hidSize', HUC, 'useBias', false, 'displayInfo', false);%(HUC by HUC+d)
        Welm_ct2{w+1,1} = [Welm_ct{w+1,1}',W];
        curAct_Tr = sigm(Welm_ct2{w+1,1}*combData_Tr(:,1:end-1));%(N-w by HUC)
        %apply relu
        curAct_Tr(curAct_Tr<0.01)=0;
        
        %at this step I can use the activations or combData as input data and train ELM
        %train using activations
        disp(['Activation of [X(' num2str(w-1) ');act(' num2str(w-1) ')] data ELM-w(' num2str(w) ')']);
        [accELM(w+1,1), ~, Wr_at{w+1,1}, Welm_at{w+1,1}] = ELM_training(curAct_Tr, Ytr0(1:end-1), 'hidSize', HUC, 'displayInfo', true);
        
        %change data for next iteration
        Xtr0 = Xtr0(:,2:end);Ytr0 = Ytr0(2:end);
        prevAct_Tr = curAct_Tr;
    end
    
    %check test data results    
    Xte0 = data.testSamples; Yte0 = testLabels;
    prevAct_Te = sigm(W{1,1}*Xte0(:,1:end-1));
    Xte0 = Xte0(:,2:end);Yte0 = Yte0(2:end);
    for w=1:maxWinSize
        %3.i=1,X_t(i) anindaki data ile act(X_t(i-1)) birlestir
        combData_Te = [Xte0;prevAct_Te];%(N-w by HUC+d)
        prevAct_Te = sigm(W{w+1,1}*combData_Te(:,1:end-1));%(N-w by HUC)
        %at this step I can use the activations or combData as input data and train ELM
        %test using activations
        [~, accELM(w,3)] = analyzeResults(prevAct_Te, Yte0(1:end-1), Wr_at{w,1}, Welm_at{w,1}, true);
        %train using combData
        [~, accELM(w,4)] = analyzeResults(combData_Te, Yte0, Wr_ct{w,1}, Welm_ct{w,1}, true);
        
        %change data for next iteration
        Xte0 = Xte0(:,2:end);Yte0 = Yte0(2:end);
    end
end

function plotChart(errorAccuracyOut, figID, rnnModelName)
    netError_Train = errorAccuracyOut.netError_Train;
    netError_Test = errorAccuracyOut.netError_Test;
    labelAccuracy_Train = errorAccuracyOut.labelAccuracy_Train;
    labelAccuracy_Test = errorAccuracyOut.labelAccuracy_Test; 
    
    figure(figID); clf;
        subplot(2,1,1);
        plot(netError_Train,'r*-');hold on;
        plot(netError_Test,'go-');
        title(['laser data//' strrep(rnnModelName, '_', '-') '//net Error']);
        xlabel('Epochs'); ylabel('Error');
        legend('Train','Test','Location','BestOutside');

        subplot(2,1,2);
        plot(labelAccuracy_Train,'r*-');hold on;
        plot(labelAccuracy_Test,'go-');
        title(['laser data//' strrep(rnnModelName, '_', '-') '//label Accuracy']);
        xlabel('Epochs'); ylabel('Accuracy');
        legend('Train','Test','Location','BestOutside');
end