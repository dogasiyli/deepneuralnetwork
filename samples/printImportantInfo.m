function printImportantInfo( outputMatrixStructCNN, cnnnet )
    if ~isempty(outputMatrixStructCNN)
        disp(['The main save folder name : ' outputMatrixStructCNN.generalInfo.dropboxSubFolderName]);
        disp(['Architecture name : ' outputMatrixStructCNN.generalInfo.arcName]);
        disp(['Start Time : ' outputMatrixStructCNN.generalInfo.dateStartStr]);
        disp(['Commit code : ' outputMatrixStructCNN.generalInfo.commitCode]);
        disp(['exampleID : ' num2str(outputMatrixStructCNN.generalInfo.exampleID)]);
        disp(['Completed epoch ' num2str(outputMatrixStructCNN.iterationInfo.epochID_main) ' of  ' num2str(outputMatrixStructCNN.iterationInfo.epochID_sub) ]);
        disp(outputMatrixStructCNN.generalInfo.dataInfo_Train);
        disp(outputMatrixStructCNN.generalInfo.dataInfo_Valid);
        disp(outputMatrixStructCNN.generalInfo.dataInfo_Test);
    end
    
    %accuracy per batch analysis
    accPerBatMat = outputMatrixStructCNN.accPerBatch.Mat;
    trCols = outputMatrixStructCNN.accPerBatch.batchColumnsTr(end-1:end);
    vaCols = outputMatrixStructCNN.accPerBatch.batchColumnsVa(end-1:end);
    teCols = outputMatrixStructCNN.accPerBatch.batchColumnsTe(end-1:end);
    apbCols = accPerBatMat(3:end,[trCols vaCols teCols]);
    rowCnt = size(apbCols,1);

    %1. find the biggest validation accuracy
    disp('******');
    [mv,mi] = max(reshape(apbCols(:,3:4),[],1)); %#ok<ASGLU>
    [valRowVa, valColVa] = ind2sub([rowCnt 2],mi);
    max_TrVaTe = apbCols(valRowVa,valColVa:2:(valColVa+4));
    
    if valColVa==1
        bestMethodCharVa = 'm';
    elseif valColVa==2
        bestMethodCharVa = 'e';
    end
    disp(['Best VALIDATION result achieved in ' num2str(valRowVa) '(' bestMethodCharVa ')  epoch.'])
    
    disp([num2str(rowCnt) '/' num2str(valRowVa) bestMethodCharVa '/' ]);
    disp(mat2str(max_TrVaTe,4))
    
    [mv_te,mi_te] = max(reshape(apbCols(:,5:6),[],1));
    [valRowTe, valColTe] = ind2sub([rowCnt 2],mi_te);
    if (valRowTe ~= valRowVa) || (valColTe ~= valColVa)
        if valColTe==1
            bestMethodCharTe = 'm';
        elseif valColTe==2
            bestMethodCharTe = 'e';
        end
        disp(['Best TEST result achieved in ' num2str(valRowTe) '(' bestMethodCharTe ') = ' num2str(mv_te,'%4.2f')])
        maxTe_TrVaTe = apbCols(valRowTe,valColTe:2:(valColTe+4));
        disp(mat2str(maxTe_TrVaTe,4))
    end
    
    disp('******');

    %analyze the "filterStatsTrack" for wvnmode, bnmode, orthInit
    if ~isempty(outputMatrixStructCNN) && isfield(outputMatrixStructCNN,'filterStatsTrack')
        batchCount = outputMatrixStructCNN.accPerBatch.batchCntTr;        
        for l=1:99
            m=[];
            matName = ['mattL' num2str(l,'%02d')];
            if isfield(outputMatrixStructCNN.filterStatsTrack,matName)
                eval(['m = outputMatrixStructCNN.filterStatsTrack.' matName ';']);
                %grab only filterID=1
                m = m(m(:,5)==1,:);
                %grab only train batches
                m = m(m(:,2)==1,:);
                
                epochCnt = max(m(:,1));
                if min(m(:,1))==0
                    epochCnt = epochCnt + 1;
                end
                %now m has 9 columns
                %       C1         C2         C3         C4           C5
                %   {'epochID' 'batchType' 'batchID' 'insertMode' 'filterID'  'min'   'mean'    'max'  'variance'}
                %ex:[    1          1          2          4           1      -28.03   1.72-13   32.24   13.377217]
                %insertModes - 1. normal - 2. afterOrth - 3. after wvn - 4. after bn
                %wvn, bn can be 'none','initial','epoch','batch'
                %orth can be yes or no
                %first check if there is any 2,3,4
                uniqueInsertModes = unique(m(:,4));
                modeCnt = [sum(m(:,4)==1) sum(m(:,4)==2) sum(m(:,4)==3) sum(m(:,4)==4)];
                
                if modeCnt(1)==batchCount && sum(modeCnt(2:4))==0
                    continue
                end
               
                %orth
                if modeCnt(2)==0
                    orth = 'NO';
                else
                    orth = 'YES';
                end
                %wvn
                if modeCnt(3)==0
                    wvn = 'none';
                elseif modeCnt(3)==1
                    wvn = 'initial';
                elseif modeCnt(3)==epochCnt
                    wvn = 'epoch';
                else
                    wvn = 'batch';
                end
                %bn
                if modeCnt(4)==0
                    bn = 'none';
                elseif modeCnt(4)==1
                    bn = 'initial';
                elseif modeCnt(4)==epochCnt
                    bn = 'epoch';
                else
                    bn = 'batch';
                end
                stateStr = ['L(' num2str(l,'%02d') ')-orth(' orth ')-wvn(' wvn ')-bn(' bn ')-uniqModes ' mat2str(uniqueInsertModes) '-modeCnt' mat2str(modeCnt)];
                disp(stateStr);
            end
        end
    end
    
    if exist('cnnnet','var') && ~isempty(cnnnet)
        layerCnt = size(cnnnet,1);
        for l=1:layerCnt
            strToDisp = '';
            if isfield(cnnnet{l,1},'initializeWeightMethod')
                strToDisp = [strToDisp 'init:' num2str(cnnnet{l, 1}.initializeWeightMethod) ' / ']; %#ok<*AGROW>
            end
            if isfield(cnnnet{l,1},'weightVarianceNormalizationMode')
                strToDisp = [strToDisp 'wvn:' cnnnet{l, 1}.weightVarianceNormalizationMode ' / '];
            end
            if isfield(cnnnet{l,1},'biasNormalizationMode')
                strToDisp = [strToDisp 'bn:' cnnnet{l, 1}.biasNormalizationMode  ' - '];
            end
            if isfield(cnnnet{l,1},'countFilt')
                strToDisp = [strToDisp 'cF:' num2str(cnnnet{l, 1}.countFilt)  ' / '];
            end
            if isfield(cnnnet{l,1},'filterSize')
                strToDisp = [strToDisp 'fs:' mat2str(cnnnet{l, 1}.filterSize)  ' / '];
            end
            if ~strcmp(strToDisp,'')
                strToDisp = ['LN:' cnnnet{l, 1}.name '/' strToDisp];
                disp(strToDisp);
            end
        end
    end
end

