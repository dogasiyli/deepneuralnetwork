function [opt_cnnnet, outParams, batchInfo, saveMainFolder] = ...
    runExample_Func(dataSetName, dataLoadFunction, dataLoadParams,...
                    curProjCode, commitCode, architectureIdentifier, ...
                    varargin)
%%  STEP 0  : Set some initialization values
    %dataSetSubIdentifier --> 'focus','resized[80 80]'
    %dataSetName --> ['SURREY-' dataSetSubIdentifier];
    %architectureIdentifier = 'Conv_Pool_ELMInit_Surray';
    %   STEP 0.a: Optional params
        [displayLevel, saveLevel, categoriesToLoad, batchParamsMode, dataShuffleMode, initialCNNNETFile, exampleID, softMaxInitialIteration, softMaxOptimizeMaxIteration,~,priorInfoUse] = getOptionalParams(varargin{:});
    %   STEP 0.a: Global params
        global minFuncStepInfo;minFuncStepInfo = [];
        global fastDiskTempPath;
    %   STEP 0.a: Local params
        rng(10);    
        %saveMainFolder = [fastDiskTempPath 'arc(' architectureIdentifier ')-ds(' dataSetName ')-softInit(' num2str(softMaxInitialIteration) ')-softAll' mat2str(softMaxOptimizeMaxIteration) '-batchParamsMode(' num2str(batchParamsMode) ')\'];
        if ~isempty(categoriesToLoad)
            categoryStr = ['-C(' strrep(strrep(strrep(mat2str(categoriesToLoad),' ','_'),'[',''),']','') ')'];
        else
            categoryStr = '';
        end
        saveMainFolder = [fastDiskTempPath curProjCode '_'  'cC(' commitCode ')' categoryStr '-exID(' num2str(exampleID) ')\'];
        saveOpts = setSaveOpts(saveLevel, saveMainFolder);
        displayOpts = setDisplayOpts(displayLevel);
    %%  STEP 1  : Load data 
        [dS, batchInfo] = feval(dataLoadFunction, dataLoadParams{:});
        %[dS, batchInfo] = createInitialDataStruct_surray(dataLoadParams);
    %%  STEP 2  : Parameters for CNN, autoencoder and other architecture related stuff
        cnnnet = architectureSet(architectureIdentifier, 'displayOpts', displayOpts, 'softMaxInitialIteration',softMaxInitialIteration,'priorInfoUse',priorInfoUse);
    %%  STEP 3  : Initialize Global Output Struct
        globalOutputStruct( 'initialize',[]...
                           ,'curProjCode', curProjCode...
                           ,'softMaxInitialIteration',softMaxInitialIteration...
                           ,'softMaxOptimizeMaxIteration',softMaxOptimizeMaxIteration...
                           ,'categoriesToLoad',categoriesToLoad...
                           ,'batchParamsMode',batchParamsMode...
                           ,'architectureIdentifier',architectureIdentifier...
                           ,'dataSetName',dataSetName...
                           ,'commitCode',commitCode...
                           ,'exampleID',exampleID...
                           ,'batchInfo',batchInfo...
                           ,'cnnnet',cnnnet);
    %%  STEP 3  : Initial training
    disp('*****************************************INITIAL*TRAINING***************************************')
        [cnnnetInitial, initFileMode] = loadCnnetFromFile(initialCNNNETFile);
        if isempty(cnnnetInitial)
            cnnnetInitial = cnnnet;
        end
        vararginParams = {'displayOpts', displayOpts,'saveOpts', saveOpts,'softMaxOptimizeMaxIteration',softMaxOptimizeMaxIteration};
        [cnnnetInitial, cnnnetResults_InitialTrain, ~, cnnnetResults_InitialValid, cnnnetResults_InitialTest, pred_labels_train_init] = deepNet_InitialTrain( dS, cnnnetInitial, vararginParams{:});
        clear vararginParams;
    %%  STEP 5  : Backpropogation
    disp('*************************************BACKPROPAGATION*TRAINING***********************************')
        paramsMinFunc = setParams_minFunc();
        dS = dS.updateDataStruct( dS, {'type', 'train', 'data_act', 'data', 'batchID_current', 1});
        vararginParams = {'dataStruct' , dS,'displayOpts', displayOpts,'saveOpts', saveOpts,'paramsMinFunc', paramsMinFunc,'batchParamsMode', batchParamsMode, 'dataShuffleMode', dataShuffleMode, 'pred_labels_train', pred_labels_train_init, 'curProjCode', curProjCode};
        checkToPause(mfilename,[],'runExample_Func(before-deepNetTrainBP)');
        [ opt_cnnnet, deepNetTrainBPOutput, welmApplyRes] = deepNetTrainBP( cnnnetInitial,vararginParams{:});
        clear vararginParams;    
    %%  STEP 6  : Final testing
    disp('****************************************FINAL*TESTING*****************************************')
        opt_cnnnet = deepNet_Test( dS, opt_cnnnet, displayOpts, saveOpts, 'train', 'Final', paramsMinFunc.maxIter+1);
        [ ~, cnnnetResults_FinalValid] = deepNet_Test( dS, opt_cnnnet, displayOpts, saveOpts, 'valid', 'Final', paramsMinFunc.maxIter+1);
        [ ~, cnnnetResults_FinalTest] = deepNet_Test( dS, opt_cnnnet, displayOpts, saveOpts, 'test', 'Final', paramsMinFunc.maxIter+1);
    % disp('*****************************************FINAL*TESTING******************************************')
    % [ deconvSet, activationValueIndicePair ] = visualizeCNNActivationResults( testData, testLabels, cnnnet, displayOpts, saveOpts, probabilitiesOut);
    
    outParams = struct;
    
    outParams.Initial = struct;
    outParams.Initial.Train = struct;
    outParams.Initial.Valid = struct;
    outParams.Initial.Test = struct;
    
    outParams.Final = struct;
    outParams.Final.Train = struct;
    outParams.Final.Valid = struct;
    outParams.Final.Test = struct;
    
    %Initial(and maybe optimized) Train Set Results
    if dS.countBatch.train>1
        outParams.Initial.Train.accuracy_Arr = cnnnetResults_InitialTrain.accuracy_Arr_Initial;
        outParams.Initial.Train.confMat_Cells = cnnnetResults_InitialTrain.confMat_Cells_Initial;
        outParams.Initial.Train.accuracy_Mean = cnnnetResults_InitialTrain.accuracy_Mean_Initial;
        outParams.Initial.Train.confMat_Mean = cnnnetResults_InitialTrain.confMat_Mean_Initial;
        if ~isempty(cnnnetResults_InitialTrain.accuracy_Arr)
            outParams.OptimizedTrain = struct;
            outParams.OptimizedTrain.accuracy_Arr = cnnnetResults_InitialTrain.accuracy_Arr;
            outParams.OptimizedTrain.confMat_Cells = cnnnetResults_InitialTrain.confMat_Cells;
            outParams.OptimizedTrain.accuracy_Mean = cnnnetResults_InitialTrain.accuracy_Mean;
            outParams.OptimizedTrain.confMat_Mean = cnnnetResults_InitialTrain.confMat_Mean;    
        end
    else
        outParams.Initial.Train.confMat = cnnnetResults_InitialTrain.confMat;
        outParams.Initial.Train.accuracy = cnnnetResults_InitialTrain.accuracy;
    end
    %Initial Validation Set Results
    if dS.countBatch.valid>1
        outParams.Initial.Valid.accuracy_Arr = cnnnetResults_InitialValid.accuracy_Arr;
        outParams.Initial.Valid.confMat_Cells = cnnnetResults_InitialValid.confMat_Cells;
        outParams.Initial.Valid.accuracy_Mean = cnnnetResults_InitialValid.accuracy_Mean;
        outParams.Initial.Valid.confMat_Mean = cnnnetResults_InitialValid.confMat_Mean;
    else
        outParams.Initial.Valid.confMat = cnnnetResults_InitialValid.confMat;
        outParams.Initial.Valid.accuracy = cnnnetResults_InitialValid.accuracy;     
    end
    %Initial Test Set Results
    if dS.countBatch.test>1
        outParams.Initial.Test.accuracy_Arr = cnnnetResults_InitialTest.accuracy_Arr;
        outParams.Initial.Test.confMat_Cells = cnnnetResults_InitialTest.confMat_Cells;
        outParams.Initial.Test.accuracy_Mean = cnnnetResults_InitialTest.accuracy_Mean;
        outParams.Initial.Test.confMat_Mean = cnnnetResults_InitialTest.confMat_Mean;
    else
        outParams.Initial.Test.confMat = cnnnetResults_InitialTest.confMat;
        outParams.Initial.Test.accuracy = cnnnetResults_InitialTest.accuracy;     
    end
    
    outParams.deepNetTrainBPOutput = deepNetTrainBPOutput;
    %Final Validation Set Results
    if dS.countBatch.valid>1
        outParams.Final.Valid.accuracy_Arr = cnnnetResults_FinalValid.accuracy_Arr;
        outParams.Final.Valid.confMat_Cells = cnnnetResults_FinalValid.confMat_Cells;
        outParams.Final.Valid.accuracy_Mean = cnnnetResults_FinalValid.accuracy_Mean;
        outParams.Final.Valid.confMat_Mean = cnnnetResults_FinalValid.confMat_Mean;
    else
        outParams.Final.Valid.confMat = cnnnetResults_FinalValid.confMat;
        outParams.Final.Valid.accuracy = cnnnetResults_FinalValid.accuracy;     
    end
    %Final Test Set Results
    if dS.countBatch.test>1
        outParams.Final.Test.accuracy_Arr = cnnnetResults_FinalTest.accuracy_Arr;
        outParams.Final.Test.confMat_Cells = cnnnetResults_FinalTest.confMat_Cells;
        outParams.Final.Test.accuracy_Mean = cnnnetResults_FinalTest.accuracy_Mean;
        outParams.Final.Test.confMat_Mean = cnnnetResults_FinalTest.confMat_Mean;
    else
        outParams.Final.Test.confMat = cnnnetResults_FinalTest.confMat;
        outParams.Final.Test.accuracy = cnnnetResults_FinalTest.accuracy;     
    end
    
    outParams.welmApplyRes = welmApplyRes;
end

function [ displayLevel, saveLevel, categoriesToLoad, batchParamsMode, dataShuffleMode, initialCNNNETFile, exampleID, softMaxInitialIteration, softMaxOptimizeMaxIteration, useAccuracyAsCost,priorInfoUse] = getOptionalParams(varargin)
  %% varargin parameters
    %P1D0 -> Passed assed paramaeter = 1, Default value used = 0
    [ displayLevel,                    saveLevel,            categoriesToLoad,  exampleID,  useAccuracyAsCost,  softMaxInitialIteration,  softMaxOptimizeMaxIteration,  batchParamsMode,  dataShuffleMode,  initialCNNNETFile,  priorInfoUse, P1D0] = parseArgs(...
    {'displayLevel'                   'saveLevel'           'categoriesToLoad' 'exampleID' 'useAccuracyAsCost' 'softMaxInitialIteration' 'softMaxOptimizeMaxIteration' 'batchParamsMode' 'dataShuffleMode' 'initialCNNNETFile' 'priorInfoUse'},...
    {    'none'     'WeightDiff_ResultEvery_ActivationLast'         []               0            false                     0                        0                         3             'uniform'              []              'none'   },...
    varargin{:});
    
    displayOpts = setDisplayOpts(displayLevel);%'none';'confusionfigures_allstrings'%'nofigures_allstrings'
    
    if (displayOpts.defaultParamSet && P1D0.useAccuracyAsCost==0)
        disp([displayOpts.ies{4} 'useAccuracyAsCost is set to -false- by default.']);
    end
    if (displayOpts.defaultParamSet && P1D0.softMaxInitialIteration==0)
        disp([displayOpts.ies{4} 'softMaxInitialIteration is set to <1> by default.']);
    end
    if (displayOpts.defaultParamSet && P1D0.softMaxOptimizeMaxIteration==0)
        disp([displayOpts.ies{4} 'softMaxOptimizeMaxIteration is set to <0> by default.']);
    end
    if (displayOpts.defaultParamSet && P1D0.batchParamsMode==0)
        disp([displayOpts.ies{4} 'batchParamsMode is set to <3> by default.']);
    end
end



