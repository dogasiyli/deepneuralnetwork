clearvars -except exampleID enforceRecreateBatches labelResetMat;%clear all;%
clc;%

    exampleID = 2;
    enforceRecreateBatches = true;
    labelResetMat = [];%[1 2 3 4;1 2 2 2];
    
    %global dropboxPath;
    global bigDataPath;
    global fastDiskTempPath;
    %set for running this script 
    %e.g. : exampleID = 1;runExample_Script;
    
    %you can reset these variables in the exampleID switch
    batchParamsMode = 1;
    dataShuffleMode = 'distCluster';%'none';%'distCluster'
    priorInfoUse = 'max';
    
    curProjCode = 'H17';
    commitCode = 'af_e46baaf';
    categoriesToLoad = [4 6];%[2 8];[1 9];[1 6];[2 10];
    batch_Count_Vec = [8 5 4 ];%batch_Count_Vec = [2 3 3];%
    validBatchID = 5;
    
    diary([fastDiskTempPath curProjCode '_mos.txt']);
    
    dataLoadFunction = @createInitialDataStruct_CIFAR;
    dataSetName = 'CIFAR10';%
    
    if ~exist('enforceRecreateBatches','var')
        enforceRecreateBatches = false;
    end
    [dS, recreated] = checkRecreateCifarBatches(categoriesToLoad, validBatchID, batch_Count_Vec, enforceRecreateBatches, labelResetMat);
    
    initialCNNNETFile = [];%[dropboxPath 'A_slctNetStruct\H11_cnnStr_eID28_acc77.91.mat'];
    switch exampleID
        case 1%CIFAR
            dataLoadParams = {[bigDataPath 'CIFAR10\'],categoriesToLoad};
            architectureIdentifier = 'CIFAR10_DEEP04_PCA_All';
            batchParamsMode = struct;
            batchParamsMode.maxIter = 20;
            batchParamsMode.epochIter = 20;  
        case 2%CIFAR
            dataLoadParams = {[bigDataPath 'CIFAR10\'],categoriesToLoad};
            architectureIdentifier = 'CIFAR10_HV01_PCA_All';
            batchParamsMode = struct;
            batchParamsMode.maxIter = 20;
            batchParamsMode.epochIter = 20;
            batchParamsMode.batchLoopType = 'all';
        case 3%CIFAR
            dataLoadParams = {[bigDataPath 'CIFAR10\'],categoriesToLoad};
            architectureIdentifier = 'CIFAR10_DEEP04';
            batchParamsMode = 4;
        case 4%CIFAR_DEEP02_PCA_Rand
            dataLoadParams = {[bigDataPath 'CIFAR10\'],categoriesToLoad};
            architectureIdentifier = 'CIFAR10_DEEP02_PCA_Rand04';
            batchParamsMode = struct;
            batchParamsMode.maxIter = 3;
            batchParamsMode.epochIter = 30;
        case 5%CIFAR
            initialCNNNETFile = 'C:\Users\dgssdmsi\OneDrive\Belgeler\slctNetStruct\S22_cnnStr_eID09_2C.mat';
            dataLoadParams = {[bigDataPath 'CIFAR10\'],categoriesToLoad};
            architectureIdentifier = 'CIFAR10_DEEP02_Rand';
            batchParamsMode = 4;
        case 6%CIFAR
            dataLoadParams = {[bigDataPath 'CIFAR10\'],categoriesToLoad};
            architectureIdentifier = 'CIFAR10_DEEP05_CONV5';
            batchParamsMode = 4;
    end

%%  STEP 1  : Learn
    [opt_cnnnet, outParams, batchInfo, saveMainFolder] = ...
                                         runExample_Func(dataSetName, dataLoadFunction, dataLoadParams...
                                         ,curProjCode , commitCode, architectureIdentifier...
                                         ,'categoriesToLoad',categoriesToLoad...
                                         ,'exampleID',exampleID...
                                         ,'batchParamsMode',batchParamsMode ...
                                         ,'dataShuffleMode', dataShuffleMode...
                                         ,'initialCNNNETFile', initialCNNNETFile...
                                         ,'priorInfoUse', priorInfoUse);
    save([saveMainFolder '\outresults.mat'],'architectureIdentifier', 'batchInfo', 'opt_cnnnet', 'outParams', 'categoriesToLoad', 'dataShuffleMode', '-v7.3');
    diary('off')
% disp('*****************************************FINAL*TESTING******************************************')
% [ deconvSet, activationValueIndicePair ] = visualizeCNNActivationResults( testData, testLabels, cnnnet, displayOpts, saveOpts, probabilitiesOut);