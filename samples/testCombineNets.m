function testCombineNets()
    rng(88);
    global fastDiskTempPath;
    R = 14;
    C = 14;
    D = 3;
    N_tr = 10;
    N_va = 5;
    N_te = 4;

%% 0. Create the data
    dataTr = rand(R,C,D,N_tr);
    dataVa = rand(R,C,D,N_va);
    dataTe = rand(R,C,D,N_te);
    
    labelsTr = randi(2,1,N_tr);
    labelsVa = randi(2,1,N_va);
    labelsTe = randi(2,1,N_te);
    
    displayOpts = setDisplayOpts('minimal');
    saveMainFolder = [fastDiskTempPath 'testCombineNets\'];
    if exist(saveMainFolder,'dir')
        rmdir(saveMainFolder,'s');
    end
    saveOpts = setSaveOpts('none', saveMainFolder);
    loadFolderForDummyData = [saveMainFolder 'data\'];
    dS = create_dS_frData(dataTr, labelsTr, 'data_valid', dataVa, 'labels_valid', labelsVa,  'data_test', dataTe, 'labels_test', labelsTe, 'loadFolder', loadFolderForDummyData);
    
%% 1. Create the networks
    varargin_1 = {  'activationType', 'relu'...
                  , 'initializeWeightMethod', 'Random'...
                  , 'applyOrthonormalization', true...
                  , 'wvnMode','initial'...
                  , 'bnMode','initial'...
                  , 'filterStatsTrackMode', 'perBatchIteration'...
                  , 'stride', [1 1] ...
                  , 'filterSize', [3 3]};
    filtCnt = [4 4 5];
    %1,2 / 14->12
    cnnnet = appendNetworkLayer( [], 'convolution', displayOpts, 'countFilt', filtCnt(1), 'inputChannelSize', 3, varargin_1{:});
    cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
    %3,4 / 12->10
    cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(2), varargin_1{:});
    cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
    %5 / 10->5
    cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [2 2], 'poolType', 'mean' );
    %6,7 / 5->3
    cnnnet = appendNetworkLayer( cnnnet, 'convolution', displayOpts, 'countFilt', filtCnt(3), varargin_1{:});
    cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts);
    %8,9 / 3->1
    cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', [3 3], 'poolType', 'mean' );
    %10
    cnnnet = appendNetworkLayer( cnnnet, 'softmax', displayOpts, 'initializeWeightMethod', 'Random', 'minFuncMaxIter', 0 ,'useAccuracyAsCost', false);
    
    vararginParams = {'displayOpts', displayOpts,'saveOpts', saveOpts,'softMaxOptimizeMaxIteration',0, 'memoryMethod', 'all_in_memory'};
    cnnnet_01 = deepNet_InitialTrain( dS, cnnnet, vararginParams{:});
    cnnnet_02 = cnnnet_01;
    cnnnet_01 = getCNNETreadyForNewData( cnnnet_01, 1, [], 1);
    cnnnet_02 = getCNNETreadyForNewData( cnnnet_02, 1, [], 1);

    for i = [1 3 6] 
        cnnnet_01{i,1}.filtersWhitened = ones(size(cnnnet_01{i,1}.filtersWhitened));
        cnnnet_01{i,1}.biasesWhitened = zeros(size(cnnnet_01{i,1}.biasesWhitened));  
        cnnnet_02{i,1}.filtersWhitened = 2*ones(size(cnnnet_02{i,1}.filtersWhitened));
        cnnnet_02{i,1}.biasesWhitened = zeros(size(cnnnet_02{i,1}.biasesWhitened));  
    end
    
    saveOpts.MainFolder = [saveMainFolder 'cnnnet_01\'];
    [cnnnet_01, ~, softMData_01_tr, ~, data_LL_01_tr, labels_LL_01_tr] = deepNet_Test(dS,cnnnet_01,displayOpts,saveOpts, 'train', 'cnnet01_{tr}', 1, false, false);
    [~, ~, softMData_01_va, ~, data_LL_01_va, labels_LL_01_va] = deepNet_Test(dS,cnnnet_01,displayOpts,saveOpts, 'valid', 'cnnet01_{va}', 1, false, false);
    [~, ~, softMData_01_te, ~, data_LL_01_te, labels_LL_01_te] = deepNet_Test(dS,cnnnet_01,displayOpts,saveOpts, 'test', 'cnnet01_{te}', 1, false, false);
    saveOpts.MainFolder = [saveMainFolder 'cnnnet_02\'];
    [cnnnet_02, ~, softMData_02_tr, ~, data_LL_02_tr, labels_LL_02_tr] = deepNet_Test(dS,cnnnet_02,displayOpts,saveOpts, 'train', 'cnnet02_{tr}', 1, false, false);
    [~, ~, softMData_02_va, ~, data_LL_02_va, labels_LL_02_va] = deepNet_Test(dS,cnnnet_02,displayOpts,saveOpts, 'valid', 'cnnet02_{va}', 1, false, false);
    [~, ~, softMData_02_te, ~, data_LL_02_te, labels_LL_02_te] = deepNet_Test(dS,cnnnet_02,displayOpts,saveOpts, 'test', 'cnnet02_{te}', 1, false, false);
    

    dtStr = {'Train','Valid','Test'};
    for c = 1:2
        for i=1:6
            for dt = 1:3
                loadFileName = [saveMainFolder 'cnnnet_' num2str(c,'%02d') '\' dtStr{dt} '\ActivatedData\Activation_' num2str(i,'%02d') '.mat'];
                load(loadFileName);
                evalStr = ['a_L' num2str(i,'%02d') '_CNN' num2str(c,'%02d') '_' dtStr{dt} ' = a;'];
                disp(loadFileName)
                disp(evalStr)
                eval(evalStr);
            end
        end
    end
    clear a loadFileName evalStr c i dt;   
    
    net1fn = [saveMainFolder 'cnnet01.mat'];
    net2fn = [saveMainFolder 'cnnet02.mat'];
    cnnnet_01 = getCNNETreadyForNewData( cnnnet_01, 1, [], 1);
    cnnnet = cnnnet_01;
    save(net1fn, 'cnnnet');
    cnnnet_02 = getCNNETreadyForNewData( cnnnet_02, 1, [], 1);
    cnnnet = cnnnet_02;
    save(net2fn, 'cnnnet');
    clear cnnnet;
    
    [cnnnet_0102, combinedCNNNETFile0102] = combineTwoNet( net1fn, net2fn, 'cnnet0102', false);
    
    saveOpts.MainFolder = [saveMainFolder 'cnnnet_0102\'];
    [cnnnet_0102, ~, softMData_0102_tr, ~, data_LL_0102_tr, labels_LL_0102_tr] = deepNet_Test(dS,cnnnet_0102,displayOpts,saveOpts, 'train', 'cnnet0102_{tr}', 1, false, false);
    [~, ~, softMData_0102_va, ~, data_LL_0102_va, labels_LL_0102_va] = deepNet_Test(dS,cnnnet_0102,displayOpts,saveOpts, 'valid', 'cnnet0102_{va}', 1, false, false);
    [~, ~, softMData_0102_te, ~, data_LL_0102_te, labels_LL_0102_te] = deepNet_Test(dS,cnnnet_0102,displayOpts,saveOpts, 'test', 'cnnet0102_{te}', 1, false, false);
    
    sumDifResults = NaN(6,3);
    for i=2:6
        for dt = 1:3
                loadFileName = [saveMainFolder 'cnnnet_0102\' dtStr{dt} '\ActivatedData\Activation_' num2str(i,'%02d') '.mat'];
                load(loadFileName);
                aCombStr = ['a_L' num2str(i,'%02d') '_CNNComb0102_' dtStr{dt}];
                evalStr = [aCombStr ' = a;'];
                disp(loadFileName)
                disp(evalStr)
                eval(evalStr);
                
                a1Str = ['a_L' num2str(i,'%02d') '_CNN' num2str(1,'%02d') '_' dtStr{dt}];
                a2Str = ['a_L' num2str(i,'%02d') '_CNN' num2str(2,'%02d') '_' dtStr{dt}];
                %eval2str = ['sumDifResults(i,dt) = sum(sum(a_L06_CNNComb0102_Valid-[a_L06_CNN01_Valid;a_L06_CNN02_Valid]));'];
                eval2str = ['sumDifResults(i,dt) = sum(sum(' aCombStr '-[' a1Str ';' a2Str ']));'];
                disp(eval2str)
                eval(eval2str);                
        end
    end
    clear a loadFileName evalStr aCombStr a1Str a2Str i dt;
    disp(sumDifResults);
end