function [ patches, sizeVis ] = acquirePatchesFunc_surray(data, varargin)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

    [     method,      P1D0] = parseArgs(...
    {    'method'     },...
    {'assignDirectly' },...
    varargin{:});

    switch method
        case 'assignDirectly'
            patches = reshape(data,[],size(data,4));
            sizeVis = numel(data)/size(data,4);
    end
end