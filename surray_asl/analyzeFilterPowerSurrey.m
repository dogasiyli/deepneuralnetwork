function [discriminationPowerPerLayer, replacableFiltersSummary, filterAnalysisCell] = analyzeFilterPowerSurrey(cnnetArchitectureFileName, dataTypeStr, saveFolderMaster, batchIDsToEvaluate)
    %[discriminationPowerPerLayer, replacableFiltersSummary, filterAnalysisCell] = analyzeFilterPowerSurrey(cnnetArchitectureFileName, 'test', 'C:\FilterPowerFigures\');

    cnnnet = [];
    %cnnetArchitectureFileName = 'D:\cnnnetStruct_commitCode(f70c952)-ds(SURREY-resized[80 80])-exampleID(2)_accuracy(97.96).mat';
    %cnnetArchitectureFileName = 'C:\FastDiskTemp\cnnnetStruct_00_accuracy(92.87).mat';
    columnNamesCells_01 = cell(1,12);
    columnNamesCells_01{1,1} = 'filterID';
    columnNamesCells_01{1,2} = 'precisionOld';
    columnNamesCells_01{1,3} = 'precisionNew';
    columnNamesCells_01{1,4} = 'IPValue';
    columnNamesCells_01{1,5} = 'IPCategory';
    columnNamesCells_01{1,6} = 'sensitivityOld';
    columnNamesCells_01{1,7} = 'sensitivityNew';
    columnNamesCells_01{1,8} = 'RPValue';
    columnNamesCells_01{1,9} = 'RPCategory';
    columnNamesCells_01{1,10} = 'IPRPCategory';
    columnNamesCells_01{1,11} = 'ImportanceValue';
    columnNamesCells_01{1,12} = 'ImportanceValue2';
    
    %saveFolderMaster = ['C:\FilterPowerFigures\' dataTypeStr '\'];
    saveFolderMaster = [saveFolderMaster dataTypeStr '\'];
    if ~exist(saveFolderMaster,'dir')
        mkdir(saveFolderMaster);
    end
    load(cnnetArchitectureFileName);
    layerCount = size(cnnnet,1);
    
    dataLoadFunction = @createInitialDataStruct_surray;
    dataLoadParams = {'resized[80 80]'};
    dataSetName = ['SURREY-' dataLoadParams{1,1}];%
    [dS, batchInfo] = feval(dataLoadFunction, dataLoadParams{:});
    
    batchCount = eval(['dS.countBatch.' dataTypeStr]);
    if exist('batchIDsToEvaluate','var')
        batchIDsToEvaluate(batchIDsToEvaluate>batchCount) = [];
    else
        batchIDsToEvaluate = 1:batchCount;
    end
    batchCount = length(batchIDsToEvaluate);
    dS = dS.updateDataStruct(dS, {'type', dataTypeStr});

    filterAnalysisCell = cell(batchCount, layerCount);
    replacableFiltersSummary = cell(layerCount,1);
    countCategory = dS.countCategory;
    confMasterBatch = zeros(countCategory,countCategory,batchCount);
    discriminationPowerPerLayer = zeros(countCategory,layerCount,batchCount);
    accMasterBatch = zeros(1,batchCount);
    replacableFilterExcelValues = [];
    for b=1:batchCount
        curBatchID = batchIDsToEvaluate(b);
        dS = dS.updateDataStruct(dS, {'batchID_current', curBatchID});
        load(dS.fileName_current,'data','labels');
        testData = reshape(data,dS.countFeat,[]);
        X = testData;
        [cnnnet, accMasterBatch(b), confMasterBatch(:,:,b)] = getCurrentResult(X, labels, cnnnet);
        h = drawConfusionMatWithNumbers( squeeze(confMasterBatch(:,:,b)), struct('figureID', {layerCount+1}, 'figureTitle' , ['BatchID(' num2str(curBatchID) ') of ' dataSetName ' ' dataTypeStr ' data-masterAcc(' num2str(accMasterBatch(b),'%4.2f') ')']));
        confMatMaster_figure_SaveFileName = [saveFolderMaster 'confusionMat_batch' num2str(b,'%02d') '.png'];
        saveas(h,confMatMaster_figure_SaveFileName);
        
        for layerID = 1:layerCount
            switch cnnnet{layerID,1}.type
                case 'convolution'
                    %for each filter set values of that filter to 0 and
                    %re-evaluate the cnnnet
                    countFilt = cnnnet{layerID,1}.countFilt;
                    
                    saveFolderFilter = [saveFolderMaster cnnnet{layerID,1}.name '\'];
                    if ~exist(saveFolderFilter,'dir')
                        mkdir(saveFolderFilter);
                    end    
                        
                    confRsltPerFilter = zeros(countCategory,countCategory,countFilt);
                    confDiffPerFilter = zeros(countCategory,countCategory,countFilt);
                    discriminationPowerPerFilter = zeros(countCategory,11,countFilt);
                    filterAnalysisCell{b, layerID} = struct;
                    accDiff = NaN(countFilt,1);
                    filterPower_excel_SaveFileName = [saveFolderFilter 'filterPower_batch' num2str(b,'%02d') '_filtAll.xlsx'];
                    for f = 1:countFilt
                        cnnnet_new = setFilterToZero(cnnnet,layerID,f);
                        [cnnnet_new, accCurrent, confMatCurrent] = getCurrentResult(X, labels, cnnnet_new);
                        confMatDiff = confMatCurrent - squeeze(confMasterBatch(:,:,b));
                        accDiff(f) = accCurrent - accMasterBatch(b);
                        
                        h = drawConfusionMatWithNumbers( confMatCurrent, struct('figureID', {layerCount+2}, 'figureTitle' , ['confNew,layerName(' cnnnet_new{layerID,1}.name '), filterID(' num2str(f) '),accDif(' num2str(accDiff(f),'%4.2f') ')-masterAcc(' num2str(accMasterBatch(b),'%4.2f') ')']));
                        confMatMaster_figure_SaveFileName = [saveFolderFilter 'confusionMat_batch' num2str(b,'%02d') '_filt' num2str(f,'%02d') '.png'];
                        saveas(h,confMatMaster_figure_SaveFileName);
                        h = drawConfusionMatWithNumbers( confMatCurrent-squeeze(confMasterBatch(:,:,b)), struct('figureID', {layerCount+5}, 'figureTitle' , ['confDif,layerName(' cnnnet_new{layerID,1}.name '), filterID(' num2str(f) '),accDif(' num2str(accDiff(f),'%4.2f') ')-masterAcc(' num2str(accMasterBatch(b),'%4.2f') ')']));
                        confMatMaster_figure_SaveFileName = [saveFolderFilter 'confusionMatDif_batch' num2str(b,'%02d') '_filt' num2str(f,'%02d') '.png'];
                        saveas(h,confMatMaster_figure_SaveFileName);
                        
                        confRsltPerFilter(:,:,f) = confMatCurrent;
                        confDiffPerFilter(:,:,f) = confMatDiff;
                        filterAnalysisCell{b, layerID}.accDif(f) = accDiff(f);
                        
                        for ctg = 1:countCategory
                            discriminationPowercurFilter = calcDiscriminationPowerPerCategory(squeeze(confMasterBatch(:,:,b)), confMatCurrent, ctg);
                            discriminationPowerPerFilter(ctg,:,f) = discriminationPowercurFilter;
                        end
                        h = displayPowerForFilter(discriminationPowerPerFilter, layerCount, f, accDiff, accMasterBatch(b));
                        filterPower_figure_SaveFileName = [saveFolderFilter 'filterPower_batch' num2str(b,'%02d') '_filt' num2str(f,'%02d') '.png'];
                        saveas(h,filterPower_figure_SaveFileName);
                        
                        drawnow;
                        
                        discriminationPowerCurFilter = [repmat(f,countCategory,1),squeeze(discriminationPowerPerFilter(:,:,f))];
                        row_Beg = countCategory*(f-1)+2;
                        if f==1
                            writeToExcelFile( filterPower_excel_SaveFileName, discriminationPowerCurFilter, 'sheetID', 1, 'row_Beg', 1, 'col_Beg', 2, 'columnNamesCells', columnNamesCells_01);
                        else
                            writeToExcelFile( filterPower_excel_SaveFileName, discriminationPowerCurFilter, 'sheetID', 1, 'row_Beg', row_Beg, 'col_Beg', 2, 'columnNamesCells', []);
                        end
                    end
                    
                    [discriminationPowerCurrentLayer_max, h] = displayPowerForLayer(discriminationPowerPerFilter, layerCount, layerID);
                    filterPower_figure_SaveFileName = [saveFolderFilter 'filterPower_batch' num2str(b,'%02d') '_filtAll.png'];
                    saveas(h,filterPower_figure_SaveFileName);
                    
                    discriminationPowerPerFilterCurrentLayer = squeeze(discriminationPowerPerFilter(:,:,10));
                    rejectionPowerPerFilterCurrentLayer = squeeze(discriminationPowerPerFilter(:,:,7));
                    identifyingPowerPerFilterCurrentLayer = squeeze(discriminationPowerPerFilter(:,:,3));
                    filterPower_excel_SaveFileName = [saveFolderFilter 'filterPower_batch' num2str(b,'%02d') '_filtALL.xlsx'];
                    writeToExcelFile( filterPower_excel_SaveFileName, discriminationPowerPerFilterCurrentLayer, 'sheetID', 2, 'col_Beg', 2, 'columnNamesCells', columnNamesCells_01(1,2:end));
                    writeToExcelFile( filterPower_excel_SaveFileName, rejectionPowerPerFilterCurrentLayer, 'sheetID', 3, 'col_Beg', 2, 'columnNamesCells', columnNamesCells_01(1,2:end));
                    writeToExcelFile( filterPower_excel_SaveFileName, identifyingPowerPerFilterCurrentLayer, 'sheetID', 4, 'col_Beg', 2, 'columnNamesCells', columnNamesCells_01(1,2:end));
                     
                    
                    discriminationPowerPerLayer(:,layerID,b) = discriminationPowerCurrentLayer_max;
                    
                    filterReplacabilityValue = 1-abs(filterAnalysisCell{b, layerID}.accDif ./ accMasterBatch(b));
                    if isempty(replacableFiltersSummary{layerID,1})
                        replacableFiltersSummary{layerID,1} = zeros(batchCount, countFilt);
                    end                    
                    replacableFiltersSummary{layerID,1}(b,:) = filterReplacabilityValue;
                    
                    [filterReplacabilityValue, replacableFilterIDs] = sort(filterReplacabilityValue,'descend');
                    replacableFilterIDs = replacableFilterIDs(filterReplacabilityValue>0.95);
                    filterReplacabilityValue = filterReplacabilityValue(1:length(replacableFilterIDs));
                    disp('Replacable filters and their replacibility:');
                    replacableFilterExcelValues_cur = [repmat([b layerID],length(replacableFilterIDs),1) replacableFilterIDs' filterReplacabilityValue'];
                    disp(replacableFilterExcelValues_cur);
                    
                    replacableFilterExcelValues = [replacableFilterExcelValues;replacableFilterExcelValues_cur];
                    replacableFilter_excel_SaveFileName = [saveFolderMaster 'replacableFiltersALL.xlsx'];
                    writeToExcelFile( replacableFilter_excel_SaveFileName, replacableFilterExcelValues, 'sheetID', 1, 'col_Beg', 2, 'columnNamesCells', {'batchID' 'layerID' 'filterID' 'replacabilityValue'});
                    
                    
                    filterAnalysisCell{b, layerID}.confRsltPerFilter = confRsltPerFilter;
                    filterAnalysisCell{b, layerID}.confDiffPerFilter = confDiffPerFilter;
                    filterAnalysisCell{b, layerID}.accDif = accDiff;
                    filterAnalysisCell{b, layerID}.discriminationPowerPerFilter = discriminationPowerPerFilter;
                    clear confRsltPerFilter confDiffPerFilter
                case 'activation'
                case 'pooling'
                case 'softmax'
                case 'fullyconnected'
                    error('not implemented yet');
                otherwise
                    error('You are suggesting a layer that is not present.Layers are - convolution, activation, pooling, softmax');
            end
        end                     
    end 
    save([saveFolderMaster dataSetName '_' dataTypeStr '.mat'],'discriminationPowerPerLayer', 'replacableFiltersSummary', 'filterAnalysisCell');
end

function [discriminationPowerCurrentLayer_max, h]= displayPowerForLayer(discriminationPowerPerFilter, layerCount, layerID)
    discriminationPowerPerFilter = permute(discriminationPowerPerFilter,[1 3 2]);
    
    discriminationPowerPerFilterCurrentLayer = squeeze(discriminationPowerPerFilter(:,:,10));
    rejectionPowerPerFilterCurrentLayer = squeeze(discriminationPowerPerFilter(:,:,7));
    identifyingPowerPerFilterCurrentLayer = squeeze(discriminationPowerPerFilter(:,:,3));
    
    discriminationPowerCurrentLayer_max = max(discriminationPowerPerFilterCurrentLayer,[],2);
    rejectionPowerCurrentLayer_max = max(rejectionPowerPerFilterCurrentLayer,[],2);
    identifyingPowerCurrentLayer_max = max(identifyingPowerPerFilterCurrentLayer,[],2);
    
    h = figure(layerCount+4);clf;

    subplot(1,3,1);
    [discriminationPowerCurrentLayer_toDisp, labelsIDX] = sort(discriminationPowerCurrentLayer_max,'descend');
    labelsIDX = reshape(labelsIDX,1,[]);
    imagesc(discriminationPowerCurrentLayer_toDisp);colorbar;
    YTickLabel_strcell = textscan(num2str(labelsIDX),'%s');
    YTickLabel_strcell = YTickLabel_strcell{1,1};
    set(gca,'YTick',1:length(labelsIDX));
    set(gca,'YTickLabel',YTickLabel_strcell);  
    title(['discriminate-lay' num2str(layerID) '-m(' num2str(labelsIDX(1)) '),l(' num2str(labelsIDX(end)) ')']);
    
    subplot(1,3,2);
    [rejectionPowerCurrentLayer_toDisp, labelsIDX] = sort(rejectionPowerCurrentLayer_max,'descend');
    labelsIDX = reshape(labelsIDX,1,[]);
    imagesc(rejectionPowerCurrentLayer_toDisp);colorbar;
    YTickLabel_strcell = textscan(num2str(labelsIDX),'%s');
    YTickLabel_strcell = YTickLabel_strcell{1,1};
    set(gca,'YTick',1:length(labelsIDX));
    set(gca,'YTickLabel',YTickLabel_strcell);  
    title(['reject-lay' num2str(layerID) '-m(' num2str(labelsIDX(1)) '),l(' num2str(labelsIDX(end)) ')']);
    
    subplot(1,3,3);
    [identifyingPowerCurrentLayer_toDisp, labelsIDX] = sort(identifyingPowerCurrentLayer_max,'descend');
    labelsIDX = reshape(labelsIDX,1,[]);
    imagesc(identifyingPowerCurrentLayer_toDisp);colorbar;
    YTickLabel_strcell = textscan(num2str(labelsIDX),'%s');
    YTickLabel_strcell = YTickLabel_strcell{1,1};
    set(gca,'YTick',1:length(labelsIDX));
    set(gca,'YTickLabel',YTickLabel_strcell);  
    title(['identify-lay' num2str(layerID) '-m(' num2str(labelsIDX(1)) '),l(' num2str(labelsIDX(end)) ')']);
end
%displayPowerForFilter(discriminationPowerPerFilter, layerCount, f, accDiff, accMasterBatch(b));
function h = displayPowerForFilter(discriminationPowerPerFilter, layerCount, filterID, accDiff, initialAccuracy)
    h = figure(layerCount+3);clf;

    rejectionPowerPerFilter_toDisp = squeeze(discriminationPowerPerFilter(:,7,filterID));
    [rejectionPowerPerFilter_toDisp, labelsIDX] = sort(rejectionPowerPerFilter_toDisp,'descend');
    labelsIDX = reshape(labelsIDX,1,[]);

    subplot(1,2,1);
    imagesc(rejectionPowerPerFilter_toDisp);colorbar;
    YTickLabel_strcell = textscan(num2str(labelsIDX),'%s');
    YTickLabel_strcell = YTickLabel_strcell{1,1};
    set(gca,'YTick',1:length(labelsIDX));
    set(gca,'YTickLabel',YTickLabel_strcell);  
    title(['RejectionPower-filt(' num2str(filterID) '),accDif(' num2str(accDiff(filterID),'%4.2f') ')-accInit(' num2str(initialAccuracy,'%4.2f') ')']);
    
    identifyPowerPerFilter_toDisp = squeeze(discriminationPowerPerFilter(:,3,filterID));
    [identifyPowerPerFilter_toDisp, labelsIDX] = sort(identifyPowerPerFilter_toDisp,'descend');
    labelsIDX = reshape(labelsIDX,1,[]);

    subplot(1,2,2);
    imagesc(identifyPowerPerFilter_toDisp);colorbar;
    YTickLabel_strcell = textscan(num2str(labelsIDX),'%s');
    YTickLabel_strcell = YTickLabel_strcell{1,1};
    set(gca,'YTick',1:length(labelsIDX));
    set(gca,'YTickLabel',YTickLabel_strcell);  
    title(['IdentifyPower-filt(' num2str(filterID) '),accDif(' num2str(accDiff(filterID),'%4.2f') ')-accInit(' num2str(initialAccuracy,'%4.2f') ')']);    
end
function discriminationPowerOfFilter = calcDiscriminationPowerPerCategory(confMasterBatch, confMatCurrent, categoryID)
    levelTresholds = [0.00 0.10 0.50 0.75 0.90 1.01];
    %11 values 
    confMatStats_Current = calcConfusionStatistics(confMatCurrent, categoryID);   
    confMatStats_Master = calcConfusionStatistics(confMasterBatch, categoryID);
    
    precisionOld = confMatStats_Master.Precision;
    precisionNew = confMatStats_Current.Precision;
    IPValue = (precisionOld-precisionNew)/precisionOld;
    IPValue = max(min(IPValue,1),0);
    IPCategory = find(levelTresholds<=IPValue,1,'last');

    sensitivityOld = confMatStats_Master.Sensitivity;
    sensitivityNew = confMatStats_Current.Sensitivity;
    if confMatStats_Current.totalPredictionOfCategory == 0
        %sensitivityNew is NaN
        %this means there is no samples that is labelled as this class
        %hence rejection power has to be zero!! ??
        RPValue = 0;
    else
        RPValue = (sensitivityOld-sensitivityNew)/sensitivityOld;
    end
    RPValue = max(min(RPValue,1),0);
    RPCategory = find(levelTresholds<=RPValue,1,'last');
    
    IPRPCategory = sub2ind([5 5],RPCategory,IPCategory);
    ImportanceValue = (IPValue + RPValue)/2;
    ImportanceValue2 = max(IPValue,RPValue);
    
    discriminationPowerOfFilter = [precisionOld precisionNew IPValue IPCategory sensitivityOld sensitivityNew RPValue RPCategory IPRPCategory ImportanceValue ImportanceValue2];
end
function cnnnet_new = setFilterToZero(cnnnet,layer,f)
%% 1. Analyze the network structure of cnnnet
    %cnnnet = cell(7,1); --> cnnnet is initialied somewhat like this
    cnnnet_new = cnnnet;
    curCell = cnnnet_new{layer,1};
    switch curCell.type
        case 'convolution'
            W_size = [curCell.inputChannelSize*prod(curCell.filterSize), curCell.countFilt];
            W = curCell.filtersWhitened;
            difOfSize =  sum(size(W)-W_size);
            assert(difOfSize==0,'problem with weight size');
            %check the size of W and find the associated value indices to
            %set to zero
            W(:,f) = 0;
            curCell.filtersWhitened = W;
            if isfield(curCell,'W_Conv_Mapped')
                curCell = rmfield(curCell,'W_Conv_Mapped');
            end
        case {'activation','pooling'}
            error('no weight values to update');
        case 'softmax'
            error('you can not update softmax weight values');
        case 'fullyconnected'
            %W = theta(1:w_len);
            %curCell.filtersWhitened = reshape(W,size(curCell.filtersWhitened));
            error('not implemented yet');
        otherwise
            error([curCell.type ' is not implemented yet'])
    end
    cnnnet_new{layer,1} = curCell;
end
function [ cnnnet, accuracyCurBatch, confCurBatch] = getCurrentResult(data, labels, cnnnet)
    displayOpts = setDisplayOpts('none');
    saveOpts = setSaveOpts('none');
    calculateBackPropMap = false;
    [ cnnnet, ~, accuracyCurBatch, confCurBatch] = ...
                      forwardPropogateDeepNet( data, labels, cnnnet ...
                                              ,'calculateBackPropMap', calculateBackPropMap ...
                                              ,'displayOpts', displayOpts ...
                                              ,'saveOpts', saveOpts); 
end