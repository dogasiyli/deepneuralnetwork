function cnnnet_appended = appendFullyConnectedLayer( cnnnet_struct_fileName, categoriesToLoad, appendMode)
%appendFullyConnectedLayer_bathces appends fully connected layer at the end
%of an already learn cnnnet struct to increase accuracy
%   We have 2 options to do this.. either batch by batch, or by collecting
%   truly classified one after another..
% appendMode = {'batchMode','greedyCorrect'}

%% 1.set the necessary parameters
    global bigDataPath;
    displayOpts = setDisplayOpts('none');
    saveOpts = setSaveOpts('none');
    calculateBackPropMap = false;
    memoryMethod = 'all_in_memory';
    dataLoadFunction = @createInitialDataStruct_CIFAR;
    %categoriesToLoad = [2 10];
    dataLoadParams = {[bigDataPath 'CIFAR10\'],categoriesToLoad};    
    dS = feval(dataLoadFunction, dataLoadParams{:});
    load(cnnnet_struct_fileName,'opt_cnnnet');
    cnnnet_initial = opt_cnnnet;
    clear opt_cnnet
    %[cnnnet, cnnnetResults_Init, ~, pred_labels_train_Init, data_LL_Init, labels_LL_Init] = deepNet_Test( dS, cnnnet_initial, displayOpts, saveOpts, 'train', '',1);

    if strcmp(appendMode,'greedyCorrect')
        data_LL_Train = [];
        labels_LL_Train = [];
    end 

    dataTypeStr = 'train';
    batchCount = eval(['dS.countBatch.' dataTypeStr]);
    dS = dS.updateDataStruct(dS, {'type', dataTypeStr});
    cnnnet_initial = setVarianceBias_ConvFalse(cnnnet_initial);
    pred_labels = struct;
    if strcmp(appendMode,'batchMode')
        elmStruct = struct;
    end  
    if (batchCount>1)
        accuracy_Arr = zeros(1,batchCount);
        confMat_Cells = cell(1,batchCount);
        accuracy_Mean = 0;
        conf_Mean = [];
        for b=1:batchCount
            dS = dS.updateDataStruct(dS, {'batchID_current', b});
            if displayOpts.fileOperations
                disp([displayOpts.ies{4} 'Loading ' dS.fileName_current ''])
            else
                disp(['Running for batch(' num2str(b) ')']);
            end
            load(dS.fileName_current,'data','labels');
            data = reshape(data,dS.countFeat,[]);
            [ cnnnet_initial, softMaxedData, accuracyCurBatch, confCurBatch, activationID ] = ...
                                                              forwardPropogateDeepNet( data, labels, cnnnet_initial ...
                                                                                      ,'calculateBackPropMap', calculateBackPropMap ...
                                                                                      ,'displayOpts', displayOpts ...
                                                                                      ,'saveOpts', saveOpts ...
                                                                                      ,'dataStruct', dS...
                                                                                      ,'memoryMethod',memoryMethod); 
            [~,predCur] = max(softMaxedData);
            pred_labels(b).pred = predCur;
            pred_labels(b).labels = labels;
            pred_labels(b).softMaxedData = softMaxedData;
            disp(['**Acc(' num2str(accuracyCurBatch,'%4.2f') '). confMat : ']);
            displayConfusionMatrix(confCurBatch,unique(labels));
            if strcmp(appendMode,'greedyCorrect')
                [data_LL_Train,labels_LL_Train, a] = appendLastLayerData(cnnnet_initial, data_LL_Train, labels_LL_Train,displayOpts, saveOpts, labels);
            end 
            accuracy_Arr(b) = accuracyCurBatch;
            confMat_Cells{b} = confCurBatch;
            accuracy_Mean = (accuracy_Mean*(b-1) + accuracyCurBatch)/(b);
            if isempty(conf_Mean)
                conf_Mean = confCurBatch;
            else
                conf_Mean = conf_Mean + confCurBatch;
            end
            try %#ok<TRYNC>
                [titleStr, resultsFigureFileName] = createFigureAndTitleNames(epochStr, batchCount, b, accuracyCurBatch, resultsFigureSaveFolder, dataTypeStr, testInitFinalStr);
                h = drawConfusionMatWithNumbers( confCurBatch, struct('figureID', {layerCount+1}, 'figureTitle' , titleStr));
                saveas(h,resultsFigureFileName);
            end
            
            if strcmp(appendMode,'batchMode')
                [Welm_b, ~, confMat_ELM_b, accuracy_ELM_b, rankDeficient_b] = getELM_Weights_SoftMax(a', labels);
                disp([displayOpts.ies{4} 'ELM acc(' num2str(accuracy_ELM_b,'%4.2f') ') for batchID(' num2str(b) ').']);
                display(confMat_ELM_b);
                elmStruct(b).Welm_b = Welm_b;
                elmStruct(b).confMat_ELM_b = confMat_ELM_b;
                elmStruct(b).accuracy_ELM_b = accuracy_ELM_b;
                elmStruct(b).rankDeficient_b = rankDeficient_b;
            end
        end 
        try %#ok<TRYNC>
            [titleStr, resultsFigureFileName] = createFigureAndTitleNames(epochStr, [], [], accuracy_Mean, resultsFigureSaveFolder, dataTypeStr, testInitFinalStr);
            h = drawConfusionMatWithNumbers( conf_Mean, struct('figureID', {layerCount+1}, 'figureTitle' , titleStr));
            saveas(h,resultsFigureFileName);
        end
        
        cnnnetResults_Init = struct;
        cnnnetResults_Init.accuracy_Arr = accuracy_Arr;
        cnnnetResults_Init.confMat_Cells = confMat_Cells;
        cnnnetResults_Init.accuracy_Mean = accuracy_Mean;
        cnnnetResults_Init.confMat_Mean = conf_Mean;
    end
    [~, cnnR_01_Valid, ~, pred_labels_valid_Init, data_LL_Valid, labels_LL_Valid] = deepNet_Test( dS, cnnnet_initial, displayOpts, saveOpts, 'valid', '',1);
    [~, cnnR_01_Test, ~, pred_labels_test_Init, data_LL_Test, labels_LL_Test] = deepNet_Test( dS, cnnnet_initial, displayOpts, saveOpts, 'test', '',1);
    
    [remainingFeatIDs, resultSummary] = findIrrelevantFeatures(data_LL_Train', labels_LL_Train, 'ELM_LDA');
    
    dataSt = struct;
    dataSt.dataTr = data_LL_Train(remainingFeatIDs,:);
    dataSt.dataVa = data_LL_Valid(remainingFeatIDs,:);
    dataSt.dataTe = data_LL_Test(remainingFeatIDs,:); 
    dataSt.labelsTr = labels_LL_Train;
    dataSt.labelsVa = labels_LL_Valid;
    dataSt.labelsTe = labels_LL_Test;
    [resultTable, W_2Layer, labelCrossMat, confMatCell] = apply_XOR_Test(dataSt, 10, maxClusterCount, false, []);
    disptable(resultTable,'hidNodeCnt|TrELM|TrOrigMapped|Tr2Layer|Va2L|Te2L');
    
    cnnnet_new = cnnnet_initial(1:end-1);
    layerType_2 = cnnnet_new{end,1}.type;
    layerType_1 = cnnnet_new{end-1,1}.type;
    layerType_0 = cnnnet_new{end-2,1}.type;
    switch layerType_1
        case 'activation'
            assert(strcmp(layerType_0,'convolution'),'otherwise unknown!!')
            cnnnet_new{end-2,1}.countFilt = length(remainingFeatIDs);
            cnnnet_new{end-2,1}.outputSize_original(3) = length(remainingFeatIDs);
            cnnnet_new{end-2,1}.outputSize_vectorized = [prod(cnnnet_new{end-2,1}.outputSize_original(1:3)) cnnnet_new{end-2,1}.outputSize_original(4)];
            cnnnet_new{end-2,1}.filtersWhitened = cnnnet_new{end-2,1}.filtersWhitened(:,remainingFeatIDs);
            cnnnet_new{end-2,1}.biasesWhitened = cnnnet_new{end-2,1}.biasesWhitened(remainingFeatIDs);
            if isfield(cnnnet_new{end-2,1},'weightVarianceValsInitial')
                cnnnet_new{end-2,1}.weightVarianceValsInitial = cnnnet_new{end-2,1}.weightVarianceValsInitial(remainingFeatIDs);
            end
            if isfield(cnnnet_new{end-2,1},'outputFilterStatsAllBatchesInitial')
                cnnnet_new{end-2,1}.outputFilterStatsAllBatchesInitial = cnnnet_new{end-2,1}.outputFilterStatsAllBatchesInitial(remainingFeatIDs,:,:);
            end
            if isfield(cnnnet_new{end-2,1},'outputFilterStats')
                cnnnet_new{end-2,1}.outputFilterStats = cnnnet_new{end-2,1}.outputFilterStats(remainingFeatIDs,:,:);
            end
            if isfield(cnnnet_new{end-2,1},'W_ConvMap')
                cnnnet_new{end-2,1} = rmfield(cnnnet_new{end-2,1},'W_ConvMap');
            end
            if isfield(cnnnet_new{end-2,1},'W_Conv_Mapped')
                cnnnet_new{end-2,1} = rmfield(cnnnet_new{end-2,1},'W_Conv_Mapped');
            end
            cnnnet_new{end-1,1}.inputSize_original = cnnnet_new{end-2,1}.outputSize_original;
            cnnnet_new{end-1,1}.inputSize_vectorized = cnnnet_new{end-2,1}.outputSize_vectorized;            
            cnnnet_new{end-1,1}.outputSize_original = cnnnet_new{end-1,1}.inputSize_original;
            cnnnet_new{end-1,1}.outputSize_vectorized = cnnnet_new{end-1,1}.inputSize_vectorized;            
            if isfield(cnnnet_new{end-1,1},'outputFilterStatsAllBatchesInitial')
                cnnnet_new{end-1,1}.outputFilterStatsAllBatchesInitial = cnnnet_new{end-1,1}.outputFilterStatsAllBatchesInitial(remainingFeatIDs,:,:);
            end
        case 'fullyconnected'
            error('not implemented yet')
        otherwise
            error('unknown combination technique')
    end
    switch layerType_2
        case 'pooling'
            cnnnet_new{end,1}.inputSize_original = cnnnet_new{end-1,1}.outputSize_original;
            cnnnet_new{end,1}.inputSize_vectorized = cnnnet_new{end-1,1}.outputSize_vectorized;
            cnnnet_new{end,1}.outputSize_original(3) = cnnnet_new{end,1}.inputSize_original(3);
            cnnnet_new{end,1}.outputSize_vectorized = [prod(cnnnet_new{end,1}.outputSize_original(1:3)) cnnnet_new{end,1}.outputSize_original(4)];
            cnnnet_new{end,1}.sizeChn = length(remainingFeatIDs);
            if isfield(cnnnet_new{end,1},'poolMapInc')
                cnnnet_new{end,1} = rmfield(cnnnet_new{end,1},'poolMapInc');
            end
            if isfield(cnnnet_new{end,1},'poolMap')
                cnnnet_new{end,1} = rmfield(cnnnet_new{end,1},'poolMap');
            end
            if isfield(cnnnet_new{end,1},'backPropInd')
                cnnnet_new{end,1} = rmfield(cnnnet_new{end,1},'backPropInd');
            end
            if isfield(cnnnet_new{end,1},'outputFilterStatsAllBatchesInitial')
                cnnnet_new{end,1}.outputFilterStatsAllBatchesInitial = cnnnet_new{end,1}.outputFilterStatsAllBatchesInitial(remainingFeatIDs,:,:);
            end
        case 'activation'
            cnnnet_new{end,1}.inputSize_original = cnnnet_new{end-1,1}.outputSize_original;
            cnnnet_new{end,1}.inputSize_vectorized = cnnnet_new{end-1,1}.outputSize_vectorized;
            cnnnet_new{end,1}.outputSize_original = cnnnet_new{end,1}.inputSize_original;
            cnnnet_new{end,1}.outputSize_vectorized = cnnnet_new{end,1}.inputSize_vectorized;
        otherwise
            error('unknown combination technique');
    end
    
    sizeHid = resultTable(end,1);
    cnnnet_new = appendNetworkLayer( cnnnet_new, 'fullyconnected', displayOpts, 'activationType', 'relu', 'sizeHid', sizeHid, 'initializeWeightMethod', 'Random');
    cnnnet_new{end,1}.filtersWhitened = W_2Layer{end,1}{1}';
    cnnnet_new{end,1}.biasesWhitened = zeros(sizeHid,1);
    
    cnnnet_new = appendNetworkLayer( cnnnet_new, 'activation', displayOpts);
    cnnnet_new = appendNetworkLayer( cnnnet_new, 'softmax', displayOpts, 'initializeWeightMethod', 'Random', 'minFuncMaxIter', 0 ,'useAccuracyAsCost', false);
    cnnnet_new{end,1}.Weight = W_2Layer{end,1}{2}';
    cnnnet_new{end,1}.BiasParams = zeros(size(cnnnet_new{end,1}.Weight,1),1);
    
    cnnnet_new{end-2,1}.fwdPropAllBatches = false;
    cnnnet_new{end-1,1}.fwdPropAllBatches = false;
    cnnnet_new{end,1}.fwdPropAllBatches = false;
    
    [cnnnet_appended, cnnnetResults_appended, ~, pred_labels_appended, data_LL_appended] = deepNet_Test( dS, cnnnet_new, displayOpts, saveOpts, 'train', '',2);
% %     [remainingFeatIDs_2, resultSummary_2] = findIrrelevantFeatures(data_LL_appended', labels_LL_Train, 'ELM_LDA');
% %     cnnnet_appended{end-2,1}.filtersWhitened = cnnnet_appended{end-2,1}.filtersWhitened(remainingFeatIDs_2,:);
% %     cnnnet_appended{end-2,1}.biasesWhitened = cnnnet_appended{end-2,1}.biasesWhitened(remainingFeatIDs_2);
% %     cnnnet_appended{end-2,1}.outputSize_original(3) = length(remainingFeatIDs_2);
% %     cnnnet_appended{end-2,1}.outputSize_vectorized = [prod(cnnnet_appended{end-2,1}.outputSize_original(1:3)) cnnnet_appended{end-2,1}.outputSize_original(4)];
% %     cnnnet_appended{end-2,1}.sizeHid = length(remainingFeatIDs_2);
% %     
% %     cnnnet_appended{end-1,1}.inputSize_original = cnnnet_appended{end-2,1}.outputSize_original;
% %     cnnnet_appended{end-1,1}.inputSize_vectorized = cnnnet_appended{end-2,1}.outputSize_vectorized;
% %     cnnnet_appended{end-1,1}.outputSize_original = cnnnet_appended{end-1,1}.inputSize_original;
% %     cnnnet_appended{end-1,1}.outputSize_vectorized = cnnnet_appended{end-1,1}.inputSize_vectorized;
% %     
% %     cnnnet_appended{end,1}.Weight = cnnnet_appended{1,1}.Welm(remainingFeatIDs_2,:)';
% %     
% %     [cnnnet_appended, cnnnetResults_appended, ~, pred_labels_appended, data_LL_appended] = deepNet_Test( dS, cnnnet_appended, displayOpts, saveOpts, 'train', '',2);
    [~, cnnR_02_Valid, ~, pred_labels_valid_02, data_LL_Valid_Appended] = deepNet_Test( dS, cnnnet_appended, displayOpts, saveOpts, 'valid', '',2);
    [~, cnnR_02_Test, ~, pred_labels_test_02, data_LL_Test_Appended] = deepNet_Test( dS, cnnnet_appended, displayOpts, saveOpts, 'test', '',2);

%     if strcmp(appendMode,'greedyCorrect')
%         disp([displayOpts.ies{4} 'Learning ELM weights for the last layer with the accumulated final layer activations.']);
%         [Welm, ~, confMat_ELM, accuracy_ELM, rankDeficient] = getELM_Weights_SoftMax(data_LL_Train', labels_LL_Train);
%         cnnnetResults_Init.rankDeficient_ELM = rankDeficient;
%         cnnnet_initial{1,1}.Welm = Welm;
%         
%         dS_LastLayer = create_dS_frData(data_LL_Train, labels_LL_Train...
%                                        ,'data_valid', data_LL_Valid, 'labels_valid', labels_LL_Valid...
%                                        ,'data_test' , data_LL_Test, 'labels_test', labels_LL_Test);
%         [cnnnet2append, numOfELMLearnt, elmStruct_dropTrue, categoryCount] = createFullyConnectedWeights(dS_LastLayer);
%         error('NOT A CORRECT WAY TO APPEND A CNNET TO ANOTHER');
%         cnnnet_new = {cnnnet_initial(1:end-1);cnnnet2append(1:end)};
%         clear cnnnet2append;
%     end
% %% 1. Test ELM in final layer outputs of original cnnnet
%     [W_TR_ELM, ~, confMat_TR_ELM, accuracy_TR_ELM, rankDeficient_TR_ELM] = getELM_Weights_SoftMax(data_LL_Train', labels_LL_Train);
%     [predLbl_VA_02,~,~,accVA02, confVA02] = analyzeActivationsElm(data_LL_Valid', labels_LL_Valid, W_TR_ELM);
%     clear data_LL_Valid
%     [predLbl_TE_02,~,~,accTE02, confTE02] = analyzeActivationsElm(data_LL_Test', labels_LL_Test, W_TR_ELM);
%     clear data_LL_Test
% %% 2. Test the appended network directly
%     %appended fully connected layer test for train/valid/test
%     [cnnnet_appended, cnnnetResults_appended, ~, pred_labels_appended, data_LL_appended] = deepNet_Test( dS, cnnnet_new, displayOpts, saveOpts, 'train', '',2);
%     [~, cnnR_02_Valid, ~, pred_labels_valid_02, data_LL_Valid_Appended] = deepNet_Test( dS, cnnnet_new, displayOpts, saveOpts, 'valid', '',2);
%     [~, cnnR_02_Test, ~, pred_labels_test_02, data_LL_Test_Appended] = deepNet_Test( dS, cnnnet_new, displayOpts, saveOpts, 'test', '',2);
%     
%     
% %% 3. Test ELM in final layer outputs of appended cnnnet
%     data_LL_appended(data_LL_appended>0) = 1;
%     data_LL_appended(data_LL_appended<=0) = -1;
%     [W_TR_appended_ELM, ~, confMat_TR_appended_ELM, accuracy_TR_appended_ELM, rankDeficient_appended_ELM] = getELM_Weights_SoftMax(data_LL_appended', labels_LL_Train);
%     [predLbl_VA_03,~,~,accVA03, confVA03] = analyzeActivationsElm(data_LL_Valid_Appended', labels_LL_Valid, W_TR_appended_ELM);
%     clear data_LL_Valid_Appended
%     [predLbl_TE_03,~,~,accTE03, confTE03] = analyzeActivationsElm(data_LL_Test_Appended', labels_LL_Test, W_TR_appended_ELM);
%     clear data_LL_Test_Appended
% %% 4. Apply ELM weights learnt to cnnet_appended and check
%     cnnnet_appended_ELM = cnnnet_appended;
%     cnnnet_appended_ELM{end}.Weight = cnnnet_appended{1,1}.Welm';    
%     [cnnnet_appended_ELM, cnnnetResults_appended_ELM, ~, pred_labels_appended_ELM] = deepNet_Test( dS, cnnnet_appended_ELM, displayOpts, saveOpts, 'train', '',3);
%     [~, cnnR_03_Valid, ~, pred_labels_valid_03] = deepNet_Test( dS, cnnnet_appended_ELM, displayOpts, saveOpts, 'valid', '',3);
%     [~, cnnR_03_Test, ~, pred_labels_test_03] = deepNet_Test( dS, cnnnet_appended_ELM, displayOpts, saveOpts, 'test', '',3);
end

function cnnnet = setVarianceBias_ConvFalse(cnnnet)
    layerCount = size(cnnnet,1);
    for layer = 1:layerCount
        curCell = cnnnet{layer,1};
        switch curCell.type
            case 'convolution'
                %if type is batch and curStepType is batch then set to true else not
                %{'none','initial','epoch','batch','iteration'}
                curCell.applyVarNorm = false;
                curCell.applyBiasNorm = false;
            case 'activation'
            case 'pooling'
            case 'softmax'
            case 'fullyconnected'
            otherwise
        end
        cnnnet{layer,1} = curCell;
    end    
end

function [data_LL,labels_LL, a] = appendLastLayerData(cnnnet, data_LL, labels_LL,displayOpts, saveOpts, labels)
    memoryMethod = 'write_to_disk';
    activationID = 1;
    idSelected = 0;
    layerCount = size(cnnnet,1);
    for i = 1:layerCount
        switch cnnnet{i,1}.type
            case {'convolution','softmax','fullyconnected'}
            case {'activation','pooling'}
                activationID = activationID + 1;
        end
        if isfield(cnnnet{i,1},'ActivationLayerID') && cnnnet{i, 1}.LastActivationLayer
            assert(activationID == cnnnet{i,1}.ActivationLayerID);
            idSelected = activationID;
        end
        if isfield(cnnnet{i,1},'activatedData')
            memoryMethod = 'all_in_memory';
        end        
    end
    if idSelected~=0 && strcmp(memoryMethod,'all_in_memory')
        for i = 1:layerCount
            if isfield(cnnnet{i,1},'activatedData')
                if i==idSelected
                    a = cnnnet{idSelected,1}.activatedData;  
                end
                cnnnet{i,1}.activatedData = [];
            end
        end
    end

    if strcmp(memoryMethod,'write_to_disk')
        layerID = size(cnnnet,1)-1;
        a = loadActivation(saveOpts, activationID, layerID, displayOpts);
    end            
    data_LL = [data_LL , a];
    labels_LL = [labels_LL ; labels(:)];
end

% % sizeHid = numOfELMLearnt*categoryCount;
% % cnnnet_new = cnnnet_initial(1:end-1);
% % cnnnet_new = appendNetworkLayer( cnnnet_new, 'fullyconnected', displayOpts, 'activationType', 'relu', 'sizeHid', sizeHid, 'initializeWeightMethod', 'Random');
% % bias_full = zeros(sizeHid,1);
% % to = 0;
% % sizeIn = cnnnet_new{end-1}.outputSize_vectorized(1);
% % W_full = zeros(sizeHid,sizeIn);
% % W_soft = zeros(categoryCount,sizeHid);
% % W_soft_b = -1*ones(categoryCount);
% % W_soft_b(eye(categoryCount)==1) = 1;
% % for b=1:numOfELMLearnt
% %     fr = to+1;
% %     to = fr+categoryCount-1;
% %     Welm_b = elmStruct_dropTrue(b).Welm_b;
% %     W_full(fr:to,:) = Welm_b';
% %     W_soft(:,fr:to) = W_soft_b;
% % end    
% % cnnnet_new{end}.filtersWhitened = W_full;
% % cnnnet_new{end}.biasesWhitened = bias_full;
% % cnnnet_new{end}.fwdPropAllBatches = false;
% % 
% % cnnnet_new = appendNetworkLayer( cnnnet_new, 'activation', displayOpts);
% % cnnnet_new{end}.fwdPropAllBatches = false;
% % 
% % cnnnet_new = appendNetworkLayer( cnnnet_new, 'softmax', displayOpts, 'initializeWeightMethod', 'Random', 'minFuncMaxIter', 400 ,'useAccuracyAsCost', false);
% % cnnnet_new{end}.Weight = W_soft;%cnnnet_appended{1,1}.Welm';
% % cnnnet_new{end}.BiasParams = zeros(categoryCount,1);