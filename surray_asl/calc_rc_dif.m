function [r_dif, c_dif, ind_dif] = calc_rc_dif(patchSize, calcMode, imageSizeCol)
%calc_rc_dif calculate the difference for moving
%leftTopMostPixel row and col IDs 
%to
%centerMostPixel row and col IDs
%where, both gives the same patch when applied as crop area
%   the related patch can have its edge lengths even or odd
%   the calculation changes if it is even or odd

%    calcMode = 'closeToBegin';%'closeToBegin','closeToEnd'
%calculate row addition
%psr  -->Patch Size Row
%ra   -->Row Add
%psra -->Patch Size Row Add
    psr = patchSize(1);
    if (mod(psr,2)==0)
        %even row count
        switch calcMode
            case 'closeToBegin'
                ra = 1;
            case 'closeToEnd'
                ra = 0;
        end
        psra = 0;
    else
        %odd row count
        ra = 0;
        psra = 1;
    end
%calculate col addition
%psc  -->Patch Size Col
%ca   -->Cow Add
%psca -->Patch Size Col Add
    psc = patchSize(2);
    if (mod(psc,2)==0)
        %even row count
        switch calcMode
            case 'closeToBegin'
                ca = 1;
            case 'closeToEnd'
                ca = 0;
        end
        psca = 0;
    else
        %odd col count
        ca = 0;
        psca = 1;
    end

    r_dif = (psr-psra)/2 - ra;
    c_dif = (psc-psca)/2 - ca;
    
%isc  -->Image Size Col
    if nargout==3 && nargin==3
        isc = imageSizeCol;
        ind_dif = r_dif*isc + c_dif;
    end
end