function createBatches(mainFolder, dataInfoStruct, samplesInfo_RGBD, batchInfo, imageRolColSize)

    imageCountMatrix = reshape([dataInfoStruct.Image_Count_RGB],5,24);

    % mainFolder = 'G:\Data\dataset5';
    personFolders = {'A';'B';'C';'D';'E'};
    letters = {'a';'b';'c';'d';'e';'f';'g';'h';'i';'k';'l';'m';'n';'o';'p';'q';'r';'s';'t';'u';'v';'w';'x';'y'};

    for bt = 1:3 %Batch Type - Train(1), Validation(2), Test(3)
        [btStr, batch_Count] = getVals(bt, batchInfo);
        for b=1:batch_Count
            fileNameBatch = [mainFolder '\data_batch_' btStr '_' num2str(b,'%02d') '.mat'];
            if exist(fileNameBatch,'file')
                disp(['***Data for (' fileNameBatch ') is being skipped']);
                continue;
            end
            
            [batch_image_count] = getBIC(bt, batchInfo, b);
            bic=1;
            disp(['**Initializing data for (' fileNameBatch ')']);
            data = rand(imageRolColSize,imageRolColSize,4,batch_image_count);
            labels = zeros(1,batch_image_count);
            samplesInfo_RGBD_batch = zeros(batch_image_count,10);
            % trainData can be in 3 different forms
            % 2 dimensional - [d numberOfSamples] = size(trainData)
            % 3 dimensional - [imageDim imageDim numberOfSamples] = size(trainData);
            % 4 dimensional - [imageDim imageDim channelSize numberOfSamples] = size(trainData)
    
            %we need to create the batch file which consists of
            %trainData - 
            %trainLabels - 
            %additional information can also be added like
            %each samples information from dataInfo which is N by 10
            
            for p=1:size(personFolders,1)
                for l = 1:size(letters,1)
                    imageCountCur = imageCountMatrix(p,l);
                    disp([personFolders{p,1} '-' letters{l,1} ', image count = ' num2str(imageCountCur)]);
                    matFileName = [mainFolder '\'  num2str(p,'%02d') '_'   num2str(l,'%02d')  '.mat'];
                    tic();
                        disp(['Loading ' matFileName]);
                        load(matFileName, 'images_RGBD');
                    toc();
                    
                    %the samples that fall into the Batch Type and Batch ID
                    slct = getSelections(batchInfo.batchDistributions, bt, p, l, b);
                    
                    %images_RGBD is [imageRolColSize imageRolColSize 4 imageCountCur]
                    slctImageCount = sum(slct==1);
                    data(:,:,:,bic:bic+slctImageCount-1) = images_RGBD(:,:,:,slct==1);
                    labels(:,bic:bic+slctImageCount-1) = l;
                    samplesInfo_slct = samplesInfo_RGBD((samplesInfo_RGBD(:,1)==p & samplesInfo_RGBD(:,2)==l),:);
                    samplesInfo_RGBD_batch(bic:bic+slctImageCount-1,:) = samplesInfo_slct(slct,:);
                    bic = bic+slctImageCount;
                end
            end
            disp(['Data for (' fileNameBatch ') is being saved']);
            a = whos('data');
            uncompressedSize = a.bytes/(1024*1024);
            save(fileNameBatch,'data','labels','samplesInfo_RGBD_batch','uncompressedSize','-v7.3');
            a = dir(fileNameBatch);
            compressedSize = a.bytes/(1024*1024);
            disp(['Data of size ' num2str(uncompressedSize, '%6.3f') 'MB is compressed into ' num2str(compressedSize, '%6.3f') 'MB']);
            disp(['**Data for (' fileNameBatch ') is saved']);
            clear a compressedSize uncompressedSize
        end %b=1:batch_Count
    end
end

function [btStr, batch_Count] = getVals(bt,batchInfo)
    switch bt
        case 1
            btStr = 'train';
            batch_Count = batchInfo.batch_Count_Train;
        case 2
            btStr = 'valid';
            batch_Count = batchInfo.batch_Count_Valid;
        case 3
            btStr = 'test';
            batch_Count = batchInfo.batch_Count_Test;
    end
end

function [batch_image_count] = getBIC(bt,batchInfo,b)
    switch bt
        case 1
            batch_image_count = sum(batchInfo.image_Count_Per_Batch_Train(b,:));
        case 2
            batch_image_count = sum(batchInfo.image_Count_Per_Batch_Valid(b,:));
        case 3
            batch_image_count = sum(batchInfo.image_Count_Per_Batch_Test(b,:));
    end
end

function [slct] = getSelections(batchDistributions, bt, p, l, b)
    bid = batchDistributions(p,l).bid;
    switch bt
        case 1
            slct = batchDistributions(p,l).TR==1 & bid==b;
        case 2
            slct = batchDistributions(p,l).VA==1 & bid==b;
        case 3
            slct = batchDistributions(p,l).TE==1 & bid==b;
    end  
end