function [dS, batchInfo] = createInitialDataStruct_surray(dataSetMode)
%createInitialDataStruct creates the dataStruct variable 

%dataSetMode -> can be either
%raw (data_batch_train_01)
%focus (data_batch_train_01_focus)
%resized[80 80] (data_batch_train_01_resized[80 80]) - think about this-> maybe rowColSize param can be passed

% dataStruct  dS
%
% 1.params that do not change
% 	a. countCategory -> total number of class IDs that the whole data has.
%   b. countBatch.{train,validation,test} -> it is a struct itself too
% 	c. batchInfo -> a matrix that has the information about the data to be loaded.
% 					IS IT MEANINGFUL TO HAVE .SAVE AND .LOAD SEPERATELY - SUCH THAT 
% 					cnnnet{layer-1}.dataStruct.batchInfo.save == cnnnet{layer}.dataStruct.batchInfo.load
% 					or maybe here we will have a fileName as a function
% 					data_batch_<type>_<XX>.mat
%   d. getFileName -> an inline function to get fileName to load
% 	e. singleImageShape -> [R,C,F]
%   f. imageShapeIdentifier-> 'square' or 'rectangle' - only square will be allowed initially
% 	g. countFeat -> prod(R,C,F)
%
% 2.params that will change accordingly
% 	a. type -> train, validation, test
%   b. data_act -> 'data' or 'act'
% 	c. loadFolder -> the exact folder that the train/valid/test data/act is in.
%
% 3.params that change per batch
%   a. batchID_current
%   b. fileName_current
% 	c. countSamples -> total number of samples in current batch
% 	d. numOfBatches -> how many batches of this data type exist
% 	e. curBatchID -> initially it will be 1, then it will increment until numOfBatches
% 	f. labelFunc -> an ?inline? function that returns labes of the so called batch.

    global bigDataPath;

% 1.params that do not change
    dS.countCategory = 24;%24 letters%
        load([bigDataPath 'dataset5\batchInfo.mat']);
    dS.batchInfo = batchInfo;
    dS.countBatch.train = batchInfo.batch_Count_Train;
    dS.countBatch.valid = batchInfo.batch_Count_Valid;
    dS.countBatch.test = batchInfo.batch_Count_Test;
    dS.countBatch.act = [];%the number of batches in an activation batch
    
    dS.dataSetMode = dataSetMode;

    switch dataSetMode
        case 'raw'
            %e.g. data_batch_train_01.mat
            dS.fileNameStruct = '<data-act>_batch_<train-valid-test>_<batchID>-<batchCount>.mat';
            dS.getFileName = @(data_act_STR, type_STR, batchID_INT, batchCnt_INT) [data_act_STR '_batch_' type_STR '_' num2str(batchID_INT,'%02d') '-' num2str(batchCnt_INT,'%02d') '.mat'];
            dS.getFileName_Current = @(dS_) [dS_.loadFolder dS_.data_act '_batch_' dS_.type '_' num2str(dS_.batchID_current,'%02d') '-' num2str(dS_.batchCnt_current,'%02d') '.mat'];
        otherwise
            %e.g. data_batch_train_01_focus.mat, data_batch_train_01_resized[80 80].mat
            dS.fileNameStruct = ['<data-act>_batch_<train-valid-test>_<batchID>-<batchCount>_' dataSetMode '.mat'];
            dS.getFileName = @(data_act_STR, type_STR, batchID_INT, dataSetMode_STR) [data_act_STR '_batch_' type_STR '_' num2str(batchID_INT,'%02d') '-' num2str(batchCnt_INT,'%02d') '_' dataSetMode_STR '.mat'];
            dS.getFileName_Current = @(dS_) [dS_.loadFolder dS_.data_act '_batch_' dS_.type '_' num2str(dS_.batchID_current,'%02d') '-' num2str(dS_.batchCnt_current,'%02d') '_' dS_.dataSetMode '.mat'];            
    end
    
    dS.updateDataStruct = @(dS_, varargin_) updateDataStruct(dS_, varargin_{:});

    dS.imageShapeIdentifier = 'square';
    dS.getLabelVec = @(samplesInfo_RGBD_batch_) samplesInfo_RGBD_batch_(:,2)';
    dS.getLabelArr = @(samplesInfo_RGBD_batch_) full(sparse(samplesInfo_RGBD_batch_(:,2), 1:size(samplesInfo_RGBD_batch_,1), 1));

% 2.params that will change accordingly
    dS.loadFolder = [bigDataPath 'dataset5\'];%will become the temporary folder for activations
    dS.type = 'train';
    dS.data_act = 'data';

% 3.params that change per batch
    dS.batchID_current = 1;
    dS.batchCnt_current = dS.countBatch.train;
    
% Extract info according to what is given above
    dS.fileName_current = dS.getFileName_Current(dS);
        load(dS.fileName_current);
        data_size_vec  = size(data);
    dS.singleImageShape = data_size_vec(1:3);
    dS.countSamples = data_size_vec(4);
        clear data_size_vec;
    dS.countFeat = prod(dS.singleImageShape);
    
% % example extraction of labels
%     load(dS.fileName_current);
%     labelVec = dS.getLabelVec(samplesInfo_RGBD_batch);
%     labelArr = dS.getLabelArr(samplesInfo_RGBD_batch);
%
% Also I need a function named 'updateDataStruct' to update the structure
% variables

end

