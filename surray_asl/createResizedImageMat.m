function createResizedImageMat( newRowSize, newColSize, figureID, dataTypeStr)
%createResizedImageMat Summary of this function goes here
%   Detailed explanation goes here
    if ~exist('figureID','var')
        figureID = [];
    end
    if ~exist('dataTypeStr','var') || isempty(dataTypeStr)
        dataTypeStr = 'train';%'train','valid','test'
    end
    switch dataTypeStr
        case 'train'
            batchCount = 17;
        case 'valid'
            batchCount = 3;
        case 'test'
            batchCount = 5;
    end            
    global bigDataPath;
    for batchID = 1:batchCount
        try
            resizedFileName = strrep(['data_batch_' dataTypeStr '_XX_resized[' num2str(newRowSize) ' ' num2str(newColSize) '].mat'],'XX',num2str(batchID,'%02d'));
            folderName = [bigDataPath 'dataset5'];
            resizedFileNameFull = [folderName '\' resizedFileName];
            if ~exist(resizedFileNameFull,'file')
                %1. Load batchData
                [dataIdentifier, countSamples] = loadBatchData(batchID, dataTypeStr);
                data = dataIdentifier.data;
                
                resizedData = zeros(newRowSize,newColSize,3,countSamples);
                drawFigBool = ones(5,24);
                for i = 1:countSamples
                    curIm = data(:,:,1:3,i);
                    x = dataIdentifier.samplesInfo_RGBD_batch(i,:);
                    curPerson = x(1);
                    curLabel = x(2);
                    curImCropped = curIm(x(7):x(8),x(9):x(10),:);
                    curImResized = imresize(curImCropped,[newRowSize newColSize]);
                    if exist('figureID','var') && ~isempty(figureID) && drawFigBool(curPerson,curLabel)==1
                        figure(figureID);clf;hold on;
                        subplot(1,3,1);
                        imshow(curIm);title(['Original Image-Person(' num2str(curPerson) '), Letter(' num2str(curLabel) ')']);
                        subplot(1,3,2);
                        imshow(curImCropped);title('Cropped Image');
                        subplot(1,3,3);
                        imshow(curImResized);title('Resized Cropped Image');
                        drawFigBool(curPerson,curLabel)=0;
                    end
                    resizedData(:,:,:,i) = curImResized;
                end
                samplesInfo_RGB = dataIdentifier.samplesInfo_RGBD_batch(:,1:6);
                data = resizedData;
                labels = samplesInfo_RGB(:,2);
                save(resizedFileNameFull,'data','labels','samplesInfo_RGB');
            end
        catch
            disp('error here')
        end
    end


end

