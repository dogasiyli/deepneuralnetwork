function displayAllData( data, labels )
    imCount = size(data,4);
    for i=1:imCount
        figure(2);
        imcur = squeeze(data(:,:,:,i));
        imshow(imcur);
        title(['imageID(' num2str(i) '),imsum(' num2str(sum(imcur(:))) '),label(' num2str(labels(i)) ')'])
    end
end

