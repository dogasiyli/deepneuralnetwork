function [ h, patchInfo_Cur] = displayImagePatchActivations( imageID, data, imageIDs, patchInfo_RGBD, patchSize, labelActivations)
    curImage = data(:,:,:,imageID);
    patchRowsToDisplay = find(imageIDs==imageID);
    patchInfo_Cur = patchInfo_RGBD(patchRowsToDisplay,:);
    h = displaySingleImagePatchActivations( curImage, patchRowsToDisplay, patchSize, patchInfo_Cur, labelActivations, 2);
end

