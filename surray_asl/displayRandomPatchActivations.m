function displayRandomPatchActivations( data, patchSize,  patchLabels, imageIDs, patchInfo_RGBD, labelActivations, categories )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

   for c = categories%1:24
        selectableRowInds = find(patchLabels==c);
        if ~isempty(selectableRowInds)
            randomInd = randi(length(selectableRowInds),1);
            selectedRow = selectableRowInds(randomInd);
            imageID = imageIDs(selectedRow);
            h = displayImagePatchActivations( imageID, data, imageIDs, patchInfo_RGBD, patchSize, labelActivations);
        end
   end
end

