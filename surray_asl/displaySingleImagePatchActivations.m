function [ h ] = displaySingleImagePatchActivations( curImage, patchRowsToDisplay, patchSize, patchInfo_Cur, labelActivations, figureID)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    h = figure(figureID);clf;
    SPRC = 2;
    SPCC = 5;
    
    mostRecognizedArea = zeros(size(curImage,1));
    imageRect = {patchInfo_Cur(1,6):patchInfo_Cur(1,7);patchInfo_Cur(1,8):patchInfo_Cur(1,9)};

    patchCount = length(patchRowsToDisplay);
    activationsOfPatches_un = labelActivations.given_un(patchRowsToDisplay);
    activationsOfPatches_un(activationsOfPatches_un<0) = 0;
    activationsOfPatches_un_pr = labelActivations.predicted_un(patchRowsToDisplay);
        
    personID = patchInfo_Cur(1,1);
    letterID = patchInfo_Cur(1,2);
    imageSeqID = patchInfo_Cur(1,3);
    
    %% RGB Data SPRC,SPCC,[1 2]
    subplot(SPRC,SPCC,[1 2]);
    rgbIm = curImage(:,:,1:3);
    imshow(rgbIm(imageRect{1},imageRect{2},1:3));hold on;
    if (max(activationsOfPatches_un)==0)
        mostRecognizedArea = zeros(size(mostRecognizedArea));
    else
        colorIntensities = activationsOfPatches_un./max(activationsOfPatches_un);
        for i=1:patchCount
            colArr = ones(1,3)*colorIntensities(i);
            lineWidth = 2+round(3*colorIntensities(i));
            curRCAdd = patchInfo_Cur(i,4:5)-patchInfo_Cur(1,[6 8]);
            p_lt = [    0                0      ] + curRCAdd;
            p_rt = [    0          patchSize(2) ] + curRCAdd;
            p_rb = [ patchSize(1)  patchSize(2) ] + curRCAdd;
            p_lb = [ patchSize(1)        0      ] + curRCAdd;
            plot([p_lt(2),p_rt(2)],[p_lt(1),p_rt(1)],'Color',colArr,'LineWidth',1);
            plot([p_rt(2),p_rb(2)],[p_rt(1),p_rb(1)],'Color',colArr,'LineWidth',1);
            plot([p_rb(2),p_lb(2)],[p_rb(1),p_lb(1)],'Color',colArr,'LineWidth',1);
            plot([p_lb(2),p_lt(2)],[p_lb(1),p_lt(1)],'Color',colArr,'LineWidth',lineWidth);

            rectR = patchInfo_Cur(i,4):(patchInfo_Cur(i,4)+patchSize(1));
            rectC = patchInfo_Cur(i,5):(patchInfo_Cur(i,4)+patchSize(2));
            mostRecognizedArea(rectR,rectC) = mostRecognizedArea(rectR,rectC) + activationsOfPatches_un(i);
        end
    end
    rgbIm_RowSize = patchInfo_Cur(1,7)-patchInfo_Cur(1,6)+1;
    rgbIm_ColSize = patchInfo_Cur(1,9)-patchInfo_Cur(1,8)+1;
    titleCells = cell(2,1);
    titleCells{1,1} = ['Person(' num2str(personID) '),Letter(' num2str(letterID) '),ImageSeqID(' num2str(imageSeqID) ')'];
    titleCells{2,1} = ['Row(' num2str(rgbIm_RowSize) '), Col(' num2str(rgbIm_ColSize) ')'];
    title(titleCells);
    xlabel({['patchSize' mat2str(patchSize)],['patchCount' mat2str(patchCount)]});
    %xlabel(xlabelCells);
    
    %% Depth Data SPRC,SPCC,[3 4]
    subplot(SPRC,SPCC,[4 5]);
    depthIm = curImage(imageRect{1},imageRect{2},4);
    imagesc(depthIm);hold on;set(gca,'XTick',[]);set(gca,'YTick',[]);
    if (max(activationsOfPatches_un)==0)
        mostRecognizedArea = zeros(size(mostRecognizedArea));
        bestPatchID = 1;
        titleStr = {'All activations=0'};
        xlabelstr = 'All patches labelled incorrectly';
    else
        colorIntensities = activationsOfPatches_un./max(activationsOfPatches_un);
        colorIntensities(colorIntensities<0) = 0;
        [~,bestPatchID] = max(colorIntensities);
        colArr = ones(1,3)*colorIntensities(bestPatchID);
        lineWidth = 1+round(3*colorIntensities(bestPatchID));
        curRCAdd = patchInfo_Cur(bestPatchID,4:5) - patchInfo_Cur(1,[6 8]);
        p_lt = [    0                0      ] + curRCAdd;
        p_rt = [    0          patchSize(2) ] + curRCAdd;
        p_rb = [ patchSize(1)  patchSize(2) ] + curRCAdd;
        p_lb = [ patchSize(1)        0      ] + curRCAdd;
        plot([p_lt(2),p_rt(2)],[p_lt(1),p_rt(1)],'Color',colArr,'LineWidth',1);
        plot([p_rt(2),p_rb(2)],[p_rt(1),p_rb(1)],'Color',colArr,'LineWidth',1);
        plot([p_rb(2),p_lb(2)],[p_rb(1),p_lb(1)],'Color',colArr,'LineWidth',lineWidth);
        plot([p_lb(2),p_lt(2)],[p_lb(1),p_lt(1)],'Color',colArr,'LineWidth',1);
        titleStr = {['bestPatchID(' num2str(bestPatchID) ')'],['Unnormalized Activation(' num2str(activationsOfPatches_un(bestPatchID),'%6.4f') ')']};
        correctPatchesArr = activationsOfPatches_un_pr<=activationsOfPatches_un;
        wrongPatchesArr = activationsOfPatches_un_pr>activationsOfPatches_un;
        xlabelstr = {['Wrong patch count = ' num2str(sum(wrongPatchesArr))],['Correct patch count = ' num2str(sum(correctPatchesArr))]};
    end
    colorbar;
    title(titleStr);
    xlabel(xlabelstr);
    
%     %% Depth Best Patch SPRC,SPCC,5
%     subplot(SPRC,SPCC,5);
%     bestPatchIm = depthIm(p_lt(1):p_lb(1),p_lt(2):p_rt(2));
%     imagesc(bestPatchIm);
%     title('Best Recognized Depth Patch');
%     colorbar;
    
    %% Most Activated Area Map SPRC,SPCC,6
    subplot(SPRC,SPCC,3);
    imagesc(mostRecognizedArea(imageRect{1},imageRect{2}));set(gca,'XTick',[]);set(gca,'YTick',[]);
    xlabel({'Most Activated','Area Map'});
    colorbar;
    
    %% RGB Most Activated Area Patch SPRC,SPCC,9
    subplot(SPRC,SPCC,9);
    maxSum = 0;
    mostActivated.rectR = [];
    mostActivated.rectC = [];
    for r = reshape(unique(patchInfo_Cur(:,4)),1,[])
        for c = reshape(unique(patchInfo_Cur(:,5)),1,[])
            rectR = r:(r+patchSize(1)-1);
            rectC = c:(c+patchSize(2)-1);
            curPatch = mostRecognizedArea(rectR,rectC);
            curSum = sum(curPatch(:));
            if curSum>maxSum
                maxSum = curSum;
                mostActivated.rectR = rectR;
                mostActivated.rectC = rectC;
            end
        end
    end
    rgbIm_MostActivated = curImage(mostActivated.rectR,mostActivated.rectC,1:3);
    imshow(rgbIm_MostActivated);
    title({'Most Activated','RGB Area','Patch'});
    xlabel('RGB Area Patch');
    
    %% RGB Most Activated Centered
    subplot(SPRC,SPCC,10);
    xlabelCells = cell(1,1);
    xlc = 1;
    xlabelCells{xlc,1} = 'RGB Area Patch';
    
    [rs,cs] = find(mostRecognizedArea==max(mostRecognizedArea(:)));
    rs = round(mean(rs(:)));
    cs = round(mean(cs(:)));
    rectR = (rs-(patchSize(1)/2)):(rs+(patchSize(1)/2));
    rowPush = max(0,patchInfo_Cur(1,6)-min(rectR(:)));
    if (rowPush>0)
        rectR = rectR+rowPush;
        xlc = xlc + 1;
        xlabelCells{xlc,1} = ['Row pushed ' num2str(rowPush) ' pixels'];
    end
    rowPull = max(0,max(rectR(:))-patchInfo_Cur(1,7));
    if (rowPull>0)
        rectR = rectR-rowPull;
        xlc = xlc + 1;
        xlabelCells{xlc,1} = ['Row pulled ' num2str(rowPull) ' pixels'];
    end
    rectC = (cs-(patchSize(2)/2)):(cs+(patchSize(2)/2));
    colPush = max(0,patchInfo_Cur(1,8)-min(rectC(:)));
    if (colPush>0)
        rectC = rectC+colPush;
        xlc = xlc + 1;
        xlabelCells{xlc,1} = ['Col pushed ' num2str(colPush) ' pixels'];
    end
    colPull = max(0,max(rectC(:))-patchInfo_Cur(1,9));
    if (colPull>0)
        rectC = rectC-colPull;
        xlc = xlc + 1;
        xlabelCells{xlc,1} = ['Col pulled ' num2str(colPull) ' pixels'];
    end
    rgbIm_MostActivatedCentered = curImage(rectR,rectC,1:3);
    imshow(rgbIm_MostActivatedCentered);
    title({'Activation','Centered'});
    xlabel(xlabelCells);
    
    %% RGB Best Patch SPRC,SPCC,[6 7]
    subplot(SPRC,SPCC,[6 7]);
    mostRecognizedArea_RGB = mostRecognizedArea./max(mostRecognizedArea(:));
    mostRecognizedArea_RGB = repmat(mostRecognizedArea_RGB,[1 1 3]);
    mostRecognizedArea_RGB = rgbIm.*mostRecognizedArea_RGB;
    imshow(mostRecognizedArea_RGB(imageRect{1},imageRect{2},:));
    xlabel('Most Activated RGB Area');

    %% RGB Best Patch SPRC,SPCC,8
    subplot(SPRC,SPCC,8);
    bestPatchIm = rgbIm(patchInfo_Cur(bestPatchID,4):(patchInfo_Cur(bestPatchID,4) + patchSize(1)),patchInfo_Cur(bestPatchID,5):(patchInfo_Cur(bestPatchID,5) + patchSize(2)),:);
    imshow(bestPatchIm);
    titleStr = {'Best Recognized Patch',['ID(' num2str(bestPatchID) ')']};
    title(titleStr);    
    xlabel({'Unnormed Activation',['(' num2str(activationsOfPatches_un(bestPatchID),'%4.2f') ')']});
end

