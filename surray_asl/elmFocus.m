function outputStruct = elmFocus( patchSize, methodID, opttheta)
%elmFocus learn a set of convolution weights in a discriminitive manner
%then focus on the most activated featureMaps
%   We will learn some weights that can discriminate the data well
%   Then we will apply these convolution weights and check out the maximally
%   activated areas per label

% in runLearnPatches.m we know how to take patches from within a batch
% what we want to do now is to run either ELM and take the most
% discriminitive patches from each single image, and learn features from
% those discriminitive patches.
% the question is - if we can already identify an image from such a single
% patch which is inside the whole big image - what is the deal with trying
% to add such strong learnt weights as a feature map


    global bigDataPath;
    inputChannelSize = 4;
    if (~exist('opttheta','var') || isempty(opttheta))
        opttheta = [];
    end
    initialPixelIterations = 1:2:(patchSize-1);
    
    batchCount = 17;

    additionalInfo_Cell = cell(length(initialPixelIterations),batchCount);
    Welm_Cell = cell(length(initialPixelIterations),batchCount,3);
    W_randoms_with_bias_Cell = cell(length(initialPixelIterations),batchCount,3);
    accuracyCalced_Cell = cell(length(initialPixelIterations),batchCount,3);
    confMatResult_Cell = cell(length(initialPixelIterations),batchCount,3);
    recognizedPatchCounts_Cell = cell(length(initialPixelIterations),batchCount,3);
    
    sizeHid = 400;
    grabbed_patchMat = [];
    grabbed_patchLabels = [];
    for i = 1:length(initialPixelIterations)%initialPixel
        for batchID = 1:batchCount
            ip = initialPixelIterations(i);
            try
                batchFileName = [bigDataPath 'dataset5\data_batch_train_' num2str(batchID,'%02d') '.mat'];
                disp(['Loading file(' batchFileName ')'])
                load(batchFileName, 'data', 'labels', 'samplesInfo_RGBD_batch');%, 'uncompressedSize'
                dataIdentifier.data = data;
                dataIdentifier.labels = labels;
                dataIdentifier.samplesInfo_RGBD_batch = samplesInfo_RGBD_batch;
                clear batchFileName labels samplesInfo_RGBD_batch;
            catch
                disp(['There is a problem loading <' dataIdentifier '>. Check it out'])
                pause
            end
            [ patchMat, patchLabels, additionalInfo] = turnBatchIntoPatches( dataIdentifier, patchSize, methodID, ip);
            clear dataIdentifier;
            imageIDs = additionalInfo.imageIDs;
            patchInfo_RGBD  = additionalInfo.patchInfo_RGBD ;
            for elmParamType = 1:3
                switch elmParamType
                    case 1
                        elmParamTypeStr = 'Standard';
                    case 2
                        elmParamTypeStr = 'CIW';
                    case 3
                        elmParamTypeStr = 'Constrained';
                end
                paramsELM = setParams_ELM(elmParamTypeStr);
                
                %here before training all of the patches, we have to
                %discard the patches that can already be discriminated by
                %the previous learnt parameters
                %[predictedLabels, accuracyCalced, confMatResult, ~, ~, ~, labelActivations] = analyzeResults(patchMat, patchLabels, W_randoms_with_bias, Welm, true);
                [patchMat, patchLabels, imageIDs, patchInfo_RGBD, recognizedPatchCounts] = clearRecognizablePatches(patchMat, patchLabels, imageIDs, patchInfo_RGBD, W_randoms_with_bias_Cell, Welm_Cell, initialPixelIterations);

                [accuracyCalced, predictedLabels, W_randoms_with_bias, Welm, A, confMatResult, labelActivations] = ELM_training(patchMat, patchLabels, 'paramsELM', paramsELM, 'sizeHid', sizeHid);
                
                
%                 [theta, thetaOut_matrix] = softMaxTrain( patchMat, patchLabels, 'paramsCategory_MinFunc', 'showInfo_noLog');
%                 [predictedLabels_SoftMax, probabilitiesOut_SoftMax] = softMaxPredict(patchMat, thetaOut_matrix);
                
                grabbed_patch_IDs = find(labelActivations.given==1);%find(predictedLabels'==patchLabels);
                
                
                grabbed_patchMat = [grabbed_patchMat,patchMat(:,grabbed_patch_IDs)];
                grabbed_patchLabels = [grabbed_patchLabels;patchLabels(grabbed_patch_IDs)];
                grabbedPatchCount = length(grabbed_patchLabels);

                %now we have to analyze which patches are truly discriminated
                %with these learn weights.. Then we will both save the weights
                %and the patches for further uses..

                figureID = (i-1)*batchCount + batchID;
                figureTitle = ['Fig(' num2str(figureID,'%02d') '),Acc(' num2str(accuracyCalced,'%4.2f') '),GrabbedPatchCount(' num2str(grabbedPatchCount,'%d') '),ELMType(' elmParamTypeStr '),BatchID(' num2str(batchID,'%02d') '),InitPixel(' num2str(ip,'%02d') ')'];
                
                h = drawConfusionMatWithNumbers( confMatResult, struct('figureID', {1}, 'figureTitle' , figureTitle));
                fileName = [bigDataPath 'figures\' figureTitle '.png'];
                saveas(h, fileName);
                
                for c = 1:24
                    selectableRowInds = find(patchLabels==c);
                    if ~isempty(selectableRowInds)
                        randomInd = randi(length(selectableRowInds),1);
                        selectedRow = selectableRowInds(randomInd);
                        imageID = imageIDs(selectedRow);
                        selectedRow = patchInfo_RGBD(selectedRow,:);
                        curImage = data(:,:,:,imageID);
                        patchRowsToDisplay = find(imageIDs==imageID);
                        h = displaySingleImagePatchActivations( curImage, patchRowsToDisplay, patchSize, patchInfo_RGBD, labelActivations, imageID, 2);
                    end
                end
                
                additionalInfo_Cell{i,batchID} = additionalInfo;
                Welm_Cell{i,batchID, elmParamType} = Welm;
                W_randoms_with_bias_Cell{i,batchID, elmParamType} = W_randoms_with_bias;
                accuracyCalced_Cell{i,batchID, elmParamType} = accuracyCalced;
                confMatResult_Cell{i,batchID, elmParamType} = confMatResult;   
                recognizedPatchCounts_Cell{i,batchID, elmParamType} = recognizedPatchCounts; 
            end
        end
    end

    outputStruct = struct;
    outputStruct.additionalInfo_Cell = additionalInfo_Cell;
    outputStruct.Welm_Cell = Welm_Cell;
    outputStruct.W_randoms_with_bias_Cell = W_randoms_with_bias_Cell;
    outputStruct.accuracyCalced_Cell = accuracyCalced_Cell;
    outputStruct.confMatResult_Cell = confMatResult_Cell;
    
end

function [patchMat, patchLabels, imageIDs, patchInfo_RGBD, recognizedPatchCounts] = clearRecognizablePatches(patchMat, patchLabels, imageIDs, patchInfo_RGBD, W_randoms_with_bias_Cell, Welm_Cell, initialPixelIterations)
    batchCount = 17;
    recognizedPatchCounts = zeros(size(Welm_Cell));
    for i = 1:length(initialPixelIterations)%initialPixel
        for batchID = 1:batchCount
            for elmParamType = 1:3
                Welm = Welm_Cell{i,batchID, elmParamType};
                W_randoms_with_bias = W_randoms_with_bias_Cell{i,batchID, elmParamType};
                if ~isempty(Welm) && ~isempty(W_randoms_with_bias)
                    [predictedLabels, accuracyCalced, confMatResult, ~, ~, ~, labelActivations] = analyzeResults(patchMat, patchLabels, W_randoms_with_bias, Welm, false);
                else
                    break;
                end
                
                %here before training all of the patches, we have to
                %discard the patches that can already be discriminated by
                %the previous learnt parameters
                %
                recognized_patch_IDs = find(labelActivations.given==1);
                patchMat(:,recognized_patch_IDs) = [];
                patchLabels(recognized_patch_IDs) = [];
                imageIDs(recognized_patch_IDs) = [];
                patchInfo_RGBD(recognized_patch_IDs,:) = [];
                recognizedPatchCounts(i,batchID,elmParamType) = length(recognized_patch_IDs);
            end
        end
    end
end