function outputStruct = elmFocus_V02( patchSize, varargin)
%elmFocus learn a set of convolution weights in a discriminitive manner
%then focus on the most activated featureMaps
%   We will learn some weights that can discriminate the data well
%   Then we will apply these convolution weights and check out the maximally
%   activated areas per label

% in runLearnPatches.m we know how to take patches from within a batch
% what we want to do now is to run either ELM and take the most
% discriminitive patches from each single image, and learn features from
% those discriminitive patches.
% the question is - if we can already identify an image from such a single
% patch which is inside the whole big image - what is the deal with trying
% to add such strong learnt weights as a feature map

    rng(10);
    iterationCount = 10;
    [ patchSize, methodID_preprocess,  sizeHid,  patchCountPerSamplePassed,  maxRAMAllowedForDataSize_MB, elmParamTypeStrCells, outputStruct...
  ...,displayOpts,  saveOpts,  paramsMinFunc...
  ...,opttheta...
    ] = getOptionalParams(patchSize, iterationCount, varargin{:});
    %1. For training - 
    %   initially we will grab patches from 1 batch,
    
    elmParamType = 2;
    elmParamTypeStr = elmParamTypeStrCells{1,elmParamType};%'Standard'(1),'CIW'(2),'Constrained'(3)
    paramsELM = setParams_ELM(elmParamTypeStr);
    patchPickMethod=1;
    accArr = zeros(17,iterationCount);
    sizeHidArr = zeros(17,iterationCount);
    allBatchParamsFolderName = getParamsFolderName([], elmParamType, patchSize, patchCountPerSamplePassed, patchPickMethod);
    for batchID = [1:2 9:17]
        paramsFolderName = getParamsFolderName(batchID, elmParamType, patchSize, patchCountPerSamplePassed, patchPickMethod);
        %1. Load batchData
        [dataIdentifier, batchImageCount] = loadBatchData(batchID);
        data = dataIdentifier.data;
        leftTopMostPixStruct = [];
        sizeHid_Cur = sizeHid;
        %2. run for different ELMs
        imageID_ToBeDisplayed = zeros(iterationCount,24,3);
        imageIDResults_AnalyzeMat = zeros(iterationCount*24,12);
        figureFolderNamesCells = cell(1,iterationCount);
        for iter = 1:iterationCount
            %2.1. Grab patches according to 'patchCountPerSample','patchSize','maxRAMAllowedForDataSize_MB'
            %     Most importantly I have to identify the leftTopMost pixels
            %     differently for the initial and following runs
            [patchMat, patchLabels, additionalInfo, leftTopMostPixStruct, patchCountPerSampleFound] = grabPatchesFromBatch(dataIdentifier, batchID, patchSize, patchCountPerSamplePassed, maxRAMAllowedForDataSize_MB, methodID_preprocess, leftTopMostPixStruct);

            %2.2. re-organize the workspace variables
            %clear dataIdentifier;
            imageIDs = additionalInfo.imageIDs;
            patchInfo_RGBD  = additionalInfo.patchInfo_RGBD ;
        
            figureFolderNamesCells{1,iter} = getFigureFolderName(batchID, iter, elmParamType, patchSize, patchCountPerSampleFound, patchPickMethod);
            
            %For ELM training 
            %I have to replicate the given data for training such that;
            %1. Check out the maximum number of patches of the class with the highest number of samples
            %2. To equalize the number of samples in each group, replicate
            %the group members by adding gaussian noise on them and adding them to the training data
            %This can be reported as a new reqularizor for data but this is
            %very similar to what they do for the input data as a whole
            %image
            disp(['Iteration ' num2str(iter) ' of ' num2str(iterationCount)])
            [accuracyCalced, predictedLabels, W_randoms_with_bias, Welm, ~, confMatResult, labelActivations] = ELM_training(patchMat, patchLabels, 'paramsELM', paramsELM, 'sizeHid', sizeHid_Cur);
            accArr(batchID,iter) = accuracyCalced;
            if (iter>1 && sizeHid_Cur<2*sizeHid)
                accDif = accArr(batchID,iter)-accArr(batchID,iter-1);
                while (accDif<2 && sizeHid_Cur<2*sizeHid)
                    %at least 2 point accuracy gain - we want to have
                    %but do not increase the hiddenSize enourmously
                    accDif = accArr(batchID,iter)-accArr(batchID,iter-1);
                    sizeHid_Cur = sizeHid_Cur + 50;
                    disp(['hidden size increased to' num2str(sizeHid_Cur) ' because accuracy dif(' num2str(accDif,'%4.2f') ')<2.']);
                    [accuracyCalced, predictedLabels, W_randoms_with_bias, Welm, ~, confMatResult, labelActivations] = ELM_training(patchMat, patchLabels, 'paramsELM', paramsELM, 'sizeHid', sizeHid_Cur);
                    accArr(batchID,iter) = accuracyCalced;
                end
            end
            sizeHidArr(batchID,iter) = sizeHid_Cur;
            
            [imageIDResults, colTitles] = getImageIDResults( imageIDs, predictedLabels, patchLabels, labelActivations, patchInfo_RGBD);
            correctPatchCount = sum(diag(confMatResult));
            
            [imageIDResults_AnalyzeMat] = writeCSVFiles(allBatchParamsFolderName, batchID, iter, patchSize, accArr, sizeHidArr, imageIDResults, imageIDResults_AnalyzeMat);
            
%           %now we have to analyze which patches are truly discriminated
%           %with these learn weights.. Then we will both save the weights
%           %and the patches for further uses..

             [fileName, figureTitle] = getFigureAndFileName_ConfMat(batchID, iter, elmParamType, accuracyCalced, correctPatchCount, elmParamTypeStr, figureFolderNamesCells{1,iter});
             h = drawConfusionMatWithNumbers( confMatResult, struct('figureID', {1}, 'figureTitle' , figureTitle, 'normalizeRowOrCol', 'row'));
             saveas(h, fileName);

            %1.(  2) Ascending order of realLabelOfThesePatches
            %2.( -4)Descending order of correctPatchCountRatio
            %3.( -8)Descending order of unnormedMaxActivation
            
            for c = 1:24
                categoryResults = imageIDResults(imageIDResults(:,2)==c,:);
                %1. the image with highest 'correctPatchCountRatio(Col-4)' which has the highest unnormedMaxActivation
                imageID_ToBeDisplayed(iter,c,1) = categoryResults(1,1);                
                %2. the image with the highest unnormedMaxActivation
                [unnormedMaxActivation,imageID]= max(categoryResults(:,8));
                imageID_ToBeDisplayed(iter,c,2) = categoryResults(imageID,1);
                %3. the worst image
                imageID_ToBeDisplayed(iter,c,3) = categoryResults(end,1);
            end
            
%             for c = 1:24
%                 %1. the image with highest 'correctPatchCountRatio(Col-4)' which has the highest unnormedMaxActivation
%                 [ h, patchInfo_Cur] = displayImagePatchActivations(imageID_ToBeDisplayed(iter,c,1), data, imageIDs, patchInfo_RGBD, patchSize, labelActivations);
%                 ti = patchInfo_Cur(1,[1 3]);%
%                 fileName = [figureFolderNamesCells{1,iter} 'Train_Batch(' num2str(batchID,'%02d') ')_Letter(' num2str(c,'%02d') ')_M1_Patch' mat2str(patchSize) '_Person(' num2str(ti(1),'%02d') ')_ImageSeq(' num2str(ti(2),'%02d') ').png'];
%                 saveas(h, fileName);
%                 
%                 %2. the image with the highest unnormedMaxActivation
%                 [ h, patchInfo_Cur] = displayImagePatchActivations(imageID_ToBeDisplayed(iter,c,2), data, imageIDs, patchInfo_RGBD, patchSize, labelActivations);
%                 ti = patchInfo_Cur(1,[1 3]);%
%                 fileName = [figureFolderNamesCells{1,iter} 'Train_Batch(' num2str(batchID,'%02d') ')_Letter(' num2str(c,'%02d') ')_M2_Patch' mat2str(patchSize) '_Person(' num2str(ti(1),'%02d') ')_ImageSeq(' num2str(ti(2),'%02d') ').png'];
%                 saveas(h, fileName);
%                 
%                 %3. the worst image
%                 [ h, patchInfo_Cur] = displayImagePatchActivations(imageID_ToBeDisplayed(iter,c,3), data, imageIDs, patchInfo_RGBD, patchSize, labelActivations);
%                 ti = patchInfo_Cur(1,[1 3]);%
%                 fileName = [figureFolderNamesCells{1,iter} 'Train_Batch(' num2str(batchID,'%02d') ')_Letter(' num2str(c,'%02d') ')_M3_Patch' mat2str(patchSize) '_Person(' num2str(ti(1),'%02d') ')_ImageSeq(' num2str(ti(2),'%02d') ').png'];
%                 saveas(h, fileName);
%             end   
            
            for iterSub = 1:iter
                for c = 1:24
                    %1. the image with highest 'correctPatchCountRatio(Col-4)' which has the highest unnormedMaxActivation
                    [ h, patchInfo_Cur] = displayImagePatchActivations(imageID_ToBeDisplayed(iterSub,c,1), data, imageIDs, patchInfo_RGBD, patchSize, labelActivations);
                    ti = patchInfo_Cur(1,[1 3]);%
                    fileName = [figureFolderNamesCells{1,iterSub} 'Train_Batch(' num2str(batchID,'%02d') ')_Letter(' num2str(c,'%02d') ')_M1_Patch' mat2str(patchSize) '_Person(' num2str(ti(1),'%02d') ')_ImageSeq(' num2str(ti(2),'%02d') ')_Iter(' num2str(iter) ').png'];
                    saveas(h, fileName);

                    %2. the image with the highest unnormedMaxActivation
                    [ h, patchInfo_Cur] = displayImagePatchActivations(imageID_ToBeDisplayed(iterSub,c,2), data, imageIDs, patchInfo_RGBD, patchSize, labelActivations);
                    ti = patchInfo_Cur(1,[1 3]);%
                    fileName = [figureFolderNamesCells{1,iterSub} 'Train_Batch(' num2str(batchID,'%02d') ')_Letter(' num2str(c,'%02d') ')_M2_Patch' mat2str(patchSize) '_Person(' num2str(ti(1),'%02d') ')_ImageSeq(' num2str(ti(2),'%02d') ')_Iter(' num2str(iter) ').png'];
                    saveas(h, fileName);

                    %3. the worst image
                    [ h, patchInfo_Cur] = displayImagePatchActivations(imageID_ToBeDisplayed(iterSub,c,3), data, imageIDs, patchInfo_RGBD, patchSize, labelActivations);
                    ti = patchInfo_Cur(1,[1 3]);%
                    fileName = [figureFolderNamesCells{1,iterSub} 'Train_Batch(' num2str(batchID,'%02d') ')_Letter(' num2str(c,'%02d') ')_M3_Patch' mat2str(patchSize) '_Person(' num2str(ti(1),'%02d') ')_ImageSeq(' num2str(ti(2),'%02d') ')_Iter(' num2str(iter) ').png'];
                    saveas(h, fileName);
                end   
            end
            
            
            %displayRandomPatchActivations( data, patchSize,  patchLabels, imageIDs, patchInfo_RGBD, labelActivations, 1:24);

            if (iter <iterationCount)
                %[~, leftTopMostPixStruct] = getMostRecognizedAreaOfAllData( [size(data,1) size(data,2)], patchSize, imageIDs, patchInfo_RGBD, labelActivations, leftTopMostPixStruct, data, patchPickMethod);
                tic();
                disp('Finding leftTopMost pixels...');
                [~, leftTopMostPixStruct.leftTopMostPixIDs, patchInfo_RGBD,labelActivations] = getMostRecognizedAreaOfByConvolution( data, additionalInfo, patchSize, imageIDs, patchInfo_RGBD, leftTopMostPixStruct.leftTopMostPixIDs, patchPickMethod, methodID_preprocess, patchCountPerSampleFound, W_randoms_with_bias, Welm, labelActivations);
                disp('Finding leftTopMost pixels finished');
                toc();
            end
            
            foundParamsFileName = getParamsSaveFileName_ELMV02(iter, accuracyCalced, correctPatchCount, elmParamTypeStr, paramsFolderName);
            save(foundParamsFileName,'additionalInfo','Welm','W_randoms_with_bias','leftTopMostPixStruct','accuracyCalced','confMatResult','correctPatchCount','imageIDResults','sizeHid_Cur')
            %outputStruct.additionalInfo_Cell{batchID, elmParamType} = additionalInfo;
            %outputStruct.Welm_Cell{batchID, elmParamType} = Welm;
            %outputStruct.W_randoms_with_bias_Cell{batchID, elmParamType} = W_randoms_with_bias;
            outputStruct.accuracyCalced_Cell{batchID, elmParamType} = accuracyCalced;
            outputStruct.confMatResult_Cell{batchID, elmParamType} = confMatResult;   
            outputStruct.recognizedPatchCounts_Cell{batchID, elmParamType} = correctPatchCount;
            %outputStruct.leftTopMostPixStruct{batchID, elmParamType} = leftTopMostPixStruct;
        end
    end
end

function [patchMat, patchLabels, additionalInfo, leftTopMostPixStruct, patchCountPerSample] = grabPatchesFromBatch(dataIdentifier, batchID, patchSize, patchCountPerSample, maxRAMAllowedForDataSize_MB, methodID_preprocess, leftTopMostPixStruct)
    global bigDataPath;
    if isempty(patchCountPerSample)
        singlePatchSizeMB = (prod(patchSize)*4*8)/(1024*1024);%mb
        singleImageAllowedMB = maxRAMAllowedForDataSize_MB/batchImageCount;%mb
        patchCountPerSample = round(singleImageAllowedMB/singlePatchSizeMB);
    end
    patchBatchFileName = [bigDataPath 'dataset5\data_batch_train_' num2str(batchID,'%02d') '_patchSize' mat2str(patchSize) '_patchCountPerSample(' num2str(patchCountPerSample) ')_methodID(' num2str(methodID_preprocess) ').mat'];
    if exist(patchBatchFileName,'file') && isempty(leftTopMostPixStruct)
        %This if statement is reached only for the initial iteration for a
        %batch of data
        disp(['Loading(' patchBatchFileName ')-{patchMat,patchLabels,additionalInfo}']);
        load(patchBatchFileName,'patchMat','patchLabels','additionalInfo');
        disp(['Loaded(' patchBatchFileName ')-{patchMat,patchLabels,additionalInfo}']);
        leftTopMostPixStruct.patchLabels = patchLabels;
        leftTopMostPixStruct.imageIDs = additionalInfo.imageIDs;
        leftTopMostPixStruct.leftTopMostPixIDs = additionalInfo.leftTopMostPixIDs;
    else
        %Whenever the leftTopMostPixels do change, this function is run!!
        %We have to optimize this part for convolving with stride=1
        warning('This part may need optimization not to re-run normalization process again and again');
        [ patchMat, patchLabels, additionalInfo] = turnBatchIntoPatches( dataIdentifier, patchSize, 'methodID_preprocess', methodID_preprocess, 'patchCountPerSample', patchCountPerSample, 'leftTopMostPixStruct', leftTopMostPixStruct);
        if isempty(leftTopMostPixStruct)
            leftTopMostPixStruct.patchLabels = patchLabels;
            leftTopMostPixStruct.imageIDs = additionalInfo.imageIDs;
            leftTopMostPixStruct.leftTopMostPixIDs = additionalInfo.leftTopMostPixIDs;
            disp(['Saving(' patchBatchFileName ')-{patchMat,patchLabels,additionalInfo}']);
            save(patchBatchFileName,'patchMat','patchLabels','additionalInfo','-v7.3');
            disp(['Saved(' patchBatchFileName ')-{patchMat,patchLabels,additionalInfo}']);
        else
            leftTopMostPixStruct.leftTopMostPixIDs = additionalInfo.leftTopMostPixIDs;
        end
    end
end

function [patchSize, methodID_preprocess,  sizeHid,  patchCountPerSample,  maxRAMAllowedForDataSize_MB, elmParamTypeStrCells, outputStruct, opttheta, displayOpts,  saveOpts,  paramsMinFunc] = getOptionalParams(patchSize, iterationCount, varargin)
  %% varargin parameters
    %P1D0 -> Passed assed paramaeter = 1, Default value used = 0
    [ opttheta,  patchCountPerSample,  maxRAMAllowedForDataSize_MB,  displayOpts,  saveOpts,  paramsCategory_MinFunc,  paramsMinFunc,  sizeHid,  methodID_preprocess, P1D0] = parseArgs(...
    {'opttheta' 'patchCountPerSample' 'maxRAMAllowedForDataSize_MB' 'displayOpts' 'saveOpts' 'paramsCategory_MinFunc' 'paramsMinFunc' 'sizeHid' 'methodID_preprocess' },...
    {    []             []                       6*1024                  []           []        'showInfo_saveLog'         []            400               3          },...
    varargin{:});

    %% General variables for display, save and minfunc purposes
    %if displayOpts is not passed as a parameter then set is to 'all' by
    %default to show the user what will happen in every detail.
    %also return it as a parameter because even if it is not passed in it
    %would be returned to be used for upcoming evaluations
    if (isempty(displayOpts))
        displayOpts = setDisplayOpts('all');
        if (displayOpts.defaultParamSet && P1D0.displayOpts==0)
            disp([displayOpts.ies{4} 'display options is set to -all- by default.']);
        end
    end
    
    %if saveOpts is not passed as a parameter then set is to 'none' by
    %default not to save anything into disk unnecessarily.
    %also return it as a parameter because even if it is not passed in it
    %would be returned to be used for upcoming evaluations
    if (isempty(saveOpts))
        saveOpts = setSaveOpts('none');
        if (displayOpts.defaultParamSet && P1D0.saveOpts==0)
            disp([displayOpts.ies{4} 'save options is set to -none- by default.']);
        end
    end
    
    if isfield(paramsMinFunc,'logfile') && isempty(strfind(paramsMinFunc.logfile,'\'))
        paramsMinFunc.logfile = [saveOpts.MainFolder paramsMinFunc.logfile];
    end
    if isempty(paramsMinFunc)
        paramsMinFunc = setParams_minFunc(paramsCategory_MinFunc);
    end
    
    %% Function specific variables in varargin param
    if (displayOpts.defaultParamSet && P1D0.sizeHid==0)
        disp([displayOpts.ies{4} 'sizeHid is set to -' num2str(sizeHid) ' by default.']);
    end                
    if (displayOpts.defaultParamSet && P1D0.maxRAMAllowedForDataSize_MB==0)
        disp([displayOpts.ies{4} 'maxRAMAllowedForDataSize_MB is set to <-' num2str(maxRAMAllowedForDataSize_MB) '> by default.']);
    end 
    if (displayOpts.defaultParamSet && P1D0.methodID_preprocess==0)
        disp([displayOpts.ies{4} 'method for preprocessing the data is set to <normalize input and patch both> by default.']);
    end 
    %% Variables that depend on a mandatory input
    if length(patchSize)==1
        patchSize = [patchSize patchSize];
    end
    
    %% Variables that can not be passed and only for initialization purposes
    elmParamTypeStrCells = {'Standard','CIW','Constrained'};
    outputStruct = struct;
%     outputStruct.additionalInfo_Cell = cell(17,iterationCount);
%     outputStruct.Welm_Cell = cell(17,iterationCount);
%     outputStruct.W_randoms_with_bias_Cell = cell(17,iterationCount);
    outputStruct.accuracyCalced_Cell = cell(17,iterationCount);
    outputStruct.confMatResult_Cell = cell(17,iterationCount);
    outputStruct.recognizedPatchCounts_Cell = cell(17,iterationCount);
%     outputStruct.leftTopMostPixStruct = cell(17,iterationCount);
end

function figureFolderName = getFigureFolderName(batchID, iter, elmParamType, patchSize, patchCountPerSample, patchPickMethod)
    global bigDataPath;
    figureFolderName_Add = ['Train_Batch(' num2str(batchID,'%02d') ')_Iter(' num2str(iter) ')_ELM(' num2str(elmParamType) ')_Patch' mat2str(patchSize) '_PC(' num2str(patchCountPerSample) ')_PP(' num2str(patchPickMethod) ')'];
    figureFolderName = [bigDataPath 'figures\' figureFolderName_Add '\'];
    if ~exist(figureFolderName,'dir')
        mkdir(figureFolderName);
    end
end

function paramsFolderName = getParamsFolderName(batchID, elmParamType, patchSize, patchCountPerSample, patchPickMethod)
    global bigDataPath;
    if isempty(batchID)
        paramsFolderName_Add = ['Train_BatchAll_ELM(' num2str(elmParamType) ')_Patch' mat2str(patchSize) '_PC(' num2str(patchCountPerSample) ')_PP(' num2str(patchPickMethod) ')'];
    else
        paramsFolderName_Add = ['Train_Batch(' num2str(batchID,'%02d') ')_ELM(' num2str(elmParamType) ')_Patch' mat2str(patchSize) '_PC(' num2str(patchCountPerSample) ')_PP(' num2str(patchPickMethod) ')'];
    end
    paramsFolderName = [bigDataPath 'paramsLearnt\' paramsFolderName_Add '\'];
    if ~exist(paramsFolderName,'dir')
        mkdir(paramsFolderName);
    end
end

function paramsFileName = getParamsSaveFileName_ELMV02(iter, accuracyCalced, correctPatchCount, elmParamTypeStr, paramsFolderName)
     paramsFileName_Add = ['Train-Iter(' num2str(iter) '),Acc(' num2str(accuracyCalced,'%4.2f') '),CorrectPatchCount(' num2str(correctPatchCount,'%d') '),ELMType(' elmParamTypeStr ')'];
     paramsFileName = [paramsFolderName paramsFileName_Add '.mat'];
end

function [fileName, figureTitle] = getFigureAndFileName_ConfMat(batchID, iter, elmParamType, accuracyCalced, correctPatchCount, elmParamTypeStr, figureFolderName)
     figureTitle = ['Train-Batch(' num2str(batchID,'%02d') ',Iter(' num2str(iter) '),ELM(' num2str(elmParamType) '),Acc(' num2str(accuracyCalced,'%4.2f') '),CorrectPatchCount(' num2str(correctPatchCount,'%d') '),ELMType(' elmParamTypeStr ')'];
     fileName = [figureFolderName figureTitle '.png'];
end




