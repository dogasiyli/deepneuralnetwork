function elmFocus_V04_FocusTrain( patchSize, varargin)
%elmFocus_V04_FocusTrain use the learnt set of convolution weights in a discriminitive manner
%then focus on the most activated featureMaps
%   We will learn some weights that can discriminate the data well
%   Then we will apply these convolution weights and check out the maximally
%   activated areas per label

% in runLearnPatches.m we know how to take patches from within a batch
% what we want to do now is to run either ELM and take the most
% discriminitive patches from each single image, and learn features from
% those discriminitive patches.
% the question is - if we can already identify an image from such a single
% patch which is inside the whole big image - what is the deal with trying
% to add such strong learnt weights as a feature map

    rng(10);
    global bigDataPath;
    iterationCount = 10;
    [ patchSize, methodID_preprocess,  patchCountPerSamplePassed, elmParamTypeStrCells, dataTypeStr...
     ,displayOpts,  saveOpts,  paramsMinFunc...
     ,opttheta...
     ,dataStruct, batchInfo ...
     ,elmParamType, patchPickMethod ...
    ] = getOptionalParams(patchSize, iterationCount, varargin{:});
    %1. For training - 
    %   initially we will grab patches from 1 batch,
    
    elmParamTypeStr = elmParamTypeStrCells{1,elmParamType};%'Standard'(1),'CIW'(2),'Constrained'(3)
    allBatchParamsFolderName = getParamsFolderName([], elmParamType, patchSize, patchCountPerSamplePassed, patchPickMethod, 'Train');
    for batchID = 1:17
        try
            focusFileName = strrep(['data_batch_' dataTypeStr '_XX_focus.mat'],'XX',num2str(batchID,'%02d'));
            folderName = [bigDataPath 'dataset5'];
            focusFileNameFull = [folderName '\' focusFileName];
            if ~exist(focusFileNameFull,'file')
                paramsFolderName_Train = getParamsFolderName(batchID, elmParamType, patchSize, patchCountPerSamplePassed, patchPickMethod, 'Train');
                paramsFolderName_dataType = getParamsFolderName(batchID, elmParamType, patchSize, patchCountPerSamplePassed, patchPickMethod, dataTypeStr);
                %1. Load batchData
                [dataIdentifier, batchImageCount] = loadBatchData(batchID,dataTypeStr);
                data = dataIdentifier.data;
                labels = dataIdentifier.samplesInfo_RGBD_batch(:,2);
                leftTopMostPixStruct = [];
    %             sizeHid_Cur = sizeHid;
                %2. run for different ELMs
                figureFolderNamesCells = cell(1,iterationCount);
                iter = iterationCount;
                [foundParamsFileName_Train, ~] = getParamsSaveFileName(batchID, iter, elmParamTypeStr, paramsFolderName_Train);
                [~, mostRecognizedPatchesMatFileName_dataType] = getParamsSaveFileName(batchID, iter, elmParamTypeStr, paramsFolderName_dataType, 'train', batchID);
                if exist (foundParamsFileName_Train,'file')
                    load(foundParamsFileName_Train,'additionalInfo','Welm','W_randoms_with_bias','leftTopMostPixStruct','accuracyCalced','confMatResult','correctPatchCount','imageIDResults','sizeHid_Cur')
                    imageIDResults_AnalyzeMat_FileNameFull = [allBatchParamsFolderName 'imageIDResults_AnalyzeMat_B(' num2str(batchID) ')_patchSize' mat2str(patchSize) '.csv'];
                    try
                        disp(['Loading csv file(' imageIDResults_AnalyzeMat_FileNameFull  ')'])
                        imageIDResults_AnalyzeMat = csvread(imageIDResults_AnalyzeMat_FileNameFull);
                        imageIDResults_AnalyzeMat_CurIt = imageIDResults_AnalyzeMat(imageIDResults_AnalyzeMat(:,1)==iter,:);
                        disp(['csv file(' imageIDResults_AnalyzeMat_FileNameFull  ') - loaded.'])
                    catch
                        disp(['csv file(' imageIDResults_AnalyzeMat_FileNameFull  ') - not loaded - there is a problem with this file'])
                        imageIDResults_AnalyzeMat_CurIt = [];
                    end
                else
                    disp(['(' foundParamsFileName_Train ') doesnt exist. skipping..']);
                    continue
                end

                [patchMat, patchLabels, additionalInfo, leftTopMostPixStruct, ~] = grabPatchesFromBatch(dataIdentifier, batchID, patchSize, patchCountPerSamplePassed, 6*1024, methodID_preprocess, [], dataTypeStr);
                imageIDs = additionalInfo.imageIDs;
                patchInfo_RGBD  = additionalInfo.patchInfo_RGBD;

                if exist(mostRecognizedPatchesMatFileName_dataType,'file')
                    disp(['Loading mostRecognizedPatchesMatFile(' mostRecognizedPatchesMatFileName_dataType ')']);
                    load(mostRecognizedPatchesMatFileName_dataType,'mostRecognizedAreas', 'leftTopMostPixStruct', 'patchInfo_RGBD');
                else
                    %[~, leftTopMostPixStruct] = getMostRecognizedAreaOfAllData( [size(data,1) size(data,2)], patchSize, imageIDs, patchInfo_RGBD, labelActivations, leftTopMostPixStruct, data, patchPickMethod);
                    tic();
                    disp('Finding leftTopMost pixels...');
                    [mostRecognizedAreas, leftTopMostPixStruct.leftTopMostPixIDs, patchInfo_RGBD] = getMostRecognizedAreaOfByConvolution_V02( data, additionalInfo, patchSize, imageIDs, patchInfo_RGBD, leftTopMostPixStruct.leftTopMostPixIDs, patchPickMethod, methodID_preprocess, patchCountPerSamplePassed, W_randoms_with_bias, Welm);
                    disp('Finding leftTopMost pixels finished');
                    toc();
                    disp(['Saving mostRecognizedPatchesMatFile(' mostRecognizedPatchesMatFileName_dataType ')']);
                    save(mostRecognizedPatchesMatFileName_dataType,'mostRecognizedAreas', 'leftTopMostPixStruct', 'patchInfo_RGBD');
                end
                [patchMat, patchLabels, additionalInfo, leftTopMostPixStruct, patchCountPerSampleFound] = grabPatchesFromBatch(dataIdentifier, batchID, patchSize, patchCountPerSamplePassed, 6*1024, methodID_preprocess, leftTopMostPixStruct, dataTypeStr);
                figureFolderNamesCells{1,iter} = getFigureFolderName(batchID, iter, elmParamType, patchSize, patchCountPerSampleFound, patchPickMethod);
                [predictedLabels, accuracyCalced, confMatResult, ~, ~, ~, labelActivations] = analyzeResults(patchMat, patchLabels, W_randoms_with_bias, Welm, true );
                %using the most recognized areas - create 
                figureFileNameDraft = [figureFolderNamesCells{1,iter} dataTypeStr '_Batch[' num2str(batchID,'%02d') ']_Letter[NUM2STRLETTER]_Iter[' num2str(iter,'%02d') '].png'];
                [ mostRecognizedPatches_Cropped, mostRecognizedPatchesReturn] = grabResultSummaryImages( mostRecognizedAreas, data, imageIDResults, imageIDResults_AnalyzeMat_CurIt, figureFileNameDraft, additionalInfo);                
                data = mostRecognizedPatchesReturn;
                save(focusFileNameFull,'data','labels');
            end
        catch
            disp('Catched error-Gotcha :)')
        end
    end
end

function [patchMat, patchLabels, additionalInfo, leftTopMostPixStruct, patchCountPerSample] = grabPatchesFromBatch(dataIdentifier, batchID, patchSize, patchCountPerSample, maxRAMAllowedForDataSize_MB, methodID_preprocess, leftTopMostPixStruct, dataTypeStr)
    global bigDataPath;
    if isempty(patchCountPerSample)
        singlePatchSizeMB = (prod(patchSize)*4*8)/(1024*1024);%mb
        singleImageAllowedMB = maxRAMAllowedForDataSize_MB/batchImageCount;%mb
        patchCountPerSample = round(singleImageAllowedMB/singlePatchSizeMB);
    end
    patchBatchFileName = [bigDataPath 'dataset5\data_batch_' dataTypeStr '_' num2str(batchID,'%02d') '_patchSize' mat2str(patchSize) '_patchCountPerSample(' num2str(patchCountPerSample) ')_methodID(' num2str(methodID_preprocess) ').mat'];
    if exist(patchBatchFileName,'file') && isempty(leftTopMostPixStruct)
        %This if statement is reached only for the initial iteration for a
        %batch of data
        disp(['Loading(' patchBatchFileName ')-{leftTopMostPixStruct,additionalInfo}']);
        load(patchBatchFileName,'leftTopMostPixStruct','additionalInfo');
        disp(['Loaded(' patchBatchFileName ')-{leftTopMostPixStruct,additionalInfo}']);
        [ patchMat, patchLabels, additionalInfo] = turnBatchIntoPatches( dataIdentifier, patchSize, 'methodID_preprocess', methodID_preprocess, 'patchCountPerSample', patchCountPerSample, 'leftTopMostPixStruct', leftTopMostPixStruct);
    else
        %Whenever the leftTopMostPixels do change, this function is run!!
        %We have to optimize this part for convolving with stride=1
        warning('This part may need optimization not to re-run normalization process again and again');
        [ patchMat, patchLabels, additionalInfo] = turnBatchIntoPatches( dataIdentifier, patchSize, 'methodID_preprocess', methodID_preprocess, 'patchCountPerSample', patchCountPerSample, 'leftTopMostPixStruct', leftTopMostPixStruct);
        if isempty(leftTopMostPixStruct)
            leftTopMostPixStruct.patchLabels = patchLabels;
            leftTopMostPixStruct.imageIDs = additionalInfo.imageIDs;
            leftTopMostPixStruct.leftTopMostPixIDs = additionalInfo.leftTopMostPixIDs;
            disp(['Saving(' patchBatchFileName ')-{leftTopMostPixStruct,additionalInfo}']);
            save(patchBatchFileName,'leftTopMostPixStruct','additionalInfo','-v7.3');
            disp(['Saved(' patchBatchFileName ')-{leftTopMostPixStruct,additionalInfo}']);
        else
            leftTopMostPixStruct.leftTopMostPixIDs = additionalInfo.leftTopMostPixIDs;
        end
    end
end

%function [patchSize, methodID_preprocess,  patchCountPerSample, elmParamTypeStrCells, dataTypeStr, opttheta, displayOpts,  saveOpts,  paramsMinFunc] = getOptionalParams(patchSize, iterationCount, varargin)
function ...
[ patchSize, methodID_preprocess,  patchCountPerSamplePassed, elmParamTypeStrCells, dataTypeStr...
 ,displayOpts, saveOpts,  paramsMinFunc...
 ,opttheta...
 ,dataStruct, batchInfo ...
 ,elmParamType, patchPickMethod ...
]  = getOptionalParams(patchSize, iterationCount, varargin)
  %% varargin parameters
    %P1D0 -> Passed assed paramaeter = 1, Default value used = 0
    [ opttheta,  patchCountPerSamplePassed,  displayOpts,  saveOpts,  paramsCategory_MinFunc,  paramsMinFunc,  methodID_preprocess,  dataTypeStr, P1D0] = parseArgs(...
    {'opttheta'     'patchCountPerSample'   'displayOpts' 'saveOpts' 'paramsCategory_MinFunc' 'paramsMinFunc' 'methodID_preprocess' 'dataTypeStr'},...
    {    []                  []                  []           []        'showInfo_saveLog'          []                   3             'train'   },...
    varargin{:});

    %% General variables for display, save and minfunc purposes
    %if displayOpts is not passed as a parameter then set is to 'all' by
    %default to show the user what will happen in every detail.
    %also return it as a parameter because even if it is not passed in it
    %would be returned to be used for upcoming evaluations
    if (isempty(displayOpts))
        displayOpts = setDisplayOpts('all');
        if (displayOpts.defaultParamSet && P1D0.displayOpts==0)
            disp([displayOpts.ies{4} 'display options is set to -all- by default.']);
        end
    end
    
    %if saveOpts is not passed as a parameter then set is to 'none' by
    %default not to save anything into disk unnecessarily.
    %also return it as a parameter because even if it is not passed in it
    %would be returned to be used for upcoming evaluations
    if (isempty(saveOpts))
        saveOpts = setSaveOpts('none');
        if (displayOpts.defaultParamSet && P1D0.saveOpts==0)
            disp([displayOpts.ies{4} 'save options is set to -none- by default.']);
        end
    end
    
    if isfield(paramsMinFunc,'logfile') && isempty(strfind(paramsMinFunc.logfile,'\'))
        paramsMinFunc.logfile = [saveOpts.MainFolder paramsMinFunc.logfile];
    end
    if isempty(paramsMinFunc)
        paramsMinFunc = setParams_minFunc(paramsCategory_MinFunc);
    end
    
    %% Function specific variables in varargin param              
    if (displayOpts.defaultParamSet && P1D0.methodID_preprocess==0)
        disp([displayOpts.ies{4} 'method for preprocessing the data is set to <normalize input and patch both> by default.']);
    end 
    if (displayOpts.defaultParamSet && P1D0.dataTypeStr==0)
        disp([displayOpts.ies{4} 'dataTypeStr is set to <' dataTypeStr '> by default.']);
    end 
    %% Variables that depend on a mandatory input
    if length(patchSize)==1
        patchSize = [patchSize patchSize];
    end
    
    %% Variables that can not be passed and only for initialization purposes
    elmParamTypeStrCells = {'Standard','CIW','Constrained'};

    [dataStruct, batchInfo] = createInitialDataStruct_surray('raw');
    elmParamType = 2;
    patchPickMethod=1;
end

function figureFolderName = getFigureFolderName(batchID, iter, elmParamType, patchSize, patchCountPerSample, patchPickMethod)
    global bigDataPath;
    figureFolderName_Add = ['Train_Batch(' num2str(batchID,'%02d') ')_Iter(' num2str(iter) ')_ELM(' num2str(elmParamType) ')_Patch' mat2str(patchSize) '_PC(' num2str(patchCountPerSample) ')_PP(' num2str(patchPickMethod) ')'];
    figureFolderName = [bigDataPath 'figures\' figureFolderName_Add '\'];
    if ~exist(figureFolderName,'dir')
        mkdir(figureFolderName);
    end
end

function paramsFolderName = getParamsFolderName(batchID, elmParamType, patchSize, patchCountPerSample, patchPickMethod, dataTypeStr)
    global bigDataPath;
    dataTypeStr(1) = upper(dataTypeStr(1));
    if isempty(batchID)
        paramsFolderName_Add = [dataTypeStr '_BatchAll_ELM(' num2str(elmParamType) ')_Patch' mat2str(patchSize) '_PC(' num2str(patchCountPerSample) ')_PP(' num2str(patchPickMethod) ')'];
    else
        paramsFolderName_Add = [dataTypeStr '_Batch(' num2str(batchID,'%02d') ')_ELM(' num2str(elmParamType) ')_Patch' mat2str(patchSize) '_PC(' num2str(patchCountPerSample) ')_PP(' num2str(patchPickMethod) ')'];
    end
    paramsFolderName = [bigDataPath 'paramsLearnt\' paramsFolderName_Add '\'];
    if ~exist(paramsFolderName,'dir')
        mkdir(paramsFolderName);
    end
end

function [fileName, figureTitle] = getFigureAndFileName_ConfMat(batchID, iter, elmParamType, accuracyCalced, correctPatchCount, elmParamTypeStr, figureFolderName)
     figureTitle = ['Train-Batch(' num2str(batchID,'%02d') ',Iter(' num2str(iter) '),ELM(' num2str(elmParamType) '),Acc(' num2str(accuracyCalced,'%4.2f') '),CorrectPatchCount(' num2str(correctPatchCount,'%d') '),ELMType(' elmParamTypeStr ')'];
     fileName = [figureFolderName figureTitle '.png'];
end