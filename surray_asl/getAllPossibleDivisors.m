function allPossibleDivisors = getAllPossibleDivisors(N)
%http://www.mathworks.com/matlabcentral/answers/21542-find-divisors-for-a-given-number?requestedDomain=www.mathworks.com#answer_28371
K = 1:N;
allPossibleDivisors = K(rem(N,1:N)==0);
end

