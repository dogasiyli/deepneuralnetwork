function figureFolderName = getFigureFolderName(batchIDDataType, dataTypeStr)
    global bigDataPath;
    mostRecognizedFiguresFileName_Add = ['mostRecognizedAreas,' dataTypeStr 'Batch(' num2str(batchIDDataType,'%02d') ')'];
    figureFolderName = [bigDataPath 'figures\' mostRecognizedFiguresFileName_Add '\'];
    if ~exist(figureFolderName,'dir')
        mkdir(figureFolderName);
    end
end