function [imageIDResults, colTitles] = getImageIDResults( imageIDs, predictedLabels, patchLabels,labelActivations,patchInfo_RGBD)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    colTitles = [ {'imageID'}...1
                 ,{'realLabelOfThesePatches'}...2
                 ,{'patchCountWithinThisImage'}...3
                 ,{'correctPatchCountRatio'}...4
                 ,{'correctPatchGuessCount'}...5
                 ,{'wrongPatchGuessCount'}...6
                 ,{'unnormedMaxActivatedPatchID-rg'}...7
                 ,{'unnormedMaxActivation-rg'}...8
                 ,{'unnormedMeanActivation-rg'}...9
                 ,{'unnormedMinActivation-rg'}...10
                 ,{'unnormedMaxActivatedPatchID-wr'}...11
                 ,{'unnormedMaxActivation-wr'}...12
                 ,{'unnormedMeanActivation-wr'}...13
                 ,{'unnormedMinActivation-wr'}...14
                 ,{'personID'}...15
                 ,{'sequenceID'}...16
                 ]';
    uniqueImageIDs = unique(imageIDs);
    imageIDResults = zeros(length(uniqueImageIDs),16);
    for i=1:length(uniqueImageIDs)
        %for the first imageID
        %1.find the best recognized one
        %2.find the worst recognized one
        curImageID = uniqueImageIDs(i);
        patchIndsOfThisImage = find(imageIDs==curImageID);
        patchCountWithinThisImage = length(patchIndsOfThisImage);
        predictionsOfThesePatches = predictedLabels(patchIndsOfThisImage);
        realLabelOfThesePatches = patchLabels(patchIndsOfThisImage(1));
        patchInfo_Cur = patchInfo_RGBD(patchIndsOfThisImage,:);
        correctPatchGuessCount = sum(predictionsOfThesePatches==realLabelOfThesePatches);
        unnormed_activations = labelActivations.given_un(patchIndsOfThisImage);
        trueGuesses = predictionsOfThesePatches==realLabelOfThesePatches;
        if (correctPatchGuessCount>0)
            [unnormedMaxActivation_t, unnormedMaxActivatedPatchID_t] = max(unnormed_activations(trueGuesses));
            unnormedMeanActivation_t = mean(unnormed_activations(trueGuesses));
            unnormedMinActivation_t = min(unnormed_activations(trueGuesses));
        else 
            unnormedMaxActivation_t = 0;
            unnormedMaxActivatedPatchID_t = 0;
            unnormedMeanActivation_t = 0;
            unnormedMinActivation_t = 0;
        end
        if (patchCountWithinThisImage==correctPatchGuessCount)
            unnormedMaxActivation_f = 0;
            unnormedMaxActivatedPatchID_f = 0;
            unnormedMeanActivation_f = 0;
            unnormedMinActivation_f = 0;
        else 
            [unnormedMaxActivation_f, unnormedMaxActivatedPatchID_f] = max(unnormed_activations(~trueGuesses));
            unnormedMeanActivation_f = mean(unnormed_activations(~trueGuesses));
            unnormedMinActivation_f = min(unnormed_activations(~trueGuesses));
        end
        correctPatchCountRatio = correctPatchGuessCount./patchCountWithinThisImage;
        imageIDResults(i,:) = [curImageID realLabelOfThesePatches patchCountWithinThisImage correctPatchCountRatio correctPatchGuessCount (patchCountWithinThisImage-correctPatchGuessCount) ...
                               unnormedMaxActivatedPatchID_t unnormedMaxActivation_t unnormedMeanActivation_t unnormedMinActivation_t...
                               unnormedMaxActivatedPatchID_f unnormedMaxActivation_f unnormedMeanActivation_f unnormedMinActivation_f...
                               patchInfo_Cur(1,[1 3])];
    end
    %sort the result
    %1.(  2) Ascending order of realLabelOfThesePatches
    %2.( -4)Descending order of correctPatchCountRatio
    %3.( -8)Descending order of unnormedMaxActivation
    imageIDResults = sortrows(imageIDResults,[2 -4 -8]);
end

