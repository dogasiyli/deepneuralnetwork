function [mostRecognizedArea, leftTopMostPixStruct] = getMostRecognizedAreaOfAllData( imSize, patchSize, imageIDs, patchInfo_RGBD, labelActivations, leftTopMostPixStruct, data, patchPickMethod, figureID)
%getMostRecognizedAreaOfAllData Summary of this function goes here
%   Detailed explanation goes here
    leftTopMostPixIDs = leftTopMostPixStruct.leftTopMostPixIDs;
    imageIDsUnique = reshape(unique(imageIDs),1,[]);
    imageCount = length(imageIDsUnique);
    mostRecognizedArea = zeros(imSize(1),imSize(2), imageCount);
    for j = 1:imageCount
        imageID = imageIDsUnique(j);
        patchRowsToDisplay = find(imageIDs==imageID);
        patchCount = length(patchRowsToDisplay);
        patchInfo_Cur = patchInfo_RGBD(patchRowsToDisplay,:);
        activationsOfPatches_un = labelActivations.given_un(patchRowsToDisplay);
        for i=1:patchCount
            rectR = patchInfo_Cur(i,4):(patchInfo_Cur(i,4)+patchSize(1));
            rectC = patchInfo_Cur(i,5):(patchInfo_Cur(i,5)+patchSize(2));
            mostRecognizedArea(rectR,rectC,j) = mostRecognizedArea(rectR,rectC) + activationsOfPatches_un(i);
        end
        
        imageRect = {patchInfo_Cur(1,6):patchInfo_Cur(1,7);patchInfo_Cur(1,8):patchInfo_Cur(1,9)};
        personID = patchInfo_Cur(1,1);
        letterID = patchInfo_Cur(1,2);
        imageSeqID = patchInfo_Cur(1,3);
        if (exist('figureID','var') && ~isempty(figureID))
            figure(figureID);clf;hold on;
            SPRC = 2;
            SPCC = 3;
            mostRecognizedArea_WPadding = mostRecognizedArea(:,:,j);
            mostRecognizedArea_WOPadding = mostRecognizedArea_WPadding(imageRect{1},imageRect{2});
            
            curImage = data(:,:,:,imageID);
            subplot(SPRC,SPCC,1);
            rgbIm = curImage(imageRect{1},imageRect{2},1:3);
            imshow(rgbIm);hold on;
            
            subplot(SPRC,SPCC,2);
            imagesc(mostRecognizedArea_WOPadding);hold on;
            xlim([1 size(mostRecognizedArea_WOPadding,2)]);
            ylim([1 size(mostRecognizedArea_WOPadding,1)]);
            set(gca,'XTick',[]);set(gca,'YTick',[]);
            title({'Most Activated','Area Map'});
            xlabel(['Person(' num2str(personID) '),Letter(' num2str(letterID) '),ImageSeqID(' num2str(imageSeqID) ')']);
            
            subplot(SPRC,SPCC,3);
            mostRecognizedArea_RGB = mostRecognizedArea_WOPadding./max(mostRecognizedArea_WOPadding(:));
            mostRecognizedArea_RGB = repmat(mostRecognizedArea_RGB,[1 1 3]);
            mostRecognizedArea_RGB = rgbIm.*mostRecognizedArea_RGB;
            imshow(mostRecognizedArea_RGB);
            title('RGB Area');
            xlabel('Activation Highlighted');
        end
        
        %patchPickMethod=1;
        [rowIDs_01,colIDs_01] = getPatchesFromMostRecognizedAreaMat( squeeze(mostRecognizedArea(:,:,j)), patchInfo_Cur, 1, patchCount);
        %patchPickMethod=2;
        [rowIDs_02,colIDs_02] = getPatchesFromMostRecognizedAreaMat( squeeze(mostRecognizedArea(:,:,j)), patchInfo_Cur, 2, patchCount);
        %patchPickMethod=3;
        [rowIDs_03,colIDs_03] = getPatchesFromMostRecognizedAreaMat( squeeze(mostRecognizedArea(:,:,j)), patchInfo_Cur, 3, patchCount);
        
        %find the row and col dif values for moving found pixels from
        %center to leftTopMost pixel of those patches
        [r_dif_1, c_dif_1, ind_dif] = calc_rc_dif(patchSize, 'closeToBegin', imSize(2));
        r_dif = round(patchSize(1)/2);
        c_dif = round(patchSize(2)/2);
        
        %move the found values from center to leftTopMost
        rowIDs_01 = rowIDs_01-r_dif;colIDs_01 = colIDs_01-c_dif;
        rowIDs_02 = rowIDs_02-r_dif;colIDs_02 = colIDs_02-c_dif;
        rowIDs_03 = rowIDs_03-r_dif;colIDs_03 = colIDs_03-c_dif;
        
        %justify the found left leftTopMostPixID values such that the
        %patches remain in the defined area
        [rowIDs_01, colIDs_01, allPushMat_01] = justifyRowColIDs(rowIDs_01,colIDs_01,patchInfo_Cur);
        [rowIDs_02, colIDs_02, allPushMat_02] = justifyRowColIDs(rowIDs_02,colIDs_02,patchInfo_Cur);
        [rowIDs_03, colIDs_03, allPushMat_03] = justifyRowColIDs(rowIDs_03,colIDs_03,patchInfo_Cur);
        
        %leftTopMostPixIDs = struct;
        %leftTopMostPixIDs.M01 = sub2ind(imSize,rowIDs_01, colIDs_01);
        %leftTopMostPixIDs.M02 = sub2ind(imSize,rowIDs_02, colIDs_02);
        %leftTopMostPixIDs.M03 = sub2ind(imSize,rowIDs_03, colIDs_03);
        
        if (exist('figureID','var') && ~isempty(figureID))
            subplot(SPRC,SPCC,4);
            rgbIm_Patched = getSelectedPatches_v01(curImage(:,:,1:3), patchSize, rowIDs_01, colIDs_01);
            rgbIm_Patched = rgbIm_Patched(imageRect{1},imageRect{2},1:3);
            imshow(rgbIm_Patched);
            title('Method-1');

            subplot(SPRC,SPCC,5);
            rgbIm_Patched = getSelectedPatches_v01(curImage(:,:,1:3), patchSize, rowIDs_02, colIDs_02);
            rgbIm_Patched = rgbIm_Patched(imageRect{1},imageRect{2},1:3);
            imshow(rgbIm_Patched);
            title('Method-2');
            
            subplot(SPRC,SPCC,6);
            rgbIm_Patched = getSelectedPatches_v01(curImage(:,:,1:3), patchSize, rowIDs_03, colIDs_03);
            rgbIm_Patched = rgbIm_Patched(imageRect{1},imageRect{2},1:3);
            imshow(rgbIm_Patched);
            title('Method-3');
        end        
        leftTopMostPixIDs(patchRowsToDisplay) = sub2ind(imSize,rowIDs_01, colIDs_01);
    end
    leftTopMostPixStruct.leftTopMostPixIDs = leftTopMostPixIDs;
end

function rgbIm_Patched = getSelectedPatches_v01(rgbIm, patchSize, rowIDs, colIDs)
    rgbIm_Patched = zeros(size(rgbIm));
    for i=1:length(rowIDs)
        pathRect = {rowIDs(i):(rowIDs(i)+patchSize(1));colIDs(i):(colIDs(i)+patchSize(2))};
        rgbIm_Patched(pathRect{1},pathRect{2},:) = rgbIm(pathRect{1},pathRect{2},:);
    end
end

function [rowIDs, colIDs, allPushMat] = justifyRowColIDs(rowIDs,colIDs,patchInfo_Cur)
    pixelCount = length(rowIDs);
    
    rowDif = patchInfo_Cur(1,6)-rowIDs;
    rowPush = max(zeros(pixelCount,1),rowDif);
    rowIDs = rowIDs + rowPush;

    rowDif = rowIDs-patchInfo_Cur(1,7);
    rowPull = max(zeros(pixelCount,1),rowDif);
    rowIDs = rowIDs - rowPull;

    colDif = patchInfo_Cur(1,8)-colIDs;
    colPush = max(zeros(pixelCount,1),colDif);
    colIDs = colIDs + colPush;

    colDif = colIDs-patchInfo_Cur(1,9);
    colPull = max(zeros(pixelCount,1),colDif);
    colIDs = colIDs - colPull;
    
    allPushMat = [rowPush,rowPull,colPush,colPull];
end
