function [mostRecognizedArea, leftTopMostPixIDsNew, patchInfo_RGBD, labelActivations] = getMostRecognizedAreaOfByConvolution( data, additionalInfo, patchSize, imageIDs, patchInfo_RGBD, leftTopMostPixIDs, patchPickMethod, methodID_preprocess, patchCountPerSample, W_randoms_with_bias, Welm, labelActivations, figureID)
%getMostRecognizedAreaOfAllData Summary of this function goes here
%   Detailed explanation goes here
    sizeImg = [size(data,1) size(data,2)];
    sizeChn = size(data,3);
    imageIDsUnique = reshape(unique(imageIDs),1,[]);
    countSamples = length(imageIDsUnique);
    leftTopMostPixIDsNew = leftTopMostPixIDs;
    mostRecognizedArea = zeros(sizeImg(1),sizeImg(2), countSamples);
    rowColIDs = cell(3,2);
    allPushMat = cell(3,1);
    stride = [5 5];
    %we need to normalize the data(if it isnt normalized yet)
    %mean&std of data is depending on all features of the input (RGBD)
    %additionalInfo.P04_meanOfData;
    %additionalInfo.P05_stdOfData;
    disp('Normalizing input data')
    [dataReshaped, additionalInfo2] = normalizeInputData(data, sizeImg, countSamples, sizeChn);
    disp('Input data normalized')
    patchMat = zeros(patchSize(1),patchSize(2),sizeChn,countSamples*patchCountPerSample);
    for j = 1:countSamples
        if (mod(j,100)==0)
            disp([num2str(j) ' of ' num2str(countSamples) ' finished...'])
        end
        imageID = imageIDsUnique(j);
        patchRowsToDisplay = find(imageIDs==imageID);
        patchCount = length(patchRowsToDisplay);
        patchInfo_Cur = patchInfo_RGBD(patchRowsToDisplay,:);
        letterID = patchInfo_Cur(1,2);
        
        %here I need to convolve the whole image
        
        %create 'allPatchesMat'
        r_CurImage = patchInfo_Cur(1,6):stride(1):(patchInfo_Cur(1,7)-patchSize(1)-1);
        c_CurImage = patchInfo_Cur(1,8):stride(2):(patchInfo_Cur(1,9)-patchSize(2)-1);
        [Ri,Ci] = meshgrid(r_CurImage,c_CurImage');
        leftTopMostPixIDs_All_ForCurPatch = sub2ind(sizeImg,Ri(:),Ci(:));
        patchCount_All = length(leftTopMostPixIDs_All_ForCurPatch);
        
        %grab patches according to leftTopMost Pixels
        %
        [patchMat_Cur, invalidPatchIDs] = acquirePatches( patchSize, dataReshaped(:,:,:,j), sizeChn, ones(patchCount_All,1), leftTopMostPixIDs_All_ForCurPatch, true);
        
        %we need a patch normalization
        %the mean of the data does only change when the batch changes
        %but the mean of patchMat cahnges at every iteration
        %the patchMean we will use here is the previous calculated one,
        %because learning is done using those values
        %mean of patches is for each linearized patch vector(14400x1) for
        %60,60,4 -> sized patches
        additionalInfo.meanPatch;
        patchMat_Cur = bsxfun(@minus,patchMat_Cur,additionalInfo.meanPatch);
        givenLabels = letterID*ones(1,patchCount_All);
        [predictedLabels, accuracyCalced, confMatResult, ~, ~, ~, labelActivationsCur] = analyzeResults(patchMat_Cur, givenLabels, W_randoms_with_bias, Welm, false);
        activationsOfPatches_un = max(0,labelActivationsCur.given_un);
        for i=1:patchCount_All
            rectR = Ri(i):(Ri(i)+patchSize(1));
            rectC = Ci(i):(Ci(i)+patchSize(2));
            mostRecognizedArea(rectR,rectC,j) = mostRecognizedArea(rectR,rectC,j) + activationsOfPatches_un(i);
        end
        %assert(max(mostRecognizedArea(:))==0,'no values above zero!!!');
        if (max(mostRecognizedArea(:))==0)
            warning('no values above zero!!!');
        end
        
        imageRect = {patchInfo_Cur(1,6):patchInfo_Cur(1,7);patchInfo_Cur(1,8):patchInfo_Cur(1,9)};
        personID = patchInfo_Cur(1,1);
        letterID = patchInfo_Cur(1,2);
        imageSeqID = patchInfo_Cur(1,3);
        if (exist('figureID','var') && ~isempty(figureID))
            figure(figureID);clf;hold on;
            SPRC = 2;
            SPCC = 3;
            mostRecognizedArea_WPadding = mostRecognizedArea(:,:,j);
            mostRecognizedArea_WOPadding = mostRecognizedArea_WPadding(imageRect{1},imageRect{2});
            
            curImage = data(:,:,:,imageID);
            subplot(SPRC,SPCC,1);
            rgbIm = curImage(imageRect{1},imageRect{2},1:3);
            imshow(rgbIm);hold on;
            
            subplot(SPRC,SPCC,2);
            imagesc(mostRecognizedArea_WOPadding);hold on;
            xlim([1 size(mostRecognizedArea_WOPadding,2)]);
            ylim([1 size(mostRecognizedArea_WOPadding,1)]);
            set(gca,'XTick',[]);set(gca,'YTick',[]);
            title({'Most Activated','Area Map'});
            xlabel(['Person(' num2str(personID) '),Letter(' num2str(letterID) '),ImageSeqID(' num2str(imageSeqID) ')']);
            
            subplot(SPRC,SPCC,3);
            mostRecognizedArea_RGB = mostRecognizedArea_WOPadding./max(mostRecognizedArea_WOPadding(:));
            mostRecognizedArea_RGB = repmat(mostRecognizedArea_RGB,[1 1 3]);
            mostRecognizedArea_RGB = rgbIm.*mostRecognizedArea_RGB;
            imshow(mostRecognizedArea_RGB);
            title('RGB Area');
            xlabel('Activation Highlighted');
        end
        
        %find the row and col dif values for moving found pixels from
        %center to leftTopMost pixel of those patches
        %[r_dif_1, c_dif_1, ind_dif] = calc_rc_dif(patchSize, 'closeToBegin', imSize(2));
        r_dif = round(patchSize(1)/2);
        c_dif = round(patchSize(2)/2);
        
        for k=1:3%patchPickMethod=1;
            personID = patchInfo_Cur(1,1);
            letterID = patchInfo_Cur(1,2);
            seqID = patchInfo_Cur(1,3);
            try
                [rowColIDs{k,1},rowColIDs{k,2},errorOccured] = getPatchesFromMostRecognizedAreaMat( squeeze(mostRecognizedArea(:,:,j)), patchInfo_Cur, k, patchCount);
                %move the found values from center to leftTopMost
                if isempty(rowColIDs{k,1}) || errorOccured
                    rowColIDs{k,1} = patchInfo_Cur(:,4);
                    rowColIDs{k,2} = patchInfo_Cur(:,5);
                    warning(['getPatchesFromMostRecognizedAreaMat returned empty row and col IDs -the inputs were used instead-Method(' num2str(k) '),Person(' num2str(personID) '),Letter(' num2str(letterID) '),SeqID(' num2str(seqID) ')']);
                else
                    rowColIDs{k,1} = rowColIDs{k,1}-r_dif;
                    rowColIDs{k,2} = rowColIDs{k,2}-c_dif;
                    %justify the found left leftTopMostPixID values such that the
                    %patches remain in the defined area
                    [rowColIDs{k,1},rowColIDs{k,2}, allPushMat{k,1}] = justifyRowColIDs(rowColIDs{k,1},rowColIDs{k,2},patchInfo_Cur);
                    if isempty(rowColIDs{k,1})
                        rowColIDs{k,1} = patchInfo_Cur(:,4);
                        rowColIDs{k,2} = patchInfo_Cur(:,5);
                        warning(['justifyRowColIDs returned empty row and col IDs -the inputs were used instead-Method(' num2str(k) '),Person(' num2str(personID) '),Letter(' num2str(letterID) '),SeqID(' num2str(seqID) ')']);
                    end
                end
            catch
                rowColIDs{k,1} = patchInfo_Cur(:,4);
                rowColIDs{k,2} = patchInfo_Cur(:,5);
                warning(['There was an error and row and col IDs were empty-the inputs were used instead-Method(' num2str(k) '),Person(' num2str(personID) '),Letter(' num2str(letterID) '),SeqID(' num2str(seqID) ')']);
            end
            if (exist('figureID','var') && ~isempty(figureID))
                subplot(SPRC,SPCC,k+3);
                rgbIm_Patched = getSelectedPatches_v01(curImage(:,:,1:3), patchSize, rowColIDs{k,1},rowColIDs{k,2});
                rgbIm_Patched = rgbIm_Patched(imageRect{1},imageRect{2},1:3);
                imshow(rgbIm_Patched);
                title(['Method-' num2str(k)]);
            end
        end
        leftTopMostPixIDsNew(patchRowsToDisplay) = sub2ind(sizeImg,rowColIDs{patchPickMethod,1}, rowColIDs{patchPickMethod,1});
        patchInfo_RGBD(patchRowsToDisplay,4:5) = [rowColIDs{k,1}, rowColIDs{k,1}];
    end
end

function rgbIm_Patched = getSelectedPatches_v01(rgbIm, patchSize, rowIDs, colIDs)
    rgbIm_Patched = zeros(size(rgbIm));
    for i=1:length(rowIDs)
        pathRect = {rowIDs(i):(rowIDs(i)+patchSize(1));colIDs(i):(colIDs(i)+patchSize(2))};
        rgbIm_Patched(pathRect{1},pathRect{2},:) = rgbIm(pathRect{1},pathRect{2},:);
    end
end

function [rowIDs, colIDs, allPushMat] = justifyRowColIDs(rowIDs,colIDs,patchInfo_Cur)
    pixelCount = length(rowIDs);
    
    rowDif = patchInfo_Cur(1,6)-rowIDs;
    rowPush = max(zeros(pixelCount,1),rowDif);
    rowIDs = rowIDs + rowPush;

    rowDif = rowIDs-patchInfo_Cur(1,7);
    rowPull = max(zeros(pixelCount,1),rowDif);
    rowIDs = rowIDs - rowPull;

    colDif = patchInfo_Cur(1,8)-colIDs;
    colPush = max(zeros(pixelCount,1),colDif);
    colIDs = colIDs + colPush;

    colDif = colIDs-patchInfo_Cur(1,9);
    colPull = max(zeros(pixelCount,1),colDif);
    colIDs = colIDs - colPull;
    
    allPushMat = [rowPush,rowPull,colPush,colPull];
end
