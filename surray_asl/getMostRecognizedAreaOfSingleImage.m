function mostRecognizedArea = getMostRecognizedAreaOfSingleImage( data, imageIDs, patchInfo_RGBD, labelActivations, imageID)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
    curImage = data(:,:,:,imageID);
    patchRowsToDisplay = find(imageIDs==imageID);
    patchCount = length(patchRowsToDisplay);
    patchInfo_Cur = patchInfo_RGBD(patchRowsToDisplay,:);
    activationsOfPatches_un = labelActivations.given_un(patchRowsToDisplay);

    mostRecognizedArea = zeros(size(curImage,1));
    for i=1:patchCount
        p_lt = patchInfo_Cur(i,4:5) + [    0                0      ];
        p_rt = patchInfo_Cur(i,4:5) + [    0          patchSize(2) ];
       %p_rb = patchInfo_Cur(i,4:5) + [ patchSize(1)  patchSize(2) ];
        p_lb = patchInfo_Cur(i,4:5) + [ patchSize(1)        0      ];
        rectR = p_lt(1):p_lb(1);
        rectC = p_lt(2):p_rt(2);
        mostRecognizedArea(rectR,rectC) = mostRecognizedArea(rectR,rectC) + activationsOfPatches_un(i);
    end
end

