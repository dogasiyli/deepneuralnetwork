function [paramsFileName, mostRecognizedPatchesMatFileName, paramsFileName_FinalWeights] = getParamsSaveFileName(batchIDTrain, iter, elmParamTypeStr, paramsFolderName, dataTypeStr, batchIDDataType)
    
     %e.g. - D:\Data\paramsLearnt\Train_Batch(01)_ELM(2)_Patch[60 60]_PC(24)_PP(1)\Train-Iter(1),ELMType(CIW).mat
     paramsFileName_Add = ['Batch(' num2str(batchIDTrain,'%02d') '),Iter(' num2str(iter) '),ELMType(' elmParamTypeStr ')'];
     paramsFileName = [paramsFolderName paramsFileName_Add '.mat'];
     
     %e.g. - D:\Data\paramsLearnt\Train_Iter(Last)_ELM(2)_Patch[60 60]_PC(24)\Batch(01)_Weights.mat
     paramsFileName_Add = ['Batch(' num2str(batchIDTrain,'%02d') ')_Weights'];
     paramsFileName_FinalWeights = [paramsFolderName paramsFileName_Add '.mat'];
     
     
     %e.g. - D:\Data\paramsLearnt\Valid_Batch(01)_ELM(2)_Patch[60 60]_PC(24)_PP(1)\mostRecognizedAreas,validBatch(XX),trainBatch(XX),ELMType(CIW).mat
     if nargin>4 && exist('dataTypeStr','var') && exist('batchIDDataType','var')
         mostRecognizedAreasFileName_Add = ['mostRecognizedAreas-' dataTypeStr 'Batch(' num2str(batchIDDataType,'%02d') '),trainBatch(' num2str(batchIDTrain,'%02d') '),ELMType(' elmParamTypeStr ')'];
         mostRecognizedPatchesMatFileName = [paramsFolderName mostRecognizedAreasFileName_Add '.mat'];
     else
         mostRecognizedPatchesMatFileName = [];
     end
end