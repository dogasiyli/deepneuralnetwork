function [rowIDs,colIDs,errorOccured] = getPatchesFromMostRecognizedAreaMat( mostRecognizedArea, patchInfCur, patchPickMethod, patchCount)
%getPatchesFromMostRecognizedAreaMat Looks at the heat map of
%mostRecognized area and return the patches leftTopMostPixelIDs according
%to the patchPickMethod
%   Detailed explanation goes here
%   I have to find rowIDs and colIDs for this image that is maximized
%   When doing this I can have different ways
%     I will linearize the mostRecognizedArea of the current image
%     I will sort the values from max to lowest
%     The rowIDs and colIDs will be the center pixels for the patches to be selected
%     Method-1 : pick them one by one-> 1:patchCount
%     Method-2 : find the mean value of the non-zero pixels
%                the pickable centers are those above the mean
%                get linearly spaced samples from them
%     3rd method doesn't linearize the image
%     Method-3 : find the maximum activated block
%                pick either 1(point),2(line) or 4(rectangle) corners
%                make the block zero and go the the next block
errorOccured = false;
    switch patchPickMethod
        case 0
            try
                wR = patchInfCur(5)/patchInfCur(6);%rowSize_div_colSize weight of row
                %so patchCount is patchCount = pcR*pcC - so what are pcR and pcC
                allPossibleValuesOf_pC = getAllPossibleDivisors(patchCount);%all possible values for pC - patchCount Column
                pC_real = sqrt(patchCount/wR);
                [~, pC] = min(abs(allPossibleValuesOf_pC-pC_real));
                pC = allPossibleValuesOf_pC(pC);
                pR = patchCount/pC;

                %so horizontally image will be divided into pC pieces that touches on top of each other
                %vertically image will be divided into pR pieces that touches on top of each other

                r_CurImage = floor(linspace(patchInfCur(7),max(patchInfCur(7),patchInfCur(8)-patchSize(1)-1),pR));
                c_CurImage = floor(linspace(patchInfCur(9),max(patchInfCur(9),patchInfCur(10)-patchSize(2)-1),pC));
                [rowIDs,colIDs] = meshgrid(r_CurImage,c_CurImage');
            catch
                errorOccured = true;
            end
        case 1
            try
                %linearize the mostRecognizedArea of the current image
                mostRecognizedArea_Vec = mostRecognizedArea(:);
                %sort the values from max to lowest
                [vals, inds] = sort(mostRecognizedArea_Vec, 'descend');
                %The rowIDs and colIDs will be the center pixels for the patches to be selected
                %pick them one by one-> 1:patchCount
                selectedInds = inds(1:patchCount);
                [rowIDs,colIDs] = ind2sub(size(mostRecognizedArea),selectedInds);
            catch
                errorOccured = true;
            end
        case 2
            try
                %linearize the mostRecognizedArea of the current image
                mostRecognizedArea_Vec = mostRecognizedArea(:);
                %sort the values from max to lowest
                [vals, inds] = sort(mostRecognizedArea_Vec, 'descend');
                %The rowIDs and colIDs will be the center pixels for the patches to be selected
                %find the mean value of the non-zero pixels
                meanAct = mean(vals(vals>0));
                %the pickable centers are those above the mean
                slct = vals>meanAct;
                selectablePixel_Vals = vals(slct);
                selectablePixel_Inds = inds(slct);
                %get linearly spaced samples from them
                slct = round(linspace(1,sum(slct),patchCount));
                selectedVals = selectablePixel_Vals(slct);
                selectedInds = selectablePixel_Inds(slct);
                [rowIDs,colIDs] = ind2sub(size(mostRecognizedArea),selectedInds);
            catch
                errorOccured = true;
            end
        case 3
            try
                selectedIndsCount = 1;
                mostRecognizedArea_Temp = mostRecognizedArea;
                rowIDs = zeros(patchCount,1);
                colIDs = zeros(patchCount,1);
                while selectedIndsCount<patchCount
                    %find the maximum activated block
                    maxPixelActivation = max(mostRecognizedArea_Temp(:));
                    [maxRegion_rows,maxRegion_cols] = find(mostRecognizedArea_Temp==maxPixelActivation);
                    %this region vals can be either connected or seperate
                    %pick either 1(point),3(line) edges+center or 5(rectangle) corners+center
                    [curRegionRows,curRegionCols,pointCount,shapeIdentifier,pointsConnectedBool] = analyzeBlock(maxRegion_rows,maxRegion_cols);
                    %make the block zero and go the the next block
                    switch shapeIdentifier
                        case 'point'
                            mostRecognizedArea_Temp(curRegionRows,curRegionCols) = 0;
                        case 'line_row'
                            mostRecognizedArea_Temp(curRegionRows(1):curRegionRows(2),curRegionCols(1)) = 0;
                        case 'line_col'
                            mostRecognizedArea_Temp(curRegionRows(1),curRegionCols(1):curRegionCols(2)) = 0;
                        case 'rectangle'
                            if pointsConnectedBool
                                mostRecognizedArea_Temp(min(curRegionRows):max(curRegionRows),min(curRegionCols):max(curRegionCols)) = 0;
                            else
                                mostRecognizedArea_Temp(curRegionRows,curRegionCols) = 0;
                            end
                    end
                    selectablePointCount = min(patchCount-selectedIndsCount+1,pointCount);
                    rowIDs(selectedIndsCount:selectedIndsCount+selectablePointCount-1) = curRegionRows(1:selectablePointCount);
                    colIDs(selectedIndsCount:selectedIndsCount+selectablePointCount-1) = curRegionCols(1:selectablePointCount);
                    selectedIndsCount = selectedIndsCount+selectablePointCount;
                end
            catch
                errorOccured = true;
            end
            
    end
    if (errorOccured)
        rowIDs = [];
        colIDs = [];
    end
end

function [curRegionRows,curRegionCols,pointCount,shapeIdentifier,pointsConnectedBool] = analyzeBlock(rows, cols)
    %this region vals can be either connected or seperate
    %for the block to be connected rows and cols must be connected
    uni_rows = unique(rows);
    uni_cols = unique(cols);
    minRow = min(uni_rows);
    maxRow = max(uni_rows);
    minCol = min(uni_cols);
    maxCol = max(uni_cols);
    rowLength = length(uni_rows);
    colLength = length(uni_cols);
    
    if (rowLength == 1 && colLength == 1)
        shapeIdentifier = 'point';
    elseif (rowLength > 1 && colLength == 1)
        shapeIdentifier = 'line_row';
    elseif (rowLength == 1 && colLength > 1)
        shapeIdentifier = 'line_col';
    else
        shapeIdentifier = 'rectangle';
    end
    
    pointsConnectedBool = true;
    switch shapeIdentifier
        case 'point'
            pointCount = 1;
            curRegionRows = uni_rows(1);
            curRegionCols = uni_cols(1);
        case 'line_row'
            pointCount = 3;
            curRegionRows = [minRow,maxRow,round(mean(uni_rows))]';
            curRegionCols = [uni_cols(1),uni_cols(1),uni_cols(1)]';
        case 'line_col'
            pointCount = 3;
            curRegionRows = [uni_rows(1),uni_rows(1),uni_rows(1)]';
            curRegionCols = [minCol,maxCol,round(mean(uni_cols))]';
        case 'rectangle'
            pointCount = 5;
            rowsAreConnected = (rowLength==(maxRow-minRow+1));
            colsAreConnected = (colLength==(maxCol-minCol+1));
            if (rowsAreConnected && colsAreConnected)
                %a connected area
                curRegionRows = [minRow,minRow,maxRow,maxRow,round(mean(uni_rows))]';
                curRegionCols = [minCol,maxCol,minCol,maxCol,round(mean(uni_cols))]';
                pointsConnectedBool = true;
            elseif (rowsAreConnected && ~colsAreConnected)
                %cols are not connected
                %get the first connected col block
                allColVals = NaN(1,maxCol);
                allColVals(minCol:maxCol) = 0;
                allColVals(uni_cols) = 1;
                minCol = find(allColVals==1,1,'first');
                maxCol = find(allColVals==0,1,'first');
                curRegionRows = [minRow,minRow,maxRow,maxRow,round(mean(uni_rows))]';
                curRegionCols = [minCol,maxCol,minCol,maxCol,round(mean(minCol:maxCol))]';
                pointsConnectedBool = true;
            elseif (~rowsAreConnected && colsAreConnected)
                %rows are not connected
                allRowVals = NaN(1,maxRow);
                allRowVals(minRow:maxRow) = 0;
                allRowVals(uni_rows) = 1;
                minRow = find(allRowVals==1,1,'first');
                maxRow = find(allRowVals==0,1,'first');
                curRegionRows = [minRow,minRow,maxRow,maxRow,round(mean(minRow:maxRow))]';
                curRegionCols = [minCol,maxCol,minCol,maxCol,round(mean(uni_cols))]';
                pointsConnectedBool = true;
            else
                %nothing is connected
                %just pick 4 random pixels and return
                pointCountAll = length(rows);
                selectedPointInds = randi(pointCountAll,1,5);
                curRegionRows = reshape(rows(selectedPointInds),1,[]);
                curRegionCols = reshape(cols(selectedPointInds),1,[]);
                pointsConnectedBool = false;
            end
    end
end













