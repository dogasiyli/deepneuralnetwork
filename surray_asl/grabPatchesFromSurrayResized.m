function grabPatchesFromSurrayResized(batchID, patchCountPerSample, convFiltSize, featCountToLearn)
    global bigDataPath;
    data = [];
    labels = [];
    displayProcessInfo = true;
    applyDummyZCA = false;
    load([bigDataPath 'dataset5\data_batch_train_' num2str(batchID,'%02d') '_resized[80 80].mat'],'data','labels');
    dataRS = reshape(data,80*80*3,[]);
    [patches_cur, imageIDs] = acquireRandomPatches(patchCountPerSample, convFiltSize, dataRS, 3);
    labels_cur = labels(imageIDs);
    [ZCA_patches, ZCA_p] = ZCA_whiten_patches(patches_cur, [], applyDummyZCA, displayProcessInfo);
    
    sizeHid = featCountToLearn;
    [accuracyCalced, predictedLabels, W_randoms_with_bias, Welm, A, confMatResult, labelActivations] = ELM_training(ZCA_patches, labels_cur, 'sizeHid', sizeHid);
    
    countSamples = size(dataRS,2);
    for imID = 1:countSamples
        indicesOfThisImg = find(imageIDs==imID);
        labelsOfThisImage_GT = labels_cur(indicesOfThisImg);
        labelsOfThisImage_PR = predictedLabels(indicesOfThisImg);
        imagePatchGuessTF = labelsOfThisImage_GT(:)==labelsOfThisImage_PR(:);
        if sum(imagePatchGuessTF==0)
            %no patch is correct
        else
            %some patches are correct
        end
    end
end