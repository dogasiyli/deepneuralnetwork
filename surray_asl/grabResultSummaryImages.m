function [ mostRecognizedPatches_Cropped, mostRecognizedPatchesReturn] = grabResultSummaryImages( mostRecognizedAreas, data, imageIDResults, imageIDResults_AnalyzeMat_CurIt, figureFileNameDraft, additionalInfo)
    warning('off', 'Images:initSize:adjustingMag');

    patchRC = 80;%size of each patch that will be cropped from original image
    patchRC_Box = 100;%size of each patchBox
    patchRC_Dif = round(0.5*(patchRC_Box-patchRC));
    minThickness = 2;%round(0.5*patchRC_Dif);    
    allowedThickness = patchRC_Dif-minThickness;%patchRC_Dif;
    
%     patchCountRow = 10;
%     patchCountCol = 13;
    countSamples = size(mostRecognizedAreas,3);

    mostRecognizedPatchesReturn = zeros(patchRC,patchRC,3,countSamples);
    mostRecognizedPatches_Cropped = [];%zeros(patchCountRow*patchRC_Box, patchCountCol*patchRC_Box, 3, 24);
    %mostRecognizedPatches_Highlighted = [];%zeros(patchCountRow*patchRC_Box, patchCountCol*patchRC_Box, 3, 24);
            
    for c = 1:24
        %first grab the sampleIDs of 
        categoryResults = imageIDResults(imageIDResults(:,2)==c,:);
        categoryResults = sortrows(categoryResults,1);
        %now the imageIDs that fall into category 'c' are in categoryResults
        patchCount_All = size(categoryResults,1);
        
        patchAct_Unnormed_Vec = categoryResults(1:patchCount_All,8);
        patchAct_Unnormed_Vec = 0.25+0.75*(patchAct_Unnormed_Vec./max(patchAct_Unnormed_Vec(:)));
        
        correctlyIdentified = categoryResults(:,4)>0.5;
        
        trueImageCount = sum(correctlyIdentified==1);
        twpc = sum(categoryResults(correctlyIdentified==1,6));
        ttpc = sum(categoryResults(correctlyIdentified==1,5));
        wrongImageCount = sum(correctlyIdentified==0);
        wwpc = sum(categoryResults(correctlyIdentified==0,6));
        wtpc = sum(categoryResults(correctlyIdentified==0,5));
        
        %patchCountCol = ceil(patchCount_All/patchCountRow);
%         rc = getAllPossibleDivisors(patchCount_All);
%         rcMed = round(length(rc)*0.5);
%         patchCountRow = rc(rcMed);
        patchCountRow = min(10,floor(sqrt(patchCount_All)));
        patchCountCol = ceil(patchCount_All/patchCountRow);
        
        mostRecognizedPatches_Cropped_Cur = zeros(patchCountRow*patchRC_Box, patchCountCol*patchRC_Box, 3);
        %mostRecognizedPatches_Highlighted_Cur = zeros(patchCountRow*patchRC_Box, patchCountCol*patchRC_Box, 3);
        
        figure(1);clf;
        for patchID = 1:patchCount_All
            [ri,ci] = ind2sub([patchCountRow patchCountCol],patchID);
            imageID = categoryResults(patchID,1);
            %1. Get the filter and the image to work on
            mostRecognizedArea_Cur = mostRecognizedAreas(:,:,imageID);
            mostActMax = max(mostRecognizedArea_Cur(:));
            mostRecognizedArea_Cur = mostRecognizedArea_Cur./mostActMax;
            rgbImCur = data(:,:,:,imageID);
            %2. Create the highlighted image
            %mostRecognizedArea_Cur_3D = repmat(mostRecognizedArea_Cur,[1 1 3]);
            %rgbIm_ToResize = mostRecognizedArea_Cur_3D.*rgbImCur(:,:,1:3);
            %rgbIm_ToResize = imresize(rgbIm_ToResize,[patchRC_Box patchRC_Box]);
            
            %3. Find the rectangle of that max area
            [rBest, cBest] = find(mostRecognizedArea_Cur==max(mostRecognizedArea_Cur(:)));
            rBest = round(mean(rBest));
            cBest = round(mean(cBest));
            patchAct = patchAct_Unnormed_Vec(patchID);%unnormedMaxActivation
            rBest_Top = round(max(1,rBest-(patchRC/2)));
            cBest_Left = round(max(1,cBest-(patchRC/2)));
            rBest_Bot = rBest_Top+patchRC-1;
            cBest_Right = cBest_Left+patchRC-1;
            rgbIm_Cropped = rgbImCur(rBest_Top:rBest_Bot,cBest_Left:cBest_Right,1:3);
            zeroPixels = sum(rgbIm_Cropped,3);
            zeroPixels = find(zeroPixels(:)==0);
            if ~isempty(zeroPixels)
                for j=1:3
                    rgbIm_Cropped(zeroPixels+6400*(j-1)) = additionalInfo.P04_meanOfData(j);
                end
            end
            
            mostRecognizedPatchesReturn(:,:,:,imageID) = rgbIm_Cropped;
            
            %4. Highlighted
            rBigImSt = patchRC_Box*(ri-1) + 1;
            cBigImSt = patchRC_Box*(ci-1) + 1;
            rBigImEn = rBigImSt + patchRC_Box-1;
            cBigImEn = cBigImSt + patchRC_Box-1;
            %mostRecognizedPatches_Highlighted_Cur(rBigImSt:rBigImEn,cBigImSt:cBigImEn,:) = rgbIm_ToResize*patchAct;
            
            
            %5. Background color for cropped image
            %The back color can be 
            %Red - Image classified as wrong
            %Yellow - Image classified as a liitle wrong a liitle right
            %Green - Image Classified correctly
            correctPatchGuessCount = categoryResults(patchID,5);
            wrongPatchGuessCount = categoryResults(patchID,6);
            %backGround_Dif = round(0.5*patchRC_Dif);
            patchCountTotal = correctPatchGuessCount+wrongPatchGuessCount;
            backGround_RatioVal = min(correctPatchGuessCount, wrongPatchGuessCount)/patchCountTotal;%
            backGround_Dif = round(minThickness + (1-backGround_RatioVal)*allowedThickness);
            
            if patchID<=patchCountRow*patchCountCol
                rBigImSt = rBigImSt + patchRC_Dif - backGround_Dif;
                rBigImEn = rBigImEn - patchRC_Dif + backGround_Dif;
                cBigImSt = cBigImSt + patchRC_Dif - backGround_Dif;
                cBigImEn = cBigImEn - patchRC_Dif + backGround_Dif;
                if (correctlyIdentified(patchID))
                    %correctPatchGuessCount>=13
                    %wrongPatchGuessCount<=11
                    mostRecognizedPatches_Cropped_Cur(rBigImSt:rBigImEn,cBigImSt:cBigImEn,1) = backGround_RatioVal;%Red
                    mostRecognizedPatches_Cropped_Cur(rBigImSt:rBigImEn,cBigImSt:cBigImEn,2) = 1;%Green
                    mostRecognizedPatches_Cropped_Cur(rBigImSt:rBigImEn,cBigImSt:cBigImEn,3) = 1-backGround_RatioVal;%Blue
                else
                    %correctPatchGuessCount<=12
                    %wrongPatchGuessCount>=12
                    mostRecognizedPatches_Cropped_Cur(rBigImSt:rBigImEn,cBigImSt:cBigImEn,1) = 1;%Red
                    mostRecognizedPatches_Cropped_Cur(rBigImSt:rBigImEn,cBigImSt:cBigImEn,2) = backGround_RatioVal;%Green
                    %mostRecognizedPatches_Cropped_Cur(rBigImSt:rBigImEn,cBigImSt:cBigImEn,3) = backGround_RatioVal;%Blue
                end
                %6. Cropped image
                rBigImSt = patchRC_Box*(ri-1) + patchRC_Dif;
                cBigImSt = patchRC_Box*(ci-1) + patchRC_Dif;
                rBigImEn = rBigImSt + patchRC-1;
                cBigImEn = cBigImSt + patchRC-1;
                mostRecognizedPatches_Cropped_Cur(rBigImSt:rBigImEn,cBigImSt:cBigImEn,:) = rgbIm_Cropped*patchAct;
            else
                disp(['Skipping visualization of ' num2str(patchID)])
            end
        end
        
        if ~isempty(imageIDResults_AnalyzeMat_CurIt)
            assert(wrongImageCount == imageIDResults_AnalyzeMat_CurIt(c,5),'wrongImageCount is wrong');
            assert(wwpc == imageIDResults_AnalyzeMat_CurIt(c,6),'wwpc is wrong');
            assert(wtpc == imageIDResults_AnalyzeMat_CurIt(c,7),'wtpc is wrong');
            assert(trueImageCount == imageIDResults_AnalyzeMat_CurIt(c,8),'trueImageCount is wrong');
            assert(twpc == imageIDResults_AnalyzeMat_CurIt(c,9),'twpc is wrong');
            assert(ttpc == imageIDResults_AnalyzeMat_CurIt(c,10),'ttpc is wrong');
        end
        
        titleInfoStr_01 = ['True Image Count(' num2str(trueImageCount) '/' num2str(patchCount_All) ')-truePatches(' num2str(ttpc) ')-wrongPatches(' num2str(twpc) ')'];
        titleInfoStr_02 = ['Wrong Image Count(' num2str(wrongImageCount) '/' num2str(patchCount_All) ')-truePatches(' num2str(wtpc) ')-wrongPatches(' num2str(wwpc) ')'];
        trueImagePerc = 100*trueImageCount/(trueImageCount+wrongImageCount);
        titleInfoStr_00 = ['mostRecognizedPatches-Cropped - c(' num2str(c,'%02d') ')-TrueImagePerc(%' num2str(trueImagePerc,'%4.2f') ')'];
        
        h = figure(1);clf;
        imshow(mostRecognizedPatches_Cropped_Cur);
        title(titleInfoStr_00);
        if ~isempty(titleInfoStr_01)
            xlabel(titleInfoStr_01);
        end
        if ~isempty(titleInfoStr_02)
            ylabel(titleInfoStr_02);
        end
        %hold on;
        %XTickInts = round(patchRC_Box/2):patchRC_Box:patchCountRow*patchRC_Box;
        %plot(XTickInts, zeros(size(XTickInts)));
        %set(gca,'XTick',XTickInts);
        %xlabel({titleInfoStr_01,titleInfoStr_02});
        %figure(2);clf;
        %imshow(mostRecognizedPatches_Highlighted_Cur); 
        %titleInfoStr_00 = ['mostRecognizedPatches-Highlighted - c(' num2str(c,'%02d') ')-TrueImagePerc(%' num2str(trueImagePerc,'%4.2f') ')'];
        %title({titleInfoStr_00,titleInfoStr_01,titleInfoStr_02});
        %mostRecognizedPatches_Cropped(:,:,:,c) = mostRecognizedPatches_Cropped_Cur;
        %mostRecognizedPatches_Highlighted(:,:,:,c) = mostRecognizedPatches_Highlighted_Cur;
        
        %figureFileNameDraft = [figureFolderName 'Train_Batch(' num2str(batchID,'%02d') ')_Letter(NUM2STRLETTER)_Iter(' num2str(iter) ').png'];
        figureFileName = strrep(figureFileNameDraft,'NUM2STRLETTER',num2str(c,'%02d'));
        saveas(h,figureFileName);
    end
    warning('on', 'Images:initSize:adjustingMag');
end

