function learnFocused_V01(learningVector, patchSize, strideParam, sizHid, sizHid2)
%learnFocused_V01 learn the weights of the training set using focused sets

%e.g. learnFocused_V01([110 211 320], [60 60], [4 4], 700, [])

%learningVector is a 3 dimension vector
%every dimension has information about which way to go in the 3 main steps
%below
%e.g. [110 211 320] :
%110 : learn convolution params by sparse autoencoder
%211 : mean pool 
%320 : use ELM for softmax weights

%%1. Learning first convolution layer params
% 1.1.after loading all, grab the  center patchSize(e.g. [60 60])

    rng(10);
    global bigDataPath;
    displayOpts = setDisplayOpts('all');
    saveOpts = setSaveOpts('minimal', bigDataPath);
    saveOpts.Figures.Results = 'Every';%'None','Last','Every'
    
    [dataStruct, batchInfo] = createInitialDataStruct_surray('focus');
    inputChannelSize = dataStruct.singleImageShape(3);
    
    normalizedPatchInit_fileName = [dataStruct.loadFolder 'focus_' mat2str(patchSize) '_all_normed.mat'];
    if exist(normalizedPatchInit_fileName,'file')
        disp(['Loading(' normalizedPatchInit_fileName ')'])
        load(normalizedPatchInit_fileName,'data','labels','additionalInfo');
    else
        centeredPatchInit_fileName = [dataStruct.loadFolder 'focus_' mat2str(patchSize) '_all.mat'];
        if exist(centeredPatchInit_fileName,'file')
            disp(['Loading(' centeredPatchInit_fileName ')'])
            load(centeredPatchInit_fileName,'data','labels');
        else
            batchCount = batchInfo.batch_Count_Train;
            totalImageCount = sum(batchInfo.image_Count_Matrix_Train(:));
            data_all = zeros([patchSize inputChannelSize totalImageCount]);
            labels_all = zeros([1 totalImageCount]);
            fr = 1;
            for b = 1:batchCount
                %1. Load batchData
                dataStruct.updateDataStruct(dataStruct, {'batchID_current', b});
                load(dataStruct.fileName_current,'data','labels');
                batchImageCount = length(labels); %#ok<NODEF>
                data = grabCenters(data, patchSize);
                to = fr+batchImageCount-1;
                disp(['Assigning batchID(' num2str(b) ') of ' num2str(batchImageCount) ' images to data_all[' num2str(fr) ':' num2str(to) ']']);
                data_all(:,:,:,fr:to) = data;
                labels_all(1,fr:to) = labels;
                fr = to+1;
            end
    % 1.2.accumulate all into 1 array
            data = data_all;
            labels = labels_all;
            clear data_all labels_all;
            disp(['Saving(' centeredPatchInit_fileName ')'])
            save(centeredPatchInit_fileName,'data','labels','-v7.3');
        end    
        countSamples = length(labels);
        sizeChn = size(data,3);
% 1.3.find mean and subtract from all
% 1.4.find std and divide all
        [data, additionalInfo] = normalizeInputData(data, patchSize, countSamples, sizeChn);
        disp(['Saving(' normalizedPatchInit_fileName ')'])
        save(normalizedPatchInit_fileName,'data','labels','additionalInfo','-v7.3');
    end
    

% 1.5.learn convolution params by
    acquirePatchesFunc = @(X_) acquirePatchesFunc_surray(X_, 'method', 'assignDirectly');
    patchCount = length(labels);
    sizeChn = size(data,3);
    elmParamTypeStr = 'CIW';%'Standard','CIW','Constrained'
    switch learningVector(1)
        case 110
        % 1.5.a. sparse autoencoder [learningVector(1)==110]
            weightParamName_01_initConvFilt = [bigDataPath 'paramsLearnt\weight01_sparseAE_LV(' num2str(learningVector(1)) ')_sizHid(' num2str(sizHid) ').mat'];
            if exist(weightParamName_01_initConvFilt,'file')
                disp('Loading initial convolution weight params learnt by ELM autoencoder.');
                load(weightParamName_01_initConvFilt,'sizeVis', 'meanPatch', 'ZCA_p', 'W', 'b','paramsAutoEncode','paramsMinFunc')
            else
                disp('Learning initial convolution weight params by using sparse autoencoder.');
                paramsMinFunc = setParams_minFunc('showInfo_noLog');
                paramsAutoEncode = struct;
                paramsAutoEncode = setParams_Missing_Autoencoder(paramsAutoEncode);
                vararginStruct = {'paramsMinFunc', paramsMinFunc, 'paramsAutoEncode', paramsAutoEncode, 'acquirePatchesFunc', acquirePatchesFunc};
                [sizeVis, meanPatch, ZCA_p, W, b] = learnPatches( ...
                    patchCount,  ...
                    patchSize, ...convFiltDim
                    sizHid,   ...countFilt
                    data,        ...
                    sizeChn,     ...
                    vararginStruct{:});%,  paramsELM, displayOpts, saveOpts           
                disp('Saving initial convolution weight params learnt by sparse autoencoder.');
                save(weightParamName_01_initConvFilt,'sizeVis', 'meanPatch', 'ZCA_p', 'W', 'b','paramsAutoEncode','paramsMinFunc','-v7.3')
            end
        case 120
        % 1.5.b. ELM autoencoder [learningVector(1)==120]
            weightParamName_01_initConvFilt = [bigDataPath 'paramsLearnt\weight01_LV(' num2str(learningVector(1)) ')_sizHid(' num2str(sizHid) ')_ELM(' elmParamTypeStr ').mat'];
            if exist(weightParamName_01_initConvFilt,'file')
                disp('Loading initial convolution weight params learnt by ELM autoencoder.');
                load(weightParamName_01_initConvFilt,'sizeVis', 'meanPatch', 'ZCA_p', 'W', 'b','paramsELM')
            else
                disp('Learning initial convolution weight params by using ELM autoencoder.');
                paramsELM = setParams_ELM('Constrained');%'Constrained','CIW'
                vararginStruct = {'paramsELM', paramsELM, 'acquirePatchesFunc', acquirePatchesFunc};
                [sizeVis, meanPatch, ZCA_p, W, b] = learnPatches( ...
                                                                  patchCount,  ...
                                                                  patchSize, ...convFiltDim
                                                                  sizHid,   ...countFilt
                                                                  data,        ...
                                                                  sizeChn,     ...
                                                                  vararginStruct{:});%,  paramsELM, displayOpts, saveOpts           
                disp('Saving initial convolution weight params learnt by ELM autoencoder.');
                save(weightParamName_01_initConvFilt,'sizeVis', 'meanPatch', 'ZCA_p', 'W', 'b','paramsELM','-v7.3')
            end
        case 130
        % 1.5.c. ELM classifier - Constrained [learningVector(1)==130]
            weightParamName_01_initConvFilt = [bigDataPath 'paramsLearnt\weight01_LV(' num2str(learningVector(1)) ')_sizHid(' num2str(sizHid) ')_ELMClassifier(' elmParamTypeStr ').mat'];
            if exist(weightParamName_01_initConvFilt,'file')
                disp('Loading initial convolution weight params learnt by ELM classifier.');
                load(weightParamName_01_initConvFilt,'sizeVis', 'meanPatch', 'ZCA_p', 'W', 'b','paramsELM','Welm')
            else
                disp('Learning initial convolution weight params by using ELM classifier.');
                paramsELM = setParams_ELM('Constrained');%'Constrained','CIW'             
                [data, sizeVis] = acquirePatchesFunc(data);
                [data, meanPatch] = meanNormalizePatches(data);
                [data, ZCA_p] = ZCA_whiten_patches(data, [], true, true);
                
                [categoryHistogram, categoryLabels] = hist(labels,unique(labels));
                minElementCount = min(categoryHistogram);
                elmData = data;
                elmLabels = labels;
                for c = categoryLabels
                    curLabelDataInds = find(elmLabels==c);
                    curLabelElementCount = categoryHistogram(c);
                    assert(curLabelElementCount==length(curLabelDataInds),'labels are problematic');
                    curLabelDataIndsScrambled = curLabelDataInds(randperm(curLabelElementCount));
                    curLabelDataIndsToDelete = curLabelDataIndsScrambled(minElementCount+1 : end);
                    elmData(:,curLabelDataIndsToDelete) = [];
                    elmLabels(:,curLabelDataIndsToDelete) = [];   
                end
                categoryHistogramNew = hist(elmLabels,unique(elmLabels));
                assert(sum(categoryHistogramNew-mean(categoryHistogramNew))==0,'histogram not normalized!!');
                clear c categoryLabels curLabelDataInds curLabelElementCount curLabelDataIndsScrambled curLabelDataIndsToDelete minElementCount
                
                [accuracyCalced_HistNormed, predictedLabels_HistNormed, W_randoms_with_bias, Welm, a_HistNormed, confMatResult_HistNormed] = ELM_training(elmData, elmLabels, 'paramsELM', paramsELM, 'sizeHid', sizHid);
                [predictedLabels, accuracyCalced, confMatResult, ~, ~, a] = analyzeResults(data, labels, W_randoms_with_bias, Welm, true );
                
                W = W_randoms_with_bias(:,1:end-1);
                b = W_randoms_with_bias(:,end);
                save(weightParamName_01_initConvFilt,'sizeVis', 'meanPatch', 'ZCA_p', 'W', 'b','paramsELM','Welm')
                
                data = a;
                clear accuracyCalced_HistNormed predictedLabels_HistNormed a_HistNormed confMatResult_HistNormed
                clear a categoryHistogram categoryLabels minElementCount elmData elmLabels categoryHistogramNew
            end
        case 140
        % 1.5.c. ELM classifier - CIW [learningVector(1)==140]
                disp('Learning initial convolution weight params by using ELM classifier.');
                paramsELM = setParams_ELM('CIW');
                paramsELM.inputChannelSize = 3;
                paramsELM.rf.minMaskSize = round(patchSize(1)*0.55);%parameter for  receptive fields(e.g. 10)
                paramsELM.rf.border = 3;%parameter for receptive fields(e.g. 3)                
                [data, sizeVis] = acquirePatchesFunc(data);
                [data, meanPatch] = meanNormalizePatches(data);
                [data, ZCA_p] = ZCA_whiten_patches(data, [], true,true);
                
                %deleted code 02
                
                %here I will try increasing the node count,
                %when accuracy gain is less then a treshold - I will stop
                sizHidOptions = [24*5:48:24*21 700];
                accAll = zeros(1,length(sizHidOptions));
                
                for i = 1:length(sizHidOptions)
                    sizHidCur = sizHidOptions(i);
                    weightParamName_01_initConvFilt = [bigDataPath 'paramsLearnt\weight01_LV(' num2str(learningVector(1)) ')_sizHid('  num2str(sizHidCur,'%03d') ')_ELMClassifier(' elmParamTypeStr ').mat'];
                    if exist(weightParamName_01_initConvFilt,'file')
                        disp('Loading initial convolution weight params learnt by ELM classifier.');
                        load(weightParamName_01_initConvFilt,'sizeVis', 'meanPatch', 'ZCA_p', 'W', 'b','paramsELM','Welm','curAcc','confMatResult_Initial')
                    else
                        [curAcc, predictedLabels_Initial, W_randoms_with_bias, Welm, ~, confMatResult_Initial] = ELM_training(data, labels, 'paramsELM', paramsELM, 'sizeHid', sizHidCur);
                        W = W_randoms_with_bias(:,1:end-1);
                        b = W_randoms_with_bias(:,end);
                        save(weightParamName_01_initConvFilt,'sizeVis', 'meanPatch', 'ZCA_p', 'W', 'b','paramsELM','Welm','curAcc','confMatResult_Initial')
                    end
                    accAll(i) = curAcc;
                    disp(['Hidden Size(' num2str(sizHidCur) '),Accuracy(' num2str(curAcc) '), confMat :'])
                    disptable(confMatResult_Initial);
                end
                
                [maxAccVals, maxAccInds] = sort(accAll,'descend');
                
                selectedInd = maxAccInds(1);
                sizHid = sizHidOptions(selectedInd);
                weightParamName_01_initConvFilt = [bigDataPath 'paramsLearnt\weight01_LV(' num2str(learningVector(1)) ')_sizHid('  num2str(sizHid,'%03d') ')_ELMClassifier(' elmParamTypeStr ').mat'];
                load(weightParamName_01_initConvFilt,'sizeVis', 'meanPatch', 'ZCA_p', 'W', 'b','paramsELM','Welm','curAcc','confMatResult_Initial')
                [predictedLabels_All, accAll, confMatResult_All] = analyzeResults(data, labels, [W b], Welm, true );
        case 150
            %here we will use the input patches as vectorized data
            %learn a fully connected network and use the initial layer
            %optimized weights  
            
            weightParamName_01_initConvFilt = [bigDataPath 'paramsLearnt\weight01_LV(' num2str(learningVector(1)) ')_sizHid('  num2str(sizHid,'%03d') ')_softmaxInitLayerOptimization.mat'];
            if exist(weightParamName_01_initConvFilt,'file')
                disp('Loading initial convolution weight params learnt by ELM classifier.');
                load(weightParamName_01_initConvFilt,'sizeVis', 'meanPatch', 'ZCA_p', 'cnnnet_layer01','curAcc','confMatResult_Initial')
            else
                cnnnet_layer01 = appendNetworkLayer( [], 'fullyconnected', displayOpts, 'sizeHid', sizHid, 'initializeWeightMethod', 'Random', 'activationType', 'sigmoid', 'randInitMode', 'Glorot_Bengio' );%Xavier, Glorot_Bengio, He_Rang_Zhen
                cnnnet_layer01 = appendNetworkLayer( cnnnet_layer01, 'activation', displayOpts, 'activationType', 'sigmoid');
                cnnnet_layer01 = appendNetworkLayer( cnnnet_layer01, 'softmax', displayOpts, 'initializeWeightMethod', 'Random', 'minFuncMaxIter', 10 );
                tempFolder = saveOpts.MainFolder;
                saveOpts.MainFolder = [tempFolder 'Train\'];%[];%
                if ~isempty(saveOpts.MainFolder) && ~exist(saveOpts.MainFolder,'dir')
                    if (displayOpts.fileOperations==1)
                        disp([displayOpts.ies{2} 'folder(' saveOpts.MainFolder ') is created as tempFolder for training operations'])
                    end
                    mkdir(saveOpts.MainFolder);
                elseif (displayOpts.fileOperations==1)
                        disp([displayOpts.ies{2} 'folder(' saveOpts.MainFolder ') was already created and will be used as tempFolder for training operations'])
                end

                [data, sizeVis] = acquirePatchesFunc(data);
                [data, meanPatch] = meanNormalizePatches(data);
                [data, ZCA_p] = ZCA_whiten_patches(data, [], true, true);
                [ cnnnet_layer01, ~, curAcc, confMatResult_Initial ] = forwardPropogateDeepNet( data, labels, cnnnet_layer01 ...
                                                                                          ,'calculateBackPropMap', true ...
                                                                                          ,'displayOpts', displayOpts ...
                                                                                          ,'saveOpts', saveOpts);   
                paramsMinFunc = setParams_minFunc();
                [ cnnnet_layer01, output] = trainCNNBackProp( [], data, labels, cnnnet_layer01, displayOpts, saveOpts, paramsMinFunc,2);
                saveOpts.MainFolder = tempFolder;
                save(weightParamName_01_initConvFilt,'sizeVis', 'meanPatch', 'ZCA_p', 'cnnnet_layer01','curAcc','confMatResult_Initial','-v7.3')
            end
        otherwise
        %case 150
        % 1.5.e. randomly initialize weights and learn softmax [learningVector(1)==150]
            disp('Learning initial convolution weight params by randomly initializing weights and learn softmax.');
            error('not implemented yet');
    end
    cnnnet = appendNetworkLayer( [], 'convolution', displayOpts, 'countFilt', sizHid, 'inputChannelSize', inputChannelSize, 'filterSize', patchSize, 'stride', strideParam, 'initializeWeightMethod', 'Random');
    cnnnet = appendNetworkLayer( cnnnet, 'activation', displayOpts, 'activationType', 'sigmoid');
    cnnnet{1,1}.filterParams.meanPatches = meanPatch;
    cnnnet{1,1}.filterParams.ZCA = ZCA_p;
    if learningVector(1)==150
%         cnnnet{1,1}.filterParams.W = cnnnet_layer01{1,1}.filtersWhitened;
%         cnnnet{1,1}.filterParams.b = cnnnet_layer01{1,1}.biasesWhitened;
        cnnnet{1,1}.filtersWhitened = cnnnet_layer01{1,1}.filtersWhitened; % The convolution need to be done by the whitened features
        cnnnet{1,1}.biasesWhitened = cnnnet_layer01{1,1}.biasesWhitened; %This should be added for all convolved feature        
        clear cnnnet_layer01 curAcc confMatResult_Initial
    else
        cnnnet{1,1}.filterParams.W = W;
        cnnnet{1,1}.filterParams.b = b;
        cnnnet{1,1}.filtersWhitened = (cnnnet{1,1}.filterParams.W*cnnnet{1,1}.filterParams.ZCA); % The convolution need to be done by the whitened features
        cnnnet{1,1}.biasesWhitened = cnnnet{1,1}.filterParams.b - cnnnet{1,1}.filtersWhitened*cnnnet{1,1}.filterParams.meanPatches; %This should be added for all convolved feature
    end
    clear sizeVis meanPatch ZCA_p b
    disp('initial convolution filters are learnt')


%%2. Use the convolution filters, convolve the data using stride param given
%%and get all the data ready for pooling or convolution
    convolvedPatchesSecond_fileName = [dataStruct.loadFolder 'focus_' mat2str(patchSize) '_convolved_L1(' num2str(learningVector(1)) ')_sizHid(' num2str(sizHid) ').mat'];
    if exist(convolvedPatchesSecond_fileName,'file')
        disp(['Loading(' convolvedPatchesSecond_fileName ')'])
        load(convolvedPatchesSecond_fileName,'data','labels');
        cnnnet{1, 1}.inputSize_original = [dataStruct.singleImageShape size(data,4)];
        cnnnet{1, 1}.outputSize_original = size(data);
        cnnnet{2, 1}.inputSize_original = size(data);
        cnnnet{2, 1}.outputSize_original = size(data);
    else
        [cnnnet, data, labels] = initialConvolution(cnnnet, dataStruct, batchInfo, patchSize, strideParam, sizHid, additionalInfo, displayOpts);
% 1.2.accumulate all into 1 array
        disp(['Saving(' convolvedPatchesSecond_fileName ')'])
        save(convolvedPatchesSecond_fileName,'data','labels','-v7.3');
    end

    switch learningVector(2)
        % 2.1.Pooling
        case 211
        % 2.1.a. mean pool [learningVector(2)==211]
            poolSize = [size(data,1) size(data,2)];
            countImages = size(data,4);
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', poolSize, 'poolType', 'mean' );
            data = reshape(data,[],countImages);
            [data, cnnnet] = pooling_ForwardPropogate( cnnnet, 3, data,'displayOpts' , displayOpts,'activationID', 2,'memoryMethod', 'all_in_memory','calculateBackPropMap', false);
        case 212
        % 2.1.b. max pool [learningVector(2)==212]
            poolSize = [size(data,1) size(data,2)];
            countImages = size(data,4);
            cnnnet = appendNetworkLayer( cnnnet, 'pooling', displayOpts, 'poolSize', poolSize, 'poolType', 'max' );
            data = reshape(data,[],countImages);
            [data, cnnnet] = pooling_ForwardPropogate( cnnnet, 3, data,'displayOpts' , displayOpts,'activationID', 2,'memoryMethod', 'all_in_memory','calculateBackPropMap', false);
        % 2.2.Convolution - learn sizHid2 filters using (sizHid dimensional 5 by 5) data (if sizHid2 is given and bigger than 0)
        case 221
        % 2.2.a. sparse autoencoder [learningVector(2)==221]
            error('not implemented yet');
        
        case 222
        % 2.2.b. ELM autoencoder [learningVector(2)==222]
            error('not implemented yet');

        case 223
        % 2.2.c. ELM classifier [learningVector(2)==223]
            error('not implemented yet');

        case 224
        % 2.2.d. randomly initialize weights and learn softmax [learningVector(2)==224]
            error('not implemented yet');
    end


%%3. Use the output of previous steps using all data and learn softMax
%%weights

    weightParamName_03_finalCNN = [bigDataPath 'paramsLearnt\weight03_LV' mat2str(learningVector) '_sizHid(' num2str(sizHid) ').mat'];
    switch learningVector(3)
        case 310
        % 3.1. using softmaxLearn [learningVector(2)==310]
            if ~exist(weightParamName_03_finalCNN,'file')
                cnnnet = appendNetworkLayer( cnnnet, 'softmax', displayOpts, 'initializeWeightMethod', 'Random' );
                paramsMinFunc = setParams_minFunc();
                paramsMinFunc.maxIter = 400;
                cnnnet{4,1}.softMaxParams.paramsMinFunc = paramsMinFunc;
                [~, cnnnet] = softMax_ForwardPropogate( cnnnet, 4, data, 'displayOpts', displayOpts, 'memoryMethod', 'all_in_memory', 'labels' , labels, 'saveOpts', saveOpts);
                if isfield(cnnnet{2, 1},'activatedData')
                    cnnnet{2, 1} = rmfield(cnnnet{2, 1},'activatedData');
                end             
                save(weightParamName_03_finalCNN,'cnnnet','-v7.3');
            else
                disp(['Loading(' weightParamName_03_finalCNN ')'])
                load(weightParamName_03_finalCNN,'cnnnet');
            end
            %cnnnet_opt = optimize_SoftMaxLayer(cnnnet, data, labels, displayOpts, [], 200);
            cnnnet_minor = cnnnet;
            iterResults = zeros(20,7);
            for i=1:20
                %use the pooled data and check which features and weights can be omitted
                [cnnnet_minor, resultMat, featIndsSorted, featIDsToDel, accuracyCalced_Init, expectedAccuracy] = reduceLastLayerWeights( cnnnet_minor, data, labels, 1.5);
                %find the new hidden size
                sizHidOld = length(featIndsSorted);
                sizHid_minor = sizHidOld-length(featIDsToDel);
                %find the save name
                weightParamName_03_cnnnet_minor_iter = [bigDataPath 'paramsLearnt\weight03_LV' mat2str(learningVector) '_sizHid(' num2str(sizHid) ')_minorIt(' num2str(sizHid_minor) ').mat'];
                if ~exist(weightParamName_03_cnnnet_minor_iter,'file')
                    %Remove the theta params from softmax and learn the final layer again
                    if isfield(cnnnet_minor{4, 1},'theta')
                        cnnnet_minor{4, 1} = rmfield(cnnnet_minor{4, 1},'theta');
                    end
                    [accuracyCalced_cnnet_minor, data, cnnnet_minor] = runCNNet(cnnnet_minor, dataStruct, batchInfo, patchSize, strideParam, sizHid_minor, additionalInfo, displayOpts, saveOpts, convolvedPatchesSecond_fileName);
                    if isfield(cnnnet_minor{2, 1},'activatedData')
                        cnnnet_minor{2, 1} = rmfield(cnnnet_minor{2, 1},'activatedData');
                    end
                    disp(['Accuracy is ' num2str(accuracyCalced_cnnet_minor) ' for hidden size' num2str(sizHid_minor) ' in iteration' num2str(i) '']);
                    save(weightParamName_03_cnnnet_minor_iter,'cnnnet_minor','data','accuracyCalced_cnnet_minor','resultMat','featIndsSorted','featIDsToDel','accuracyCalced_Init','-v7.3');
                else
                    load(weightParamName_03_cnnnet_minor_iter,'cnnnet_minor','data','accuracyCalced_cnnet_minor','resultMat','featIndsSorted','featIDsToDel','accuracyCalced_Init');
                end
                iterResults(i,:) = [i sizHidOld sizHid_minor sizHidOld-sizHid_minor expectedAccuracy accuracyCalced_cnnet_minor accuracyCalced_cnnet_minor-expectedAccuracy];
                disptable(iterResults(1:i,:),{'i','inFeat','newFeat','difFeat','expAcc','calcedAcc','difAcc'},[]);
                disp(['New feat count(' num2str(sizHid_minor) ',hidSizeDif(' num2str(sizHidOld-sizHid_minor) ')']);
                disp(['ExpectedAcc(' num2str(expectedAccuracy,'%4.2f') '), foundAcc(' num2str(expectedAccuracy,'%4.2f') '),accDif(' num2str(expectedAccuracy,'%4.2f') ')']);
            end

%             sizHid = 700;
%             sizHid_minor = 452;
%             weightParamName_03_cnnnet_minor_iter = [bigDataPath 'paramsLearnt\weight03_LV' mat2str(learningVector) '_sizHid(' num2str(sizHid) ')_minorIt(' num2str(sizHid_minor) ').mat'];
%             load(weightParamName_03_cnnnet_minor_iter,'cnnnet_minor','data','accuracyCalced_cnnet_minor','resultMat','featIndsSorted','featIDsToDel','accuracyCalced_Init');
            
            resultName_03_finalCNN = [bigDataPath 'paramsLearnt\result03_test_LV' mat2str(learningVector) '_sizHid(' num2str(sizHid_minor) ').mat'];
            if ~exist(resultName_03_finalCNN,'file') 
                [cnnnet_test, labels_test, predictions_test, selectedLeftTopMostPixelIDs_test, confMat_test] = runCNNET_ValidTest(cnnnet_minor, patchSize, additionalInfo, 'test');
                save(resultName_03_finalCNN,'cnnnet_test','labels_test','predictions_test','selectedLeftTopMostPixelIDs_test','confMat_test','-v7.3');
            end
            resultName_03_finalCNN = [bigDataPath 'paramsLearnt\result03_valid_LV' mat2str(learningVector) '_sizHid(' num2str(sizHid_minor) ').mat'];
            if ~exist(resultName_03_finalCNN,'file') 
                [cnnnet_valid, labels_valid, predictions_valid, selectedLeftTopMostPixelIDs_valid, confMat_valid] = runCNNET_ValidTest(cnnnet_minor, patchSize, additionalInfo, 'valid');
                save(resultName_03_finalCNN,'cnnnet_valid','labels_valid','predictions_valid','selectedLeftTopMostPixelIDs_valid','confMat_valid','-v7.3');
            end
            
        case 311
        % 3.1. using softmaxLearn [learningVector(2)==311]
            if ~exist(weightParamName_03_finalCNN,'file')
                cnnnet = appendNetworkLayer( cnnnet, 'softmax', displayOpts, 'initializeWeightMethod', 'Random' );
                paramsMinFunc = setParams_minFunc();
                paramsMinFunc.maxIter = 400;
                cnnnet{4,1}.softMaxParams.paramsMinFunc = paramsMinFunc;
                [~, cnnnet] = softMax_ForwardPropogate( cnnnet, 4, data, 'displayOpts', displayOpts, 'memoryMethod', 'all_in_memory', 'labels' , labels, 'saveOpts', saveOpts);
                if isfield(cnnnet{2, 1},'activatedData')
                    cnnnet{2, 1} = rmfield(cnnnet{2, 1},'activatedData');
                end             
                save(weightParamName_03_finalCNN,'cnnnet','-v7.3');
            else
                disp(['Loading(' weightParamName_03_finalCNN ')'])
                load(weightParamName_03_finalCNN,'cnnnet');
            end
            dataTypeStr = 'train';
            evalStr = ['[cnnnet_' dataTypeStr ', labels_' dataTypeStr ', predictions_' dataTypeStr ', selectedLeftTopMostPixelIDs_' dataTypeStr '] = runCNNET_ValidTest(cnnnet, patchSize, additionalInfo, "' dataTypeStr '");'];
            eval(evalStr);
           %[cnnnet_train, labels_train, predictions_train, selectedLeftTopMostPixelIDs_train] = runCNNET_ValidTest(cnnnet, patchSize, additionalInfo, 'test');
            
        case 320
        % 3.2. ELM [learningVector(2)==320]
            error('not implemented yet');

        % 2.2.Convolution - learn sizHid2 filters using (sizHid dimensional 5 by 5) data (if sizHid2 is given and bigger than 0)
        otherwise
            error(['Unknown learning setup' mat2str(learningVector(3))]);
    end
end

function [accuracyCalced_cnnet, pooledData, cnnnet] = runCNNet(cnnnet, dataStruct, batchInfo, patchSize, strideParam, sizHid, additionalInfo, displayOpts, saveOpts, convolvedPatchesSecond_fileName)
    %disp(['Loading(' convolvedPatchesSecond_fileName ')'])
    %load(convolvedPatchesSecond_fileName,'data','labels');
    [cnnnet, data, labels] = initialConvolution(cnnnet, dataStruct, batchInfo, patchSize, strideParam, sizHid, additionalInfo, displayOpts);
    data = reshape(data,[],size(data,4));
    [data, cnnnet] = pooling_ForwardPropogate( cnnnet, 3, data,'displayOpts' , displayOpts,'activationID', 2,'memoryMethod', 'all_in_memory','calculateBackPropMap', false);
    [~, cnnnet] = softMax_ForwardPropogate( cnnnet, 4, data, 'displayOpts', displayOpts, 'memoryMethod', 'all_in_memory', 'labels' , labels, 'saveOpts', saveOpts);
    layerCount = size(cnnnet,1);
    error('AnalyzeResults needs to have bias params involved');
    W_opt = cnnnet{layerCount,1}.Weight;
    [~, accuracyCalced_cnnet] = analyzeResults([], labels, [], W_opt, false, data);
    pooledData = data;
end

function [cnnnet, data_all, labels_all] = initialConvolution(cnnnet, dataStruct, batchInfo, patchSize, strideParam, sizHid, additionalInfo, displayOpts)
    batchCount = batchInfo.batch_Count_Train;
    totalImageCount = sum(batchInfo.image_Count_Matrix_Train(:));

     rc = patchSize(1);%row size convolution
     cc = patchSize(2);%col size convolution
    rst = strideParam(1);%row stride size
    cst = strideParam(2);%col stride size
    ro = 1+(dataStruct.singleImageShape(1)-rc)/rst;%row size output
    co = 1+(dataStruct.singleImageShape(2)-cc)/cst;%col size output  


    data_all = zeros([ro co sizHid totalImageCount]);
    labels_all = zeros([1 totalImageCount]);
    fr = 1;   

    for b = 1:batchCount
        %1. Load batchData
        dataStruct.updateDataStruct(dataStruct, {'batchID_current', b});
        load(dataStruct.fileName_current,'data','labels');
        batchImageCount = length(labels); 
        sizeChn = size(data,3);

        %normalize it
        data = permute(data,[1 2 4 3]);%sizeImg,sizeImg,countSamples,sizeChn
        dataSize_permuted = size(data);
        data = reshape(data,[],sizeChn);
        for i=1:sizeChn
            %subtract mean from each channel
            data(data(:,i)~=0,i) = data(data(:,i)~=0,i)-additionalInfo.P04_meanOfData(i);
        end  
        data = bsxfun(@rdivide,data,additionalInfo.P05_stdOfData);
        data = reshape(data,dataSize_permuted);
        data = permute(data,[1 2 4 3]);%sizeImg,sizeImg,sizeChn,countSamples

        data = reshape(data,dataStruct.countFeat,[]);
        [a, cnnnet] = convolution_ForwardPropogate(cnnnet, 1, data, 'displayOpts', displayOpts, 'memoryMethod', 'all_in_memory');   
        [a, cnnnet] =  activation_ForwardPropogate(cnnnet, 2, a,'displayOpts' , displayOpts,'activationID', 1,'memoryMethod', 'all_in_memory');
        cnnnet{1, 1}.activatedData = [];

        to = fr+batchImageCount-1;
        disp(['Assigning batchID(' num2str(b) ') of ' num2str(batchImageCount) ' images to data_all[' num2str(fr) ':' num2str(to) ']']);
        data_all(:,:,:,fr:to) = reshape(a,[ro co sizHid batchImageCount]);
        labels_all(1,fr:to) = labels;
        fr = to+1;
    end
end

function data = grabCenters(data, patchSize)
    ri = size(data,1);
    ci = size(data,2);
    ro = patchSize(1);
    co = patchSize(2);
    
    rDif = ri-ro;
    cDif = ci-co;
    
    rSt = round(rDif/2)+1;
    cSt = round(cDif/2)+1;
    
    rEn = rSt + ro - 1;
    cEn = cSt + co - 1;
    
    disp(['For focusing into the images of size [' num2str(ro) ',' num2str(co) '] from [' num2str(ri) ',' num2str(ci) '], selected rows[' num2str(rSt) ':' num2str(rEn) '],cols[' num2str(cSt) ':' num2str(cEn) ']']);
    data = data(rSt:rEn,cSt:cEn,:,:); 
end

%% Results analysis
%1. 2016-06-13 12:43 A.M.
%   used function call : learnFocused_V01([120 212 310], [60 60], [5 5], 700, []);
%   120-ELM autoencoder - used for initial convolution filters learning
%   212-max pool - used as the pooling method to make 5 by 5 patches : 1 by 1
%   310-softmaxLearn - used as the final layer optimization
%   Explanation : 
%   It is obvious that the ELM autoencoder could not learn discriminitive
%   features. the input was 60 by 60 RGB data, which were grabbed from the
%   most discriminitive 80 by 80 images' centers. but autoencoder failed to
%   find the discriminitive features. Hence the activation of the result of
%   pooling layer seemed obviously uninformative. Finally, softmax learn
%   was always finding one letter as the master and predicting a single one
%   of all rather than all others. the letter was changing from one to
%   another but it was always having a bias on either one of them.

%1. 2016-06-13 01:15 A.M.
%   used function call : learnFocused_V01([130 212 310], [60 60], [5 5], 700, []);
%   130-ELM classifier - used for initial convolution filters learning
%   212-max pool - used as the pooling method to make 5 by 5 patches : 1 by 1
%   310-softmaxLearn - used as the final layer optimization
%   Explanation : 
%   ELM classifier made the same thing. It assigned letter 22 to almost all
%   data :(





















% %                 [categoryHistogram, categoryLabels] = hist(labels,unique(labels));
% %                 minElementCount = min(categoryHistogram);
% %                 minElementCount = 1711;
%                 elmData = data;
%                 elmLabels = labels;
% %                 for c = categoryLabels
% %                     curLabelDataInds = find(elmLabels==c);
% %                     curLabelElementCount = categoryHistogram(c);
% %                     assert(curLabelElementCount==length(curLabelDataInds),'labels are problematic');
% %                     curLabelDataIndsScrambled = curLabelDataInds(randperm(curLabelElementCount));
% %                     curLabelDataIndsToDelete = curLabelDataIndsScrambled(minElementCount+1 : end);
% %                     elmData(:,curLabelDataIndsToDelete) = [];
% %                     elmLabels(:,curLabelDataIndsToDelete) = [];   
% %                 end
% %                 categoryHistogramNew = hist(elmLabels,unique(elmLabels));
% %                 assert(sum(categoryHistogramNew-mean(categoryHistogramNew))==0,'histogram not normalized!!');
% %                 clear c categoryLabels curLabelDataInds curLabelElementCount curLabelDataIndsScrambled curLabelDataIndsToDelete minElementCount






%            [dSValid, batchInfValid] = createInitialDataStruct_surray('raw');
%            dSValid = dSValid.updateDataStruct(dSValid, {'type', 'valid', 'batchID_current', 1});
%            load(dSValid.fileName_current,'data','labels');
%            data = data(:,:,1:3,:);
%            possibleOutputs = calculateConvolutionOutputParams( size(data), patchSize(1), [], 6*1024, dSValid.countBatch.train);
%            cnnnet_minor_valid = getCNNETreadyForNewData( cnnnet_minor, 2, size(data));
%            cnnnet_minor_valid{1,1}.stride = [4 4];
%            cnnnet_minor_valid{3,1}.poolSize = [54 54];
%            sizHidCur = cnnnet_minor_valid{1,1}.countFilt;
%            accuracyCalced_valid = runCNNet(cnnnet_minor_valid, dSValid, batchInfValid, patchSize, [4 4], sizHidCur, additionalInfo, displayOpts, saveOpts);
           
            %now load and test the data with cnnnet and cnnnet_minor
%            accuracyCalced_cnnet = runCNNet(cnnnet, dataStruct, batchInfo, patchSize, strideParam, sizHid, additionalInfo, displayOpts, saveOpts, convolvedPatchesSecond_fileName);


