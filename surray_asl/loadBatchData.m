function [dataIdentifier, batchImageCount] = loadBatchData(batchID, dataTypeStr)
    if ~exist('dataTypeStr','var') || isempty(dataTypeStr)
        dataTypeStr = 'train';%'train','',''
    end
    global bigDataPath;
    try
        batchFileName = [bigDataPath 'dataset5\data_batch_' dataTypeStr '_' num2str(batchID,'%02d') '.mat'];
        disp(['Loading file(' batchFileName ')'])
        load(batchFileName, 'data', 'labels', 'samplesInfo_RGBD_batch');%, 'uncompressedSize'
        dataIdentifier.data = data;
        dataIdentifier.dataSize = size(data);
        dataIdentifier.labels = labels;
        dataIdentifier.samplesInfo_RGBD_batch = samplesInfo_RGBD_batch;
    catch
        disp(['There is a problem loading <' batchFileName '>. Check it out'])
        pause
    end
    batchImageCount = length(dataIdentifier.labels);
end