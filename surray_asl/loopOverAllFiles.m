function [ dataInfoStruct, dataInfo_RGB, dataInfo_Depth ] = loopOverAllFiles(mainFolder)
%loopOverAllFiles This function will read all the png filers into a mat
%array
%   Detailed

%What to do?
% 1. Loop over all images to
%   a. Create a csv file that contains the information of all input data
%   b. Find the min and max sizes of images - they are not the same size.
% 2. Loop over all images again to
%   a. Put them in a mat file such as dataset5_A_a.mat,dataset5_D_h.mat,

%mainFolder = 'G:\Data\dataset5';
personFolders = {'A';'B';'C';'D';'E'};
letters = {'a';'b';'c';'d';'e';'f';'g';'h';'i';'k';'l';'m';'n';'o';'p';'q';'r';'s';'t';'u';'v';'w';'x';'y'};

dataInfoStruct = struct;%cell(size(personFolders,1),size(letters,1));
dataInfo_RGB = [];%zeros(1,6);
dataInfo_Depth = [];%zeros(1,6);
%Columns
%1. personID
%2. letterID
%3. count within the video
%4. percentage of video position 0-start, 1-end
%5. rowSize
%6. colSize

% First round of loop
    for p=1:size(personFolders,1)
        for l = 1:size(letters,1)
            %here we are in a folder of a person
            videoFolder = [mainFolder '\'  personFolders{p,1} '\' letters{l,1} '\'];
            listing = dir(videoFolder);
            %a file is valid as long as it has both depth and rgb sample
            numberOfFiles_All = ceil((size(listing,1) - 2) / 2);
            dataInfo_RGB_current = zeros(numberOfFiles_All, 6);
            dataInfo_Depth_current = zeros(numberOfFiles_All, 6);
            r=0;%RGB
            d=0;%Depth
            for f = 3:size(listing,1)%ignoring the first 2 as . and ..
                curFileName = listing(f,1).name;
                if strncmpi(curFileName,'color',5)
                    %for COLOR
                    %check if the depth image exists too
                    deptEquivFileName = strrep([videoFolder curFileName],'color','depth');
                    if exist(deptEquivFileName,'file')
                        rgbImage = double(imread([videoFolder curFileName]))./255;
                        %reads a R by C by 3 uint image which is from 0 to 255
                        r = r+1;
                        dataInfo_RGB_current(r,:) = [p, l, r, r, size(rgbImage,1), size(rgbImage,2)];
                    else
                        warning(['depth(' deptEquivFileName ') of color(' curFileName ') not exists-hence skipping']);
                    end
                elseif strncmpi(curFileName,'depth',5)
                    %for Depth
                    %check if the depth image exists too
                    colorEquivFileName = strrep([videoFolder curFileName],'depth','color');
                    if exist(colorEquivFileName,'file')
                        depthImage = double(imread([videoFolder curFileName]))./255;
                        %reads a R by C uint image which is from 0 to 255
                        d = d+1;
                        dataInfo_Depth_current(d,:) = [p, l, d, d, size(depthImage,1), size(depthImage,2)];
                    else
                        warning(['color(' colorEquivFileName ') of depth(' curFileName ') not exists-hence skipping']);
                    end
                else
                    error(['What is this file?<' videoFolder curFileName  '>']);
                end 
            end
            %1.clear the unnnecessary lines from current depth and rgb info mats
            dataInfo_RGB_current = dataInfo_RGB_current(1:r,:);
            dataInfo_Depth_current = dataInfo_Depth_current(1:d,:);
            %2. for the 4th column divide by max value
            dataInfo_RGB_current(:,4) = (dataInfo_RGB_current(:,4)-1)./(max(dataInfo_RGB_current(:,4))-1);
            dataInfo_Depth_current(:,4) = (dataInfo_Depth_current(:,4)-1)./(max(dataInfo_Depth_current(:,4))-1);
            
            %2. append the current information to all info mats
            dataInfo_RGB = [dataInfo_RGB ; dataInfo_RGB_current];%zeros(1,6);
            dataInfo_Depth = [dataInfo_Depth ; dataInfo_Depth_current];%zeros(1,6);
            %3. add these to the related cell too
            dataInfoStruct(p,l).RGB = dataInfo_RGB_current;
            dataInfoStruct(p,l).Depth = dataInfo_Depth_current;
            dataInfoStruct(p,l).MaxRowSize_RGB = max(dataInfo_RGB_current(:,5));
            dataInfoStruct(p,l).MinRowSize_RGB = min(dataInfo_RGB_current(:,5));
            dataInfoStruct(p,l).MaxColSize_RGB = max(dataInfo_RGB_current(:,6));
            dataInfoStruct(p,l).MinColSize_RGB = min(dataInfo_RGB_current(:,6));

            dataInfoStruct(p,l).MaxRowSize_Depth = max(dataInfo_Depth_current(:,5));
            dataInfoStruct(p,l).MinRowSize_Depth = min(dataInfo_Depth_current(:,5));
            dataInfoStruct(p,l).MaxColSize_Depth = max(dataInfo_Depth_current(:,6));
            dataInfoStruct(p,l).MinColSize_Depth = min(dataInfo_Depth_current(:,6));
            
            dataInfoStruct(p,l).Image_Count_RGB = r;
            dataInfoStruct(p,l).Image_Count_Depth = d;

            %4. display additional information about the files that has
            %been read from the folder
            
        end
    end

    MaxRowSize_RGB_All = resize([dataInfoStruct.MaxRowSize_RGB],5,24);
    MaxColSize_RGB_All = resize([dataInfoStruct.MaxColSize_RGB],5,24);
    MaxRowSize_Depth_All = resize([dataInfoStruct.MaxRowSize_Depth],5,24);
    MaxColSize_Depth_All = resize([dataInfoStruct.MaxColSize_Depth],5,24);
    
    Image_Count_RGB_All = resize([dataInfoStruct.Image_Count_RGB],5,24);
    Image_Count_Depth_All = resize([dataInfoStruct.Image_Count_Depth],5,24);
    
    MaxRowSize_RGB = max(MaxRowSize_RGB_All(:));
    MaxColSize_RGB = max(MaxColSize_RGB_All(:));
    MaxRowSize_Depth = max(MaxRowSize_Depth_All(:));
    MaxColSize_Depth = max(MaxColSize_Depth_All(:));
    
    Image_Count_RGB = sum(Image_Count_RGB_All(:));
    Image_Count_Depth = sum(Image_Count_Depth_All(:));
    
    depthImages = zeros(MaxRowSize_Depth,MaxColSize_Depth,Image_Count_Depth);
    rgbImages = zeros(MaxRowSize_RGB,MaxColSize_RGB,3,Image_Count_RGB);

    
    r=0;%RGB
    d=0;%Depth
% Second round of loop
    for p=1:size(personFolders,1)
        for l = 1:size(letters,1)
            %here we are in a folder of a person
            videoFolder = [mainFolder '\'  personFolders{p,1} '\' letters{l,1} '\'];
            listing = dir(videoFolder);
            %a file is valid as long as it has both depth and rgb sample
            numberOfFiles_All = ceil((size(listing,1) - 2) / 2);
            for f = 3:size(listing,1)%ignoring the first 2 as . and ..
                curFileName = listing(f,1).name;
                if strncmpi(curFileName,'color',5)
                    %for COLOR
                    %check if the depth image exists too
                    deptEquivFileName = strrep([videoFolder curFileName],'color','depth');
                    if exist(deptEquivFileName,'file')
                        rgbImage = double(imread([videoFolder curFileName]))./255;
                        %reads a R by C by 3 uint image which is from 0 to 255
                        r = r+1;
                        rgbImages(:,:,:,r) = rgbImage;
                    end
                elseif strncmpi(curFileName,'depth',5)
                    %for Depth
                    %check if the depth image exists too
                    colorEquivFileName = strrep([videoFolder curFileName],'depth','color');
                    if exist(colorEquivFileName,'file')
                        depthImage = double(imread([videoFolder curFileName]))./255;
                        %reads a R by C uint image which is from 0 to 255
                        d = d+1;
                        depthImages(:,:,d) = depthImage;
                    else
                        warning(['color(' colorEquivFileName ') of depth(' curFileName ') not exists-hence skipping']);
                    end
                else
                    error(['What is this file?<' videoFolder curFileName  '>']);
                end 
            end

            % display additional information about the files that has
            % been read from the folder
            
        end
    end
end

