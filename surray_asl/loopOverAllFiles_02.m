function [ imageCountMatrix, imageCountTotal, imageRolColSize, samplesInfo_RGBD] = loopOverAllFiles_02(mainFolder)
%loopOverAllFiles This function will read all the png filers into a mat
%array
%   What to do?
%     1. Loop over all images to
%       a. Put them in a mat file such as dataset5_A_a.mat,dataset5_D_h.mat,
%       b. Make them square by padding zeros around - size will be max of 

    fileNameDataInfo = [mainFolder '\dataInfo.mat'];
    load(fileNameDataInfo,'dataInfoStruct','samplesInfo_RGBD');

    % mainFolder = 'G:\Data\dataset5';
    personFolders = {'A';'B';'C';'D';'E'};
    letters = {'a';'b';'c';'d';'e';'f';'g';'h';'i';'k';'l';'m';'n';'o';'p';'q';'r';'s';'t';'u';'v';'w';'x';'y'};

    % dataInfoStruct = struct;%cell(size(personFolders,1),size(letters,1));
    % samplesInfo_RGBD = [];%zeros(1,6);
    %Columns - 1. personID / 2.letterID / 3. count within the video / 4.percentage of video position 0-start, 1-end / 5. rowSize / 6. colSize
    %I want to add 4 more cols to it and return
    samplesInfo_RGBD(1,10) = 0;%this will do the job to increase colSize to 10 and make the new values 0

    MaxRowSize_RGB_All = reshape([dataInfoStruct.MaxRowSize_RGB],5,24);
    MaxColSize_RGB_All = reshape([dataInfoStruct.MaxColSize_RGB],5,24);
    MaxRowSize_Depth_All = reshape([dataInfoStruct.MaxRowSize_Depth],5,24);
    MaxColSize_Depth_All = reshape([dataInfoStruct.MaxColSize_Depth],5,24);
    
    imageCountMatrix_RGB = reshape([dataInfoStruct.Image_Count_RGB],5,24);
    imageCountMatrix_Depth = reshape([dataInfoStruct.Image_Count_Depth],5,24);
    assert(sum(abs(imageCountMatrix_RGB(:)-imageCountMatrix_Depth(:)))==0,'the image counts must be the same');
    imageCountTotal = sum(imageCountMatrix_RGB(:));
    imageCountMatrix = imageCountMatrix_RGB;
    clear image_Count_Matrix_RGB image_Count_Matrix_Depth
    
    MaxRowSize_RGB = max(MaxRowSize_RGB_All(:));
    MaxColSize_RGB = max(MaxColSize_RGB_All(:));
    MaxRowSize_Depth = max(MaxRowSize_Depth_All(:));
    MaxColSize_Depth = max(MaxColSize_Depth_All(:));
    
    imageRolColSize = max([MaxRowSize_RGB MaxColSize_RGB MaxRowSize_Depth MaxColSize_Depth]);
    
    ra=0;%RGB_ALL
    da=0;%Depth_ALL
    for p=1:size(personFolders,1)
        for l = 1:size(letters,1)
            %here we are in a folder of a person
            videoFolder = [mainFolder '\'  personFolders{p,1} '\' letters{l,1} '\'];
            listing = dir(videoFolder);
            
            imageCountCur = imageCountMatrix(p,l);
            images_RGBD = zeros(imageRolColSize,imageRolColSize,4,imageCountCur);
            
            disp([personFolders{p,1} '-' letters{l,1} ', image count = ' num2str(imageCountCur)]);
            
            r=0;%RGB
            d=0;%Depth
            %a file is valid as long as it has both depth and rgb sample
            matFileName = [mainFolder '\'  num2str(p,'%02d') '_'   num2str(l,'%02d')  '.mat'];
            if ~exist(matFileName, 'file')
                for f = 3:size(listing,1)%ignoring the first 2 as . and ..
                    curFileName = listing(f,1).name;
                    if strncmpi(curFileName,'color',5)
                        %for COLOR
                        %check if the depth image exists too
                        deptEquivFileName = strrep([videoFolder curFileName],'color','depth');
                        if exist(deptEquivFileName,'file')
                            rgbImage = double(imread([videoFolder curFileName]))./255;
                            %reads a R by C by 3 uint image which is from 0 to 255
                            r = r+1; ra = ra + 1;
                            %to pad the image with zeros
                            [rStart, rEnd, cStart, cEnd ] = getRC(imageRolColSize, rgbImage);
                            images_RGBD(rStart:end-rEnd,cStart:end-cEnd,1:3,r) = rgbImage;
                            samplesInfo_Current = [rStart, rStart+size(rgbImage,1)-1, cStart, cStart+size(rgbImage,2)-1];
                            if (sum(samplesInfo_RGBD(ra,7:10))==0)
                                samplesInfo_RGBD(ra,7:10) = samplesInfo_Current;
                            else
                                assert(sum(abs(samplesInfo_RGBD(ra,7:10)-samplesInfo_Current))==0,'These vectors must be equal')
                            end
                        end
                    elseif strncmpi(curFileName,'depth',5)
                        %for Depth
                        %check if the depth image exists too
                        colorEquivFileName = strrep([videoFolder curFileName],'depth','color');
                        if exist(colorEquivFileName,'file')
                            depthImage = double(imread([videoFolder curFileName]))./255;
                            %reads a R by C uint image which is from 0 to 255
                            d = d+1; da = da + 1;
                            %to pad the image with zeros
                            [rStart, rEnd, cStart, cEnd ] = getRC(imageRolColSize, depthImage);
                            images_RGBD(rStart:end-rEnd,cStart:end-cEnd,4,d) = depthImage;
                            samplesInfo_Current = [rStart, rStart+size(depthImage,1)-1, cStart, cStart+size(depthImage,2)-1];
                            if (sum(samplesInfo_RGBD(ra,7:10))==0)
                                samplesInfo_RGBD(da,7:10) = samplesInfo_Current;
                            else
                                assert(sum(abs(samplesInfo_RGBD(da,7:10)-samplesInfo_Current))==0,'These vectors must be equal')
                            end                            
                        end
                    else
                        error(['What is this file?<' videoFolder curFileName  '>']);
                    end 
                end
                tic();
                disp(['Saving ' matFileName]);
                save(matFileName, 'images_RGBD');
                toc();
            else
                disp(['Skipping ' matFileName]);
            end
        end
    end
    fileNameToSave = [mainFolder '\samplesInfo.mat'];
    save(fileNameToSave,'imageCountMatrix', 'imageCountTotal', 'imageRolColSize', 'samplesInfo_RGBD');
end

function [rStart, rEnd, cStart, cEnd ] = getRC(imageRolColSize, rgbdImage)
    rDif = imageRolColSize - size(rgbdImage,1);
    if (rDif==0)
        rStart = 1;
        rEnd = 0;
    else
        rStart = floor(rDif/2);
        rEnd = rDif-rStart+1;
    end
    cDif = imageRolColSize - size(rgbdImage,2);
    if (cDif==0)
        cStart = 1;
        cEnd = 0;
    else
        cStart = floor(cDif/2);
        cEnd = cDif-cStart+1;
    end  
end
