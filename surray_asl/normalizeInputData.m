function [dataReshaped, additionalInfo] = normalizeInputData(data, sizeImg, countSamples, sizeChn)
    additionalInfo = struct;
    if length(sizeImg)==1
        sizeImg = [sizeImg sizeImg];
    end
    %grab patches with stride == patchSize
    %normalization : 
    %input --> subtract mean value of pixels from all pixels for
    %R-G-B-D differently -
        additionalInfo.P01_initialDataSize = size(data);%sizeImg,sizeImg,sizeChn,countSamples
    dataReshaped = permute(data,[1 2 4 3]);%sizeImg,sizeImg,countSamples,sizeChn
        additionalInfo.P02_initialReshapedDataSize = size(dataReshaped);
    dataReshaped = reshape(dataReshaped,[],sizeChn);%sizeImg*sizeImg*countSamples,sizeChn
        additionalInfo.P03_nextReshapedDataSize = size(dataReshaped);
    data_mean = NaN(1,sizeChn);
    data_std = NaN(1,sizeChn);
    for i=1:sizeChn
        data_mean(i) = mean(dataReshaped(dataReshaped(:,i)~=0,i));
        data_std(i) = std(dataReshaped(dataReshaped(:,i)~=0,i));
        %subtract mean from each channel
        dataReshaped(dataReshaped(:,i)~=0,i) = dataReshaped(dataReshaped(:,i)~=0,i)-data_mean(i);
    end
        additionalInfo.P04_meanOfData = data_mean;
        additionalInfo.P05_stdOfData = data_std;        
    %divide each value to its standart deviation
    dataReshaped = bsxfun(@rdivide,dataReshaped,data_std);
    %now the data is zero normalized and re-scaled to -1 and 1
        additionalInfo.P06_minOfData = min(dataReshaped);
    dataReshaped_Mean = NaN(1,sizeChn);
    dataReshaped_Std = NaN(1,sizeChn);
    for i=1:sizeChn
        dataReshaped_Mean(i) = mean(dataReshaped(dataReshaped(:,i)~=0,i));
        dataReshaped_Std(i) = std(dataReshaped(dataReshaped(:,i)~=0,i));
    end
        additionalInfo.P07_meanOfReshapedData = dataReshaped_Mean;
        additionalInfo.P08_stdOfReshapedData = dataReshaped_Std;
        additionalInfo.P09_maxOfReshapedData = max(dataReshaped);
    %now reshape data back to its original way
    dataReshaped = reshape(dataReshaped,[sizeImg(1),sizeImg(2),countSamples,sizeChn]);%sizeImg,sizeImg,countSamples,sizeChn
    dataReshaped = permute(dataReshaped,[1 2 4 3]);%sizeImg,sizeImg,sizeChn,countSamples
        additionalInfo.P10_finalReshapedDataSize = size(dataReshaped);
end