function [cnnet_out, labels_all, predictions_all, selectedLeftTopMostPixelIDs_all, confMat_All] = runCNNET_ValidTest(cnnnet, patchSize, additionalInfo, dataTypeStr, figureID)
    [dataStruct, batchInfo] = createInitialDataStruct_surray('raw');
    displayOpts = setDisplayOpts('none');
    strideParam = [2 2];

    dataTypeStr(1) = upper(dataTypeStr(1));
    batchCount = eval(['batchInfo.batch_Count_' dataTypeStr]);
    totalImageCount = eval(['sum(batchInfo.image_Count_Matrix_' dataTypeStr '(:));']);
    
    labels_all = zeros([1 totalImageCount]);
    predictions_all = zeros([1 totalImageCount]);
    selectedLeftTopMostPixelIDs_all = zeros([1 totalImageCount]);
    fr = 1;
	trueTotal = 0;
	confMat_Batch = zeros(24,24);
	confMat_All = zeros(24,24);
    for b = 1:batchCount
        %1. Load batchData
        dataStruct.updateDataStruct(dataStruct, {'batchID_current', b});
        load(dataStruct.fileName_current,'data','labels','samplesInfo_RGBD_batch');
        data = data(:,:,1:3,:);
        rgbData = data;
        batchImageCount = length(labels); 
        sizeChn = size(data,3);
        %normalize it
        data = permute(data,[1 2 4 3]);%sizeImg,sizeImg,countSamples,sizeChn
        dataSize_permuted = size(data);
        data = reshape(data,[],sizeChn);
        for i=1:sizeChn
            %subtract mean from each channel
            data(data(:,i)~=0,i) = data(data(:,i)~=0,i)-additionalInfo.P04_meanOfData(i);
        end  
        data = bsxfun(@rdivide,data,additionalInfo.P05_stdOfData);
        data = reshape(data,dataSize_permuted);
        data = permute(data,[1 2 4 3]);%sizeImg,sizeImg,sizeChn,countSamples

        data = reshape(data,[],batchImageCount);
        
        %now we will not use all data at once
        %we will deal with it in batches
        countSamplesAll = size(data,2);
        predictions_batch = zeros(1,countSamplesAll);
        selectedLeftTopMostPixelIDs_batch = zeros(1,countSamplesAll);
		trueBatch = 0;
        for i=1:countSamplesAll
            curSampleRGB = rgbData(:,:,:,i);
            curSample = data(:,i);
            curSample = reshape(curSample, [272,272,3,1]);
            curSampleInfo = samplesInfo_RGBD_batch(i,:);
            [patchLabels, imageIDs, leftTopMostPixIDs] = selectPatches_ByStride(curSampleInfo, [272 272], 1, patchSize, strideParam);
            [patchMat, invalidPatchIDs] = acquirePatches( patchSize, curSample, sizeChn, imageIDs, leftTopMostPixIDs, true);
            
            cnnet_out = getCNNETreadyForNewData( cnnnet, 2, [patchSize sizeChn size(patchMat,2)]);
            cnnet_out{1,1}.stride = [1 1];%[4 4];
                   
           [a, cnnet_out] = convolution_ForwardPropogate(cnnet_out, 1, patchMat, 'displayOpts', displayOpts, 'memoryMethod', 'all_in_memory');   
           [a, cnnet_out] = activation_ForwardPropogate(cnnet_out, 2, a,'displayOpts' , displayOpts,'activationID', 1,'memoryMethod', 'all_in_memory');
           cnnet_out{3,1}.poolSize = cnnet_out{1,1}.outputSize_original([1 2]);
           [a, cnnet_out] = pooling_ForwardPropogate(cnnet_out, 3, a,'displayOpts' , displayOpts,'activationID', 2,'memoryMethod', 'all_in_memory','calculateBackPropMap', false);
           meanA = mean(a,2);
           [finalLayerOutput, ~, predictedGesture_meanSoft, ~, probabilitiesOut] = softMax_ForwardPropogate( cnnet_out, 4, meanA, 'displayOpts', displayOpts, 'memoryMethod', 'all_in_memory', 'labels' , patchLabels');
           [finalLayerOutputPatches, ~, predictedGesturesPatches, ~, probsOutPatches] = softMax_ForwardPropogate( cnnet_out, 4, a, 'displayOpts', displayOpts, 'memoryMethod', 'all_in_memory', 'labels' , patchLabels');
           
           probsOutPatches = bsxfun(@minus,probsOutPatches,min(probsOutPatches));
           zeroProbsPatches = sum(probsOutPatches)<0.01;
           
           sumActivationPatches = sum(finalLayerOutputPatches(:,(zeroProbsPatches==0)),2)';
           
           predictedGesturesPatches(zeroProbsPatches==1) = [];
           
           [sumVal, predictedGesture_sum] = max(sumActivationPatches);
           sumActivationPatches = sumActivationPatches-min(sumActivationPatches);
           sum_histogram = sumActivationPatches/max(sumActivationPatches);
                      
           p_meanSoftMax = probabilitiesOut(predictedGesture_meanSoft);
           givenGesture = patchLabels(1);
           
           %predictedGesture = predictedGesture_meanSoft;
           predictedGesture = predictedGesture_sum;

           
           %for the predicted gesture - 
           [rs, cs] = ind2sub([272,272],leftTopMostPixIDs(predictedGesturesPatches==predictedGesture));
           rs = floor(mean(rs));
           cs = floor(mean(cs));
           selectedLeftTopMostPixelIDs_batch(i) = sub2ind([272,272],rs,cs);
           %for the incorrectly found gesture -
           if givenGesture~=predictedGesture
               [rs_pred, cs_pred] = ind2sub([272,272],leftTopMostPixIDs(predictedGesturesPatches==predictedGesture));
               rs_pred = floor(mean(rs_pred));
               cs_pred = floor(mean(cs_pred));
               selectedLeftTopMostPixelIDs_batch(i) = sub2ind([272,272],rs_pred,cs_pred);
		   else
			   trueBatch = trueBatch + 1;
			   trueTotal = trueTotal + 1;
			   accBatch = trueBatch/i;
			   accTotal = trueTotal/totalImageCount;
           end
		   confMat_Batch(givenGesture, predictedGesture) = confMat_Batch(givenGesture, predictedGesture) + 1;
           
           predictions_batch(i) = predictedGesture;
           if exist('figureID','var') && ~isempty(figureID)
               figure(figureID);
               fRS = 3; fCS = 4;
               
               %First row 1,2,3,4
               subplot(fRS,fCS,1);imshow(curSampleRGB);title('RGB')
               
               re=rs+patchSize(1)-1;
               ce=cs+patchSize(2)-1;
               titleStr = ['meanSoft-Given(' num2str(givenGesture) '),Found(' num2str(predictedGesture_meanSoft) '),Prob(' num2str(p_meanSoftMax,'%4.2f') ')'];
               subplot(fRS,fCS,2);imshow(curSampleRGB(rs:re,cs:ce,:));title(titleStr);
               titleStr = ['sumAct-Given(' num2str(givenGesture) '),Found(' num2str(predictedGesture_sum) '),Prob(' num2str(sum_histogram(predictedGesture_sum),'%4.2f') ')'];
               subplot(fRS,fCS,3);imshow(curSample(rs:re,cs:ce,:));title(titleStr);
               
               subplot(fRS,fCS,4);imshow(curSample);title('Normalized')
               
               
               %Second row  5,6,7,8
               if givenGesture==predictedGesture
                   subplot(fRS,fCS,[5 6]);bar(1:24,probabilitiesOut);
                   title('Probabilities of true patch');xlim([0.5 24.5]);set(gca,'XTick',1:24);
               else
                   re=rs_pred+patchSize(1)-1;
                   ce=cs_pred+patchSize(2)-1;
                   subplot(fRS,fCS,[5 6]);imshow(curSampleRGB(rs_pred:re,cs_pred:ce,:));title('Wrongly identified area');
               end
               subplot(fRS,fCS,[7 8]);bar(1:24,sum_histogram);title('Normalized sum of gesture patch activations'); xlim([0.5 24.5]);set(gca,'XTick',1:24);
               
               %Third row  9->12
               subplot(fRS,fCS,9:12);
               imagesc(finalLayerOutputPatches(:,(zeroProbsPatches==0)));
               set(gca,'YTick',1:24);
               colorbar;
               title(['Probibilities of convolution patches-deletedPatchCount(' num2str(sum(zeroProbsPatches==1)) ')']);
           end
		   
		   disp(['Batch' dataTypeStr '(' num2str(b) '), sample(' num2str(i) ' of ' num2str(countSamplesAll) '), batchAcc(' num2str(trueBatch) '/' num2str(countSamplesAll) '=' num2str(accBatch,'%4.2f') '), totalAcc(' num2str(trueTotal) '/' num2str(totalImageCount) '=' num2str(accTotal,'%4.2f') ')']);
        end
        disp(['Batch' dataTypeStr '(' num2str(b) '), All Samples, batchAcc(' num2str(trueBatch) '/' num2str(countSamplesAll) '=' num2str(accBatch,'%4.2f') '), totalAcc(' num2str(trueTotal) '/' num2str(totalImageCount) '=' num2str(accTotal,'%4.2f') ')']);
		confMat_All = confMat_All + confMat_Batch;
		disp('Batch ConfMat:')
		disptable(confMat_Batch);
		disp('Total ConfMat:')
		disptable(confMat_All);
        to = fr+batchImageCount-1;
        disp(['Assigning batchID(' num2str(b) ') of ' num2str(batchImageCount) ' images to data_all[' num2str(fr) ':' num2str(to) ']']);
        labels_all(1,fr:to) = labels;
        predictions_all(fr:to) = predictions_batch;
        selectedLeftTopMostPixelIDs_all(fr:to) = selectedLeftTopMostPixelIDs_batch;
        fr = to+1;
    end 
end