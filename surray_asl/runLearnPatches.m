global bigDataPath;
methodID = 3;
patchSize = 10;
inputChannelSize = 4;
opttheta = [];

global minFuncStepInfo;
for ip = 1:2:9%initialPixel
    for batchID = 1:17
        [ patchMat, patchLabels, additionalInfo] = turnBatchIntoPatches( [bigDataPath 'dataset5' filesep 'data_batch_train_' num2str(batchID,'%02d') '.mat'], patchSize, methodID, ip);
        
        minFuncStepInfo.AdditionalLogTitle = ['initialPixel(' num2str(ip,'%02d') '),batchID(' num2str(batchID,'%02d') ')'];
        [W, opttheta] = trainAutoEncoder(patchMat,'sizeHid', 100,'paramsCategory_MinFunc', 'showInfo_saveLog','theta',opttheta);
        
        figureID = 1+floor((ip-1)/2)*17 + batchID;
        h = visualizeLearntFeatures( W, inputChannelSize, patchSize, 1);
        fileName = [bigDataPath 'figures' filesep 'Fig(' num2str(figureID,'%03d') '),' minFuncStepInfo.AdditionalLogTitle '.png'];
        saveas(h, fileName);
    end
end