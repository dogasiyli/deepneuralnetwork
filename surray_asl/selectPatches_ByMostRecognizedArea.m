function [patchLabels, imageIDs, leftTopMostPixIDs] = selectPatches_ByMostRecognizedArea(patchSelectionStruct, samplesInfo_RGBD_batch, patchSize)
    % col 5 and 6 are R and C of the real image that is padded with sth
    % col  7 is the initial rowID where the real image reside
    % col  8 is the    last rowID where the real image reside
    % col  9 is the initial colID where the real image reside
    % col 10 is the    last colID where the real image reside
    
    mostRecognizedArea = patchSelectionStruct.mostRecognizedArea;
    patchCount = patchSelectionStruct.patchCount;
    patchPickMethod = patchSelectionStruct.patchPickMethod;
    
    
    [sizeImgRow, sizeImgCol, countSamples] = size(mostRecognizedArea);
    
    if (patchCount==0), patchCount=12; end
    
    
    allImageBlock = NaN([patchCount*countSamples, 7]);
    patchLabels = zeros(patchCount*countSamples,1);
    r_start = 1;
    
    for i = 1:countSamples
        %I have to find the stride parameter for both 
        %row(vertical)
        %col(horizontal)
        patchInfCur = samplesInfo_RGBD_batch(i,:);
        
        if (patchInfCur(5)<patchSize(1) || patchInfCur(6)<patchSize(2))
            warning('Patch size is bigger than defined input image dimensions-paused');
            pause;
        end

        switch patchPickMethod
            case 0
                wR = patchInfCur(5)/patchInfCur(6);%rowSize_div_colSize weight of row
                %so patchCount is patchCount = pcR*pcC - so what are pcR and pcC
                allPossibleValuesOf_pC = getAllPossibleDivisors(patchCount);%all possible values for pC - patchCount Column
                pC_real = sqrt(patchCount/wR);
                [~, pC] = min(abs(allPossibleValuesOf_pC-pC_real));
                pC = allPossibleValuesOf_pC(pC);
                pR = patchCount/pC;

                %so horizontally image will be divided into pC pieces that touches on top of each other
                %vertically image will be divided into pR pieces that touches on top of each other

                r_CurImage = floor(linspace(patchInfCur(7),max(patchInfCur(7),patchInfCur(8)-patchSize(1)-1),pR));
                c_CurImage = floor(linspace(patchInfCur(9),max(patchInfCur(9),patchInfCur(10)-patchSize(2)-1),pC));
                [Ri,Ci] = meshgrid(r_CurImage,c_CurImage');
            otherwise
                [Ri,Ci] = getPatchesFromMostRecognizedAreaMat( mostRecognizedArea, patchInfCur, patchPickMethod, patchCount);
        end
        
        RC_CurImage = [Ri(:),Ci(:)];%1st col rowIDs, 2nd col colIDs
        curImagePatchCount = size(RC_CurImage,1);
        
        %row is smaller area
        rowsToDelete = RC_CurImage(:,1)<patchInfCur(:,7);
        assert(sum(rowsToDelete)==0,'no row should be deleted');
        %row is bigger area
        rowsToDelete = RC_CurImage(:,1)+patchSize(1)>patchInfCur(:,8);
        assert(sum(rowsToDelete)==0,'no row should be deleted');
        %col is smaller area
        rowsToDelete = RC_CurImage(:,2)<patchInfCur(:,9);
        assert(sum(rowsToDelete)==0,'no row should be deleted');
        %col is bigger area
        rowsToDelete = RC_CurImage(:,2)+patchSize(2)>patchInfCur(:,10);
        assert(sum(rowsToDelete)==0,'no row should be deleted');
        %RC_CurImage(rowsToDelete,:) = [];   
        
        r_end = r_start + curImagePatchCount-1;
        allImageBlock(r_start:r_end,1:2) = RC_CurImage;
        allImageBlock(r_start:r_end,3:6) = repmat(samplesInfo_RGBD_batch(i,7:10),[curImagePatchCount, 1]);
        allImageBlock(r_start:r_end,7) = i;
        patchLabels(r_start:r_end) = samplesInfo_RGBD_batch(i,2);
        r_start = r_end +1;
    end
    allImageBlock = allImageBlock(1:r_end,:,:);
    patchLabels = patchLabels(1:r_end);

    imageIDs = allImageBlock(:,7);
    leftTopMostPixIDs = sub2ind([sizeImgRow, sizeImgCol],allImageBlock(:,1), allImageBlock(:,2));
end