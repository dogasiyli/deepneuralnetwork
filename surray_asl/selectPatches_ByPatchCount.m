function [patchLabels, imageIDs, leftTopMostPixIDs] = selectPatches_ByPatchCount(samplesInfo_RGBD_batch, sizeImg, countSamples, patchSize, patchCount)
    % col 5 and 6 are R and C of the real image that is padded with sth
    % col  7 is the initial rowID where the real image reside
    % col  8 is the    last rowID where the real image reside
    % col  9 is the initial colID where the real image reside
    % col 10 is the    last colID where the real image reside
    
    if (patchCount==0), patchCount=12; end
    allImageBlock = NaN([patchCount*countSamples, 7]);
    patchLabels = zeros(patchCount*countSamples,1);
    r_start = 1;
    
    for i = 1:countSamples
        %I have to find the stride parameter for both 
        %row(vertical)
        %col(horizontal)
        patchInfo_Cur = samplesInfo_RGBD_batch(i,:);
        
        if (patchInfo_Cur(5)<patchSize(1) || patchInfo_Cur(6)<patchSize(2))
            warning('Patch size is bigger than defined input image dimensions-paused');
            pause;
        end
        
%         wR = patchInfo_Cur(5)/patchInfo_Cur(6);%rowSize_div_colSize weight of row
%         %so patchCount is patchCount = pcR*pcC - so what are pcR and pcC
%         allPossibleValuesOf_pC = getAllPossibleDivisors(patchCount);%all possible values for pC - patchCount Column
%         pC_real = sqrt(patchCount/wR);
%         [~, pC] = min(abs(allPossibleValuesOf_pC-pC_real));
%         pC = allPossibleValuesOf_pC(pC);
%         pR = patchCount/pC;
%         
%         %so horizontally image will be divided into pC pieces that touches on top of each other
%         %vertically image will be divided into pR pieces that touches on top of each other
%         
%         r_CurImage = floor(linspace(patchInfo_Cur(7),max(patchInfo_Cur(7),patchInfo_Cur(8)-patchSize(1)-1),pR));
%         c_CurImage = floor(linspace(patchInfo_Cur(9),max(patchInfo_Cur(9),patchInfo_Cur(10)-patchSize(2)-1),pC));
%         [Ri,Ci] = meshgrid(r_CurImage,c_CurImage');
%         RC_CurImage = [Ri(:),Ci(:)];%1st col rowIDs, 2nd col colIDs
        RC_CurImage = getPatchPixIDs(patchInfo_Cur(5:10), patchCount, patchSize);
        
        curImagePatchCount = size(RC_CurImage,1);
        r_end = r_start + curImagePatchCount-1;
        allImageBlock(r_start:r_end,1:2) = RC_CurImage;
        allImageBlock(r_start:r_end,3:6) = repmat(samplesInfo_RGBD_batch(i,7:10),[curImagePatchCount, 1]);
        allImageBlock(r_start:r_end,7) = i;
        patchLabels(r_start:r_end) = samplesInfo_RGBD_batch(i,2);
        r_start = r_end +1;
    end
    allImageBlock = allImageBlock(1:r_end,:,:);
    patchLabels = patchLabels(1:r_end);

    imageIDs = allImageBlock(:,7);
    leftTopMostPixIDs = sub2ind([sizeImg, sizeImg],allImageBlock(:,1), allImageBlock(:,2));
end