function [patchLabels, imageIDs, leftTopMostPixIDs] = selectPatches_ByStride(samplesInfo_RGBD_batch, sizeImg, countSamples, patchSize, stride)
    % col 5 and 6 are R and C of the real image that is padded with sth
    % col  7 is the initial rowID where the real image reside
    % col  8 is the    last rowID where the real image reside
    % col  9 is the initial colID where the real image reside
    % col 10 is the    last colID where the real image reside
    
    if (~exist('stride','var') || isempty(stride))
        stride = [1 1];
    elseif length(stride)==1
        stride = [stride stride];
    elseif size(stride,2)==1
        stride = stride';
    end

    pRMax = ceil((sizeImg(1)-patchSize(1))/stride(1))+1;
    pCMax = ceil((sizeImg(2)-patchSize(2))/stride(2))+1;

    allImageBlock = NaN([pRMax*pCMax*countSamples, 7]);
    patchLabels = zeros(pRMax*pCMax*countSamples,1);
    r_start = 1;
    
    for i = 1:countSamples
        %I have to find the stride parameter for both 
        %row(vertical)
        %col(horizontal)
        patchInfo_Cur = samplesInfo_RGBD_batch(i,:);
        
        if (patchInfo_Cur(5)<patchSize(1) || patchInfo_Cur(6)<patchSize(2))
            warning('Patch size is bigger than defined input image dimensions-paused');
            pause;
        end

        r_CurImage = patchInfo_Cur(7):stride(1):(patchInfo_Cur(8)-patchSize(1)-1);
        c_CurImage = patchInfo_Cur(9):stride(2):(patchInfo_Cur(10)-patchSize(2)-1);
        [Ri,Ci] = meshgrid(r_CurImage,c_CurImage');
        RC_CurImage = [Ri(:),Ci(:)];%1st col rowIDs, 2nd col colIDs
        curImagePatchCount = size(RC_CurImage,1);
        
        %row is smaller area
        rowsToDelete = RC_CurImage(:,1)<patchInfo_Cur(:,7);
        assert(sum(rowsToDelete)==0,'no row should be deleted');
        %row is bigger area
        rowsToDelete = RC_CurImage(:,1)+patchSize(1)>patchInfo_Cur(:,8);
        assert(sum(rowsToDelete)==0,'no row should be deleted');
        %col is smaller area
        rowsToDelete = RC_CurImage(:,2)<patchInfo_Cur(:,9);
        assert(sum(rowsToDelete)==0,'no row should be deleted');
        %col is bigger area
        rowsToDelete = RC_CurImage(:,2)+patchSize(2)>patchInfo_Cur(:,10);
        assert(sum(rowsToDelete)==0,'no row should be deleted');
        %RC_CurImage(rowsToDelete,:) = [];   
        
        r_end = r_start + curImagePatchCount-1;
        allImageBlock(r_start:r_end,1:2) = RC_CurImage;
        allImageBlock(r_start:r_end,3:6) = repmat(samplesInfo_RGBD_batch(i,7:10),[curImagePatchCount, 1]);
        allImageBlock(r_start:r_end,7) = i;
        patchLabels(r_start:r_end) = samplesInfo_RGBD_batch(i,2);
        r_start = r_end +1;
    end
    allImageBlock = allImageBlock(1:r_end,:,:);
    patchLabels = patchLabels(1:r_end);

    imageIDs = allImageBlock(:,7);
    leftTopMostPixIDs = sub2ind([sizeImg, sizeImg],allImageBlock(:,1), allImageBlock(:,2));
end