function [ batchInfo ] = seperateASLData( mainFolder, testPersonID)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    [ memAvail, memMatlab ] = displayMemory;%returns in megabytes

    bathSizeMaxMB = 6*1024;

    percentages.Train = 0.85;
    percentages.Validation = 1-percentages.Train;
    percentages.Test = 1.0;
    
    fileNameDataInfo = [mainFolder filesep 'dataInfo.mat'];
    load(fileNameDataInfo,'dataInfoStruct','samplesInfo_RGBD');
    fileNameSamplesInfo = [mainFolder filesep 'samplesInfo.mat'];
    load(fileNameSamplesInfo,'imageCountMatrix', 'imageCountTotal', 'imageRolColSize', 'samplesInfo_RGBD');
    
    fileNameBatchInfo = [mainFolder filesep 'batchInfo.mat'];
    if (exist(fileNameBatchInfo,'file'))
        load(fileNameBatchInfo);
        batchInfoLoaded = batchInfo;
    else
        batchInfoLoaded = [];
    end
    [ batchInfo ] = seperateASLData_UserIndependant( dataInfoStruct, imageRolColSize, testPersonID, bathSizeMaxMB, percentages );
    structsAreSame = false;
    if ~isempty(batchInfoLoaded)
        structsAreSame = compareStructs(batchInfoLoaded, batchInfo);
    end
    if structsAreSame
        disp(['Struct was already created and is in <' fileNameBatchInfo '>']);
    else
        disp(['Struct in <' fileNameBatchInfo '> is different than the one being created now.']);
        save([mainFolder filesep 'batchInfo.mat'],'batchInfo');
        createBatches(mainFolder, dataInfoStruct, samplesInfo_RGBD, batchInfo, imageRolColSize);
    end
end

