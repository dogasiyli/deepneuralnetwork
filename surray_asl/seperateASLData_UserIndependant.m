function [ batchInfo ] = seperateASLData_UserIndependant( dataInfoStruct, imageRolColSize, testPersonID, bathSizeMaxMB, percentages )
%seperateASLData_UserIndependant This function will seperate the data into batches
%   The batches will be user independant
%   There are 5 people in the dataset - so training will be done for 4
%   people and the testing will be done with the other person.
%   Can we do this such that 

    %Matrix is 5 by 24 where each row represents a person and each column
    %represents a class of letter
    image_Count_Matrix = reshape([dataInfoStruct.Image_Count_RGB],5,24);
    batchDistributions = struct([]);
    
    %I need trainData, validationData and testData
    %testData obviously will only have the testPerson's data
    %train data would have the samples
    
    %The max batch size is given in mb or count of images
    %I will know how many image can there be in a batch
    
    singleImageSize = imageRolColSize^2*4;%this many double values
    singleImageSize = singleImageSize*8;%bytes
    singleImageSize = singleImageSize/(1024*1024);%megabytes
    disp(['Single image size is ' num2str(singleImageSize) ' MB'])
    
    imageCountMaxPerBatch = floor(bathSizeMaxMB/singleImageSize);
    disp(['Maximum allowed image count per batch is ' num2str(imageCountMaxPerBatch)])

    image_Count_Matrix_Train = image_Count_Matrix((1:5)~=testPersonID,:);
    image_Count_Matrix_Test = image_Count_Matrix(testPersonID,:);
    
    image_Count_Total_Test = sum(image_Count_Matrix_Test(:));
    batch_Count_Test = ceil(image_Count_Total_Test/imageCountMaxPerBatch);
    repCount = ceil(image_Count_Total_Test/batch_Count_Test);
    bid_Test = repmat(1:batch_Count_Test,1,repCount);
    bid_Test = bid_Test(1:image_Count_Total_Test);
    

    image_Count_Total_TrainValid = sum(image_Count_Matrix_Train(:));
    image_Count_Total_Train = floor(image_Count_Total_TrainValid*percentages.Train);
    image_Count_Total_Valid = image_Count_Total_TrainValid-image_Count_Total_Train;

    batch_Count_Train = ceil((image_Count_Total_Train)/imageCountMaxPerBatch);
    batch_Count_Valid = ceil((image_Count_Total_Valid)/imageCountMaxPerBatch);
    batch_Count_TrainValid = batch_Count_Train+batch_Count_Valid;
    
    repCount = ceil(image_Count_Total_TrainValid/batch_Count_TrainValid);
    bid_TrainValid = repmat(1:batch_Count_TrainValid,1,repCount);
    bid_TrainValid = bid_TrainValid(1:image_Count_Total_TrainValid);

    disp(['Train - imageCount(' num2str(image_Count_Total_Train) '), batchCount(' num2str(batch_Count_Train) ')'])
    disp(['Valid - imageCount(' num2str(image_Count_Total_Valid) '), batchCount(' num2str(batch_Count_Valid) ')'])
    disp(['Test  - imageCount(' num2str(image_Count_Total_Test) '), batchCount(' num2str(batch_Count_Test) ')'])
    

    % mainFolder = 'G:\Data\dataset5';
    personFolders = {'A';'B';'C';'D';'E'};
    letters = {'a';'b';'c';'d';'e';'f';'g';'h';'i';'k';'l';'m';'n';'o';'p';'q';'r';'s';'t';'u';'v';'w';'x';'y'};
    
    image_Count_Matrix_Train = zeros(5,24);
    image_Count_Matrix_Valid = zeros(5,24);
    image_Count_Matrix_Test  = zeros(5,24);
    image_Count_Per_Batch_Train = zeros(batch_Count_Train,24);
    image_Count_Per_Batch_Valid = zeros(batch_Count_Valid,24);
    image_Count_Per_Batch_Test = zeros(batch_Count_Test,24);

    %first I need to fill in batchDistributions cell
    %so that I can know which sample to put into which batch data
    for p=1:size(personFolders,1)
        for l = 1:size(letters,1)
            imageCountCur = image_Count_Matrix(p,l);
            %these images will be distributed as
            %train
            %validation
            %test
            if (p==testPersonID)
                %test
                %train-validation-test information will all be 3=test
                tvt = 3*ones(1,imageCountCur);%all will be testData
                %Batch ID information will be set as 1-2-3-...-batch_Count_Test
                bid = bid_Test(1:imageCountCur);
                bid_Test = bid_Test(1+imageCountCur : end);
                
                batchDistributions(p,l).tvt = tvt;
                batchDistributions(p,l).bid = bid;
                batchDistributions(p,l).TR = false(1,imageCountCur);
                batchDistributions(p,l).VA = false(1,imageCountCur);
                batchDistributions(p,l).TE = true(1,imageCountCur);
                
                image_Count_Matrix_Test(p,l) = image_Count_Matrix_Test(p,l) + sum(batchDistributions(p,l).TE==1);
            else
                %train or validation
                %Batch ID information will be set as 1-2-3-...-batch_Count_TrainValid
                %if "1<=bid<=batch_Count_Train" then it is train sample
                %else if "batch_Count_Train+1<=bid<=batch_Count_TrainValid" then it is validation sample
                bid = bid_TrainValid(1:imageCountCur);
                bid_TrainValid = bid_TrainValid(1+imageCountCur : end);
                
                %train-validation-test information will be 
                %1=train if 
                tvt = 1*ones(1,imageCountCur);%will be either train(1) or validation(2)
                tvt(bid>batch_Count_Train) = 2;
                batchDistributions(p,l).tvt = tvt;
                batchDistributions(p,l).TR = bid <= batch_Count_Train;
                batchDistributions(p,l).VA = bid >  batch_Count_Train;
                batchDistributions(p,l).TE = false(1,imageCountCur);
                
                bid(bid>batch_Count_Train) = bid(bid>batch_Count_Train)-batch_Count_Train;
                batchDistributions(p,l).bid = bid;
                
                image_Count_Matrix_Train(p,l) = image_Count_Matrix_Train(p,l) + sum(batchDistributions(p,l).TR==1);
                image_Count_Matrix_Valid(p,l) = image_Count_Matrix_Valid(p,l) + sum(batchDistributions(p,l).VA==1);
            end
            
            for b=1:batch_Count_Train
                image_Count_Per_Batch_Train(b,l) = image_Count_Per_Batch_Train(b,l) + sum(batchDistributions(p,l).TR==1 & bid==b);
            end
            for b=1:batch_Count_Valid
                image_Count_Per_Batch_Valid(b,l) = image_Count_Per_Batch_Valid(b,l) + sum(batchDistributions(p,l).VA==1 & bid==b);
            end
            for b=1:batch_Count_Test
                image_Count_Per_Batch_Test(b,l) = image_Count_Per_Batch_Test(b,l) + sum(batchDistributions(p,l).TE==1 & bid==b);
            end
        end
    end
    
    batchInfo.batchSizesMB_Train = singleImageSize*sum(image_Count_Per_Batch_Train,2);
    batchInfo.batchSizesMB_Valid = singleImageSize*sum(image_Count_Per_Batch_Valid,2);
    batchInfo.batchSizesMB_Test = singleImageSize*sum(image_Count_Per_Batch_Test,2);
    
    batchInfo.image_Count_Per_Batch_Train = image_Count_Per_Batch_Train;
    batchInfo.image_Count_Per_Batch_Valid = image_Count_Per_Batch_Valid;
    batchInfo.image_Count_Per_Batch_Test = image_Count_Per_Batch_Test;
    
    batchInfo.image_Count_Matrix_Train = image_Count_Matrix_Train;
    batchInfo.image_Count_Matrix_Valid = image_Count_Matrix_Valid;
    batchInfo.image_Count_Matrix_Test = image_Count_Matrix_Test;
    batchInfo.batch_Count_Train = batch_Count_Train;
    batchInfo.batch_Count_Valid = batch_Count_Valid;
    batchInfo.batch_Count_Test = batch_Count_Test;
    batchInfo.batchDistributions = batchDistributions;
end

