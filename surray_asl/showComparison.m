function showComparison( batchID, figureID, imageID, personID, letterID )

    load(['G:\Data\dataset5\data_batch_train_' num2str(batchID,'%02d') '.mat']);    
    if exist('imageID','var') && ~isempty(imageID)
        %everything is fine
        personID = samplesInfo_RGBD_batch(imageID,1);
        letterID = samplesInfo_RGBD_batch(imageID,2);
    else
        %it loads samplesInfo_RGBD_batch
        selectableInds =  find(samplesInfo_RGBD_batch(:,1)==personID & samplesInfo_RGBD_batch(:,2)==letterID);
        imageID = selectableInds(randi(length(selectableInds)));
    end
    
    figure(figureID);
    
    sizeImg = size(data);
    subplot(2,2,3);imshow(squeeze(data(:,:,1:3,imageID)));title(['Padded-Size(' num2str(sizeImg(1)) ' rows, ' num2str(sizeImg(2)) ' cols)']);
    rowSt = samplesInfo_RGBD_batch(imageID,7);
    rowEn = samplesInfo_RGBD_batch(imageID,8);
    colSt = samplesInfo_RGBD_batch(imageID,9);
    colEn = samplesInfo_RGBD_batch(imageID,10);
    
    subplot(2,2,4);imshow(squeeze(data(rowSt:rowEn,colSt:colEn,1:3,imageID)));
    title(['Cropped-Size(' num2str(samplesInfo_RGBD_batch(imageID,5)) ' rows,' num2str(samplesInfo_RGBD_batch(imageID,6)) ' cols)']);
    
    
    load(['G:\Data\dataset5\data_batch_train_' num2str(batchID,'%02d') '_resized[80 80].mat'])
    subplot(2,2,2);
    imshow(squeeze(data(:,:,1:3,imageID)));
    title('Resized-Size(80 rows, 80 cols)');
    
    
    load(['G:\Data\dataset5\data_batch_train_' num2str(batchID,'%02d') '_focus.mat']);
    subplot(2,2,1);
    imshow(squeeze(data(:,:,1:3,imageID)));
    title(['b(' num2str(batchID,'%02d') '),i(' num2str(imageID,'%02d') '),l(' num2str(labels(imageID),'%02d') ')-Focused-Size(80 rows, 80 cols)']);
end

