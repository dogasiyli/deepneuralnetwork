function [ patchMat, patchLabels, additionalInfo] = turnBatchIntoPatches( dataIdentifier, patchSize, varargin)...'methodID_preprocess','initialPixel','patchCountPerSample'
%turnBatchIntoPatches Loads the batch file into memory and returns patches
%according to methodID_preprocess
%   dataIdentifier : e.g. 'G:\Data\dataset5\data_batch_train_01.mat'
%   patchSize = [rowSize colSize];
%   Method-1 : We know where the real information is
%              Grab all the adjacent patches


%% 1.Load the batchFile
%    e.g. load('G:\Data\dataset5\data_batch_train_01.mat') will load - [folderName 'data_batch_' batchType '_' num2str(batchID,'%02d') '.mat']
%    - data : 4-D data which is R by C by F by N
%    - labels : 1 by N vector
%    - samplesInfo_RGBD_batch : N by 10 matrix
%    - uncompressedSize : the data size to be loaded in RAM
    
    [data, samplesInfo_RGBD_batch] = getData(dataIdentifier);
    
%% 2.Get optional parameters    
    [  methodID_preprocess,  patchSelectionStruct...
     , sizeImg, countSamples, sizeChn...
     , leftTopMostPixStruct...
  ..., countFeat, displayOpts,  saveOpts...
     ] = getOptionalParams(data, varargin{:});
    
%% 3. Get patchLabels, imageIDs and leftTopMostPixelIDs
%     If these are already in the optional parameter 'leftTopMostPixStruct'
%     just grab these variables from that struct
%     Otherwise select patches according to other optional parameters
    if isempty(leftTopMostPixStruct)
        [patchLabels, imageIDs, leftTopMostPixIDs] = selectPatches(samplesInfo_RGBD_batch, sizeImg, countSamples, patchSize, patchSelectionStruct);
    else
        patchLabels = leftTopMostPixStruct.patchLabels;
        imageIDs = leftTopMostPixStruct.imageIDs;
        leftTopMostPixIDs = leftTopMostPixStruct.leftTopMostPixIDs;
    end

%% 4. Pre-processing of input data and/or patches
    [additionalInfo, patchMat, patchLabels] = preprocessDataAndOrPatches(data, patchSize, patchLabels, imageIDs, methodID_preprocess, sizeChn, leftTopMostPixIDs, sizeImg, countSamples);

%% 5. Create and return additional information structs and arrays    
    %for the selected patches, I also want to return the same table that
    %informs the patches position in its root image
    %leftTopMostPixIDs = sub2ind([sizeImg, sizeImg],allImageBlock(:,1), allImageBlock(:,2));
    [leftTopMostPixIDs_R,leftTopMostPixIDs_C] = ind2sub([sizeImg, sizeImg],leftTopMostPixIDs);
    patchInfo_RGBD = [samplesInfo_RGBD_batch(additionalInfo.imageIDs,1:3) leftTopMostPixIDs_R leftTopMostPixIDs_C samplesInfo_RGBD_batch(additionalInfo.imageIDs,7:10)];
    %Col-01 Person ID
    %Col-02 GestureID
    %Col-03 Picture ID
    %Col-04 patchRowStart
    %Col-05 patchColStart
    %Col-06 image Row Start - %look at loopOverAllFiles_02.m
    %Col-07 image Row End   - %look at loopOverAllFiles_02.m
    %Col-08 image Col Start - %look at loopOverAllFiles_02.m
    %Col-09 image Col End   - %look at loopOverAllFiles_02.m
    additionalInfo.patchInfo_RGBD = patchInfo_RGBD;
    additionalInfo.leftTopMostPixIDs = leftTopMostPixIDs;
end

function [  methodID_preprocess,  patchSelectionStruct...
          , sizeImg, countSamples, sizeChn, leftTopMostPixStruct...
          , countFeat  , displayOpts,  saveOpts...
         ] = getOptionalParams(data, varargin)
%   %% global variables
%     global curStep;curStep = 0;
%     global minFuncStepInfo;
%     minFuncStepInfo = []; %#ok<NASGU>
%     minFuncStepInfo = struct;
%     minFuncStepInfo.FunEvals = 1;
%     minFuncStepInfo.Iteration = 1;
    
  %% varargin parameters
    %P1D0 -> Passed assed paramaeter = 1, Default value used = 0
    [ methodID_preprocess,  patchCountPerSample,  initialPixel,  mostRecognizedAreaMat,  patchPickMethod,  leftTopMostPixStruct,  displayOpts,  saveOpts, P1D0] = parseArgs(...
    {'methodID_preprocess' 'patchCountPerSample' 'initialPixel' 'mostRecognizedAreaMat' 'patchPickMethod' 'leftTopMostPixStruct' 'displayOpts' 'saveOpts'},...
    {         3                    []                  []                   []                 []                  []                  []          []    },...
    varargin{:});

    %% General variables for display, save and minfunc purposes
    %if displayOpts is not passed as a parameter then set is to 'all' by
    %default to show the user what will happen in every detail.
    %also return it as a parameter because even if it is not passed in it
    %would be returned to be used for upcoming evaluations
    if (isempty(displayOpts))
        displayOpts = setDisplayOpts('all');
        if (displayOpts.defaultParamSet && P1D0.displayOpts==0)
            disp([displayOpts.ies{4} 'display options is set to -all- by default.']);
        end
    end
    
    %if saveOpts is not passed as a parameter then set is to 'none' by
    %default not to save anything into disk unnecessarily.
    %also return it as a parameter because even if it is not passed in it
    %would be returned to be used for upcoming evaluations
    if (isempty(saveOpts))
        saveOpts = setSaveOpts('none');
        if (displayOpts.defaultParamSet && P1D0.saveOpts==0)
            disp([displayOpts.ies{4} 'save options is set to -none- by default.']);
        end
    end
    
    %% Function specific variables in varargin param
    if (displayOpts.defaultParamSet && P1D0.methodID_preprocess==0)
        disp([displayOpts.ies{4} 'method for preprocessing the data is set to <normalize input and patch both> by default.']);
    end 
    %% Variables that depend on a mandatory input
    %  NONE
    %% Variables that can not be passed and only for initialization purposes
    [sizeImg, countSamples, sizeChn, countFeat] = exploreDataSize(data, 4);
    
    if isempty(initialPixel) && isempty(patchCountPerSample)
        patchCountPerSample = 12;
        if (displayOpts.defaultParamSet)
            disp([displayOpts.ies{4} 'patchCountPerSample is set to ' num2str(patchCountPerSample) ' by default.']);
        end 
    end
    
    patchSelectionStruct.patchCountPerSample = patchCountPerSample;
    patchSelectionStruct.initialPixel = initialPixel;
    patchSelectionStruct.mostRecognizedAreaMat = mostRecognizedAreaMat;
    patchSelectionStruct.patchPickMethod = patchPickMethod;
end

function [data, samplesInfo_RGBD_batch] = getData(dataIdentifier)
    data = [];
    %dataIdentifier is either a struct with parameters
    %dataIdentifier.{'data', 'labels', 'samplesInfo_RGBD_batch'}
    %or a mat file that has 'data', 'labels', 'samplesInfo_RGBD_batch' in it
    if ischar(dataIdentifier)
        try
            disp(['Loading file(' dataIdentifier ')'])
            load(dataIdentifier, 'data', 'labels', 'samplesInfo_RGBD_batch');%, 'uncompressedSize'
        catch
            disp(['There is a problem loading <' dataIdentifier '>. Check it out'])
            pause
        end
    elseif isstruct(dataIdentifier)
        data = dataIdentifier.data;
        %labels = dataIdentifier.labels;
        samplesInfo_RGBD_batch = dataIdentifier.samplesInfo_RGBD_batch;
    else
        error('Unknown type of input "dataIdentifier". It has to be either string or struct')
    end
end

function [additionalInfo, patchMat, patchLabels] = preprocessDataAndOrPatches(data, patchSize, patchLabels, imageIDs, methodID_preprocess, sizeChn, leftTopMostPixIDs, sizeImg, countSamples)
    switch methodID_preprocess
        case 1
            %grab patches with stride == patchSize
            %normalization : input(none), patches(none)
            additionalInfo = struct([]);
            [patchMat, invalidPatchIDs] = acquirePatches( patchSize, data, sizeChn, imageIDs, leftTopMostPixIDs, true);
        case 2
            [dataReshaped, additionalInfo] = normalizeInputData(data, sizeImg, countSamples, sizeChn);
            [patchMat, invalidPatchIDs] = acquirePatches( patchSize, dataReshaped, sizeChn, imageIDs, leftTopMostPixIDs, true);
        case 3
            %for this method we need to evaluate the meanPatches by using
            %all the patches that will be used during training - we
            %wouldn't be using only the mean of that current patch - I
            %think it would be a mistake to use the mean of initially
            %acquired patches mean to do this
            [dataReshaped, additionalInfo] = normalizeInputData(data, sizeImg, countSamples, sizeChn);
            [patchMat, invalidPatchIDs] = acquirePatches( patchSize, dataReshaped, sizeChn, imageIDs, leftTopMostPixIDs, true);
%             patchLabels(invalidPatchIDs) = [];
%             imageIDs(invalidPatchIDs) = [];
            [patchMat, meanPatch] = meanNormalizePatches(patchMat);            
            additionalInfo.meanPatch = meanPatch;
    end
    additionalInfo.imageIDs = imageIDs;
end

function [patchLabels, imageIDs, leftTopMostPixIDs] = selectPatches(samplesInfo_RGBD_batch, sizeImg, countSamples, patchSize, patchSelectionStruct)
    if (~isempty(patchSelectionStruct.mostRecognizedAreaMat))
        [patchLabels, imageIDs, leftTopMostPixIDs] = selectPatches_ByMostRecognizedArea(patchSelectionStruct, samplesInfo_RGBD_batch, patchSize);
    elseif (~isempty(patchSelectionStruct.patchCountPerSample))
        [patchLabels, imageIDs, leftTopMostPixIDs] = selectPatches_ByPatchCount(samplesInfo_RGBD_batch, sizeImg, countSamples, patchSize, patchSelectionStruct.patchCountPerSample);
    elseif (~isempty(patchSelectionStruct.initialPixel))
        if length(patchSelectionStruct.initialPixel)==1
            [patchLabels, imageIDs, leftTopMostPixIDs] = selectPatches_v02(samplesInfo_RGBD_batch, sizeImg, countSamples, patchSize, patchSelectionStruct.initialPixel);
        else
            patchLabels = [];
            imageIDs = [];
            leftTopMostPixIDs = [];
            for ip = 1:length(patchSelectionStruct.initialPixel)
                [patchLabels_Cur, imageIDs_Cur, leftTopMostPixIDs_Cur] = selectPatches_v02(samplesInfo_RGBD_batch, sizeImg, countSamples, patchSize, patchSelectionStruct.initialPixel(ip));
                patchLabels = [patchLabels;patchLabels_Cur];
                imageIDs = [imageIDs;imageIDs_Cur];
                leftTopMostPixIDs = [leftTopMostPixIDs;leftTopMostPixIDs_Cur];
            end
            clear patchLabels_Cur imageIDs_Cur leftTopMostPixIDs_Cur
        end
    else
        error('WTF');
    end
end

function [patchLabels, imageIDs, leftTopMostPixIDs] = selectPatches_v02(samplesInfo_RGBD_batch, sizeImg, countSamples, patchSize, initialPixel)
    error('This function is incorrect');
    % col 5 and 6 are R and C of the real image that is padded with sth
    % col  7 is the initial rowID where the real image reside
    % col  8 is the    last rowID where the real image reside
    % col  9 is the initial colID where the real image reside
    % col 10 is the    last colID where the real image reside
    
    %1 - what will be the size of patches?
    % patchSize = 10;%square -> patchSize by patchSize
    %2 - for each image there is an area that is selectable
    %    so we can start by picking the left topmost pixel which is
    %    selectable from each image then 
    %    all images are 227 by 227 so I need all the row and colID
    %    of patches
    initialPixel = mod(initialPixel,patchSize);
    if (initialPixel==0), initialPixel=1; end
    
    RC_SingleImage = 1:patchSize:sizeImg-patchSize;
    [Ri,Ci] = meshgrid(RC_SingleImage,RC_SingleImage');
    RC_SingleImage = [Ri(:),Ci(:)] + (initialPixel-1);%1st col rowIDs, 2nd col colIDs
    singleImagePatchCount = size(RC_SingleImage,1);
    allImageBlock = NaN([singleImagePatchCount*countSamples, 7]);
    
    patchLabels = zeros(singleImagePatchCount*countSamples,1);
    r_start = 1;
    
    for i = 1:countSamples
        r_CurImage = (samplesInfo_RGBD_batch(i,7)+initialPixel-1):patchSize:samplesInfo_RGBD_batch(i,8)-patchSize;
        c_CurImage = (samplesInfo_RGBD_batch(i,9)+initialPixel-1):patchSize:samplesInfo_RGBD_batch(i,10)-patchSize;
        [Ri,Ci] = meshgrid(r_CurImage,c_CurImage');
        RC_CurImage = [Ri(:),Ci(:)];%1st col rowIDs, 2nd col colIDs
        curImagePatchCount = size(RC_CurImage,1);
        
        r_end = r_start + curImagePatchCount-1;
        allImageBlock(r_start:r_end,1:2) = RC_CurImage;
        allImageBlock(r_start:r_end,3:6) = repmat(samplesInfo_RGBD_batch(i,7:10),[curImagePatchCount, 1]);
        allImageBlock(r_start:r_end,7) = i;
        patchLabels(r_start:r_end) = samplesInfo_RGBD_batch(i,2);
        r_start = r_end +1;
    end
    allImageBlock = allImageBlock(1:r_end,:,:);
    patchLabels = patchLabels(1:r_end);

    %row is smaller area
    rowsToDelete = allImageBlock(:,1)<allImageBlock(:,3);
    allImageBlock(rowsToDelete,:) = [];
    patchLabels(rowsToDelete,:) = [];
    %row is bigger area
    rowsToDelete = allImageBlock(:,1)+patchSize>allImageBlock(:,4);
    allImageBlock(rowsToDelete,:) = [];
    patchLabels(rowsToDelete,:) = [];
    %col is smaller area
    rowsToDelete = allImageBlock(:,2)<allImageBlock(:,5);
    allImageBlock(rowsToDelete,:) = [];
    patchLabels(rowsToDelete,:) = [];
    %col is bigger area
    rowsToDelete = allImageBlock(:,2)+patchSize>allImageBlock(:,6);
    allImageBlock(rowsToDelete,:) = [];
    patchLabels(rowsToDelete,:) = [];

    imageIDs = allImageBlock(:,7);
    leftTopMostPixIDs = sub2ind([sizeImg, sizeImg],allImageBlock(:,1), allImageBlock(:,2));
end

function [patchLabels, imageIDs, leftTopMostPixIDs] = selectPatches_v01(samplesInfo_RGBD_batch, sizeImg, countSamples, patchSize, initialPixel)
     error('This function is incorrect - not expected to be used-deprecated');   
    % col 5 and 6 are R and C of the real image that is padded with sth
    % col  7 is the initial rowID where the real image reside
    % col  8 is the    last rowID where the real image reside
    % col  9 is the initial colID where the real image reside
    % col 10 is the    last colID where the real image reside
    
    %1 - what will be the size of patches?
    % patchSize = 10;%square -> patchSize by patchSize
    %2 - for each image there is an area that is selectable
    %    so we can think as if all patches within an image is
    %    selectable, then remove the ones that re not selectable -
    %    makes sense
    %    all images are 227 by 227 so I need all the row and colID
    %    of patches
    initialPixel = mod(initialPixel,patchSize);
    if (initialPixel==0), initialPixel=1; end
    
    RC_SingleImage = initialPixel:patchSize:sizeImg-patchSize;
    [Ri,Ci] = meshgrid(RC_SingleImage,RC_SingleImage');
    RC_SingleImage = [Ri(:),Ci(:)];%1st col rowIDs, 2nd col colIDs
    singleImagePatchCount = size(RC_SingleImage,1);
    allImageBlock = NaN([singleImagePatchCount, 7, countSamples]);
    allImageBlock(:,1:2,:) = repmat(RC_SingleImage,[1,1,countSamples]);
    patchLabels = zeros(singleImagePatchCount,countSamples);
    for i = 1:countSamples
        allImageBlock(:,3:6,i) = repmat(samplesInfo_RGBD_batch(i,7:10),[singleImagePatchCount, 1]);
        allImageBlock(:,7,i) = i;
        patchLabels(:,i) = samplesInfo_RGBD_batch(i,2);
    end
    allImageBlock = permute(allImageBlock,[1,3,2]);
    allImageBlock = reshape(allImageBlock,[singleImagePatchCount*countSamples, 7]);
    patchLabels = reshape(patchLabels,[],1);

    %row is smaller area
    rowsToDelete = allImageBlock(:,1)<allImageBlock(:,3);
    allImageBlock(rowsToDelete,:) = [];
    patchLabels(rowsToDelete,:) = [];
    %row is bigger area
    rowsToDelete = allImageBlock(:,1)+patchSize>allImageBlock(:,4);
    allImageBlock(rowsToDelete,:) = [];
    patchLabels(rowsToDelete,:) = [];
    %col is smaller area
    rowsToDelete = allImageBlock(:,2)<allImageBlock(:,5);
    allImageBlock(rowsToDelete,:) = [];
    patchLabels(rowsToDelete,:) = [];
    %col is bigger area
    rowsToDelete = allImageBlock(:,2)+patchSize>allImageBlock(:,6);
    allImageBlock(rowsToDelete,:) = [];
    patchLabels(rowsToDelete,:) = [];

    imageIDs = allImageBlock(:,7);
    leftTopMostPixIDs = sub2ind([sizeImg, sizeImg],allImageBlock(:,1), allImageBlock(:,2));
end