function [ h ] = visuailzeImagePatchesPerCategory( ...
    data             ...
   ,patchSize        ...
   ,patchInfo_RGBD   ...
   ,rowsToSelect     ...
   ,labelActivations ...
   ,imageIDResults   ...
   ,letterID         ...
   ,figureID         ...
   ,methodID)
%visuailzeImagePatchesPerCategory Visualize an image from a selected
%category such that, all its patches will be displayed in a block
%mentioning either it is a correct or a false guess and the unnormalized
%activation along with its normalized activation in the title

    h = figure(figureID);clf;
    
%   Some features are learnt via ELM from patches of a batch
%   Then I either convolve the trained images or some other set of images
%   I get a 'imageIDResults' matrix that gives me :
%   colTitles = [ {'imageID'}...1
%                ,{'realLabelOfThesePatches'}...2
%                ,{'patchCountWithinThisImage'}...3
%                ,{'correctPatchCountRatio'}...4
%                ,{'correctPatchGuessCount'}...5
%                ,{'wrongPatchGuessCount'}...6
%                ,{'unnormedMaxActivatedPatchID'}...7
%                ,{'unnormedMaxActivation'}...8
%                ,{'unnormedMeanActivation'}...9
%                ,{'unnormedMinActivation'}...10
%                ]';
%   According to these values I would like to visualize some images's
%   patches, and see which patch gets activated correctly and which is not
%   I can have different methods to deal with this visualizations
%   Method-1 : The best image with all the patches correctly classified
%              ! Maybe there is no such image - so get the one with the
%              min value of incorrectPatchCount/totalPatchCount
%   Method-2 : The image with the highest maximum unnormalized 
%              patch activation, which is correctly classified


end

