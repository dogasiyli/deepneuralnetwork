function [deconvSet, filterAnalysisCell, unnecessaryFilterIDStruct, minMaxOfFiltersStruct] = visualizeActivationSurrey(cnnetArchitectureFileName, dataTypeStr, batchIDsToEvaluate)
    %cnnetArchitectureFileName

    cnnnet = [];
    %cnnetArchitectureFileName = 'D:\cnnnetStruct_commitCode(f70c952)-ds(SURREY-resized[80 80])-exampleID(2)_accuracy(97.96).mat';
    %cnnetArchitectureFileName = 'C:\FastDiskTemp\cnnnetStruct_00_accuracy(92.87).mat';
    load(cnnetArchitectureFileName);
    layerCount = size(cnnnet,1);
    
    dataLoadFunction = @createInitialDataStruct_surray;
    dataLoadParams = {'resized[80 80]'};
    dataSetName = ['SURREY-' dataLoadParams{1,1}];%
    [dS, batchInfo] = feval(dataLoadFunction, dataLoadParams{:});
    
    %dataTypeStr = 'test';
    batchCount = eval(['dS.countBatch.' dataTypeStr]);
    if exist('batchIDsToEvaluate','var')
        batchIDsToEvaluate(batchIDsToEvaluate>batchCount) = [];
    else
        batchIDsToEvaluate = 1:batchCount;
    end
    batchCount = length(batchIDsToEvaluate);
    dS = dS.updateDataStruct(dS, {'type', dataTypeStr});

    displayLevel = 'all';
    displayOpts = setDisplayOpts(displayLevel);
    %saveOpts = setSaveOpts('none', 'D:\deleteSaved\');
    saveOpts = setSaveOpts('none', 'C:\FastDiskTemp\deleteSaved\');

    
    accuracy_Arr = zeros(1,batchCount);
    confMat_Cells = cell(1,batchCount);
    accuracy_Mean = 0;
    conf_Mean = [];
    htx = @(M_) bsxfun(@rdivide,exp(M_),sum(exp(M_)));
    filterAnalysisCell = cell(batchCount, layerCount);
    countCategory = dS.countCategory;
    mostActivFeatMaps = zeros(countCategory,layerCount+1,batchCount);
    unnecessaryFilterIDStruct = struct([]);
    minMaxOfFiltersStruct = struct([]);
    for b=1:batchCount
        curBatchID = batchIDsToEvaluate(b);
        dS = dS.updateDataStruct(dS, {'batchID_current', curBatchID});
        load(dS.fileName_current,'data','labels');
        testData = reshape(data,dS.countFeat,[]);
        X = testData;
        activationID = 1;
        for layer = 1:layerCount
            switch cnnnet{layer,1}.type
                case 'convolution'
                    [X, cnnnet] = convolution_ForwardPropogate( cnnnet, layer, X,'dataStruct', dS ,'displayOpts', displayOpts,'saveOpts' , saveOpts,'activationID', activationID);
                    %for each filter normalize the data and create maps of
                    %min, max and sum of values per label
                    eachFilterOutImageSize = prod(cnnnet{layer,1}.outputSize_original(1:2));
                    countFilt = cnnnet{layer,1}.countFilt;
                    minMat = zeros(countFilt, countCategory);
                    maxMat = zeros(countFilt, countCategory);
                    sumMat = zeros(countFilt, countCategory);
                    try
                        minValsOfFeatMap = minMaxOfFiltersStruct{layer,1}.minVals;
                        maxValsOfFeatMap = minMaxOfFiltersStruct{layer,1}.maxVals;
                    catch
                        minValsOfFeatMap = zeros(batchCount,countFilt);
                        maxValsOfFeatMap = zeros(batchCount,countFilt);
                    end
                    for f = 1:countFilt
                        filterSlct = ((f-1)*eachFilterOutImageSize + 1):(f*eachFilterOutImageSize);
                        X_normed = X(filterSlct,:);
                        minValsOfFeatMap(b,f) = min(X_normed(:));
                        maxValsOfFeatMap(b,f) = max(X_normed(:));
                        X_normed = max(X_normed,0);%hardcoded for now
                        zeroX = X_normed==0;
                        X_normed = X_normed./max(X_normed(:));
                        X_normed(zeroX) = 0;
                        for ctg=1:countCategory
                            sampleSlct = find(labels==ctg);
                            Xslct = X_normed(:,sampleSlct(:));
                            minMat(f,ctg) = min(Xslct(:));
                            maxMat(f,ctg) = max(Xslct(:));
                            sumMat(f,ctg) = sum(Xslct(:));
                        end
                    end
                    minMaxOfFiltersStruct{layer,1}.minVals = minValsOfFeatMap;
                    minMaxOfFiltersStruct{layer,1}.maxVals = maxValsOfFeatMap;                 
                    clear X_normed zeroX f filterSlct ctg sampleSlct
                    filterAnalysisCell{b, layer} = struct;
                    filterAnalysisCell{b, layer}.minMat = minMat;
                    filterAnalysisCell{b, layer}.maxMat = maxMat;
                    filterAnalysisCell{b, layer}.sumMat = sumMat;
                    unnecessaryFilterIDs = find(sum(sumMat,2)==0);
                    eval(['unnecessaryFilterIDStruct{' num2str(layer) ',1}.b' num2str(curBatchID,'%02d') ' = unnecessaryFilterIDs;'])
                    if ~isempty(unnecessaryFilterIDs)
                        disp(['unnecessaryFilterIDs of layer(' num2str(layer) ') for batchID(' num2str(curBatchID) ') are - ' mat2str(unnecessaryFilterIDs(:))])
                    end
                case 'activation'
                    activationID = activationID + 1;
                    [X, cnnnet] = activation_ForwardPropogate( cnnnet, layer, X,'displayOpts' , displayOpts, 'saveOpts', saveOpts,'activationID', activationID);
                case 'pooling'
                    activationID = activationID + 1;
                    [X, cnnnet] = pooling_ForwardPropogate( cnnnet, layer, X,'displayOpts' , displayOpts ,'saveOpts', saveOpts,'activationID', activationID);
                case 'softmax'
                    [softMaxedData, cnnnet, predictionsOut] = softMax_ForwardPropogate( cnnnet, layer, X,'saveOpts', saveOpts,'displayOpts', displayOpts,'labels' , labels);
                case 'fullyconnected'
                    error('not implemented yet');
                    %[X, cnnnet, ~, W] = applyFullyConnected(cnnnet, layer, X, activationID, displayOpts, saveOpts, memoryMethod, curStep, dataStruct);
                otherwise
                    error('You are suggesting a layer that is not present.Layers are - convolution, activation, pooling, softmax');
            end
        end
        [accuracyCurBatch, confCurBatch, ~] = calculateConfusion(predictionsOut, labels, true, cnnnet{end,1}.countCategory);
        
        probabilitiesOut = htx(softMaxedData);
        
        accuracy_Arr(b) = accuracyCurBatch;
        confMat_Cells{b} = confCurBatch;
        accuracy_Mean = (accuracy_Mean*(b-1) + accuracyCurBatch)/(b);
        if isempty(conf_Mean)
            conf_Mean = confCurBatch;
        else
            conf_Mean = conf_Mean + confCurBatch;
        end
        try %#ok<TRYNC>
            drawConfusionMatWithNumbers( confCurBatch, struct('figureID', {layerCount+1}, 'figureTitle' , ['BatchID(' num2str(curBatchID) ') of ' dataSetName ' ' dataTypeStr ' data-acc(' num2str(accuracyCurBatch,'%4.2f') ')']));
        end
        
        [deconvSet, activationValueIndicePair, mostActivFeatMaps(:,:,b)] = visualizeCNNActivationResults( testData, labels, cnnnet, setDisplayOpts('none'), saveOpts, probabilitiesOut,struct('batchID',1));  
    end 
    try %#ok<TRYNC>
        drawConfusionMatWithNumbers( conf_Mean, struct('figureID', {layerCount+1}, 'figureTitle' , ['BatchMean of surrey test data-acc(' num2str(accuracy_Mean,'%4.2f') ')']));
    end
end

