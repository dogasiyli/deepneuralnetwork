function h = visualizeLearntFeatures( W, inputChannelSize, patchSize, figureID)
%visualizeLearntFeatures 

    %W is filterCount by (inputChannelSize*patchSize^2)
    [filterCount, d] = size(W);
    Rc = filterCount;
    Cc = inputChannelSize;

    h = figure(figureID);clf;
    for r=1:Rc
        for c = 1:Cc
            colStart = patchSize*patchSize*(c-1) + 1;
            colEnd = colStart + patchSize*patchSize - 1;
            wPatch = reshape(W(r,colStart:colEnd),patchSize,patchSize);
            subplot(Rc,Cc,sub2ind([Cc,Rc],c,r));
            imagesc(wPatch);
        end
    end

end

