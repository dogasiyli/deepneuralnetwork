function [imageIDResults_AnalyzeMat] = writeCSVFiles(allBatchParamsFolderName, batchID, iter, patchSize, accArr, sizeHidArr, imageIDResults, imageIDResults_AnalyzeMat)
    accuracyCalced = accArr(batchID,iter);
    sizeHid_Cur = sizeHidArr(batchID,iter);
    
    filename = ['Train_B(' num2str(batchID) ')_It(' num2str(iter) ')_Acc(' num2str(accuracyCalced,'%4.2f') ')_sizHid(' num2str(sizeHid_Cur) ').csv'];
    csvwrite([allBatchParamsFolderName filename], imageIDResults);
    disp(['imageIDResults is written into (' filename ') in folder(' allBatchParamsFolderName ')']);
    
    filename = 'Accuracy.csv';
    csvwrite([allBatchParamsFolderName filename], accArr);
    disp(['accArr is written into (' filename ') in folder(' allBatchParamsFolderName ')']);

    filename = 'sizeHidArr.csv';
    csvwrite([allBatchParamsFolderName filename], sizeHidArr);
    disp(['sizeHidArr is written into (' filename ') in folder(' allBatchParamsFolderName ')']);
            
    imageIDResults_AnalyzeMat_rowIDs = (1 + 24*(iter-1)):24*iter;
    %Col-1 : iterationID
    imageIDResults_AnalyzeMat(imageIDResults_AnalyzeMat_rowIDs,1) = iter;
    %Col-2 : LetterID
    imageIDResults_AnalyzeMat(imageIDResults_AnalyzeMat_rowIDs,2) = 1:24;
    %Col-3 : Accuracy
    imageIDResults_AnalyzeMat(imageIDResults_AnalyzeMat_rowIDs,3) = accuracyCalced;
    %Col-4 : HiddenSize
    imageIDResults_AnalyzeMat(imageIDResults_AnalyzeMat_rowIDs,4) = sizeHid_Cur;
    for c = 1:24
        imIDRes_slct = imageIDResults(imageIDResults(:,2)==c,:);
        %Col-5 : Wrong image count
        wi_slct = imIDRes_slct(imIDRes_slct(:,4)<=0.5,:);
        imageIDResults_AnalyzeMat(imageIDResults_AnalyzeMat_rowIDs(c), 5) = size(wi_slct,1);
        %Col-6 : wrong patch count in these wrong images
        wwpc = sum(wi_slct(:,6));
        imageIDResults_AnalyzeMat(imageIDResults_AnalyzeMat_rowIDs(c), 6) = wwpc;
        %Col-7 : true patch count in these wrong images
        wtpc = sum(wi_slct(:,5));
        imageIDResults_AnalyzeMat(imageIDResults_AnalyzeMat_rowIDs(c), 7) = wtpc;

        %Col-8 : True image count
        ti_slct = imIDRes_slct(imIDRes_slct(:,4)>0.5,:);
        imageIDResults_AnalyzeMat(imageIDResults_AnalyzeMat_rowIDs(c), 8) = size(ti_slct,1);
        %Col-9 : wrong patch count in these wrong images
        twpc = sum(ti_slct(:,6));
        imageIDResults_AnalyzeMat(imageIDResults_AnalyzeMat_rowIDs(c), 9) = twpc;
        %Col-10: true patch count in these wrong images
        ttpc = sum(ti_slct(:,5));
        imageIDResults_AnalyzeMat(imageIDResults_AnalyzeMat_rowIDs(c),10) = ttpc;
    end
    
    wrongImageCountVec = imageIDResults_AnalyzeMat(imageIDResults_AnalyzeMat_rowIDs,5);
    correctImageCountVec = imageIDResults_AnalyzeMat(imageIDResults_AnalyzeMat_rowIDs,8);
    totalImageCountVec = wrongImageCountVec+correctImageCountVec;
    imageIDResults_AnalyzeMat(imageIDResults_AnalyzeMat_rowIDs,11) = wrongImageCountVec./totalImageCountVec;
    imageIDResults_AnalyzeMat(imageIDResults_AnalyzeMat_rowIDs,12) = correctImageCountVec./totalImageCountVec;

    filename = ['imageIDResults_AnalyzeMat_B(' num2str(batchID) ')_patchSize' mat2str(patchSize) '.csv'];
    csvwrite([allBatchParamsFolderName filename], imageIDResults_AnalyzeMat);
    disp(['imageIDResults_AnalyzeMat is written into (' filename ') in folder(' allBatchParamsFolderName ')']);

end