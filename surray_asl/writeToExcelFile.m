function [ fileWrittenStatus, errMsgStr ] = writeToExcelFile( filename, variablesMat, varargin)
    %filename = 'testdata.xlsx';
    %variablesMat = [12.5, 98.38; 13.25, 99.1297; 14.11, 97.087];
    %columnNamesCells = {'Time','Temperature'};
    %A = {'Time','Temperature'; 12,98; 13,99; 14,97};
    %sheetID - 1 will be default
    %xlRange = 'A1';-can also be 'D2:H4' like rectangular range
    [rowCount, colCount] = size(variablesMat);
    [ filename, columnNamesCells, sheetID, xlRange, draftFileName, xlRangeWC] = getOptionalParams(filename, rowCount, colCount, varargin{:});
    
    %1. If there is a draftFile
    %   first save the draft file as your new file
    %   then write your data starting with the first line available
    %   but the xlRange becomes important in this case
    %   if nothing is mentioned thern default is the 2nd line meanin A2
    %   but if the initial line is passed then AX will be the xlRange where
    %   X is the initial line passed
    draftFileIsUsed = false;
    if ~isempty(draftFileName)
        if exist(draftFileName,'file')
            %first try and if there is a problem loading the draft excel file then do not
            %load it
            try
                copyfile(draftFileName, filename, 'f');
                disp(['draftFile(' draftFileName ') is copied to its new destination(' filename ')']);
                draftFileIsUsed = true;
            catch
                disp(['There is a problem with the draftFile(' draftFileName '). New file has been created.']);
                draftFileIsUsed = false;
            end
        else
            disp(['The draftFile(' draftFileName ') doesnt exist. New file has been created.']);
            draftFileIsUsed = false;            
        end
    end
    
    if (draftFileIsUsed || isempty(columnNamesCells))
        %columnNamesCells - will not be used 
        %warning('draft file can not be used - it is overwritten :(');
        A = num2cell(variablesMat);
        [fileWrittenStatus, errMsgStr] = xlswrite(filename,A,sheetID,xlRange);
    else
        %columnNamesCells - will be used 
        B = num2cell(variablesMat);
        A = [columnNamesCells; B];
        [fileWrittenStatus, errMsgStr] = xlswrite(filename,A,sheetID,xlRangeWC);
    end
end

% STEP-1 : Set optional param defaults
function [ filename, columnNamesCells, sheetID, xlRange_calced, draftFileName, xlRangeWC] = getOptionalParams(filename, rowCount, colCount, varargin)
    %Optional inputs with default values
    %1. columnNamesCells - 
    %2. sheetID - 
    %3. xlRange - 
    %4. draftFileName - 
    %P1D0 -> Passed assed parameter = 1, Default value used = 0
    [   columnNamesCells,  sheetID,  xlRange,  col_Beg,  row_Beg,  draftFileName, P1D0] = parseArgs(...
    {  'columnNamesCells' 'sheetID' 'xlRange' 'col_Beg' 'row_Beg' 'draftFileName' },...
    {          []             1         []        1         2            []       },...
    varargin{:});

    if isempty(strfind(filename,'.xls'))
        filename = [filename '.xlsx'];
    end
   
    if (P1D0.columnNamesCells==0 || (~isempty(columnNamesCells) && length(columnNamesCells)~=colCount))
        columnNamesCells = setIterativeCellStr( 'col', colCount );
    end

    if (P1D0.xlRange==1)
        try
            semiColumnIndex = strfind(xlRange,':');
            %it can have either a rectangle or a single cell
            isSingleCell = ~isempty(xlRange) && isempty(semiColumnIndex);
            isRectangleCell = ~isempty(xlRange) && ~isempty(semiColumnIndex);
            if isSingleCell
                %the initial letter represents col
                %the latter letters represent rowID
                row_Beg = str2num(xlRange(2:end));
                col_Beg = upper(xlRange(1))-'A'+1;
            elseif isRectangleCell
                row_Beg = str2num(xlRange(2:semiColumnIndex-1));
                col_Beg = upper(xlRange(1))-'A'+1;
            else
                row_Beg = 2;
                col_Beg = 1;
                warning(['Couldnt analyze <' xlRange  '> as an excel range string']);
            end
        catch
            row_Beg = 2;
            col_Beg = 1;
            warning(['Couldnt analyze <' xlRange  '> as an excel range string']);
        end
    end

    %the row_Beg and col_Beg are initialized by passed params or from xlRange
    %here we hcalculate the end row and cols
    row_End = row_Beg + rowCount-1;
    col_End = col_Beg + colCount-1;
    
    xlRange_calced_fr_col = char(double('A') + col_Beg - 1);
    xlRange_calced_fr_row = num2str(row_Beg);
    if col_End>26 %last character = Z==26
        col_End_Init = floor(col_End/26);
        col_End_Last = col_End - col_End_Init*26;
        xlRange_calced_to_col = [char(double('A') + col_End_Init - 1) char(double('A') + col_End_Last - 1)];
    else
        xlRange_calced_to_col = char(double('A') + col_End - 1);
    end
    xlRange_calced_to_row = num2str(row_End);
    xlRange_calced = [ xlRange_calced_fr_col xlRange_calced_fr_row ':' xlRange_calced_to_col xlRange_calced_to_row];
    xlRangeWC = [ xlRange_calced_fr_col xlRange_calced_fr_row ':' xlRange_calced_to_col num2str(row_End+1)];
    %disp(['xlRange_calced-->' xlRange_calced])

    if (P1D0.draftFileName==1 && ~isempty(draftFileName))
        %the initial line is the 2nd one by default
        %otherwise it will be the one 
    end
end