function [X, labels, labelHist] = createTripletErr_Data( Nk, K, D, figureID )
%createTripletErr_Data This function creates Nk samples from each K class of D dimensions
%for triplet error testing

    labels = reshape(bsxfun(@plus,zeros(Nk,1),1:K),1,K*Nk);
    X = zeros(K*Nk,D);
    mu_All = eye(K);
    mu_All(mu_All>0) = 2:K:2+K*(K-1);%;[2 5 8];%randi(10,[1 K]);
    for k=1:K
        x = generateAutoencoderExample(Nk, D, 1, false);
        x = bsxfun(@plus,x,mu_All(k,:));
        X(labels==k,:) = x;
    end
    %[labels, C] = kmeans(Y_sig, K);
    
    if exist('figureID','var') && ~isempty(figureID)
        figure(figureID);clf;hold on;
        subplot(2,1,1);hold on;
        for k=1:K
            col = rand(1,3);
            scatter3(X(labels==k,1),X(labels==k,2),X(labels==k,3),'MarkerFaceColor',col);
        end
        subplot(2,1,2);hist(labels,unique(labels));
    end
    labelHist = hist(labels,unique(labels));
end

