function [ tripletIdx ] = generateAllPossibleTriplets(labels)
%generateAllPossibleTriplets creates all possible triplets according to the
%labels given
%   This is regardless of the distances of samples

%so a triplet consists of anchor,positive,negative
%for each sample we need to pair it with another sample from its own class
%if a class has Nk samples there a re Nk*(Nk-1) couples like this

%there are Nnotk samples that are in the dataset which belongs to different
%classes. so for that class for eachcouple there are Nnotk samples to make
%a triplet

%this results in sum_over_k(Nk*(Nk-1)*Nnotk)
%if we assume that for each class there are Nk samples and there are K
%classes then there are K*( (Nk*(Nk-1)) * (K-1)*Nk ) possible triplets

%hence having same number of samples from each class makes thiings faster
%we only calculate a matrix and a vector at first, then create a bigger one
%with these (adding, mapping, extrapolating)

    uniqLabels = unique(labels);
    K = length(uniqLabels);
    Nk = hist(labels, uniqLabels);

    tripletGenerateMode = findGenerateMode(Nk);

    switch tripletGenerateMode
        case 'allequal'
            n = Nk(1);
            %there are n samples per class
            %suppose the samples were labelled such as
            %[1..n;n+1..2*n;2*n+1..3*n;...n*K]
            %I need to create the couple grouping for n samples from a
            %class
            n_grouping = get_n_grouping(n);
            k_grouping = get_k_grouping(K);
                        
            grCntPerClass = size(n_grouping,1);
            grCntPerGroup = size(k_grouping,1);
            
            tripletIdx = NaN(grCntPerGroup*grCntPerClass*n, 5);
            
            to = 0;
            for k=1:grCntPerGroup
                i = k_grouping(k,1);
                j = k_grouping(k,2);
                ids_i = find(labels==i);
                ids_j = find(labels==j);
                i_mat = repmat(ids_i(n_grouping),n,1);
                j_vec = reshape(repmat(ids_j,grCntPerClass,1),[n*grCntPerClass,1]);
                i_l = i*ones(size(j_vec));
                j_l = j*ones(size(j_vec));
                triplet_i_j =  [i_mat j_vec i_l j_l];
                fr = to + 1;
                to = fr + n*grCntPerClass - 1;
                tripletIdx(fr:to,:) = triplet_i_j;
            end
            
        case 'grouped'
        otherwise%case 'varying';
    end
end

function n_grouping = get_n_grouping(n)
% there are n samples from a class
% we need to find each grouping of different samples
% [1 2;1 3;...;1 n;2 3;2 4;...;2 n;...;n-1 n]
    n_grouping = nchoosek(1:n,2);
end

function k_grouping = get_k_grouping(k)
% there are n samples from a class
% we need to find each grouping of different samples
% [1 2;1 3;...;1 n;2 3;2 4;...;2 n;...;n-1 n]
    all_grouping = nchoosek(1:k,3);
    grCnt = size(all_grouping,1);
    %perms gives 6 different combinations of a 3 grouping
    %[ a p n]
    %1-1 2 3
    %2-1 3 2
    %3-2 1 3
    %4-2 3 1
    %5-3 1 2
    %6-3 2 1
    k_grouping_length = 6*grCnt;
    k_grouping = zeros(k_grouping_length,3);
    to = 0;
    for i=1:grCnt
        fr = to+1;
        to = fr+6-1;
        k_grouping(fr:to,:) = perms(all_grouping(i,:));
    end
    k_grouping = sortrows(k_grouping,1:3);
end

function tripletGenerateMode = findGenerateMode(Nk)
%    Nk is number of samples per class
% if all has the same number of samples it is 'allequal'
% if there are big groups of same samples it is 'grouped'
% if all have different values it is 'varying'
    uniqNk = unique(Nk);
    histNk = hist(Nk, uniqNk);
    if length(uniqNk)==1
        tripletGenerateMode = 'allequal';
    elseif max(histNk) > length(Nk)*0.5
        tripletGenerateMode = 'grouped';
    else
        tripletGenerateMode = 'varying';
    end
end