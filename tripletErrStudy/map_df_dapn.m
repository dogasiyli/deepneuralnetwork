function [ df_dapn_mp ] = map_df_dapn(df_d_apn, tripletIdx_slctd, apn_123, N )
N_trip = size(tripletIdx_slctd,1);
df_da_idx = [tripletIdx_slctd(:,apn_123)' 1:N];%add dummy sampleIDx to end, for the following line not to cry about it
    W = full(sparse((df_da_idx)',1:length(df_da_idx),ones(1,length(df_da_idx))))';
    W_mp = sparse(W(1:N_trip,:));
    df_dapn_mp = W_mp'*df_d_apn;
    
    %check if the accumulated error is zero for non existing samples
    nonZeroSamplesFound = reshape(find(sum(abs(df_dapn_mp),2)~=0),1,[]);
    nonZeroSamplesGiven = unique(df_da_idx(1:N_trip));
    assert(length(nonZeroSamplesGiven)==length(nonZeroSamplesFound) && sum(nonZeroSamplesFound-nonZeroSamplesGiven)==0,'error')
end

