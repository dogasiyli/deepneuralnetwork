

Nk = 50;%samples per class
K = 3;%number of classes
D = 3;%input dimension size
alfaParam = 0.1;
figureID = 1;%[];%
[X, labels, labelHist] = createTripletErr_Data(Nk, K, D, figureID);

tripletIdx  = generateAllPossibleTriplets(labels);

tic;
X_dist = sqrt(pdist2_fast(X,X));
fast_t = toc;

tic;
D_simp = pdist2(X,X,'euclidean');
simp_t = toc;

norm_fast_simp = norm(X_dist-D_simp,'fro');

disp(['  fast distance func last ' num2str(fast_t,'%4.2f') ' seconds']);
disp(['simple distance func last ' num2str(simp_t,'%4.2f') ' seconds']);
disp(['distance difference ' num2str(norm_fast_simp,'%4.2f')]);

%now map distances from X_dist to tripletIdx
%distances of where 
%the first 3 cols of it are a/p/n
%4 is the classLabel(a and p)
%5 is the classLabel(n)
tripletErr_a_p = X_dist(sub2ind(size(X_dist),tripletIdx(:,1),tripletIdx(:,2)));
tripletErr_a_n = X_dist(sub2ind(size(X_dist),tripletIdx(:,1),tripletIdx(:,3)));
tripletErr = tripletErr_a_n - tripletErr_a_p;

tripLetErrMat = [tripletIdx tripletErr_a_p tripletErr_a_n tripletErr];

slctdTriplets = find(tripletErr<alfaParam);
N_trip = length(slctdTriplets);

tripLetErrMat_slctd = tripLetErrMat(slctdTriplets,:);
tripletIdx_slctd = tripletIdx(slctdTriplets,1:3); 
unqiqueSamplesSelected = unique(tripLetErrMat_slctd(:,1:3));
usc = [length(unique(tripLetErrMat_slctd(:,1))) length(unique(tripLetErrMat_slctd(:,2))) length(unique(tripLetErrMat_slctd(:,3)))];
clear tripLetErrMat slctdTriplets tripletErr tripletErr_a_p tripletErr_a_n

disp([mat2str(labelHist,4) ' number of samples per classes created ' num2str(length(tripletIdx),'%d') ' number of triplets']);
disp(['From ' num2str(length(tripletIdx),'%d') ' number of triplets, only ' num2str(size(tripLetErrMat_slctd,1),'%d') ' of them are selected.']);
disp(['From ' num2str(length(labels),'%d') ' number of samples, uniqueCnt(anchor,positive,negative) ' mat2str(usc,4) ' (' num2str(length(unqiqueSamplesSelected),'%d') ') of them contribute to the system.']);

df_da = (X(tripLetErrMat_slctd(:,3),:) - X(tripLetErrMat_slctd(:,2),:));
df_dp = (X(tripLetErrMat_slctd(:,1),:) - X(tripLetErrMat_slctd(:,3),:));
df_dn = (X(tripLetErrMat_slctd(:,2),:) - X(tripLetErrMat_slctd(:,1),:));

%now from each class select 3 least affected and 3 most affected samples
%plot all the directions applied on them

samples_hist = reshape(hist(tripletIdx_slctd(:),1:K*Nk),[],K);

%now accumulate the df_dX over each sample
df_da_mp = map_df_dapn(df_da, tripletIdx_slctd, 1, K*Nk);
df_dp_mp = map_df_dapn(df_dp, tripletIdx_slctd, 2, K*Nk);
df_dn_mp = map_df_dapn(df_dn, tripletIdx_slctd, 3, K*Nk);
df_accum = (df_da_mp + df_dp_mp + df_dn_mp);
df_accum = (2/N_trip)*df_accum;

if exist('figureID','var') && ~isempty(figureID)
    figure(figureID+1);clf;hold on;
    subplot(2,1,2);hist(tripletIdx_slctd(:),1:K*Nk);
    for k=1:K
        subplot(2,1,1);hold on;
        col = rand(1,3);
        scatter3(X(labels==k,1),X(labels==k,2),X(labels==k,3),10+samples_hist(:,k)','MarkerFaceColor',col);
        quiver(p1(1),p1(2),dp(1),dp(2),0);
        
        [~, sampleBiggestIdx] = max(samples_hist(:,k));
        minCnts = sort(samples_hist(:,k));
        minVal = find(minCnts>0,1,'first');
        minVal = minCnts(minVal); 
        [~, sampleMinIdx] = find(samples_hist(:,k)==minVal,1,'first');
        
        sampleBiggestIdx = sampleBiggestIdx + (k-1)*Nk;
        sampleMinIdx = sampleMinIdx + (k-1)*Nk;
        %draw all the lines from the biggest to wherever they need to move
        %to
        df_da_idx_bs = find(tripletIdx_slctd(:,1)==sampleMinIdx);
        df_dp_idx_bs = find(tripletIdx_slctd(:,2)==sampleMinIdx);
        df_dn_idx_bs = find(tripletIdx_slctd(:,3)==sampleMinIdx);
        frX = mean(X(labels==k,:));
        difMean = Nk*mean(df_accum(labels==k,:));
        toX = frX + difMean;
        colCur = zeros(1,3);
        colCur(k) = 1;
        quiver3([frX(1) toX(1)],[frX(2) toX(2)],[frX(3) toX(3)],[frX(1) toX(1)],[frX(2) toX(2)],[frX(3) toX(3)]);
        line([frX(1) toX(1)],[frX(2) toX(2)],[frX(3) toX(3)],'Color',colCur);
    end
end